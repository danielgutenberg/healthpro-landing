<?php

return [

	'calendar_name' => 'HealthPro',

	'drivers' => [
		'google' => \WL\Modules\ProfileCalendar\Models\CalendarDriverGoogle::class,
	],

];

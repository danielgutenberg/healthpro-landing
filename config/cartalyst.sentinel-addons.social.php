<?php
/**
 * Part of the Sentinel Social package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Sentinel Social
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

return [

	/*
	|--------------------------------------------------------------------------
	| Connections
	|--------------------------------------------------------------------------
	|
	| Connections are simple. Each key is a unique slug for the connection. Use
	| anything, just make it unique. This is how you reference it in Sentinel
	| Social. Each slug requires a driver, which must match a valid inbuilt
	| driver or may match your own custom class name that inherits from a
	| valid base driver.
	|
	| Make sure each connection contains an "identifier" and a "secret". Thse
	| are also known as "key" and "secret", "app id" and "app secret"
	| depending on the service. We're using "identifier" and
	| "secret" for consistency.
	|
	| OAuth2 providers may contain an optional "scopes" array, which is a
	| list of scopes you're requesting from the user for that connection.
	|
	| You may use multiple connections with the same driver!
	|
	*/

	'connections' => [

		'facebook' => [
			'driver'     => 'WL\Packages\SentinelSocial\Facebook',
			'identifier' => env('connections_facebook_identifier', ''),
			'secret'     => env('connections_facebook_secret', ''),
			'scopes'     => ['email', 'public_profile', 'user_friends'],
		],

//		'github' => [
//			'driver'     => 'GitHub',
//			'identifier' => '',
//			'secret'     => '',
//			'scopes'     => ['user'],
//		],

		// right now google doesn't allow authentication on local addresses
		'google' => [
			'driver'     => 'WL\Packages\SentinelSocial\Google',
			'identifier' => env('connections_google_identifier', ''),
			'secret'     => env('connections_google_secret', ''),
			'scopes'     => [
				'https://www.googleapis.com/auth/userinfo.profile',
				'https://www.googleapis.com/auth/userinfo.email',
			],
		],

		'linkedin' => [
			'driver'     => 'WL\Packages\SentinelSocial\LinkedIn',
			'identifier' => env('connections_linkedin_identifier', ''),
			'secret'     => env('connections_linkedin_secret', ''),
			'scopes'     => ['r_basicprofile', 'r_emailaddress'],
		],

//		'microsoft' => [
//			'driver'     => 'Microsoft',
//			'identifier' => '',
//			'secret'     => '',
//			'scopes'     => ['wl.basic', 'wl.emails'],
//		],

//		'twitter' => [
//			'driver'     => 'Twitter',
//			'identifier' => env('connections_twitter_identifier', 'ZYUUkB9sF7nqk3hFEwJONAapM'),
//			'secret'     => env('connections_twitter_secret', 'j3Ot8cZHuc6HRyrozFC0Lonvk72xKc17EhTnzVM5AMoZYXbauD'),
//		],

//		'tumblr' => [
//			'driver'     => 'Tumblr',
//			'identifier' => '',
//			'secret'     => '',
//		],
//
//		'vkontakte' => [
//			'driver'     => 'Vkontakte',
//			'identifier' => '',
//			'secret'     => '',
//			'scopes'     => [],
//		],

	],

	/*
	|--------------------------------------------------------------------------
	| Social Link Model
	|--------------------------------------------------------------------------
	|
	| When users are registered, a "social link provider" will map the social
	| authentications with user instances. Feel free to use your own model
	| with our provider.
	|
	*/

	'link' => '\WL\Modules\ProfileGroup\Models\SocialLink',

];

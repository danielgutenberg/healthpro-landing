<?php

return [

	'env' => env('APP_ENV', 'develop'),
	/*
	|--------------------------------------------------------------------------
	| Application Debug Mode
	|--------------------------------------------------------------------------
	|
	| When your application is in debug mode, detailed error messages with
	| stack traces will be shown on every error that occurs within your
	| application. If disabled, a simple generic error page is shown.
	|
	*/

	'debug' => env('APP_DEBUG', false),
	/*
	|--------------------------------------------------------------------------
	| Application URL
	|--------------------------------------------------------------------------
	|
	| This URL is used by the console to properly generate URLs when using
	| the Artisan command line tool. You should set this to the root of
	| your application so that it is used when running Artisan tasks.
	|
	*/

	'url' => env('APP_URL', 'http://localhost'),

	/*
	|--------------------------------------------------------------------------
	| Logging Configuration
	|--------------------------------------------------------------------------
	|
	| Here you may configure the log settings for your application. Out of
	| the box, Laravel uses the Monolog PHP logging library. This gives
	| you a variety of powerful log handlers / formatters to utilize.
	|
	| Available Settings: "single", "daily", "syslog"
	|
	*/
	'log' => 'daily',

	/*
	|--------------------------------------------------------------------------
	| Application Timezone
	|--------------------------------------------------------------------------
	|
	| Here you may specify the default timezone for your application, which
	| will be used by the PHP date and date-time functions. We have gone
	| ahead and set this to a sensible default for you out of the box.
	|
	*/

	'timezone' => 'UTC',
	/*
	|--------------------------------------------------------------------------
	| Application Locale Configuration
	|--------------------------------------------------------------------------
	|
	| The application locale determines the default locale that will be used
	| by the translation service provider. You are free to set this value
	| to any of the locales which will be supported by the application.
	|
	*/

	'locale' => 'en',
	/*
	|--------------------------------------------------------------------------
	| Encryption Key
	|--------------------------------------------------------------------------
	|
	| This key is used by the Illuminate encrypter service and should be set
	| to a random, long string, otherwise these encrypted values will not
	| be safe. Make sure to change it before deploying any application!
	|
	*/

	'key' => env('APP_ENCRYPTION_KEY', 'RkouFKiXZEmz2kiPEONAkQrb1EmUVbU8'),
	'cipher' => MCRYPT_RIJNDAEL_128,
	/*
	|--------------------------------------------------------------------------
	| Autoloaded Service Providers
	|--------------------------------------------------------------------------
	|
	| The service providers listed here will be automatically loaded on the
	| request to your application. Feel free to add your own services to
	| this array to grant expanded functionality to your applications.
	|
	*/

	'providers' => [


		/*
		 * Laravel Framework Service Providers...
		 */
		'Illuminate\Auth\AuthServiceProvider',
		'Illuminate\Bus\BusServiceProvider',
		'Illuminate\Cache\CacheServiceProvider',
		'Illuminate\Foundation\Providers\ConsoleSupportServiceProvider',
		'Illuminate\Cookie\CookieServiceProvider',
		'Illuminate\Database\DatabaseServiceProvider',
		'Illuminate\Encryption\EncryptionServiceProvider',
		'Illuminate\Filesystem\FilesystemServiceProvider',
		'Illuminate\Foundation\Providers\FoundationServiceProvider',
		'Illuminate\Hashing\HashServiceProvider',
		'Illuminate\Mail\MailServiceProvider',
		'Illuminate\Pagination\PaginationServiceProvider',
		'Illuminate\Pipeline\PipelineServiceProvider',
		'Illuminate\Queue\QueueServiceProvider',
		'Illuminate\Redis\RedisServiceProvider',
		#'Illuminate\Auth\Passwords\PasswordResetServiceProvider',
		'Illuminate\Session\SessionServiceProvider',
		'Illuminate\Translation\TranslationServiceProvider',
		'Illuminate\Validation\ValidationServiceProvider',
		'Illuminate\View\ViewServiceProvider',
		'Illuminate\Broadcasting\BroadcastServiceProvider',


		/*
		 * Package Service Providers...
		 */
		'Former\FormerServiceProvider',
		'Cartalyst\Sentinel\Laravel\SentinelServiceProvider',
		'Cartalyst\Sentinel\Addons\Social\Laravel\SocialServiceProvider',
		'Stevebauman\Location\LocationServiceProvider',
		'Cviebrock\EloquentSluggable\SluggableServiceProvider',
		'Watson\Sitemap\SitemapServiceProvider',
		'Creativeorange\Gravatar\GravatarServiceProvider',
		'Jenssegers\Agent\AgentServiceProvider',
        'Aloha\Twilio\Support\Laravel\ServiceProvider',

		'App\Providers\BusServiceProvider',
		'App\Providers\ConfigServiceProvider',
		'App\Providers\EventServiceProvider',
		'App\Providers\RouteServiceProvider',
		'App\Providers\SitemapServiceProvider',
//		'App\Providers\BindingProvider',
		'App\Providers\LogServiceProvider',
		'App\Providers\UserServiceProvider',
		'App\Providers\SubscriberServiceProvider',
		'App\Providers\RepositoriesServiceProvider',
		'App\Providers\PopulatorsServiceProvider',
		'App\Providers\ErrorServiceProvider',
		'App\Providers\UrlGeneratorServiceProvider',
//		'WL\Modules\Notification\Providers\NotificationServiceProvider',
		'App\Providers\ValidationServiceProvider',
		'App\Providers\BladeServiceProvider',
		'App\Providers\DevEnvironmentServiceProvider',
		'App\Providers\ViewComposerServiceProvider',
		'App\Providers\EventSubscriberServiceProvider',
		'App\Providers\SessionServiceProvider',
//		'App\Providers\RelationServiceProvider',
		'WL\Modules\Coupons\Providers\CouponServiceProvider',
		'WL\Modules\Provider\ServiceProviders\ProviderServiceProvider',
		'WL\Modules\Questionnaires\QuestionnaireServiceProvider',

		/*
		 * WL Service Providers...
		 */

		'WL\Security\Providers\AuthorizationServiceProvider',
		'WL\Yaml\YamlServiceProvider',
		'WL\Manifest\ManifestServiceProvider',
		'WL\Modules\Meta\Providers\MetaServiceProvider',
		'WL\Modules\Menu\ServiceProviders\MenuSettingsServiceProvider',
		'WL\Modules\Profile\Providers\ProfileServiceProvider',
		'WL\Settings\SettingsServiceProvider',
		'WL\Gettext\GettextServiceProvider',
		'WL\Translator\TranslatorServiceProvider',
		'WL\AssetManager\AssetManagerServiceProvider',

		//NOTHING goes below here.



	],
	/*
	|--------------------------------------------------------------------------
	| Service Provider Manifest
	|--------------------------------------------------------------------------
	|
	| The service provider manifest is used by Laravel to lazy load service
	| providers which are not needed for each request, as well to keep a
	| list of all of the services. Here, you may set its storage spot.
	|
	*/

	'manifest' => storage_path() . '/meta',
	/*
	|--------------------------------------------------------------------------
	| Class Aliases
	|--------------------------------------------------------------------------
	|
	| This array of class aliases will be registered when this application
	| is started. However, feel free to register as many as you wish as
	| the aliases are "lazy" loaded so they don't hinder performance.
	|
	*/

	'aliases' => [
		/* Laravel Base Aliases */
		'App' => 'Illuminate\Support\Facades\App',
		'Artisan' => 'Illuminate\Support\Facades\Artisan',
		'Auth' => 'Illuminate\Support\Facades\Auth',
		'Blade' => 'Illuminate\Support\Facades\Blade',
		'Bus' => 'Illuminate\Support\Facades\Bus',
		'Cache' => 'Illuminate\Support\Facades\Cache',
		'Config' => 'Illuminate\Support\Facades\Config',
		'Cookie' => 'Illuminate\Support\Facades\Cookie',
		'Crypt' => 'Illuminate\Support\Facades\Crypt',
		'DB' => 'Illuminate\Support\Facades\DB',
		'Eloquent' => 'Illuminate\Database\Eloquent\Model',
		'Event' => 'Illuminate\Support\Facades\Event',
		'File' => 'Illuminate\Support\Facades\File',
		'Hash' => 'Illuminate\Support\Facades\Hash',
		'Input' => 'Illuminate\Support\Facades\Input',
		'Inspiring' => 'Illuminate\Foundation\Inspiring',
		'Lang' => 'Illuminate\Support\Facades\Lang',
		'Log' => 'Illuminate\Support\Facades\Log',
		'Mail' => 'Illuminate\Support\Facades\Mail',
		'Password' => 'Illuminate\Support\Facades\Password',
		'Queue' => 'Illuminate\Support\Facades\Queue',
		'Redirect' => 'Illuminate\Support\Facades\Redirect',
		'Redis' => 'Illuminate\Support\Facades\Redis',
		'Request' => 'Illuminate\Support\Facades\Request',
		'Response' => 'Illuminate\Support\Facades\Response',
		'Route' => 'Illuminate\Support\Facades\Route',
		'Schema' => 'Illuminate\Support\Facades\Schema',
		'Session' => 'Illuminate\Support\Facades\Session',
		'Storage' => 'Illuminate\Support\Facades\Storage',
		'URL' => 'Illuminate\Support\Facades\URL',
		'Validator' => 'Illuminate\Support\Facades\Validator',
		'View' => 'Illuminate\Support\Facades\View',

		/* Laravel Optional Aliases */
		'ClassLoader' => 'Illuminate\Support\ClassLoader',
		'Controller' => 'Illuminate\Routing\Controller',
		'Paginator' => 'Illuminate\Support\Facades\Paginator',
		'Seeder' => 'Illuminate\Database\Seeder',
		'SSH' => 'Illuminate\Support\Facades\SSH',
		'Str' => 'Illuminate\Support\Str',

		/* Package Aliases */

		'Activation' => 'Cartalyst\Sentinel\Laravel\Facades\Activation',
		'Reminder' => 'Cartalyst\Sentinel\Laravel\Facades\Reminder',
		'Sentinel' => 'Cartalyst\Sentinel\Laravel\Facades\Sentinel',
		'SentinelSocial' => 'Cartalyst\Sentinel\Addons\Social\Laravel\Facades\Social',
		'Carbon' => 'Carbon\Carbon', //
		'Former' => 'Former\Facades\Former', // Former
		'Location' => 'Stevebauman\Location\Facades\Location',
		'Sitemap' => 'Watson\Sitemap\Facades\Sitemap',
		'Google' => 'PulkitJalan\Google\Facades\Google',
		'Gravatar' => 'Creativeorange\Gravatar\Facades\Gravatar',
        'Twilio' => 'Aloha\Twilio\Support\Laravel\Facade',

		/* WL Aliases */
//		'AssetManager' => 'WL\AssetManager\Facades\AssetManager', // AssetManager Alias,
		'Manifest' => 'WL\Manifest\Facades\Manifest', // Manifest Alias,
		'BladeCompiler' => 'WL\Gettext\Facades\BladeCompiler', // BladeCompiler Alias
		'Gettext' => 'WL\Gettext\Facades\Gettext', // Gettext Alias
		'MetaHelper' => 'WL\Helpers\Models\Meta',
		'MenuHelper' => 'WL\Helpers\Menu',
		'UserMenuHelper' => 'WL\Helpers\UserMenu',
		'TrackingHelper' => 'WL\Helpers\Tracking',
		'ModelProfile' => 'WL\Modules\Profile\Models\ModelProfile',
		'Setting' => 'WL\Settings\Facades\Setting', // Settings Alias
		'MetaService' => 'WL\Modules\Meta\Facades\MetaService', // Meta Service Alias
		'DBTranslator' => 'WL\Translator\Facades\DBTranslator', // Translation Alias
		'Yaml' => 'WL\Yaml\Facades\Yaml', // Yaml Alias'Agent' => 'Jenssegers\Agent\Facades\Agent',
		'PayPal' => 'Srmklive\PayPal\Facades\PayPal',

		/** Debugbar */
//		'Debugbar'           => 'Barryvdh\Debugbar\Facade',

	],
];

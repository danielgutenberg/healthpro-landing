<?php
return array(
    'entities' => array(
        'tag' => '\App\Tag',
        'help-posts' => '\App\HelpPost',
        'menu-item'  => '\App\MenuItem',
    ),
);

<?php namespace App\Exceptions;

use App\Http\Controllers\Api\ApiController;
use Exceptions\HttpReportedException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

class Handler extends ExceptionHandler
{

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		AuthorizationException::class,
		HttpException::class,
		ModelNotFoundException::class,
		ValidationException::class,
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Exception $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		if($this->isHttpResponseException($e)){
			return $e->getResponse();
		}
		if(preg_match('@^\/(ajax|api)\/@', $request->getRequestUri())){
			return ApiController::generateApiErrorResponse(get_class($e) . ' : ' . $e->getMessage());
		}
		if(!($e instanceof ValidationException) && config('app.debug') && !$this->isHttpException($e)){
			return $this->renderExceptionWithWhoops($e);
		}

		if (app()->environment('production', 'staging') ) {

			$status = ($this->isHttpException($e))
				? $e->getStatusCode()
				: 500;

			if (view()->exists("errors.{$status}")) {
				return response()->view("errors.{$status}", [], $status);
			} else {
				return response()->view("errors.http", ['status' => $status], $status);
			}
		}

		return parent::render($request, $e);
	}

	protected function isHttpResponseException(Exception $e)
	{
		return $e instanceof HttpResponseException || $e instanceof HttpReportedException;
	}

	/**
	 * Render an exception using Whoops.
	 *
	 * @param  \Exception $e
	 * @return \Illuminate\Http\Response
	 */
	protected function renderExceptionWithWhoops(Exception $e)
	{
		$whoops = new Run;
		$whoops->pushHandler(new PrettyPageHandler());
		return new Response(
			$whoops->handleException($e), 500
		);

	}
}

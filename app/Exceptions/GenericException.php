<?php namespace App\Exceptions;

use \Exception;

class GenericException extends Exception
{
	public $messages = [];

	public function addErrorMessage($field, $message)
	{
		$this->messages[$field][] = $message;
		return $this;
	}
}

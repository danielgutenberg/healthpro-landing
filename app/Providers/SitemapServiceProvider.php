<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;

/**
 * To subscribe a new model in sitemap, just add a new row in $listeners
 * in format  'sitemap_myname' => 'ModelClassName'
 * and implement two functions in ModelClassName
 *
 * Add self URL 'sitemap_myname.xml' to sitemap.xml
 * function handleSitemapIndex() {
 * 		Sitemap::addSitemap(route('sitemap',['myname']), self::max('updated_at') );
 * }
 *
 * Add all URL to sitemap_myname.xml
 * function handleSitemapPage(){
 * 		foreach(self::all() as $model){
 * 			Sitemap::addTag(route('model_root',[$model->id]), $model->updated_at );
 * 		}
 * }
 */
class SitemapServiceProvider extends ServiceProvider
{
	protected $defer = false;

	protected $listeners = [
		'sitemap_pages' => 'WL\Modules\Page\Models\Page',
		'sitemap_helps' => 'WL\Modules\HelpPost\Models\HelpPost',
		'sitemap_blogs' => 'WL\Modules\Blog\BlogPost',
	];

	/**
	 * Perform post-registration booting of services.
	 *
	 * @param  Dispatcher  $events
	 * @return void
	 */
	public function boot(Dispatcher $events)
	{
		foreach ($this->listeners as $eventName => $class) {
			$events->listen('sitemap_index', $class."@handleSitemapIndex");
			$events->listen($eventName, $class."@handleSitemapPage");
		}
	}

	/**
	 * Register bindings in the container.
	 *
	 * @return void
	 */
	public function register()
	{
	}

}

<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use WL\Modules\Page\Populators\FormerPagePopulator;
use WL\Modules\Page\Populators\PagePopulator;

use WL\Modules\Role\Populators\FormerRolePopulator;
use WL\Modules\Role\Populators\RolePopulator;

use WL\Modules\Education\Populators\EducationPopulator;
use WL\Modules\Education\Populators\FormerEducationPopulator;

use WL\Modules\Certification\Populators\CertificationPopulator;
use WL\Modules\Certification\Populators\FormerCertificationPopulator;

class PopulatorsServiceProvider extends ServiceProvider
{
	protected $repositories = [

		PagePopulator::class 			=> FormerPagePopulator::class,
		RolePopulator::class			=> FormerRolePopulator::class,
		EducationPopulator::class		=> FormerEducationPopulator::class,
		CertificationPopulator::class	=> FormerCertificationPopulator::class,

	];

	public function register()
	{
		foreach ($this->repositories as $interface => $class) {
			$this->app->bind($interface, $class);
		}
	}
}

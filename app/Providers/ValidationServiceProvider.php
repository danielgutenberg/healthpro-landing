<?php namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use WL\Modules\Provider\Validators\ProviderSessionValidator;

class ValidationServiceProvider extends ServiceProvider {

	protected $extensions = [
		'passwordchecker' 	=> 'WL\Validation\PasswordCheckerVerifier@validate',
		'metafieldexists' 	=> 'WL\Validation\MetaFieldsVerifier@validate',
		'ccnumber' 			=> 'WL\Validation\CreditCardNumberVerifier@validate',
		'ccbrand' 			=> 'WL\Validation\CreditCardBrandVerifier@validate',
		'embedvideo' 		=> 'WL\Validation\EmbedVideoUrlChecker@validate',
		'phone' 	     	=> 'WL\Modules\Phone\PhoneValidator@validate',
	];

	public function boot() {
		array_walk($this->extensions, function($extension, $alias) {
			$this->app['validator']->extend($alias, $extension);
		});

		#@todo here we have 2 options 1. To register global custom validator 2. Or register you custom validator in your commands because somewhere can be registered
			#@todo another validator and rewrite your.
		Validator::resolver(function ($translator, $data, $rules, $messages) {
			return new ProviderSessionValidator($translator, $data, $rules, $messages);
		});
	}

	public function register() {}
}

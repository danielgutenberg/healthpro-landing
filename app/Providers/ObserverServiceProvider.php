<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use WL\Modules\Page\Models\Page;
use WL\Modules\Profile\Models\ClientProfile;
use WL\Modules\Certification\Models\Certification;
use WL\Modules\Ticket\Models\Ticket;
use WL\Modules\Ticket\Models\TicketConversation;

use WL\Modules\Page\Observers\PageObserver;
use WL\Modules\Certification\Observers\CertificationObserver;
use WL\Modules\Ticket\Observers\TicketObserver;
use WL\Modules\Ticket\Observers\TicketConversationObserver;


class ObserverServiceProvider extends ServiceProvider
{
	/**
	 * @var array
	 */
	protected $observers = [
		Page::class                 => PageObserver::class,
		Certification::class        => CertificationObserver::class,
		Ticket::class               => TicketObserver::class,
		TicketConversation::class   => TicketConversationObserver::class,
	];

	/**
	 * Register observers
	 *
	 * @return void
	 */
	public function boot()
	{
		foreach ($this->observers as $model => $observer) {
			$model::observe( new $observer() );
		}
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{}
}

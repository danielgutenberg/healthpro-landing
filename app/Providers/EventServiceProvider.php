<?php namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use WL\Events\Listeners\BasicEmailHealthproToClientDispatcher;
use WL\Events\Listeners\BasicNotificationDispatcher;
use WL\Events\Listeners\BasicSmsDispatcher;
use WL\Events\Listeners\OAuthEventListener;
use WL\Events\Listeners\OrderEventListener;
use WL\Events\Listeners\ProfileCalendarEventListener;
use WL\Events\Listeners\ProfileNotesEventListener;
use WL\Events\Listeners\ProviderListener;
use WL\Events\Listeners\ProviderMembershipListener;
use WL\Events\Listeners\ProfileRequirementChangeListener;
use WL\Events\Listeners\SalesForceEventListener;
use WL\Events\Services\EventHelperService;
use WL\Events\Services\EventHelperServiceInterface;
use WL\Modules\Appointment\Events\AppointmentEventListener;
use WL\Modules\CustomerServiceRequests\Events\CustomerServiceRequestEventListener;
use WL\Modules\Payment\Events\CreditChangedEvent;
use WL\Modules\Payment\Events\PaymentCardExpiredEvent;
use WL\Modules\Payment\Events\PaymentCardExpiringEvent;
use WL\Modules\Payment\Events\PaymentMethodAdded;
use WL\Modules\Payment\Events\PaymentMethodRemoved;
use WL\Modules\Payment\Events\PaymentRefundEvent;
use WL\Modules\PricePackage\Events\PricePackageEventListener;
use WL\Modules\Profile\Events\ProfileDetailsChangeEvent;
use WL\Modules\Profile\Events\ProfileMetaChangeEvent;
use WL\Modules\ProfileCalendar\Events\RemoteCalendarDisabledEvent;
use WL\Modules\ProfileNotes\Events\ReminderTriggeredEvent;
use WL\Modules\Provider\Events\AcceptPaymentMethodUpdateEvent;
use WL\Modules\Provider\Events\IsSearchableChangeEvent;
use WL\Modules\Provider\Events\LocationAdded;
use WL\Modules\Provider\Events\LocationRemoved;
use WL\Modules\Provider\Events\ProgressUpdate;
use WL\Modules\Provider\Events\ProviderCreateEvent;
use WL\Modules\Provider\Events\ProviderPlanCancelledEvent;
use WL\Modules\Provider\Events\ServiceAdded;
use WL\Modules\Provider\Events\SessionAdded;
use WL\Modules\Provider\Events\SocialAccountChangeEvent;
use WL\Modules\Review\Events\ReviewFirstCommentEvent;
use WL\Modules\Location\Events\LocationEventsListener;
use WL\Modules\Payment\Events\PaymentEventListener;
use WL\Events\Listeners\ScheduleEventsListener;

class EventServiceProvider extends ServiceProvider
{

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'event.name' => [
			'EventListener',
		],
		'phone.verifying' => ['\WL\Events\Listeners\PhoneEventListener@verifying'],
		'group.invite' => ['\WL\Events\Listeners\GroupListener@invite'],

		'WL\Modules\Classes\Events\ClassNotifyProvider' => ['\WL\Events\Listeners\ClassNotifyProviderSender'],

		'WL\Modules\Client\Events\ClientAskToReviewProvider' => ['\WL\Events\Listeners\ClientAskToReviewProviderSender'],

		'WL\Modules\Provider\Events\SessionAdded' => [ProfileRequirementChangeListener::class],
		'WL\Modules\Provider\Events\SessionRemoved' => [ProfileRequirementChangeListener::class],
		'WL\Modules\Profile\Events\AvatarAddedEvent' => [ProfileRequirementChangeListener::class],
		'WL\Modules\Profile\Events\AvatarRemovedEvent' => [ProfileRequirementChangeListener::class],
		PaymentMethodAdded::class => [ProfileRequirementChangeListener::class],
		PaymentMethodRemoved::class => [ProfileRequirementChangeListener::class],
		'WL\Modules\Profile\Events\TermsAccepted' => [ProfileRequirementChangeListener::class],
        ServiceAdded::class => [
            SalesForceEventListener::class . '@queue',
			ProfileRequirementChangeListener::class
		],
		'WL\Modules\User\Events\ActivationSentEvent' => [ProfileRequirementChangeListener::class],
		'WL\Modules\User\Events\ActivationCompletedEvent' => [ProfileRequirementChangeListener::class],
		'WL\Modules\HasOffers\Events\RegisteredEvent' => ['\WL\Events\Listeners\UpdateHasOffers'],
		'WL\Modules\HasOffers\Events\IsSearchableEvent' => ['\WL\Events\Listeners\UpdateHasOffers'],
		'WL\Modules\User\Events\BecameSearchableEvent' => ['\WL\Events\Listeners\UserListener@searchable'],
		'WL\Modules\HasOffers\Events\ChargedEvent' => ['\WL\Events\Listeners\UpdateHasOffers'],

        IsSearchableChangeEvent::class => [SalesForceEventListener::class],
        LocationAdded::class => [SalesForceEventListener::class . '@queue'],
        LocationRemoved::class => [SalesForceEventListener::class . '@queue'],
        SocialAccountChangeEvent::class => [SalesForceEventListener::class . '@queue'],
        ProviderCreateEvent::class => [
            SalesForceEventListener::class . '@queue',
            SalesForceEventListener::class . '@getRemoteId',
        ],
        ProgressUpdate::class => [SalesForceEventListener::class . '@queue'],
        ProfileDetailsChangeEvent::class => [SalesForceEventListener::class . '@queue'],
        ProfileMetaChangeEvent::class => [
			SalesForceEventListener::class . '@queue',
			ProfileRequirementChangeListener::class
		],
        CreditChangedEvent::class => [SalesForceEventListener::class . '@queue'],
		'WL\Modules\Profile\Events\ProfileEmailChangeEvent' => [SalesForceEventListener::class . '@changeEmail'],
		ReviewFirstCommentEvent::class => [
			SalesForceEventListener::class . '@review',
			'\WL\Events\Listeners\ReviewListener@firstComment',
		],

		RemoteCalendarDisabledEvent::class => [ProfileCalendarEventListener::class . '@calendarDisabled'],
		ReminderTriggeredEvent::class => [ProfileNotesEventListener::class . '@reminderTriggered'],

        PaymentRefundEvent::class => [OrderEventListener::class . '@paymentRefund'],

        PaymentCardExpiringEvent::class => [PaymentEventListener::class . '@paymentCardExpiring'],
        PaymentCardExpiredEvent::class => [PaymentEventListener::class . '@paymentCardExpired'],
        ProviderPlanCancelledEvent::class => [ProviderMembershipListener::class . '@cancelled'],
        AcceptPaymentMethodUpdateEvent::class => [ProviderListener::class . '@acceptPayments'],

		'event-mail-provider-client' => [BasicEmailHealthproToClientDispatcher::class],
		'event-sms-basic' => [BasicSmsDispatcher::class],
		'event-notification' => [BasicNotificationDispatcher::class],

		'schedule-events' => [ScheduleEventsListener::class],
		'location-events' => [LocationEventsListener::class],
		'payment-events' => [PaymentEventListener::class],
		'appointment-events' => [AppointmentEventListener::class],
		'customer-service-request-events' => [CustomerServiceRequestEventListener::class],
		'price-package-events' => [PricePackageEventListener::class],
		'oauth-events' => [OAuthEventListener::class]
	];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);

		$this->app->singleton(EventHelperServiceInterface::class, function () {
			return new EventHelperService();
		});

	}

}

<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Event;
use WL\Modules\Referral\ReferralListener;

class EventSubscriberServiceProvider extends ServiceProvider {

	protected $subscribers = [
		ReferralListener::class
	];

	/**
	 * Register Event Subscribers ..
	 */
	public function boot() {
		array_walk($this->subscribers, function($subscriber) {
			Event::subscribe($subscriber);
		});
	}

	public function register() {

	}
}

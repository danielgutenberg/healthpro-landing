<?php namespace App\Providers;

use View;
use Illuminate\Support\ServiceProvider;
use WL\Modules\User\Composers\UserComposer;
use WL\Modules\Profile\Composers\ProfileComposer;
use WL\Payment\Composers\PaypalModeComposer;
use WL\Security\Composers\AuthUtilComposer;

class ViewComposerServiceProvider extends ServiceProvider {

	/**
	 * Register bindings in the container.
	 *
	 * @return void
	 */
	public function boot() {
		View::composers([
			ProfileComposer::class => ['dashboard.*', 'comment.*', 'menu.*', 'landing.index.*'],
			UserComposer::class    => ['dashboard.*', 'landing.index.*'],
			AuthUtilComposer::class => ['*']
		]);
	}

	/**
	 * Register
	 *
	 * @return void
	 */
	public function register() {
		//
	}

}

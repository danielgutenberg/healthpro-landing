<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use WL\Repositories\Country\CountryRepository;
use WL\Repositories\Country\DbCountryRepository;

use WL\Modules\Page\Repositories\PageRepository;
use WL\Modules\Page\Repositories\DbPageRepository;

use WL\Modules\Certification\Repositories\CertificationRepository;
use WL\Modules\Certification\Repositories\DbCertificationRepository;

use WL\Modules\Notification\Repositories\DbNotificationRepository;
use WL\Modules\Notification\Repositories\NotificationRepositoryInterface;

use WL\Modules\Education\Repository\EducationRepository;
use WL\Modules\Education\Repository\DbEducationRepository;

use WL\Modules\Role\Repositories\RoleRepository;
use WL\Modules\Role\Repositories\DbRoleRepository;

use WL\Security\Repositories\ApiKeyRepository;
use WL\Security\Repositories\DbApiKeyRepository;

use WL\Schedule\DbSchedulePublicHolidayRepository;
use WL\Schedule\DbScheduleRegistrationClientRepository;
use WL\Schedule\ScheduleAvailabilityRepository;
use WL\Schedule\DbScheduleRegistrationSlotRepository;

use WL\Schedule\SchedulePublicHolidayRepository;
use WL\Schedule\ScheduleRegistrationSlotRepository;
use WL\Schedule\DbScheduleAvailabilityRepository;

use WL\Schedule\ScheduleRegistrationClientRepository;

class RepositoriesServiceProvider extends ServiceProvider
{
	protected $repositories = [
		PageRepository::class		=> DbPageRepository::class,
		CertificationRepository::class	=> DbCertificationRepository::class,
		EducationRepository::class	=> DbEducationRepository::class,
		RoleRepository::class	=> DbRoleRepository::class,
		CountryRepository::class	=> DbCountryRepository::class,
		ApiKeyRepository::class         => DbApiKeyRepository::class,
		ScheduleAvailabilityRepository::class  => DbScheduleAvailabilityRepository::class,
		ScheduleRegistrationSlotRepository::class   => DbScheduleRegistrationSlotRepository::class,
		ScheduleRegistrationClientRepository::class   => DbScheduleRegistrationClientRepository::class,
		SchedulePublicHolidayRepository::class   => DbSchedulePublicHolidayRepository::class,
		NotificationRepositoryInterface::class       => DbNotificationRepository::class,
	];

	public function register()
	{
		foreach ($this->repositories as $interface => $class) {
			$this->app->bind($interface, $class);
		}
	}
}


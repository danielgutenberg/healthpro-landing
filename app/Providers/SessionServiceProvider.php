<?php namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class SessionServiceProvider extends ServiceProvider
{

	public function register() {
		$this->app->singleton('Illuminate\Cookie\Middleware\EncryptCookies');
	}
}

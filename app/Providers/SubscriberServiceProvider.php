<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use WL\Contracts\Subscriber;
use WL\Subscriber\MailchimpSubscriber;
use WL\Subscriber\LogSubscriber;

class SubscriberServiceProvider extends ServiceProvider
{
    protected $defer = false;

	public function register()
	{
		// we just need to log the newsletter on test
		$this->app->bind(
			'WL\Contracts\Subscriber',
			'testing' === $this->app->environment()
				? LogSubscriber::class
				: MailchimpSubscriber::class
		);
	}
}

<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class DevEnvironmentServiceProvider extends ServiceProvider
{

	/**
	 * Bootstrap any application services.
	 *
	 * @param  \Illuminate\Bus\Dispatcher $dispatcher
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		if ($this->app->environment() == 'production') {
			//Intentionally empty.
		} else {
			$this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
		}
	}

}

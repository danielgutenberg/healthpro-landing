<?php namespace App\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use WL\Modules\Tag\TagRepository;
use WL\Modules\Tag\TagRepositoryInterface;
use WL\Modules\Tag\TagService;
use WL\Modules\Tag\TagServiceInterface;
use WL\Modules\Taxonomy\TaxonomyRepository;
use WL\Modules\Taxonomy\TaxonomyRepositoryInterface;
use WL\Modules\Taxonomy\TaxonomyService;
use WL\Modules\Taxonomy\TaxonomyServiceInterface;

class TagsTaxonomyServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->instance('TagService', new TagService(new TagRepository(),new TaxonomyService(new TaxonomyRepository())));
		$this->app->booting(function () {
			AliasLoader::getInstance()->alias('TagService', 'WL\Modules\Tag\Facade\TagService');
		});

		$this->app->instance('TaxonomyService', new TaxonomyService(new TaxonomyRepository()));
		$this->app->booting(function () {
			AliasLoader::getInstance()->alias('TaxonomyService', 'WL\Modules\Taxonomy\Facade\TaxonomyService');
		});

		$this->app->bind(TagServiceInterface::class, TagService::class);
		$this->app->bind(TaxonomyServiceInterface::class, TaxonomyService::class);

		$this->app->bind(TagRepositoryInterface::class, TagRepository::class);
		$this->app->bind(TaxonomyRepositoryInterface::class, TaxonomyRepository::class);

	}

}

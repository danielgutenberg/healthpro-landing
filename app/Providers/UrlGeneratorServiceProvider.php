<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\UrlGenerator as UrlGeneratorContract;
use WL\Routing\UrlGenerator;

class UrlGeneratorServiceProvider extends ServiceProvider
{
	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('url', function($app) {
			return new UrlGenerator($app['router']->getRoutes(), request());
		});
	}
}

<?php namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use WL\Modules\Appointment\Models\ProviderAppointment;
use WL\Modules\GiftCertificates\Models\GiftCertificate;
use WL\Modules\Order\Adapters\EntityName;
use WL\Modules\Payment\Adapters\EntityType;
use WL\Modules\Payment\Models\PaymentBankAccount;
use WL\Modules\Payment\Models\PaymentCard;
use WL\Modules\Payment\Models\PaymentPaypalAccount;
use WL\Modules\PricePackage\Models\PricePackage;
use WL\Modules\Provider\Models\BillingCycle;
use WL\Modules\Provider\Models\ProviderPlan;
use WL\Modules\Provider\Models\ProviderPlanAction;
use WL\Modules\Provider\Models\ProviderServiceSession;

class RelationServiceProvider extends ServiceProvider
{
	protected $morphMap = [
		EntityName::APPOINTMENT => ProviderAppointment::class,
		EntityName::PACKAGE => PricePackage::class,
		EntityName::CERTIFICATE => GiftCertificate::class,
		ProviderServiceSession::PACKAGE_ENTITY_TYPE => ProviderServiceSession::class,
        EntityType::BANK_ACCOUNT => PaymentBankAccount::class,
        EntityType::CARD => PaymentCard::class,
		EntityType::PAYPAL => PaymentPaypalAccount::class,
        EntityName::MEMBERSHIP => ProviderPlan::class,
        EntityName::MEMBERSHIP_UPGRADE => ProviderPlanAction::class,
        EntityName::BILLING_CYCLE_FEES => BillingCycle::class,
	];

	public function register()
	{
		Relation::morphMap($this->morphMap);
	}
}


<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Event;
use Sentinel;

class UserServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		Sentinel::removeCheckpoint('activation');

		// handle sentinel events
		Event::listen('sentinel.registered',  'WL\Events\Listeners\UserListener@register');
		Event::listen('sentinel.authenticated', 'WL\Events\Listeners\UserListener@login');
		Event::listen('sentinel.activated',  'WL\Events\Listeners\UserListener@activate');

		Event::listen('user.logout',  'WL\Events\Listeners\UserListener@logout');

		// Sentinel doesn't provide the event firing for reminders
		// so we use the standard eloquent events.
		Event::listen('eloquent.created: WL\Modules\User\Models\Reminder',    'WL\Events\Listeners\UserListener@forgot');

		Event::listen('eloquent.updated: WL\Modules\User\Models\Reminder',    'WL\Events\Listeners\UserListener@reset');
	}
}

<?php
##################################
# Admin Comments Management      #
##################################
Route::get('comments/{resource}/{resourceId}', array(
	'as'    => 'admin.comments.index',
	'uses'  => 'Admin\CommentsController@index',
));
Route::get('comments/{resource}/{resourceId}/{id}/edit', array(
	'as'    => 'admin.comments.edit',
	'uses'  => 'Admin\CommentsController@edit',
));
Route::put('comments/{resource}/{resourceId}/{id}', array(
	'as'    => 'admin.comments.update',
	'uses'  => 'Admin\CommentsController@update',
));
Route::delete('comments/{resource}/{resourceId}/{id}', array(
	'as'    => 'admin.comments.destroy',
	'uses'  => 'Admin\CommentsController@destroy',
));


##################################
# Admin Attachments Management   #
##################################
Route::get('attachments/{id}/delete', array(
	'as'    => 'admin.attachments.delete',
	'uses'  => 'Admin\AttachmentsController@delete',
));

##################################
# Admin Options Management       #
##################################
Route::get('options/{group}', array(
	'as'    => 'options-index',
	'uses'  => 'Admin\OptionsController@index',
));
Route::post('options/{group}', array(
	'as'    => 'options-save',
	'uses'  => 'Admin\OptionsController@save',
));

Route::get('options/reset/{group}', array(
	'as'    => 'options-reset',
	'uses'  => 'Admin\OptionsController@reset',
));

##################################
# Admin Lang/Locale Management   #
##################################
Route::get('language', array(
	'as'    => 'admin-language',
	'uses'  => 'Admin\LanguageController@getIndex',
));
Route::get('language/{current_locale}', array(
	'as'    => 'admin-language-locale',
	'uses'  => 'Admin\LanguageController@getView',
));
Route::post('language/{current_locale}', array(
	'as'    => 'admin-language-locale-post',
	'uses'  => 'Admin\LanguageController@postUpload',
));
Route::get('language/download/{current_locale}/{file}', array(
	'as'    => 'admin-language-locale-download',
	'uses'  => 'Admin\LanguageController@getDownload',
));
Route::get('language/download/pot', array(
	'as'    => 'admin-language-download-pot',
	'uses'  => 'Admin\LanguageController@getDownloadPot',
));
Route::get('language/delete/{current_locale}', array(
	'as'    => 'admin-language-locale-delete',
	'uses'  => 'Admin\LanguageController@getDelete',
));

##################################
# Admin Translaiton Management   #
##################################
Route::get('translations', array(
	'as'    => 'admin-translations',
	'uses'  => 'Admin\TranslationsController@getIndex',
));

# Translations
Route::get('translations/edit/{id}/{lang}', array(
	'as'    => 'admin-translations-edit',
	'uses'  => 'Admin\TranslationsController@getEdit',
));
Route::post('translations/edit/{id}/{lang}', array(
	'as'    => 'admin-translations-edit-post',
	'uses'  => 'Admin\TranslationsController@postEdit',
));
Route::get('translations/add/{id}/{lang}', array(
	'as'    => 'admin-translations-add',
	'uses'  => 'Admin\TranslationsController@getAdd',
));
Route::post('translations/add/{id}/{lang}', array(
	'as'    => 'admin-translations-add-post',
	'uses'  => 'Admin\TranslationsController@postAdd',
));
Route::post('translations/delete/{id}/{lang}', array(
	'as'    => 'admin-translations-delete',
	'uses'  => 'Admin\TranslationsController@postDelete',
));
Route::get('translations/groups/destroy/{group}/{lang}/{id}', array(
	'as'   => 'admin-translations-value-destroy',
	'uses' => 'Admin\TranslationsController@getDestroy',
));



# Translations Groups
Route::get('translations/groups/create', array(
	'as'   => 'admin-translations-groups-create',
	'uses' => 'Admin\TranslationsGroupsController@getCreate',
));
Route::post('translations/groups/create', array(
	'as'   => 'admin-translations-groups-create-post',
	'uses' => 'Admin\TranslationsGroupsController@postCreate',
));
Route::get('translations/groups/edit/{id}', array(
	'as'    => 'admin-translations-groups-edit',
	'uses'  => 'Admin\TranslationsGroupsController@getEdit',
));
Route::post('translations/groups/edit/{id}', array(
	'as'    => 'admin-translations-groups-edit-post',
	'uses'  => 'Admin\TranslationsGroupsController@postEdit',
));
Route::post('translations/groups/delete', array(
	'as'    => 'admin-translations-groups-delete',
	'uses'  => 'Admin\TranslationsGroupsController@postDelete',
));
Route::get('translations/groups/destroy/{id}', array(
	'as'   => 'admin-translations-groups-destroy',
	'uses' => 'Admin\TranslationsGroupsController@getDestroy',
));




# Roles Management
Route::resource('roles', 'Admin\RolesController', [
	'except' => [
		'show'
	]
]);



# Notification Management
Route::resource('notifications', 'Admin\NotificationsController', [
	'except' => [
		'show'
	]
]);


# Profiles Management
Route::resource('profiles', 'Admin\ProfilesController', [
	'except' => [
		'show'
	]
]);


Route::delete('delete', [
	'as'	=> 'admin.profiles.delete',
	'uses'	=> 'Admin\ProfilesController@delete',
]);



/** Profile Locations ... */
Route::match(['get'],'profiles/offices/view/{profileId}/{id?}', ['as' => 'profiles_view_attached_location', 'uses' => 'Admin\ProfilesController@profileViewAttachedLocation']);
Route::match(['get', 'post'],'profiles/attach/{profileId}', ['as' => 'profiles_attach_location', 'uses' => 'Admin\ProfilesController@profileAttachLocation']);
Route::match(['get', 'post'],'profiles/detach/{profileId}/{location_id}', ['as' => 'profiles_detach_location', 'uses' => 'Admin\ProfilesController@profileDetachLocation']);

Route::resource('profiles/provider', 'Admin\ProviderProfilesController', [
	'except' => [
		'show'
	]
]);

Route::resource('profiles/client', 'Admin\ClientProfilesController', [
	'except' => [
		'show'
	]
]);

Route::resource('profiles/staff', 'Admin\StaffProfilesController', [
	'except' => [
		'show'
	]
]);

Route::group(array('prefix' => 'profiles/provider', 'namespace' => 'Admin'), function() {

	Route::get('/{id}/restore', [
		'as' => 'admin-provider-profile-restore',
		'uses' => 'ProviderProfilesController@restore'
	]);

	/** Profile Locations ... */
	Route::match(['get'],'offices/view/{profileId}/{id?}', [
		'as' => 'profiles_view_attached_location',
		'uses' => 'ProviderProfilesController@profileViewAttachedLocation'
	]);

	Route::match(['get', 'post'],'attach/{profileId}', [
		'as' => 'profiles_attach_location',
		'uses' => 'ProviderProfilesController@profileAttachLocation'
	]);

	Route::match(['get', 'post'],'detach/{profileId}/{location_id}', [
		'as' => 'profiles_detach_location',
		'uses' => 'ProviderProfilesController@profileDetachLocation'
	]);


	/** Profile Educations */
	Route::match(['get'],'educations/view/{profileId}/{id?}', [
		'as' => 'profiles_view_attached_education',
		'uses' => 'ProviderProfilesController@profileViewAttachedEducation'
	]);

	Route::match(['get', 'post'],'attach/education/{profileId}', [
		'as' => 'profiles_attach_education',
		'uses' => 'ProviderProfilesController@profileAttachEducation'
	]);

	Route::match(['get', 'post'],'detach/education/{profileId}/{education_id}', [
		'as' => 'profiles_detach_education',
		'uses' => 'ProviderProfilesController@profileDetachEducation'
	]);


	/** Profile tags ... */
	Route::match(['get', 'post'],'tags/{profileId}/{taxonomy_id}', [
		'as' => 'profiles_tags',
		'uses' => 'ProviderProfilesController@profileTags'
	]);
});






# Locations Management
Route::resource('locations', 'Admin\LocationsController', [
	'except' => [
		'show'
	]
]);

Route::get('orders/item/{orderItemId}/refund', [
	'as'	=> 'admin.orders.refund',
	'uses'	=> 'Admin\OrderController@refundOrderItem',
]);


Route::delete('locations/delete', [
	'as'	=> 'admin.locations.delete',
	'uses'	=> 'Admin\LocationsController@delete',
]);

# Countries Management
Route::resource('countries', 'Admin\CountriesController', [
	'except' => [
		'show'
	]
]);

# Coupons Management
Route::resource('coupons', 'Admin\CouponsController', [
	'except' => [
		'show'
	]
]);


Route::delete('coupons/delete', [
	'as'	=> 'admin.coupons.delete',
	'uses'	=> 'Admin\CouponsController@delete',
]);

# Credits Management
Route::resource('credits', 'Admin\CreditsController', [
	'except' => [
		'show'
	]
]);






# Certifications Management
Route::resource('certifications', 'Admin\CertificationsController', [
	'except' => [
		'show'
	]
]);


Route::delete('certifications/delete', [
	'as'	=> 'admin.certifications.delete',
	'uses'	=> 'Admin\CertificationsController@delete',
]);







# Educations Management
Route::resource('educations', 'Admin\EducationsController', [
	'except' => [
		'show'
	]
]);

Route::delete('educations/delete', [
	'as'	=> 'admin.educations.delete',
	'uses'	=> 'Admin\EducationsController@delete',
]);





# Groups Management
Route::resource('groups', 'Admin\GroupsController', [
	'except' => [
		'show'
	]
]);

Route::delete('groups/delete', [
	'as'	=> 'admin.groups.delete',
	'uses'	=> 'Admin\GroupsController@delete',
]);

Route::post('groups/content/attach/{groups}', [
	'as'   => 'admin.groups.attach.content',
	'uses' => 'Admin\GroupsController@saveContent',
])->where('groups', '[0-9]+');

Route::post('groups/content/detach/{groups}', [
	'as'   => 'admin.groups.detach.content',
	'uses' => 'Admin\GroupsController@deleteContent',
])->where('groups', '[0-9]+');



Route::post('groups/user/{type}/{groups}', [
	'as'   => 'admin.groups.attach.user',
	'uses' => 'Admin\GroupsController@manageAttachedUsers',
])->where('groups', '[0-9]+');

Route::match(['GET', 'POST'],'groups/users/{groups}/{email?}', [
	'as'   => 'admin.groups.users',
	'uses' => 'Admin\GroupsController@getUsersByEmail'
]);



Route::match(['get', 'post'],'users/{users}/permissions/save',['as' => 'admin.users.permissions.save','uses' => 'Admin\UserController@permissionsSave']);


Route::post('taxonomy/{taxonomy}/tags',['as' => 'admin.taxonomy.tags.store','uses' => 'Admin\TaxonomyController@tagsStore']);
Route::get('taxonomy/{taxonomy}/tags/edit',['as' => 'admin.taxonomy.tags.edit','uses' => 'Admin\TaxonomyController@tagsEdit']);
Route::resource('taxonomy','Admin\TaxonomyController');

Route::post('tags/{tag}/tags',['as' => 'admin.tags.tags.store','uses' => 'Admin\TagsController@tagsStore']);
Route::get('tags/{tag}/tags/edit',['as' => 'admin.tags.tags.edit','uses' => 'Admin\TagsController@tagsEdit']);
Route::put('tags/{tag}/position',['as' => 'admin.tag.position.set','uses' => 'Admin\TagsController@setTagPosition']);
Route::post('tags/{tag}/aliases',['as' => 'admin.tags.aliases.store','uses' => 'Admin\TagsController@aliasesStore']);
Route::get('tags/aliases/{alias}/make-as-primary-tag',['as' => 'admin.tags.aliases.make-as-primary-tag','uses' => 'Admin\TagsController@makeAliasAsPrimaryTag']);
Route::get('tags/{tag}/aliases/edit',['as' => 'admin.tags.aliases.edit','uses' => 'Admin\TagsController@aliasesEdit']);

# User Management
Route::match(['get', 'post'],'users/{users}/permissions/save',['as' => 'admin.users.permissions.save','uses' => 'Admin\UserController@permissionsSave']);
Route::resource('users', 'Admin\UserController');

Route::get('users/{user}/non-managers', ['as' => 'admin.user.non.managers', 'uses' => 'Admin\UserController@nonManagers']);
Route::get('users/{user}/managers/show', ['as' => 'admin.user.managers.show', 'uses' => 'Admin\UserController@managersShow']);
Route::post('users/{user}/managers/sync', ['as' => 'admin.user.managers.sync', 'uses' => 'Admin\UserController@syncManagers']);
Route::get('users/{user}/invitations/{type}/{provider}', ['as' => 'admin.user.invitations.manage', 'uses' => 'Admin\UserController@invitations']);

# Pages Management
Route::delete('pages/delete', [
	'as'	=> 'admin.pages.delete',
	'uses'	=> 'Admin\PagesController@delete',
]);
Route::get('pages/translate/{id}/{lang}', array(
	'as'   => 'admin.pages.translate',
	'uses' => 'Admin\PagesController@translate',
));
Route::resource('pages', 'Admin\PagesController');

# Admin Dashboard
Route::get('/', [
	'as'	=> 'admin-dashboard',
	'uses' 	=> 'Admin\DashboardController@getIndex',
]);

Route::resource('tickets','Admin\TicketsController');
Route::post('tickets/{ticket}/conversations',['as' => 'admin.tickets.conversations.store','uses' => 'Admin\TicketsController@conversationsStore']);

Route::resource('help','Admin\HelpController');

Route::post('sort', 'Admin\SortableController@sort');


Route::get('salesforce', [
	'as' => 'admin.salesforce.queue',
	'uses' => 'Admin\SalesforceController@queue'
]);

Route::get('salesforce/execqueue', [
	'as'	=> 'admin.salesforce.exec_queue',
	'uses'	=> 'Admin\SalesforceController@execQueue',
]);

Route::get('salesforce/queueall', [
	'as'	=> 'admin.salesforce.queue_all',
	'uses'	=> 'Admin\SalesforceController@queueAll',
]);

////////////////////// Admin SEO settings /////////////////////////
Route::get('/seo/options/{routeName?}', [
	'as' => 'admin-seo-settings',
	'uses' => 'Admin\SeoSettingsController@getIndex'
]);

Route::post('/seo/options/{routeName}', [
	'as' => 'admin-seo-settings-save',
	'uses' => 'Admin\SeoSettingsController@save'
]);


# Reviews Management
Route::delete('reviews-comments/delete', [
	'as'	=> 'admin.reviews-comments.delete',
	'uses'	=> 'Admin\ReviewsCommentsController@delete',
]);
Route::put('reviews-comments/update-status', [
	'as'	=> 'admin.reviews-comments.update.status',
	'uses'	=> 'Admin\ReviewsCommentsController@updateStatus',
]);
Route::resource('reviews-comments', 'Admin\ReviewsCommentsController', [
	'except' => ['show', 'store', 'create']
]);

# Fees Management
Route::resource('fees', 'Admin\PaymentFeesController', [
	'except' => [
		'show'
	]
]);

Route::get('logs', [
	'as' => 'admin-laravel-logs',
	'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index',
]);

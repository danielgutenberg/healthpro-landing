<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		'App\Http\Middleware\ReplaceTestVars',
		'App\Http\Middleware\FixForVagrantProtocolPortForwarding',
		'App\Http\Middleware\VerifyCsrfToken',
		'App\Http\Middleware\SaveAffiliateData',
		'App\Http\Middleware\RegisterMiddleware',
		'App\Http\Middleware\LogoutUserWithNoProfile'
	];

	/**
	 * All of the application's route middleware keys.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'force.https'  		 => '\App\Http\Middleware\ForceHTTPSProtocol',
		'force.http'  		 => '\App\Http\Middleware\ForceHTTPProtocol',
		'auth'  		 	 => '\App\Http\Middleware\Authenticated',
		'guest' 		 	 => '\App\Http\Middleware\IsGuest',
		'admin' 			 => '\App\Http\Middleware\AdminMiddleware',
		'api.auth'		     => 'App\Http\Middleware\ApiAuthMiddleware',
		'member.redirector'  => '\WL\Security\Middlewares\UnauthorizedMemberRedirectorMiddleware',
		'not.ready'  		 => 'App\Http\Middleware\NotReadyBeforeMiddleware',
		'me.resolve'  		 => 'App\Http\Middleware\ResolveMeMiddleware'
	];
}

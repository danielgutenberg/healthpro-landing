<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;

class FixForVagrantProtocolPortForwarding {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$return = $next($request);
		if ($return instanceof RedirectResponse) {
			$content = $this->_repairLinks($return->getTargetUrl());
			$return->setTargetUrl($content);
		}else{
			$content = $this->_repairLinks($return->getContent());
			$return->setContent($content);
		}
		return $return;
	}

	private function _repairLinks($str)
	{
		$str = preg_replace("/(https:\/\/[\w\.]+):8000/", "$1:44300", $str);
		$str = preg_replace("/(http:\/\/[\w\.]+):44300/", "$1:8000", $str);
		return $str;
	}
}

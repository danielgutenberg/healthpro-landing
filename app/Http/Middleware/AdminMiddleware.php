<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\ResponseFactory;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class AdminMiddleware
{
	protected $response;

	public function __construct(ResponseFactory $response)
	{
		$this->response = $response;
	}

	public function handle($request, Closure $next)
	{
		if (Sentinel::guest()) {
			return $this->response->redirectGuest(route('auth-login'));
		}

		if (!Sentinel::inRole('administrator')) {
			return $this->response->redirectGuest(route('dashboard-home'));
		}

		return $next($request);
	}
}

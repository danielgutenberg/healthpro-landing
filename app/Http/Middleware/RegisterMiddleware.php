<?php namespace App\Http\Middleware;

use Closure;
use WL\Modules\Order\Models\Order;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Security\Facades\AuthorizationUtils;


class RegisterMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$profile = ProfileService::getCurrentProfile();
		$isPro = $profile && $profile->type == ModelProfile::PROVIDER_PROFILE_TYPE;

		$excludedRoute = $request->is(
			'ajax/*',
			'api/*',
			'activate/*',
			'auth/*',
			'terms',
			'dashboard/my-profile/*',
			'pricing',
			'dashboard/billing/info',
			'dashboard/register-as/client'
		);

		if ($excludedRoute || !$isPro || $profile->is_searchable) {
			return $next($request);
		}

		// if professional has no active plan then force him to billing page
		if (!ProviderMembershipService::getActivePlan($profile->id) && $request->is('dashboard/*')) {
			return redirect(url('dashboard/billing/info'));
		}

		## if user has not yet completed an order
		if (ProviderMembershipService::getProfilePlanHistory($profile->id)->isEmpty()) {
			$redirect = redirect(url('pricing'));
			if($socialProvider = $profile->owner()->socials->first()) {
				$redirect->with('socialSignup', $socialProvider->provider);
			}
			return $redirect;
		}

		if (!$request->is('/') && !$request->is('wizard/*')) {
			return redirect(url('/'));
		}
		return $next($request);
	}

}

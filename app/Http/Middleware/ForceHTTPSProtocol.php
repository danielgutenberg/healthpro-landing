<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

class ForceHTTPSProtocol {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (!$request->secure()  && isset($_ENV['FORCE_HTTPS']) && $_ENV['FORCE_HTTPS']) {
			return Redirect::secure($request->path());
		}
		return $next($request);
	}

}

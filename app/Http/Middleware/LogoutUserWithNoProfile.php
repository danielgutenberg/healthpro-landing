<?php namespace App\Http\Middleware;

use Closure;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Facades\AuthorizationUtils;
use Sentinel;


class LogoutUserWithNoProfile
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (AuthorizationUtils::isLoggedIn() && !ProfileService::getCurrentProfile()) {
			Sentinel::logout();
		}
		// log out users who need to be forced to log in
		$user = AuthorizationUtils::loggedInUser();
		if ($user && $user->force_login) {
			Sentinel::logout();
			ProfileService::invalidateCurrentProfileId();
			$user->force_login = 0;
			$user->save();
		}

		return $next($request);
	}

}

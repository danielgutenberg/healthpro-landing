<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Crypt;


class SaveAffiliateData
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ((int) $request->affiliateId && (int) $request->offerId) {
			$dataObject = [
				'affiliate_id' => (int) $request->affiliateId,
				'offer_id' => (int) $request->offerId
			];
			$hasOffersId = base64_encode(Crypt::encrypt($dataObject));
			session(['has_offers_id' => $hasOffersId]);
		}
		# hack for camillle leon
		if ($request->url() == route('profile-show', ['camille-leon'])) {
			$dataObject = [
				'affiliate_id' => 1020,
				'offer_id' => 12
			];
			$hasOffersId = base64_encode(Crypt::encrypt($dataObject));
			session(['has_offers_id' => $hasOffersId]);
		}
		if ($request->hp_code) {
			session(['coupon_code' => $request->hp_code]);
		}
		if ($request->is_blogger) {
			session(['is_blogger' => true]);
		}

		return $next($request);
	}

}

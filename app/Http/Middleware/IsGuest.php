<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\ResponseFactory;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class IsGuest
{

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	protected $response;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard $auth
	 * @param ResponseFactory $response
	 * @return \App\Http\Middleware\IsGuest
	 */
	public function __construct(Guard $auth, ResponseFactory $response)
	{
		$this->auth = $auth;

		$this->response = $response;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (Sentinel::check()) {
			return $this->response->redirectToIntended(route('dashboard-myprofile'));
		}

		return $next($request);
	}
}

<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\ResponseFactory;

class LandingMiddleware
{
	protected $response;

	public function __construct( ResponseFactory $response )
	{
		$this->response = $response;
	}

	public function handle($request, Closure $next)
	{

		return $next( $request );
	}
}

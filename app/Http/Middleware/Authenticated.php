<?php namespace App\Http\Middleware;

use Closure;
use WL\Security\Facades\AuthorizationUtils;


class Authenticated
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (!AuthorizationUtils::isLoggedIn()) {
			if ($request->ajax()) {
				return response('Unauthorized', 401);
			} else {
				session(['redirect_url' => $request->fullUrl()]);
				return redirect(route('auth-login'));
			}
		}

		return $next($request);
	}

}

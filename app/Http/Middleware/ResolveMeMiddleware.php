<?php namespace App\Http\Middleware;

use Closure;
use WL\Modules\Profile\Facades\ProfileService;
use Illuminate\Support\Facades\Input;

class ResolveMeMiddleware
{
	private $replaceableParams = ['profileId', 'providerId', 'clientId'];
	private $replaceableInputData = ['profile_id', 'provider_id', 'client_id'];
	private $placeholders = ['me'];

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$this->replaceParams($request);
		$this->replaceInputData($request);

		return $next($request);
	}

	private function replaceParams($request)
	{
		$route = $request->route();

		foreach ($this->replaceableParams as $param) {
			if ($route && $route->hasParameter($param) && in_array($route->getParameter($param), $this->placeholders, true)) {
				$route->setParameter($param, ProfileService::getCurrentProfileId());
			}
		}
	}

	private function replaceInputData($request)
	{
		$input = $request->all();

		foreach ($this->replaceableInputData as $param) {
			if ($request->has($param) && in_array($request->input($param), $this->placeholders, true)) {
				$input[$param] = ProfileService::getCurrentProfileId();
			}
		}


		// This doesn't affect inherited "Request" objects
		$request->replace($input);
	}

}

<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Session\TokenMismatchException;
use App\Http\Controllers\Api\ApiController;

class VerifyCsrfToken extends BaseVerifier
{

	protected $except = ['api/*', 'ajax/v1/stripe/webhook', 'push/*'];

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		foreach ($this->except as $route) {
			if( $request->is($route) )
				return $next($request);
		}
		if (strpos($request->getRequestUri(), '/ajax/') === 0) {
			try {
				return parent::handle($request, $next);
			} catch (TokenMismatchException $e) {
				return ApiController::generateApiErrorResponse(get_class($e) . ':' . $e->getMessage(), 419);
			}
		} else {
			return parent::handle($request, $next);
		}
	}
}

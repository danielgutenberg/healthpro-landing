<?php namespace App\Http\Middleware;

use App\Http\Controllers\Api\ApiController;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Closure;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response as ResponseCodes;
use Illuminate\Support\Facades\Response;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Facades\ApiKeyService;
use WL\Yaml\Facades\Yaml;

/**
 * Class ApiAuthMiddleware
 * Api authentication consists of main four parts
 * 1) Check existence of authentication headers (accessKey and expire)
 * 2) Try to find user and secret key
 * 3) Validate provided signature
 * 4) Try to login user
 * @package App\Http\Middleware
 */
class ApiAuthMiddleware
{
	/** Access key
	 * @var string
	 */
	private $accessKey;

	/** Signature
	 * @var string
	 */
	private $signature;

	/** Expiration date unix time
	 * @var integer
	 */
	private $expire;

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$errors = $this->authenticate($request);
		if ($errors) {
			return $errors;
		}
		return $next($request);
	}

	/**
	 * Authenticate request
	 *
	 * @param $request
	 * @return bool
	 */
	public function authenticate(Request $request)
	{
		try {
			// Extract and validate header
			if (!$this->extractAuthData($request)) {
				return $this->respondWithError("Authorization header isn't valid", ResponseCodes::HTTP_UNAUTHORIZED, 89);
			}

			// Guest access
			if ($this->validateGuestAccess()) {
				return false;
			}

			// Try find secret key and user by access key
			$userApiKey = ApiKeyService::getApiKey($this->accessKey);
			if (!$userApiKey) {
				return $this->respondWithError('Could not authenticate you', ResponseCodes::HTTP_UNAUTHORIZED, 135);
			}

			// Validate signature
			$isValid = $this->validateSignature($userApiKey->api_secret_key);
			if (!$isValid) {
				return $this->respondWithError('Invalid or expired token', ResponseCodes::HTTP_UNAUTHORIZED, 89);
			}

			// Try to login user
			$authUser = Sentinel::login($userApiKey->user, $remember = false);
			if (!$authUser) {
				return $this->respondWithError('Could not authenticate you', ResponseCodes::HTTP_UNAUTHORIZED, 64);
			}

			// Try to set current profile id
			$profileId = $userApiKey->profile->id;
			try {
				ProfileService::setCurrentProfileId($profileId);
			} catch (ModelNotFoundException $ex) {
				return $this->respondWithError('Could not set current profile', ResponseCodes::HTTP_UNAUTHORIZED, 64);
			}

		} catch (\Exception $ex) {
			return $this->respondWithError("Internal error", ResponseCodes::HTTP_INTERNAL_SERVER_ERROR, 131);
		}

		return false;
	}

	/**
	 *Extract auth data from Authorization header
	 *
	 * @param $request
	 * @return bool
	 */
	private function extractAuthData($request)
	{
		$authHeader = $request->header('Authorization');
		$expire = $request->header('X-HPRO-Expire');

		if (empty($authHeader) || empty($expire)) {
			return false;
		}

		$authHeaderArray = explode(':', $authHeader);

		if (count($authHeaderArray) != 2) {
			return false;
		}

		$this->expire = $expire;
		list($this->accessKey, $this->signature) = $authHeaderArray;
		return true;
	}

	/**
	 * Validates guest access
	 * @return mixed
	 */
	public function validateGuestAccess()
	{
		$privateKey = Yaml::get('private_key', 'wl.security.api-consumer-keys');

		return $this->validateSignature($privateKey);
	}

	/**
	 * Validates signature
	 *
	 * string @param $secretKey
	 * @return mixed
	 */
	private function validateSignature($secretKey)
	{
		return ApiKeyService::isValid($this->signature, $this->accessKey, $this->expire, $secretKey);
	}

	/**
	 * Response helper method
	 *
	 * @param string $message
	 * @param int $httpCode
	 * @param null $statusCode
	 * @return mixed
	 */
	private function respondWithError($message, $httpCode = ResponseCodes::HTTP_UNAUTHORIZED, $statusCode = null)
	{
		return ApiController::generateApiErrorResponse($message, $httpCode, $statusCode);
	}

}

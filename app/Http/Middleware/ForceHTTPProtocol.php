<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

class ForceHTTPProtocol {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($request->secure() && (!isset($_ENV['FORCE_HTTPS']) || !$_ENV['FORCE_HTTPS'])) {
			return Redirect::to($request->path(), 302, [], false);
		}
		return $next($request);
	}

}

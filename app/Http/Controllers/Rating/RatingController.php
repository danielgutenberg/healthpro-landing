<?php namespace App\Http\Controllers\Rating;

use App\Http\Requests\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use WL\Controllers\ControllerSite;
use WL\Modules\Rating\Commands\RateEntityCommand;

/**
 * Class RatingController
 * @package App\Http\Controllers\Rating
 */
class RatingController extends ControllerSite
{
	use DispatchesJobs;

	private $shortTypes = [
		'review' => 'WL\Modules\Review\Models\Review',
	];

	public function rate($entityShortType, $entityId, $ratingType, $ratingLabel, $ratingValue, Request $request)
	{
		$entityType = array_key_exists($entityShortType, $this->shortTypes)
			? $this->shortTypes[$entityShortType]
			: $entityShortType;

		$inpData = [
			'entity_type' => $entityType,
			'entity_id' => $entityId,
			'rating_type' => $ratingType,
			'rating_label' => $ratingLabel,
			'rating_value' => $ratingValue,
		];

		$this->runCommandWithErrorHandle(new RateEntityCommand($inpData), $request);

		return back();
	}
}

<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\User\Commands\ChangeUserPasswordCommand;
use WL\Modules\User\Commands\LoginUserCommand;
use WL\Modules\User\Commands\LogoutUserCommand;
use WL\Modules\User\Commands\RegisterUserCommand;
use WL\Modules\User\Commands\ResetPasswordCommand;
use WL\Modules\User\Commands\SendActivationCommand;

class AuthApiController extends ApiController
{
	/**
	 * @api {post} /auth/register Register
	 * @apiDescription Creates the user and the initial profile. On Success, it logs the user in.
	 * @apiName ajax-auth-sign-up
	 * @apiGroup Auth
	 *
	 * @apiParam {String} email User's email
	 * @apiParam {String} first_name
	 * @apiParam {String} last_name
	 *
	 * @apiParam (Password Registration) {String {7..255}} [password]
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.email An account already exists for this email.
	 * @apiError (Error: 422) {String[]} messages.password Password is too short or not set
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"email":["An account already exists for this email."]}}}
	 *
	 *
	 *

	 **/
	/**
	 * Register new user
	 *
	 * @param ApiRequest $request
	 * @return string
	 */
	public function signUp(ApiRequest $request)
	{
		$isApi = $this->isApiCall($request->route());
		$isAjax = $this->isAjaxCall($request->route());
		$social = null;

		$email = $request->input('email');
		$password = $request->input('password');
		$firstName = $request->input('first_name');
		$lastName = $request->input('last_name');
		$profileType = $request->input('profile_type', ModelProfile::PROVIDER_PROFILE_TYPE);
		$redirectTo = $request->input('redirect_to');
		$code = $request->has('code') ? $request->input('code') : session('coupon_code');

		return $this->exec([
			new RegisterUserCommand($request, $email, $password, $firstName, $lastName, $profileType, $code, $isApi, $isAjax)
		]);
	}

	/**
	 * @api {post} /auth/login Login
	 * @apiDescription Logs in the user.  Either social or email/password combination is needed.  Not both
	 * @apiName ajax-auth-login
	 * @apiGroup Auth
	 *
	 * @apiParam {String} email
	 *
	 * @apiParam (Password Registration) {String {7..255}} [password]
	 *
	 * @apiParam (Social Register) {object} [social] Mobile Only.
	 * @apiParam (Social Register) {String} social.provider [facebook|linkedin] social network name
	 * @apiParam (Social Register) {String} social.accessToken social network id
	 *
	 * @apiParam (Web) {Boolean} [remember]
	 * @apiParam {String {7..255}} password
	 * @apiParam {Boolean} [remember]
	 * @apiParam {String} [profile_type] client|provider
	 * @apiVersion 1.0.0
	 *
	 */

	/**
	 * Sign in user
	 *
	 * @param ApiRequest $request
	 * @return string
	 */
	public function signIn(ApiRequest $request)
	{
		$social = null;
		$isApi = $this->isApiCall($request->route());
		$isAjax = $this->isAjaxCall($request->route());

		if ($isApi)
			$social = $request->get('social');


		$email = $request->input('email');
		$password = $request->input('password');
		$remember = $request->input('remember');
		$profileType = $request->input('profile_type');
		$redirectTo = $request->input('redirect_to');

		return $this->exec(new LoginUserCommand($email, $password, $remember, $profileType, $isApi, $isAjax, $social, $redirectTo));
	}

	/**
	 * @api {post} /auth/reset Reset Password
	 * @apiDescription Sends the user a reset link to reset their password.
	 * @apiName ajax-auth-reset-password
	 * @apiGroup Auth
	 *
	 * @apiParam {String} email
	 * @apiVersion 1.0.0
	 */
	/**
	 * Reset password
	 * @param ApiRequest $request
	 * @return string
	 */
	public function resetPassword(ApiRequest $request)
	{
		$email = $request->input('email');
		return $this->exec(new ResetPasswordCommand($email));
	}

	/**
	 * @api {post} /auth/set_password Updates Password
	 * @apiDescription Updates user password after clicking on link in email for forgot password.
	 * @apiName ajax-auth-set-new-password
	 * @apiGroup Auth
	 *
	 * @apiVersion 1.0.0
	 */
	/**
	 * Reset password
	 * @param ApiRequest $request
	 * @return string
	 */
	public function setNewPassword(ApiRequest $request)
	{
		$password = $request->input('newPassword');
		$confirmPassword = $request->input('newPassword_confirmation');

		return $this->exec(new ChangeUserPasswordCommand('', $password, $confirmPassword, true));
	}


	/**
	 * @api {get} /auth/logout Logout User
	 * @apiHeader {api_access_key} Authorization api_access_key:signature
	 * @apiHeader {expire} X-HPRO-Expire time() + 100
	 * @apiName ajax-auth-logout
	 * @apiGroup Auth
	 *
	 * @apiParam (AuthorizationHeader) {string} api_access_key key which you receive when login
	 * @apiParam (AuthorizationHeader) {integer} expire time when signature will expire
	 * @apiParam (AuthorizationHeader) {string} signature base64_encode(hash_hmac('sha256', $api_access_key . ':' . $expire, $api_secret_key, true));
	 *
	 * @return string
	 */

	public function logout()
	{
		return $this->exec(new LogoutUserCommand());
	}

	/**
	 * @api {get} /auth/caffeine Reset Session
	 * @apiDescription For ajax only. Sends a request to the server to keep the session alive for the website
	 * @apiName ajax-auth-caffeine
	 * @apiGroup Auth
	 *
	 * @apiVersion 1.0.0
	 */
	/**
	 * Reset Session
	 * @return string
	 */

	public function refreshSession()
	{
		return response('', 204);
	}


	/**
	 * @api {get} /auth/send-activation Re-send activation email
	 * @apiDescription For ajax only. Sends a request to the server to re-send activation email
	 * @apiName ajax-send-activation
	 * @apiGroup Auth
	 *
	 * @apiVersion 1.0.0
	 *
	 * @return string
	 */

	public function sendActivation()
	{
		return $this->exec(new SendActivationCommand());
	}

}

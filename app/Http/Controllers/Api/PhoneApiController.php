<?php namespace App\Http\Controllers\Api;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use WL\Modules\Phone\Phone;
use WL\Modules\Phone\Commands\UpdateProfilePhonesCommand;
use WL\Modules\Phone\Contracts\PhoneServiceContract;
use WL\Modules\Phone\PhoneFactory;
use WL\Modules\Phone\Transformers\PhoneTransformer;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\User\Repositories\UserRepository;
use InvalidArgumentException;

class PhoneApiController extends ApiController
{

    /**
     * @api {put} /user/me User Update
     * @apiDescription Updates user email or password.
     * @apiName ajax-user-update-put
     * @apiGroup User
     *
     * @apiParam {String {7..255}} current_password
     * @apiParam (Change Email) {String} [email]
     * @apiParam (Change Password) {String {7..255}} [password]
     * @apiParam (Change Password) {String {7..255}} [confirmed_password]
     * @apiVersion 1.0.0
     *
     *  @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     * {"message":"success"}
     *
     * @apiError (Error: 422) {Object} messages An object with validation messages for each field
     *
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 422
     *     {"message":"error","errors":{"email":{"messages":["This is your current e-mail address. Specify any other e-mail."],"status_code":"validation"}}}
     *
     * @param Request $request
     * @return mixed
     */
    public function addPhone(Request $request, PhoneServiceContract $phoneService)
    {
        $profile = ProfileService::getProfileById(ProfileService::getCurrentProfileId());

        $data = [
            'prefix'      => 1,
            'number'      => $request->phone,
            'is_notified' => 0,
            'rel_id'      => $profile->id,
            'rel_type'    => ModelProfile::class,
        ];

        $phone = $phoneService->addPhone($data);

        $phoneUseCount = Phone::where('prefix', $phone->prefix)->where('number', $phone->number)->whereNotNull('verified_at')->count();
        if ($phoneUseCount >= Phone::MAX_VERIFICATIONS) {
            throw new InvalidArgumentException('This phone number has already been used too many times for verification.');
        }

        $isSuccess = $phoneService->verifyPhone($phone, $request->phone_verify_method);
        if (!$isSuccess) {
            throw new InvalidArgumentException($phone->getErrors());
        }

        return $this->respondOk((new PhoneTransformer())->transform($phone));
    }

    public function resendCode(Request $request, PhoneServiceContract $phoneService, $profileId, $phoneId)
    {
        $profile = ProfileService::getProfileById($profileId);
        $phone = $profile->phone;

        if ($phone->id != $phoneId) {
            throw new ModelNotFoundException(_('Invalid phone'));
        }

        $isSuccess = $phoneService->verifyPhone($phone, $request->phone_verify_method);
        if (!$isSuccess) {
            throw new InvalidArgumentException($phone->getErrors());
        }

        return $this->respondOk((new PhoneTransformer())->transform($phone));
    }


    public function verifyPhone(PhoneServiceContract $phoneService, Request $request, $profileId, $phoneId)
    {
        $profile = ProfileService::getProfileById($profileId);
        $phone = $profile->phone;

        if ($phone->id != $phoneId) {
            throw new ModelNotFoundException(_('Invalid phone'));
        }

        $isValid = $phoneService->completeVerification($phone, $request->input('code'));
        if (!$isValid) {
            throw new InvalidArgumentException('Incorrect verification code');
        }

        return $this->respondOk((new PhoneTransformer())->transform($phone));
    }

	public function editPhone(PhoneServiceContract $phoneService, Request $request, $profileId, $phoneId)
	{
		$profile = ProfileService::getProfileById($profileId);
		$phone = $profile->phone;

		if ($phone->id != $phoneId) {
			throw new ModelNotFoundException(_('Invalid phone'));
		}

		$isValid = $phoneService->editPhone($phone, $request->number, $request->phone_verify_method);
		if (!$isValid) {
			throw new InvalidArgumentException('Incorrect verification code');
		}

		return $this->respondOk((new PhoneTransformer())->transform($phone));
	}
}






<?php namespace App\Http\Controllers\Api;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use WL\Modules\Questionnaires\Facade\QuestionnaireService;
use WL\Modules\Questionnaires\QuestionnairePaginateResultTransformer;
use WL\Modules\Questionnaires\QuestionnaireWithResultTransformer;
use WL\Modules\Questionnaires\QuestionnaireWithTemplateTransformer;
use Illuminate\Support\Facades\Input;
use Watson\Validating\ValidationException;


#TODO profile security
class QuestionnaireApiController extends ApiController
{

	public function getQuestionnaire($questionnaireId)
	{
		return $this->respondOk(
			(new QuestionnaireWithTemplateTransformer())->transform(
				QuestionnaireService::getQuestionnaireById($questionnaireId))
		);
	}

	public function getQuestionnaireResultByOwner($questionnaireId, $ownerId)
	{
		return $this->respondOk(
			(new QuestionnaireWithResultTransformer(QuestionnaireService::getResult($questionnaireId, $ownerId)))->transform(
				QuestionnaireService::getQuestionnaireById($questionnaireId))
		);
	}

	public function setQuestionnaire()
	{
		$questionnaireData = json_decode(Input::get('questionnaireData'),true);

		$questionnaire = QuestionnaireService::setQuestionnaire($questionnaireData);
		$result = ['ok' => false];

		try {
			QuestionnaireService::saveQuestionnaire($questionnaire);
			$result['id'] = $questionnaire->id;
			$result['ok'] = true;
		}catch (ValidationException $ex)
		{
			$result['errors'] = $questionnaire->getErrors();

		}
		return $this->respondOk($result);
	}

	public function updateQuestionnaire($questionnaireId)
	{
		$questionnaireData = json_decode(Input::get('questionnaireData'),true);

		$questionnaire = QuestionnaireService::setQuestionnaire($questionnaireData, $questionnaireId);

		$result = ['ok' => false];

		try {
			QuestionnaireService::saveQuestionnaire($questionnaire);
			$result['id'] = $questionnaire->id;
			$result['ok'] = true;
		}catch (ValidationException $ex)
		{
			$result['errors'] = $questionnaire->getErrors();
		}

		return $this->respondOk($result);
	}

	public function setTemplate($questionnaireId)
	{
		$templateArray = json_decode(Input::get('questionsArray'),true);

		return $this->respondOk(['ok' => QuestionnaireService::setTemplate(QuestionnaireService::getQuestionnaireById($questionnaireId), $templateArray)]);
	}

	public function copyQuestionnaire($questionnaireId, $ownerProfileId)
	{
		$newQuestionnaire = QuestionnaireService::copyQuestionnaire($questionnaireId, $ownerProfileId);

		$resp = ['ok' => $newQuestionnaire != null];

		if (isset($newQuestionnaire)) {
			$resp['id'] = $newQuestionnaire->id;
		}

		return $this->respondOk($resp);
	}

	public function paginateResults($questionnaireId, $page, $perPage)
	{
		return $this->respondOk(
			(new QuestionnairePaginateResultTransformer())->transformCollection(QuestionnaireService::getPaginatedResults($questionnaireId, $page, $perPage)->all())
		);
	}

	public function setResult($questionnaireId)
	{
		$resultArray = json_decode(Input::get('resultArray'),true);
		return $this->respondOk(['ok' => QuestionnaireService::setResult(QuestionnaireService::getQuestionnaireById($questionnaireId), Sentinel::check(), $resultArray)]);
	}


	public function searchMyQuestionnaire($profileId, $term,$page,$perPage)
	{
		return $this->respondOk(
			(new QuestionnaireWithTemplateTransformer())->transformCollection(QuestionnaireService::searchQuestionnaire($profileId,$term,$page,$perPage)->all()));
	}

	public function searchPublicQuestionnaire($term,$page,$perPage)
	{
		return $this->respondOk(
			(new QuestionnaireWithTemplateTransformer())->transformCollection(QuestionnaireService::searchPublicQuestionnaire($term,$page,$perPage)->all()));
	}

	public function removeQuestionnaire($questionnaireId)
	{
		return $this->respondOk(['ok' => QuestionnaireService::removeQuestionnaireById($questionnaireId)]);
	}



}

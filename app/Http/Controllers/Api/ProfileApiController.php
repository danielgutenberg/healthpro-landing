<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use WL\Modules\Profile\Commands\CheckEmailExistsCommand;
use WL\Modules\Profile\Commands\CheckProviderSlugCommand;
use WL\Modules\Profile\Commands\CreateProfileCommand;
use WL\Modules\Profile\Commands\DeleteAssetFromProfileCommand;
use WL\Modules\Profile\Commands\DeleteFileFromProfileCommand;
use WL\Modules\Profile\Commands\GetProfileAssetCommand;
use WL\Modules\Profile\Commands\GetProfileAssetsCommand;
use WL\Modules\Profile\Commands\GetProfileAvailableTagsCommand;
use WL\Modules\Profile\Commands\GetProfileBillingDetailsCommand;
use WL\Modules\Profile\Commands\GetProfileDetailsCommand;
use WL\Modules\Profile\Commands\GetProfileSummaryDetailsCommand;
use WL\Modules\Profile\Commands\GetUserProfilesCommand;
use WL\Modules\Profile\Commands\ImportProviderClientsCsvCommand;
use WL\Modules\Profile\Commands\ImportProviderClientsListCommand;
use WL\Modules\Profile\Commands\Retreive\SearchProfessionalByNameCommand;
use WL\Modules\Profile\Commands\SetCurrentProfileCommand;
use WL\Modules\Profile\Commands\UpdateProfileAssetCommand;
use WL\Modules\Profile\Commands\UpdateProfileBillingDetailsCommand;
use WL\Modules\Profile\Commands\UpdateProfileCommand;
use WL\Modules\Profile\Commands\UploadAssetToProfileCommand;
use WL\Modules\Profile\Commands\UploadFileToProfileCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Transformers\ClientPrivateProfileTransformer;
use WL\Modules\Profile\Transformers\ProfileAssetTransformer;
use WL\Modules\Profile\Transformers\ProviderImportCsvTransformer;
use WL\Modules\Profile\Transformers\ProviderPrivateProfileTransformer;
use WL\Modules\Profile\Transformers\ProviderPublicProfileTransformer;
use WL\Modules\Profile\Transformers\PublicProfileTransformer;
use WL\Modules\Profile\Transformers\RestrictedProfileTransformer;
use WL\Modules\Profile\Transformers\StaffPrivateProfileTransformer;

class ProfileApiController extends ApiController
{

	/**
	 * @api {post} /profiles Create and Login into profile
	 * @apiDescription Create Provider or Client Profile
	 * @apiName api-profiles-create
	 * @apiGroup Profiles
	 *
	 * @apiParam {String} first_name
	 * @apiParam {String} last_name
	 * @apiParam {String} profile_type can be client or provider
	 *
	 * @apiVersion 1.0.0
	 */

	public function createProfile(Request $request)
	{
		$inpData = $request->all();
		$inpData['is_api'] = $this->isApiCall($request->route());
		$result = $this->runCommandWithErrorHandle(new CreateProfileCommand($inpData));

		return $this->respondOk($result);

	}

	/**
	 * @api {get} /profiles Get current logged in user profiles
	 * @apiDescription Get current logged in user profiles
	 * @apiName api-profiles-get-all-user
	 * @apiGroup Profiles
	 *
	 * @apiVersion 1.0.0
	 */
	public function getProfiles(Request $request)
	{
		$items = $this->runCommandWithErrorHandle(new GetUserProfilesCommand());

		return $this->respondOk((new PublicProfileTransformer())->transformCollection($items));
	}

	/**
	 * @api {get} /profiles/professionals/search/name Search for professional by name
	 * @apiDescription Search for professional by name
	 * @apiName ajax-profiles-search-professional-by-name
	 * @apiGroup Profiles
	 *
	 * @apiParam {String} query Query string
	 * @apiParam {Number} [limit] Limit result count
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":[{"id":107,"type":"provider","slug":"some-first-name-90210-some-last-name","is_searchable":0,"verified":0,"created_at":"2016-02-22 21:22:36","updated_at":"2016-02-22 21:22:36","avatar":null,"public_url":"http:\/\/localhost\/p\/some-first-name-90210-some-last-name","title":null,"gender":null,"first_name":"some_first_name_90210","last_name":"some_last_name","birthday":null,"full_name":"some_first_name_90210 some_last_name"},{"id":108,"type":"provider","slug":"some-first-name-90210-some-last-name-1","is_searchable":0,"verified":0,"created_at":"2016-02-22 21:22:36","updated_at":"2016-02-22 21:22:36","avatar":null,"public_url":"http:\/\/localhost\/p\/some-first-name-90210-some-last-name-1","title":null,"gender":null,"first_name":"some_first_name_90210","last_name":"some_last_name","birthday":null,"full_name":"some_first_name_90210 some_last_name"},{"id":109,"type":"provider","slug":"some-first-name-90210-some-last-name-2","is_searchable":0,"verified":0,"created_at":"2016-02-22 21:22:36","updated_at":"2016-02-22 21:22:36","avatar":null,"public_url":"http:\/\/localhost\/p\/some-first-name-90210-some-last-name-2","title":null,"gender":null,"first_name":"some_first_name_90210","last_name":"some_last_name","birthday":null,"full_name":"some_first_name_90210 some_last_name"},{"id":110,"type":"provider","slug":"some-first-name-90210-some-last-name-3","is_searchable":0,"verified":0,"created_at":"2016-02-22 21:22:36","updated_at":"2016-02-22 21:22:36","avatar":null,"public_url":"http:\/\/localhost\/p\/some-first-name-90210-some-last-name-3","title":null,"gender":null,"first_name":"some_first_name_90210","last_name":"some_last_name","birthday":null,"full_name":"some_first_name_90210 some_last_name"},{"id":111,"type":"provider","slug":"some-first-name-90210-some-last-name-4","is_searchable":0,"verified":0,"created_at":"2016-02-22 21:22:36","updated_at":"2016-02-22 21:22:36","avatar":null,"public_url":"http:\/\/localhost\/p\/some-first-name-90210-some-last-name-4","title":null,"gender":null,"first_name":"some_first_name_90210","last_name":"some_last_name","birthday":null,"full_name":"some_first_name_90210 some_last_name"}]}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"query":["query is required"]}}}
	 **/
	public function searchProfessionalByName(ApiRequest $request)
	{
		return $this->exec(new SearchProfessionalByNameCommand($request->all()));
	}

	/**
	 * Check if user's profile exists
	 * @param $email
	 * @return mixed
	 */
	public function emailExists($profileType, $email)
	{
		try {
			$exists = $this->dispatch(new CheckEmailExistsCommand([
				'profileType' => $profileType,
				'email' => $email
			]));
		} catch (ValidationException $e) {
			return $this->respondNotFound();
		}

		return $this->respondOk([
			'exists' => $exists
		]);
	}

	/**
	 * @api {put} profiles/switch-profile/:id Switch Authed Profile
	 * @apiDescription Switch the currently auth'd profile to the passed :id
	 * @apiName api-profiles-switch-profile
	 * @apiGroup Profiles
	 *
	 * @apiParam {Number} profileId
	 * @apiVersion 1.0.0
	 */
	public function setCurrentProfile($profileId)
	{
		return $this->exec(new SetCurrentProfileCommand($profileId));
	}


	/**
	 * @api {PUT} /profiles/:id Profile Update
	 * @apiDescription Update the profile's overly detailed profile.
	 * @apiName ajax-profiles-update
	 * @apiGroup Profiles
	 *
	 * @apiParam {Number} profileId User's email
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {String} [first_name] First name of the User.
	 * @apiParam {String} [last_name]  Last name of the User.
	 * @apiParam {Boolean} [update_slug] Update slug if first_name or last_name has been changed changed
	 * @apiParam {Object} [birthday]  User birthday
	 * @apiParam {Number} birthday.day
	 * @apiParam {Number} birthday.month
	 * @apiParam {Number} birthday.year
	 * @apiParam {String} [gender]  Profile's Gender
	 * @apiParam {Number} [phone]  Profile's Phone.
	 * @apiParam {String{2}} [country_id]  Profile's country
	 * @apiParam {string} [slug] provider's slug, allowed for provider. only letters, digits and dash are allowed
	 *
	 * @apiParam {Object} [social_pages] The social/website links
	 * @apiParam {String} [social_pages.link] The profile's website
	 * @apiParam {String} [social_pages.twitter] The profile's twitter profile url
	 * @apiParam {String} [social_pages.facebook] The profile's facebook profile url
	 * @apiParam {String} [social_pages.googleplus] The profile's googleplus profile url
	 * @apiParam {String} [social_pages.linkedin] The profile's linkedin profile url
	 * @apiParam {String} [social_pages.pinterest] The profile's pinterest profile url
	 * @apiParam {String} [social_pages.instagram] The profile's instagram profile url
	 * @apiParam {String} [social_pages.youtube] The profile's youtube profile url
	 *
	 * @apiParam {String} [about_self_background] Provider Profile Tags
	 * @apiParam {String} [about_what_i_do] Provider Profile Tags
	 * @apiParam {String} [about_services_benefit] Provider Profile Tags
	 * @apiParam {String} [about_self_specialities] Provider Profile Tags
	 *
	 * @apiParam {array} [tags] Profile tags
	 * @apiParam {String[]} tags.[educational_activity] List of educational_activity tags.
	 * @apiParam {String[]} tags.[languages] List of languages tags.
	 * @apiParam {String[]} tags.[age_group] List of age_group tags.
	 * @apiParam {String[]} tags.[type_of_services] List of type_of_services tags.
	 * @apiParam {String[]} tags.[gender_identities] List of gender_identities tags.
	 * @apiParam {String[]} tags.[special_needs] List of special_needs tags.
	 * @apiParam {String[]} tags.[session_format] List of session_format tags.
	 * @apiParam {String[]} tags.[concerns_conditions] List of concerns_conditions tags.
	 * @apiParam {String[]} tags.[services] List of services tags.
	 * @apiParam {String[]} tags.[experience] List of experience tags.
	 *
	 * @apiParam {Object[]} [professional_body] A list of professional title a user has.
	 * @apiParam {String} professional_body.title The name of the professional title
	 * @apiParam {Date} professional_body.expiry_date The expiration date
	 *
	 * @apiParam {Object[]} [educations] A list of universities a user has attended.
	 * @apiParam {String} educations.education_id The id of the university (from /core/educations)
	 * @apiParam {Date} educations.from The date the user started attending.
	 * @apiParam {Date} [educations.to] The date the user left the university.
	 *
	 *
	 * @apiParam {Object[]} [reminders] A list of reminders to update.
	 * @apiParam {Integer} reminders.id The id of reminder, should receive with get full profile details
	 * @apiParam {Integer} reminders.value The value of reminder, indicates how much hours before will be sent reminder (in hours)
	 *
	 *
	 * ]     * @apiSuccessExample {json} Success-Response:
	 *     HTTP/1.1 200
	 *    {
	 *        "success": "true",
	 *    }
	 */

	public function updateProfile(ApiRequest $request, $id)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = $id;

		$data = $this->runCommandWithErrorHandle(new UpdateProfileCommand($inpData));

		return $this->respond('updated', $data, []);
	}


	/**
	 * @api {POST} /profiles/:id/files File Upload
	 * @apiDescription Uploads file to profile
	 * @apiName ajax-profiles-upload-file
	 * @apiGroup Profile Assets
	 *
	 * @apiParam {Number} profileId Profile Id
	 * @apiParam {File} [avatar] Avatar Image
	 *
	 * @apiVersion 1.0.0
	 */

	public function uploadFileToProfile(ApiRequest $request, $id)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = $id;

		$data = $this->runCommandWithErrorHandle(new UploadFileToProfileCommand($inpData));

		return $this->respond('uploaded', $data, []);

	}


	/**
	 * @api {DELETE} /profiles/:id/files/:fileId File Delete
	 * @apiDescription Deletes file from profile
	 * @apiName ajax-profiles-delete-file
	 * @apiGroup Profile Assets
	 *
	 * @apiParam {Number} profileId Profile Id
	 * @apiParam {Number} fileId File id
	 *
	 * @apiVersion 1.0.0
	 */
	public function deleteFileFromProfile(ApiRequest $request, $id, $fileId)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = $id;
		$inpData['file_id'] = $fileId;

		$data = $this->runCommandWithErrorHandle(new DeleteFileFromProfileCommand($inpData));

		return $this->respond('delete', $data, []);

	}

	/**
	 * @api {get} /profiles/:id Profile Get
	 * @apiDescription Return basic (public) Profile details
	 * @apiName ajax-profiles-get-detailed
	 * @apiGroup Profiles
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422 {"message":"error","errors":{"messages":{"profile_id":["validation.required"]}}}
	 */
	/**
	 * @api {get} /profiles/me Profile Get Me
	 * @apiDescription Return private (full) Profile Details
	 * @apiName ajax-profiles-get-me
	 * @apiGroup Profiles
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422 {"message":"error","errors":{"messages":{"profile_id":["validation.required"]}}}
	 */
	public function getProfilePublicDetails(ApiRequest $request, $id = null)
	{
		if ($id === null) {
			return $this->respondWithError('Profile not set', 500);
		}

		$transformers = [
			ModelProfile::PROVIDER_PROFILE_TYPE => new ProviderPrivateProfileTransformer(),
			ModelProfile::CLIENT_PROFILE_TYPE => new ClientPrivateProfileTransformer(),
			ModelProfile::STAFF_PROFILE_TYPE => new StaffPrivateProfileTransformer(),
			'public' => new PublicProfileTransformer(),
			'restricted' => new RestrictedProfileTransformer(),
		];


		$currentId = ProfileService::getCurrentProfileId();
		$response = $this->runCommandWithErrorHandle(new GetProfileDetailsCommand(['profile_id' => $id]))['profile'];

		if ($id == $currentId) {
			$transformer = $response->type;
		} else {
			$transformer = ProfileService::profileIsVisible($currentId, $id) ? 'public' : 'restricted';
		}

		$transformer = array_get($transformers, $transformer);
		if(!$transformer){
			return $this->respondWithError('no Transformer found', 500);
		}
		return $this->respondOk($transformer->transform($response));
	}

	/**
	 * @api {get} /providers/:id|:slug ?:fields Provider Profile Get Summary
	 * @apiDescription Return Provider Profile Summary
	 * @apiName ajax-profile-provider-get-summary
	 * @apiGroup Providers
	 *
	 * @apiParam {string} [fields] optional parameter, if not set returns default fields, if `fields=all` returns all fields, to returns some fields,you must specify it with comma delimiter like `fields=tags,assets`
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 404 {"message":"error","errors":{"messages":"No profile with such ID or Slug"}}
	 */
	public function getProvider(ApiRequest $request, $idOrSlug)
	{
		try {
			$profile = ProfileService::getProfileByIdOrSlug($idOrSlug);
		} catch (ModelNotFoundException $e) {
			return $this->respondWithError('No profile with such ID or Slug', 404);
		}

		if ($profile->type == ModelProfile::PROVIDER_PROFILE_TYPE) {
			$profile = $this->runCommandWithErrorHandle(new GetProfileSummaryDetailsCommand([
				'profile_id' => $profile->id,
				'fields' => $request->get('fields', null)
			]));

			return $this->respondOk((new ProviderPublicProfileTransformer())->transform($profile));

		}

		return $this->respondWithError('No provider with such ID or Slug', 404);

	}

	/**
	 * @api {get} core/profile/slug/:slug Provider Slug Verify
	 * @apiDescription Check provider slug, duplication, syntax and if it was manually changed
	 * @apiName ajax-profile-check-slug
	 * @apiGroup Core
	 *
	 * @apiParam {String} slug only letters, digits and dashes are allowed
	 */
	public function checkSlug(ApiRequest $request, $slug)
	{
		$inpData = ['slug' => $slug];
		$result = $this->runCommandWithErrorHandle(new CheckProviderSlugCommand($inpData));

		return $this->respondOk($result);
	}

	/**
	 * @api {get} /profiles/available-tags Available tags for Profile
	 * @apiDescription Available tags for Profile
	 * @apiName ajax-profiles-available-tags
	 * @apiGroup Profiles
	 */
	public function getProfileAvailableTags(ApiRequest $request)
	{
		$inpData = $request->all();
		$data = $this->runCommandWithErrorHandle(new GetProfileAvailableTagsCommand($inpData));

		return $this->respond('profileAvailableTags', $data, []);

	}

	/**
	 * @api {put} /profiles/:id/videos/:assetId Video Asset Update
	 * @apiDescription update profile asset
	 * @apiName ajax-profiles-update-asset-video
	 * @apiGroup Profile Assets
	 *
	 * @apiParam id Profile id
	 * @apiParam assetId Asset id
	 */
	/**
	 * @api {put} /profiles/:id/photos/:assetId Photo Asset Update
	 * @apiDescription update profile asset
	 * @apiName ajax-profiles-update-asset-photo
	 * @apiGroup Profile Assets
	 *
	 * @apiParam id Profile id
	 * @apiParam assetId Asset id
	 */
	/**
	 * @api {put} /profiles/:id/certificates/:assetId Certificate Asset Update
	 * @apiDescription update profile asset
	 * @apiName ajax-profiles-update-asset-certificate
	 * @apiGroup Profile Assets
	 *
	 * @apiParam id Profile id
	 * @apiParam assetId Asset id
	 */
	public function updateProfileAsset(ApiRequest $request, $id, $assetId)
	{
		$inpData = $request->all();
		$inpData['asset_id'] = $assetId;
		$inpData['profile_id'] = $id;

		$data = $this->runCommandWithErrorHandle(new UpdateProfileAssetCommand($inpData));

		return $this->respond('updateProfileAsset', $data, []);
	}

	/**
	 * @api {get} /profiles/:id/videos/:assetId Video Asset Get
	 * @apiDescription Get Video asset
	 * @apiName ajax-profiles-get-asset-video
	 * @apiGroup Profile Assets
	 *
	 * @apiParam id Profile id
	 * @apiParam assetId Asset id
	 */

	/**
	 * @api {get} /profiles/:id/certificates/:assetId Certificate Asset Get
	 * @apiDescription Get Certificate asset
	 * @apiName ajax-profiles-get-asset-certificate
	 * @apiGroup Profile Assets
	 *
	 * @apiParam id Profile id
	 * @apiParam assetId Asset id
	 */
	/**
	 * @api {get} /profiles/:id/photos/:assetId Photo Asset Get
	 * @apiDescription Get photo asset
	 * @apiName ajax-profiles-get-asset-photo
	 * @apiGroup Profile Assets
	 *
	 * @apiParam id Profile id
	 * @apiParam assetId Asset id
	 */
	public function getProfileAsset(ApiRequest $request, $id, $assetId)
	{
		$inpData['asset_id'] = $assetId;
		$inpData['profile_id'] = $id;

		$asset = $this->runCommandWithErrorHandle(new GetProfileAssetCommand($inpData));

		return $this->respond('getProfileAsset', (new ProfileAssetTransformer())->transform($asset), []);
	}


	/**
	 * @api {get} /profiles/:id/videos/ Video Assets Get
	 * @apiDescription Get All associated with profile video Assets
	 * @apiName ajax-profiles-all-get-asset-video
	 * @apiGroup Profile Assets
	 *
	 * @apiParam id Profile id
	 */

	public function getProfileVideo(ApiRequest $request, $id)
	{
		return $this->getProfileAssets($request, $id, 'video');
	}


	/**
	 * @api {get} /profiles/:id/certificates Certificate Assets Get
	 * @apiDescription Get All associated with profile certificate Assets
	 * @apiName ajax-profiles-all-get-asset-certificate
	 * @apiGroup Profile Assets
	 *
	 * @apiParam id Profile id
	 */
	public function getProfileCertificate(ApiRequest $request, $id)
	{
		return $this->getProfileAssets($request, $id, 'certificate');
	}

	/**
	 * Intentionally no docs
	 */
	public function getProfilePodcast(ApiRequest $request, $id)
	{
		return $this->getProfileAssets($request, $id, 'podcast');
	}

	/**
	 * @api {get} /profiles/:id/photos Photo Assets Get
	 * @apiDescription Get All associated with profile photo Assets
	 * @apiName ajax-profiles-all-get-asset-photo
	 * @apiGroup Profile Assets
	 *
	 * @apiParam id Profile id
	 */
	public function getProfilePhoto(ApiRequest $request, $id)
	{
		return $this->getProfileAssets($request, $id, 'photo');
	}

	/**
	 * Intentionally no docs
	 */
	public function getProfileArticle(ApiRequest $request, $id)
	{
		return $this->getProfileAssets($request, $id, 'article');
	}


	/**
	 * @param ApiRequest $request
	 * @param            $id
	 * @param            $assetType
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function getProfileAssets(ApiRequest $request, $id, $assetType)
	{
		$inpData['profile_id'] = $id;
		$inpData['asset_type'] = $assetType;

		$assets = $this->runCommandWithErrorHandle(new GetProfileAssetsCommand($inpData));

		return $this->respond('getProfileAsset', (new ProfileAssetTransformer())->transformCollection($assets), []);
	}

	/**
	 * @api {post} /profiles/:id/videos Video Asset Add
	 * @apiDescription Add an asset to the profile.
	 * @apiName ajax-profiles-upload-asset-video
	 * @apiGroup Profile Assets
	 *
	 * @apiParam {Number} profileId
	 * @apiVersion 1.0.0
	 * @apiParam        url http//someurl.com
	 * @apiParam        title
	 * @apiParam        description
	 * @apiParam        type video
	 */
	/**
	 * @api {post} /profiles/:id/photos Photo Asset Add
	 * @apiDescription Add an asset to the profile.
	 * @apiName ajax-profiles-upload-asset-photo
	 * @apiGroup Profile Assets
	 *
	 * @apiParam {Number} profileId
	 * @apiVersion 1.0.0
	 * @apiParam        title
	 * @apiParam        description
	 * @apiParam        type photo
	 * @apiParam        file
	 */
	/**
	 * @api {post} /profiles/:id/certificates Certificate Asset Add
	 * @apiDescription Add an asset to the profile.
	 * @apiName ajax-profiles-upload-asset-certificate
	 * @apiGroup Profile Assets
	 *
	 * @apiParam {Number} profileId
	 * @apiVersion 1.0.0
	 * @apiParam        title
	 * @apiParam        description
	 * @apiParam        type certificate
	 * @apiParam        file
	 */
	public function uploadProfileAsset(ApiRequest $request, $id)
	{
		$inpData['asset'] = $request->all();
		$inpData['profile_id'] = $id;

		$data = $this->runCommandWithErrorHandle(new UploadAssetToProfileCommand($inpData));

		return $this->respond('uploadProfileAsset', $data, []);
	}

	/**
	 * @api {Delete} /profiles/:id/videos/:assetId Video Asset Delete
	 * @apiDescription Delete a asset from the profile.
	 * @apiName ajax-profiles-delete-asset-video
	 * @apiGroup Profile Assets
	 *
	 * @apiParam {Number} profileId Profile ID
	 * @apiParam {Number} assetId Asset ID
	 */

	/**
	 * @api {Delete} /profiles/:id/photos/:assetId Photo Asset Delete
	 * @apiDescription Delete a asset from the profile.
	 * @apiName ajax-profiles-delete-asset-photo
	 * @apiGroup Profile Assets
	 *
	 * @apiParam {Number} profileId Profile ID
	 * @apiParam {Number} assetId Asset ID
	 */

	/**
	 * @api {Delete} /profiles/:id/certificates/:assetId Certificate Asset Delete
	 * @apiDescription Delete a asset from the profile.
	 * @apiName ajax-profiles-delete-asset-certificate
	 * @apiGroup Profile Assets
	 *
	 * @apiParam {Number} profileId Profile ID
	 * @apiParam {Number} assetId Asset ID
	 */


	public function deleteProfileAsset(ApiRequest $request, $id, $assetId)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = $id;
		$inpData['asset_id'] = $assetId;

		$data = $this->runCommandWithErrorHandle(new DeleteAssetFromProfileCommand($inpData));

		return $this->respond('deleteProfileAsset', $data, []);
	}

	/**
	 * @api {POST} /profiles/:id/import/csv Import CSV
	 * @apiDescription imports CSV file with existing provider clients fields in csv(see WA-169): lastName,firstName,email,phone
	 * @apiName ajax-profiles-import-csv
	 * @apiGroup Profile Import
	 *
	 * @apiParam {Number} id Profile ID
	 * @apiParam {file} file A CSV file with data
	 * @apiParam {boolean} [sendEmails] Optional param by default false. indicate when new clients will receive emails about registration
	 * @apiParam {string} [customEmailBody] Optional param has default email when null. Custom email body which clients will receive
	 */
	public function importCsvProviderClients(ApiRequest $request, $id)
	{
		$data = $request->all();
		$data['providerId'] = $id;

		return $this->exec(new ImportProviderClientsCsvCommand($data), function ($result) {
			return (new ProviderImportCsvTransformer())->transform($result);
		});
	}

	/**
	 * @api {POST} /profiles/:id/import/list Import CSV
	 * @apiDescription imports a list with existing provider clients fields: lastName,firstName,email,phone
	 * @apiName ajax-profiles-import-list
	 * @apiGroup Profile Import
	 *
	 * @apiParam {Number} id Profile ID
	 * @apiParam {data} an array with client data
	 * @apiParam {boolean} [sendEmails] Optional param by default false. indicate when new clients will receive emails about registration
	 * @apiParam {string} [customEmailBody] Optional param has default email when null. Custom email body which clients will receive
	 */
	public function importListProviderClients(ApiRequest $request, $id)
	{
		$data = $request->all();
		$data['providerId'] = $id;

		return $this->exec(new ImportProviderClientsListCommand($data), function ($result) {
			return (new ProviderImportCsvTransformer())->transform($result);
		});
	}

	/**
	 * @api {GET} /profiles/{profile_id}/billing_details
	 * @apiDescription Get profile billing details
	 * @apiName ajax-profiles-billing-details-get
	 * @apiGroup Profiles
	 *
	 * @apiParam {Number} profile_id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"billing_first_name":"first_name","billing_last_name":"last_name","billing_address":"12345 NW 4th Ave, New-York USA"}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"query":["query is required"]}}}
	 **/
	public function getBillingDetails($profile_id)
	{
		return $this->exec(new GetProfileBillingDetailsCommand(['profile_id' => $profile_id]));
	}

	/**
	 * @api {PUT} /profiles/{profile_id}/billing_details
	 * @apiDescription Set profile billing details
	 * @apiName ajax-profiles-billing-details-update
	 * @apiGroup Profiles
	 *
	 * @apiParam {Number} profile_id
	 * @apiParam {String} billing_first_name
	 * @apiParam {String} billing_last_name
	 * @apiParam {String} billing_address
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"billing_first_name":"first_name","billing_last_name":"last_name","billing_address":"12345 NW 4th Ave, New-York USA"}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"query":["query is required"]}}}
	 **/
	public function setBillingDetails($profile_id, ApiRequest $request)
	{
		$data = $request->all();
		$data['profile_id'] = $profile_id;

		return $this->exec(new UpdateProfileBillingDetailsCommand($data));
	}


}

<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use WL\Modules\Appointment\Commands\BlockProviderAppointmentTimeCommand;
use WL\Modules\Appointment\Commands\CancelAppointmentCommand;
use WL\Modules\Appointment\Commands\ChangeAppointmentLocationCommand;
use WL\Modules\Appointment\Commands\ConfirmAppointmentCommand;
use WL\Modules\Appointment\Commands\EditAppointmentCommand;
use WL\Modules\Appointment\Commands\EditBlockedTimeCommand;
use WL\Modules\Appointment\Commands\GetAppointmentCommand;
use WL\Modules\Appointment\Commands\GetAppointmentPaymentOptionsCommand;
use WL\Modules\Appointment\Commands\GetAppointmentsCommand;
use WL\Modules\Appointment\Commands\GetPaymentOptionsCommand;
use WL\Modules\Appointment\Commands\GetProviderAppointmentsCommand;
use WL\Modules\Appointment\Commands\GetProviderAvailabilityCommand;
use WL\Modules\Appointment\Commands\GetProviderBlockedTimesCommand;
use WL\Modules\Appointment\Commands\LockTimeForAppointmentCommand;
use WL\Modules\Appointment\Commands\MarkAppointmentCommand;
use WL\Modules\Appointment\Commands\MarkAppointmentPaidCommand;
use WL\Modules\Appointment\Commands\NotifyClientAboutAppointmentCommand;
use WL\Modules\Appointment\Commands\UnBlockProviderAppointmentTimeCommand;
use WL\Modules\Appointment\Transformers\AppointmentTransformer;
use WL\Modules\Appointment\Transformers\BlockedTimeTransformer;
use WL\Modules\Appointment\Transformers\FullAppointmentTransformer;
use WL\Modules\Appointment\Transformers\GetProviderAvailabilityCommandRecurringResultTransformer;
use WL\Modules\Appointment\Transformers\GetProviderAvailabilityCommandResultTransformer;
use WL\Modules\Appointment\Transformers\LockedAppointmentTransformer;
use WL\Modules\Appointment\Transformers\LockTimeForAppointmentResultTransformer;

class AppointmentApiController extends ApiController
{

	/**
	 * @api {get} /providers/:providerId/availabilities Provider Availabilities Get
	 * @apiDescription Provides available time for single provider.
	 * @apiName ajax-provider-availability-get
	 * @apiGroup Providers
	 *
	 * @apiParam {Number} provider_id Provider id
	 * @apiParam {Number} location_id Comma separated Location IDs
	 * @apiParam {Number} service_id Comma separated Service IDs
	 * @apiParam {DateTime} from From date and time
	 * @apiParam {Number} length With length greater or equal than
	 * @apiParam {Number} gap With gap greater or equal than
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"results":[{"locations":{"1":{"id":1,"name":"Flatley, Price and Swift","location_type_id":3,"open_from":"16:08:14","open_till":"16:08:14","created_at":"2015-06-30 16:08:14","updated_at":"2015-06-30 16:08:14","is_verified":1,"verified_at":null,"service_area":null}},"availabilities":{"2015-01-11":[{"id":37,"location_id":1,"from":"2015-01-11T14:00:00+0000","until":"2015-01-11T14:30:00+0000"},{"id":37,"location_id":1,"from":"2015-01-11T14:30:00+0000","until":"2015-01-11T15:00:00+0000"},{"id":37,"location_id":1,"from":"2015-01-11T15:00:00+0000","until":"2015-01-11T15:30:00+0000"},{"id":37,"location_id":1,"from":"2015-01-11T15:30:00+0000","until":"2015-01-11T16:00:00+0000"}]}}],"count":1}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.from Date is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"from":["From must be an date."]}}}
	 **/
	public function providerAvailability($providerId, ApiRequest $request)
	{
		$data = $request->all();
		$data['provider_id'] = $providerId;
		$data['location_id'] = $request->inputArr('location_id');
		$data['service_id'] = $request->inputArr('service_id');

		return $this->exec(new GetProviderAvailabilityCommand($data), function ($result) use ($data) {
			return (new GetProviderAvailabilityCommandResultTransformer())->transform($result);
		});
	}

	/**
	 * @api {post} /appointments/ Appointment Lock Create
	 * @apiDescription Creates and lock appointment.
	 * @apiName ajax-appointment-lock-post
	 * @apiGroup Appointments
	 *
	 * @apiParam {Number} session_id Session id
	 * @apiParam {DateTime} from From date and time
	 * @apiParam {DateTime} [until] Until date and time. You must provide until time if you don't want to pass availability_id.
	 * @apiParam {Number} [provider_id] Provider id. Required if availability_id isn't supplied
	 * @apiParam {Number} [client_id] Client id. Will be current profile if not defined
	 * @apiParam {Number} [location_id] Location id. Required if availability_id isn't supplied
	 * @apiParam {Number} [price] Price id. Required if availability_id isn't supplied
	 * @apiParam {Number} [availability_id] Availability id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"session_id":31,"schedule_registration_id":37,"location_id":1,"client_id":3,"price":10,"length":10,"updated_at":"2015-05-28 10:19:00","created_at":"2015-05-28 10:19:00","id":10}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.from Date is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"from":["From must be an date."]}}}
	 * @param Request $request
	 * @return mixed
	 */
	public function lockAvailability(Request $request)
	{
		return $this->exec(new LockTimeForAppointmentCommand($request->all()), function ($result) {
			return (new LockTimeForAppointmentResultTransformer())->transform($result);
		});
	}

	/**
	 * @api {delete} /appointments/:appointmentId Appointment Cancel
	 * @apiDescription Voids appointment.
	 * @apiName ajax-appointment-cancel-delete
	 * @apiGroup Appointments
	 *
	 * @apiParam {Number} appointmentId Appointment id
	 * @apiParam {String} reason Reason of cancellation
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"appointmentId":["Appointment id must be provided."]}}}
	 **/
	public function cancelAppointment($appointmentId, ApiRequest $request)
	{
		$reason = $request->input('reason');
		return $this->exec(new CancelAppointmentCommand($appointmentId, $reason));
	}


	/**
	 * @api {get} /appointments Client Appointments Get
	 * @apiDescription Provides appointments of current client.
	 * @apiName ajax-appointments-client-get
	 * @apiGroup Appointments
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {String} status [locked|pending|confirmed|missed|attended|cancelled] Comma separated string default value is `confirmed`.
	 * @apiParam {String} type [future|past] Grab certain appointment type. Default value is `all`
	 * @apiParam {String} return_fields [all|basic] defines what fields should be return in response. Default value is `basic`.
	 *
	 *
	 * * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":[{"id":1,"provider_id":4,"provider_service_id":1,"is_first_time":0,"duration":60,"session_id":2,"schedule_registration_id":1,"location_id":52,"client_id":3,"price":30,"created_at":{"date":"2015-07-07 08:37:23.000000","timezone_type":3,"timezone":"UTC"},"updated_at":{"date":"2015-07-07 08:37:23.000000","timezone_type":3,"timezone":"UTC"},"cancellation_policy":null,"transaction_id":null,"status":"locked","location":{"id":52,"name":"Target fitness 1","location_type_id":1,"open_from":null,"open_till":null,"created_at":"2015-07-07 08:37:23","updated_at":"2015-07-07 08:37:23","is_verified":0,"verified_at":"2015-07-07 08:37:23","service_area":0,"status":"active"},"registration":{"id":1,"provider_id":4,"location_id":52,"date":{"date":"2015-07-07 13:00:00.000000","timezone_type":3,"timezone":"UTC"},"unlock_time":null,"type":"lock","current_user_count":1,"from":"2015-07-07 13:00:00","until":"2015-07-07 14:00:00"}}]}
	 *
	 * @param ApiRequest $request
	 * @return mixed
	 */
	public function getAppointments(ApiRequest $request)
	{
		$statuses = $request->inputArr('status', []);
		$type = $request->input('type');

		$data['statuses'] = $statuses;
		$data['type'] = $type;

		return $this->exec(new GetAppointmentsCommand($data), function ($result) {
			return (new FullAppointmentTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {get} /appointments/:appointmentId Appointment Get
	 * @apiDescription Provides appointment.
	 * @apiName ajax-provider-appointment-get
	 * @apiGroup Appointments
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} appointmentId Appointment id
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 * {"message":"success","data":{"appointment":{"date":{"date":"2015-07-03 14:00:00.000000","timezone_type":3,"timezone":"UTC"}},"provider":{"id":4,"title":"mr","gender":"male","first_name":"Ghost Anakin","last_name":"Skywalker","full_name":"Ghost Anakin Skywalker","avatar":"http:\/\/healthpro.app:8000\/uploads\/profiles\/e3733ec126a29dd2ee9bb0c8c19d08bc-250x250.jpg","ratings":{"details":[{"rating_label":"professionalism","rating_value":3,"rating_name":"Professionalism","rating_counts":"5"},{"rating_label":"value","rating_value":3.2,"rating_name":"Value","rating_counts":"5"},{"rating_label":"personal_touch","rating_value":4.8,"rating_name":"Personal Touch","rating_counts":"5"}],"overall":3.6666666666667}},"location":{"id":52,"name":"somename","service_area":10,"address":{"address_id":1,"address":"some string","province":"Portsmouth","city":"Portsmouth","postal_code":211,"country":"US"}}}}
	 *
	 * @param $appointmentId
	 * @return mixed
	 */
	public function getAppointment($appointmentId)
	{
		$data['appointment_id'] = $appointmentId;

		return $this->exec(new GetAppointmentCommand($data), function ($result) {
			return (new LockedAppointmentTransformer())->transform($result);
		});
	}

	public function getAppointmentPaymentOptions($appointmentId)
	{
		$data['appointment_id'] = $appointmentId;

		return $this->exec(new GetAppointmentPaymentOptionsCommand($data));
	}

	public function getProviderPaymentOptions($providerId)
	{
		return $this->exec(new GetPaymentOptionsCommand($providerId));
	}



	/**
	 * @api {get} /providers/:providerId/appointments Provider Appointments Get
	 * @apiDescription Provides appointments.
	 * @apiName ajax-provider-appointments-get
	 * @apiGroup Appointments Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {String} status [locked|pending|confirmed] Comma separated string default value is `pending, confirmed`.
	 * @apiParam {String} type [pending|previous] Grab certain appointment type. Default value is `all`
	 * @apiParam {String} return_fields [all|basic] defines what fields should be return in response. Default value is `basic`.
	 * @apiParam {String} [order] [asc|desc] order by time
	 *
	 * @apiParam {Number} providerId Provider id
	 *
	 * @param $providerId
	 * @param ApiRequest $request
	 * @return mixed
	 */
	public function getProviderAppointments($providerId, ApiRequest $request)
	{
		$statuses = $request->inputArr('status', []);
		$type = $request->input('type');
		$returnFields = $request->input('return_fields', 'basic');
		$order = $request->input('order');

		$data['provider_id'] = $providerId;
		$data['statuses'] = $statuses;
		$data['type'] = $type;
		$data['order'] = $order;

		return $this->exec(new GetProviderAppointmentsCommand($data), function ($result) use ($returnFields) {
			return $returnFields === 'basic'
				? (new AppointmentTransformer())->transformCollection($result)
				: (new FullAppointmentTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {get} /providers/:providerId/blocked Provider Appointments Get
	 * @apiDescription Provides appointments.
	 * @apiName ajax-provider-appointments-get
	 * @apiGroup Appointments Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {String} status [locked|pending|confirmed] Comma separated string default value is `pending, confirmed`.
	 * @apiParam {String} type [pending|previous] Grab certain appointment type. Default value is `all`
	 * @apiParam {String} return_fields [all|basic] defines what fields should be return in response. Default value is `basic`.
	 *
	 * @apiParam {Number} providerId Provider id
	 *
	 * @param $providerId
	 * @param ApiRequest $request
	 * @return mixed
	 */
	public function getProviderBlockedTimes($providerId)
	{
		$data['provider_id'] = $providerId;

		return $this->exec(new GetProviderBlockedTimesCommand($data), function ($result) {
			return (new BlockedTimeTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {put} /appointments/:appointmentId/mark Appointment mark as attended/missed
	 * @apiDescription Allows a Provider to mark previous appointment as attended/missed
	 * @apiName ajax-appointment-mark
	 * @apiGroup Appointments
	 *
	 * @apiParam {Number} appointmentId Appointment id
	 * @apiParam {String} mark Appointment mark - attended|missed
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"appointmentId":["Appointment id must be provided."]}}}
	 * @param $appointmentId
	 * @param ApiRequest $request
	 * @return mixed
	 */
	public function markAppointment($appointmentId, ApiRequest $request)
	{
		$mark = $request->input('mark');

		return $this->exec(new MarkAppointmentCommand($appointmentId, $mark));
	}

	/**
	 * @api {put} /appointments/:appointmentId/paid Appointment mark as attended/missed
	 * @apiDescription Allows a Provider to mark previous appointment as attended/missed
	 * @apiName ajax-appointment-mark
	 * @apiGroup Appointments
	 *
	 * @apiParam {Number} appointmentId Appointment id
	 * @apiParam {String} paid Appointment paid - yes|no
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"appointmentId":["Appointment id must be provided."]}}}
	 * @param $appointmentId
	 * @param ApiRequest $request
	 * @return mixed
	 */
	public function appointmentPaid($appointmentId, ApiRequest $request)
	{
		$paid = $request->input('paid');

		return $this->exec(new MarkAppointmentPaidCommand($appointmentId, $paid));
	}

	/**
	 * @api {put} /appointments/:appointmentId/confirm Appointment Confirm
	 * @apiDescription Allows a provider to confirm a pending appointment.
	 * @apiName ajax-appointment-confirm
	 * @apiGroup Appointments
	 *
	 * @apiParam {Number} appointmentId Appointment id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"appointmentId":["Appointment id must be provided."]}}}
	 * @param $appointmentId
	 * @return mixed
	 */
	public function confirmAppointment($appointmentId)
	{
		return $this->exec(new ConfirmAppointmentCommand($appointmentId));
	}

	/**
	 * @api {put} /appointments/:appointmentId/notify Appointment Client Notify
	 * @apiDescription Triggers a client to receive a notification about the appointment.
	 * @apiName ajax-appointment-notify
	 * @apiGroup Appointments
	 *
	 * @apiParam {Number} appointmentId Appointment id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"appointmentId":["Appointment id must be provided."]}}}
	 * @param $appointmentId
	 * @return mixed
	 */
	public function notifyClientAboutAppointment($appointmentId)
	{
		return $this->exec(new NotifyClientAboutAppointmentCommand($appointmentId));
	}

	/**
	 * @api {POST} providers/{providerId}/availabilities Provider Availabilities Set Unavailable
	 * @apiDescription Block provider time for the specified period. Blocks clients from booking within any overlapped availabilities
	 * @apiName ajax-provider-availability-block
	 * @apiGroup Appointments Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiParam {DateTime} from
	 * @apiParam {DateTime} until
	 *
	 * @param $providerId
	 * @return mixed
	 */
	public function providerBlockOwnTime(ApiRequest $request, $providerId)
	{
		$data = $request->all();
		$data['provider_id'] = $providerId;

		return $this->exec(new BlockProviderAppointmentTimeCommand($data), function ($result) {
			return (new BlockedTimeTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {PUT} providers/{providerId}/registrations/{registrationId} Provider Availabilities Set Unavailable
	 * @apiDescription Edit existing block of time
	 * @apiName ajax-provider-block-edit
	 * @apiGroup Appointments Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiParam {Number} registrationId Registration Slot id
	 * @apiParam {DateTime} from
	 * @apiParam {DateTime} until
	 * @apiParam {String} description
	 *
	 * @param $registrationId
	 * @return mixed
	 */
	public function providerEditBlockedTime($providerId, $registrationId, ApiRequest $request)
	{
		$data = $request->all();
		$data['provider_id'] = $providerId;
		$data['registration_id'] = $registrationId;

		return $this->exec(new EditBlockedTimeCommand($data), function ($result) {
			return (new BlockedTimeTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {DELETE} providers/{providerId}/availabilities Remove unavailable time range
	 * @apiDescription Removes blocked provider time by specified period
	 * @apiName ajax-provider-availability-unblock
	 * @apiGroup Appointments Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiParam {DateTime} from
	 * @apiParam {DateTime} until
	 *
	 * @param $providerId
	 * @return mixed
	 */
	public function providerUnBlockOwnTime(ApiRequest $request, $providerId)
	{
		$data = $request->all();
		$data['provider_id'] = $providerId;

		return $this->exec(new UnBlockProviderAppointmentTimeCommand($data), function ($result) {
			return (new BlockedTimeTransformer())->transformCollection($result);
		});
	}

	public function editAppointment($appointmentId, ApiRequest $request)
	{
		$data = $request->all();
		$data['appointment_id'] = $appointmentId;

		return $this->exec(new EditAppointmentCommand($data), function ($result) {
			return (new LockTimeForAppointmentResultTransformer())->transform($result);
		});
	}

	/**
	 * @api {PUT} appointments/{appointmentId}/location Update home visit appointment location
	 * @apiDescription Updates home visit appointment location with client location
	 * @apiName ajax-appointment-change-location
	 * @apiGroup Appointments
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} appointmentId Appointment ID
	 * @apiParam {Number} location_id Appointment ID
	 *
	 * @param $appointmentId
	 * @param $request
	 * @return mixed
	 */
	public function changeAppointmentLocation($appointmentId, ApiRequest $request)
	{
		$data = [
			'location_id' => $request->get('location_id'),
			'appointment_id' => $appointmentId,
		];

		return $this->exec(new ChangeAppointmentLocationCommand($data));
	}
}

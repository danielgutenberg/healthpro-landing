<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use App\Http\Requests\Request;
use WL\Modules\Taxonomy\LoadTaxonomyTagsCommand;
use WL\Modules\Taxonomy\TagTransformer;

/**
 * Class TaxonomyApiController
 * @package App\Http\Controllers\Api
 */
class TaxonomyApiController extends ApiController
{

	/**
	 * @api {get} /core/taxonomy/:taxonomySlug/tags Get Taxonomy Tags
	 * @apiDescription Get tags for a given taxonomy.
	 * @apiName ajax-core-taxonomy-tags
	 * @apiGroup Core-Tags
	 *
	 * @apiParam {String} taxonomySlug Slug for taxonomy to get tags for.
	 * @apiParam (Get parameters) {String} [sort] Field to sort by
	 *
	 * @apiVersion 1.0.0
	 */
	/**
	 * Returns Taxonomy tags
	 * @param $taxonomySlug
	 * @return mixed
	 */

	public function getTags(ApiRequest $request, $taxonomySlug)
	{
		return $this->exec(new LoadTaxonomyTagsCommand(
			$taxonomySlug,
			$request->get('loadAllTags', true),
			$request->get('sort', null)),
			function ($result) {
				return (new TagTransformer())->transformCollection($result);
			}
		);
	}


}

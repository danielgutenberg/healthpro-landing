<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use Illuminate\Http\Request;
use WL\Modules\Rating\Commands\GetEntityRatingCommand;
use WL\Modules\Rating\Commands\GetEntityRatingsCommand;
use WL\Modules\Rating\Commands\GetOverallEntityRatingsCommand;
use WL\Modules\Rating\Commands\RateEntityCommand;
use WL\Modules\Rating\Commands\RateEntityRatingsCommand;
use WL\Modules\Rating\Transformers\OverallRatingTransformer;
use WL\Modules\Rating\Transformers\RatingTransformer;

class RatingApiController extends ApiController
{

	/**
	 * @api {post} rating/:entityType/:entityId/:ratingLabel Rate Entity
	 * @apiDescription Rate some entity
	 * @apiName api-rating-rate
	 * @apiGroup Ratings
	 *
	 * @apiParam {Integer} entity_id
	 * @apiParam {String} entity_type
	 * @apiParam {String} [rating_type] if rating_label has record in config.wl.rating.ratings type is optional
	 * @apiParam {String} rating_label
	 * @apiParam {Numeric} rating_value
	 * @apiVersion 1.0.0
	 */
	public function rateEntity(ApiRequest $request, $entityType, $entityId, $ratingLabel)
	{
		$inpData = [
			'entity_id' => $entityId,
			'entity_type' => $entityType,
			'rating_label' => $ratingLabel
		];
		$inpData = array_merge($inpData, $request->all());

		$data = $this->runCommandWithErrorHandle(new RateEntityCommand($inpData));

		return $this->respondOk($data);
	}


	/**
	 * @api {post} rating/:entityType/:entityId/:ratingLabel Rate Entity Multiple Ratings
	 * @apiDescription Rate multiple ratings related to Entity
	 * @apiName api-rating-rate-multiple
	 * @apiGroup Ratings
	 *
	 * @apiParam {Integer} entity_id
	 * @apiParam {String} entity_type
	 * @apiParam {Array}  ratings
	 * @apiParam    {String} [rating_type] if rating_label has record in config.wl.rating.ratings type is optional
	 * @apiParam    {String} rating_label
	 * @apiParam    {Numeric} rating_value
	 * @apiVersion 1.0.0
	 */
	public function rateEntityRatings(ApiRequest $request, $entityType, $entityId)
	{
		$inpData = [
			'entity_id' => (integer)$entityId,
			'entity_type' => $entityType,
		];
		$inpData = array_merge($inpData, $request->all());

		$data = $this->runCommandWithErrorHandle(new RateEntityRatingsCommand($inpData));

		return $this->respondOk($data);
	}


	/**
	 * @api {get} rating/:entityType:/entityId/:ratingLabel Gets specified rating
	 * @apiDescription Gets specified rating by rating_label
	 * @apiName api-rating-get
	 * @apiGroup Ratings
	 *
	 * @apiParam {Integer} entity_id
	 * @apiParam {String} entity_type
	 * @apiParam {String} rating_label
	 * @apiVersion 1.0.0
	 */
	public function getEntityRating(ApiRequest $request, $entityType, $entityId, $ratingLabel)
	{
		$inpData = [
			'entity_id' => (integer)$entityId,
			'entity_type' => $entityType,
			'rating_label' => $ratingLabel
		];

		$data = $this->runCommandWithErrorHandle(new GetEntityRatingCommand($inpData));

		return $this->respondOk((new RatingTransformer())->transform($data));
	}

	/**
	 * @api {get} rating/:entityType/:entityId Get All Entity Ratings
	 * @apiDescription Receive all ratings associated with Entity
	 * @apiName api-rating-get-ratings
	 * @apiGroup Ratings
	 *
	 * @apiParam {Integer} entity_id
	 * @apiParam {String} entity_type
	 * @apiVersion 1.0.0
	 */
	public function getEntityRatings(ApiRequest $request, $entityType, $entityId)
	{
		$inpData = [
			'entity_id' => $entityId,
			'entity_type' => $entityType,
		];

		$data = $this->runCommandWithErrorHandle(new GetEntityRatingsCommand($inpData));

		return $this->respondOk((new RatingTransformer())->transformCollection($data));
	}


	/**
	 * @api {get} rating/:entityType/:entityId/overall/:ratingType Get Overall rating
	 * @apiDescription Receive Entity overall rating by type
	 * @apiName api-rating-get-ratings
	 * @apiGroup Ratings
	 *
	 * @apiParam {Integer} entity_id
	 * @apiParam {String} entity_type
	 * @apiParam {String} rating_type
	 * @apiVersion 1.0.0
	 */
	public function getOverallEntityRatings(ApiRequest $request, $entityType, $entityId, $ratingType)
	{
		$inpData = [
			'entity_id' => $entityId,
			'entity_type' => $entityType,
			'rating_type' => $ratingType
		];

		$data = $this->runCommandWithErrorHandle(new GetOverallEntityRatingsCommand($inpData));

		return $this->respondOk((new OverallRatingTransformer())->transform($data));
	}

}

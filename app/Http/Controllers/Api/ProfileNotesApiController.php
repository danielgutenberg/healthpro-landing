<?php namespace App\Http\Controllers\Api;


use App\Http\Requests\ApiRequest;
use WL\Modules\ProfileNotes\Commands\CreateNoteCommand;
use WL\Modules\ProfileNotes\Commands\DeleteNoteCommand;
use WL\Modules\ProfileNotes\Commands\LoadNotesCommand;
use WL\Modules\ProfileNotes\Commands\UpdateNoteCommand;
use WL\Modules\ProfileNotes\Transformers\NoteTransformer;

class ProfileNotesApiController extends ApiController
{

	/**
	 * @api {get} profiles/:profileId/notes/:elmType/:elmId
	 * @apiDescription Get profile notes
	 * @apiName ajax-profile-notes-get
	 * @apiGroup Profiles
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} profileId Profile ID
	 * @apiParam {String} elmType Element Type
	 * @apiParam {Number} elmId Element ID
	 */
	public function getNotes($profileId, $elmType, $elmId, ApiRequest $request)
	{
		return $this->exec(new LoadNotesCommand($profileId, $elmType, $elmId, null, $request->all()), function($result){
			return (new NoteTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {post} profiles/:profileId/notes/:elmType/:elmId
	 * @apiDescription Create client personal note
	 * @apiName ajax-profile-notes-create
	 * @apiGroup Profiles
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} profileId Profile ID
	 * @apiParam {String} elmType Element Type
	 * @apiParam {Number} elmId Element ID
	 * @apiParam {String} text Note text
	 * @apiParam {Datetime} noteDate note date
	 */
	public function createNote($profileId, $elmType, $elmId, ApiRequest $request)
	{
		return $this->exec(new CreateNoteCommand($profileId, $elmType, $elmId, null, $request->all()));
	}

	/**
	 * @api {put} profiles/:profileId/notes/:elmType/:elmId
	 * @apiDescription Update client personal note
	 * @apiName ajax-profile-notes-update
	 * @apiGroup Profiles
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} profileId Profile ID
	 * @apiParam {String} elmType Element Type
	 * @apiParam {Number} elmId Element ID
	 * @apiParam {Number} noteId Note ID
	 * @apiParam {String} text Note text
	 * @apiParam {Datetime} noteDate note date
	 */
	public function updateNote($profileId, $elmType, $elmId, $noteId, ApiRequest $request)
	{
		return $this->exec(new UpdateNoteCommand($profileId, $elmType, $elmId, $noteId, $request->all()));
	}

	/**
	 * @api {delete} profiles/:profileId/notes/:elmType/:elmId
	 * @apiDescription Delete profile note
	 * @apiName ajax-profile-notes-delete
	 * @apiGroup Profiles
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} profileId Profile ID
	 * @apiParam {String} elmType Element Type
	 * @apiParam {Number} elmId Element ID
	 * @apiParam {Number} noteId Note ID
	 */
	public function deleteNote($profileId, $elmType, $elmId, $noteId, ApiRequest $request)
	{
		return $this->exec(new DeleteNoteCommand($profileId, $elmType, $elmId, $noteId, $request->all()));
	}
}

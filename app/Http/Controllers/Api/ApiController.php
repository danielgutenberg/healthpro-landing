<?php namespace App\Http\Controllers\Api;

use App\Commands\Command;
use App\Commands\Exceptions\CommandExecutionException;
use App\Exceptions\GenericException;
use App\Exceptions\GenericExceptionAuthorization;
use App\Exceptions\GenericExceptionValidation;
use Exceptions\HttpReportedException;
use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use WL\Controllers\ControllerBase;
use WL\Security\Facades\AuthorizationUtils;
use WL\Security\Services\Exceptions\AuthorizationException;

/**
 * Base API controller
 * Class ApiController
 * @package App\Http\Controllers\Api
 */
class ApiController extends ControllerBase
{
	use DispatchesJobs;

	/**
	 * Dispatch helper. Execute from start to respond with exception handling.
	 * @param $command Command|array<Command>
	 * @param $callable callable
	 * @return mixed
	 */
	protected function exec($command, $callable = null)
	{
		$result = null;
		$dispatchResults = null;
		if (!is_array($command))
			$command = [$command];

		$commands = $command;

		foreach ($commands as $command) {
			$dispatchResult = $this->runCommandWithErrorHandle($command);
			if (isset($dispatchResult)) {
				$dispatchResults[] = $dispatchResult;
			}
		}

		if (isset($dispatchResults) && count($dispatchResults) == 1) {
			$dispatchResults = $dispatchResults[0];
		}

		$result = $dispatchResults;
		if (is_callable($callable) && (!empty($dispatchResults))) {
			$result = $callable($dispatchResults);
		}

		return $this->respondOk($result);
	}

	/**
	 * Respond with not found error
	 * @param string $message string
	 * @return mixed
	 */
	protected function respondNotFound($message = 'Not Found')
	{
		return $this->respondWithError($message, 404);
	}

	/**
	 * Respond with some error
	 * @param $data string
	 * @param $httpCode integer
	 * @return mixed
	 */
	protected function respondOk($data = null, $httpCode = 200)
	{
		return $this->respond('success', $data, null, $httpCode);
	}

	/**
	 * Respond with some error
	 * @param $errors string|array
	 * @param $httpCode integer
	 * @param null $statusCode integer
	 * @return mixed
	 */
	protected function respondWithValidationError(MessageBag $errors, $httpCode = 422, $statusCode = null)
	{
		$resultErrors = [];
		if ($errors instanceof MessageBag) {
			foreach ($errors->getMessages() as $key => $msg) {
				$resultErrors[$key] = [
					'messages' => (is_array($msg) ? $msg : [$msg]),
					'status_code' => 'validation'
				];
			}
		}

		return $this->respondWithError($resultErrors, $httpCode, $statusCode);
	}

	/**
	 * Respond with some error
	 * @param $messages string|array
	 * @param $httpCode integer
	 * @param null $statusCode integer
	 * @return mixed
	 */
	protected function respondWithError($messages, $httpCode, $statusCode = null)
	{
		$errors = [];

		if (is_array($messages)) {
			$errors = $messages;
		} else {
			$errors['error'] = ['messages' => [$messages]];
			if ($statusCode) {
				$errors['error']['status_code'] = $statusCode;
			}
		}

		return $this->respond('error', null, $errors, $httpCode);
	}

	/**
	 * Basic respond
	 * @param $message string
	 * @param $data array
	 * @param $errors array
	 * @param int $httpCode integer
	 * @param array $headers array
	 * @return mixed
	 */
	protected function respond($message, $data, $errors, $httpCode = 200, $headers = [])
	{
		$responseArr = [];

		$responseArr['message'] = $message;

		if (isset($data))
			$responseArr['data'] = $data;

		if (isset($errors))
			$responseArr['errors'] = $errors;

		return Response::json($responseArr, $httpCode, $headers);
	}


	/**
	 * @param $command
	 * @return mixed|null
	 * @throws \Exception
	 */
	protected function runCommandWithErrorHandle($command)
	{
		try {
			return $this->dispatch($command);

		} catch (AuthorizationException $e) {
		    $message = null;
			if (app()->environment('production', 'staging')) {
                $loggedIn = AuthorizationUtils::isLoggedIn();
                $message = __($loggedIn ? 'You do not have sufficient permissions.' : 'You must be logged in to do that.');
			} else {
				$message = $e->getMessage();
			}
            $response = $this->respondWithError($message, 401);
		} catch (ModelNotFoundException $e) {
			$response = $this->respondNotFound();
		} catch (ValidationException $e) {
            $response = $this->respondWithValidationError($e->errors());
		} catch (CommandExecutionException $e) {
			$response = $this->respondWithError($e->getMessage(), 500); //['message' => $ce->getMessage(), 'data' => $ce->getData()], 500);
		} catch (GenericExceptionValidation $e) {
			$response = $this->respondWithValidationError($e->messages);
		} catch (GenericExceptionAuthorization $e) {
			$response = $this->respondWithError($e->getMessage(), 401);
		} catch (GenericException $e) {
			$response = $this->respondWithError($e->getMessage(), 500);
		} catch (\Exception $e) {
            $response = $this->respondWithError($e->getMessage(), 500);
		}
		if(isset($response)) {
		    Log::error($e);
            throw new HttpResponseException($response);
        }
	}


	/**
	 * @param MessageBag $errors
	 * @param int $httpCode
	 * @param null $statusCode
	 * @return mixed
	 */
	public static function generateApiValidationErrorResponse(MessageBag $errors, $httpCode = 422, $statusCode = null)
	{
		return (new ApiController())->respondWithValidationError($errors, $httpCode, $statusCode);
	}

	/**
	 * @param $message
	 * @param int $httpCode
	 * @param null $statusCode
	 * @return mixed
	 */
	public static function generateApiErrorResponse($message, $httpCode = 500, $statusCode = null)
	{
		return (new ApiController())->respondWithError($message, $httpCode, $statusCode);
	}

	/**
	 * @param $message
	 * @param $data
	 * @param $errors
	 * @param int $httpCode
	 * @param array $headers
	 * @return mixed
	 */
	public static function generateApiResponse($message, $data, $errors, $httpCode = 200, $headers = [])
	{
		return (new ApiController())->respond($message, $data, $errors, $httpCode, $headers);
	}

	protected function isApiCall($route)
	{
		return starts_with($route->getName(), 'api-');
	}

	protected function isAjaxCall($route)
	{
		return starts_with($route->getName(), 'ajax-');
	}
}

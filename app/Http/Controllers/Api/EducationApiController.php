<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use Illuminate\Http\Request;
use WL\Modules\Education\Commands\SearchEducationCommand;
use WL\Modules\Education\Transformers\EducationTransformer;


/**
 * Class EducationApiController
 * @package App\Http\Controllers\Api
 */
class EducationApiController extends ApiController
{

	/**
	 * @api {get} /core/educations/search Educations
	 * @apiDescription Search educations for auto complete.
	 * @apiName ajax-core-educations-search
	 * @apiGroup Core
	 *
	 * @apiParam {String} q The text to search Education name's for.
	 *
	 * @apiSuccess {String} message "success"
	 * @apiSuccess {Object[]} data
	 * @apiSuccess {Number} data.id Education Db Name
	 * @apiSuccess {String} data.name Education Institution's Name
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *	{
	 *		"message": "success",
	 *		"data": [
	 *			{
	 *				"id": 12,
	 *				"name": "Aaron's Academy of Beauty"
	 *			}
	 *		]
	 *	}
	 * @apiVersion 1.0.0
	 */

	/*
	 * Search Educations for auto complete
	 *
	 * @param Request $request
	 * @return mixed
	 */
	public function searchEducations(ApiRequest $request)
	{

		return $this->respondOk(
			(new EducationTransformer())->transformCollection(
				$this->dispatch(new SearchEducationCommand($request->all()))
			)
		);
	}


}

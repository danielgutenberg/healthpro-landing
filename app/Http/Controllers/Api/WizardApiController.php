<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Wizard\Commands\GetStartedWizardsCommand;
use WL\Wizard\Commands\LoadWizardCommand;
use WL\Wizard\Commands\StatusWizardCommand;
use WL\Wizard\Commands\StoreFrontWizardCommand;
use WL\Wizard\Exceptions\ValidationWizardException;

class WizardApiController extends ApiController
{

	/**
	 *
	 * Render wizard .
	 *
	 * @param Request $request
	 * @param string $wizard
	 * @param array $step
	 * @return mixed
	 *
	 * @api {GET} /wizard/:slug/:step Get wizard info by slug and step.
	 * @apiDescription  Get wizard info by slug and step.
	 * @apiName api-wizard-step-get
	 * @apiGroup Wizard
	 *
	 * @apiParam {String} slug The wizard slug must be set here.
	 * @apiParam {String} step The wizard step must be set here.
	 */
	public function wizardStep($wizard, $step)
	{
		$info = $this->runCommandWithErrorHandle(new StatusWizardCommand($wizard, $step));

		return $this->respondOk([
				'profileId' => ProfileService::getCurrentProfileId(),
				'_token' => csrf_token(),
			] + $info);
	}

	/**
	 * Get next step info ..
	 *
	 * @param $wizard
	 * @return mixed
	 */
//	public function next($wizard)
//	{
//		try {
//			$step = app('wizard')
//				->getWizardCurrentPage($wizard);
//
//			$info = $this->dispatch(
//				new StatusWizardCommand($wizard, $step->getSlug())
//			);
//
//			return $this->respondOk([
//					'profile' => ProfileService::getCurrentProfileId(),
//					'_token' => csrf_token(),
//				] + $info);
//
//		} catch (ValidationWizardException $e) {
//
//			return $this->respondWithValidationError($e->getMessage());
//		}
//	}

	/**
	 * Render wizard .
	 * @param Request $request
	 * @param string $wizard
	 * @return mixed
	 * @internal param array $step
	 *
	 * @api {GET} /wizard/:slug Get wizard info by slug.
	 * @apiDescription  Get wizard info by slug.
	 * @apiName api-wizard-get
	 * @apiGroup Wizard
	 *
	 * @apiParam {String} slug The wizard slug must be set here.
	 * @apiParam {Boolean} resetToFirstStep reset stored wizard step to first
	 */
	public function wizard(Request $request, $wizard)
	{

		$info = $this->runCommandWithErrorHandle(new LoadWizardCommand($wizard, $request->get('resetToFirstStep', null)));
		return $this->respondOk([
				'profile' => ProfileService::getCurrentProfileId(),
				'_token' => csrf_token(),
			] + $info);

	}


	/**
	 * Associate front-end data with wizard .
	 *
	 * @api {POST} /wizard/:wizardSlug Associate front-end data.
	 * @apiDescription  Store front-end data.
	 * @apiName api-wizard-store-front-data
	 * @apiGroup Wizard
	 *
	 * @apiParam {String} wizardSlug The wizard slug must be set here.
	 * @apiParam {String} data your data here
	 */
	public function storeFrontData(Request $request, $wizard)
	{
		$inpData = [
			'data' => $request->get('data', null),
			'slug' => $wizard
		];

		$result = $this->runCommandWithErrorHandle(new StoreFrontWizardCommand($inpData));

		return $this->respondOk($result);
	}

	/**
	 * Returns wizards
	 *
	 * @api {GET} /wizards
	 * @apiDescription  returns wizards states
	 * @apiName api-wizard-get-started
	 * @apiGroup Wizard
	 */
	public function startedWizards()
	{
		$result = $this->runCommandWithErrorHandle(new GetStartedWizardsCommand());

		return $this->respondOk($result);
	}
}

<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use WL\Modules\Appointment\Commands\GetClientHistoryCommand;
use WL\Modules\Appointment\Transformers\ClientHistoryTransformer;
use WL\Modules\PricePackage\Commands\GetClientPackageHistoryCommand;
use WL\Modules\PricePackage\Transformers\FullPurchasedPricePackageTransformer;
use WL\Modules\Profile\Commands\CanInviteProviderClientCommand;
use WL\Modules\Profile\Commands\ClientMeta\ProviderClientMetaGetCommand;
use WL\Modules\Profile\Commands\ClientMeta\ProviderClientMetaToggleCommand;
use WL\Modules\Profile\Commands\GetProfileClientsCommand;
use WL\Modules\Profile\Commands\GetProfileClientsInvitesCommand;
use WL\Modules\Profile\Commands\Invitations\InviteProviderClientCommand;
use WL\Modules\Profile\Commands\Invitations\ReinviteProviderClientCommand;
use WL\Modules\Profile\Transformers\ProviderClientInviteTransformer;
use WL\Modules\Profile\Transformers\ProviderClientProfileTransformer;
use WL\Modules\Profile\Transformers\ProviderImportTransformer;
use WL\Modules\Profile\Transformers\PublicProfileTransformer;

class ProviderClientsApiController extends ApiController
{
	/**
	 * @api {get} providers/:providerId/clients Provider Clients Get
	 * @apiDescription Provides the list of all the previous clients of the provider
	 * @apiName ajax-provider-clients-get
	 * @apiGroup Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {String} return_fields [all|basic] defines what fields should be return in response. Default value is `basic`.
	 * @apiParam {Number} providerId Provider id
	 *
	 * @param $providerId
	 * @param ApiRequest $request
	 * @return mixed
	 */
	public function getProviderClients($providerId, ApiRequest $request)
	{
		$returnFields = $request->input('return_fields', 'basic');

		$data = [
			'profile_id' => $providerId,
			'return_fields' => $returnFields,
		];

		return $this->exec(new GetProfileClientsCommand($data), function ($result) use ($returnFields) {
			return $returnFields === 'basic'
				? (new PublicProfileTransformer())->transformCollection($result)
				: (new ProviderClientProfileTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {get} providers/:providerId/invites Provider Invites Get
	 * @apiDescription Provides the list of all pending client invitations
	 * @apiName ajax-provider-invites-get
	 * @apiGroup Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} providerId Provider id
	 *
	 * @param $providerId
	 * @return mixed
	 */
	public function getProviderClientInvites($providerId)
	{
		$data = [
			'profile_id' => $providerId,
		];

		return $this->exec(new GetProfileClientsInvitesCommand($data), function ($result) {
			return (new ProviderClientInviteTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {put} providers/:providerId/clients/:clientId/invite Invites Client to your client list
	 * @apiDescription Resend invitation to existing client
	 * @apiName ajax-provider-client-reinvite
	 * @apiGroup Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Number} clientId Client ID
	 *
	 */
	public function reinviteClient($providerId, $clientId)
	{
		$data = [
			'provider_id' => $providerId,
			'client_id' => $clientId,
		];

		return $this->exec(new ReinviteProviderClientCommand($data), function ($result) {
			return (new ProviderClientInviteTransformer())->transform($result);
		});
	}

	/**
	 * @api {post} providers/:providerId/clients/:clientId/invite Invites Client to your client list
	 * @apiDescription Send invitation to existing client
	 * @apiName ajax-provider-client-invite
	 * @apiGroup Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Number} clientId Client ID
	 *
	 */
	public function inviteClient($providerId, $clientId)
	{
		$data = [
			'provider_id' => $providerId,
			'client_id' => $clientId,
		];

		return $this->exec(new InviteProviderClientCommand($data), function ($result) {
			return (new ProviderClientInviteTransformer())->transform($result);
		});
	}

	/**
	 * @api {put} providers/:providerId/clients/:clientId/meta/toggle
	 * @apiDescription Toggle provider-client personalize value
	 * @apiName ajax-provider-client-meta-toggle
	 * @apiGroup Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Number} clientId Client ID
	 * @apiParam {String} meta_key Meta field name
	 * @apiParam {Boolean} [force] Force value to meta key
	 */
	public function toggleClientMeta($providerId, $clientId, ApiRequest $request)
	{
		$data = $request->all();
		$data['provider_id'] = $providerId;
		$data['client_id'] = $clientId;

		return $this->exec(new ProviderClientMetaToggleCommand($data));
	}

	/**
	 * @api {get} providers/:providerId/clients/:clientId/meta
	 * @apiDescription Get provider-client personalize value
	 * @apiName ajax-provider-client-meta-get
	 * @apiGroup Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Number} clientId Client ID
	 * @apiParam {String} [fields] Meta fields names separated by comma
	 */
	public function getClientMeta($providerId, $clientId, ApiRequest $request)
	{
		$data = [
			'provider_id' => $providerId,
			'client_id' => $clientId,
			'fields' => [],
		];
		if($request->get('fields')){
			$data['fields'] = explode(',', $request->get('fields'));
		}
		return $this->exec(new ProviderClientMetaGetCommand($data));
	}

	/**
	 * @api {get} providers/:providerId/clients/:clientId/packages Provider Appointments History Get for Specific client
	 * @apiDescription Provides appointments history for the provider client.
	 * @apiName ajax-provider-client-history-get
	 * @apiGroup Appointments Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiParam {Number} clientId Client id
	 *
	 * @param $providerId
	 * @param $clientId
	 * @return mixed
	 */
	public function getClientPackageHistory($providerId, $clientId)
	{
		$data['provider_id'] = $providerId;
		$data['client_id'] = $clientId;

		return $this->exec(new GetClientPackageHistoryCommand($data), function ($result) {
			return (new FullPurchasedPricePackageTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {get} providers/:providerId/clients/:clientId/appointments Provider Appointments History Get for Specific client
	 * @apiDescription Provides appointments history for the provider client.
	 * @apiName ajax-provider-client-history-get
	 * @apiGroup Appointments Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiParam {Number} clientId Client id
	 *
	 * @param $providerId
	 * @param $clientId
	 * @return mixed
	 */
	public function getClientHistory($providerId, $clientId)
	{
		$data['provider_id'] = $providerId;
		$data['client_id'] = $clientId;

		return $this->exec(new GetClientHistoryCommand($data), function ($result) {
			return (new ClientHistoryTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {get} providers/:providerId/can-invite/:clientId/ Check if provider can invite the client
	 * @apiDescription Check if provider can invite the client
	 * @apiName ajax-provider-can-invite-client
	 * @apiGroup Providers
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiParam {Number} clientId Client id
	 *
	 * @param $providerId
	 * @param $clientId
	 * @return mixed
	 */
	public function canInviteClient($providerId, $clientId)
	{
		$data['providerId'] = (int) $providerId;
		$data['clientId'] = (int) $clientId;

		return $this->exec(new CanInviteProviderClientCommand($data));
	}
}

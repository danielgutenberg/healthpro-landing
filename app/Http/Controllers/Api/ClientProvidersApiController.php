<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use WL\Modules\Profile\Commands\AddProviderClientCommand;
use WL\Modules\Profile\Commands\ConfirmClientInvitationCommand;
use WL\Modules\Profile\Commands\GetProfileProviderCommand;
use WL\Modules\Profile\Commands\GetProfileProvidersCommand;
use WL\Modules\Profile\Commands\GetProfileProvidersInvitesCommand;
use WL\Modules\Profile\Commands\GetProviderClientStatusCommand;
use WL\Modules\Profile\Transformers\ClientProviderProfileTransformer;
use WL\Modules\Profile\Transformers\ProviderClientInviteStatusTrasformer;
use WL\Modules\Profile\Transformers\PublicProfileTransformer;

class ClientProvidersApiController extends ApiController
{
	/**
	 * @api {get} clients/:clientId/providers Get Client Providers
	 * @apiDescription Provides the list of all the professionals for the client
	 * @apiName ajax-client-providers-get
	 * @apiGroup Clients
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} clientId Client ID
	 *
	 * @param $clientId
	 * @param ApiRequest $request
	 * @return mixed
	 */
	public function getClientProviders($clientId, ApiRequest $request)
	{
		$returnFields = $request->input('return_fields', 'basic');

		$data = [
			'profile_id' => $clientId,
			'return_fields' => $returnFields,
		];

		return $this->exec(new GetProfileProvidersCommand($data), function ($result) use ($returnFields) {
			return $returnFields === 'basic'
				? (new PublicProfileTransformer())->transformCollection($result)
				: (new ClientProviderProfileTransformer())->transformCollection($result);

		});
	}

	/**
	 * @api {get} clients/:clientId/invites Get Client Provider Invitations
	 * @apiDescription Provides the list of all the professional invitation for the client
	 * @apiName ajax-client-invites-get
	 * @apiGroup Clients
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} clientId Client ID
	 *
	 * @param $clientId
	 * @return mixed
	 */
	public function getClientInvites($clientId)
	{
		$data = [
			'profile_id' => $clientId,
		];

		return $this->exec(new GetProfileProvidersInvitesCommand($data), function ($result) {
			return (new PublicProfileTransformer())->transformCollection($result);

		});
	}

	/**
	 * @api {get} clients/:clientId/providers/:providerId Get Client Provider
	 * @apiDescription Provides the data of the selected professional for the client
	 * @apiName ajax-client-provider-get
	 * @apiGroup Clients
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} clientId Client ID
	 * @apiParam {Number} providerId Provider ID
	 *
	 * @param $clientId
	 * @param $providerId
	 * @param ApiRequest $request
	 * @return mixed
	 */
	public function getClientProvider($clientId, $providerId, ApiRequest $request)
	{
		$returnFields = $request->input('return_fields', 'basic');

		$data = [
			'profile_id' => $clientId,
			'provider_id' => $providerId,
			'return_fields' => $returnFields,
		];

		return $this->exec(new GetProfileProviderCommand($data), function ($result) use ($returnFields) {
			return $returnFields === 'basic'
				? (new PublicProfileTransformer())->transform($result)
				: (new ClientProviderProfileTransformer())->transform($result);

		});
	}

	/**
	 * @api {put} clients/:clientId/providers/:providerId Update Client Provider invitation
	 * @apiDescription Approve or decline professional invitation
	 * @apiName ajax-client-provider-confirm
	 * @apiGroup Clients
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} clientId Client ID
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Boolean} decline Decline invitation
	 *
	 * @param $clientId
	 * @param $providerId
	 * @param ApiRequest $request
	 * @return mixed
	 */
	public function confirmClientProvider($clientId, $providerId, ApiRequest $request)
	{
		$data = [
			'clientId' => $clientId,
			'ProviderId' => $providerId,
			'approve' => $request->input('decline') !== '1',
		];
		$this->exec(new ConfirmClientInvitationCommand($data));
		return $this->getProviderInviteStatus($clientId, $providerId);
	}

	/**
	 * @api {post} clients/:clientId/providers/:providerId/add
	 * @apiDescription Add professional to My Professionals list
	 * @apiName ajax-client-provider-add
	 * @apiGroup Clients
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} [clientId] Client ID
	 * @apiParam {Number} providerId Provider ID
	 *
	 */
	public function addProvider($clientId, $providerId)
	{
		$data = [
			'providerId' => $providerId,
			'clientId' => $clientId,
		];

		$this->exec(new AddProviderClientCommand($data));
		return $this->getProviderInviteStatus($clientId, $providerId);
	}

	/**
	 * @api {get} clients/:clientId/providers/:providerId/status
	 * @apiDescription Get professional invite status - not_invited/invited/approved/decline
	 * @apiName ajax-client-provider-status
	 * @apiGroup Clients
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiParam {Number} [clientId] Client ID
	 * @apiParam {Number} providerId Provider ID
	 *
	 */
	public function getProviderInvite($clientId, $providerId)
	{
		return $this->getProviderInviteStatus($clientId, $providerId);
	}

	private function getProviderInviteStatus($clientId, $providerId)
	{
		$data = [
			'providerId' => $providerId,
			'clientId' => $clientId,
		];

		return $this->exec(new GetProviderClientStatusCommand($data), function($result){
			return (new ProviderClientInviteStatusTrasformer())->transform($result);
		});
	}
}

<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use Illuminate\Http\Request;
use WL\Modules\Payment\Commands\AddBankAccountCommand;
use WL\Modules\Payment\Commands\AddCardCommand;
use WL\Modules\Payment\Commands\AddCreditToProfileCommand;
use WL\Modules\Payment\Commands\GetAccountCreditBalanceCommand;
use WL\Modules\Payment\Commands\GetAccountInfoCommand;
use WL\Modules\Payment\Commands\GetAccountVerificationCommand;
use WL\Modules\Payment\Commands\GetBankAccountCommand;
use WL\Modules\Payment\Commands\GetBankAccountsCommand;
use WL\Modules\Payment\Commands\GetCardCommand;
use WL\Modules\Payment\Commands\GetCardsCommand;
use WL\Modules\Payment\Commands\GetPaymentMethodsCommand;
use WL\Modules\Payment\Commands\GetPaymentOptionsCommand;
use WL\Modules\Payment\Commands\RefundPaymentOperationCommand;
use WL\Modules\Payment\Commands\RemoveBankAccountCommand;
use WL\Modules\Payment\Commands\RemoveCardCommand;
use WL\Modules\Payment\Commands\RemovePaypalCommand;
use WL\Modules\Payment\Commands\RemoveStripeCommand;
use WL\Modules\Payment\Commands\SavePaymentOptionToSession;
use WL\Modules\Payment\Commands\UpdateAccountInfoCommand;
use WL\Modules\Payment\Commands\UpdateAccountVerificationCommand;
use WL\Modules\Payment\Commands\UpdateCardCommand;
use WL\Modules\Payment\Facades\PaymentMethodService;
use WL\Modules\Payment\Paypal\RestApiGateway;
use WL\Modules\Payment\Transformers\BankAccountTransformer;
use WL\Modules\Payment\Transformers\CreditCardTransformer;
use WL\Modules\Payment\Transformers\PaymentMethodsTransformer;

class PaymentApiController extends ApiController
{
	/**
	 * @api {post} /profiles/:profileId/credit_cards Credit Card Add
	 * @apiDescription Adds Credit Card to profile.
	 * @apiName ajax-credit-card-add
	 * @apiGroup Profile Credit Cards
	 *
	 * @apiParam {Number} profileId Profile id
	 * @apiParam (Post parameters) {Number} number Credit Card number (visa or mastercard)
	 * @apiParam (Post parameters) {Number{2}} expiryMonth Expiry Month
	 * @apiParam (Post parameters) {Number{4}} expiryYear Expiry Year
	 * @apiParam (Post parameters) {Number} cvv CVC, CVV
	 * @apiParam (Post parameters) {String} name Card Holder
	 * @apiParam (Post parameters) {String} type Credit or Debit
	 * @apiParam (Post parameters) {String} legal_entity Legal entity
	 * @apiParam (Post parameters) {String} legal_entity.first_name "Michael"
	 * @apiParam (Post parameters) {String} legal_entity.last_name Legal entity "Cors"
	 * @apiParam (Post parameters) {String} legal_entity.dob.day day (1-31)
	 * @apiParam (Post parameters) {String} legal_entity.dob.month month (1-12)
	 * @apiParam (Post parameters) {String} legal_entity.dob.year year 4-digit (2015)
	 * @apiParam (Post parameters) {String} tos_accepted User accept TOS of Payment Gateway (true|false|null)
	 * @apiParamExample [{hash}] [legal_entity]
	 *      {
	 * first_name: "Michael"
	 *          last_name: "Cors"
	 *          type: individual|company
	 *          dob: {
	 * day: (1-31)
	 *                  month: (1-12)
	 *                  year: (2015)
	 *              }
	 *       }
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"gateway_token":"1","card_number":1,"card_type":"visa","exp_month":10,"exp_year":2017,"account_id":1,"updated_at":"2015-06-03 08:07:00","created_at":"2015-06-03 08:07:00","id":6}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function addCreditCard($profileId, ApiRequest $request)
	{
		$data = $request->all();
		$data['profile_id'] = $profileId;
		$data['ip'] = $request->ip();
		$data['user_agent'] = $request->header('User-Agent');

		return $this->exec(new AddCardCommand($data), function ($result) {
			return (new CreditCardTransformer())->transform($result);
		});
	}

	/**
	 * @api {put} /profiles/:profileId/credit_cards/:creditCardId Credit Card Update
	 * @apiDescription Updates Credit Card.
	 * @apiName ajax-credit-card-update
	 * @apiGroup Profile Credit Cards
	 *
	 * @apiParam {Number} profileId Profile id
	 * @apiParam {Number} creditCardId Credit Card id
	 * @apiParam (Post parameters) {Number} number Credit Card number (visa or mastercard)
	 * @apiParam (Post parameters) {Number{2}} expiryMonth Expiry Month
	 * @apiParam (Post parameters) {Number{4}} expiryYear Expiry Year
	 * @apiParam (Post parameters) {Number} cvv CVC, CVV
	 * @apiParam (Post parameters) {String} name Card Holder
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"gateway_token":"1","card_number":1,"card_type":"visa","exp_month":10,"exp_year":2017,"account_id":1,"updated_at":"2015-06-03 08:07:00","created_at":"2015-06-03 08:07:00","id":6}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function updateCreditCard($profileId, $creditCardId, ApiRequest $request)
	{
		$data = $request->all();
		$data['profile_id'] = $profileId;
		$data['credit_card_id'] = $creditCardId;

		return $this->exec(new UpdateCardCommand($data), function ($result) {
			return (new CreditCardTransformer())->transform($result);
		});
	}

	/**
	 * @api {delete} /profiles/:profileId/credit_cards/:creditCardId Credit Card Delete
	 * @apiDescription Removes Credit Card.
	 * @apiName ajax-credit-card-delete
	 * @apiGroup Profile Credit Cards
	 *
	 * @apiParam {Number} profileId Profile id
	 * @apiParam {Number} creditCardId Credit Card id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function removeCreditCard($profileId, $creditCardId)
	{
		$data = [
			'profile_id' => $profileId,
			'credit_card_id' => $creditCardId,
		];

		return $this->exec(new RemoveCardCommand($data));
	}

	/**
	 * @api {delete} /profiles/:profileId/paypal/:paypalId Paypal Remove
	 * @apiDescription Removes Paypal option from payment
	 * @apiName ajax-paypal-delete
	 * @apiGroup Profile Paypal
	 *
	 * @apiParam {Number} profileId Profile id
	 * @apiParam {Number} paypalId Paypal id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function removePaypal($profileId, $paypalId)
	{
		$data = [
			'profile_id' => $profileId,
			'paypal_id' => $paypalId,
		];

		return $this->exec(new RemovePaypalCommand($data));
	}

	public function savePaymentOptionToSession(Request $request, $profileId)
	{
		$data = [
			'profile_id' => $profileId,
			'accepted_payment_methods' => $request->accepted_payment_methods,
		];

		return $this->exec(new SavePaymentOptionToSession($data));
	}

	/**
	 * @api {delete} /profiles/:profileId/stripe Disconnect Stripe
	 * @apiDescription Removes Stripe option from payment
	 * @apiName ajax-stripe-delete
	 * @apiGroup Profile Stripe
	 *
	 * @apiParam {Number} profileId Profile id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function removeStripe($profileId)
	{
		$data = [
			'profile_id' => $profileId
		];

		return $this->exec(new RemoveStripeCommand($data));
	}

	/**
	 * @api {get} /profiles/:profileId/credit_cards/:creditCardId Credit Card Get
	 * @apiDescription Loads Credit Card.
	 * @apiName ajax-credit-card-get
	 * @apiGroup Profile Credit Cards
	 *
	 * @apiParam {Number} profileId Profile id
	 * @apiParam {Number} creditCardId Credit Card id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"gateway_token":"1","can_delete"=>true,"card_number":1,"card_type":"visa","exp_month":10,"exp_year":2017,"account_id":1,"updated_at":"2015-06-03 10:28:27","created_at":"2015-06-03 10:28:27","id":8}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function getCreditCard($profileId, $creditCardId)
	{
		$data = [
			'profile_id' => $profileId,
			'credit_card_id' => $creditCardId
		];

		return $this->exec(new GetCardCommand($data), function ($result) {
			return (new CreditCardTransformer())->transform($result);
		});
	}

	/**
	 * @api {get} /profiles/:profileId/credit_cards/ Credit Cards Gets
	 * @apiDescription Loads Credit Cards.
	 * @apiName ajax-credit-cards-get
	 * @apiGroup Profile Credit Cards
	 *
	 * @apiParam {Number} profileId Profile id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":[{"gateway_token":"1","can_delete"=>true,"card_number":1,"card_type":"visa","exp_month":10,"exp_year":2017,"account_id":1,"updated_at":"2015-06-03 10:28:27","created_at":"2015-06-03 10:28:27","id":8}]}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function getCreditCards($profileId)
	{
		$data = ['profile_id' => $profileId];

		return $this->exec(new GetCardsCommand($data), function ($result) {
			return (new CreditCardTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {get} /profiles/:profileId/payment_methods/ Professional payment options
	 * @apiDescription Loads Payment Methods.
	 * @apiName ajax-payment-methods-get
	 * @apiGroup Profile Payment Methods
	 *
	 * @apiParam {Number} profileId Profile id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function getProfessionalsPaymentMethods($profileId)
	{
		$data = ['profile_id' => $profileId];

		return $this->exec(new GetPaymentOptionsCommand($data), function ($result) {
			return (new PaymentMethodsTransformer())->transform($result);
		});
	}

	/**
	 * @api {post} /profiles/:profileId/bank_accounts Bank Account Add
	 * @apiDescription Adds Bank Account to profile.
	 * @apiName ajax-bank-account-add
	 * @apiGroup Profile Bank Accounts
	 *
	 * @apiParam {Number} profileId Profile id
	 * @apiParam (Post parameters) {Number} account_number Bank Account number
	 * @apiParam (Post parameters) {Number} [routing_number] Routing number (sometimes required see stripe docs)
	 * @apiParam (Post parameters) {String} country 2 symbol Country code ex. "us"
	 * @apiParam (Post parameters) {String} currency currency code
	 * @apiParam (Post parameters) {String} legal_entity.first_name "Michael"
	 * @apiParam (Post parameters) {String} legal_entity.last_name Legal entity "Cors"
	 * @apiParam (Post parameters) {String} legal_entity.dob.day day (1-31)
	 * @apiParam (Post parameters) {String} legal_entity.dob.month month (1-12)
	 * @apiParam (Post parameters) {String} legal_entity.dob.year year 4-digit (2015)
	 * @apiParam (Post parameters) {String} tos_accepted User accept TOS of Payment Gateway (true|false|null)
	 * @apiParamExample [{hash}] [legal_entity]
	 *      {
	 * first_name: "Michael"
	 *          last_name: "Cors"
	 *          type: individual|company
	 *          dob: {
	 * day: (1-31)
	 *                  month: (1-12)
	 *                  year: (2015)
	 *              }
	 *       }
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"gateway_token":"1","account_number":1,"routing_number":"1","country":"us","currency":"usd","account_id":1,"updated_at":"2015-06-03 08:07:00","created_at":"2015-06-03 08:07:00","id":6}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function addBankAccount($profileId, ApiRequest $request)
	{
		$data = $request->all();
		$data['profile_id'] = $profileId;

		return $this->exec(new AddBankAccountCommand($data), function ($result) {
			return (new BankAccountTransformer())->transform($result);
		});
	}

	/**
	 * @api {get} /profiles/:profileId/bank_accounts/:bankAccountId Gets Bank Account
	 * @apiDescription Gets Bank Account
	 * @apiName ajax-bank-account-get
	 * @apiGroup Profile Bank Accounts
	 *
	 * @apiParam {Number} profileId Profile id
	 * @apiParam {Number} bankAccountId Bank Account id
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"gateway_token":"1","account_number":1,"can_delete"=>true, "routing_number":"1","country":"us","currency":"usd","account_id":1,"updated_at":"2015-06-03 08:07:00","created_at":"2015-06-03 08:07:00","id":6}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function getBankAccount($profileId, $bankAccountId)
	{
		$data['profile_id'] = $profileId;
		$data['bank_account_id'] = $bankAccountId;

		return $this->exec(new GetBankAccountCommand($data), function ($result) {
			return (new BankAccountTransformer())->transform($result);
		});
	}

	/**
	 * @api {get} /profiles/:profileId/bank_accounts/ Gets Bank Accounts
	 * @apiDescription Gets Bank Accounts
	 * @apiName ajax-bank-accounts-get
	 * @apiGroup Profile Bank Accounts
	 *
	 * @apiParam {Number} profileId Profile id
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":[{"gateway_token":"1","account_number":1,"can_delete"=>true, "routing_number":"1","country":"us","currency":"usd","account_id":1,"updated_at":"2015-06-03 08:07:00","created_at":"2015-06-03 08:07:00","id":6}]}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function getBankAccounts($profileId)
	{
		$data['profile_id'] = $profileId;

		return $this->exec(new GetBankAccountsCommand($data), function ($result) {
			return (new BankAccountTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {delete} /profiles/:profileId/bank_accounts/:bankAccountId Delete Bank Account
	 * @apiDescription Delete Bank Account
	 * @apiName ajax-bank-accounts-delete
	 * @apiGroup Profile Bank Accounts
	 *
	 * @apiParam {Number} profileId Profile id
	 * @apiParam {Number} bankAccountId Bank Account id
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function removeBankAccount($profileId, $bankAccountId)
	{
		$data = [
			'profile_id' => $profileId,
			'bank_account_id' => $bankAccountId
		];

		return $this->exec(new RemoveBankAccountCommand($data));
	}

	/**
	 * @api {get} /profiles/:profileId/payment_account/verification_status Get account status including fields needed
	 * @apiDescription Get Account verification status
	 * @apiName ajax-payment-verification-get
	 * @apiGroup Profile Payment Account
	 *
	 * @apiParam {Number} profileId Profile id
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"status":"unverified","fields_needed":["legal_entity.dob.day","legal_entity.dob.month","legal_entity.dob.year","legal_entity.first_name","legal_entity.last_name","legal_entity.type","tos_acceptance.date","tos_acceptance.ip"]}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function getAccountVerification($profileId)
	{
		$data = ['profile_id' => $profileId];

		return $this->exec(new GetAccountVerificationCommand($data));
	}

	/**
	 * @api {get} /profiles/:profileId/payment_account Get account info
	 * @apiDescription Get Account info
	 * @apiName ajax-payment-account-get
	 * @apiGroup Profile Payment Account
	 *
	 * @apiParam {Number} profileId Profile id
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"id":"some_id","object":"account","business_logo":null,"business_name":"some name","business_url":null,"charges_enabled":true,"country":"US","currencies_supported":["usd","aed","afn","all","amd","ang","aoa","ars","aud","awg","azn","bam","bbd","bdt","bgn","bif","bmd","bnd","bob","brl","bsd","bwp","bzd","cad","cdf","chf","clp","cny","cop","crc","cve","czk","djf","dkk","dop","dzd","egp","etb","eur","fjd","fkp","gbp","gel","gip","gmd","gnf","gtq","gyd","hkd","hnl","hrk","htg","huf","idr","ils","inr","isk","jmd","jpy","kes","kgs","khr","kmf","krw","kyd","kzt","lak","lbp","lkr","lrd","lsl","ltl","mad","mdl","mga","mkd","mnt","mop","mro","mur","mvr","mwk","mxn","myr","mzn","nad","ngn","nio","nok","npr","nzd","pab","pen","pgk","php","pkr","pln","pyg","qar","ron","rsd","rub","rwf","sar","sbd","scr","sek","sgd","shp","sll","sos","srd","std","svc","szl","thb","tjs","top","try","ttd","twd","tzs","uah","ugx","uyu","uzs","vnd","vuv","wst","xaf","xcd","xof","xpf","yer","zar","zmw"],"debit_negative_balances":false,"decline_charge_on":{"avs_failure":false,"cvc_failure":false},"default_currency":"usd","details_submitted":false,"display_name":null,"email":null,"external_accounts":{"object":"list","data":[{"id":"card_17aBTrD1k5Mcx8EOQpGtTRdZ","object":"card","account":"acct_17aBTrD1k5Mcx8EO","address_city":"","address_country":"","address_line1":"","address_line1_check":null,"address_line2":"","address_state":"","address_zip":"","address_zip_check":null,"brand":"Visa","country":"US","currency":"usd","cvc_check":"pass","default_for_currency":true,"dynamic_last4":null,"exp_month":3,"exp_year":2017,"fingerprint":"utlioIx9BLdlR0rg","funding":"debit","last4":"5556","metadata":[],"name":"some name","tokenization_method":null}],"has_more":false,"total_count":1,"url":"\/v1\/accounts\/acct_17aBTrD1k5Mcx8EO\/external_accounts"},"legal_entity":{"additional_owners":null,"address":{"city":null,"country":"US","line1":null,"line2":null,"postal_code":null,"state":null},"business_name":"some name","business_tax_id_provided":false,"dob":{"day":null,"month":null,"year":null},"first_name":null,"last_name":null,"personal_address":{"city":null,"country":null,"line1":null,"line2":null,"postal_code":null,"state":null},"personal_id_number_provided":false,"ssn_last_4_provided":false,"type":"individual","verification":{"details":null,"details_code":null,"document":null,"status":"unverified"}},"managed":true,"metadata":[],"product_description":null,"statement_descriptor":null,"support_phone":null,"timezone":"Etc\/UTC","tos_acceptance":{"date":null,"ip":null,"user_agent":null},"transfer_schedule":{"delay_days":2,"interval":"daily"},"transfers_enabled":false,"verification":{"disabled_reason":"fields_needed","due_by":null,"fields_needed":["legal_entity.dob.day","legal_entity.dob.month","legal_entity.dob.year","legal_entity.first_name","legal_entity.last_name","tos_acceptance.date","tos_acceptance.ip"]}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function getAccountInfo($profileId)
	{
		$data = ['profile_id' => $profileId];

		return $this->exec(new GetAccountInfoCommand($data));
	}

	/**
	 * @api {put} /profiles/:profileId/payment_account Update account info
	 * @apiDescription Update Account info
	 * @apiName ajax-payment-account-put
	 * @apiGroup Profile Payment Account
	 *
	 * @apiParam {Number} profileId Profile id
	 * @apiParam {Object} account_info Account data to update as defined https://stripe.com/docs/api/curl#update_account
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"id":"some_id","object":"account","business_logo":null,"business_name":"some name","business_url":null,"charges_enabled":true,"country":"US","currencies_supported":["usd","aed","afn","all","amd","ang","aoa","ars","aud","awg","azn","bam","bbd","bdt","bgn","bif","bmd","bnd","bob","brl","bsd","bwp","bzd","cad","cdf","chf","clp","cny","cop","crc","cve","czk","djf","dkk","dop","dzd","egp","etb","eur","fjd","fkp","gbp","gel","gip","gmd","gnf","gtq","gyd","hkd","hnl","hrk","htg","huf","idr","ils","inr","isk","jmd","jpy","kes","kgs","khr","kmf","krw","kyd","kzt","lak","lbp","lkr","lrd","lsl","ltl","mad","mdl","mga","mkd","mnt","mop","mro","mur","mvr","mwk","mxn","myr","mzn","nad","ngn","nio","nok","npr","nzd","pab","pen","pgk","php","pkr","pln","pyg","qar","ron","rsd","rub","rwf","sar","sbd","scr","sek","sgd","shp","sll","sos","srd","std","svc","szl","thb","tjs","top","try","ttd","twd","tzs","uah","ugx","uyu","uzs","vnd","vuv","wst","xaf","xcd","xof","xpf","yer","zar","zmw"],"debit_negative_balances":false,"decline_charge_on":{"avs_failure":false,"cvc_failure":false},"default_currency":"usd","details_submitted":false,"display_name":null,"email":null,"external_accounts":{"object":"list","data":[{"id":"card_17aBTrD1k5Mcx8EOQpGtTRdZ","object":"card","account":"acct_17aBTrD1k5Mcx8EO","address_city":"","address_country":"","address_line1":"","address_line1_check":null,"address_line2":"","address_state":"","address_zip":"","address_zip_check":null,"brand":"Visa","country":"US","currency":"usd","cvc_check":"pass","default_for_currency":true,"dynamic_last4":null,"exp_month":3,"exp_year":2017,"fingerprint":"utlioIx9BLdlR0rg","funding":"debit","last4":"5556","metadata":[],"name":"some name","tokenization_method":null}],"has_more":false,"total_count":1,"url":"\/v1\/accounts\/acct_17aBTrD1k5Mcx8EO\/external_accounts"},"legal_entity":{"additional_owners":null,"address":{"city":null,"country":"US","line1":null,"line2":null,"postal_code":null,"state":null},"business_name":"some name","business_tax_id_provided":false,"dob":{"day":null,"month":null,"year":null},"first_name":null,"last_name":null,"personal_address":{"city":null,"country":null,"line1":null,"line2":null,"postal_code":null,"state":null},"personal_id_number_provided":false,"ssn_last_4_provided":false,"type":"individual","verification":{"details":null,"details_code":null,"document":null,"status":"unverified"}},"managed":true,"metadata":[],"product_description":null,"statement_descriptor":null,"support_phone":null,"timezone":"Etc\/UTC","tos_acceptance":{"date":null,"ip":null,"user_agent":null},"transfer_schedule":{"delay_days":2,"interval":"daily"},"transfers_enabled":false,"verification":{"disabled_reason":"fields_needed","due_by":null,"fields_needed":["legal_entity.dob.day","legal_entity.dob.month","legal_entity.dob.year","legal_entity.first_name","legal_entity.last_name","tos_acceptance.date","tos_acceptance.ip"]}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function updateAccountInfo($profileId, ApiRequest $request)
	{
		$data = $request->input();
		$data['profile_id'] = $profileId;

		return $this->exec(new UpdateAccountInfoCommand($data));
	}

	/**
	 * @api {get} /profiles/:profileId/credit_balance Get account credit balance
	 * @apiDescription Get Account credit balance
	 * @apiName ajax-payment-account-credit-get
	 * @apiGroup Profile Payment Account
	 *
	 * @apiParam {Number} profileId Profile id
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      *      {"message":"success","data":"20"}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function getProfileCreditBalance($profileId)
	{
		$data = ['profile_id' => $profileId];

		return $this->exec(new GetAccountCreditBalanceCommand($data));
	}

	/**
	 * @api {post} /profiles/:profileId/credit_balance Adds credit to an account
	 * @apiDescription Add credit to an account
	 * @apiName ajax-payment-account-credit-add-post
	 * @apiGroup Profile Payment Account
	 *
	 * @apiParam {Number} profileId Profile id
	 * @apiParam {Number} amount Amount
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      *      {"message":"success","data":"20"}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function addCreditToProfile($profileId, Request $request)
	{
		$data = ['profile_id' => $profileId];
		$data['amount'] = $request->input("amount");

		return $this->exec(new AddCreditToProfileCommand($data));
	}

    /**
     * @api {put} /payments/refund/:operationId
     * @apiDescription Refund payment operation
     * @apiName ajax-payments-refund
     * @apiGroup Payments
     *
     * @apiParam {Number} operationId Payment operation id
     *
     * @apiVersion 1.0.0
     *
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 422
     *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
     **/
    public function refundOperation($operationId)
    {
        $this->exec(new RefundPaymentOperationCommand(['operation_id' => $operationId]));

        return $this->respondOk();
	}

	public function stripeWebhook(Request $request)
	{
		$this->exec(new UpdateAccountVerificationCommand($request->input()));

		return $this->respondOk();
	}

	public function paypal(Request $request, RestApiGateway $provider)
	{
		$response = $provider->createPayment($request);

		return $response->getId();

	}

	public function completePaypal(Request $request, RestApiGateway $provider)
	{
		$senderPaymentMethod = PaymentMethodService::getPaymentMethod(7);
		$account = $senderPaymentMethod->getSourceEntity();
		$account->payer_id = $request->payerID;
		$account->token = $request->paymentID;
		$account->save();

		$response = $provider->completeAuthorization($senderPaymentMethod, 35);
	}

	public function paypalAccount(RestApiGateway $provider)
	{
		return $provider->authorizeAccount();
	}
}

<?php namespace App\Http\Controllers\Api;


use App\Http\Requests\ApiRequest;
use WL\Modules\Provider\Commands\CancelProviderPlanCommand;
use WL\Modules\Provider\Commands\ChangeProviderPlanCommand;
use WL\Modules\Provider\Commands\GetNextCycleEstimate;
use WL\Modules\Provider\Commands\GetProviderPlanDetailsCommand;
use WL\Modules\Provider\Commands\ReviveProviderPlanCommand;
use WL\Modules\Provider\Commands\TerminateProviderPlanCommand;
use WL\Modules\Provider\Commands\UpdateProviderPlanCommand;
use WL\Modules\Provider\Exceptions\NoActivePlanFound;
use WL\Modules\Provider\Transformers\PlanDetailsTransformer;

class ProviderMembershipApiController extends ApiController
{
    /**
     * @api {GET} /providers/{providerId}/membership/estimate
     * @apiDescription Get Provider next billing estimate.
     * @apiName ajax-provider-billing-estimate
     * @apiGroup Providers Membership
     *
     * @apiVersion 1.0.0
     * @apiParam {Number} providerId Provider ID
     */
    public function billingEstimate($providerId)
    {
        return $this->dispatch(new GetNextCycleEstimate(['profileId' => $providerId]));
    }

    /**
     * @api {PUT} /providers/{providerId}/membership
     * @apiDescription Edit Provider active plan details.
     * @apiName ajax-provider-membership-edit
     * @apiGroup Providers Membership
     *
     * @apiVersion 1.0.0
     * @apiParam {Number} providerId Provider ID
     * @apiParam {Number} cycles Number of cycles
     */
    public function editProviderPlan($providerId, ApiRequest $request)
    {
    	$inputData = $request->all();
    	$inputData['profileId'] = $providerId;
        return $this->dispatch(new UpdateProviderPlanCommand($inputData)) ? $request->get('cycles') : null;
    }

    /**
     * @api {GET} /providers/{providerId}/membership
     * @apiDescription Get Provider active plan details.
     * @apiName ajax-provider-membership-details
     * @apiGroup Providers Membership
     *
     * @apiVersion 1.0.0
     * @apiParam {Number} providerId Provider ID
     */
    public function membershipDetails($providerId)
    {
        $data = $this->dispatch(new GetProviderPlanDetailsCommand(['profileId' => $providerId]));
        if(!$data) {
            throw new NoActivePlanFound();
        }
		return (new PlanDetailsTransformer())->transform($data);
    }

    /**
     * @api {PUT} /providers/{providerId}/membership/change
     * @apiDescription Change current plan.
     * @apiName ajax-provider-membership-change
     * @apiGroup Providers Membership
     *
     * @apiVersion 1.0.0
     * @apiParam {Number} providerId Provider ID
     * @apiParam {Number} productId Product ID
     */
    public function changeProviderPlan($providerId, ApiRequest $request)
    {
        $inputData = $request->all();
        $inputData['profileId'] = $providerId;
        $result = $this->dispatch(new ChangeProviderPlanCommand($inputData));
        return (new PlanDetailsTransformer())->transform($result);
    }

    /**
     * @api {PUT} /providers/{providerId}/membership/cancel
     * @apiDescription Cancel current plan.
     * @apiName ajax-provider-membership-cancel
     * @apiGroup Providers Membership
     *
     * @apiVersion 1.0.0
     * @apiParam {Number} providerId Provider ID
     */
    public function cancelProviderPlan($providerId)
    {
        $this->dispatch(new CancelProviderPlanCommand(['profileId' => $providerId]));
        $this->respondOk();
    }

    /**
     * @api {PUT} /providers/{providerId}/membership/revive
     * @apiDescription Revive current plan.
     * @apiName ajax-provider-membership-revive
     * @apiGroup Providers Membership
     *
     * @apiVersion 1.0.0
     * @apiParam {Number} providerId Provider ID
     */
    public function reviveProviderPlan($providerId)
    {
        $this->dispatch(new ReviveProviderPlanCommand(['profileId' => $providerId]));
        $this->respondOk();
    }

	/**
	 * @api {DELETE} /providers/{providerId}/membership
	 * @apiDescription Terminate current plan.
	 * @apiName ajax-provider-membership-terminate
	 * @apiGroup Providers Membership
	 *
	 * @apiVersion 1.0.0
	 * @apiParam {Number} providerId Provider ID
	 */
	public function terminateProviderPlan($providerId)
	{
		$this->dispatch(new TerminateProviderPlanCommand(['profileId' => $providerId]));
        $this->respondOk();
	}
}

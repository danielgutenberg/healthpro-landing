<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use WL\Modules\Provider\Commands\AddProviderServiceCommand;
use WL\Modules\Provider\Commands\LoadProviderServicesCommand;
use WL\Modules\Provider\Commands\LoadProviderServiceTypesCommand;
use WL\Modules\Provider\Commands\RemoveProviderServiceCommand;
use WL\Modules\Provider\Commands\RemoveProviderServiceSessionCommand;
use WL\Modules\Provider\Commands\SortProviderServiceCommand;
use WL\Modules\Provider\Commands\UpdateProviderServiceCommand;
use WL\Modules\Provider\Transformers\ProviderServiceTransformer;

class ProviderServiceApiController extends ApiController
{
	/**
	 * @api {get} /providers/services All Valid Services
	 * @apiDescription Get a list of provider services. Acts as a alternative entry point to /taxonomies/services/tags
	 * @apiName ajax-provider-service-types
	 * @apiGroup Providers
	 *
	 * @apiParam (Get parameters) {String} [sort] Field to sort by
	 *
	 * @apiVersion 1.0.0
	 */
	public function getProviderServiceTypes(ApiRequest $request)
	{
		return $this->exec(new LoadProviderServiceTypesCommand($request->get('sort', null)));
	}

	/**
	 * Get Individual Provider Services
	 *
	 * @api {get} providers/:providerId/services Provider Services Get
	 * @apiDescription Get services for the authed provider.
	 * @apiName ajax-profile-provider-services
	 * @apiGroup Providers
	 *
	 * @apiParam {Number} providerId Profile id
	 *
	 * @apiVersion 1.0.0
	 */
	public function getProviderServices($providerId)
	{
		return $this->exec(new LoadProviderServicesCommand($providerId), function ($result) {
			return (new ProviderServiceTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {post} /providers/:providerId/services Provider Service Add
	 * @apiDescription Add a service for the authed provider.
	 * @apiName ajax-provider-service-add
	 * @apiGroup Providers
	 *
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiParam {String} name
	 * @apiParam {Array} service_type_ids
	 * @apiParam {Array} location_ids Location id's
	 * @apiParam {Array} sessions
	 * @apiParam {Number} deposit Non refundable deposit [0..100]
	 * @apiParam {String} deposit_type [flat_rate|percentage]
	 *
	 * @apiVersion 1.0.0
	 */
	public function addProviderService($providerId, ApiRequest $request)
	{
		$name = $request->get('name');
		$serviceTypeIds = $request->get('service_type_ids');
		$locationIds = $request->get('location_ids');
		$sessions = $request->get('sessions');

		$deposit = $request->get('deposit');
		$depositType = $request->get('deposit_type');
		$certificates = $request->get('gift_certificates');
		$description = $request->get('description', null);

		return $this->exec(new AddProviderServiceCommand($providerId, $name, $serviceTypeIds, $locationIds, $sessions, $deposit, $depositType, $certificates, $description), function ($result) {
			return (new ProviderServiceTransformer())->transform($result);
		});
	}

	/**
	 * @api {put} providers/:providerId/services/:serviceId Provider Service Update
	 * @apiDescription Update services for the authed provider.
	 * @apiName ajax-provider-service-update
	 * @apiGroup Providers
	 *
	 * @apiParam {Number} providerId Profile id
	 * @apiParam {Number} serviceId A single service
	 * @apiParam {String} name
	 * @apiParam {Array} location_ids Location id's
	 * @apiParam {Array} sessions
	 * @apiParam {Number} deposit Non refundable deposit [0..100]
	 * @apiParam {String} deposit_type [flat_rate|percentage]
	 *
	 * @apiVersion 1.0.0
	 */
	public function updateProviderService($providerId, $serviceId, ApiRequest $request)
	{
		$name = $request->get('name');
		$locationIds = $request->get('location_ids');
		$serviceTypeIds = $request->get('service_type_ids');
		$sessions = $request->get('sessions');
		$deposit = $request->get('deposit');
		$depositType = $request->get('deposit_type');
		$certificates = $request->get('gift_certificates');
		$description = $request->get('description', null);

		return $this->exec(new UpdateProviderServiceCommand($providerId, $serviceId, $name, $serviceTypeIds, $locationIds, $sessions, $deposit, $depositType, $certificates, $description), function ($result) {
			return (new ProviderServiceTransformer())->transform($result);
		});
	}

	/**
	 * Sort Provider Services
	 *
	 * @api {put} providers/:providerId/services/sort Provider Services Sort
	 * @apiDescription Sort provider services
	 * @apiName ajax-provider-services-sort
	 * @apiGroup Providers
	 *
	 * @apiParam {Number} providerId Profile id
	 * @apiParam {Array} service_ids Service IDs
	 *
	 * @apiVersion 1.0.0
	 */
	public function sortProviderServices($providerId, ApiRequest $request)
	{
		$serviceIds = $request->get('service_ids');

		return $this->exec(new SortProviderServiceCommand($providerId, $serviceIds), function ($result) {
			return (new ProviderServiceTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {delete} providers/:providerId/services/:serviceId Provider Service Delete
	 * @apiDescription Delete service for the authed provider.
	 * @apiName ajax-provider-service-remove
	 * @apiGroup Providers
	 *
	 * @apiParam {Number} providerId Profile id
	 * @apiParam {Number} serviceId A single service
	 * @apiVersion 1.0.0
	 */
	public function removeProviderService($providerId, $serviceId)
	{
		return $this->exec(new RemoveProviderServiceCommand($providerId, $serviceId));
	}


	/**
	 * @api {delete} providers/:providerId/servicesessions/:serviceSessionId Provider Service Delete
	 * @apiDescription Delete service for the authed provider.
	 * @apiName ajax-provider-service-remove
	 * @apiGroup Providers
	 *
	 * @apiParam {Number} providerId Profile id
	 * @apiParam {Number} serviceSessionId A single service
	 * @apiVersion 1.0.0
	 */
	public function removeProviderServiceSession($providerId, $serviceSessionId)
	{
		return $this->exec(new RemoveProviderServiceSessionCommand($providerId, $serviceSessionId));
	}

}

<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use WL\Modules\Taxonomy\LoadChildTagsCommand;
use WL\Modules\Taxonomy\TagTransformer;

/**
 * Class TaxonomyApiController
 * @package App\Http\Controllers\Api
 */
class TagApiController extends ApiController
{

	/**
	 * @api {get} /core/taxonomy/:taxonomySlug/:tagSlug/tags Taxonomy tag child tags
	 * @apiDescription Get child tags for a given parents :taxonomySlug and :tagSlug
	 * @apiName ajax-core-taxonomy-tags-childs
	 * @apiGroup Core-Tags
	 *
	 * @apiParam {String} taxonomySlug Taxonomy slug
	 * @apiParam {String} tagSlug One or more hierarchical tag slugs divided by '/'
	 * @apiParam (Get parameters) {String} [sort] Field to sort by
	 * @apiExample Example usage:
	 *   http://healthpro:8000/ajax/v1/core/taxonomy/services/tags/fitness/tags
	 *   http://healthpro:8000/ajax/v1/core/taxonomy/services/tags/fitness/zumba/tags
	 *
	 * @apiVersion 1.0.0
	 */
	/**
	 * Get child tags for a given parents :taxonomySlug and :tagSlug
	 *
	 * @param String $taxonomySlug
	 * @param String $tagSlug
	 * @return mixed
	 */

	public function getChildTags(ApiRequest $request,$taxonomySlug, $tagSlug)
	{
		$tagSlugs = explode('/', $tagSlug);

		return $this->exec(new LoadChildTagsCommand($taxonomySlug, $tagSlugs,$request->get('sort',null)), function ($result) {
			return (new TagTransformer())->transformCollection($result);
		});
	}


}

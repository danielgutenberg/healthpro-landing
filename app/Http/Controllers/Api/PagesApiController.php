<?php namespace App\Http\Controllers\Api;


use WL\Modules\Page\Commands\GetPageCommand;
use WL\Modules\Page\Transformers\PageTransformer;

class PagesApiController extends ApiController
{
	public function getPage($pageSlug, $lang = null)
	{
		return $this->exec(new GetPageCommand($pageSlug, $lang), function ($result) {
			return (new PageTransformer())->transform($result);
		});
	}
}

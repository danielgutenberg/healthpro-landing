<?php namespace App\Http\Controllers\Api;
use App\Http\Requests\ApiRequest;
use Http\Requests\Api\Search\SearchApiRequest;
use WL\Modules\Search\Commands\SearchAvailableProvidersCommand;
use WL\Modules\Search\Commands\SearchProviderServicesByTagCommand;
use WL\Modules\Search\Transformers\SearchAvailableProviderTransformer;

class SearchApiController extends ApiController
{
	/**
	 * @api {get} /providers/search Get available providers
	 * @apiDescription Gets providers and appointments or classes with available time
	 * @apiName ajax-search-available-providers-get
	 * @apiGroup Search
	 *
	 * @apiParam (Get parameters) {String} location Postal code OR City,Province OR Geo coordinates: lat lon OR Geo coordinates: lat1 lon1 , lat2 lon2 for coordBox search
	 * @apiParam (Get parameters) {String} [location_type_id] Location type
	 * @apiParam (Get parameters) {String} [split_by_location] Split professional item by location
	 * @apiParam (Get parameters) {DateTime} from From date
	 * @apiParam (Get parameters) {DateTime} [until] Until date. If not specified then until equals to from
	 * @apiParam (Get parameters) {Number} [service] Service tag_ids or Tag Names (comma-separated for multiple tag search)
	 * @apiParam (Get parameters) {String} [type=appointment] appointment|class
	 * @apiParam (Get parameters) {Number} [radius=5] Search radius, miles.
	 * @apiParam (Get parameters) {Array} [query] Taxonomy Query. Query can be an array.
	 * @apiParam (Get parameters) {Array} query.tag tag is just text that specifies type of query
	 * @apiParam (Get parameters) {Array} query.tag.taxonomy Taxonomy should be the slug of the search taxonomy  An array of tag ids. Tags can be optional or required: `!932` to require tag 932.
	 * @apiParam (Get parameters) {Boolean} [include_child_tags=1] Search by child tags also (1 or 0)
	 * @apiParam (Get parameters) {Number} [duration=30] Appointment\Class duration, minutes.
	 * @apiParam (Get parameters) {Number} [gap=0] Gap between Appointment\Class session, minutes.
	 * @apiParam (Get parameters) {Number} [page=1] Page
	 * @apiParam (Get parameters) {Number} [per_page=10] Results per page
	 * @apiParam (Get parameters) {Boolean} [availabilities=1] Search for just listings or also members
	 * @apiParam (Get parameters) {String} [sort=relevance] "rating"|"distance"|"relevance"|"name". The "-" sign in front sets order to DESC (ex:"-name" for sort by name descending ).
	 * @apiParam (Get parameters) {Number} [provider_id] Provider id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"results":[{"profile":{"id":240,"title":"Dr.","gender":"male","slug":"camila-rempel","first_name":"Al","last_name":"Johnson","full_name":"Al Johnson","avatar":"http:\/\/healthpro.app:8000\/uploads\/profiles\/5ecf9859ecbcc3740c8495f78da8655f-250x250.png","ratings":{"details":[],"overall":0}},"locations":{"79":{"id":79,"name":"Hackett, Spencer and Jones","service_area":0,"address":{"address_id":79,"address":"858 Walsh Plaza Suite 446","province":"NY","city":"Seneca Falls","postal_code":"13148","country":"US","longitude":"-76.789560","latitude":"42.925770"},"services":[{"id":79,"profile_id":240,"name":"Yoga","parent_id":null,"tag_id":603,"parent_name":null}]}},"availabilities":{"2015-08-18":[{"id":683,"location_id":79,"from":"2015-08-18T13:45:00-0400","until":"2015-08-18T14:20:00-0400"},{"id":683,"location_id":79,"from":"2015-08-18T14:15:00-0400","until":"2015-08-18T14:50:00-0400"},{"id":683,"location_id":79,"from":"2015-08-18T14:45:00-0400","until":"2015-08-18T15:20:00-0400"},{"id":683,"location_id":79,"from":"2015-08-18T15:15:00-0400","until":"2015-08-18T15:50:00-0400"},{"id":683,"location_id":79,"from":"2015-08-18T15:45:00-0400","until":"2015-08-18T16:20:00-0400"},{"id":689,"location_id":79,"from":"2015-08-18T09:00:00-0400","until":"2015-08-18T09:35:00-0400"},{"id":689,"location_id":79,"from":"2015-08-18T09:30:00-0400","until":"2015-08-18T10:05:00-0400"},{"id":689,"location_id":79,"from":"2015-08-18T10:00:00-0400","until":"2015-08-18T10:35:00-0400"},{"id":689,"location_id":79,"from":"2015-08-18T10:30:00-0400","until":"2015-08-18T11:05:00-0400"},{"id":689,"location_id":79,"from":"2015-08-18T11:00:00-0400","until":"2015-08-18T11:35:00-0400"}],"2015-08-19":[],"2015-08-17":[{"id":692,"location_id":79,"from":"2015-08-17T09:00:00-0400","until":"2015-08-17T09:35:00-0400"},{"id":692,"location_id":79,"from":"2015-08-17T09:30:00-0400","until":"2015-08-17T10:05:00-0400"},{"id":692,"location_id":79,"from":"2015-08-17T10:00:00-0400","until":"2015-08-17T10:35:00-0400"},{"id":692,"location_id":79,"from":"2015-08-17T10:30:00-0400","until":"2015-08-17T11:05:00-0400"},{"id":692,"location_id":79,"from":"2015-08-17T11:00:00-0400","until":"2015-08-17T11:35:00-0400"},{"id":692,"location_id":79,"from":"2015-08-17T11:30:00-0400","until":"2015-08-17T12:05:00-0400"}]}}],"count":1}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"from":["isn't valid date"]}}}
	 **/
	public function searchAvailableProviders(SearchApiRequest $request)
	{
		$inputData = $request->all();
		$inputData['provider_id'] = $request->inputArr('provider_id');
		$inputData['requiredServiceTags'] = $request->requiredServiceTags();
		$inputData['optionalServiceTags'] = $request->optionalServiceTags();

		$inputData['requiredProfileTags'] = $request->requiredProfileTags();
		$inputData['optionalProfileTags'] = $request->optionalProfileTags();
		if (isset($inputData['sort']))
			$inputData['order'] = $request->extractOrderFromSort($inputData['sort']);

		return $this->exec(new SearchAvailableProvidersCommand($inputData), function ($result) use ($inputData) {

			if (!empty($result) && ($result->count > 0)) {
				$result->results = (new SearchAvailableProviderTransformer(
					['serviceTags' => array_merge($inputData['optionalServiceTags'], $inputData['requiredServiceTags'])]
				))->transformCollection($result->results);
			}

			return $result;
		});
	}


	/**
	 * @api {get} /core/services/search Search services by Tag
	 * @apiDescription Search for provider services (Appointments and Classes)
	 * @apiName ajax-core-services-search
	 * @apiGroup Core
	 *
	 * @apiParam (Get parameters) {String} q Tag to search
	 * @apiParam (Get parameters) {String} split=0 Split Appointmets from Classes
	 * @apiParam (Get parameters) {String} [type] "class"|"appointment"|"null" Filter by service type. Default value is "null"
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK /core/services/search?q=yo
	 *      {"message":"success","data":[{"name":"Phoenix Rising Yoga","tag_id":626,"appointments":1,"classes":0},{"name":"Prenatal Yoga","tag_id":607,"appointments":1,"classes":0},{"name":"Svaroopa Yoga","tag_id":632,"appointments":1,"classes":0},{"name":"Pregnancy Yoga","tag_id":607,"appointments":1,"classes":0}]}
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK /core/services/search?q=yo&split=1
	 *      {"message":"success","data":[{"name":"Acro Yoga Appointment","tag_id":604,"appointments":1},{"name":"Aerial Yoga Class","tag_id":605,"classes":1},{"name":"Ananda Yoga Class","tag_id":606,"classes":1},{"name":"Ananda Yoga Appointment","tag_id":606,"appointments":2},{"name":"Anusara Yoga Appointment","tag_id":612,"appointments":1},{"name":"Bikram Yoga Appointment","tag_id":615,"appointments":1},{"name":"Iyengar Yoga Class","tag_id":620,"classes":1},{"name":"Iyengar Yoga Appointment","tag_id":620,"appointments":2},{"name":"Suspension Yoga Class","tag_id":610,"classes":1},{"name":"Svaroopa Yoga Class","tag_id":632,"classes":1},{"name":"Vinyasa Yoga Appointment","tag_id":634,"appointments":1},{"name":"Laya Yoga Class","tag_id":624,"classes":1}]}
	 **/
	public function searchProviderServicesByTag(ApiRequest $request)
	{
		$data = $request->all();

		if (isset($data['q']))
			$data['q'] = rtrim($data['q'], "\\");

		return $this->exec(new SearchProviderServicesByTagCommand($data));
	}
}

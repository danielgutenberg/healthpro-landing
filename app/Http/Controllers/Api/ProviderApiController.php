<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use WL\Modules\Profile\Commands\GetProviderFullDetailsCommand;
use WL\Modules\Provider\Commands\UpdateProviderAcceptedMethods;

class ProviderApiController extends ApiController
{
    /**
     * @api {get} /providers/{providerId}/full Professional get details
     * @apiDescription Return full Provider Details
     * @apiName ajax-provider-full-get
     * @apiGroup Providers
     *
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 422 {"message":"error","errors":{"messages":{"profile_id":["validation.required"]}}}
     */
    public function getProviderDetails($providerId)
    {
        return $this->exec(new GetProviderFullDetailsCommand(['profile_id' => $providerId]));
	}

    /**
     * @api {put} /providers/{providerId}/acceptPayment
     * @apiDescription Return full Provider Details
     * @apiName ajax-provider-accept-methods-update
     * @apiGroup Providers
     *
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 422 {"message":"error","errors":{"messages":{"profile_id":["validation.required"]}}}
     */
    public function updateProviderAcceptedMethods($providerId, ApiRequest $request)
    {
        $data = [
            'profileId' => $providerId,
            'method' => $request->get('method'),
        ];

        $this->exec(new UpdateProviderAcceptedMethods($data));
        return $this->respondOk();
	}
}

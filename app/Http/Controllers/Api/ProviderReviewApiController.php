<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use Illuminate\Http\Request;
use WL\Modules\Provider\Commands\Review\GetProviderReviewsCommand;
use WL\Modules\Provider\Commands\Review\CreateProviderReviewsCommand;
use WL\Modules\Provider\Commands\Review\UpdateProviderReviewCommand;
use WL\Modules\Provider\Commands\Review\DeleteProviderReviewCommand;
use WL\Modules\Provider\Commands\Review\GetProviderReviewRatingsCommand;
use WL\Modules\Provider\Commands\Review\SetProviderReviewRatingsCommand;
use WL\Modules\Provider\Commands\Review\UpdateProviderReviewRatingCommand;
use WL\Modules\Provider\Commands\Review\DeleteProviderReviewRatingCommand;
use WL\Modules\Provider\Commands\Review\GetProviderReviewHelpfulRatingCommand;
use WL\Modules\Provider\Commands\Review\RateProviderReviewHelpfulCommand;
use WL\Modules\Provider\Commands\Review\UnrateProviderReviewHelpfulCommand;
use WL\Modules\Review\Transformers\ReviewTransformer;
use WL\Modules\Rating\Transformers\RatingTransformer;
use WL\Modules\Profile\Facades\ProfileService;

class ProviderReviewApiController extends ApiController
{

	/**
	 * @api {GET} /providers/{providerId}/reviews Get Provider Reviews
	 * @apiDescription Get reviews for the specified provider.
	 * @apiName ajax-provider-reviews
	 * @apiGroup Providers Reviews
	 *
	 * @apiVersion 1.0.0
	 * @apiParam {Number} providerId Provider ID
	 */
	public function getProviderReviews($providerId)
	{
		return $this->exec(new GetProviderReviewsCommand($providerId), function ($result) {
			return (new ReviewTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {POST} /providers/{providerId}/reviews Create Provider Reviews
	 * @apiDescription Create reviews for the specified provider.
	 * @apiName ajax-provider-review-add
	 * @apiGroup Providers Reviews
	 *
	 * @apiVersion 1.0.0
	 * @apiParam {Number} providerId Provider ID
	 *
	 * @apiParam {String} comment Review Comment
	 * @apiParam {Object[]} ratings Review ratings {label1: value1, label2: value2, ...}
	 * @apiParam {String} ratings.ratingLabel Rated value for Review Rating label
	 */
	public function createProviderReview($providerId, ApiRequest $request)
	{
		$comment = $request->get('comment');
		$ratings = $request->get('ratings',[]);
		return $this->exec(new CreateProviderReviewsCommand($providerId, $comment, $ratings), function($result){
			return (new ReviewTransformer())->transform($result);
		});
	}

	/**
	 * @api {PUT} /providers/{providerId}/reviews/{reviewId} Update Provider Reviews
	 * @apiDescription Update provider review.
	 * @apiName ajax-provider-review-update
	 * @apiGroup Providers Reviews
	 *
	 * @apiVersion 1.0.0
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Number} reviewId Review ID
	 *
	 * @apiParam {String} [comment] Review Comment
	 * @apiParam {Object[]} [ratings] One or many review ratings {ratingLabel1: value1, ratingLabel2: value2, ...}
	 * @apiParam {String} ratings.ratingLabel1 Rated value for Review Rating with label 'ratingLabel1'
	 */
	public function updateProviderReview($providerId, $reviewId, ApiRequest $request)
	{
		$comment = $request->get('comment');
		$ratings = $request->get('ratings',[]);
		return $this->exec(new UpdateProviderReviewCommand($providerId, $reviewId, $comment, $ratings), function($result){
			return (new ReviewTransformer())->transform($result);
		});
	}

	/**
	 * @api {DELETE} /providers/{providerId}/reviews/{reviewId} Delete Provider Reviews
	 * @apiDescription Delete provider review.
	 * @apiName ajax-provider-review-delete
	 * @apiGroup Providers Reviews
	 *
	 * @apiVersion 1.0.0
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Number} reviewId Review ID
	 */
	public function deleteProviderReview($providerId, $reviewId)
	{
		return $this->exec(new DeleteProviderReviewCommand($providerId, $reviewId));
	}


	/**
	 * @api {GET} /providers/{providerId}/reviews/{reviewId}/ratings Get Provider Review Ratings
	 * @apiDescription Get provider review ratings.
	 * @apiName ajax-provider-review-ratings
	 * @apiGroup Providers Reviews
	 *
	 * @apiVersion 1.0.0
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Number} reviewId Review ID
	 */
	public function getProviderReviewRatings($providerId, $reviewId)
	{
		return $this->exec(new GetProviderReviewRatingsCommand($providerId, $reviewId), function ($result) {
			return (new RatingTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {POST} /providers/{providerId}/reviews/{reviewId}/ratings Set Provider Review Ratings
	 * @apiDescription Set Provider Review Ratings. See review.yaml for ratings labels
	 * @apiName provider-review-rating-add
	 * @apiGroup Providers Reviews
	 *
	 * @apiVersion 1.0.0
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Number} reviewId Review ID
	 *
	 * @apiParam {Number} ratingLabel1 Rated value for ratingLabel1
	 * @apiParam {Number} [ratingLabel2] Rated value for ratingLabel2
	 */
	public function setProviderReviewRatings($providerId, $reviewId, ApiRequest $request)
	{
		$ratings = $request->all();
		return $this->exec(new SetProviderReviewRatingsCommand($providerId, $reviewId, $ratings));
	}

	/**
	 * @api {PUT} /providers/{providerId}/reviews/{reviewId}/ratings/{ratingId} Update Provider Review Rating
	 * @apiDescription Update Provider Review Rating. See review.yaml for ratings labels
	 * @apiName provider-review-rating-update
	 * @apiGroup Providers Reviews
	 *
	 * @apiVersion 1.0.0
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Number} reviewId Review ID
	 * @apiParam {Number} ratingId Rating ID
	 *
	 * @apiParam {Number} value Rating value
	 */
	public function updateProviderReviewRating($providerId, $reviewId, $ratingId, ApiRequest $request)
	{
		$value = $request->get('value');
		return $this->exec(new UpdateProviderReviewRatingCommand($providerId, $reviewId, $ratingId, $value));
	}

	/**
	 * @api {DELETE} /providers/{providerId}/reviews/{reviewId}/ratings/{ratingId} Delete Provider Review Rating
	 * @apiDescription Delete Provider Review Rating.
	 * @apiName provider-review-rating-delete
	 * @apiGroup Providers Reviews
	 *
	 * @apiVersion 1.0.0
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Number} reviewId Review ID
	 * @apiParam {Number} ratingId Rating ID
	 */
	public function deleteProviderReviewRating($providerId, $reviewId, $ratingId)
	{
		return $this->exec(new DeleteProviderReviewRatingCommand($providerId, $reviewId, $ratingId));
	}


	/**
	 * @api {GET} /providers/{providerId}/reviews/{reviewId}/helpful Get number of review helpful
	 * @apiDescription Get number of review helpful.
	 * @apiName ajax-provider-review-helpful
	 * @apiGroup Providers Reviews
	 *
	 * @apiVersion 1.0.0
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Number} reviewId Review ID
	 */
	public function getProviderReviewsHelpfulValue($providerId, $reviewId)
	{
		return $this->exec(new GetProviderReviewHelpfulRatingCommand($providerId, $reviewId), function($result) {
			return ($result) ? $result->rating_value : 0;
		});
	}

	/**
	 * @api {POST} /providers/{providerId}/reviews/{reviewId}/helpful Mark review as helpful
	 * @apiDescription Current user marks review as helpful
	 * @apiName provider-review-helpful-set
	 * @apiGroup Providers Reviews
	 *
	 * @apiVersion 1.0.0
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Number} reviewId Review ID
	 */
	public function setProviderReviewsHelpfulValue($providerId, $reviewId)
	{
		return $this->exec(new RateProviderReviewHelpfulCommand($providerId, $reviewId));
	}

	/**
	 * @api {DELETE} /providers/{providerId}/reviews/{reviewId}/helpful Unset review helpful
	 * @apiDescription Delete current user's helpful rating
	 * @apiName provider-review-helpful-unset
	 * @apiGroup Providers Reviews
	 *
	 * @apiVersion 1.0.0
	 * @apiParam {Number} providerId Provider ID
	 * @apiParam {Number} reviewId Review ID
	 */
	public function unsetProviderReviewHelpfulRating($providerId, $reviewId)
	{
		return $this->exec(new UnrateProviderReviewHelpfulCommand($providerId, $reviewId));
	}

}

<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use WL\Modules\Appointment\Commands\AddServiceRequestCommand;
use WL\Modules\Appointment\Commands\AffiliateRequestCommand;
use WL\Modules\Appointment\Commands\ContactRequestCommand;
use WL\Modules\Appointment\Commands\DemoRequestCommand;
use WL\Modules\Appointment\Commands\NewsletterRequestCommand;
use WL\Modules\Appointment\Commands\PartnerRequestCommand;
use WL\Modules\Appointment\Commands\SubmitAQuestionCommand;

class CustomerServiceRequestsController extends ApiController
{
	/**
	 * @api {post} /forms/add-service-request
	 * @apiDescription Requests new service to be added
	 * @apiName ajax-add-service-request
	 * @apiGroup Customer Service Requests
	 *
	 * @apiParam {String} couponName service
	 * @apiParam {String} email user's email address
	 * @apiVersion 1.0.0
	 **/
	public function addServiceRequest(ApiRequest $request)
	{
		$data = [
			'service' => $request->get('service'),
			'email' => $request->get('email'),
		];
		return $this->exec(new AddServiceRequestCommand($data));
	}

	public function demoRequest(ApiRequest $request)
	{
		$data = [
			'first_name' => $request->get('first_name'),
			'last_name' => $request->get('last_name'),
			'phone' => $request->get('phone'),
			'email' => $request->get('email'),
		];
		return $this->exec(new DemoRequestCommand($data));
	}

	public function partnerRequest(ApiRequest $request)
	{
		$data = $request->all();

		return $this->exec(new PartnerRequestCommand($data));
	}

	public function affiliateRequest(ApiRequest $request)
	{
		$data = $request->all();

		return $this->exec(new AffiliateRequestCommand($data));
	}

	public function contactRequest(ApiRequest $request)
	{
		$data = $request->all();

		return $this->exec(new ContactRequestCommand($data));
	}

	public function newsletterRequest(ApiRequest $request)
	{
		$data = $request->all();

		return $this->exec(new NewsletterRequestCommand($data));
	}

	public function submitAQuestion(ApiRequest $request)
	{
		$data = $request->all();

		return $this->exec(new SubmitAQuestionCommand($data));
	}
}

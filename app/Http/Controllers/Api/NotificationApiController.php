<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Notification\RestRequest;
use WL\Modules\User\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use WL\Modules\Notification\Facades\NotificationService;
use WL\Modules\Notification\Models\Notification;
use WL\Modules\Notification\Commands;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Facades\AuthorizationUtils;

class NotificationApiController extends ApiController
{


	/**
	 * @api {get} /notifications Get List
	 * @apiDescription Get List of Notifications for provided GET params
	 * @apiName api-notifications-get-list
	 * @apiGroup Notifications
	 *
	 * @apiParam {String} [fields] Limits Fields in response. Coma-separated ex: fields=sender_id,type,seen,job
	 * @apiParam {Array} [filters] Apply filters for results. ex: filters[sender_id]=IN|22,23,24     Comparison operators that can be used: =,<>,<,>,IN,!IN,BETWEEN,!BETWEEN
	 * @apiParam {Number} [page]=0  Page number. Used with perPage parameter
	 * @apiParam {Number} [perPage]=50 Results Per Page
	 * @apiParam {String} [sort]  Sort results ex: sort=id|desc,type|asc,...
	 * @apiVersion 1.0.0
	 */
	//	 * @apiParam {String} [pagination] Paginate in {page}-{perPage} style. Overrides page/perPage params. ex: pagination=0-40 where first is page, second is perPage.
	/**
	 * @param RestRequest $request
	 * @return mixed
	 */
	public function getList(RestRequest $request)
	{
		$queryParams = $request->extractApiDataFromParams();

		return $this->exec(new Commands\LoadNotificationsListCommand($queryParams['fields'], $queryParams['filters'], [$queryParams['page'], $queryParams['perPage']], $queryParams['sorts']));

	}

	/**
	 * @api {post} /notifications Create
	 * @apiDescription Create New Notification
	 * @apiName api-notifications-create-new
	 * @apiGroup Notifications
	 *
	 * @apiParam {Array}  dispatchers      Array of Entities with 'id' and 'type' that will dispatch notification.
	 * @apiParam {Number} sender_id
	 * @apiParam {String} sender_type
	 * @apiParam {String} type
	 * @apiParam {String} title
	 * @apiParam {String} description
	 * @apiVersion 1.0.0
	 */
	/**
	 * @param RestRequest $request
	 * @return mixed
	 */
	public function createNew(RestRequest $request)
	{

		return $this->exec(new Commands\CreateNotificationCommand(array_except($request->all(), ['_token'])));

	}


	/**
	 * @api {get} /notifications/:notificationId Get Single
	 * @apiDescription Get Single Notification
	 * @apiName api-notifications-get-details
	 * @apiGroup Notifications
	 *
	 * @apiParam {Number} notificationId
	 * @apiParam {GET} parent=1  Search 'parent' notification or 'result' by notificationId provided.
	 * @apiParam {GET} relations=0  Load relations or not. Allows to see who red notification if used with parent=1
	 * OR if parent=0 to see full notification data for current resultID
	 * @apiVersion 1.0.0
	 */
	/**
	 * @param $notificationId
	 * @return mixed
	 */
	public function getDetails($notificationId, RestRequest $request)
	{

		return $this->exec(new Commands\LoadNotificationCommand($notificationId, $request->get('parent', true), $request->get('relations', false)));
	}


	/**
	 * @api {put} /notifications/:notificationId Update Single
	 * @apiDescription Update Single Notification with data provided
	 * @apiName api-notifications-update-single
	 * @apiGroup Notifications
	 *
	 * @apiParam {Number} notificationId
	 *
	 * @apiParam {Number} sender_id
	 * @apiParam {String} sender_type
	 * @apiParam {Number} dispatcher_id
	 * @apiParam {String} dispatcher_type
	 * @apiParam {String} type
	 * @apiParam {String} title
	 * @apiParam {String} description
	 * @apiVersion 1.0.0
	 */
	/**
	 * @param RestRequest $request
	 * @param $notificationId
	 * @return mixed
	 */
	public function updateSingle(RestRequest $request, $notificationId)
	{
		$forbidden = ['id'];
		return $this->exec(new Commands\UpdateNotificationCommand($notificationId, array_except($request->all(), $forbidden)));

	}


	/**
	 * @api {delete} /notifications/:notificationId Delete Single
	 * @apiDescription Delete Single Notification
	 * @apiName api-notifications-delete-single
	 * @apiGroup Notifications
	 *
	 * @apiParam {Number} notificationId
	 *
	 * @apiVersion 1.0.0
	 */
	public function deleteSingle($notificationId, RestRequest $request)
	{
		return $this->exec(new Commands\DeleteNotificationCommand($notificationId));
	}


	/**
	 * @api {put} /notifications/:profileId/inbox/:notificationId/read Read Profile Single Inbox
	 * @apiDescription Mark Notification as Read/Unread for Profile
	 * @apiName api-notifications-read-single
	 * @apiGroup Notifications
	 *
	 * @apiParam {Number} profileId
	 * @apiParam {Number} notificationId
	 *
	 * @apiParam {Number} [read]  Mark notification as read/not-read
	 * @apiVersion 1.0.0
	 */
	public function readSingle(RestRequest $request, $profileId, $notificationId)
	{
		return $this->exec(new Commands\ReadNotificationCommand($profileId, $notificationId, $request->get('read', true)));
	}

	/**
	 * @api {put} /notifications/:profileId/inbox/read Read Profile Inbox List
	 * @apiDescription Mark List of Notifications as Read/Unread for Profile specified
	 * @apiName api-notifications-read-inbox-list
	 * @apiGroup Notifications
	 *
	 * @apiParam {Number} profileId
	 *
	 * @apiParam {Number} [read]  Mark List of notifications as read/not-read
	 * @apiParam {GET} [GET]  All params that are used in Get List (except fields) can be used to limit the list of notifications on witch changes will apply
	 * @apiVersion 1.0.0
	 */
	/**
	 * @param RestRequest $request
	 * @return mixed
	 */
	public function readInboxList($profileId, RestRequest $request)
	{
		$queryParams = $request->extractApiDataFromParams();

		return $this->exec(new Commands\ReadNotificationsListByIdsCommand($profileId, $queryParams, $request->get('read', true)));
	}

	/**
	 * @api {put} /notifications/me/inbox/read Read My Inbox List
	 * @apiDescription Mark Inbox List of Notifications as Read/Unread for Current User
	 * @apiName api-notifications-read-my-inbox-list
	 * @apiGroup Notifications
	 *
	 * @apiParam {Number} notificationId
	 *
	 * @apiParam {Number} [read]  Mark List of notifications as read/not-read
	 * @apiParam {GET} [GET]  All params that are used in Get List (except fields) can be used to limit the list of notifications on witch changes will apply
	 * @apiVersion 1.0.0
	 */
	/**
	 * @param RestRequest $request
	 * @return mixed
	 */
	public function readMyInboxList(RestRequest $request)
	{
		$queryParams = $request->extractApiDataFromParams();
		return $this->exec(new Commands\ReadMyNotificationsListCommand($queryParams, $request->get('read', true)));
	}


	/**
	 * @api {get} /notifications/:profileId/inbox Get Inbox List
	 * @apiDescription Get Inbox List of Notifications for Profile owner with provided GET params
	 * @apiName api-notifications-get-inbox
	 * @apiGroup Notifications
	 *
	 * @apiParam {Number} profileId
	 * @apiParam {String} [fields] Limits Fields in response. Coma-separated ex: fields=type,seen,job,etc..
	 * @apiParam {Array} [filters] Apply filters for results. ex: filters[sender_id]=IN|22,23,24     Comparison operators that can be used: =,<>,<,>,IN,!IN,BETWEEN,!BETWEEN,NULL,!NULL
	 * @apiParam {Number} [page]  Page number. Used with perPage parameter
	 * @apiParam {Number} [perPage]  Results Per Page
	 * @apiParam {String} [sort]  Sort results ex: sort=id|desc,type|asc,...
	 * @apiVersion 1.0.0
	 */
	public function getInbox($profileId, RestRequest $request)
	{
		$queryParams = $request->extractApiDataFromParams();

		return $this->exec(new Commands\LoadInboxNotificationsCommand(
			$profileId,
			$queryParams['fields'],
			$queryParams['filters'],
			[$queryParams['page'], $queryParams['perPage']],
			$queryParams['sorts']
		));

	}


	/**
	 * @api {get} /notifications/me/inbox Get My Inbox List
	 * @apiDescription Get Inbox List of Notifications received by Current user with provided GET params
	 * @apiName api-notifications-get-my-inbox
	 * @apiGroup Notifications
	 *
	 * @apiParam {String} [fields] Limits Fields in response. Coma-separated ex: fields=sender_id,type,seen,job
	 * @apiParam {Array} [filters] Apply filters for results. ex: filters[sender_id]=IN|22,23,24     Comparison operators that can be used: =,<>,<,>,IN,!IN,BETWEEN,!BETWEEN
	 * @apiParam {Number} [page]  Page number. Used with perPage parameter
	 * @apiParam {Number} [perPage]  Results Per Page
	 * @apiParam {String} [sort]  Sort results ex: sort=id|desc,type|asc,...
	 * @apiVersion 1.0.0
	 */
	public function getMyInbox(RestRequest $request)
	{
		$queryParams = $request->extractApiDataFromParams();

		return $this->exec(new Commands\LoadMyInboxNotificationsListCommand(
			$queryParams['fields'],
			$queryParams['filters'],
			[$queryParams['page'], $queryParams['perPage']],
			$queryParams['sorts']
		));

	}

	/**
	 * @api {get} /notifications/:profileId/outbox Get Outbox List
	 * @apiDescription Get Outbox List of Notifications for Profile with provided GET params
	 * @apiName api-notifications-get-outbox
	 * @apiGroup Notifications
	 *
	 * @apiParam {Number} profileId
	 * @apiParam {String} [fields] Limits Fields in response. Coma-separated ex: fields=sender_id,type,seen,job
	 * @apiParam {Array} [filters] Apply filters for results. ex: filters[sender_id]=IN|22,23,24     Comparison operators that can be used: =,<>,<,>,IN,!IN,BETWEEN,!BETWEEN
	 * @apiParam {Number} [page]  Page number. Used with perPage parameter
	 * @apiParam {Number} [perPage]  Results Per Page
	 * @apiParam {String} [sort]  Sort results ex: sort=id|desc,type|asc,...
	 * @apiVersion 1.0.0
	 */
	public function getOutbox($profileId, RestRequest $request)
	{
		$queryParams = $request->extractApiDataFromParams();

		return $this->exec(new Commands\LoadOutboxNotificationsCommand(
			$profileId,
			$queryParams['fields'],
			$queryParams['filters'],
			[$queryParams['page'], $queryParams['perPage']],
			$queryParams['sorts']
		));

	}

	/**
	 * @api {get} /notifications/me/outbox Get My Outbox List
	 * @apiDescription Get Outbox List of Notifications sent by Current user with provided GET params
	 * @apiName api-notifications-get-my-outbox
	 * @apiGroup Notifications
	 *
	 * @apiParam {String} [fields] Limits Fields in response. Coma-separated ex: fields=receiver_id,type,seen,job
	 * @apiParam {Array} [filters] Apply filters for results. ex: filters[sender_id]=IN|22,23,24     Comparison operators that can be used: =,<>,<,>,IN,!IN,BETWEEN,!BETWEEN
	 * @apiParam {Number} [page]  Page number. Used with perPage parameter
	 * @apiParam {Number} [perPage]  Results Per Page
	 * @apiParam {String} [sort]  Sort results ex: sort=id|desc,type|asc,...
	 * @apiVersion 1.0.0
	 */
	public function getMyOutbox(RestRequest $request)
	{
		$queryParams = $request->extractApiDataFromParams();

		return $this->exec(new Commands\LoadMyOutboxNotificationsListCommand(
			$queryParams['fields'],
			$queryParams['filters'],
			[$queryParams['page'], $queryParams['perPage']],
			$queryParams['sorts']
		));

	}

	/**
	 * @api {get} /notifications/me/inbox/:notificationId Get My Single Inbox Notification
	 * @apiDescription Get Single Notification from Current user Inbox by notificationId
	 * @apiName api-notifications-get-my-inbox-single
	 * @apiGroup Notifications
	 *
	 * @apiParam {Number} notificationId Notification ID
	 * @apiVersion 1.0.0
	 */
	/**
	 * @param $resultId
	 * @return mixed
	 */
	public function getMyInboxSingle($notificationId)
	{
		return $this->exec(new Commands\LoadMyInboxNotificationsListCommand(
			[],
			[['id', '=', $notificationId]]
		));
	}

	/**
	 * @api {get} /notifications/me/outbox/:notificationId Get My Single Outbox Notification
	 * @apiDescription Get Single Notification from Current user Outbox by notificationId
	 * @apiName api-notifications-get-my-outbox-single
	 * @apiGroup Notifications
	 *
	 * @apiParam {Number} notificationId
	 * @apiVersion 1.0.0
	 */
	/**
	 * @param $notificationId
	 * @return mixed
	 */
	public function getMyOutboxSingle($notificationId)
	{
		return $this->exec(new Commands\LoadMyOutboxNotificationsListCommand(
			[],
			[['id', '=', $notificationId]]
		));
	}

	/**
	 * @api {get} /notifications/:profileId/inbox/:notificationId Get Profile Single Inbox
	 * @apiDescription Get Single Notification for Profile owner Inbox by notificationId
	 * @apiName api-notifications-get-inbox-single
	 * @apiGroup Notifications
	 *
	 * @apiParam {Number} profileId      Profile ID
	 * @apiParam {Number} notificationId Notification ID
	 * @apiVersion 1.0.0
	 */
	/**
	 * @param $resultId
	 * @return mixed
	 */
	public function getInboxSingle($profileId, $notificationId)
	{
		return $this->exec(new Commands\LoadInboxNotificationsCommand(
			$profileId,
			[],
			[['id', '=', $notificationId]]
		));
	}

	/**
	 * @api {get} /notifications/:profileId/outbox/:notificationId Get Profile Single Outbox Notification
	 * @apiDescription Get Single Notification from Profile owner Outbox by notificationId
	 * @apiName api-notifications-get-outbox-single
	 * @apiGroup Notifications
	 *
	 * @apiParam {Number} profileId      Profile ID
	 * @apiParam {Number} notificationId
	 * @apiVersion 1.0.0
	 */
	/**
	 * @param $notificationId
	 * @return mixed
	 */
	public function getOutboxSingle($profileId, $notificationId)
	{
		return $this->exec(new Commands\LoadOutboxNotificationsCommand(
			$profileId,
			[],
			[['id', '=', $notificationId]]
		));
	}

	/**
	 * @api {put} /notifications/me/inbox/:notificationId/read Read My Single Inbox Notification
	 * @apiDescription Mark Notification as Read/Unread
	 * @apiName api-notifications-read-my-inbox-single
	 * @apiGroup Notifications
	 *
	 * @apiParam {Number} notificationId
	 *
	 * @apiParam {Number} [read]  Mark notification as read/not-read
	 * @apiVersion 1.0.0
	 */
	public function readMyInboxSingle(RestRequest $request, $notificationId)
	{
		if ($request->exists('read')) {
			$filters['seen'] = ['seen', '=', 0];
		} else {
			$filters['seen'] = ['seen', '=', 1];
		}
		$filters = array_merge($filters, ['id' => ['id', '=', $notificationId]]);

		return $this->exec(new Commands\ReadMySingleNotificationsListCommand($filters, $request->get('read', true)));
	}


}

<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use Illuminate\Http\Request;
use WL\Modules\ProviderVerification\Commands\ProviderVerificationCommand;
use WL\Modules\ProviderVerification\Models\ProviderVerification as Verification;
use WL\Modules\ProviderVerification\Transformers\ProviderVerificationTransformer;

class ProviderVerificationApiController extends ApiController
{

	/**
	 * @api {POST} /providers/{providerId}/verification Provider Verify
	 * @apiDescription Verifirst provider verification.
	 * @apiName ajax-provider-verification
	 * @apiGroup Providers
	 *
	 * @apiVersion 1.0.0
	 * @apiParam {Number} providerId Provider ID
	 *
	 * @apiParam {String{..30}} firstName
	 * @apiParam {String{..30}} [middleName]
	 * @apiParam {String{..30}} lastName
	 * @apiParam {String{..30}} country Country code. Ex: ‘US’
	 * @apiParam {String{..30}} address
	 * @apiParam {String{..30}} [address2]
	 * @apiParam {String{..50}} city
	 * @apiParam {String{..10}} state
	 * @apiParam {String{..10}} zip
	 * @apiParam {String{..11}} ssn
	 * @apiParam {Date} dateOfBirth Format: yyyy-mm-dd
	 * @apiParam {String} [phone] Format: aaa-ppp-nnnn. Standard US phone number
	 */
	public function verification($providerId, ApiRequest $request)
	{
		$params = $request->all();
		return $this->exec(new ProviderVerificationCommand($providerId, $params), function($result){
			return (new ProviderVerificationTransformer())->transform($result);
		});
	}

}

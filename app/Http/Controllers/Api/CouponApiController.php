<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use WL\Modules\Coupons\Commands\GetCouponEntity;

class CouponApiController extends ApiController
{
	/**
	 * @api {get} /coupons/:coupon Gets coupon entity
	 * @apiDescription Gets coupon entity by name
	 * @apiName ajax-coupon-get
	 * @apiGroup Coupons
	 *
	 * @apiParam {String} couponName Coupon code
	 * @apiParam {Array} types list of coupon types
	 * @apiParam {String} for "clinet", "provider" (optional)
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"id":112,"name":"somename","description":"some-description","applyTo":"WL\\Modules\\Order\\Models\\Order","amount":10,"amountType":"fixed","minimumTotal":null,"validFrom":"2016-08-24 21:20:28","validUntil":"2016-08-24 22:20:28","ownerId":null,"ownerType":null,"created_at":"2016-08-24 21:20:28","updated_at":"2016-08-24 21:20:28","max_usages":1,"actual_usages":0,"deleted_at":null,"issued_by":"staff"}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"coupon":{"messages":["Coupon is not exists","validation.coupon"],"status_code":"validation"}}}
	 **/
	public function get($couponName, ApiRequest $request)
	{
		$data = [
			'coupon' => $couponName,
			'types' => $request->get('types', []),
		];
		if($request->get('for')) {
			$data['for'] = $request->get('for');
		}
		return $this->exec(new GetCouponEntity($data));
	}
}

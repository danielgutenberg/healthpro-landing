<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use WL\Modules\Order\Commands\AddItemsToOrderCommand;
use WL\Modules\Order\Commands\BookOrderCommand;
use WL\Modules\Order\Commands\CancelOrderCommand;
use WL\Modules\Order\Commands\ChangeOrderPaymentMethodCommand;
use WL\Modules\Order\Commands\CreateOrderCommand;
use WL\Modules\Order\Commands\LoadOrderCommand;
use WL\Modules\Order\Commands\ProfessionalBillingReportCommand;
use WL\Modules\Order\Transformers\InvoiceFullTransformer;
use WL\Modules\Order\Transformers\OrderItemReportTransformer;
use WL\Modules\Order\Transformers\OrderTransformer;
use WL\Modules\Order\ValueObjects\ReportResultContainer;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Order\Commands\ProfileTransactionsReportCommand;


class OrderApiController extends ApiController
{

	/**
	 * @api {get} /orders/:orderId Returns order by id
	 * @apiDescription Returns order by id.
	 * @apiName ajax-order-items-get
	 * @apiGroup Orders
	 *
	 * @apiParam {Number} orderId Order id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"id":24,"profile_id":3,"card_id":24,"subtotal":104,"discount":35,"total":69,"line_items":{"purchasables":[{"name":"appointment","type":"purchasable","amount":25,"payload":null},{"name":"appointment","type":"purchasable","amount":43,"payload":null},{"name":"appointment","type":"purchasable","amount":36,"payload":null}],"coupons":[{"name":"coupon","type":"coupon","amount":5,"payload":{"name":"coupon-1","description":"some-description","type":"fixed"}},{"name":"coupon","type":"coupon","amount":30,"payload":{"name":"coupon-2","description":"some-description","type":"fixed"}}]}}}
	 *
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.from Date is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"appointmentId":["Appointment id must be provided."]}}}
	 **/
	public function getOrder($orderId)
	{
		$data = [
			'order_id' => $orderId
		];

		return $this->exec(new LoadOrderCommand($data), function ($result) {
			return (new OrderTransformer())->transform($result);
		});
	}

	/**
	 * @api {post} /orders/ Creates order
	 * @apiDescription Returns created order
	 * @apiName ajax-order-create-post
	 * @apiGroup Orders
	 *
	 * @apiParam {Number} profile_id Profile id
	 * @apiParam {Array} items [id: 42, name: appointment|coupon]
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"id":24,"profile_id":3,"card_id":24,"subtotal":104,"discount":35,"total":69,"line_items":{"purchasables":[{"name":"appointment","type":"purchasable","amount":25,"payload":null},{"name":"appointment","type":"purchasable","amount":43,"payload":null},{"name":"appointment","type":"purchasable","amount":36,"payload":null}],"coupons":[{"name":"coupon","type":"coupon","amount":5,"payload":{"name":"coupon-1","description":"some-description","type":"fixed"}},{"name":"coupon","type":"coupon","amount":30,"payload":{"name":"coupon-2","description":"some-description","type":"fixed"}}]}}}
	 *
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.from Date is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"appointmentId":["Appointment id must be provided."]}}}
	 **/
	public function createOrder(ApiRequest $request)
	{
		// temporary hack
		$data = $request->all();
		if (isset($data['card_id']))
			$data['payment_method_id'] = $data['card_id'];
		//

		return $this->exec(new CreateOrderCommand($data), function ($result) {
			return (new OrderTransformer())->transform($result);
		});
	}

	/**
	 * @api {put} /orders/:orderId Add items to order
	 * @apiDescription Add items to order
	 * @apiName ajax-order-add-items-post
	 * @apiGroup Orders
	 *
	 * @apiParam {Number} orderId Profile id
	 * @apiParam {Array} items [{id: 42, name: appointment|coupon}]
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"id":24,"profile_id":3,"card_id":24,"subtotal":104,"discount":35,"total":69,"line_items":{"purchasables":[{"name":"appointment","type":"purchasable","amount":25,"payload":null},{"name":"appointment","type":"purchasable","amount":43,"payload":null},{"name":"appointment","type":"purchasable","amount":36,"payload":null}],"coupons":[{"name":"coupon","type":"coupon","amount":5,"payload":{"name":"coupon-1","description":"some-description","type":"fixed"}},{"name":"coupon","type":"coupon","amount":30,"payload":{"name":"coupon-2","description":"some-description","type":"fixed"}}]}}}
	 *
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.from Date is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"appointmentId":["Appointment id must be provided."]}}}
	 **/
	public function addItemsToOrder($orderId, ApiRequest $request)
	{
		$data = $request->all();
		$data['order_id'] = $orderId;

		return $this->exec(new AddItemsToOrderCommand($data), function ($result) {
			return (new OrderTransformer())->transform($result);
		});
	}

	/**
	 * @api {put} /orders/:orderId Book order
	 * @apiDescription Book order
	 * @apiName ajax-order-book-post
	 * @apiGroup Orders
	 *
	 * @apiParam {Number} orderId Order id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.from Date is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"appointmentId":["Appointment id must be provided."]}}}
	 **/
	public function bookOrder(ApiRequest $request, $orderId)
	{
		$data = $request->all();
		$data['order_id'] = $orderId;
		return $this->exec(new BookOrderCommand($data));
	}

	/**
	 * @api {delete} /orders/:orderId Cancel order
	 * @apiDescription Cancel order
	 * @apiName ajax-order-cancel-delete
	 * @apiGroup Orders
	 *
	 * @apiParam {Number} orderId Order id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.from Date is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"appointmentId":["Appointment id must be provided."]}}}
	 **/
	public function cancelOrder($orderId)
	{
		$data = ['order_id' => $orderId];
		return $this->exec(new CancelOrderCommand($data));
	}

	/**
	 * @api {put} /orders/:orderId/payment Change PaymentMethod id
	 * @apiDescription Change Order PaymentMethod id, if items not authorized
	 * @apiName ajax-order-change-payment-method
	 * @apiGroup Orders
	 *
	 * @apiParam {Number} orderId Order id
	 * @apiParam {Number} paymentMethodId PaymentMethodId
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 *
	 * @apiError (Error: 422) {Object} messages Order has authorized items
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"error":{"messages":["Order has already authorized items"]}}}
	 **/
	public function changeOrderPaymentMethod(ApiRequest $request, $orderId)
	{
		$data = $request->all();
		$data['profileId'] = ProfileService::getCurrentProfileId();
		$data['orderId'] = $orderId;

		$result = $this->exec(new ChangeOrderPaymentMethodCommand($data));

		return $result;
	}

	/**
	 * @api {get} /profiles/:profileId/transactions/ Payment Transactions Get
	 * @apiDescription Loads Profile Order Transactions.
	 * @apiName ajax-order-transaction-get
	 * @apiGroup Profile Orders
	 *
	 * @apiParam (Get parameters) {Number} profileId Profile id
	//  * @apiParam (Get parameters) {Date|int} begin - Date: Begin Date, Int: begin at record number
	//  * @apiParam (Get parameters) {Date|int} end - Date: End Dated, Int: end at record number
	 * @apiParam (Get parameters) {string} type Transaction Type ('captured' or 'authorized')
	 *      {"message":"success","data":{"usd": {"total_amount": 550, "total_qty": 3,"transactions":
	 *        [{"name": "Gershon", "product": "Yoga", "date": "2015-06-03 10:28:27", "amount", 55, "payout": 53.6}]}}
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function getProfileTransactions(ApiRequest $request)
	{
		$inputDate = [
			"profile_id" => $request->profileId,
			"type" => $request->type,
			"begin" => $request->begin,
			"end" => $request->end,
		];

		return $this->exec(new ProfileTransactionsReportCommand($inputDate), function ($response) use ($inputDate) {
			if ($response->count()) {
				return (new OrderItemReportTransformer())->transform($response);
			}
		});
	}

	/**
	 * @api {get} /profiles/:profile_id/billing/ Billing Report Get
	 * @apiDescription Loads Profile Billing Report.
	 * @apiName ajax-order-billing-get
	 * @apiGroup Profile Orders
	 *
	 * @apiParam (Get parameters) {Number} profileId Profile id
	 * @apiParam (Get parameters) {int} begin - begin at record number
	 * @apiParam (Get parameters) {int} end - end at record number
	 * @apiParam (Get parameters) {string} status Order status ('booked' or 'cancelled')
     *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"number":["number is required"]}}}
	 **/
	public function getProfileBilling($profileId, ApiRequest $request)
	{
	    $data = $request->all();
	    $data['profile_id'] = $profileId;


		return $this->exec(new ProfessionalBillingReportCommand($data), function (ReportResultContainer $response) {
			if ($response->total_records) {
                $response->records = (new InvoiceFullTransformer())->transformCollection($response->records);
				return $response;
			}
		});
	}

}

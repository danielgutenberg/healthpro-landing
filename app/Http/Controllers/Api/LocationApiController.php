<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use WL\Modules\Location\Commands\AddClientLocationCommand;
use WL\Modules\Location\Commands\AddLocationAvailabilitiesCommand;
use WL\Modules\Location\Commands\AddProviderLocationCommand;
use WL\Modules\Location\Commands\CheckHomeLocationRadiusCommand;
use WL\Modules\Location\Commands\GetClientLocationsCommand;
use WL\Modules\Location\Commands\GetLocationAvailabilitiesCommand;
use WL\Modules\Location\Commands\GetProviderLocationsCommand;
use WL\Modules\Location\Commands\LoadLocationCountriesCommand;
use WL\Modules\Location\Commands\LoadLocationTypesCommand;
use WL\Modules\Location\Commands\RemoveClientLocationCommand;
use WL\Modules\Location\Commands\RemoveProviderLocationCommand;
use WL\Modules\Location\Commands\SearchLocationCommand;
use WL\Modules\Location\Commands\UpdateClientLocationCommand;
use WL\Modules\Location\Commands\UpdateLocationAvailabilitiesCommand;
use WL\Modules\Location\Commands\UpdateProviderLocationCommand;
use WL\Modules\Location\Transformer\LocationAvailabilityTransformer;
use WL\Modules\Location\Transformer\LocationTransformer;
use WL\Modules\Location\Transformer\PostCodeAutocompleteTransformer;

class LocationApiController extends ApiController
{
	/**
	 * @api {get} /core/locations/types Location Types
	 * @apiDescription Returns location types of provider
	 * @apiName ajax-location-types-get
	 * @apiGroup core
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":[{"id":1,"name":"home","type":"on-site"},{"id":2,"name":"home","type":"virtual"},{"id":3,"name":"home","type":"on-site"}]}
	 **/
	public function getLocationTypes()
	{
		return $this->exec(new LoadLocationTypesCommand());
	}

	/**
	 * @api {get} /providers/:providerId/locations Locations Get
	 * @apiDescription Gets location availabilities to provider.
	 * @apiName ajax-provider-locations-get
	 * @apiGroup Provider Locations
	 *
	 * @apiParam {String} [service_area_unit] Convert service_area to [kilometer|mile]
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      "{"message":"success","data":[{"id":52,"name":"somename","address":{"address_id":1,"address":"some address","province":"Portsmouth","city":"Portsmouth","postal_code":211,"country":"US"}}]}
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiVersion 1.0.0
	 **/
	public function getProviderLocations($providerId, ApiRequest $request)
	{
		$serviceAreaUnit = $request->get('service_area_unit');

		$data = $this->dispatch(new GetProviderLocationsCommand($providerId, $serviceAreaUnit));

		return $this->respondOk(
			(new LocationTransformer())->transformCollection($data)
		);

	}

	/**
	 * @api {post} /providers/:providerId/locations Location Create
	 * @apiDescription Adds location and availabilities to provider.
	 * @apiName ajax-provider-location-add
	 * @apiGroup Provider Locations
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiParam {String} name Location name
	 * @apiParam {String} address Address
	 * @apiParam {Number} location_type_id Location type
	 * @apiParam {String} postal_code Postal code
	 * @apiParam {Number{2}} country_code Country code
	 * @apiParam {Number} [service_area] Service area in km
	 * @apiParam {Number} [padding_time] Padding time in minutes
	 * @apiParam {String} [service_area_unit] Unit of service_area [kilometer|mile]
	 * @apiParam {Bool} address_coord_search=1 Search automatically for Address coordinates (if none provided)
	 * @apiParam {String} [address_lon] Address longitude
	 * @apiParam {String} [address_lat] Address latitude
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{...location data ...}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.from Date is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"from":["From must be an date."]}}}
	 **/
	public function addProviderLocation($providerId, ApiRequest $request)
	{
		$data = $request->all();
		$data['provider_id'] = $providerId;

		return $this->exec(new AddProviderLocationCommand($data), function ($result) {
			return (new LocationTransformer())->transform($result);
		});
	}

	/**
	 * @api {post} /clients/:clientId/locations Location Create
	 * @apiDescription Adds location to client.
	 * @apiName ajax-client-location-add
	 * @apiGroup Client Locations
	 *
	 * @apiParam {Number} clientId Client id
	 * @apiParam {String} name Location name
	 * @apiParam {String} address Address
	 * @apiParam {Number} location_type_id Location type
	 * @apiParam {String} postal_code Postal code
	 * @apiParam {Number{2}} country_code Country code
	 * @apiParam {String} [address_lon] Address longitude
	 * @apiParam {String} [address_lat] Address latitude
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{...location data ...}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.from Date is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"from":["From must be an date."]}}}
	 **/
	public function addClientLocation($clientId, ApiRequest $request)
	{
		$data = $request->all();
		$data['client_id'] = $clientId;

		return $this->exec(new AddClientLocationCommand(
			$data
		), function ($result) {
			return (new LocationTransformer())->transform($result);
		});
	}

	/**
	 * @api {put} /clients/:clientId/locations/:locationId Location Update
	 * @apiDescription Update location fields
	 * @apiName ajax-client-location-update
	 * @apiGroup Client Locations
	 *
	 * @apiParam {Number} clientId Client id
	 * @apiParam {Number} locationId Location id
	 * @apiParam {Array} name, address, postal_code, country_code Location fields
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *    {
	 *        "message": "success",
	 *        "data": true
	 *    }
	 * @apiVersion 1.0.0
	 */
	public function updateClientLocation($clientId, $locationId, ApiRequest $request)
	{
		$updateFieldsArr = [
			'name',
			'address',
			'postal_code',
			'country_code',
			'address_line_two'
		];

		$updateFields = [];
		foreach ($updateFieldsArr as $field) {
			if (array_key_exists($field, $request->all())) {
				$updateFields[$field] = $request->get($field);
			}
		}

		$data = [
			'client_id' => $clientId,
			'location_id' => $locationId
		];

		$data = $data + $updateFields;

		return $this->exec(new UpdateClientLocationCommand(
			$data
		), function ($result) {
			return (new LocationTransformer())->transform($result);
		});
	}

	/**
	 * @api {delete} /clients/:clientId/locations/:locationId Location Delete
	 * @apiDescription Delete location
	 * @apiName ajax-client-location-remove
	 * @apiGroup Client Locations
	 *
	 * @apiParam {Number} clientId Client id
	 * @apiParam {Number} locationId Location id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.from Date is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"clientId":["Client id must be supplied."]}}}
	 **/
	public function removeClientLocation($clientId, $locationId)
	{
		return $this->exec(new RemoveClientLocationCommand([
			'client_id' => $clientId,
			'location_id' => $locationId
		]));
	}

	/**
	 * @api {get} /clients/:clientId/locations Locations Get
	 * @apiDescription Gets location of client
	 * @apiName ajax-client-locations-get
	 * @apiGroup Client Locations
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      "{"message":"success","data":[{"id":52,"name":"somename","address":{"address_id":1,"address":"some address","province":"Portsmouth","city":"Portsmouth","postal_code":211,"country":"US"}}]}
	 *
	 * @apiParam {Number} clientId Client id
	 * @apiVersion 1.0.0
	 **/
	public function getClientLocations($clientId)
	{
		$data = ['client_id' => $clientId];

		$data = $this->dispatch(new GetClientLocationsCommand($data));

		return $this->respondOk(
			(new LocationTransformer())->transformCollection($data)
		);
	}


	/**
	 * @api {post} /providers/:providerId/locations/:locationId/availabilities Location Availabilities Create
	 * @apiDescription Adds location availabilities to provider.
	 * @apiName ajax-location-availabilities-add
	 * @apiGroup Provider Locations
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiParam {Number} locationId Location Id
	 * @apiParam {Array} availabilities Availabilities
	 * @apiParam (Availabilities) {Number} providerId Provider id
	 * @apiParam (Availabilities) {Number{1..7}} day Day (1 = Sunday, 2 = Monday, …, 7 = Saturday)
	 * @apiParam (Availabilities) {DateTime} from From
	 * @apiParam (Availabilities) {DateTime} until Until
	 * @apiParam (Availabilities) {DateTime} [valid_from] Valid From
	 * @apiParam (Availabilities) {DateTime} [valid_until] Valid Until
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":[{"id":107,"provider_id":4,"location_id":65,"day":0,"from":{"date":"2015-05-29 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-05-29 02:00:00.000000","timezone_type":3,"timezone":"UTC"},"max_clients":1,"type":"single","waiting_for_remove":0,"created_at":"2015-05-29 11:12:38","updated_at":"2015-05-29 11:12:38","location":{"id":65,"name":"somename","location_type_id":1,"open_from":null,"open_till":null,"created_at":"2015-05-29 11:12:38","updated_at":"2015-05-29 11:12:38","is_verified":0,"verified_at":"2015-05-29 11:12:38"}},{"id":108,"provider_id":4,"location_id":65,"day":2,"from":{"date":"2015-05-29 02:00:00.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-05-29 04:00:00.000000","timezone_type":3,"timezone":"UTC"},"max_clients":1,"type":"single","waiting_for_remove":0,"created_at":"2015-05-29 11:12:38","updated_at":"2015-05-29 11:12:38","location":{"id":65,"name":"somename","location_type_id":1,"open_from":null,"open_till":null,"created_at":"2015-05-29 11:12:38","updated_at":"2015-05-29 11:12:38","is_verified":0,"verified_at":"2015-05-29 11:12:38"}},{"id":109,"provider_id":4,"location_id":65,"day":4,"from":{"date":"2015-05-29 04:00:00.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-05-29 06:00:00.000000","timezone_type":3,"timezone":"UTC"},"max_clients":1,"type":"single","waiting_for_remove":0,"created_at":"2015-05-29 11:12:38","updated_at":"2015-05-29 11:12:38","location":{"id":65,"name":"somename","location_type_id":1,"open_from":null,"open_till":null,"created_at":"2015-05-29 11:12:38","updated_at":"2015-05-29 11:12:38","is_verified":0,"verified_at":"2015-05-29 11:12:38"}}]}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.from Date is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"from":["From must be an date."]}}}
	 **/

	public function addLocationAvailabilities(ApiRequest $request, $providerId, $locationId)
	{
		$data = $request->all();
		$data['provider_id'] = $providerId;
		$data['location_id'] = $locationId;
		$data['availabilities'] = $request->input('availabilities');

		return $this->exec(new AddLocationAvailabilitiesCommand($data), function ($result) {
			return (new LocationAvailabilityTransformer())->transformCollection($result);
		});
	}

	/**
	 * @api {get} /providers/:providerId/locations/:locationId/availabilities Location Availabilities Get
	 * @apiDescription Gets provider location availabilities
	 * @apiName ajax-location-availabilities-get
	 * @apiGroup Provider Locations
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiParam {Number} locationId Location Id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":[{"id":107,"provider_id":4,"location_id":65,"day":0,"from":{"date":"2015-05-29 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-05-29 02:00:00.000000","timezone_type":3,"timezone":"UTC"},"max_clients":1,"type":"single","waiting_for_remove":0,"created_at":"2015-05-29 11:12:38","updated_at":"2015-05-29 11:12:38","location":{"id":65,"name":"somename","location_type_id":1,"open_from":null,"open_till":null,"created_at":"2015-05-29 11:12:38","updated_at":"2015-05-29 11:12:38","is_verified":0,"verified_at":"2015-05-29 11:12:38"}},{"id":108,"provider_id":4,"location_id":65,"day":2,"from":{"date":"2015-05-29 02:00:00.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-05-29 04:00:00.000000","timezone_type":3,"timezone":"UTC"},"max_clients":1,"type":"single","waiting_for_remove":0,"created_at":"2015-05-29 11:12:38","updated_at":"2015-05-29 11:12:38","location":{"id":65,"name":"somename","location_type_id":1,"open_from":null,"open_till":null,"created_at":"2015-05-29 11:12:38","updated_at":"2015-05-29 11:12:38","is_verified":0,"verified_at":"2015-05-29 11:12:38"}},{"id":109,"provider_id":4,"location_id":65,"day":4,"from":{"date":"2015-05-29 04:00:00.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-05-29 06:00:00.000000","timezone_type":3,"timezone":"UTC"},"max_clients":1,"type":"single","waiting_for_remove":0,"created_at":"2015-05-29 11:12:38","updated_at":"2015-05-29 11:12:38","location":{"id":65,"name":"somename","location_type_id":1,"open_from":null,"open_till":null,"created_at":"2015-05-29 11:12:38","updated_at":"2015-05-29 11:12:38","is_verified":0,"verified_at":"2015-05-29 11:12:38"}}]}
	 *
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"providerId":["Must be numeric"]}}}
	 **/

	public function getLocationAvailabilities(ApiRequest $request, $providerId, $locationId)
	{
		$data = $this->dispatch(new GetLocationAvailabilitiesCommand($providerId, $locationId));

		return $this->respondOk(
			(new LocationAvailabilityTransformer())->transformCollection($data)
		);
	}

	/**
	 * @api {put} /providers/:providerId/locations/:locationId/availabilities Location Availabilities Update
	 * @apiDescription Updates location availabilities.
	 * @apiName ajax-location-availabilities-update
	 * @apiGroup Provider Locations
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiParam {Number} locationId Location id
	 * @apiParam {String} name Location name
	 * @apiParam {String} address Address
	 * @apiParam {Number} location_type_id Location type
	 * @apiParam {Number{1..7}} postal_code Postal code
	 * @apiParam {Number{2}} country_code Country code
	 * @apiParam (Availability) {Array} availabilities Availabilities
	 * @apiParam (Availability) {Number} providerId Provider id
	 * @apiParam (Availability) {Number{0..6}} day Day
	 * @apiParam (Availability) {DateTime} from From
	 * @apiParam (Availability) {DateTime} until Until
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":[{"id":113,"provider_id":4,"location_id":67,"day":0,"from":{"date":"2015-05-29 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-05-29 02:00:00.000000","timezone_type":3,"timezone":"UTC"},"max_clients":1,"type":"single","waiting_for_remove":0,"created_at":"2015-05-29 11:15:37","updated_at":"2015-05-29 11:15:37","location":{"id":67,"name":"somename","location_type_id":1,"open_from":null,"open_till":null,"created_at":"2015-05-29 11:15:37","updated_at":"2015-05-29 11:15:37","is_verified":0,"verified_at":"2015-05-29 11:15:37"}},{"id":114,"provider_id":4,"location_id":67,"day":2,"from":{"date":"2015-05-29 02:00:00.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-05-29 04:00:00.000000","timezone_type":3,"timezone":"UTC"},"max_clients":1,"type":"single","waiting_for_remove":0,"created_at":"2015-05-29 11:15:37","updated_at":"2015-05-29 11:15:37","location":{"id":67,"name":"somename","location_type_id":1,"open_from":null,"open_till":null,"created_at":"2015-05-29 11:15:37","updated_at":"2015-05-29 11:15:37","is_verified":0,"verified_at":"2015-05-29 11:15:37"}},{"id":115,"provider_id":4,"location_id":67,"day":4,"from":{"date":"2015-05-29 04:00:00.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-05-29 06:00:00.000000","timezone_type":3,"timezone":"UTC"},"max_clients":1,"type":"single","waiting_for_remove":0,"created_at":"2015-05-29 11:15:38","updated_at":"2015-05-29 11:15:38","location":{"id":67,"name":"somename","location_type_id":1,"open_from":null,"open_till":null,"created_at":"2015-05-29 11:15:37","updated_at":"2015-05-29 11:15:37","is_verified":0,"verified_at":"2015-05-29 11:15:37"}}]}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.from Date is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"from":["From must be an date."]}}}
	 **/
	public function updateLocationAvailabilities($providerId, $locationId, ApiRequest $request)
	{
		$data = $request->all();
		$availabilities = $request->get('availabilities');

		$data['provider_id'] = $providerId;
		$data['location_id'] = $locationId;
		$data['availabilities'] = $availabilities;

		return $this->exec(new UpdateLocationAvailabilitiesCommand($data));
	}

	/**
	 * @api {delete} /providers/:providerId/locations/:locationId Location Delete
	 * @apiDescription Delete location and availabilities
	 * @apiName ajax-location-remove
	 * @apiGroup Provider Locations
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiParam {Number} locationId Location id
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":true}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.from Date is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"messages":{"providerId":["Provider id must be supplied."]}}}
	 **/
	public function removeProviderLocation($providerId, $locationId)
	{
		return $this->exec(new RemoveProviderLocationCommand($providerId, $locationId));
	}

	/**
	 * @api {get} /core/locations/search Location Search
	 * @apiDescription Search locations for auto complete.
	 * @apiName ajax-core-locations-search
	 * @apiGroup Core
	 *
	 * @apiParam {String} q The city or province or region or postal code to search for.
	 * @apiParam {Number} limit=30 Limit number of results (for each group if group=1)
	 * @apiParam {Number} minMatch=50 Minimal match percentage with q field
	 * @apiParam {String} search=city,province Comma-separated column names to search in.
	 * @apiParam {String} with=country Comma-separated relation names to grab.
	 * @apiParam {Number} group=1 Group results by search columns or not.
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *    {
	 *        "message": "success",
	 *        "data": [
	 *            {"country":"United States","city":"Camak","province":"GA","region":""}
	 *        ]
	 *    }
	 * @apiVersion 1.0.0
	 */

	/*
	 * Search Locations for auto complete
	 *
	 * @param Request $request
	 * @return mixed
	 */
	public function searchLocations(ApiRequest $request)
	{
		$data = $request->all();
		$data['search'] = $request->inputArr('search');
		$data['with'] = $request->inputArr('with');

		if (isset($data['q'])) {
			$data['q'] = rtrim($data['q'], "\\");
		}

		return $this->exec(new SearchLocationCommand($data), function ($result) {
			return (new PostCodeAutocompleteTransformer())->transformCollection(
				$result
			);
		});
	}

	/**
	 * @api {put} /providers/:providerId/locations/:locationId Location Update
	 * @apiDescription Update location fields
	 * @apiName ajax-provider-location-update
	 * @apiGroup Provider Locations
	 *
	 * @apiParam {Number} providerId Provider id
	 * @apiParam {Number} locationId Location id
	 * @apiParam {Array} name, address, postal_code, country_code Location fields
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *    {
	 *        "message": "success",
	 *        "data": true
	 *    }
	 * @apiVersion 1.0.0
	 */
	public function updateProviderLocation($providerId, $locationId, ApiRequest $request)
	{
		$updateFieldsArr = [
			'name',
			'address',
			'postal_code',
			'country_code',
			'service_area',
			'service_area_unit',
			'padding_time',
			'address_label',
			'address_line_two',
			'timezone'
		];

		$updateFields = [];
		foreach ($updateFieldsArr as $field) {
			if (array_key_exists($field, $request->all())) {
				$updateFields[$field] = $request->get($field);
			}
		}

		return $this->exec(new UpdateProviderLocationCommand(
			$providerId,
			$locationId,
			$updateFields
		), function ($result) {
			return (new LocationTransformer())->transform($result);
		});
	}

	/**
	 * @api {get} /core/locations/countries Location Countries
	 * @apiDescription Returns information like calling code, name and abbreviation for all countries
	 * @apiName ajax-location-types-get
	 * @apiGroup Core
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":[{code: "AF", name: "Afghanistan", calling_code: "93", rank: 10},{code: "AL", name: "Albania", calling_code: "355", rank: 20}]}
	 **/
	public function getCountries()
	{
		return $this->exec(new LoadLocationCountriesCommand());
	}

	/**
	 * @api {get} /locations/check-radius/{locationId}/{homeLocationId} Location Countries
	 * @apiName ajax-check-home-location-radius
	 * @apiGroup Provider Locations
	 *
	 * @apiParam {Number} locationId Provider location id
	 * @apiParam {Number} homeLocationId Client Location id
	 *
	 * @param $locationId
	 * @param $homeLocationId
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *    {
	 *        "message": "success",
	 *        "data": true
	 *    }
	 *
	 * @apiVersion 1.0.0
	 */
	public function checkHomeLocationRadius($locationId, $homeLocationId)
	{
		return $this->exec(new CheckHomeLocationRadiusCommand([
			'locationId' => $locationId,
			'homeLocationId' => $homeLocationId,
		]));
	}
}

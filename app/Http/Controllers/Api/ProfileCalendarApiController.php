<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\ProfileCalendar\Commands\DisconnectProviderCalendarCommand;
use WL\Modules\ProfileCalendar\Commands\GetProviderCalendarsCommand;
use WL\Modules\ProfileCalendar\Commands\InitProfileCalendarCommand;
use WL\Modules\ProfileCalendar\Commands\MarkCalendarAsFullSyncCommand;
use WL\Modules\ProfileCalendar\Transformers\CalendarAccountTransformer;
use WL\Modules\ProfileCalendar\Transformers\CalendarTransformer;


/**
 * Class ProfileCalendarApiController
 * @package App\Http\Controllers\Api
 */
class ProfileCalendarApiController extends ApiController
{

	/**
	 * @api {get} /schedule/calendar/calendars
	 * @apiDescription List all calendars
	 * @apiName ajax-calendar-calendars
	 * @apiGroup schedule
	 *
	 * @apiSuccess {String} message "success"
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *	{
	 *		"message": "success",
	 *	}
	 * @apiVersion 1.0.0
	 */

	public function calendars()
	{
		$data = [
			'profile_id' => ProfileService::getCurrentProfileId(),
		];
		$accounts = $this->dispatch(new GetProviderCalendarsCommand($data));
		return $this->respondOk((new CalendarAccountTransformer())->transformCollection($accounts));
	}

	/**
	 * @api {post} /schedule/calendar/connect/{driver}/{remoteId}
	 * @apiDescription Connect to remote calendar.
	 * @apiName ajax-calendar-connect
	 * @apiGroup schedule
	 *
	 * @apiParam {String} caledarId - API 3rd party calendar ID.
	 * @apiParam {Number} oauthId - API 3rd party OAuth token ID.
	 * @apiParam {Number} full_sync - Full sync calendar
	 *
	 * @apiSuccess {String} message "success"
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *	{
	 *		"message": "success",
	 *	}
	 * @apiVersion 1.0.0
	 */

	public function connect($calendarId, ApiRequest $request)
	{
		$data = [
			'profile_id' => ProfileService::getCurrentProfileId(),
			'external_id' => $calendarId,
			'oauthId' => $request->get('oauthId'),
		];
		$result = $this->dispatch(new InitProfileCalendarCommand($data));
		if($request->get('full_sync') == '1'){
			$result = $this->dispatch(new MarkCalendarAsFullSyncCommand(['calendar_id' => $result['local']->id, 'mark' => true]));
		}

		return $this->respondOk((new CalendarTransformer())->transform($result));
	}

	/**
	 * @api {put} /schedule/calendar/disconnect/{calendarId}
	 * @apiDescription Disconnect from a calendar.
	 * @apiName ajax-calendar-disconnect
	 * @apiGroup schedule
	 *
	 * @apiParam {Number} calendarId.
	 *
	 * @apiSuccess {String} message "success"
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *	{
	 *		"message": "success",
	 *	}
	 * @apiVersion 1.0.0
	 */

	public function disconnect($calendarId)
	{
	    return $this->respondOk(
			$this->dispatch(new DisconnectProviderCalendarCommand(['calendar_id' => $calendarId]))
		);
	}


	/**
	 * @api {put} /schedule/calendar/mark/{calendarId}
	 * @apiDescription Switch calendar between Full sync or One direction sync.
	 * @apiName ajax-calendar-mark
	 * @apiGroup schedule
	 *
	 * @apiParam {Number} calendarId.
	 *
	 * @apiSuccess {String} message "success"
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *	{
	 *		"message": "success",
	 *	}
	 * @apiVersion 1.0.0
	 */

	public function mark($calendarId)
	{
		$result = $this->dispatch(new MarkCalendarAsFullSyncCommand(['calendar_id' => $calendarId]));
		return $this->respondOk((new CalendarTransformer())->transform($result));
	}


}

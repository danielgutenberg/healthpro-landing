<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use WL\Modules\GiftCertificates\Commands\CreateGiftCertificateCommand;
use WL\Modules\GiftCertificates\Commands\GetProfileGiftCertificatesCommand;
use WL\Modules\GiftCertificates\Commands\RedeemGiftCertificateCommand;
use WL\Modules\GiftCertificates\Transformers\GiftCertificateTransformer;

class GiftCertificateController extends ApiController
{
	/**
	 * @api {get} /coupons/:coupon Gets coupon entity
	 * @apiDescription Gets coupon entity by name
	 * @apiName ajax-coupon-get
	 * @apiGroup Coupons
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 *      {"message":"success","data":{"id":112,"name":"somename","description":"some-description","applyTo":"WL\\Modules\\Order\\Models\\Order","amount":10,"amountType":"fixed","minimumTotal":null,"validFrom":"2016-08-24 21:20:28","validUntil":"2016-08-24 22:20:28","ownerId":null,"ownerType":null,"created_at":"2016-08-24 21:20:28","updated_at":"2016-08-24 21:20:28","max_usages":1,"actual_usages":0,"deleted_at":null,"issued_by":"staff"}}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 * @apiError (Error: 422) {String[]} messages.number Number is invalid or not specified.
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"coupon":{"messages":["Coupon is not exists","validation.coupon"],"status_code":"validation"}}}
	 **/
	public function createGiftCertificate(ApiRequest $request)
	{
		$data = $request->all();
		$certificate =  $this->runCommandWithErrorHandle(new CreateGiftCertificateCommand($data));

		return $this->respondOk((new GiftCertificateTransformer())->transform($certificate));
	}

	public function redeem(ApiRequest $request, $id)
	{
		$data = $request->all();
		$data['gift_certificate_id'] = $id;
		$certificate =  $this->runCommandWithErrorHandle(new RedeemGiftCertificateCommand($data));

		return $this->respondOk((new GiftCertificateTransformer())->transform($certificate));
	}

	public function getProfileCertificates()
	{
		$items = $this->runCommandWithErrorHandle(new GetProfileGiftCertificatesCommand());

		return $this->respondOk((new GiftCertificateTransformer())->transformCollection($items));
	}
}

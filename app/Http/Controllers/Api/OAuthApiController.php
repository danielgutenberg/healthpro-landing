<?php namespace App\Http\Controllers\Api;


use App\Http\Controllers\Api\ApiController;
use WL\Modules\OAuth\Commands\DisconnectProviderOAuth;

class OAuthApiController extends ApiController
{
    /**
     * @api {put} /oauth/:oauthId/disconnect
     * @apiDescription Disconnect from OAuth2.0 account
     * @apiName ajax-oauth-disconnect
     * @apiGroup OAuth2.0
     *
     * @apiParam {Number} oauthId
     *
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *
     * @apiVersion 1.0.0
     **/
    public function disconnect($oauthId)
    {
        $this->dispatch(new DisconnectProviderOAuth($oauthId));

        return $this->respondOk();
    }
}
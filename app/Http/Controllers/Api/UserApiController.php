<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use WL\Modules\User\Commands\ChangeUserEmailCommand;
use WL\Modules\User\Commands\ChangeUserPasswordCommand;
use WL\Modules\User\Transformers\UserTransformer;
use WL\Modules\User\Commands\LoadCurrentUserCommand;

class UserApiController extends ApiController
{
	/**
	 * @api {get} /user/me User Get
	 * @apiDescription Gets user data.
	 * @apiName ajax-user-get
	 * @apiGroup User
	 *
	 * @apiVersion 1.0.0
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 * {"message":"success","data":{"user":{"email":"user@example.com"},"user_password_required":false}}
	 *
	 * @return mixed
	 */
	public function get()
	{
		list($user, $manualPasswordRequired) = $this->dispatch(new LoadCurrentUserCommand());
		$user->manualPasswordRequired = $manualPasswordRequired;
		$user->emailVerified = $user->isActivated();
		return $this->respondOk((new UserTransformer())->transform($user));
	}

	/**
	 * @api {put} /user/me User Update
	 * @apiDescription Updates user email or password.
	 * @apiName ajax-user-update-put
	 * @apiGroup User
	 *
	 * @apiParam {String {7..255}} current_password
	 * @apiParam (Change Email) {String} [email]
	 * @apiParam (Change Password) {String {7..255}} [password]
	 * @apiParam (Change Password) {String {7..255}} [confirmed_password]
	 * @apiVersion 1.0.0
	 *
	 *  @apiSuccessExample {json} Success-Response:
	 *      HTTP/1.1 200 OK
	 * {"message":"success"}
	 *
	 * @apiError (Error: 422) {Object} messages An object with validation messages for each field
	 *
	 * @apiErrorExample {json} Error-Response:
	 *     HTTP/1.1 422
	 *     {"message":"error","errors":{"email":{"messages":["This is your current e-mail address. Specify any other e-mail."],"status_code":"validation"}}}
	 *
	 * @param Request $request
	 * @return mixed
	 */
	public function update(Request $request)
	{
		$data = $request->all();
		$email = isset($data['email']) ? $data['email']: '';
		$currentPassword = isset($data['current_password']) ? $data['current_password']: '';
		$oldEmail = isset($data['old_email']) ? $data['old_email']: '';
		$password = isset($data['password']) ? $data['password']: '';
		$confirmedPassword = isset($data['confirmed_password']) ? $data['confirmed_password']: '';

		$result = $email ?
				$this->runCommandWithErrorHandle(new ChangeUserEmailCommand($email, $currentPassword, $oldEmail)) :
				$this->runCommandWithErrorHandle(new ChangeUserPasswordCommand($currentPassword, $password, $confirmedPassword));

		return $this->respondOk($result);
	}
}

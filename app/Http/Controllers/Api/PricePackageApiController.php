<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use WL\Modules\Location\Commands\AddLocationAppointmentsCommand;
use WL\Modules\Order\Transformers\FullOrderWithItemsTransformer;
use WL\Modules\Order\Transformers\OrderTransformer;
use WL\Modules\PricePackage\Commands\AddPackageScheduleCommand;
use WL\Modules\PricePackage\Commands\AddPricePackageCommand;
use WL\Modules\PricePackage\Commands\CanPurchasePricePackageCommand;
use WL\Modules\PricePackage\Commands\ChargePricePackageCommand;
use WL\Modules\PricePackage\Commands\DeletePricePackageCommand;
use WL\Modules\PricePackage\Commands\GetChargedPackageOfEntityCommand;
use WL\Modules\PricePackage\Commands\GetEntityPricePackagesCommand;
use WL\Modules\PricePackage\Commands\GetPricePackageCommand;
use WL\Modules\PricePackage\Commands\GetProfilePricePackagesCommand;
use WL\Modules\PricePackage\Commands\GetPurchasedPricePackageCommand;
use WL\Modules\PricePackage\Commands\GetPurchasedPricePackagesCommand;
use WL\Modules\PricePackage\Commands\SetPackageNotActiveCommand;
use WL\Modules\PricePackage\Commands\UpdatePackageAutoRenewCommand;
use WL\Modules\PricePackage\Commands\UpdatePackageScheduleCommand;
use WL\Modules\PricePackage\Commands\UpdatePricePackageCommand;
use WL\Modules\PricePackage\Transformers\ChargedPricePackageTransformer;
use WL\Modules\PricePackage\Transformers\FullPurchasedPricePackageTransformer;
use WL\Modules\PricePackage\Transformers\PricePackageTransformer;
use WL\Modules\Profile\Facades\ProfileService;

class PricePackageApiController extends ApiController
{

	/**
	 * @ api {post} /profiles/:profileId/packages Add Price Package
	 * @apiDescription Associate price Package with entity and profile
	 * @apiName api-price-package-add
	 * @apiGroup PricePackage
	 *
	 * @apiParam {Number} profileId Profile ID
	 * @apiParam {Integer} entity_id
	 * @apiParam {String} entity_type
	 * @apiParam {String} title
	 * @apiParam {String} description
	 * @apiParam {Float} price
	 * @apiParam {Integer} number_of_visits
	 *
	 * @apiSuccessExample
	 * {json} "{"message":"success","data":{"id":23,"title":"priceTitle","description":"priceDescription","price":99.9,"number_of_visits":10,"entity_id":1,"entity_type":"EntityType"}}"
	 *
	 * @apiErrorExample {json}
	 * {"message":"error","errors":{"messages":{"entity_id":["validation.required"],"entity_type":["validation.required"],"title":["validation.required"],"description":["validation.required"],"price":["validation.required"],"number_of_visits":["validation.required"]}}}
	 *
	 */

	public function addPricePackage(ApiRequest $request, $profileId)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = $profileId;
		$pack = $this->runCommandWithErrorHandle(new AddPricePackageCommand($inpData));

		return $this->respondOk((new PricePackageTransformer())->transform($pack));
	}



	/**
	 * @ api {put} /profiles/:profileId/packages/:id Update Price Package
	 * @apiDescription Update Price Package
	 * @apiName api-price-package-update
	 * @apiGroup PricePackage
	 *
	 * @apiParam {Integer} profileId Profile ID
	 * @apiParam {Integer} id PricePackage id
	 * @apiParam {String} title
	 * @apiParam {String} description
	 * @apiParam {Float} price
	 * @apiParam {Integer} number_of_visits
	 */
	public function updatePricePackage(ApiRequest $request, $profileId, $id)
	{
		$inpData = $request->all();
		$inpData['package_id'] = $id;
		$inpData['profile_id'] = $profileId;
		$data = $this->runCommandWithErrorHandle(new UpdatePricePackageCommand($inpData));
		return $this->respondOk($data);
	}


	/**
	 * @ api {get} /profiles/:profileId/packages/:id Get PricePackage
	 * @apiDescription get PricePackage
	 * @apiName api-price-package-get
	 * @apiGroup PricePackage
	 *
	 * @apiParam {Integer} profileId Profile ID
	 * @apiParam {Integer} id PricePackage id
	 *
	 * @apiSuccessExample {json}
	 * "{"message":"success","data":{"id":25,"title":"priceTitle","description":"priceDescription","price":99.9,"number_of_visits":10,"entity_id":1,"entity_type":"EntityType"}}"
	 *
	 */
	public function getPricePackage(ApiRequest $request, $profileId, $id)
	{
		$inpData['package_id'] = $id;
		$inpData['profile_id'] = $profileId;

		$data = $this->runCommandWithErrorHandle(new GetPricePackageCommand($inpData));

		return $this->respondOk((new PricePackageTransformer())->transform($data));
	}

	/**
	 * @ api {delete} /profiles/:profileId/packages/:id Delete PricePackage
	 * @apiDescription delete PricePackage
	 * @apiName api-price-package-delete
	 * @apiGroup PricePackage
	 *
	 * @apiParam {Integer} profileId Profile ID
	 * @apiParam {Integer} id PricePackage id
	 */
	public function deletePricePackage(ApiRequest $request, $profileId, $id)
	{
		$inpData['package_id'] = $id;
		$inpData['profile_id'] = $profileId;

		return $this->exec(new DeletePricePackageCommand($inpData));
	}


	/**
	 * @ api {get} /profiles/:profileId/packages/entity/:entityId/:entity Get Entity PricePackages
	 * @apiDescription get all PricePackages associated with entity
	 * @apiName api-price-package-entity-packages
	 * @apiGroup PricePackage
	 *
	 * @apiParam {Integer} profileId Profile ID
	 * @apiParam {Integer} entity_id
	 * @apiParam {String} entity_type
	 */
	public function getEntityPricePackages(ApiRequest $request, $profileId, $entityId, $entityType)
	{
		$inpData = [
			'profile_id' => $profileId,
			'entity_id' => $entityId,
			'entity_type' => $entityType
		];

		$items = $this->runCommandWithErrorHandle(new GetEntityPricePackagesCommand($inpData));

		return $this->respondOk((new PricePackageTransformer())->transformCollection($items));
	}

	/**
	 * @ api {get} /profiles/:profileId/packages Get Profile PricePackages
	 * @apiDescription get all PricePackages associated with Profile
	 * @apiName api-price-package-profile-packages
	 * @apiGroup PricePackage
	 *
	 * @apiParam {Integer} profileId Profile ID
	 */
	public function getProfilePricePackages(ApiRequest $request, $profileId)
	{
		$inpData = [
			'profile_id' => $profileId
		];

		$items = $this->runCommandWithErrorHandle(new GetProfilePricePackagesCommand($inpData));

		return $this->respondOk((new PricePackageTransformer())->transformCollection($items));
	}

	public function getPurchasedPricePackages()
	{
		$inpData = [
			'profile_id' => ProfileService::getCurrentProfileId()
		];

		$items = $this->runCommandWithErrorHandle(new GetPurchasedPricePackagesCommand($inpData));

		return $this->respondOk((new FullPurchasedPricePackageTransformer())->transformCollection($items));
	}

	public function getPurchasedPricePackage($id)
	{
		$inpData = [
			'profile_id' => ProfileService::getCurrentProfileId(),
			'package_id' => $id
		];

		$package = $this->runCommandWithErrorHandle(new GetPurchasedPricePackageCommand($inpData));

		return $this->respondOk((new FullPurchasedPricePackageTransformer())->transform($package));
	}

	public function chargePricePackage(ApiRequest $request, $packageId, $clientId)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = $clientId;
		$inpData['package_id'] = $packageId;

		$data = $this->runCommandWithErrorHandle(new ChargePricePackageCommand($inpData));

		return $this->respondOk($data);
	}

	public function getChargePricePackage(ApiRequest $request, $clientId, $entityType, $entityId)
	{
		$inpData['profile_id'] = $clientId;
		$inpData['entity_id'] = $entityId;
		$inpData['entity_type'] = $entityType;

		$package = $this->runCommandWithErrorHandle(new GetChargedPackageOfEntityCommand($inpData));

		return $this->respondOk((new ChargedPricePackageTransformer())->transform($package));
	}

	public function updateSchedule(ApiRequest $request, $pricePackageChargeId, $recurringScheduleId)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();
		$inpData['package_id'] = $pricePackageChargeId;
		$inpData['schedule_id'] = $recurringScheduleId;

		$package = $this->runCommandWithErrorHandle(new UpdatePackageScheduleCommand($inpData));

		return $this->respondOk((new ChargedPricePackageTransformer())->transform($package));
	}

	public function addSchedule(ApiRequest $request, $pricePackageChargeId)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();
		$inpData['package_id'] = $pricePackageChargeId;

		$package = $this->runCommandWithErrorHandle(new AddPackageScheduleCommand($inpData));

		return $this->respondOk((new FullOrderWithItemsTransformer())->transform($package));
	}

	public function updateAutoRenew(ApiRequest $request, $pricePackageChargeId)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();
		$inpData['package_id'] = $pricePackageChargeId;

		$package = $this->runCommandWithErrorHandle(new UpdatePackageAutoRenewCommand($inpData));

		return $this->respondOk((new ChargedPricePackageTransformer())->transform($package));
	}

	public function cancelPackage(ApiRequest $request, $pricePackageChargeId)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();
		$inpData['package_id'] = $pricePackageChargeId;

		$package = $this->runCommandWithErrorHandle(new SetPackageNotActiveCommand($inpData));

		return $this->respondOk((new ChargedPricePackageTransformer())->transform($package));
	}

	public function canPurchasePackage(ApiRequest $request, $sessionId)
	{
		$inpData = $request->all();
		$inpData['session_id'] = $sessionId;

		return $this->exec(new CanPurchasePricePackageCommand($inpData));
	}
}

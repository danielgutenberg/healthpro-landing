<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use WL\Modules\Classes\ClientCommands\BookMultipleClassAvailabilitiesCommand;
use WL\Modules\Classes\ClientCommands\CancelInRangeClassRegistrationsCommand;
use WL\Modules\Classes\ClientCommands\InstantBookClassAvailabilityCommand;
use WL\Modules\Classes\ClientCommands\InstantCancelClassRegistrationCommand;
use WL\Modules\Classes\ClientCommands\GetClientUpcomingClassesCommand;
use WL\Modules\Classes\Commands\AddAvailabilityToClassesCommand;
use WL\Modules\Classes\Commands\AssociateTagsWithClassesCommand;
use WL\Modules\Classes\Commands\CreateClassesCommand;
use WL\Modules\Classes\Commands\DeleteClassesAssetCommand;
use WL\Modules\Classes\Commands\DeleteClassesAvailabilityCommand;
use WL\Modules\Classes\Commands\DeleteClassesCommand;
use WL\Modules\Classes\Commands\GetClassesCommand;
use WL\Modules\Classes\Commands\GetAllClassesCommand;
use WL\Modules\Classes\Commands\GetClassRegistrationsOnDateCommand;
use WL\Modules\Classes\Commands\GetUpcomingClassesCommand;
use WL\Modules\Classes\Commands\UpdateClassesAssetCommand;
use WL\Modules\Classes\Commands\UpdateClassesAvailabilityCommand;
use WL\Modules\Classes\Commands\UpdateClassesCommand;
use WL\Modules\Classes\Commands\UploadAssetToClassesCommand;
use WL\Modules\Classes\Model\Classes;
use WL\Modules\Classes\Transformer\ClassesAssetTransformer;
use WL\Modules\Classes\Transformer\ClassesTransformer;
use WL\Modules\PricePackage\Commands\AddPricePackageCommand;
use WL\Modules\PricePackage\Commands\DeletePricePackageCommand;
use WL\Modules\PricePackage\Commands\GetChargedVisitsOfEntityCommand;
use WL\Modules\PricePackage\Commands\GetPricePackageCommand;
use WL\Modules\PricePackage\Commands\UpdatePricePackageCommand;
use WL\Modules\PricePackage\Transformers\PricePackageTinyTransformer;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Schedule\Transformer\ClassesRegistrationTransformer;
use WL\Schedule\Transformer\ScheduleAvailabilityTransformer;
use WL\Schedule\Transformer\ScheduleRegistrationTransformer;
use WL\Schedule\Transformer\ScheduleRegistrationWithLoadedClientsTransformer;

class ClassesApiController extends ApiController
{
	/*
	 * Class
	 */

	/**
	 * @api {GET} /classes/:id Class Single Get by Id
	 * @apiDescription  Get Class by Id
	 * @apiName api-profile-class-get
	 * @apiGroup Classes
	 *
	 * @apiSuccessExample {json} Success-Response:
	 * "{"message":"success","data":{"id":117,"trial":0,"name":"some new class","profile_id":4,"description":"description","assets":{"photo":[{"id":36,"title":"some title","description":"some description","type":"photo","file":{"original":"1\/classes\/cce5c19b8dc05ad495ddfe963dd8f3b1.jpg","215x140":"1\/classes\/cce5c19b8dc05ad495ddfe963dd8f3b1-215x140.jpg","90x90":"1\/classes\/cce5c19b8dc05ad495ddfe963dd8f3b1-90x90.jpg"}}]},"tags":{"help-category":[{"id":752,"name":"Affiliate Program","slug":"affiliate-program","parent_id":null,"is_alias":0,"is_custom":0},{"id":750,"name":"Dashboard","slug":"dashboard","parent_id":null,"is_alias":0,"is_custom":0}]},"availabilities":[{"id":91,"provider_id":4,"day":4,"from":"01:00","until":"04:00","type":"class","location_id":1,"max_clients":5}],"rating":null,"packages":[{"id":70,"price":50,"number_of_visits":10}]}}"
	 */
	public function getClass(ApiRequest $request, $id)
	{
		$data = $this->runCommandWithErrorHandle(new GetClassesCommand(['class_id' => $id]));

		return $this->respondOk( (new ClassesTransformer())->transform($data));
	}

	/**
	 * @api {GET} /provider/:providerId/classes/ Provider Classes Get
	 * @apiDescription Gets all classes owned by a profile. If upcoming=false receive all Profile Classes, if true receive only upcoming classes
	 * @apiName api-profile-classes-get
	 * @apiGroup Classes Provider
	 *
	 * @apiParam {boolean} upcoming
	 *
	 * @apiSuccessExample {json} when upcoming not set
	 * "{"message":"success","data":[{"id":137,"trial":0,"name":"some new class","profile_id":4,"description":"description","price":40,"assets":[],"tags":[],"availabilities":[],"rating":null,"packages":[{"id":90,"title":"someTitle","description":"SomeDescr","price":50,"number_of_visits":10,"entity_id":137,"entity_type":"WL\\Modules\\Classes\\Model\\Classes"}]},{"id":138,"trial":0,"name":"some new class","profile_id":4,"description":"description","assets":[],"tags":[],"availabilities":[],"rating":null,"packages":[{"id":91,"title":"someTitle","description":"SomeDescr","price":50,"number_of_visits":10,"entity_id":138,"entity_type":"WL\\Modules\\Classes\\Model\\Classes"}]}]}"
	 * @apiSuccessExample {json} when upcoming=true
	 * "{"message":"success","data":[{"id":105,"availability_id":106,"from":{"date":"2015-06-26 01:00:20.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-06-26 04:00:20.000000","timezone_type":3,"timezone":"UTC"},"type":"regular","current_user_count":2,"clients":[{"id":4,"title":null,"gender":null,"first_name":"Ghost Anakin","last_name":"Skywalker","full_name":"Ghost Anakin Skywalker","avatar":"1\/profiles\/89e1a2b1e199ac31d4512e365f269e2a-250x250.jpg"},{"id":6,"title":null,"gender":null,"first_name":null,"last_name":null,"full_name":" ","avatar":null}]},{"id":106,"availability_id":107,"from":{"date":"2015-06-27 01:00:20.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-06-27 04:00:20.000000","timezone_type":3,"timezone":"UTC"},"type":"regular","current_user_count":1,"clients":[{"id":4,"title":null,"gender":null,"first_name":"Ghost Anakin","last_name":"Skywalker","full_name":"Ghost Anakin Skywalker","avatar":"1\/profiles\/89e1a2b1e199ac31d4512e365f269e2a-250x250.jpg"}]}]}"
	 */
	public function getClasses(ApiRequest $request, $profileId)
	{
		if (!$request->get('upcoming', false)) {
			$data = $this->runCommandWithErrorHandle(new GetAllClassesCommand(['profile_id' => $profileId]));
			return $this->respondOk( (new ClassesTransformer())->transformCollection($data));
		}

		$data = $this->runCommandWithErrorHandle(new GetUpcomingClassesCommand(['profile_id' => $profileId]));
		return $this->respondOk( (new ScheduleRegistrationWithLoadedClientsTransformer())->transformCollection($data));

	}


	/**
	 * @api {POST} /classes/ Class Single Create
	 * @apiDescription  Create Class
	 * @apiName api-profile-class-create
	 * @apiGroup Classes
	 * @apiParam {String} 	name
	 * @apiParam {String} 	description
	 * @apiParam {Boolean} 	trial
	 * @apiParam {Array} 	[packages]
	 * @apiParam {Integer} 		packages.number_of_visits
	 * @apiParam {Float} 		packages.price
	 * @apiParam {String} 		[packages.title]
	 * @apiParam {String} 		[packages.description]
	 *
	 * @apiSuccessExample {json} Success-Response:
	 * "{"message":"success","data":{"id":98,"trial":false,"name":"some new class","profile_id":4,"description":"description","price":40,"assets":[],"tags":[],"availabilities":[],"rating":null,"packages":[{"id":51,"number_of_visits":10,"price":99.3}]}}"
	 */
	public function createClass(ApiRequest $request)
	{
		$inpData = $request->all();

		$data = $this->runCommandWithErrorHandle(new CreateClassesCommand($inpData));

		return $this->respondOk( (new ClassesTransformer())->transform($data));
	}

	/**
	 * @api {PUT} /classes/:id Class Single Update
	 * @apiDescription  Update Class
	 * @apiName api-profile-class-update
	 * @apiGroup Classes
	 * @apiParam {String} name
	 * @apiParam {String} description
	 * @apiParam {Boolean} trial
	 *
	 * @apiSuccessExample {json} Success-Response:
	 * "{"message":"success","data":true}"
	 */
	public function updateClass(ApiRequest $request, $id)
	{
		$inpData = $request->all();
		$inpData['class_id'] = $id;

		$data = $this->runCommandWithErrorHandle(new UpdateClassesCommand($inpData));

		return $this->respondOk( $data);
	}

	/**
	 * @api {DELETE} /classes/:id Class Single Remove
	 * @apiDescription  Delete Class
	 * @apiName api-profile-class-delete
	 * @apiGroup Classes
	 *
	 * @apiSuccessExample {json} Success-Response:
	 * "{"message":"success","data":true}"
	 */
	public function deleteClass(ApiRequest $request, $id)
	{
		$inpData['class_id'] = $id;

		$data = $this->runCommandWithErrorHandle(new DeleteClassesCommand($inpData));

		return $this->respondOk( $data);
	}


	/*
	 * Provider Class Asset
	 */

	/**
	 * @api {POST} /classes/:id/asset Class Asset Upload
	 * @apiDescription  Upload ClassAsset to Class
	 * @apiName api-profile-class-asset-upload
	 * @apiGroup Classes
	 * @apiParam {String} title
	 * @apiParam {String} description
	 * @apiParam {String} type
	 * @apiParam {File} file
	 *
	 * @apiSuccessExampl {json} Success-Response:
	 * "{"message":"success","data":{"id":44,"title":"some title","description":"some description","type":"photo","file":{"original":"1\/classes\/8a82deb710fc14e403b2d5c24fe9e719.jpg","215x140":"1\/classes\/8a82deb710fc14e403b2d5c24fe9e719-215x140.jpg","90x90":"1\/classes\/8a82deb710fc14e403b2d5c24fe9e719-90x90.jpg"}}}"
	 */
	public function uploadClassAsset(ApiRequest $request, $classId)
	{
		$inpData = $request->all();
		$inpData['class_id'] = $classId;

		$data = $this->runCommandWithErrorHandle(new UploadAssetToClassesCommand($inpData));

		return $this->respondOk( (new ClassesAssetTransformer())->transform($data));
	}

	/**
	 * @api {PUT} /classes/:classId/asset/:id Class Asset Update
	 * @apiDescription  Update ClassesAsset
	 * @apiName api-profile-class-asset-update
	 * @apiGroup Classes
	 * @apiParam {String} title
	 * @apiParam {String} description
	 * @apiParam {String} type
	 *
	 * @apiSuccessExample {json} Success-Response:
	 * "{"message":"success","data":true}"
	 */

	public function updateClassAsset(ApiRequest $request, $classId, $id)
	{
		$inpData = $request->all();
		$inpData['class_id'] = $classId;
		$inpData['asset_id'] = $id;

		$data = $this->runCommandWithErrorHandle(new UpdateClassesAssetCommand($inpData));


		return $this->respondOk( $data);
	}

	/**
	 * @api {DELETE} /classes/:classId/asset/:id Class Asset Delete
	 * @apiDescription  Delete ClassAsset
	 * @apiName api-profile-class-asset-delete
	 * @apiGroup Classes
	 *
	 * @apiSuccessExample {json} Success-Response:
	 * "{"message":"success","data":true}"
	 *
	 */

	public function deleteClassAsset(ApiRequest $request, $classId, $id)
	{
		$inpData = [
			'class_id' => $classId,
			'asset_id' => $id
		];

		$data = $this->runCommandWithErrorHandle(new DeleteClassesAssetCommand($inpData));

		return $this->respondOk( $data);
	}


	/*
	 * Class Tags
	 */

	/**
	 * @api {PUT} /classes/:classId/tags Class Tags Update
	 * @apiDescription  Associate tags with Class
	 * @apiName api-profile-class-tags-associate
	 * @apiGroup Classes
	 * @apiParam {TagIds} tags
	 *
	 * @apiSuccessExample {json} Success-Response:
	 * "{"message":"success","data":true}"
	 */

	public function associateTagsWithClass(ApiRequest $request, $classId)
	{
		$inpData = $request->all();
		$inpData['class_id'] = $classId;

		$data = $this->runCommandWithErrorHandle(new AssociateTagsWithClassesCommand($inpData));

		return $this->respondOk( $data);
	}


	/*
	 * Class Availability
	 */

	/**
	 * @api {POST} /classes/:classId/schedule Class Schedule Add
	 * @apiDescription  Add Schedule To Class
	 * @apiName api-profile-class-schedule-add
	 * @apiGroup Classes
	 * @apiParam {Integer} day
	 * @apiParam {Time} from
	 * @apiParam {Time} until
	 * @apiParam {LocationId} location_id
	 * @apiParam {Integer} max_clients
	 *
	 * @apiSuccessExample  {json} Success-Response:
	 * "{"message":"success","data":{"id":109,"provider_id":4,"day":4,"from":"01:00","until":"04:00","type":"class","location_id":1,"max_clients":5}}"
	 */

	public function addClassAvailability(ApiRequest $request, $classId)
	{
		$inpData = $request->all();
		$inpData['class_id'] = $classId;

		$data = $this->runCommandWithErrorHandle(new AddAvailabilityToClassesCommand($inpData));

		return $this->respondOk( (new ScheduleAvailabilityTransformer())->transform($data));
	}

	/**
	 * @api {PUT} /classes/:classId/schedule/:id Class Schedule Update
	 * @apiDescription  Update Schedule
	 * @apiName api-profile-class-schedule-update
	 * @apiGroup Classes
	 * @apiParam {Integer} day
	 * @apiParam {Time} from
	 * @apiParam {Time} until
	 * @apiParam {LocationId} location_id
	 * @apiParam {Integer} max_clients
	 *
	 * @apiSuccessExample  {json} Success-Response:
	 * "{"message":"success","data":true}"
	 *
	 */
	public function updateClassAvailability(ApiRequest $request, $classId, $id)
	{
		$inpData = $request->all();
		$inpData['class_id'] = $classId;
		$inpData['availability_id'] = $id;

		$data = $this->runCommandWithErrorHandle(new UpdateClassesAvailabilityCommand($inpData));

		return $this->respondOk( $data);
	}

	/**
	 * @api {DELETE} /classes/:classId/schedule/:id Class Schedule Delete
	 * @apiDescription  Delete Schedule From Class
	 * @apiName api-profile-class-schedule-update
	 * @apiGroup Classes
	 *
	 * @apiSuccessExample  {json} Success-Response:
	 * "{"message":"success","data":true}"
	 *
	 */

	public function deleteClassAvailability(ApiRequest $request, $classId, $id)
	{
		$inpData = [
			'class_id' => $classId,
			'availability_id' => $id
		];

		$data = $this->runCommandWithErrorHandle(new DeleteClassesAvailabilityCommand($inpData));

		return $this->respondOk( $data);
	}


	/**
	 * @api {GET} classes/:classId/registrations/:date Class Registrations Get by Date
	 * @apiDescription  Get Class registrations on date
	 * @apiName api-profile-classes-get-registrations
	 * @apiGroup Classes
	 *
	 * @apiParam {Integer} classId
	 * @apiParam {Date}    date
	 * @apiSuccessExample  {json} Success-Response:
	 * "{"message":"success","data":[{"id":109,"availability_id":120,"from":"2015-06-26 01:00:24","until":"2015-06-26 04:00:24","current_user_count":2,"clients":[{"id":4,"title":null,"gender":null,"first_name":"Ghost Anakin","last_name":"Skywalker","full_name":"Ghost Anakin Skywalker","avatar":"1\/profiles\/89e1a2b1e199ac31d4512e365f269e2a-250x250.jpg"},{"id":6,"title":null,"gender":null,"first_name":null,"last_name":null,"full_name":" ","avatar":null}]},{"id":110,"availability_id":121,"from":"2015-06-27 01:00:24","until":"2015-06-27 04:00:24","current_user_count":1,"clients":[{"id":4,"title":null,"gender":null,"first_name":"Ghost Anakin","last_name":"Skywalker","full_name":"Ghost Anakin Skywalker","avatar":"1\/profiles\/89e1a2b1e199ac31d4512e365f269e2a-250x250.jpg"}]}]}"
	 */

	public function getClassRegistrationsOnDate(ApiRequest $request, $classId, $date)
	{
		$inpData = [
			'class_id' => $classId,
			'date' => $date,
			'profile_id' => ProfileService::getCurrentProfileId()
		];

		$items = $this->runCommandWithErrorHandle(new GetClassRegistrationsOnDateCommand($inpData));

		return $this->respondOk( (new ClassesRegistrationTransformer())->transformCollection($items));
	}


	/**
	 * Client routes
	 */


	/**
	 * @api {POST} /clients/:clientId/classes/:classId Class Registrations Book
	 * @apiDescription  It books some Availability from specified Class in specified date Range
	 * @apiName api-client-class-book
	 * @apiGroup Classes Client
	 *
	 * @apiParam {Integer} clientId
	 * @apiParam {Integer} classId
	 *
	 * @apiParam {Date} date_from
	 * @apiParam {Date} date_until
	 * @apiParam {Array} availability_ids Array of ids ( availability id1: was 'mon 14:00 till 16:00' so every monday in date_from till date_until will be book if there client can be registred)
	 *
	 * @apiSuccessExample  {json} Success-Response:
	 * "{"message":"","data":{"12":{"01-01-2015":32,"08-01-2015":33,"15-01-2015":34,"22-01-2015":35,"29-01-2015":36}},"errors":[]}" registration date: registrationID
	 */

	public function bookClassesAvailability(ApiRequest $request, $clientId, $classId)
	{
		$inpData = [
			'class_id' => $classId,
			'client_id' => $clientId
		];

		$inpData = array_merge($inpData, $request->all());

		$data = $this->runCommandWithErrorHandle(new BookMultipleClassAvailabilitiesCommand($inpData));


		return $this->respondOk( $data);
	}


	/**
	 * @api {DELETE} /clients/:clientId/classes/ Class Registrations Cancel
	 * @apiDescription  It cancel registrations, if cancellation rules allowed, will be money back
	 * @apiName api-client-class-cancel-multiply
	 * @apiGroup Classes Client
	 *
	 * @apiParam {Integer} clientId
	 *
	 * @apiParam {Date} date_from
	 * @apiParam {Date} date_until
	 *
	 * @apiParam {Array} availability_ids Array of ids
	 *
	 * @apiSuccessExample  {json} Success-Response:
	 * "{"message":"","data":{"1":{"1":{"registration":true,"pay":false},"2":{"registration":true,"pay":false},"3":{"registration":true,"pay":false},"4":{"registration":true,"pay":false},"5":{"registration":true,"pay":false}}},"errors":[]}" what cancelled `registration` and if refund `pay`
	 */

	public function cancelMultipleClassRegistration(ApiRequest $request, $clientId)
	{
		$inpData = $request->all();
		$inpData['client_id'] = $clientId;

		$data = $this->runCommandWithErrorHandle(new CancelInRangeClassRegistrationsCommand($inpData));

		return $this->respondOk( $data);
	}

	/**
	 * @api {GET} /clients/:clientId/classes Class Registrations Get
	 * @apiDescription Upcoming Classes registrations
	 * @apiName api-client-class-upcoming
	 * @apiGroup Classes Client
	 *
	 * @apiParam {Integer} clientId
	 *
	 * @apiSuccessExample  {json} Success-Response:
	 * "{"message":"success","data":[{"id":113,"availability_id":125,"date":"2015-07-02 01:00:00","unlock_time":null,"type":"regular","current_user_count":1,"created_at":"2015-06-26 08:48:52","updated_at":"2015-06-26 08:48:52","freeDurationLeft":null,"freeDurationRight":null,"from":"2015-07-02 01:00:00","until":"2015-07-02 04:00:00","classes":{"id":161,"profile_id":4,"name":"some new class","description":"description","trial":0,"price":40,"created_at":"2015-06-26 08:48:52","updated_at":"2015-06-26 08:48:52","availabilities":[{"id":125,"provider_id":4,"location_id":1,"day":4,"from":{"date":"2015-06-26 01:00:00.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-06-26 04:00:00.000000","timezone_type":3,"timezone":"UTC"},"max_clients":5,"type":"class","waiting_for_remove":0,"created_at":"2015-06-26 08:48:52","updated_at":"2015-06-26 08:48:52"},{"id":126,"provider_id":4,"location_id":1,"day":5,"from":{"date":"2015-06-26 01:00:00.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-06-26 04:00:00.000000","timezone_type":3,"timezone":"UTC"},"max_clients":5,"type":"class","waiting_for_remove":0,"created_at":"2015-06-26 08:48:52","updated_at":"2015-06-26 08:48:52"}],"assets":[],"tags":[],"rating":null,"packages":[]}},{"id":114,"availability_id":126,"date":"2015-07-03 01:00:00","unlock_time":null,"type":"regular","current_user_count":1,"created_at":"2015-06-26 08:48:52","updated_at":"2015-06-26 08:48:52","freeDurationLeft":null,"freeDurationRight":null,"from":"2015-07-03 01:00:00","until":"2015-07-03 04:00:00","classes":{"id":161,"profile_id":4,"name":"some new class","description":"description","trial":0,"price":40,"created_at":"2015-06-26 08:48:52","updated_at":"2015-06-26 08:48:52","availabilities":[{"id":125,"provider_id":4,"location_id":1,"day":4,"from":{"date":"2015-06-26 01:00:00.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-06-26 04:00:00.000000","timezone_type":3,"timezone":"UTC"},"max_clients":5,"type":"class","waiting_for_remove":0,"created_at":"2015-06-26 08:48:52","updated_at":"2015-06-26 08:48:52"},{"id":126,"provider_id":4,"location_id":1,"day":5,"from":{"date":"2015-06-26 01:00:00.000000","timezone_type":3,"timezone":"UTC"},"until":{"date":"2015-06-26 04:00:00.000000","timezone_type":3,"timezone":"UTC"},"max_clients":5,"type":"class","waiting_for_remove":0,"created_at":"2015-06-26 08:48:52","updated_at":"2015-06-26 08:48:52"}],"assets":[],"tags":[],"rating":null,"packages":[]}}]}"
	 */

	public function getUpcomingClasses(ApiRequest $request, $clientId)
	{
		$inpData = [
			'from' => Carbon::now(),
			'client_id' => $clientId
		];

		$data = $this->runCommandWithErrorHandle(new GetClientUpcomingClassesCommand($inpData));

		return $this->respondOk( $data);
	}

	//Classes Price Packages

	/**
	 * @api {post} /classes/:classId/packages Class Price Package Add
	 * @apiDescription Associate price Package with Class and Profile
	 * @apiName api-classes-price-package-add
	 * @apiGroup Classes
	 *

	 * @apiParam {Integer} classId
	 * @apiParam {String} title
	 * @apiParam {String} description
	 * @apiParam {Float} price
	 * @apiParam {Integer} number_of_visits
	 *
	 * @apiSuccessExample {json} Success-Response:
	 * {"message":"success","data":{"id":23,"price":99.9,"number_of_visits":10}}
	 *
	 * @apiErrorExample {json} Success-Response:
	 * {"message":"error","errors":{"messages":{"entity_id":["validation.required"],"entity_type":["validation.required"],"title":["validation.required"],"description":["validation.required"],"price":["validation.required"],"number_of_visits":["validation.required"]}}}
	 *
	 */

	public function addClassesPricePackage(ApiRequest $request, $classId)
	{
		$inpData = $request->all();
		$inpData['entity_id'] = $classId;
		$inpData['entity_type'] = Classes::class;
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();

		$pack = $this->runCommandWithErrorHandle(new AddPricePackageCommand($inpData));

		return $this->respondOk((new PricePackageTinyTransformer())->transform($pack));
	}


	/**
	 * @api {put} /classes/:classId/packages/:packageId Class Price Package Update
	 * @apiDescription Update Price Package
	 * @apiName api-classes-price-package-update
	 * @apiGroup Classes
	 *
	 * @apiParam {Integer} classId
	 * @apiParam {Integer} packageId PricePackage id
	 * @apiParam {String} title
	 * @apiParam {String} description
	 * @apiParam {Float} price
	 * @apiParam {Integer} number_of_visits
	 */
	public function updateClassesPricePackage(ApiRequest $request, $classId, $packageId)
	{
		$inpData = $request->all();
		$inpData['package_id'] = $packageId;
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();

		$data = $this->runCommandWithErrorHandle(new UpdatePricePackageCommand($inpData));
		return $this->respondOk($data);
	}

	/**
	 * @ api {delete} /classes/:classId/packages/:packageId Class Price Package Delete
	 * @apiDescription Delete Price Package
	 * @apiName api-classes-price-package-delete
	 * @apiGroup Classes
	 *
	 * @apiParam {Integer} profileId Profile ID
	 * @apiParam {Integer} id PricePackage id
	 */
	public function deleteClassesPricePackage(Request $request, $classId, $id)
	{
		$inpData['package_id'] = $id;
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();

		$data = $this->runCommandWithErrorHandle(new DeletePricePackageCommand($inpData));

		return $this->respondOk($data);
	}
	/**
	 * @api {get} /classes/:classId/packages/:packageId Class Price Package Get by Id
	 * @apiDescription get PricePackage
	 * @apiName api-price-package-get
	 * @apiGroup Classes
	 *
	 * @apiParam {Integer} classId
	 * @apiParam {Integer} packageId PricePackage id
	 *
	 * @apiSuccessExample {json} Success-Response:
	 * "{"message":"success","data":{"id":25,"price":99.9,"number_of_visits":10}}"
	 *
	 */
	public function getClassesPricePackage(ApiRequest $request, $classId, $packageId)
	{
		$inpData['package_id'] = $packageId;
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();

		$data = $this->runCommandWithErrorHandle(new GetPricePackageCommand($inpData));

		return $this->respondOk((new PricePackageTinyTransformer())->transform($data));
	}

	/**
	 * @api {get} /client/:clientId/classes/:classId/credits Client Class Credits Get
	 * @apiDescription get PricePackage Gets the number of remaining credits for a given class based on the Price Package purchased
	 * @apiName api-classes-price-package-charge-get
	 * @apiGroup Classes Client
	 *
	 * @apiParam {Integer} classId
	 * @apiParam {Integer} clientId
	 *
	 * @apiSuccessExample {json} Success-Response:
	 * {"message":"success","data":{"id":25,"price":99.9,"number_of_visits":10,"}}
	 *
	 */
	public function getClassesChargePricePackage(ApiRequest $request, $clientId, $classId)
	{
		$inpData['profile_id'] = $clientId;
		$inpData['entity_id'] = $classId;
		$inpData['entity_type'] = Classes::class;

		$data = $this->runCommandWithErrorHandle(new GetChargedVisitsOfEntityCommand($inpData));

		return $this->respondOk($data);
	}
}

<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use WL\Modules\Conversation\Facades\ConversationService;
use WL\Modules\Conversation\Transformers\ConversationAutocompleteTransformer;
use WL\Modules\Conversation\Transformers\ConversationListTransformer;
use WL\Modules\Conversation\Transformers\ConversationTransformer;
use WL\Modules\Conversation\Transformers\MessageTransformer;
use WL\Modules\Profile\Facades\ProfileService;

class ConversationApiController extends ApiController
{
	public function createConversation(Request $request)
	{
		$data = $request->all();
		$data['recipient_ids'] = [$request->input('recipient_id')];
		$data['sender_id'] = ProfileService::getCurrentProfileId();
		$result = ConversationService::createConversation($data['sender_id'], $data['recipient_ids'], $data['content']);
		return $this->respondOk((new ConversationTransformer())->transform($result));

		#TODO - add command here that calls the service (CreateConversationCommand)
	}

	public function getConversations(Request $request)
	{
		$data = $request->all();
		$data['profile_id'] = ProfileService::getCurrentProfileId();
		$result = ConversationService::getConversationListByProfileId($data['profile_id']);
		return $this->respondOk((new ConversationListTransformer())->transformCollection($result));

		#TODO - add command here that calls the service (CreateConversationCommand)
	}

	public function getConversation($conversationId)
	{
		$profileId = ProfileService::getCurrentProfileId();
		$result = ConversationService::getConversation($conversationId, $profileId);
		return $this->respondOk((new ConversationTransformer())->transform($result));

		#TODO - add command here that calls the service (CreateConversationCommand)
	}

	public function deleteConversation($conversationId)
	{
		#TODO - continue from here
		$profileId = ProfileService::getCurrentProfileId();
		if (ConversationService::deleteConversation($conversationId, $profileId))
			return $this->respondOk();
		return $this->respondWithError("Can't delete conversation", 500);
		#TODO - add command here that calls the service (CreateConversationCommand)
	}

	public function addMessageToConversation($conversationId, Request $request)
	{
		$data = $request->all();
		$data['sender_id'] = ProfileService::getCurrentProfileId();
		$result = ConversationService::addMessage($conversationId, $data['sender_id'], $data['content']);
		return $this->respondOk((new MessageTransformer())->transform($result));

		#TODO - add command here that calls the service (CreateConversationCommand)
	}

	public function markMessageAsRead($conversationId, Request $request)
	{
		$data = $request->all();
		$data['profile_id'] = ProfileService::getCurrentProfileId();
		if(ConversationService::markMessageAsRead($conversationId, $data['profile_id']))
			return $this->respondOk();
		return $this->respondWithError("Can't mark message as read", 500);
		#TODO - add command here that calls the service (CreateConversationCommand)
	}

	public function searchForParticipants(Request $request)
	{
		$query = $request->input('query');
		$currentProfileId = ProfileService::getCurrentProfileId();
		$result = ConversationService::searchParticipants($currentProfileId, $query);
		$result = (new ConversationAutocompleteTransformer())->transform($result);

		return $this->respondOk($result);
	}
}

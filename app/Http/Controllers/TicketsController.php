<?php namespace App\Http\Controllers;

use Response;
use Illuminate\Http\Request;
use WL\Controllers\ControllerBase;
use WL\Modules\Ticket\Models\Ticket;
use Sentinel;
use App\Http\Requests\Site\Ticket\CreateRequest;
use Exception;

#todo update this
class TicketsController extends ControllerBase {

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param CreateRequest $request
	 * @return Response
	 */
	public function store(CreateRequest $request)
	{
		try {
			$ticket = new Ticket($request->fields());

			$ticket->saveMeta('speciality', $request->speciality());

			if ($user = Sentinel::check()) {
				$ticket->user()->associate($user);
			}

			if (!$ticket->save()) {
				throw new Exception;
			}

			$json = ['msg'	=> __('Thank you for your message!')];
			$status = 200;

		} catch (Exception $e) {
			$json = $ticket->getErrors();
			$status = 422;
		}

		return Response::json($json, $status);
	}
}

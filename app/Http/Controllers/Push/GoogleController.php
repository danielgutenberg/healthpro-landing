<?php
namespace App\Http\Controllers\Push;

use Request;
use WL\Controllers\ControllerFront;
use WL\Modules\OAuth\Models\OauthToken;
use WL\Modules\ProfileCalendar\Commands\TriggerCalendarChannelCommand;

class GoogleController extends ControllerFront
{
	public function calendarPush()
	{
		$channelId = Request::header('X-Goog-Channel-ID');
		$resourceId = Request::header('X-Goog-Resource-ID');
		$calendarId = Request::get('cid');

		$this->dispatch(new TriggerCalendarChannelCommand($channelId, OauthToken::PROVIDER_GOOGLE, $calendarId));
	}
}

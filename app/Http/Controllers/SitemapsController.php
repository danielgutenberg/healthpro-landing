<?php namespace App\Http\Controllers;

use WL\Controllers\ControllerSite;
use Sitemap;
use Event;

class SitemapsController extends ControllerSite
{

	/**
	 * Display sitemapindex. sitemap_index.xml
	 *
	 * @return Response
	 */
	public function index()
	{
		Event::fire("sitemap_index");
		return Sitemap::renderSitemapIndex();
	}

	/**
	 * Display sitemap. sitemap_page.xml
	 *
	 * @return Response
	 */
	public function page($name)
	{
		Event::fire("sitemap_{$name}");
		return Sitemap::renderSitemap();
	}

}

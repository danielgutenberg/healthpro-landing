<?php namespace App\Http\Controllers\Referral;

use Sentinel;
use WL\Controllers\ControllerFront;
use WL\Modules\Referral\ReferralListener;

class ReferralController extends ControllerFront {

	/**
	 * Register to cookie referral code and register to registration page ..
	 *
	 * @param $code
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function confirm($code) {
		if( $user = Sentinel::check()  )
			return redirect(route('home'));

		if( ! session(ReferralListener::REFERRAL_CODE_SESSION_KEY) )
			session([ReferralListener::REFERRAL_CODE_SESSION_KEY => $code]);

		return redirect(route('auth-register'));
	}
}

<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use WL\Controllers\ControllerFront;
use WL\Modules\Payment\Commands\AuthorizeStripeAccountCommand;
use WL\Modules\Profile\Facades\ProfileService;

class StripeOauthController extends ControllerFront
{
	public function authorize(Request $request)
	{
		$data = [
			'code' => $request->code,
			'profile_id' => ProfileService::getCurrentProfileId()
		];
		try {
			$this->dispatch(new AuthorizeStripeAccountCommand($data));
		} catch (ValidationException $ve) {
			return Redirect::route('dashboard-settings-payment')
				->withInput($request->except('password'))
				->withErrors($ve->errors());
		}

		return redirect(route('dashboard-settings-payment'));
	}
}

<?php
namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\ResetPasswordRequest;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use RuntimeException;
use Session;
use WL\Controllers\ControllerFront;
use WL\Modules\Payment\Commands\RegisterPaypalSubscriptionCommand;
use WL\Modules\Payment\Paypal\RestApiGateway;
use WL\Modules\Profile\Commands\UpgradeOldProfessionalCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\User\Commands\ActivateUserCommand;
use WL\Modules\User\Commands\ForceLoginUserCommand;
use WL\Modules\User\Commands\LoginUserCommand;
use WL\Modules\User\Commands\LogoutUserCommand;
use WL\Modules\User\Commands\RegisterUserCommand;
use WL\Modules\User\Commands\ResetPasswordCommand;
use WL\Modules\User\Commands\SendActivationCommand;
use WL\Modules\User\Commands\SwitchCurrentUserCommand;
use WL\Modules\User\Exceptions\ActivationException;
use WL\Modules\User\Exceptions\UserExceptions;
use WL\Modules\User\Models\User as User;
use WL\Yaml\Facades\Yaml;

class AuthController extends ControllerFront
{
	/**
	 * Stores new user
	 * @param Request $request
	 * @return mixed
	 */
	public function postCreate(Request $request)
	{
		$email = $request->input('email');
		$password = $request->input('password');
		$firstName = $request->input('first_name');
		$lastName = $request->input('last_name');
		$profileType = $request->input('profile_type', ModelProfile::CLIENT_PROFILE_TYPE);
		$code = $request->input('code');

		try {
			$this->dispatch(new RegisterUserCommand($email, $password, $firstName, $lastName, $profileType, $code));
			$this->dispatch(new LoginUserCommand($email, $password, $remember = true, $profileType, false, false, false));
		} catch (ValidationException $ve) {
			return Redirect::route('auth-register')
				->withInput($request->except('password'))
				->withErrors($ve->errors());
		}

		return redirect(route('home'));
	}


	/**
	 * Displays the form for user creation
	 * @param Request $request
	 * @return \Illuminate\View\View
	 */
	public function getCreate(Request $request)
	{
		$request->session()->reflash();
		return Redirect::route('home', $request->all())
			->with('popup', ['register']);
	}

	public function getCreateProfessional(Request $request)
	{
		$request->session()->reflash();
		return Redirect::route('home', $request->all())
			->with('popup', ['register:provider']);
	}

	public function getCreateClient(Request $request)
	{
		$request->session()->reflash();
		return Redirect::route('home', $request->all())
			->with('popup', ['register:client']);
	}

	/**
	 * Displays the login form
	 * @param Request $request
	 * @return \Illuminate\View\View
	 */
	public function getLogin(Request $request)
	{
		if ($user = Sentinel::check()) {
			return back('home');
		}
		$request->session()->reflash();
		return redirect(route('home'))->with('popup', ['login']);
	}

	/**
	 * Attempt to do login
	 * @param Request $request
	 * @return \Illuminate\View\View
	 */
	public function postLogin(Request $request)
	{
		$email = $request->input('email');
		$password = $request->input('password');
		$remember = $request->input('remember');
		$profileType = $request->input('profile_type');
		$redirectTo = $request->input('redirect_to');

		$result = null;

		try {
			$result = $this->dispatch(new LoginUserCommand($email, $password, $remember, $profileType, false, false, false));
		} catch (ValidationException $ve) {
			$redirectUrl = route('auth-login');
			return redirect($redirectUrl)->withInput($request->except('password'))->withErrors($ve->errors());
		}

		// default redirect url
		$redirectUrl = route('dashboard-home');

		// get referer page
		$referer = $request->headers->get('referer');

		// redirect admin and ignore the redirect_to param
		if ($result->isSpecialUser) {
			$redirectUrl = route('admin-dashboard');

			// use the redirect param
		} else {
			if ($redirectTo) {
				$redirectUrl = $redirectTo;

				// redirect to referer if one exists and it is not the current page
			} else {
				if ($referer && $referer != URL::current()) {
					$redirectUrl = $referer;
				}
			}
		}

		// all hail spaghetti!

		return redirect($redirectUrl);
	}

	/**
	 * Displays the forgot password form
	 */
	public function getForgot()
	{
		if ($code = Input::get('code')) {
			return $this->getReset($code);
		};

		$data['popup'] = 'reset';
		return $this->render('site.home.professionals', $data);
	}

	/**
	 * Attempt to reset password with given email
	 * @param Request $request
	 * @return mixed
	 */
	public function postForgot(Request $request)
	{
		$email = $request->input('email');

		try {
			$this->dispatch(new ResetPasswordCommand($email));
		} catch (ValidationException $ex) {
			return Redirect::route('auth-reset-password')
				->with('info', __('Please check your email'));
		}
	}

	/**
	 * Shows the change password form with the given token
	 * @param $code
	 * @return \Illuminate\View\View
	 */
	public function getReset($code)
	{
		$user = User::findByReminder($code);
		if (!$user) {
			return Redirect::route('auth-reset-password')
				->with('info', __('That reset code is invalid. Please enter your email address again to get a new code'));
		}
		$this->dispatch(new ForceLoginUserCommand($user->id));
		if (!Reminder::exists($user)) {
			return Redirect::route('auth-reset-password')
				->withErrors('Please enter your email if you want to change your password.');
		}

		$data['popup'] = 'new_password';
		$data['slides'] = Yaml::get('items', 'wl.home_slider.meta');
		return $this->render('site.home.professionals', $data);
	}

	public function upgradeOldProfessionals()
	{
		try {
			$this->dispatch(new UpgradeOldProfessionalCommand());
		} catch (\Exception $ex) {
			return Redirect::route('home');
		}

		return Redirect::route('home');
	}


	/**
	 * Attempt change password of the user
	 * @param ResetPasswordRequest|Request $request
	 * @param $code
	 * @return mixed
	 */
	public function postReset(ResetPasswordRequest $request, $code)
	{
		try {
			$user = User::findByReminder($code);

			if (!Reminder::exists($user)) {
				throw new RuntimeException('Please enter your email if you want to change your password.');
			}

			Reminder::complete($user, $code, $request->password());

		} catch (RuntimeException $e) {
			return Redirect::route('auth-reset-password')
				->withErrors(__($e->getMessage()));
		}

		return Redirect::route('auth-login')
			->with('info', __('Password successfully changed'));
	}

	/**
	 * Log the user out of the application.
	 *
	 */
	public function getLogout()
	{
		$this->dispatch(new LogoutUserCommand());

		return redirect(route('home'));
	}

	/**
	 * Log the user out of the application.
	 *
	 */
	public function switchCurrentUser($userId)
	{
		try {
			$this->dispatch(new SwitchCurrentUserCommand($userId));
		} catch (ActivationException $e) {
			return back()->withError($e->getMessage());
		}

		return redirect(route('dashboard-myprofile'));
	}

	/**
	 * Activate user account ..
	 *
	 * @param $code
	 * @param $email
	 * @param $hash
	 * @return mixed
	 * @internal param Request $request
	 */
	public function activate($code, $email, $hash)
	{
		try {
			$newUser = $this->dispatch(new ActivateUserCommand($code, $email, $hash));

			if (Crypt::decrypt(base64_decode($hash)) == $email || $newUser) {
				return redirect(route('dashboard-myprofile-personalinfo'))
					->with('user_activated', 1);
			}

			return redirect(route('dashboard-settings-account'))
				->with('email_updated', true);
		} catch (ActivationException $e) {
			return redirect(route('home'))
				->withErrors($e);
		}
	}

	/**
	 * Send activation to user .
	 *
	 * @param Request $request
	 * @return mixed
	 * @throws UserExceptions
	 */
	public function sendActivation(Request $request)
	{
		try {
			if (!$user = Sentinel::check()) {
				throw new UserExceptions(_('Invalid login'));
			}

			$this->dispatch(
				new SendActivationCommand($user)
			);

			if ($request->ajax()) {
				return response()
					->json(['success' => true]);
			}

			return redirect(route('dashboard-myprofile'))
				->withSuccess(_('Successfully send activation'));

		} catch (ActivationException $e) {
			if ($request->ajax()) {
				return response()
					->json(['success' => false, 'messages' => $e->getMessage()], 500);
			}

			return redirect(route('home'))
				->withErros($e);
		}
	}

	public function getPaypalRegister(Request $request)
	{
		$data = $request->all();
		$data['profile_id'] = ProfileService::getCurrentProfileId();

		try {
			$this->dispatch(new RegisterPaypalSubscriptionCommand($data));

			return redirect('pricing')->with('payment_popup', 'popup_payment_success');
		} catch (ValidationException $ve) {
			#todo delete account if adding paypal fails
			return redirect('pricing')->with('payment_popup', 'popup_payment_failed');
		}
	}

	public function paypalAccountConfirm(Request $request, RestApiGateway $provider)
	{
		$provider->authorizeAccountConfirm($request);

		return redirect(route('dashboard-settings-payment'));
	}
}

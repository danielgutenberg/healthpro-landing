<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Database\QueryException;
use Redirect;
use Event;
use Validator;
use Input;
use SentinelSocial;
use WL\Controllers\ControllerFront;
use Cartalyst\Sentinel\Addons\Social\AccessMissingException;
use WL\Modules\User\Exceptions\SocialUserIdNotEqualsException;

class TwitterController extends ControllerFront
{
	public function getAuthorize()
	{
		$url = SentinelSocial::getAuthorizationUrl('twitter', route('twitter-authenticate'));

		return Redirect::to($url);
	}

	public function getAuthenticate()
	{
		try {

			SentinelSocial::authenticate('twitter', route('twitter-authenticate'), function ($link, $provider, $token, $slug) {

//				if we will need any other data we can grab and save it here
//				\WL\Modules\User\Models\User
				$user = $link->getUser();
//				$data = $provider->getUserDetails($token);
				User::setManualPasswordRequired($user->email);
			});

			return Redirect::route('home')
				->with('success', 'Successfully authenticated');

		} catch (AccessMissingException $e) {

			return Redirect::route('auth-login')
				->with('error', $e->getMessage());

		} catch (\InvalidArgumentException $e) {

			return Redirect::route('auth-login')
				->with('error', $e->getMessage());

		} catch (QueryException $e) {

			if (strpos($e->getMessage(), 'social_provider_user_id_unique') > 0)
				throw new SocialUserIdNotEqualsException();

			throw $e;
		}
	}
}

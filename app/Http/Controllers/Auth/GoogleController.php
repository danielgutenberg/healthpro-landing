<?php
namespace App\Http\Controllers\Auth;

class GoogleController extends SocialSignInController
{
	public function __construct()
	{
		$this->provider = 'google';
		$this->redirectUrl = route('google-authenticate');

		parent::__construct();
	}
}

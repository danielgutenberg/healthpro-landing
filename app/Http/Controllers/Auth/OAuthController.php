<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use WL\Controllers\ControllerFront;
use WL\Modules\OAuth\Exceptions\OAuthAccountAlreadyInUse;
use WL\Modules\OAuth\Facades\OAuthService;
use WL\Modules\OAuth\Models\OauthToken;
use WL\Modules\Profile\Facades\ProfileService;

class OAuthController extends ControllerFront
{
	protected $routes = [
		OauthToken::SCOPE_CALENDAR => 'dashboard-settings-calendar',
	];

	public function authorize($provider, Request $request)
	{
		$scope = $request->get('scope');
		$request->session()->flash('authorize-referrer', URL::previous());
		$request->session()->flash('authorize-scope', $scope);

		$scopes = OAuthService::getScopes($request->get('tokenId'), $scope);

		$url = OAuthService::getAuthorizationUrl($provider, $scopes);
		return Redirect::to($url);
	}

	public function authenticate($provider, Request $request)
	{
		$referrer = $request->session()->get('authorize-referrer');
		$scope = $request->session()->get('authorize-scope');

		try {
            OAuthService::handleResponse($provider, $request, ProfileService::getCurrentProfileId(), $scope);
        } catch (OAuthAccountAlreadyInUse $e) {
            Session::flash('error', 'The account is already linked to another user');
        }

		if($route = array_get($this->routes, $scope)) {
			return Redirect::route($route);
		}
		return $referrer ? Redirect::to($referrer) : Redirect::route('dashboard-home');
	}
}

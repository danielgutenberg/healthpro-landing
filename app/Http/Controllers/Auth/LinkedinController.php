<?php
namespace App\Http\Controllers\Auth;

class LinkedinController extends SocialSignInController
{
	public function __construct()
	{
		$this->provider = 'linkedin';
		$this->redirectUrl = route('linkedin-authenticate');

		parent::__construct();
	}
}

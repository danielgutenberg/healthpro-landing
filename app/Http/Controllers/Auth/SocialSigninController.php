<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Request;
use Redirect;
use SentinelSocial;
use WL\Controllers\ControllerFront;
use Cartalyst\Sentinel\Addons\Social\AccessMissingException;
use WL\Modules\Coupons\Commands\ApplyCouponCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\SocialId\Services\SocialIdServiceInterface;
use WL\Modules\User\Facades\User;
use WL\Modules\User\Facades\User as UserService;
use Illuminate\Support\Facades\Session;

abstract class SocialSignInController extends ControllerFront
{
	protected $provider;
	protected $redirectUrl;

	public function getAuthorize()
	{
		$url = SentinelSocial::getAuthorizationUrl($this->provider, $this->redirectUrl);

		return Redirect::to($url)->withInput();
	}

	public function getAuthenticate(SocialIdServiceInterface $socialIdService)
	{
		$redirectToRegister = false;
		$newSignup = false;
		$this->socialIdService = $socialIdService;
		try {
			SentinelSocial::authenticate($this->provider, $this->redirectUrl, function ($link, $provider, $token, $slug) use (&$successRoute, &$redirectToRegister, &$socialLink, &$newSignup) {
				$socialLink = $link;

				$user = $link->getUser();

				User::forceActivate($user->email);
				//Only new users have no profiles. Or bugs.

				if ($user->profiles()->get()->isEmpty()) {
					if (Request::old('login') == 'true') {
						/*
						 * if a user logs in with social media but has not yet registered for the site, we need to send
						 * them through the registering process to ensure that they accept terms and conditions
						 */
						$redirectToRegister = true;
						return;
					}

					if (Request::old('profile') == 'client') {
						$profile_type = ModelProfile::CLIENT_PROFILE_TYPE;
						if (Request::old('pro_page')) {
							$successRoute = 'profile-show';
						} else {
							$successRoute = 'home';
						}
					} else {
						$newSignup = true;
						$profile_type = ModelProfile::PROVIDER_PROFILE_TYPE;
						$successRoute = 'pricing';
					}

					$profile = User::registerUserProfile($user, $this->stripIllegalChars($user->first_name), $this->stripIllegalChars($user->last_name), $profile_type);
					User::attachUserRole($user);
					User::setManualPasswordRequired($user->email);

					if (session()->has('coupon_code')) {
						$inputData = [
							'coupon' => session('coupon_code'),
							'applyToType' => ModelProfile::PROVIDER_PROFILE_TYPE,
							'applyTo' => $profile->id,
						];
						$this->dispatch(new ApplyCouponCommand($inputData));
					}

					$data = $provider->getUserDetails($token);

					// don't import avatars from linkedin as they are very low quality
					#todo - see if there is a way to import higher quality picture
					if ($profile && $this->provider != 'linkedin') {
						ProfileService::setProfileAvatarFromUrl(ModelProfile::typeCast($profile), $data->imageUrl);
					}
				} else {
					$successRoute = $this->getSuccessRoute($user);
				}

			});
			if ($redirectToRegister) {
				/*
				 * if user needs to register we delete the user that was created and redirect them back to the register
				 * screen
				 */
				$this->deleteTemporaryUser($socialLink);

				return Redirect::route('auth-login')
					->with('registerFirst', true);
			}

			if ($successRoute == 'profile-show') {
				$redirect = route($successRoute, ['id' => Request::old('pro_page')]);
			} else {
				$redirect = route($successRoute);
			}

			if (session('login_profile')) {
				ProfileService::setCurrentProfileId(session('login_profile'));
			}

			$productId = Request::old('product_id');
			if ($productId) {
				session(['productId' => $productId]);
			}

			return Redirect::to($redirect)
				->with('success', 'Successfully authenticated')
				->with('socialSignup', $this->provider);

		} catch (AccessMissingException $e) {
			return Redirect::route('auth-login')
				->with('error', $e->getMessage());

		} catch (\InvalidArgumentException $e) {
			return Redirect::route('auth-login')
				->with('error', $e->getMessage());
		} catch (QueryException $e) {
			if (strpos($e->getMessage(), 'social_provider_user_id_unique') > 0) {
				$successRoute = $this->processDuplicate($e->getBindings()[2]);

				return Redirect::route($successRoute)
					->with('success', 'Successfully authenticated');
			}

			throw $e;
		}
	}

	private function stripIllegalChars($string)
	{
		return preg_replace("/[^\p{Latin}0-9 ']/", '', $string);
	}

	private function deleteTemporaryUser($link)
	{
		/*
		 * when we require the user to register we must delete the social link as well as the user that was created in
		 * the database
		 */
		UserService::logoutUser();
		ProfileService::invalidateCurrentProfileId();
		Session::flush();

		$user = $link->getUser();
		// delete the social media credentials
		$link->delete();
		// delete the user that was temporariliy created
		$user->delete();
	}

	private function getSuccessRoute($user)
	{
		$successRoute = 'dashboard-schedule';
		$profile = $user->profiles()->first();
		if ($profile->type == ModelProfile::CLIENT_PROFILE_TYPE) {
			if (Request::old('pro_page')) {
				$successRoute = 'profile-show';
			} else {
				$pros = ProviderOriginatedService::getClientProviders($profile->id);
				if (!$pros->isEmpty()) {
					$successRoute = 'dashboard-my-providers';
				} else {
					$successRoute = 'home';
				}
			}
		} elseif (!$profile->is_searchable) {
			$successRoute = 'dashboard-appointments-setup';
		}

		return $successRoute;
	}

	private function processDuplicate($id)
	{
		$link = $this->socialIdService->findLink($id);
		$token = $link->oauth2_access_token;
		$socialInfo = $this->socialIdService->getSocialInfo($this->provider, $token);

		$this->socialIdService->deleteLink($id);

		$user = User::getUserByEmail($socialInfo->email);

		User::loginEntity($user);
		ProfileService::setCurrentProfileId($user->profiles->first()->id);

		return $this->getSuccessRoute($user);
	}
}

<?php
namespace App\Http\Controllers\Auth;

class FacebookController extends SocialSignInController
{
	public function __construct()
	{
		$this->provider = 'facebook';
		$this->redirectUrl = route('facebook-authenticate');

		parent::__construct();
	}
}

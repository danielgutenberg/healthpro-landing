<?php namespace App\Http\Controllers\Admin;

use Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use WL\Controllers\ControllerAdmin;
use WL\Modules\Ticket\Models\Ticket;
use Sentinel;

class TicketsController extends ControllerAdmin {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tickets = Ticket::all()->sortByDesc('id');
		return view('admin.tickets.index', compact('tickets'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$ticket = Ticket::find($id);
		return view('admin.tickets.show', compact('ticket'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	/**
	 * Store a newly created conversation resource in storage.
	 *
	 * @return Response
	 */
	public function conversationsStore($ticketId, Request $request)
	{
		$ticket = Ticket::find($ticketId);
		$conv = $ticket->newConversation(Sentinel::check());
		$conv->message = $request->input("message");
		if (!$conv->save()){
			return  Redirect::back()->withError($conv->getErrors());
		}
		return  Redirect::back()->withSuccess("Saved");
	}
}

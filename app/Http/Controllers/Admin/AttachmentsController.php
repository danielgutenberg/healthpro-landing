<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use WL\Controllers\ControllerAdmin;
use WL\Modules\Attachment\Facades\AttachmentService;

class AttachmentsController extends ControllerAdmin
{
	public function delete( $id, Request $request )
	{
		try {
			AttachmentService::removeById($id);
			$success = 1;
			$message = 'Attachment successfully deleted.';

		} catch (ModelNotFoundException $e) {
			$message = $e->getMessage();
			$success = 0;
		}

		if ($request->ajax()) {
			return Response::json(array(
				'success' => $success,
				'message' => $message,
			));
		} else {
			Session::flash($success ? 'success' : 'error', $message);
			return Redirect::back();
		}
	}
}

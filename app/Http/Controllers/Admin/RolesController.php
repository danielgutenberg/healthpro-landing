<?php namespace App\Http\Controllers\Admin;

use WL\Controllers\ControllerAdmin;
use App\Http\Requests\Admin;
use WL\Modules\Role\Models\Role;
use App\Http\Requests\Admin\Role\EditRoleRequest;
use App\Http\Requests\Admin\Role\RolesRequest;

use WL\Modules\Role\Populators\RolePopulator;

use \WL\Controllers\Repository;
use WL\Modules\Role\Repositories\RoleRepository;
use WL\Security\Facades\Authorizer;

class RolesController extends ControllerAdmin {

	use Repository;

	/**
	 * @var Profile
	 */
	protected $model;

	public function __construct(Role $model, RoleRepository $repository) {
		parent::__construct();

		$this->model = $model;

		$this->setRepository($repository);
	}

	/**
	 * Display a listing of the resource.
	 * GET /admin/roles
	 *
	 * @return Response
	 */
	public function index() {
		return view('admin/roles/index', array('roles' => Role::all()));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/roles/create
	 *
	 * @return Response
	 */
	public function create(RolePopulator $populator) {

		$entry = $this->getRepository();
		$populator->setModel($entry)->populate();

		return view('admin.roles.save', [
			'accessPermissions' => Authorizer::getRoles(),
			'roles'             => Role::all()
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin/roles
	 *
	 * @return Response
	 */
	public function store(RolesRequest $request) {
		try {
			$role = \Sentinel::getRoleRepository()->createModel()->create([
				'name' => $request->get('name'),
			]);

			$role->savePermissions($request->get('permissions'));

			\Session::flash('success', 'Role successfully created');

			return \Redirect::route('admin.roles.edit', ['id' => $role->id]);
		} catch (\Exception $e) {
			\Session::flash('error', $e->getMessage());

			return \Redirect::route('admin.roles.index');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/roles/{id}/edit
	 *
	 */
	public function edit(Role $role, RolePopulator $populator) {
		$populator->setModel($role)->populate();

		return view('admin.roles.save', [
			'role'              => $role,
			'roles'             => Role::all(),
			'accessPermissions' => Authorizer::getRoles()
		]);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/roles/{id}
	 *
	 */
	public function update(EditRoleRequest $request, Role $role) {
		try {
			$role->name        = $request->get('name');
			$role->savePermissions($request->get('permissions'));

			\Session::flash('success', 'Role successfully edited');

		} catch (\Exception $e) {
			\Session::flash('error', $e->getMessage());
		}


		return \Redirect::route('admin.roles.edit', array('id' => $role->id));
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin/roles/{id}
	 *
	 */
	public function destroy(Role $role) {
		$role->delete();

		return \Redirect::route('admin.roles.index')->withSuccess('Deleted successfully');
	}
}

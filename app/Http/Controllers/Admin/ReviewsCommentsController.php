<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ReviewsComment\ChangeStatusRequest;
use App\Http\Requests\Admin\ReviewsComment\DeleteRequest;
use App\Http\Requests\Admin\ReviewsComment\UpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use WL\Controllers\ControllerAdmin;
use WL\Modules\Comment\Facades\CommentService;
use WL\Modules\Comment\Models\Comment;
use WL\Modules\Comment\Populators\CommentPopulator;
use WL\Modules\Comment\Repositories\CommentRepositoryInterface;
use WL\Modules\Review\Models\Review;
use WL\Modules\Review\Repositories\ReviewRepositoryInterface;

class ReviewsCommentsController extends ControllerAdmin
{
	protected $commentModel;
	protected $reviewModel;

	protected $reviewRepository;
	protected $commentRepository;

	protected $commentType = Review::COMMENT_TYPE_BODY;

	protected $perPage = 20;

	public function __construct(Review $reviewModel, Comment $commentModel, ReviewRepositoryInterface $reviewRepository, CommentRepositoryInterface $commentRepository) {
		parent::__construct();

		$this->reviewModel = $reviewModel;

		$this->commentModel = $commentModel;

		$this->reviewRepository = $reviewRepository;

		$this->commentRepository = $commentRepository;
	}

	public function index(Request $request)
	{
		$status = $request->get('status', null);

		$entries = CommentService::getPaginatedReviewsByType($this->commentType, $status, $this->perPage);
		$entries->appends(['status' => $status]);

		return view('admin.reviews-comments.index', [
			'entries' => $entries,
			'status' => $status,
			'perPage' => $this->perPage,
			'commentModel' => $this->commentModel,
		]);
	}

	public function edit(Comment $comment, CommentPopulator $populator)
	{
		$populator->setModel($comment)->populate();

		$statusList = CommentService::getAvailableStatuses();

		return $this->render('admin.reviews-comments.edit', [
			'entry' => $comment,
			'statuses' => array_combine($statusList, $statusList)
		]);
	}

	public function update(Comment $comment, UpdateRequest $request)
	{
		try {
			$comment->fill($request->fields());

			if ($comment->save()) {
				Session::flash('success', 'You have successfully edited the comment');
				return Redirect::route('admin.reviews-comments.edit', ['id' => $comment->id]);

			} else {
				Session::flash('error', 'Please fix the errors before saving the comment');
				return Redirect::back()
					->withInput($request->invalidFields())
					->withErrors($comment->getErrors());
			}

		} catch (ModelNotFoundException $e) {
			Session::flash('error', 'The comment cannot be found');
			return Redirect::route('admin.reviews-comments.index');
		}
	}

	public function destroy(Comment $comment)
	{
		try {
			CommentService::removeComment($comment->id);
			Session::flash('success', 'You have successfully deleted the comment');

		} catch (ModelNotFoundException $e) {
			Session::flash('error', 'The comment cannot be found');
		}

		return Redirect::route('admin.reviews-comments.index');
	}

	public function delete(DeleteRequest $request)
	{
		$this->commentRepository->delete( $request->fields() );

		$successMessage = 'Entries successfully deleted';

		if ($request->ajax()) {
			return Response::json([
				'success' => 1,
				'message' => $successMessage,
			]);
		} else {
			Session::flash('success', $successMessage);
			return Redirect::route('admin.reviews-comments.index');
		}
	}

	public function updateStatus(ChangeStatusRequest $request)
	{
		$status = $request->get('status');

		foreach	($request->fields() as $id) {
			CommentService::updateStatus($id, $status);
		}

		$successMessage = 'Entries status successfully updated';

		if ($request->ajax()) {
			return Response::json([
				'success' => 1,
				'message' => $successMessage,
			]);
		} else {
			Session::flash('success', $successMessage);
			return Redirect::route('admin.reviews-comments.index');
		}
	}
}

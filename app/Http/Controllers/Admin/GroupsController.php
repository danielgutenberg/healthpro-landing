<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Content\StoreRequest as ContentStoreRequest;
use App\Http\Requests\Admin\Group\StoreRequest;
use WL\Modules\User\Models\User;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Redirect;
use Watson\Validating\ValidationException;
use WL\Controllers\ControllerAdmin;
use WL\Controllers\Repository;
use WL\Modules\Location\Models\Location;
use WL\Modules\Profile\Models\ProviderProfile;
use WL\Modules\ProfileGroup\Exceptions\ProfileGroupException;
use WL\Modules\ProfileGroup\Models\ContentGroup;
use WL\Modules\ProfileGroup\Models\ProfileGroup;
use WL\Modules\ProfileGroup\Repositories\GroupRepository;
use WL\Modules\ProfileGroup\Populators\GroupPopulator;

class GroupsController extends ControllerAdmin {

	use Repository;

	protected $model;

	protected $repository;

	public function __construct(ProfileGroup $model, GroupRepository $repository) {
		parent::__construct();

		$this->model = $model;

		$this->repository = $repository;
		$this->setRepository($repository);
	}


	/**
	 * Display a listing of the resource.
	 * GET /admin\groups
	 *
	 * @return Response
	 */
	public function index() {
		$groups = ProfileGroup::all();

		return view('admin.group.index', compact('groups'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin\groups/create
	 *
	 * @return Response
	 */
	public function create(GroupPopulator $populator) {
		$entry = $this->getRepository();

		$populator->setModel($entry)->populate();
		$locations = Location::all()->pluck('name', 'id')->all();

		return view('admin.group.save', compact('locations'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin\groups
	 *
	 * @return Response
	 */
	public function store(ProfileGroup $group, StoreRequest $request) {
		try {
			DB::transaction(function() use($request, $group) {
				$group->setFeeder($request);
				$group->fill($request->fields());
				$group->saveOrFail();

				$group->syncPhones( $group->getFeeder()->phones() );
				$group->saveMany( $group->getFeeder()->meta() );
				$group->saveSocialPages( $group->getFeeder()->socialPages() );
			});
		} catch(ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors() ?: $e->getMessage())
				->withInput();
		}

		return Redirect::route('admin.groups.edit', ['id' => $group->id])
			->with('success', __('Successfully saved group'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin\groups/{id}/edit
	 *
	 * @param ProfileGroup $group
	 * @param GroupPopulator $populator
	 * @internal param int $id
	 * @return Response
	 */
	public function edit(ProfileGroup $group, GroupPopulator $populator) {
		$locations = Location::all()->pluck('name', 'id')->all();

		$populator->setModel($group)->populate();

		$articles 		= $group->content('article')->get();
		$videos 		= $group->content('embed-video')->get();
		$users      	= $group->profiles->map(function($profile) {
			return $profile->users()->where('is_owner', '=', 1)->first();
		});
		$socialPages 	= $group->getConfigPages();

		return view('admin.group.save', compact('group', 'locations', 'articles', 'videos', 'users', 'socialPages'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin\groups/{id}
	 *
	 * @param ProfileGroup $group
	 * @param StoreRequest $request
	 * @internal param int $id
	 * @return Response
	 */
	public function update(ProfileGroup $group, StoreRequest $request) {
		try {
			DB::transaction(function() use($request, $group) {
				$group->setFeeder($request);
				$group->fill($request->fields());
				$group->saveOrFail();

				$group->syncPhones(  $group->getPhoneObjects( $group->getFeeder()->phones() ?: [] )  );
				$group->saveMany( $group->getFeeder()->meta() );
				$group->saveSocialPages( $group->getFeeder()->socialPages() );

			});
		} catch(ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors() ?: $e->getMessage())
				->withInput();
		}


		return Redirect::route('admin.groups.edit', ['id' => $group->id])
			->with('success', __('Successfully saved group'));
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin\groups/{id}
	 *
	 * @param ProfileGroup $group
	 * @internal param int $id
	 * @return Response
	 */
	public function destroy(ProfileGroup $group) {
		try {
			$this->repository->destroy($group->id);
			Session::flash('success', 'You have successfully deleted the group');

		} catch (ModelNotFoundException $e) {
			Session::flash('error', 'The group with id ' . $group->id . ' cannot be found');
		}

		return Redirect::route('admin.groups.index');
	}


	/**
	 * Add new Content ...
	 *
	 * @param ContentStoreRequest $request
	 * @param ContentGroup $content
	 * @param ProfileGroup $group
	 * @throws \Exception
	 * @internal param $groupId
	 * @return mixed
	 */
	public function saveContent(ContentStoreRequest $request, ContentGroup $content, ProfileGroup $group) {
		try {
			if( $request->get('id') ) {
				$content = $content->find($request->get('id'));
			}

			$group->saveContent($content, $request->all());

			$contentSaved = $group->content($request->get('type'))->orderBy('id', 'DESC')->first();

			$embedVideoFields = [];
			if( $contentSaved->type == 'embed-video' ) {
				$embedVideo  	  = $content->present()->embedVideo();
				$embedVideoFields = $content->present()->getEmbedVideoFields() + ['url' => $contentSaved->url, 'id' => $contentSaved->id];

				$contentSaved->description = $embedVideo->description;
				$contentSaved->save();
			}

			$response = [
				'error' => false,
				'data'  => $contentSaved,
				'message' => __(ucfirst($request->get('type')  ) .' successfully saved')
			];

			if( count($embedVideoFields) )
				$response = array_merge($response, ['embed' => $embedVideoFields]);

			return Response::json($response);

		} catch(ValidationException $e) {
			return Response::json($e->getErrors(), 422);
		}
	}

	/**
	 * 	Delete an content by id ..
	 *
	 * @param Request $request
	 * @param ContentGroup $content
	 * @return mixed
	 * @throws \Exception
	 */
	public function deleteContent(Request $request, ContentGroup $content) {
		if( $request->get('id') ) {
			$content->find($request->get('id'))->delete();

			return Response::json(__('Successfully deleted'), 200);
		}
	}


	/**
	 * Get users by email ...
	 *
	 * @param Request $request
	 * @param ProfileGroup $group
	 * @return mixed
	 */
	public function getUsersByEmail(Request $request, ProfileGroup $group) {
		$items = $group->disjoinedEmails( $request->get('email', null) )->pluck('email', 'id')->all();
		$users = new Collection($items);

		if( $request->ajax() ) {
			return Response::json(
				$users->map(function($user, $id) {
					return ['label' => $user, 'id' => $id];
				})->toArray()
			);
		}
	}

	/**
	 * 	Attach / Detach user by id ...
	 *
	 * @param Request $request
	 * @param $type
	 * @param ProfileGroup $group
	 * @return mixed
	 */
	public function manageAttachedUsers(Request $request, $type, ProfileGroup $group) {
		try {
			if( $request->ajax() ) {
				if( ! $user = User::find( $request->get('user_id') ) )
					throw new ProfileGroupException(__('Invalid user'));

				$profile = $user->profile(ProviderProfile::class);

				if(! isset($profile->id))
					throw new ProfileGroupException('User profile not found');

				if( $type == 'attach' ) {
					$group->invite($profile);

					return Response::json(['error' => false, 'message' => __('Profile Attached successfully'), 'data' => $user->toArray()]);
				} elseif( $type = 'detach' ) {
					$group->reject($profile);

					return Response::json(['error' => false, 'message' => __('Profile rejected successfully')]);
				}

				throw new ProfileGroupException(__('Invalid type'));
			}
		} catch(ProfileGroupException $e) {
			return Response::json(['message' => $e->getMessage()], 422);
		}
	}
}

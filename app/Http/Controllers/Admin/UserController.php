<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\User\CreateRequest;
use App\Http\Requests\Admin\User\UpdateRequest;
use WL\Modules\Phone\PhoneFactory;
use WL\Modules\Role\Models\Role;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\User\Repositories\UserRepository;
use Illuminate\Http\Request;
use Sentinel;
use Watson\Validating\ValidationException;
use WL\Controllers\ControllerAdmin;
use WL\Modules\User\Models\User;
use App\Http\Requests\Admin\User\SyncManagersRequest;
use WL\Modules\User\Exceptions\UserExceptions;
use WL\Modules\Profile\Exceptions\MainProfileNotFoundException;
use Illuminate\Support\Facades\Response;
use Redirect;
use WL\Modules\Profile\Models\ProviderProfile;
use WL\Security\Facades\Authorizer;

class UserController extends ControllerAdmin
{

	/**
	 * @var UserRepository
	 */
	private $repository;

	/**
	 * @param UserRepository $repository
	 */
	public function __construct( UserRepository $repository )
	{
		parent::__construct();

		$this->repository = $repository;
	}

	/**
	 * Display a listing of the resource.
	 * GET /admin/users
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = $this->repository->getUsers();
		return view('admin.user.index', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/users/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.user.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin/users
	 *
	 * @param Request $r
	 * @return Response
	 */
	public function store(CreateRequest $r)
	{
		try {
			$user = Sentinel::register($r->input());
			return redirect(route("admin.users.show", [$user->id]));
		} catch (ValidationException $e) {
			return redirect(route("admin.users.create"))->withErrors($e->getErrors());
		}
	}

	/**
	 * Display the specified resource.
	 * @param int $id
	 * @return Result
	 */
	public function show($id)
	{
		$user = $this->repository->getById($id);
		if (!$user)
			return redirect(route("admin.users.index"))
				->withError("User #{$id} not found");

		$managers = $this->repository->managers($user, false);

		$profiles = app(ModelProfileService::class)->getUserProfiles($user->id);

		foreach($profiles as $k => $profile){
			$profiles[$k] = ProfileService::loadProfileBasicDetailsModel($profile);
		}
		return view('admin.user.show', compact('user', 'managers', 'profiles'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/users/{id}/edit
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = $this->repository->getById($id);
		if (!$user) return redirect(route("admin.users.index"))->withError("User #{$id} not found");

		return view('admin.user.edit', [
			'user' => $user
		]);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/users/{id}
	 *
	 * @param  int $id
	 * @param Request $r
	 * @return Response
	 */
	public function update($id, UpdateRequest $r)
	{
		$user = $this->repository->getById($id);
		if (!$user) return redirect(route("admin.users.index"))->withError("User #{$id} not found");

		try {
			$data = [
				'first_name' => $r->input('first_name'),
				'last_name' => $r->input('last_name'),
				'email' => $r->input('email'),
			];
			if ($r->input('password')) {
				$data['password'] = $r->input('password');
			}
			$this->repository->updateUser($user,$data);

			$phones = array();
			foreach ($r->input('phones', []) as $data) {
				$phone = PhoneFactory::fromArray([
					'prefix' => ltrim($data['code'],'+'),
					'number' => $data['locNumber']
				]);

				if ($phone->isValid()) {
					$phones[] = $phone;
				}
			}
			$user->syncPhones($phones);

			return redirect(route("admin.users.show", [$user->id]))->withSuccess('Save');
		} catch (ValidationException $e) {
			return redirect(route("admin.users.edit", [$user->id]))->withErrors($e->getErrors());
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin/users/{id}
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if (!($user = $this->repository->getById($id))) {
			return redirect(route("admin.users.index"))->withError("User #{$id} not found");
		}
		$user->delete();
		return redirect(route("admin.users.index"))->withSuccess('User was removed');
	}

	/**
	 * Save user permission
	 * @param int $userId
	 * @param Request $r
	 * @return Response
	 */
	public function permissionsSave($userId, Request $r)
	{
		if (!($user = $this->repository->getById($userId))) {
			return redirect(route("admin.users.index"))->withError("User #{$userId} not found");
		}
		$roles = Role::all();
		$permissions = Authorizer::getRoles();

		if ($r->method() == 'POST') {
			foreach ($user->roles as $role) $role->users()->detach($user);
			if (($roleId = $r->input('role', false)) && ($role = Sentinel::findRoleById($roleId))) {
				$role->users()->attach($user);
			}
			$user = $this->repository->getById($user->id);

			$user->permissions = array();

			$havePerms = [];
			foreach ($user->roles as $role) {
				foreach ($role->permissions as $perm => $val) {
					$havePerms[$perm] = $user->hasAccess($perm);
				}
			}

			$needPerm = array();
			foreach ($r->input('permissions', []) as $i => $perm) {
				$needPerm[$perm] = true;
				if (empty($havePerms[$perm])) $user->updatePermission($perm, true, true);
			}
			foreach ($havePerms as $perm => $val) {
				if (empty($needPerm[$perm])) $user->updatePermission($perm, false, true);
			}
			$user->save();

			return redirect(route("admin.users.show", [$user->id]))->withSuccess("Save");
		} else {
			return view('admin.user.permissions-save', [
				'user' => $user,
				'roles' => $roles,
				'permissions' => $permissions
			]);
		}
	}

	/**
	 * Show all managers for current user profile ...
	 *
	 * @param \WL\Modules\User\Models\User $user
	 * @param Request $request
	 * @return \Illuminate\View\View
	 */
	public function managersShow(User $user, Request $request) {
		$permissions = Authorizer::getRoles();
		$managers    = $user->managers(false);
		$invitations = $user->invitations();

		return view('admin.user.managers', compact('user', 'permissions', 'managers', 'invitations'));
	}

	/**
	 * Get non managers list ...
	 *
	 */
	public function nonManagers(User $user) {
		$nonManagers = $user->nonManagers();

		if(! $nonManagers || !count($nonManagers))
			return Response::json([]);

		return Response::json(
			$user->nonManagers()->map(function($user) {
				return ['label' => $user->email, 'id' => $user->id];
			})->toArray()
		);
	}

	/**
	 * Sync managers for current user profile ...
	 *
	 * @param User $user
	 * @param SyncManagersRequest|Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function syncManagers(User $user, SyncManagersRequest $request) {
		try {
			if( $request->isMethod('POST') ) {
				$invited = User::find($request->get('user_id'));

				if( ! isset($invited->id) )
					throw new UserExceptions('Invited not found');

				switch( $request->get('type') ) {
					case 'exclude':
							if( $user->isInvited( $invited ) ) {
								$user->exclude( $invited );

								$message = 'Successfully excluded manager';
							}
						break;

					default:
						/** If current user is invited that update relation ... */
							$user->invite( $invited, $request->get('permissions', []) , true );

							if( $user->isInvited( $invited ) ) {
								$message = 'Successfully updated invitation';
							} else {
								$message = 'Successfully invited';
							}
						break;
				}
			}
		} catch(UserExceptions $e) {
			return Redirect::route('admin.user.managers.show', ['user' => $user->id])
				->with('error', __($e->getMessage()));
		} catch(MainProfileNotFoundException $e) {
			return Redirect::route('admin.user.managers.show', ['user' => $user->id])
				->with('error', __($e->getMessage()));
		}

		return Redirect::route('admin.user.managers.show', ['user' => $user->id])
			->with('success', __($message));
	}

	/**
	 * Manage user invitations ...
	 */
	public function invitations(User $user, $type, ProviderProfile $profile) {
		try {
			if( $type == 'accept' ) {
				$user->accept( $profile );

				$message = 'Successfully accepted invitation';
			} elseif( $type == 'decline' ) {
				$user->decline( $profile );

				$message = 'Successfully rejected invitation';
			}
		} catch(UserExceptions $e) {
			return Redirect::route('admin.user.managers.show', ['user' => $user->id])
				->with('error', __($e->getMessage()));
		}

		return Redirect::route('admin.user.managers.show', ['user' => $user->id])
			->with('success', __($message));
	}

}

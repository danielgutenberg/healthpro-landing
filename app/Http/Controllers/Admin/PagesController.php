<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Page\CreateRequest;
use App\Http\Requests\Admin\Page\DeleteRequest;
use App\Http\Requests\Admin\Page\UpdateRequest;
use WL\Modules\Page\Models\Page;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use WL\Controllers\ControllerAdmin;
use WL\Modules\Page\Populators\PagePopulator;
use WL\Modules\Page\Repositories\PageRepository;
use Illuminate\Support\Facades\View;
use Former\Facades\Former;

class PagesController extends ControllerAdmin
{
	/**
	 * @var Page
	 */
	protected $model;

	/**
	 * @var \WL\Modules\Page\Repositories\PageRepository
	 */
	private $repository;

	/**
	 * @param Page $model
	 * @param PageRepository $repository
	 */
	public function __construct( Page $model, PageRepository $repository )
	{
		parent::__construct();

		$this->model = $model;

		$this->repository = $repository;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$filters = [];

		$entries = $this->repository->getFlatList( $filters );

		$pageTitle = __( 'Pages Management' );

		$selectedLanguage = $this->model->getModelLanguage();

		// Show the page
		return View::make('admin/pages/index', compact('entries', 'pageTitle', 'selectedLanguage'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param \WL\Modules\Page\Populators\PagePopulator $populator
	 * @return Response
	 */
	public function create(PagePopulator $populator)
	{
		$entry = $this->repository->createNew();

		$populator->setModel($entry)->populate();

		$pageTitle = __( 'Create New Page' );

		return View::make('admin/pages/create', compact('pageTitle', 'entry'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param CreateRequest $request
	 * @return Response
	 */
	public function store(CreateRequest $request)
	{
		$entry = $this->repository->createNew();

		$entry->fill( $request->fields() );

		// we will need request for the events
		$entry->setFeeder( $request );

		if ($entry->save()) {
			Session::flash('success', 'You have successfully created a new page');
			return Redirect::route('admin.pages.edit', ['id' => $entry->id]);

		} else {

			Session::flash('error', 'Please fix the errors before saving the page');
			return Redirect::back()
				->withInput( $request->invalidFields() )
				->withErrors($entry->getErrors());
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @param \WL\Modules\Page\Populators\PagePopulator $populator
	 * @return Response
	 */
	public function edit($id, PagePopulator $populator)
	{
		try {
			$entry = $this->repository->getById($id);

			$populator->setModel($entry)->populate();

			$pageTitle = __( 'Edit Page' );

			return View::make('admin/pages/edit', compact('pageTitle', 'entry'));

		} catch (ModelNotFoundException $e) {
			Session::flash('error', 'The page with id ' . $id . ' cannot be found');
			return Redirect::route('admin.pages.index');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @param UpdateRequest $request
	 * @return Response
	 */
	public function update($id, UpdateRequest $request)
	{
		try {
			$entry = $this->repository->getById($id);

			$entry->fill( $request->fields() );

			// we will need request for the events
			$entry->setFeeder( $request );

			if ($entry->save()) {
				Session::flash('success', 'You have successfully edited the page');
				return Redirect::route('admin.pages.edit', ['id' => $entry->id]);

			} else {
				Session::flash('error', 'Please fix the errors before saving the page');
				return Redirect::back()
					->withInput( $request->invalidFields() )
					->withErrors($entry->getErrors());
			}

		} catch (ModelNotFoundException $e) {
			Session::flash('error', 'The page with id ' . $id . ' cannot be found');
			return Redirect::route('admin.pages.index');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try {
			$entry = $this->repository->destroy($id);
			Session::flash('success', 'You have successfully deleted the page');

		} catch (ModelNotFoundException $e) {
			Session::flash('error', 'The page with id ' . $id . ' cannot be found');
		}

		return Redirect::route('admin.pages.index');
	}

	/**
	 * Delete multiple entities at once
	 * @param DeleteRequest $request
	 */
	public function delete(DeleteRequest $request)
	{
		$this->repository->delete( $request->fields() );

		$successMessage = 'Entries successfully deleted';

		if ($request->ajax()) {
			return Response::json([
				'success' => 1,
				'message' => $successMessage,
			]);
		} else {
			Session::flash('success', $successMessage);
			return Redirect::route('admin.pages.index');
		}
	}

	/**
	 * @param $id
	 * @param $lang
	 * @internal param $resource
	 * @return \Redirect
	 */
	public function translate( $id, $lang )
	{
		try {
			$entry = $this->repository->getById($id);

			if ( !$entry->hasTranslation( $lang ) ) {
				$entry->translate( $lang, $this->model->id );
			}

			Session::flash('success', 'You have successfully created a new translation');
			return Redirect::route('admin.pages.edit', ['id' => $entry->translator( $lang )->id]);

		} catch(ModelNotFoundException $e) {
			Session::flash('error', 'The page with id ' . $id . ' cannot be found');
			return Redirect::route('admin.pages.index');
		}
	}
}

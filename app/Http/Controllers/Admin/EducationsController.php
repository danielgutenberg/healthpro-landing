<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

use WL\Controllers\ControllerAdmin;
use WL\Modules\Education\Models\Education;
use WL\Controllers\Repository;
use WL\Modules\Education\Populators\EducationPopulator;
use WL\Modules\Education\Repository\EducationRepository;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\Admin\Education\DeleteRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\Admin\Education\StoreRequest;
use Watson\Validating\ValidationException;
use WL\Modules\Education\Populators\FormerEducationPopulator;

class EducationsController extends ControllerAdmin {

	use Repository;

	protected $model;

	public function __construct(Education $model, EducationRepository $repository) {
		parent::__construct();

		$this->model = $model;

		$this->setRepository($repository);
	}


	/**
	 * Display a listing of the resource.
	 * GET /admin/educations
	 *
	 * @return Response
	 */
	public function index(Request $request) {
		if ($name = $request->get('name')) {
			$educations = $this->_repository->getList(['name:like'=>"%{$name}%","is_custom"=>0], true);
		} else {
			$educations = $this->_repository->getList();
		}

		if( $request->ajax() ) {
			return Response::json(
				$educations->map(function($education) {
					return ['label' => $education->name, 'id' => $education->id];
				})->toArray()
			);
		}

		return view('admin.educations.index', compact('educations'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/educations/create
	 *
	 * @return Response
	 */
	public function create() {
		return view('admin.educations.save');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin/educations
	 *
	 * @return Response
	 */
	public function store(StoreRequest $request, Education $education) {
		try {
			$education->setFeeder($request);
			$education->fill($request->fields());
			$education->saveOrFail();
		} catch(ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors())
				->withInput();
		}

		return Redirect::route('admin.educations.edit', ['id' => $education->id])
			->with('success', __('Successfully saved education'));
	}


	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/educations/{id}/edit
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit(Education $education, EducationPopulator $populator) {
		$education = $this->getRepository($education);

		$populator->setModel($education)->populate();

		return view('admin.educations.save', compact('education'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/educations/{id}
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update(StoreRequest $request, Education $education) {
		try {
			$education->setFeeder($request);
			$education->fill($request->fields());
			$education->saveOrFail();
		} catch(ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors())
				->withInput();
		}

		return Redirect::route('admin.educations.edit', ['id' => $education->id])
			->with('success', __('Successfully edited location'));
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin/educations/{id}
	 *
	 * @param Education $education
	 * @internal param int $id
	 * @return Response
	 */
	public function destroy(Education $education) {
		try {
			$this->_repository->destroy($education->id);

			Session::flash('success', 'You have successfully deleted the education');

		} catch (ModelNotFoundException $e) {
			Session::flash('error', 'The education with id ' . $education->id . ' cannot be found');
		}

		return Redirect::route('admin.educations.index');
	}

	/**
	 * 	Delete multiple educations ..
	 *
	 * @param DeleteRequest $request
	 * @return mixed
	 */
	public function delete(DeleteRequest $request) {
		$this->_repository->delete($request->fields());

		$successMessage = 'Entries successfully deleted';

		if ($request->ajax()) {
			return Response::json([
				'success' => 1,
				'message' => $successMessage,
			]);
		}

		return Redirect::route('admin.educations.index')->withSuccess($successMessage);
	}

}

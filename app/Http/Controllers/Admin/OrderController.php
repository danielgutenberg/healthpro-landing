<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use WL\Controllers\ControllerAdmin;
use WL\Modules\Order\ValueObjects\ItemCancellationOptions;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Search\Facades\OrderService;

class OrderController extends ControllerAdmin
{
	public function refundOrderItem($orderItemId, Request $request)
	{
		$reverseTransfer = $request->input("reverse_transfer");
		$reverseFee = $request->input("reverse_fee");
		$reverseDeposit = $request->input("reverse_deposit");

		OrderService::cancelItem(ProfileService::getCurrentProfileId(), $orderItemId, new ItemCancellationOptions($reverseTransfer, $reverseFee, $reverseDeposit));

		return back();
	}
}

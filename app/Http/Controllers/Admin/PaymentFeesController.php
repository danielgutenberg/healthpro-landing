<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use stdClass;
use WL;
use WL\Controllers\ControllerAdmin;
use WL\Modules\Payment\Facades\PaymentConfig;

class PaymentFeesController extends ControllerAdmin
{

	/**
	 * Display a listing of the resource.
	 * GET /admin/fees
	 * @return Response
	 */
	public function index()
	{
		$fees = PaymentConfig::getAllFees();
		$defaultServiceFee = PaymentConfig::getDefaultServiceFee();

		return view('admin.fees.index', compact('fees', 'defaultServiceFee'));
	}

	/**
	 * Edit fee
	 * GET /admin/fees/:id
	 * @return Response
	 */
	public function edit($id)
	{
		$fee = PaymentConfig::getFee($id);

		return view('admin.fees.save', compact('fee'));
	}


	/**
	 * Create fee
	 * GET /admin/fees
	 * @return Response
	 */
	public function create()
	{
		return view('admin.fees.save');
	}

	/**
	 * Delete fee
	 * @param $id
	 * @return mixed
	 */
	public function destroy($id)
	{
		PaymentConfig::deleteFee($id);

		return Redirect::route('admin.fees.index')
			->with('success', __('Successfully deleted fee'));
	}


	/**
	 * Store a newly created resource in storage.
	 * POST /admin/fees
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$id = $request->input('id');
		$startDay = $request->input('start_day');
		$endDay = $request->input('end_day');
		$type = $request->input('type');
		$value = $request->input('value');

		$fee = new stdClass();
		$fee->id = $id;
		$fee->startDay = $startDay;
		$fee->endDay = $endDay;
		$fee->value = $value;
		$fee->type = $type;

		try {
			$result = PaymentConfig::saveFeeValues([$fee]);
		} catch (ValidationException $e) {
			return Redirect::back()
				->withErrors($e->errors())
				->withInput();
		}

		return Redirect::route('admin.fees.edit', ['id' => $result[0]->id])
			->with('success', __('Successfully saved fee'));
	}

}

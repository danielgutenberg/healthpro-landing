<?php
namespace App\Http\Controllers\Admin;

use WL\Controllers\ControllerAdmin;

use DBTranslator;
use Response;
use Request;
use Redirect;
use Input;
use Former;
use Session;

class TranslationsController extends ControllerAdmin
{
	protected $model;

	protected $modelValue;

	protected $modelGroup;

	public function __construct( \WL\Translator\Models\TranslationStringGroup $modelGroup, \WL\Translator\Models\TranslationString $model, \WL\Translator\Models\TranslationStringValue $modelValue )
	{
		$this->model      = $model;

		$this->modelGroup = $modelGroup;

		$this->modelValue = $modelValue;
	}

	/**
	 * @return Response
	 */
	public function getIndex()
	{
		$languages = DBTranslator::getLanguages();

		$pageTitle = 'Translations';

		$groups = $this->modelGroup->orderBy('name', 'ASC')->get();

		return view('admin/translations/index', compact('languages', 'pageTitle', 'groups'));
	}

	public function getEdit( $id, $lang )
	{
		try {
			$langConfig = DBTranslator::getLanguageConfig( $lang );

			$group = $this->modelGroup->findOrFail( $id );

			$pageTitle = '"' . $group->name . '" Group';

			return view('admin/translations/edit', compact('pageTitle', 'lang', 'langConfig', 'group'));

		} catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

			Session::flash('error', 'Group not found. ID - ' . $id);
			return redirect( route('admin-translations') );
		}
	}

	public function postEdit( $id, $lang )
	{
		try {
			$group = $this->modelGroup->findOrFail( $id );

			$post = Input::all();

			if (is_array( $post['translation'] )) {
				foreach( $post['translation'] as $translationId => $values ) {

					$translation = $this->model->find( $translationId );

					if (null === $translation) {
						$translation = new $this->model;
						$translation->group_id = $group->id;
					}

					$translation->name = $values['name'];

					$translation->save();

					foreach( $values['translations']['content'] as $k => $translationValue ) {

						$translationValues = [
							'content'   => $values['translations']['content'][$k],
							'frequency' => $values['translations']['frequency'][$k],
							'lang'      => $lang,
						];

						$valueId = isset( $values['translations']['id'][$k] ) ? $values['translations']['id'][$k] : 0;

						$translation->saveValue( $translationValues, $valueId );
					}
				}
			}

			Session::flash('success', 'Values successfully saved.');

			return redirect( route('admin-translations-edit', ['id' => $id, 'lang' => $lang] ) );


		} catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

			Session::flash('error', 'Group not found. ID - ' . $id);
			return redirect( route('admin-translations') );
		}
	}

	public function getAdd( $id, $lang )
	{
		try {
			$langConfig = DBTranslator::getLanguageConfig( $lang );

			$group = $this->modelGroup->findOrFail( $id );

			$pageTitle = 'Add Translation';

			$entry = new $this->model;

			Former::populate( $entry );

			return view('admin/translations/add', compact('pageTitle', 'lang', 'langConfig', 'group', 'entry'));

		} catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

			Session::flash('error', 'Group not found. ID - ' . $id);
			return redirect( route('admin-translations') );
		}
	}

	public function postAdd( $id, $lang )
	{
		try {
			$group = $this->modelGroup->findOrFail( $id );

			$entry = new $this->model;

			$entry->group_id = $group->id;

			$entry->fill( Input::all() );

			if ($entry->save()) {
				Session::flash('success', 'Group successfully created');
				return redirect(
					route( 'admin-translations-edit', ['id' => $group->id, 'lang' => $lang] )
				);
			} else {
				return redirect( route( 'admin-translations-add', ['id' => $group->id, 'lang' => $lang] ) )
					->withErrors( $entry->getErrors() )->withInput();
			}

		} catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

			Session::flash('error', 'Group not found. ID - ' . $id);
			return redirect( route('admin-translations') );
		}
	}

	public function postDelete()
	{
		$ids = Input::get('ids', array());

		$successMessage = 'Translations successfully deleted';

		foreach ($ids as $id => $selected) {

			$id =       (int) $id;
			$selected = (int) $selected;

			if ($selected) {
				$entry = $this->model->find( $id );
				if ($entry) {
					$entry->delete();
				}
			}
		}

		if (Request::ajax()) {
			return Response::json([
				'success' => 1,
				'message' => $successMessage,
			]);
		} else {
			Session::flash('success', $successMessage);
			return redirect( route('admin-translations') );
		}
	}

	public function getDestroy( $groupId, $lang, $id )
	{
		try {
			$group = $this->modelGroup->findOrFail( $groupId );

			$entry = $this->modelValue->findOrFail( $id );

			$successMessage = 'Translation value successfully deleted';

			$entry->delete();

			if (Request::ajax()) {
				return Response::json([
					'success' => 1,
					'message' => $successMessage,
				]);
			} else {
				Session::flash('success', $successMessage);
				return redirect( route('admin-translations-edit', ['id' => $group->id, 'lang' => $lang]) );
			}


		} catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

			$errorMessage = 'Something went wrong. Please reload the page and try again.';

			if (Request::ajax()) {
				return Response::json([
					'success' => 0,
					'message' => $errorMessage,
				]);
			} else {
				Session::flash( 'error', $errorMessage );
				return redirect( route('admin-translations') );
			}


		}
	}
}

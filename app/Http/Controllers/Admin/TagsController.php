<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use WL\Controllers\ControllerAdmin;
use WL\Modules\Taxonomy\Facades\TagService;
use WL\Modules\Taxonomy\Repositories\TagRepositoryInterface as  TagRepository;

class TagsController extends ControllerAdmin
{
	/**
	 * @var TagRepository
	 */
	private $repository;

	/**
	 * @param TagRepository $repository
	 */
	public function __construct(TagRepository $repository)
	{
		parent::__construct();

		$this->repository = $repository;
	}

	/**
	 * Store a newly created resource in storage.
	 * @param Request $request
	 * @param int $tagId
	 * @param bool $makeAlias
	 * @return mixed
	 */
	public function tagsStore(Request $request, $tagId, $makeAlias = false)
	{


		$parentTag = $this->repository->getById($tagId);
		$tagsNames = [];
		foreach ($request->input('tags',[]) as $data){
			if (empty($data['name'])) continue;
			$tagsNames[] = $data['name'];
		}

		if($makeAlias){
			TagService::addAliasesToTag($parentTag->id,$parentTag->taxonomy_id,$tagsNames,['is_custom'=>false]);
		}else{
			TagService::addChildrenToTag($parentTag->id,$parentTag->taxonomy_id,$tagsNames,['is_custom'=>false]);
		}
		return Response::json(['success' => true]);
	}

	/**
	 * Show the form for editing the specified resource.
	 * @param $tagId
	 * @param bool $makeAlias
	 * @return \Illuminate\View\View
	 */
	public function tagsEdit($tagId, $makeAlias = false)
	{
		$tag = $this->repository->getById($tagId);
		return view('admin.tags.tags-save-ajax', compact('tag', 'makeAlias'));
	}

	/**
	 * Store a newly created resource in storage.
	 * @param Request $request
	 * @param $tagId
	 * @return mixed
	 */
	public function aliasesStore(Request $request, $tagId)
	{
		return $this->tagsStore($request, $tagId, true);
	}

	/**
	 * Show the form for editing the specified resource.
	 * @param $tagId
	 * @return \Illuminate\View\View
	 */
	public function aliasesEdit($tagId)
	{
		return $this->tagsEdit($tagId, true);
	}

	/**
	 * Make alias ($tagId) as primary tag
	 * @param $tagId
	 * @param Request $request
	 * @return mixed
	 */
	public function makeAliasAsPrimaryTag($tagId, Request $request)
	{
		$response = redirect($request->header('referer') ?: route("admin.taxonomy.index"));

		$tag = $this->repository->getById($tagId);

		if (TagService::switchWithParent($tagId)) {
			return $response->withError($tag->getErrors());
		}
		return $response->withSuccess("Set alias '{$tag->name}' as default tag.");
	}

	/**
	 * Set tag's new position and parent
	 * @param integer $tagId
	 * @return boolean
	 */
	public function setTagPosition($tagId, Request $request)
	{
		if (!TagService::setParent($tagId,$request->input('parentId')))
			return Response::json(false);

		if ($request->input('afterId')){
			TagService::setPositionAfterTag($tagId,$request->input('afterId',0));
		}elseif ($request->input('beforeId')){
			TagService::setPositionBeforeTag($tagId,$request->input('beforeId',0));
		}

		return Response::json(true);
	}

}

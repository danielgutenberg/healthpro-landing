<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

use \WL\Controllers\ControllerAdmin;
use WL\Modules\Certification\Models\Certification;
use \WL\Controllers\Repository;
use WL\Modules\Certification\Populators\CertificationPopulator;
use WL\Modules\Certification\Repositories\CertificationRepository;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests\Admin\Certification\DeleteRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\Admin\Certification\StoreRequest;
use Watson\Validating\ValidationException;
use WL\Modules\Certification\Populators\FormerCertificationPopulator;

class CertificationsController extends ControllerAdmin {

	use Repository;

	protected $model;

	public function __construct(Certification $model, CertificationRepository $repository) {
		parent::__construct();

		$this->model = $model;

		$this->setRepository($repository);
	}

	/**
	 * Display a listing of the resource.
	 * GET /admin/certifications
	 *
	 * @return Response
	 */
	public function index(Request $request) {
		$certifications = Certification::all();

		return view('admin.certifications.index', compact('certifications'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/certifications/create
	 *
	 * @return Response
	 */
	public function create() {
		return view('admin.certifications.save');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin/certifications
	 *
	 * @return Response
	 */
	public function store(StoreRequest $request, Certification $education) {
		try {
			$education->setFeeder($request);
			$education->fill($request->fields());
			$education->saveOrFail();
		} catch(ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors())
				->withInput();
		}

		return Redirect::route('admin.certifications.edit', ['id' => $education->id])
			->with('success', __('Successfully saved education'));
	}


	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/certifications/{id}/edit
	 *
	 * @param \WL\Modules\Certification\Models\Certification $education
	 * @param \WL\Modules\Certification\Populators\CertificationPopulator $populator
	 * @internal param int $id
	 * @return Response
	 */
	public function edit(Certification $education, CertificationPopulator $populator) {
		$education = $this->getRepository($education);

		$populator->setModel($education)->populate();

		return view('admin.certifications.save', compact('education'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/certifications/{id}
	 *
	 * @param StoreRequest $request
	 * @param Certification $education
	 * @internal param int $id
	 * @return Response
	 */
	public function update(StoreRequest $request, Certification $education) {
		try {
			$education->setFeeder($request);
			$education->fill($request->fields());
			$education->saveOrFail();
		} catch(ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors())
				->withInput();
		}

		return Redirect::route('admin.certifications.edit', ['id' => $education->id])
			->with('success', __('Successfully edited location'));
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin/certifications/{id}
	 *
	 * @param \WL\Modules\Certification\Models\Certification $education
	 * @internal param int $id
	 * @return Response
	 */
	public function destroy(Certification $education) {
		try {
			$this->_repository->destroy($education->id);

			Session::flash('success', 'You have successfully deleted the education');

		} catch (ModelNotFoundException $e) {
			Session::flash('error', 'The education with id ' . $education->id . ' cannot be found');
		}

		return Redirect::route('admin.certifications.index');
	}


	/**
	 * 	Delete multiple certifications
	 *
	 * @param DeleteRequest $request
	 * @return mixed
	 */
	public function delete(DeleteRequest $request) {
		$this->_repository->delete($request->fields());

		$successMessage = 'Entries successfully deleted';

		if ($request->ajax()) {
			return Response::json([
				'success' => 1,
				'message' => $successMessage,
			]);
		}

		return Redirect::route('admin.certifications.index')->withSuccess($successMessage);
	}

}

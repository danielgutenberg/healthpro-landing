<?php
namespace App\Http\Controllers\Admin;

use WL\Controllers\ControllerAdmin;

use Gettext;
use Response;
use Redirect;
use Input;

class LanguageController extends ControllerAdmin
{
	/**
	 * @return Response
	 */
	public function getIndex()
	{
		$locales = Gettext::getLocales();

		return view('admin/language/index', compact('locales'));
	}

	public function getView( $current_locale )
	{
		$locales = Gettext::getLocales();

		if (!in_array( $current_locale, $locales )) {
			return redirect( route( 'admin-language' ) );
		}

		$files = Gettext::getLocaleFiles( $current_locale );

		$pageTitle = $current_locale;

		return view('admin/language/view', compact(
			'locales',
			'current_locale',
			'files',
			'pageTitle'
		));
	}

	public function postUpload( $current_locale )
	{

		if (Input::hasFile('po') && Input::hasFile('mo')) {

			$files = [
				'po'    => Input::file('po')->getPathname(),
				'mo'    => Input::file('mo')->getPathname(),
			];

			Gettext::setLocaleFiles( $files, $current_locale );

			return Redirect::back()
				->with( 'success', 'Files successfully uploaded.' );

		} else {
			return  Redirect::back()
				->with( 'error', 'Please upload both .po and .mo files.' );
		}
	}

	public function getDownload( $current_locale, $file )
	{
		$files = Gettext::getLocaleFiles( $current_locale );

		if ($files && isset($files[$file])) {
			return Response::download($files[$file]);
		} else {
			return view('error/404');
		}
	}

	public function getDownloadPot()
	{
		$file = Gettext::getPotPath();

		if ($file) {
			return Response::download($file);
		} else {
			return view('error/404');
		}
	}

	public function getDelete( $current_locale )
	{
		if ($current_locale) {
			Gettext::deleteLocaleFiles( $current_locale );
		}

		return  Redirect::back()
			->with( 'success', 'Files successfully deleted.' );
	}
}
<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Group\StoreRequest;
use \WL\Controllers\ControllerAdmin;
use WL\Controllers\Repository;
use WL\Modules\ProfileGroup\Populators\GroupPopulator;
use WL\Modules\ProfileGroup\Repositories\GroupRepository;
use DB;
use Watson\Validating\ValidationException;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Redirect;

class ArticlesController extends ControllerAdmin {

	use Repository;

	/**
	 * @var Location
	 */
	protected $model;

	/**
	 * Display a listing of the resource.
	 * GET /admin\groups
	 *
	 * @return Response
	 */
	public function index() {

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin\groups/create
	 *
	 * @return Response
	 */
	public function create() {

	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin\groups
	 *
	 * @return Response
	 */
	public function store() {

	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin\groups/{id}/edit
	 *
	 * @param Group $group
	 * @param GroupPopulator $populator
	 * @internal param int $id
	 * @return Response
	 */
	public function edit() {

	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin\groups/{id}
	 *
	 * @param Group $group
	 * @param StoreRequest $request
	 * @internal param int $id
	 * @return Response
	 */
	public function update() {

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin\groups/{id}
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id) {
		//
	}

}

<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ClientProfile\StoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Redirect;
use Watson\Validating\ValidationException;
use WL\Controllers\ControllerProfile;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ClientProfile as Profile;
use WL\Modules\Profile\Populators\ClientProfilePopulator;
use WL\Modules\Profile\Repositories\ClientProfileRepository;
use WL\Modules\Search\Facades\OrderService;
use WL\Modules\User\Models\User;

class ClientProfilesController extends ControllerProfile
{

	public function __construct(ClientProfileRepository $repository)
	{
		parent::__construct();

		$this->setRepository($repository);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/profiles/create
	 *
	 * @return Response
	 */
	public function create(ClientProfilePopulator $populator, Request $request)
	{
		$entry = $this->getRepository();

		$populator->setModel($entry)->populate();

		$allUsers = User::all(['id', 'email', 'first_name', 'last_name'])->toArray();

		$allUsersEmails = array_column($allUsers, 'email');

		$usersIds = explode(",", $request->get('users', ""));
		$users = [];

		foreach ($usersIds as $id) {
			if ($id) {
				$users[] = User::find($id);
			}
		}


		return view('admin.profiles.client.save', compact('allUsersEmails', 'allUsers', 'users'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin/profiles
	 *
	 * @return \Response
	 */
	public function store(Profile $profile, StoreRequest $request)
	{
		try {
			$profile = $this->getRepository($profile);
			$profile->fill($request->fields());
			$profile->setFeeder($request);
			$profile->saveOrFail();

			$owner = $request->get('profile_owner', null);
			foreach ($request->users() as $id => $email) {

				if ($owner == $id || $owner == null) {
					$profile->users()->attach($id, ['is_owner' => 1]);
					$owner = $id;
				} else {
					$profile->users()->attach($id);
				}

			}

		} catch (ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors())
				->withInput();
		}

		return \Redirect::route('admin.profiles.client.edit', ['id' => $profile->id])->with('success', __('Successfully stored'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/profiles/{id}/edit
	 *
	 * @param Profile $profile
	 * @internal param int $id
	 * @return \Response
	 */
	public function edit($profile, ClientProfilePopulator $populator)
	{
		$profile = ProfileService::getProfileById($profile)->typeInstance();

		if (isset($profile->id)) {
			$profile = $profile->typeInstance();
		}

		$title = __('Edit Profile');

		$users = $profile->users;

		$populator->setModel($profile)->populate();

		$allUsers = User::all(['id', 'email', 'first_name', 'last_name'])->toArray();

		$allUsersEmails = array_column($allUsers, 'email');

		$orders = OrderService::getOrders($profile->id, 1, -1);

		return view('admin.profiles.client.save', compact('profile', 'title', 'users', 'allUsersEmails', 'allUsers', 'orders'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/profiles/{id}
	 *
	 * @param Profile $profile
	 * @param StoreRequest $request
	 * @internal param int $id
	 * @return Response
	 */
	public function update($profile, StoreRequest $request)
	{
		try {
			$profile = Profile::find($profile);
			$profile = $this->getRepository($profile);

			$profile->fill($request->fields());
			$profile->setFeeder($request);

			$profile->users()->detach(array_column($profile->users()->get(['id'])->toArray(), 'id'));
			$owner = $request->get('profile_owner', null);
			foreach ($request->users() as $id => $email) {
				if ($owner == $id || $owner == null) {
					$profile->users()->attach($id, ['is_owner' => 1]);
					$owner = $id;
				} else {
					$profile->users()->attach($id);
				}

			}

			$profile->saveOrFail();

		} catch (ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors())
				->withInput();
		}

		return \Redirect::route('admin.profiles.client.edit', ['id' => $profile->id])->with('success', 'The profile with id ' . $profile->id . ' successfully edited');
	}
}

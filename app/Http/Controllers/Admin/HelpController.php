<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\AdminRequest as Request;
use WL\Controllers\ControllerAdmin;
use WL\Modules\HelpPost\Facades\HelpPostService;
use WL\Modules\HelpPost\Models\HelpPost;
use App\Http\Requests\Admin\Help\CreateRequest;
use App\Http\Requests\Admin\Help\UpdateRequest;
use Redirect;
use Session;
use WL\Modules\Taxonomy\Facades\TagService;
use WL\Modules\Taxonomy\Models\Tag;

class HelpController extends ControllerAdmin {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		if ($request->has('section') || $request->has('category')) {
			Session::set('admin-help-filter.section',  array_map(function($tag){ return $tag->id; }, $request->taxonomyTags('section')) );
			Session::set('admin-help-filter.category', array_map(function($tag){ return $tag->id; }, $request->taxonomyTags('category')) );
			return Redirect::route("admin.help.index");
		}

		$tags = array_merge((array)Session::get("admin-help-filter.section",[]), (array)Session::get("admin-help-filter.category",[]));

		if(count($tags)==2){
			$posts = HelpPostService::getByTags($tags,['withAnyTag'=>false,'tagsField'=>'id']);
		}else{
			$posts = HelpPostService::getByTags($tags,['withAnyTag'=>true,'tagsField'=>'id']);
		}

		return view('admin.help.index', compact('posts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$post = new HelpPost();
		$sectionTags = Session::get("admin-help-filter.section");
		$categoryTags = Session::get("admin-help-filter.category");
		return view('admin.help.save', compact('post','sectionTags','categoryTags'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateRequest $request)
	{
		$post = new HelpPost($request->all());
		if (!$post->save()) {
			return Redirect::back()->withErrors($post->getErrors())->withInput();
		}

		$taxonomy = HelpPostService::getCategoryTaxonomy();
		$tag_ids = $request->input(HelpPostService::getCategorySlug(),[]);
		$tagsNames = [];

		if(is_array($tag_ids)){
			foreach($tag_ids as $id){
				$tagsNames[] = Tag::find($id)->name;
			}
		}
		else{
			$tagsNames[] = Tag::find($tag_ids)->name;
		}

		if($tagsNames)TagService::associateTagsWithEntity($post,$tagsNames,$taxonomy->id);

		$taxonomy = HelpPostService::getSectionTaxonomy();
		$tag_ids = $request->input(HelpPostService::getSectionSlug(),[]);
		$tagsNames = [];

		if(is_array($tag_ids)){
			foreach($tag_ids as $id){
				$tagsNames[] = Tag::find($id)->name;
			}
		}
		else{
			$tagsNames[] = Tag::find($tag_ids)->name;
		}

		if($tagsNames)TagService::associateTagsWithEntity($post,$tagsNames,$taxonomy->id);

		return Redirect::route("admin.help.show", [$post->id])->withSuccess('Save');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = HelpPost::find($id);
		$sections = TagService::getEntityTagsForTaxonomyId($post,HelpPostService::getSectionTaxonomy()->id);
		$categories = TagService::getEntityTagsForTaxonomyId($post,HelpPostService::getCategoryTaxonomy()->id);
		return view('admin.help.show', compact('post','sections','categories'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = HelpPost::find($id);

		$sectionTags = [];
		foreach (TagService::getEntityTagsForTaxonomySlug($post,HelpPostService::getSectionSlug()) as $tag) $sectionTags[] = $tag;
		$categoryTags = [];
		foreach (TagService::getEntityTagsForTaxonomySlug($post,HelpPostService::getCategorySlug()) as $tag) $categoryTags[] = $tag;

		return view('admin.help.save', compact('post','sectionTags','categoryTags'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, UpdateRequest $request)
	{
		$post = HelpPost::find($id);
		$post->fill($request->only('subject','content'));

		if (!$post->save()) {
			return Redirect::back()->withErrors($post->getErrors())->withInput();
		}

		$taxonomy = HelpPostService::getCategoryTaxonomy();
		$tag_ids = $request->input(HelpPostService::getCategorySlug(),[]);
		$tagsNames = [];

		if(is_array($tag_ids)){
			foreach($tag_ids as $id){
				$tagsNames[] = Tag::find($id)->name;
			}
		}
		else{
			$tagsNames[] = Tag::find($tag_ids)->name;
		}

		if($tagsNames)TagService::explicitSetTagsWithEntity($post,$tagsNames,$taxonomy->id);

		$taxonomy = HelpPostService::getSectionTaxonomy();
		$tag_ids = $request->input(HelpPostService::getSectionSlug(),[]);
		$tagsNames = [];

		if(is_array($tag_ids)){
			foreach($tag_ids as $id){
				$tagsNames[] = Tag::find($id)->name;
			}
		}
		else{
			$tagsNames[] = Tag::find($tag_ids)->name;
		}

		if($tagsNames)TagService::explicitSetTagsWithEntity($post,$tagsNames,$taxonomy->id);

		return Redirect::route("admin.help.show", [$post->id])->withSuccess('Save');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if ($post = HelpPost::find($id)){
			$taxonomy = HelpPostService::getSectionTaxonomy();
			$tags = TagService::getEntityTagsForTaxonomyId($post,$taxonomy->id);
			$tagsNames = [];
			foreach($tags as $tag){
				$tagsNames[] = $tag->name;
			}
			if($tagsNames)TagService::removeAssociateTagsWithEntity($post,$tagsNames,$taxonomy->id);

			$taxonomy = HelpPostService::getCategoryTaxonomy();
			$tags = TagService::getEntityTagsForTaxonomyId($post,$taxonomy->id);
			$tagsNames = [];
			foreach($tags as $tag){
				$tagsNames[] = $tag->name;
			}

			if($tagsNames)TagService::removeAssociateTagsWithEntity($post,$tagsNames,$taxonomy->id);

			$post->delete();
			return Redirect::route("admin.help.index")->withSuccess("Help #{$id} was deleted");
		}
		return Redirect::back()->withErrors("Help #'{$id}' not found");
	}

}

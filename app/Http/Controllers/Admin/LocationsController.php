<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\Location\DeleteRequest;
use App\Http\Requests\Admin\Location\StoreRequest;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Redirect;
use Watson\Validating\ValidationException;
use WL;
use WL\Controllers\ControllerAdmin;
use WL\Modules\Location\Models\Country;
use WL\Modules\Location\Models\Location;
use WL\Modules\Location\Models\LocationType;
use WL\Modules\Location\Populators\LocationPopulator;
use WL\Modules\Location\Facades\LocationService;

class LocationsController extends ControllerAdmin
{

	/**
	 * Display a listing of the resource.
	 * GET /admin/locations
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$locations = LocationService::getLocations();

		if ($request->ajax()) {
			return Response::json(
				$locations->map(function ($location) {
					return ['label' => $location->name, 'id' => $location->id];
				})->toArray()
			);
		}

		return view('admin.locations.index', compact('locations'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/locations/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$location = new Location();
		$locationTypes = LocationType::all()->pluck('name', 'id')->all();
		$locationTypesJson = LocationType::all()->toJson();

		$countries = Country::all()->map(function ($location) {
			return ['id' => $location->id, 'name' => str_replace("'", " ", $location->name)];
		})->pluck('name', 'id')->all();

		return view('admin.locations.save', compact('location', 'locationTypes', 'locationTypesJson', 'countries'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin/locations
	 *
	 * @return Response
	 */
	public function store(StoreRequest $request, Location $location)
	{
		try {
			DB::transaction(function () use ($request, $location) {
				$location->setFeeder($request);
				$location->fill($request->except(['_token']));
				$location->saveOrFail();

				if ($request->locationType() && $address = $location->getFeeder()->address()) {
					if ($locationType = LocationType::find($request->locationType())) {
						if ($locationType->type == 'on-site') {
							$location->syncPhones($location->getPhoneObjects($location->getFeeder()->phones() ?: []));

							$location->savePostalCode($address);
						} else {
							$location->saveVirtualAddress($request->get('address'));
						}
					}
				}
			});
		} catch (ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors())
				->withInput();
		}

		return Redirect::route('admin.locations.edit', ['id' => $location->id])
			->with('success', __('Successfully saved location'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/locations/{id}/edit
	 *
	 * @param Location $location
	 * @param LocationPopulator $populator
	 * @internal param int $id
	 * @return Response
	 */
	public function edit(Location $location, LocationPopulator $populator)
	{
		$countries = Country::all()->map(function ($location) {
			return ['id' => $location->id, 'name' => str_replace("'", " ", $location->name)];
		})->pluck('name', 'id')->all();

		$populator->setModel($location)->populate();

		$profiles = $location->profiles;

		$locationTypes = LocationType::all()->pluck('name', 'id')->all();
		$locationTypesJson = LocationType::all()->toJson();

		return view('admin.locations.save', compact('location', 'profiles', 'locationTypes', 'locationTypesJson', 'countries'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/locations/{id}
	 *
	 * @param StoreRequest $request
	 * @param Location $location
	 * @internal param int $id
	 * @return Response
	 */
	public function update(StoreRequest $request, Location $location)
	{
		try {
			DB::transaction(function () use ($request, $location) {
				$location->setFeeder($request);
				$location->update($request->all());
				$location->fill($request->fields());
				$location->saveOrFail();

				if ($request->locationType() && $address = $location->getFeeder()->address()) {
					if ($locationType = LocationType::find($request->locationType())) {
						if ($locationType->type == 'on-site') {
							$location->syncPhones($location->getPhoneObjects($location->getFeeder()->phones() ?: []));

							$location->savePostalCode($address);
						} else {
							$location->saveVirtualAddress($request->get('address'));
						}
					}
				}
			});
		} catch (ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors())
				->withInput();
		}

		return Redirect::route('admin.locations.edit', ['id' => $location->id])
			->with('success', __('Successfully edited location'));
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin/locations/{id}
	 *
	 * @param Location $location
	 * @internal param int $id
	 * @return Response
	 */
	public function destroy(Location $location)
	{
		try {
			LocationService::removeLocation($location->id);
			Session::flash('success', 'You have successfully deleted the page');

		} catch (ModelNotFoundException $e) {
			Session::flash('error', 'The location with id ' . $location->id . ' cannot be found');
		}

		return Redirect::route('admin.locations.index');
	}

	/**
	 *    Remove locations by ids ...
	 *
	 * @param Location $profile
	 * @param DeleteRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete(Location $profile, DeleteRequest $request)
	{
		try {
			foreach ($request->get('ids',[]) as $locaationId){
				LocationService::removeLocation($locationId);
			}

			$successMessage = 'Entries successfully deleted';

			\Session::flash('success', $successMessage);

			if ($request->ajax()) {
				return Response::json([
					'success' => 1,
					'message' => $successMessage,
				]);
			}
		} catch (ModelNotFoundException $e) {
			\Session::flash('error', 'The profile with id ' . $profile->id . ' cannot be found');
		}

		if (!$request->ajax()) {
			return Redirect::route('admin.locations.index');
		}
	}

}

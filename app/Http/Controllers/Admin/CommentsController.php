<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Comment\UpdateRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use WL\Controllers\ControllerAdmin;
use WL\Modules\Comment\Populators\CommentPopulator;
use WL\Modules\Comment\Services\CommentServiceInterface;

class CommentsController extends ControllerAdmin
{
	private $commentService;

	/**
	 * @param CommentServiceInterface $commentService
	 */
	public function __construct(CommentServiceInterface $commentService)
	{
		parent::__construct();

		$this->commentService = $commentService;

		// TODO Very bad!
		$this->renderVar('repository', $commentService);
	}

	public function index($resource, $resourceId)
	{
		$entries = $this->commentService->getCommentsForEntity($resource, $resourceId);

		return $this->render('admin.comments.index', compact('entries', 'resourceId', 'resource'));
	}

	/**
	 * @param $resource
	 * @param $resourceId
	 * @param $id
	 * @param CommentPopulator $populator
	 * @return \WL\Controllers\Response
	 */
	public function edit($resource, $resourceId, $id, CommentPopulator $populator)
	{
		$entry = $this->commentService->getComment($id);

		$populator->setModel($entry)->populate();

		$pageTitle = __('Edit Comment');

		return $this->render('admin.comments.edit', compact('entry', 'resourceId', 'resource'));
	}

	public function update($resource, $resourceId, $id, UpdateRequest $request)
	{
		try {
			$entry = $this->commentService->getById($id);

			$entry->fill($request->fields());

			if ($entry->save()) {
				Session::flash('success', 'You have successfully edited the comment');
				return Redirect::route('admin.comments.edit', ['resource' => $resource, 'resourceId' => $resourceId, 'id' => $entry->id]);

			} else {
				Session::flash('error', 'Please fix the errors before saving the comment');
				return Redirect::back()
					->withInput($request->invalidFields())
					->withErrors($entry->getErrors());
			}

		} catch (ModelNotFoundException $e) {
			Session::flash('error', 'The comment with id ' . $id . ' cannot be found');
			return Redirect::route('admin.comments.index', ['resource' => $resource, 'resourceId' => $resourceId]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param string $resource
	 * @param int $resourceId
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($resource, $resourceId, $id)
	{
		try {
			$entry = $this->commentService->destroy($id);
			Session::flash('success', 'You have successfully deleted the comment');
		} catch (ModelNotFoundException $e) {
			Session::flash('error', 'The page with id ' . $id . ' cannot be found');
		}

		return Redirect::route('admin.comments.index', ['resource' => $resource]);
	}
}

<?php namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;
use Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use WL\Controllers\ControllerAdmin;
use Sentinel;
use WL\Modules\SalesForce\Commands\SalesForceQueueAll;
use WL\Modules\SalesForce\Models\SalesforceSyncQueue;

class SalesforceController extends ControllerAdmin {

	use DispatchesJobs;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	}

	/**
	 * Display a listing of the queued items.
	 *
	 * @return Response
	 */
	public function queue()
	{
		$queue = SalesforceSyncQueue::all();
		return view('admin.salesforce.queue', compact('queue'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Start sync process
	 * POST /admin/salesforce/execqueue
	 *
	 * @return Response
	 */
	public function execQueue() {
		try {
			Artisan::queue('salesforce:sync');
			Session::flash('success', 'You have successfully started the sync process');

		} catch (Exception $e) {
			Session::flash('error', 'Fail to start sync');
		}

		return Redirect::route('admin.salesforce.queue');
	}

	/**
	 * Insert all providers into que
	 * POST /admin/salesforce/queueall
	 *
	 * @return Response
	 */
	public function queueAll() {
		try {
			$this->dispatch(new SalesForceQueueAll());
			Session::flash('success', 'You have successfully added all providers into the queue');

		} catch (Exception $e) {
			Session::flash('error', 'Fail to insert into queue');
		}

		return Redirect::route('admin.salesforce.queue');
	}

}

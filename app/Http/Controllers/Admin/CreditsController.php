<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Certification\CouponRequest;
use Illuminate\Http\Request;
use WL\Controllers\ControllerAdmin;
use WL\Modules\Comment\Populators\CouponPopulator;
use WL\Modules\Coupons\Facades\CouponService;
use WL\Modules\Coupons\Models\Coupon;
use WL\Modules\Coupons\ValueObjects\CreateCreditRequest;
use WL\Modules\Profile\Models\ModelProfile;

class CreditsController extends ControllerAdmin
{
	public function index()
	{
		$applyTo = [ModelProfile::CLIENT_PROFILE_TYPE, ModelProfile::PROVIDER_PROFILE_TYPE];
		$coupons = Coupon::whereIn('applyTo', $applyTo)->get();

		return view('admin.credits.index', ['coupons' => $coupons]);
	}

	public function edit($couponId)
	{
		$coupon = CouponService::getCoupon($couponId);
		$populator = new CouponPopulator();
		$populator->setModel($coupon)->populate();

		return view('admin.credits.save', compact('coupon'));
	}

	public function create()
	{
		return view('admin.credits.save', []);
	}

	public function store(CouponRequest $request)
	{
		$name = $request->input('name');
		$description = $request->input('description');
		$applyTo = $request->input('apply_to');
		$amount = $request->input('amount', 0);
		$validFrom = $request->input('valid_from');
		$validUntil = $request->input('valid_until');

		$createCoupon = new CreateCreditRequest($name, $description, $amount, $applyTo, $validFrom, $validUntil);
		CouponService::createCoupon($createCoupon);

		return redirect(route('admin.credits.index'));
	}

	public function destroy(Request $request)
	{
		CouponService::removeById($request->input('id'));

		return redirect(route('admin.credits.index'));
	}
}

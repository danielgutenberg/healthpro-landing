<?php namespace App\Http\Controllers\Admin;

use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Input;
use WL\Controllers\ControllerAdmin;
use WL\Modules\Meta\Commands\LoadAvailableRouteSeoSettingsCommand;
use WL\Modules\Meta\Commands\LoadRouteSeoSettingsCommand;
use WL\Modules\Meta\Commands\SaveRouteSeoSettingsCommand;

class SeoSettingsController extends ControllerAdmin
{
	use DispatchesJobs;

	public function getIndex($routeName = null)
	{

		if ($routeName) {
			$fields = $this->dispatch(new LoadRouteSeoSettingsCommand($routeName));
			return view('admin.seo.settings', compact('routeName', 'fields'));
		}

		$routes = $this->dispatch(new LoadAvailableRouteSeoSettingsCommand());
		return view('admin.seo.index', compact('routes'));
	}

	public function save($routeName)
	{
		try {
			$this->dispatch(new SaveRouteSeoSettingsCommand($routeName, Input::all()));
		} catch (ValidationException $ex) {

		}

		return back();
	}
}

<?php namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use WL\Modules\Attachment\Facades\AttachmentService;
use WL\Modules\Certification\Models\Certification;
use WL\Modules\Education\Models\Education;
use App\Http\Requests\Admin\Education\StoreRequest as EducationRequest;
use App\Http\Requests\Admin\Location\StoreRequest as LocationRequest;
use App\Http\Requests\Admin\ProviderProfile\StoreRequest;
use WL\Modules\HasOffers\Events\IsSearchableEvent;
use WL\Modules\HasOffers\Events\RegisteredEvent;
use WL\Modules\Payment\Commands\GetPaymentOperationsCommand;
use WL\Modules\Payment\Facades\PaymentService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Modules\Provider\Models\ProviderPlanAction;
use WL\Modules\Search\Facades\OrderService;
use WL\Modules\User\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Input;
use Redirect;
use Watson\Validating\ValidationException;
use WL\Controllers\ControllerProfile;
use WL\Helpers\Html as HtmlHelper;
use WL\Modules\Location\Models\Location;
use WL\Modules\Profile\Models\ProviderProfile as Profile;
use WL\Modules\Profile\Repositories\ProviderProfileRepository;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\Taxonomy\Models\Taxonomy;
use WL\Modules\Taxonomy\Services\TaxonomyServiceInterface;
use WL\Modules\Profile\Populators\ProviderProfilePopulator;
use Illuminate\Support\Facades\Crypt;

class ProviderProfilesController extends ControllerProfile
{
    use DispatchesJobs;

	public function __construct(ProviderProfileRepository $repository)
	{
		parent::__construct();

		$this->setRepository($repository);
	}


	/**
	 * Show the form for creating a new resource.
	 * GET /admin/profiles/create
	 *
	 * @return Response
	 */
	public function create(ProviderProfilePopulator $populator, Request $request)
	{
		$certifications = HtmlHelper::selectMultiple('certifications', Certification::all());

		$entry = $this->getRepository();

		$populator->setModel($entry)->populate();

		$allUsers = User::all(['id', 'email', 'first_name', 'last_name'])->toArray();

		$allUsersEmails = array_column($allUsers, 'email');

		$usersIds = explode(",", $request->get('users', ""));
		$users = [];

		foreach ($usersIds as $id) {
			if ($id) $users[] = User::find($id);
		}

		return view('admin.profiles.provider.save', compact('certifications', 'allUsersEmails', 'allUsers', 'users'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin/profiles
	 *
	 * @param Profile $profile
	 * @param StoreRequest $request
	 * @return \Response
	 */
	public function store(Profile $profile, StoreRequest $request)
	{
		try {
			$profile->fill($request->fields() + [
					'is_searchable' => Input::get('is_searchable', 0),
					'is_bookable' => Input::get('is_bookable', 0)
				]
			);
			$profile->setFeeder($request);
			$profile->saveOrFail();

			/** @var Save certifications .. $certifications */
			$profile->saveCertifications($profile->getFeeder()->certifications());

			/** Upload profile attachments if exists .. */

			$attachments = $profile->getFeeder()->attachments();
			if (!$attachments) {
				foreach ($attachments as $attachmentType => $attachmentFile) {
						AttachmentService::uploadImage($attachmentType, $this, $attachmentFile);
				}
			}


			$owner = $request->get('profile_owner', null);
			foreach ($request->users() as $id => $email) {

				if ($owner == $id || $owner == null) {
					$profile->users()->attach($id, ['is_owner' => 1]);
					$owner = $id;
				} else {
					$profile->users()->attach($id);
				}

			}

		} catch (ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors())
				->withInput();
		}

		return Redirect::route('admin.profiles.provider.edit', ['id' => $profile->id])->with('success', __('Successfully stored'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/profiles/{id}/edit
	 *
	 * @param Profile $profile
	 * @param ProviderProfilePopulator $populator
	 * @return \Response
	 * @internal param int $id
	 */
	public function edit($profile, ProviderProfilePopulator $populator)
	{
		$profile = app(ModelProfileService::class)->getProfileById($profile)->typeInstance();

		if (isset($profile->id))
			$profile = $profile->typeInstance();

		$title = __('Edit Profile');

		$intersected = array_map(function ($certification) {
			return $certification['id'];
		}, $profile->certifications->toArray());

		$certifications = HtmlHelper::selectMultiple(
			'certifications',
			Certification::all(),
			$profile->certifications,
			$intersected
		);

		$educations = $profile->educations;
		$locations = $profile->locations;
		$users = $profile->users;
		$cards = PaymentService::getCards($profile->id);

		$profile->card = count($cards) > 0 ? '************' . $cards[0]->getSourceEntity()->card_number : '';

		$fields = ['accepted_terms_conditions', 'phone', 'first_name', 'last_name'];
		$accepted = ProfileService::loadProfileMetaFields($profile->id, $fields);
		$params = ProfileService::loadProfileMetaFieldHashed($profile->id, 'has_offers_id') ?: [];
		$plan = ProviderMembershipService::getActivePlan($profile->id);
		$memberHistory = new \stdClass();
        $memberHistory->first_activation = ProviderMembershipService::getFirstActivation($profile->id);
        $lastAction = ProviderMembershipService::getProviderActions($profile->id)->first();
        $memberHistory->cancel_date = null;
        $until_time = Carbon::now();
        if($lastAction && $lastAction->action == ProviderPlanAction::ACTION_CANCEL) {
            $memberHistory->cancel_date = $until_time = $lastAction->request_date;
        }
        $memberHistory->time_since = $until_time->diffForHumans($memberHistory->first_activation, true);
        $operations = $this->dispatch(new GetPaymentOperationsCommand(['profile_id' => $profile->id]));

		$profile->affiliateId = array_get($params, 'affiliate_id');
		$profile->offerId = array_get($params, 'offer_id');
		$profile->terms = $accepted['accepted_terms_conditions'] == null ? 0 : 1;
		$profile->phone = $accepted['phone'] ?: '';
		$profile->first_name = $accepted['first_name'];
		$profile->last_name = $accepted['last_name'];
		$profile->commission_free_period_in_days = ProfileService::getCommissionFreePeriod($profile->id);
		$profile->credit = PaymentService::getCredit($profile->id);


		$allUsers = User::all(['id', 'email', 'first_name', 'last_name'])->toArray();
		$allUsersEmails = array_column($allUsers, 'email');

		$taxonomies = app(TaxonomyServiceInterface::class)->getTaxonomies();

		$populator->setModel($profile)->populate();

		return view('admin.profiles.provider.save', compact('profile', 'title', 'certifications', 'educations', 'locations', 'users', 'taxonomies', 'allUsers', 'allUsersEmails', 'plan', 'memberHistory', 'operations'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/profiles/{id}
	 *
	 * @param int $profile
	 * @param StoreRequest $request
	 * @internal param int $id
	 * @return Response
	 */
	public function update($profile, StoreRequest $request)
	{
		try {
			$profile = Profile::find($profile);
			$profile = $this->getRepository($profile);

			ProfileService::setCommissionFreePeriod($profile->id,Input::get('commission_free_period_in_days'));

			$profile->fill($request->fields() + ['is_searchable' => Input::get('is_searchable', 0)]);
			$profile->setFeeder($request);

			$profile->users()->detach(array_column($profile->users()->get(['id'])->toArray(), 'id'));
			$owner = $request->get('profile_owner', null);
			foreach ($request->users() as $id => $email) {
				if ($owner == $id || $owner == null) {
					$profile->users()->attach($id, ['is_owner' => 1]);
					$owner = $id;
				} else {
					$profile->users()->attach($id);
				}

			}

			$profile->saveOrFail();

			if (Input::get('new_affiliate') && Input::get('offer_id') && Input::get('affiliate_id')) {
				$dataObject = [
					'affiliate_id' => (int) Input::get('affiliate_id'),
					'offer_id' => (int) Input::get('offer_id')
				];
				$hasOffersId = base64_encode(Crypt::encrypt($dataObject));
				ProfileService::storeProfileMetaFields($profile->id, ['has_offers_id' => $hasOffersId], ['has_offers_id']);
				event(new RegisteredEvent($profile, 'originated_from_admin'));
				if (Input::get('is_searchable')) {
					event(new IsSearchableEvent($profile, 'originated_from_admin'));
				}
			}

			/** @var Save certifications .. $certifications */
			$profile->saveCertifications($profile->getFeeder()->certifications());

			$profile->syncPhones($profile->getPhoneObjects($profile->getFeeder()->phones() ?: []));

			/** Upload profile attachments if exists .. */
			$attachments = $profile->getFeeder()->attachments();
			if (!$attachments) {
				foreach ($attachments as $attachmentType => $attachmentFile) {
					AttachmentService::uploadImage($attachmentType, $this, $attachmentFile);
				}
			}

		} catch (ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors())
				->withInput();
		}

		return Redirect::route('admin.profiles.provider.edit', ['id' => $profile->id])->with('success', 'The profile with id ' . $profile->id . ' successfully edited');
	}

	/**
	 *    Attach tags to profile ...
	 *
	 * @param $profileID
	 * @param $taxonomyID
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function profileTags($profileID, $taxonomyID, Request $request)
	{
		$profile = Profile::whereId($profileID)->first();
		$taxonomy = Taxonomy::whereId($taxonomyID)->first();

		if ($request->isMethod('POST')) {
			$profile->syncTagsByTaxonomy($request->get('tags', []), $taxonomy);

			return Redirect::route('admin.profiles.provider.edit', ['id' => $profile->id])->with('success', __('Successfully saved'));
		}

		return view('admin.profiles.provider.additional.profile-tags', compact('profile', 'taxonomy'));
	}


	/**
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Attach Education to profile                                        *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 *
	 * @param ProviderProfilePopulator $populator
	 * @param $profileID
	 * @param null $id
	 * @return \Illuminate\View\View
	 */

	public function profileViewAttachedEducation(ProviderProfilePopulator $populator, $profileID, $id = null)
	{
		$profile = Profile::whereId($profileID)->first();

		if ($id && $pivot = $profile->educations()->wherePivot('id', $id)->first()) {
			$populator->setModel($profile)->populateFields($pivot->toArray()['pivot']);
		}

		return view('admin.profiles.provider.additional.attach-education', compact('profile', 'pivot'));
	}

	/**
	 * @param EducationRequest $request
	 * @param $profileID
	 * @return string
	 */
	public function profileAttachEducation(EducationRequest $request, $profileID)
	{
		$profile = Profile::whereId($profileID)->first();
		$education = Education::whereId($request->get('education_id'))->first();

		if ($education === null) {

			if (!$request->get('education_id', "") && ($name = $request->get('education_name', false))) {
				$education = new Education(['name' => $name, 'is_custom' => 1]);
				$education->save();
			} else {
				return json_encode([
					'error' => true,
					'message' => __('Custom Education Name empty')
				]);
			}
		}

		if ($id = $request->get('id')) {
			$profile->updateAttachedEducationById($id, $request->except(['_token', 'education_id', 'education_name']));
		} else {
			$profile->attachEducation($education, $request->except(['_token', 'education_id', 'education_name']));
		}

		return json_encode([
			'error' => false,
			'message' => __('Successfully saved'),
//			'data'      => $profile->educations()->orderBy('profiles_educations.id','desc')->first()
		]);
	}

	/**
	 * @param $profileID
	 * @param $educationID
	 * @return string
	 */
	public function profileDetachEducation($profileID, $educationID)
	{
		$profile = Profile::whereId($profileID)->first();

		try {
			$profile->detachEducations([$educationID]);

			return json_encode([
				'error' => false,
				'message' => __('Successfully deleted')
			]);
		} catch (ValidationException $e) {
			return json_encode([
				'error' => true,
				'messages' => $e->getErrors()
			]);
		}
	}


	/**
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Attach Location to profile                                         *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */

	/**
	 * @param ProviderProfilePopulator $populator
	 * @param $profileID
	 * @param null $id
	 * @return \Illuminate\View\View
	 */
	public function profileViewAttachedLocation(ProviderProfilePopulator $populator, $profileID, $id = null)
	{
		$profile = Profile::whereId($profileID)->first();

		if ($id && $pivot = $profile->locations()->wherePivot('id', $id)->first()) {
			$populator->setModel($profile)->populateFields($pivot->toArray()['pivot']);
		}

		return view('admin.profiles.provider.additional.attach-location', compact('profile', 'pivot'));
	}

	/**
	 * @param LocationRequest $request
	 * @param $profileID
	 * @return string
	 */
	public function profileAttachLocation(LocationRequest $request, $profileID)
	{
		$profile = Profile::whereId($profileID)->first();
		$location = Location::whereId($request->get('location_id'))->first();

		if ($id = $request->get('id')) {
			$profile->updateAttachedLocationById($id, $request->except(['_token', 'location_id']));
		} else {
			$profile->attachLocation($location, $request->except(['_token', 'location_id']));
		}

		return json_encode([
			'error' => false,
			'message' => __('Successfully saved')
		]);
	}

	/**
	 * @param Request $request
	 * @param $profileID
	 * @param $locationID
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function profileDetachLocation(Request $request, $profileID, $locationID)
	{
		$profile = Profile::whereId($profileID)->first();

		$profile->detachLocations([$locationID]);

		if ($request->ajax()) {
			return Response::json([
				'error' => false,
				'message' => __('Successfully deleted')
			]);
		}

		return Redirect::back()
			->with('success', __('Successfully detached'));
	}


}

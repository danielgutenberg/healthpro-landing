<?php
namespace App\Http\Controllers\Admin;

use Whoops\Example\Exception;
use WL\Controllers\ControllerAdmin;

use Translation;
use Response;
use Request;
use Redirect;
use Input;
use Former;
use Session;
use Watson\Validating\ValidationException;

class TranslationsGroupsController extends ControllerAdmin
{
	protected $model;

	public function __construct( \WL\Translator\Models\TranslationStringGroup $model )
	{
		$this->model = $model;
	}

	public function getCreate()
	{
		$pageTitle = 'Create Group';

		$entry = new $this->model;

		Former::populate( $entry );

		return view('admin.translations.groups.create', compact('pageTitle', 'entry'));
	}

	public function postCreate()
	{
		$entry = new $this->model;

		$entry->fill( Input::all() );

		if ($entry->save()) {
			Session::flash('success', 'Group successfully created');
			return redirect(
				route( 'admin-translations' )
			);
		} else {
			return redirect( route( 'admin-translations-groups-create' ) )
				->withErrors( $entry->getErrors() )->withInput();
		}
	}

	public function getEdit( $id )
	{
		$pageTitle = 'Edit Group';

		try {
			$entry = $this->model->findOrFail( $id );

			Former::populate( $entry );

			return view('admin.translations.groups.edit', compact('pageTitle', 'entry'));

		} catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

			Session::flash('error', 'Group not found. ID - ' . $id);
			return redirect( route('admin-translations') );
		}
	}

	public function postEdit( $id )
	{
		try {
			$entry = $this->model->findOrFail( $id );

			$entry->fill( Input::all() );

			$entry->saveOrFail();

			Session::flash('success', 'Entry successfully created');

			return redirect( route('admin-translations') );

		} catch (ValidationException $e) {

			return redirect( route( 'admin-translations-groups-edit', ['id' => $id] ) )
				->withErrors( $e->getErrors() )->withInput();

		} catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

			Session::flash('error', 'Group not found. ID - ' . $id);
			return redirect( route('admin-translations') );
		}
	}

	public function postDelete()
	{
		$ids = Input::get('ids', array());

		$successMessage = 'Groups successfully deleted';

		foreach ($ids as $id => $selected) {

			$id =       (int) $id;
			$selected = (int) $selected;

			if ($selected) {
				$entry = $this->model->find( $id );
				if ($entry) {
					$entry->delete();
				}
			}
		}

		if (Request::ajax()) {
			return Response::json(array(
				'success' => 1,
				'message' => $successMessage,
			));
		} else {
			Session::flash('success', $successMessage);
			return redirect( route('admin-translations') );
		}
	}

	public function getDestroy( $id )
	{
		try {
			$entry = $this->model->findOrFail( $id );

			$entry->delete();

			Session::flash('success', 'Group successfully deleted');

		} catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e ) {

			Session::flash('error', 'Group not found. ID - ' . $id);
		}

		return redirect( route('admin-translations') );
	}
}

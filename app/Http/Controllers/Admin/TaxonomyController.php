<?php namespace App\Http\Controllers\Admin;

use WL\Controllers\ControllerAdmin;
use Response;
use WL\Modules\Taxonomy\Facades\TagService;
use WL\Modules\Taxonomy\Repositories\TagRepositoryInterface as TagRepository;
use WL\Modules\Taxonomy\Repositories\TaxonomyRepositoryInterface as TaxonomyRepository;
use \Watson\Validating\ValidationException;
use App\Http\Requests\Admin\Taxonomy\CreateRequest;
use App\Http\Requests\Admin\Taxonomy\UpdateRequest;
use App\Http\Requests\Admin\Taxonomy\TagUpdateRequest;

class TaxonomyController extends ControllerAdmin
{

	/**
	 * @var TaxonomyRepository
	 */
	private $repository;

	/**
	 * @param TaxonomyRepository $repository
	 */
	public function __construct( TaxonomyRepository $repository )
	{
		parent::__construct();

		$this->repository = $repository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$taxonomies = $this->repository->getList();
		return view('admin.taxonomy.index', compact('taxonomies'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$taxonomy = null;
		return view('admin.taxonomy.save-ajax', compact('taxonomy'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param CreateRequest $request
	 * @return Response
	 */
	public function store(CreateRequest $request)
	{
		$taxonomy = $this->repository->createNew();
		$taxonomy->name = $request->input('name');
		$taxonomy->act_like = $request->input('act_like');
		$taxonomy->allow_custom_tags = !empty($request->input('allow_custom_tags'));
		if ($taxonomy->isValid()) {
			$taxonomy->save();
			return Response::json($taxonomy->toArray());
		}
		return Response::json($taxonomy->getErrors(), 422);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$taxonomy = $this->repository->getById($id);
		return view('admin.taxonomy.show', compact('taxonomy'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$taxonomy = $this->repository->getById($id);
		return view('admin.taxonomy.save-ajax', compact('taxonomy'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @param UpdateRequest $request
	 * @return Response
	 */
	public function update($id, UpdateRequest $request)
	{
		$taxonomy = $this->repository->getById($id) ;
		$taxonomy->name = $request->input('name');
		$taxonomy->act_like = $request->input('act_like');
		$taxonomy->allow_custom_tags = !empty($request->input('allow_custom_tags'));
		if ($taxonomy->isValid()) {
			$taxonomy->save();
			return Response::json($taxonomy->toArray());
		}
		return Response::json($taxonomy->getErrors(), 422);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if (null == ($taxonomy = $this->repository->getById($id)) ){
			return redirect(route("admin.taxonomy.index"))->withErrors("Taxonomy '{$id}' not found");
		}
		$taxonomy->delete();
		return redirect(route("admin.taxonomy.index"))->withSuccess("Taxonomy '{$taxonomy->name}' was deleted.");
	}

	public function tagsEdit($taxonomyId)
	{
		$taxonomy = $this->repository->getById($taxonomyId);
		return view('admin.taxonomy.tags-save-ajax', compact('taxonomy'));
	}

	public function tagsStore($taxonomyId, TagUpdateRequest $request)
	{
		$taxonomy = $this->repository->getById($taxonomyId);
		$tagsNames = [];
		foreach ($request->input('tags',[]) as $data){

			if (empty($data['name'])) continue;

			if(!empty($data['id'])) {
				continue;
			}

			$tagsNames[] = $data['name'];
		}
		TagService::forceAddTagNames($tagsNames,$taxonomy->id,['is_custom'=>false]);
		return Response::json(['id'=>$taxonomy->id]);
	}

}

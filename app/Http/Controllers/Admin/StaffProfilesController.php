<?php namespace App\Http\Controllers\Admin;

use WL\Modules\User\Models\User;
use Illuminate\Http\Request;
use WL\Controllers\ControllerProfile;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\StaffProfile as Profile;

use WL\Modules\Profile\Repositories\StaffProfileRepository;

use App\Http\Requests\Admin\StaffProfile\StoreRequest;
use Watson\Validating\ValidationException;

use Redirect;
use Illuminate\Support\Facades\Response;
use WL\Modules\Profile\Populators\StaffProfilePopulator;

class StaffProfilesController extends ControllerProfile {

	public function __construct(StaffProfileRepository $repository) {
		parent::__construct();

		$this->setRepository($repository);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/profiles/create
	 *
	 * @return Response
	 */
	public function create(StaffProfilePopulator $populator,Request $request) {
		$entry = $this->getRepository();

		$populator->setModel($entry)->populate();

		$allUsers = User::all(['id','email','first_name','last_name'])->toArray();

		$allUsersEmails = array_column($allUsers,'email');

		$usersIds = explode(",",$request->get('users',""));
		$users = [];

		foreach($usersIds as $id){
			if($id)$users[] = User::find($id);
		}

		return view('admin.profiles.staff.save',compact('allUsersEmails','allUsers','users'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin/profiles
	 *
	 * @return \Response
	 */
	public function store(Profile $profile, StoreRequest $request) {
		try {
			$profile = $this->getRepository($profile);
			$profile->fill($request->fields());
			$profile->setFeeder($request);
			$profile->saveOrFail();

			$owner = $request->get('profile_owner',null);
			foreach($request->users() as $id => $email){

				if($owner == $id || $owner == null){
					$profile->users()->attach($id,['is_owner'=>1]);
					$owner = $id;
				}else{
					$profile->users()->attach($id);
				}

			}

		} catch(ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors())
				->withInput();
		}

		return \Redirect::route('admin.profiles.staff.edit', ['id' => $profile->id])->with('success', __('Successfully stored'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/profiles/staff/{id}/edit
	 *
	 * @param Profile $profile
	 * @param StaffProfilePopulator $populator
	 * @internal param int $id
	 * @return \Response
	 */
	public function edit($profile, StaffProfilePopulator $populator)
	{
		$profile = ProfileService::getProfileById($profile)->typeInstance();

		if( isset($profile->id) )
			$profile = $profile->typeInstance();

		$title = __('Edit Profile');

		$users			= $profile->users;

		$populator->setModel($profile)->populate();

		$allUsers = User::all(['id','email','first_name','last_name'])->toArray();

		$allUsersEmails = array_column($allUsers,'email');


		return view('admin.profiles.staff.save', compact('profile', 'title','users','allUsersEmails','allUsers'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/profiles/staff/{id}
	 *
	 * @param Profile $profile
	 * @param StoreRequest $request
	 * @internal param int $id
	 * @return Response
	 */
	public function update( $profile, StoreRequest $request) {
		try {

			$profile = Profile::find($profile);
			$profile = $this->getRepository($profile);

			/**
			 * @var Profile $profile
			 */
			$profile->fill($request->fields());
			$profile->setFeeder($request);
			$profile->saveMany($request->meta());

			$profile->users()->detach(array_column($profile->users()->get(['id'])->toArray(),'id'));
			$owner = $request->get('profile_owner',null);
			foreach($request->users() as $id => $email){
				if($owner == $id || $owner == null){
					$profile->users()->attach($id,['is_owner'=>1]);
					$owner = $id;
				}else{
					$profile->users()->attach($id);
				}

			}

			$profile->saveOrFail();

		} catch(ValidationException $e) {
			return Redirect::back()
				->withErrors($e->getErrors())
				->withInput();
		}

		return \Redirect::route('admin.profiles.staff.edit', ['id' => $profile->id])->with('success', 'The profile with id ' . $profile->id . ' successfully edited');
	}
}

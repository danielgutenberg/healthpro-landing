<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin;
use App\Http\Requests\Admin\Location\StoreRequest;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Redirect;
use Watson\Validating\ValidationException;
use WL;
use WL\Controllers\ControllerAdmin;
use WL\Controllers\Repository;
use WL\Modules\Location\Models\Country;

use WL\Modules\Location\Repositories\LocationRepositoryInterface;

class CountriesController extends ControllerAdmin
{
    use Repository;

    private $repository;

    public function __construct(LocationRepositoryInterface $repository)
    {
        parent::__construct();

        $this->repository = $repository;
        $this->setRepository($repository);
    }

    /**
     * Display a listing of the resource.
     * GET /admin/countries
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($name = $request->get('name')) {
            $countries = $this->repository->getList(['name:like' => "%{$name}%"]);
        } else {
            $countries = $this->repository->getList();
        }

        if ($request->ajax()) {
            return Response::json(
                $countries->map(function ($country) {
                    return ['label' => $country->name, 'id' => $country->id];
                })->toArray()
            );
        }

        return view('admin.countries.index', compact('countries'));
    }

    /**
     * Show the form for editing the specified resource.
     * GET /admin/countries/{id}/edit
     *
     * @param Country $country
     * @return Response
     * @internal param Country $country
     * @internal param int $id
     */
    public function edit(Country $country)
    {
        return view('admin.countries.save', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     * PUT /admin/countries/{id}
     *
     * @param StoreRequest $request
     * @param Country $country
     * @internal param int $id
     * @return Response
     */
    public function update(StoreRequest $request, Country $country)
    {
        try {
            DB::transaction(function () use ($request, $country) {
                $country->setFeeder($request);
                $country->update($request->all());
                $country->fill($request->fields());
                $country->saveOrFail();
            });
        } catch (ValidationException $e) {
            return Redirect::back()
                ->withErrors($e->getErrors())
                ->withInput();
        }

        return Redirect::route('admin.countries.edit', ['id' => $country->id])
            ->with('success', __('Successfully edited location'));
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /admin/countries/{id}
     *
     * @param Country $country
     * @internal param int $id
     * @return Response
     */
    public function destroy(Country $country)
    {
        try {
            $this->repository->destroy($country->id);
            Session::flash('success', 'You have successfully deleted the country');

        } catch (ModelNotFoundException $e) {
            Session::flash('error', 'The country with id ' . $country->id . ' cannot be found');
        }

        return Redirect::route('admin.countries.index');
    }

}

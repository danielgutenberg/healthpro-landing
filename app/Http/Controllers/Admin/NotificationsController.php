<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\AdminRequest;
use Carbon\Carbon;
use WL\Controllers\ControllerAdmin;
use WL\Modules\Notification\Facades\NotificationService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Role\Models\Role;

class NotificationsController extends ControllerAdmin
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('admin.notifications.index', ['roles' => Role::all()]);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  AdminRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(AdminRequest $request)
	{
		$title = $request->get('title');
		$description = $request->get('description');
		$senderProfileId = ProfileService::getCurrentProfileId();
		$roleId = Role::find($request->get('role_id'))->id;

		$result = NotificationService::broadcastNotificationByRole($senderProfileId, $title, $description, $roleId, Carbon::now()->addDays(60));
		
		return view('admin.notifications.index', ['roles' => Role::all(), 'result' => $result]);
	}


}

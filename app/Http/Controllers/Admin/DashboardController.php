<?php namespace App\Http\Controllers\Admin;

use WL\Controllers\ControllerAdmin;

class DashboardController extends ControllerAdmin
{
	/**
	* Admin dashboard
	*
	*/

	public function getIndex()
	{
		return view('admin/dashboard');
	}

}
<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Certification\CouponRequest;
use Illuminate\Http\Request;
use WL\Controllers\ControllerAdmin;
use WL\Modules\Comment\Populators\CouponPopulator;
use WL\Modules\Coupons\Facades\CouponService;
use WL\Modules\Coupons\Models\Coupon;
use WL\Modules\Coupons\ValueObjects\CreateClientCouponRequest;
use WL\Modules\Coupons\ValueObjects\CreateProviderCouponRequest;
use WL\Modules\Order\Models\Order;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Provider\Models\ProviderPlan;

class CouponsController extends ControllerAdmin
{
	public function index()
	{
		$coupons = Coupon::whereNotIn('applyTo', [ModelProfile::CLIENT_PROFILE_TYPE, ModelProfile::PROVIDER_PROFILE_TYPE])->get();

		$coupons = $coupons->map(function($coupon) {
			if($coupon->amountType == Coupon::AMOUNT_TYPE_FIXED) {
				$coupon->amount = '$' . $coupon->amount;
			} else {
				$coupon->amount = $coupon->amountType == Coupon::AMOUNT_TYPE_QUANTITY ? $coupon->amount : $coupon->amount . '%';
			}
			return $coupon;
		});

		$coupons = $coupons->groupBy('available_for');
        $data = array_merge([
            ModelProfile::CLIENT_PROFILE_TYPE => [],
            ModelProfile::PROVIDER_PROFILE_TYPE => [],
        ], $coupons->all());

		return view('admin.coupons.index', $data);
	}

	public function edit($couponId)
	{
		$coupon = CouponService::getCoupon($couponId);
		$populator = new CouponPopulator();
		$populator->setModel($coupon)->populate();

		return view('admin.coupons.save', [
		    'coupon' => $coupon,
            'amountTypes' => $this->getAmountTypes()
        ]);
	}

	public function create(Request $request)
	{
	    $for = $request->get('for', ModelProfile::CLIENT_PROFILE_TYPE);
		return view('admin.coupons.save', [
		    'amountTypes' => $this->getAmountTypes($for),
            'for' => $for,
        ]);
	}

    private function getAmountTypes($for=ModelProfile::CLIENT_PROFILE_TYPE)
    {
        $types =[
			ModelProfile::CLIENT_PROFILE_TYPE => [
            	Coupon::AMOUNT_TYPE_FIXED => 'Fixed',
            	Coupon::AMOUNT_TYPE_PERCENT => 'Percent',
        	],
			ModelProfile::PROVIDER_PROFILE_TYPE => [
				Coupon::AMOUNT_TYPE_FIXED => 'Price',
				Coupon::AMOUNT_TYPE_QUANTITY => 'Months',
			],
		];

        return $types[$for];
	}

	public function store(CouponRequest $request)
	{
		$name = $request->input('name');
		$description = $request->input('description');
		$amount = $request->input('amount', 0);
		$validFrom = $request->input('valid_from');
		$validUntil = $request->input('valid_until');
        $amountType = $request->input('amount_type', Coupon::AMOUNT_TYPE_FIXED);
        $for = $request->input('availableFor', ModelProfile::CLIENT_PROFILE_TYPE);
        $applyTo = $amountType === Coupon::AMOUNT_TYPE_QUANTITY ? ProviderPlan::class : Order::class;
        $issuedBy = $request->input('type');

        if($for === ModelProfile::CLIENT_PROFILE_TYPE) {
            $createCoupon = new CreateClientCouponRequest($name, $description, $amount, $amountType, $issuedBy, $validFrom, $validUntil);
		} else {
            $createCoupon = new CreateProviderCouponRequest($name, $description, $amount, $amountType, $applyTo, $validFrom, $validUntil);
        }
		CouponService::createCoupon($createCoupon);

		return redirect(route('admin.coupons.index'));
	}

	public function destroy(Request $request)
	{
		CouponService::removeById($request->input('id'));

		return redirect(route('admin.coupons.index'));
	}
}

<?php
namespace App\Http\Controllers\Admin;

use WL\Controllers\ControllerAdmin;
use WL\Settings\Renderer\Table as TableRenderer;
use Illuminate\Http\Request;
use Former;
use WL\Settings\Facades\Setting;
use Session;

class OptionsController extends ControllerAdmin
{
	/**
	 * Display a listing of options
	 *
	 * @param $group
	 * @return Response
	 */
	public function index( $group )
	{
		$settings = Setting::group( $group );

		Former::populate( array(
			'options' => (object)Setting::all(),
		) );

	    $renderer = new TableRenderer( $settings, array(
		    'action'    => route(
			    'options-save',
			    array(
				    'group' => $group,
			    )
		    ),
		    'reset'     => route(
			    'options-reset',
			    array(
				    'group' => $group,
			    )
		    )
	    ) );

		$pageTitle = isset($settings['title']) ? $settings['title'] : null;

		// Show the page
		return view('admin/options/index', compact('pageTitle', 'renderer'));
	}

	public function save( Request $request, $group )
	{
		$options = $request->get('options');

		foreach ($options as $name => $value) {
			Setting::set( $name, $value );
		}

		Session::flash('success', trans('admin/settings/general.message.success'));

		return redirect(route(
			'options-index',
			array(
				'group'  => $group,
			)
		));
	}

	public function reset( $group )
	{
		Setting::reset( $group );

		Session::flash('info', trans('admin/settings/general.message.reset'));

		return redirect(route(
			'options-index',
			array(
				'group'  => $group,
			)
		));
	}
}

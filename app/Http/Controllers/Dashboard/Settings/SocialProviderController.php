<?php
namespace App\Http\Controllers\Dashboard\Settings;

use Redirect;
use Sentinel;
use SentinelSocial;
use WL\Controllers\ControllerDashboard;
use Cartalyst\Sentinel\Addons\Social\AccessMissingException;
use InvalidArgumentException;

class SocialProviderController extends ControllerDashboard
{

	public function getIndex()
	{
		return $this->render('dashboard.settings.social.index');
	}

	public function getDisconnect( $provider )
	{
		try {
			$this->checkProvider($provider);

			Sentinel::check()->disconnect( $provider );

			return Redirect::route( 'dashboard-profile' )
				->with('success', __('Successfully removed a connection'));

		} catch ( InvalidArgumentException $e ) {
			return $this->redirectWithError( $e->getMessage() );
		}
	}

	public function getConnect( $provider )
	{
		try {
			$this->checkProvider($provider);

			return Redirect::to(
				SentinelSocial::getAuthorizationUrl(
					$provider,
					route( 'dashboard-settings-social-authenticate', ['provider' => $provider] )
				)
			);

		} catch (InvalidArgumentException $e) {
			return $this->redirectWithError( $e->getMessage() );
		}
	}

	public function getAuthenticate( $provider )
	{
		try {
			$this->checkProvider($provider);

			SentinelSocial::authenticate( $provider, route( 'dashboard-settings-social-authenticate', ['provider' => $provider] ), function($link, $provider, $token, $slug) {
				// do something
			});

			return Redirect::route( 'dashboard-profile' )
				->with('success', __('Successfully added new connection'));

		} catch (AccessMissingException $e) {
			return $this->redirectWithError( $e->getMessage() );

		} catch (InvalidArgumentException $e) {
			return $this->redirectWithError( $e->getMessage() );
		}
	}

	/**
	 * @param $provider
	 * @return $this
	 */
	protected function checkProvider($provider)
	{
		$connections = SentinelSocial::getConnections();

		if (!in_array( $provider, array_keys( $connections ) )) {
			throw new InvalidArgumentException('Provider cannot be found');
		}

		return $this;
	}

	/**
	 * @param $error
	 * @return mixed
	 */
	protected function redirectWithError($error)
	{
		return Redirect::route( 'dashboard-profile' )->with( 'error', $error );
	}
}

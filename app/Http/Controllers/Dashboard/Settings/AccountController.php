<?php
namespace App\Http\Controllers\Dashboard\Settings;

use Illuminate\Foundation\Bus\DispatchesJobs;
use WL\Controllers\ControllerDashboard;
use WL\Modules\User\Commands\LoadCurrentUserCommand;

class AccountController extends ControllerDashboard
{
	use DispatchesJobs;

	public function getIndex()
	{
		list($user, $manualPasswordRequired) = $this->dispatch(new LoadCurrentUserCommand());

		$email = $user->email;

		return view('dashboard.settings.account.index', compact('email', 'manualPasswordRequired'));
	}
}

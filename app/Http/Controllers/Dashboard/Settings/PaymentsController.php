<?php
namespace App\Http\Controllers\Dashboard\Settings;

use WL\Controllers\ControllerDashboard;
use WL\Modules\Payment\Paypal\RestApiGateway;

class PaymentsController extends ControllerDashboard
{
	public function getIndex(RestApiGateway $provider)
	{
		$profileType = $this->currentProfile->type;

		$stripeUrl = 'https://connect.stripe.com/oauth/authorize?response_type=code&client_id='
			. config('stripe')['client_id'] . '&scope=read_write&merchant=acct_15WQ7XLuV511TJOD'
			. '&redirect_uri=' . route('stripe.authorize');

		$paypalUrl = $provider->getPaypalLoginUrl();

		return view('dashboard.settings.payments.index', compact('profileType', 'stripeUrl', 'paypalUrl'));
	}
}

<?php
namespace App\Http\Controllers\Dashboard\Settings;

use WL\Controllers\ControllerDashboard;

class CalendarController extends ControllerDashboard
{
	public function getIndex()
	{
		$profileType = $this->currentProfile->type;

		return view('dashboard.settings.calendar.index', compact('profileType'));
	}
}

<?php
namespace App\Http\Controllers\Dashboard\Settings;

use App\Http\Requests;
use App\Http\Requests\Dashboard\Settings\UserProfile\SaveNewPhoneRequest;
use App\Http\Requests\Dashboard\Settings\UserProfile\SaveSettingsRequest;
use App\Http\Requests\Dashboard\Settings\UserProfile\UploadPhotoRequest;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use WL\Controllers\ControllerDashboard;
use WL\Modules\Location\Repositories\LocationRepository;
use WL\Modules\Phone\Exceptions\PhoneException;
use WL\Modules\User\Populators\UserProfilePopulator;
use WL\Modules\User\Exceptions\InvalidPhoneException;
use WL\Modules\User\Repositories\UserRepository;
use Exception;
use WL\Modules\Location\Models\Phone;
use WL\Modules\Attachment\Facades\AttachmentService;

class UserProfileController extends ControllerDashboard
{
	/**
	 * @param UserProfilePopulator $populator
	 * @return \WL\Controllers\Response
	 */
	public function getIndex(UserProfilePopulator $populator)
	{
		$populator->setModel(Sentinel::check())->populate();

		return $this->render('dashboard.settings.user-profile.index');
	}

	/**
	 * @param UploadPhotoRequest $request
	 * @return mixed
	 */
	public function postUploadPhoto(UploadPhotoRequest $request)
	{
		try {
			AttachmentService::uploadImage('avatar', Sentinel::check(), $request->avatar());

			return Redirect::route('dashboard-settings-user-profile')
				->with('success', __('Photo successfully uploaded.'));

		} catch (Exception $e) {
			return Redirect::route('dashboard-settings-user-profile')
				->with('error', $e->getMessage());
		}
	}

	public function getDeletePhoto()
	{
		if ($avatar = AttachmentService::findFile('avatar', Sentinel::check())) {
			AttachmentService::removeById($avatar->id);
		}

		return Redirect::route('dashboard-settings-user-profile')
			->with('success', __('Photo successfully removed.'));
	}


	public function postSaveSettings(SaveSettingsRequest $request, UserRepository $repository)
	{
		// the repo should be added to profile,
		// but since the profiles are not ready,
		// I've added it to User model for now.
		$repository->updateMeta(Sentinel::check(), $request->fields());

		return Redirect::route('dashboard-settings-user-profile')
			->with('success', __('Settings successfully saved.'));
	}

	public function postSaveNewPhone(SaveNewPhoneRequest $request, UserRepository $repository, LocationRepository $countryRepository)
	{
		try {
			$repository->addPhone(Sentinel::check(), [
				'locNumber'	=> $request->phone(),
				'code'		=> $countryRepository->getCallingCode($request->country()),
			]);
		} catch (InvalidPhoneException $e) {
			return Redirect::route('dashboard-settings-user-profile')
				->with('error', $e->getMessage());

		} catch (PhoneException $e) {
			return Redirect::route('dashboard-settings-user-profile')
				->with('error', $e->getMessage());
		}


		return Redirect::route('dashboard-settings-user-profile')
			->with('success', __('Settings successfully saved.'));
	}

	public function getVerifyPhoneRequest($phoneId, $verifyBy, UserRepository $repository)
	{
		$phone = $repository->getPhoneById(Sentinel::check(), $phoneId);
		if (!$phone) return Redirect::back()->with('error', __("Phone not found"));

		return view('dashboard.settings.user-profile.phone-verify-step1', compact('phone','verifyBy'));
	}

	public function postVerifyPhoneRequest($phoneId, Request $request, UserRepository $repository)
	{
		$phone = $repository->getPhoneById(Sentinel::check(), $phoneId);
		if (!$phone) return Redirect::back()->with('error', __("Phone not found"));

		$phoneUseCount = Phone::where('code',$phone->code)->where('locNumber',$phone->locNumber)->whereNotNull('verified_at')->count();
		if ($phoneUseCount >= Phone::MAX_VERIFICATIONS) {
			return Redirect::back()->with('error', __("This phone number has already been used too many times for verification."));
		}

		$isSuccess = $phone->sendVerifyCode($request->input('verify_by'));
		if (!$isSuccess) {
			return Redirect::back()->with('error', $phone->getErrors());
		}
		return Redirect::route('dashboard-settings-user-profile-verify-phone-step2',[$phoneId]);
	}

	public function getVerifyPhoneAccept($phoneId, UserRepository $repository)
	{
		$phone = $repository->getPhoneById(Sentinel::check(), $phoneId);
		if (!$phone) return Redirect::back()->with('error', __("Phone not found"));

		return view('dashboard.settings.user-profile.phone-verify-step2', compact('phone'));
	}

	public function postVerifyPhoneAccept($phoneId, Request $request, UserRepository $repository)
	{
		$phone = $repository->getPhoneById(Sentinel::check(), $phoneId);
		if (!$phone) return Redirect::back()->with('error', __("Phone not found"));

		$isValid = $phone->applyVerifyCode($request->input('code'));
		if (!$isValid) {
			return Redirect::route('dashboard-settings-user-profile-verify-phone-step2',[$phoneId])->withError("Incorect verification code");
		}
		return Redirect::route('dashboard-settings-user-profile',[$phoneId])->withSuccess("Success Phone veriffication");
	}
}

<?php namespace App\Http\Controllers\Dashboard\Settings;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Session;
use WL\Controllers\ControllerDashboard;
use WL\Modules\Phone\Commands\GetProfilePhonesCommand;
use WL\Modules\Profile\Commands\GetProviderNotifications;
use WL\Modules\Profile\Commands\UpdateProfileProviderDetailsCommand;
use WL\Modules\Profile\Services\ModelProfileService;

class NotificationsController extends ControllerDashboard
{

	use DispatchesJobs;

	/**
	 * @param ModelProfileService $profileService
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function index(Request $request, ModelProfileService $profileService)
	{
		$profile = $profileService->getCurrentProfile();

		if (!$profile->assertType('provider'))
			return view('dashboard.settings.notifications.index', compact('profile'));

		$phones = $this->dispatch(
			new GetProfilePhonesCommand(
				$profile
			)
		);

		$providerMeta = $this->dispatch(
			new GetProviderNotifications([
				'profile' => $profile
			])
		);

		$emailNotifications = $providerMeta['email'];
		$smsNotifications = $providerMeta['sms'];

		if ($request->old()) {
			Session::reflash();
			$emailNotifications = array_merge($emailNotifications, $request->old('notifications.email'));
			$smsNotifications = array_merge($emailNotifications, $request->old('notifications.sms'));
		}
		$customPhones = $request->old('phones', []);

		return view('dashboard.settings.notifications.index', compact('profile','phones', 'customPhones', 'emailNotifications', 'smsNotifications'));
	}

	/**
	 * Save notification page ..
	 *
	 * @param Request $request
	 * @param ModelProfileService $profileService
	 * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function save(Request $request, ModelProfileService $profileService)
	{
		$post = $request->all();
		$post['profile'] = $profileService->getCurrentProfile();

		if (isset($post['phones'])) {
			$post['phones'] = array_filter($post['phones'], function ($item) {
				return (!empty($item['prefix']) || !empty($item['number']));
			});
		}

		try {
			$this->dispatch(
				new UpdateProfileProviderDetailsCommand($post)
			);
		} catch (ValidationException $e) {
			return redirect(route('dashboard-notifications'))
				->withInput($request->all())
				->withErrors($e->errors());
		}

		return back();
	}

}

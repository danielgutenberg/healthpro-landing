<?php namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Request;
use Former;
use Redirect;
use Session;
use Validator;
use WL\Controllers\ControllerDashboard;
use WL\Modules\Profile\Commands\CreateProfileCommand;
use WL\Modules\Profile\Models\ModelProfile;

class ProfilesController extends ControllerDashboard
{
	public function getRegister($type = ModelProfile::PROVIDER_PROFILE_TYPE)
	{
		# allow only provider and client profiles to be registered
		if (!in_array($type, [ModelProfile::PROVIDER_PROFILE_TYPE, ModelProfile::CLIENT_PROFILE_TYPE])) {
			abort(404);
		}

		if ($this->currentProfile->type == $type) {
			return redirect(route('dashboard-home'));
		}

		Former::populate([
			'first_name' => $this->currentProfile->first_name,
			'last_name' => $this->currentProfile->last_name,
		]);

		return $this->render('dashboard.profiles.register', [
			'type' => $type,
			'email' => $this->user->email,
		]);
	}

	public function postRegister($type = ModelProfile::PROVIDER_PROFILE_TYPE, Request $request)
	{
		# allow only provider and client profiles to be registered
		if (!in_array($type, [ModelProfile::PROVIDER_PROFILE_TYPE, ModelProfile::CLIENT_PROFILE_TYPE])) {
			abort(404);
		}

		if ($this->currentProfile->type == $type) {
			return redirect(route('dashboard-home'));
		}

		$firstName = $request->get('first_name');
		$lastName = $request->get('last_name');

		$validator = Validator::make(
			[
				'first_name' => $firstName,
				'last_name' => $lastName,
			],
			[
				'first_name' => 'required|max:100',
				'last_name' => 'required|max:100',
			],
			[
				'first_name.required' => 'Please add your first name',
				'last_name.required' => 'Please add your last name',
			]
		);

		if ($validator->fails()) {
			return Redirect::back()
				->withErrors($validator)
				->withInput();
		}

		# pass the first and last name from the current client
		$inpData = array_merge($request->all(), [
			'profile_type' => $type,
			'first_name' => $firstName,
			'last_name' => $lastName,
			'is_api' => false,
		]);

		$result = $this->runCommandWithErrorHandle(new CreateProfileCommand($inpData), $request);

		if (!isset($result->profile_id) || !$result->profile_id) {
			return Redirect::back()
				->withErrors('Something went wrong. Please reload the page and try again.')
				->withInput();
		}

		Session::flash('success', 'Profile successfuly created');
		return redirect(route('home'));
	}
}

<?php
namespace App\Http\Controllers\Dashboard;

use WL\Controllers\ControllerDashboard;

class MyGiftCertificatesController extends ControllerDashboard
{
	public function show()
	{
		return view('dashboard.my-gift-certificates.show');
	}
}

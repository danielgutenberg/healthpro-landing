<?php namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Request;
use WL\Controllers\ControllerDashboard;
use Carbon\Carbon;
use WL\Modules\OAuth\Facades\OAuthService;
use WL\Modules\OAuth\Models\OauthToken;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\ProfileCalendar\Facades\ProfileCalendar;
use WL\Modules\ProfileCalendar\Models\ProfileCalendar as ProfileCalendarModel;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\ProfileCalendar\Commands\GetProviderLastSyncedCommand;

class ScheduleController extends ControllerDashboard
{
	public function show(Request $request)
	{
		$profile = $this->currentProfile;
		$date = $request->get('date');
		if ($date) {
			try {
				$date = Carbon::parse($date)->format('Y-m-d\TH:i');
			} catch (\Exception $ex) {
				$date = null;
			}
		}

		$scope = OauthToken::SCOPE_CALENDAR;
		$providers = OAuthService::getAllProviders($scope);

		$lastSynced = false;
		if($profile->type === ModelProfile::PROVIDER_PROFILE_TYPE) {
			$lastSynced = $this->dispatch(new GetProviderLastSyncedCommand(['profile_id' => $profile->id]));
		}

		return $this->render('dashboard.schedule.show',compact('profile', 'date', 'lastSynced', 'providers', 'scope'));
	}
}

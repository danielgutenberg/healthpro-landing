<?php
namespace App\Http\Controllers\Dashboard;

use WL\Controllers\ControllerDashboard;
use WL\Modules\Profile\Models\ModelProfile;

class MyLocationsController extends ControllerDashboard
{
	public function show()
	{
		if ($this->currentProfile->type != ModelProfile::CLIENT_PROFILE_TYPE) {
			return redirect(route('dashboard-home'));
		}

		return view('dashboard.my-locations.show');
	}
}

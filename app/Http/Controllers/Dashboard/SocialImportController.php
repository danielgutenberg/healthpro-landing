<?php namespace App\Http\Controllers\Dashboard;

use Cartalyst\Sentinel\Addons\Social\AccessMissingException;
use Illuminate\Http\Request;
use Redirect;
use SentinelSocial;
use Session;
use WL\Controllers\ControllerDashboard;
use WL\Modules\Country\Services\CountryService;
use WL\Modules\Profile\Commands\UpdateProfileCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Packages\SentinelSocial\SocialExportableInterface;

class SocialImportController extends ControllerDashboard
{
	use \Illuminate\Foundation\Bus\DispatchesJobs;
	use \Illuminate\Foundation\Validation\ValidatesRequests;


	public function getAuthorize($socialProviderSlug)
	{
		$url = SentinelSocial::getAuthorizationUrl($socialProviderSlug, route('dashboard-import-authenticate', [$socialProviderSlug]));
		return Redirect::to($url);
	}

	public function getAuthenticate($socialProviderSlug)
	{
		try {
			$data = [];
			SentinelSocial::authenticate($socialProviderSlug, route('dashboard-import-authenticate', [$socialProviderSlug]), function ($link, SocialExportableInterface $provider, $token, $slug) use (&$data) {
				$data = $provider->requestFormatedFields($token, [
					'location',
					'languages',
					'certifications',
					'educations',
					'three-current-positions',
					'date-of-birth',
					'skills',
					'interests',
					'associations',
				]);
			});

			$profile = ProfileService::getCurrentProfile();

			// Prepare data for FORM fields, and pass to confirmation view through ->withInput($formData)
			$formData = [];
			if (!empty($data->birthday)) {
				$formData['birthday'] = (array)$data->birthday;
			}
			if (!empty($data->languages)) {
				$formData['languages'] = (array)$data->languages;
			}
			if (!empty($data->location)) {
				$formData['country_code'] = strtoupper($data->location->code);
			}
			if (!empty($data->interests)) {
				$formData['interests'] = (string)$data->interests;
			}
			if (!empty($data->skills)) {
				$formData['skills'] = (array)$data->skills;
			}

			if ($profile->type == ModelProfile::PROVIDER_PROFILE_TYPE) {
				foreach ($data->certifications as $certName) {
					$formData['certifications'][] = $certName;
				}
				foreach ($data->educations as $education) {
					$formData['educations'][] = (array)$education;
				}
			}

			return Redirect::route('dashboard-import-confirm')
				->withInput($formData);

		} catch (AccessMissingException $e) {
			return Redirect::back()
				->withErrors($e->getMessage());
		}
	}

	public function getConfirmation(Request $request)
	{
		Session::reflash();
		$data['birthday'] = $request->old('birthday');
		$data['languages'] = $request->old('languages');
		$data['country_code'] = $request->old('country_code');
		$data['interests'] = $request->old('interests');
		$data['skills'] = $request->old('skills');
		$data['certifications'] = $request->old('certifications');
		$data['educations'] = $request->old('educations');

		$data['countries'] = [];
		foreach (CountryService::loadCountries() as $c) {
			$data['countries'][$c->code] = "{$c->name}";
		}

		return view('dashboard.social-import-confirmation', $data);
	}

	public function postConfirmation(Request $request)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();

		if (!empty($inpData['educations'])) {
			#todo Education is broken, working with trait and profile not implemented. Need Education refactor
			unset($inpData['educations']);
		}

		$this->runCommandWithErrorHandle(new UpdateProfileCommand($inpData), $request);
		return Redirect::route('dashboard-home')->withSuccess(__('Profile successfully updated'));
	}
}

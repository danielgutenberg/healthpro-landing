<?php namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;
use Redirect;
use Response;
use SentinelSocial;
use WL\Controllers\ControllerDashboard;
use WL\Modules\Country\Services\CountryService;
use WL\Modules\Education\Models\Education;
use WL\Modules\Profile\Commands\DeleteFileFromProfileCommand;
use WL\Modules\Profile\Commands\GetProfileDetailsCommand;
use WL\Modules\Profile\Commands\SetCurrentProfileCommand;
use WL\Modules\Profile\Commands\UpdateProfileCommand;
use WL\Modules\Profile\Commands\UploadFileToProfileCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Transformers\ProfileEducationTransformer;
use WL\Modules\Profile\Transformers\ProfileJobTransformer;
use WL\Modules\Taxonomy\Facades\TagService;
use WL\Packages\SentinelSocial\SocialExportableInterface;

class MyProfileController extends ControllerDashboard
{

	/**
	 * Update Profile attributes
	 * @param Request $request
	 * @return array
	 */
	public function updateProfile(Request $request)
	{
		$inpData = $request->all();
		$inpData['avatar'] = $request->file('avatar');
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();

		$this->runCommandWithErrorHandle(new UpdateProfileCommand($inpData), $request);

		if ($request->ajax() || $request->wantsJson()) {
			return new JsonResponse(['success' => true]);
		}
		return back();
	}

	public function getProfileSocial(Request $request)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();

		$data = $this->runCommandWithErrorHandle(new GetProfileDetailsCommand($inpData), $request);

		//	$data['socialPagesConfig'] = ProfileService::loadSocialAccounts($inpData['profile']);

		return $this->render('dashboard.myprofile.web', $data);
	}

	public function getProfileSuitability(Request $request)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();

		$data = $this->runCommandWithErrorHandle(new GetProfileDetailsCommand($inpData), $request);

		$data['concerns'] = TagService::getTaxonomyTagsBySlug('concerns-conditions', false, null);

		return $this->render('dashboard.myprofile.suitability', $data);
	}

	public function getProfileCredentials(Request $request)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();

		$data = $this->runCommandWithErrorHandle(new GetProfileDetailsCommand($inpData), $request);
		$jobsTransformer = new ProfileJobTransformer();
		$data['profile']->jobs = $jobsTransformer->transformCollection($data['profile']->jobs);

		$educationsTransformer = new ProfileEducationTransformer();
		$data['profile']->educations = $educationsTransformer->transformCollection($data['profile']->educations);

		$data['availableEducations'] = Education::all();

		return $this->render('dashboard.myprofile.credentials', $data);
	}

	public function getProfileMedia(Request $request)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();

		$data = $this->runCommandWithErrorHandle(new GetProfileDetailsCommand($inpData), $request);

		return $this->render('dashboard.myprofile.media', $data);
	}

	public function getProfile(Request $request, $id)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = $id;

		$data = $this->runCommandWithErrorHandle(new GetProfileDetailsCommand($inpData), $request);

		$data['countries'] = [];
		foreach (CountryService::loadCountries() as $c) {
			$data['countries'][$c->code] = "{$c->name}";
		}

		$data['autoAction'] = $request->old('_autoAction_');

		return $this->render('dashboard.myprofile.aboutme', $data);
	}

	public function getSocialAvatarsAuthorize($socialProviderSlug)
	{
		$callback = route('dashboard-myprofile-socialavatars-authenticate', [$socialProviderSlug]);
		$url = SentinelSocial::getAuthorizationUrl($socialProviderSlug, $callback);
		return Redirect::to($url);
	}

	public function getSocialAvatarsAuthenticate($socialProviderSlug)
	{
		try {
			$callback = route('dashboard-myprofile-socialavatars-authenticate', [$socialProviderSlug]);
			SentinelSocial::authenticate($socialProviderSlug, $callback, function ($link, SocialExportableInterface $provider, $token, $slug) {

			});
			return Redirect::route('dashboard-myprofile-aboutme')->withInput(['_autoAction_' => 'link-social-avatar-import']);
		} catch (\Cartalyst\Sentinel\Addons\Social\AccessMissingException $e) {
			return Redirect::route('dashboard-myprofile-aboutme')->with('error', $e->getMessage());
		}
	}

	public function getSocialAvatars()
	{
		$pictureUrls = [];

		if ($imgUrl = $this->user->getGravatarUrl()) {
			$pictureUrls['gravatar'][] = $imgUrl;
		}

		$queryFields = [
			'linkedin' => ['picture-urls::(original)'],
			'facebook' => ['picture.type(large){url}'],
			//'google'   => ['image'],
		];

		foreach ($this->user->getActiveSocials() as $socialLink) {
			if (!isset($queryFields[$socialLink->provider])) {
				continue;
			}
			$provider = SentinelSocial::make($socialLink->provider, \Route::currentRouteName());
			$data = $provider->requestFormatedFields($socialLink->oauth2_access_token, $queryFields[$socialLink->provider]);

			$pictureUrls[$socialLink->provider] = $data->pictureUrls;
		}
		if (isset($pictureUrls['google'])) {
			$pictureUrls['google'] = preg_replace("/\bsz=\d+/", 'sz=300', $pictureUrls['google']);
		}

		$unlinkedSocials = array_diff(array_keys($queryFields), array_keys($pictureUrls));

		return $this->render('dashboard.myprofile.socialavatar', compact('pictureUrls', 'unlinkedSocials'));
	}

	public function index(Request $request)
	{
		return $this->getProfile($request, ProfileService::getCurrentProfileId());
	}

	public function uploadFile(Request $request)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();
		$inpData['files'] = $request->file('files');

		$data = $this->runCommandWithErrorHandle(new UploadFileToProfileCommand($inpData), $request);

		return Response::json($data);
	}

	public function deleteFile(Request $request)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();

		$data = $this->runCommandWithErrorHandle(new DeleteFileFromProfileCommand($inpData), $request);

		return Response::json($data);
	}

	public function setCurrentProfile($profileId)
	{
		$this->dispatch(new SetCurrentProfileCommand($profileId));
		return redirect()->route('dashboard-myprofile');
	}

	public function getProfileInfo(Request $request)
	{
		$inpData = $request->all();
		$inpData['profile_id'] = ProfileService::getCurrentProfileId();

		$data = $this->runCommandWithErrorHandle(new GetProfileDetailsCommand($inpData), $request);

		$data['countries'] = [];
		foreach (CountryService::loadCountries() as $c) {
			$data['countries'][$c->code] = "{$c->name}";
		}

		$data['autoAction'] = $request->old('_autoAction_');

		return $this->render('dashboard.myprofile.personalinfo', $data);
	}
}

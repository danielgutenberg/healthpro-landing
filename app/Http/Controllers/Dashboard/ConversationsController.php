<?php namespace App\Http\Controllers\Dashboard;

use WL\Controllers\ControllerDashboard;

class ConversationsController extends ControllerDashboard
{
	public function show()
	{
		$profile = $this->currentProfile;
		return $this->render('dashboard.conversations.show',compact('profile'));
	}
}

<?php
namespace App\Http\Controllers\Dashboard;

use WL\Controllers\ControllerDashboard;
use WL\Modules\Profile\Commands\ConfirmClientInvitationExternalCommand;
use WL\Modules\Profile\Models\ModelProfile;

class MyProvidersController extends ControllerDashboard
{
	public function show()
	{
		if ($this->currentProfile->type == ModelProfile::PROVIDER_PROFILE_TYPE) {
			return redirect(route('dashboard-my-clients'));
		}
		return $this->render('dashboard.my-providers.show');
	}
}

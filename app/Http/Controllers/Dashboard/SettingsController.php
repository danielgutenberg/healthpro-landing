<?php
namespace App\Http\Controllers\Dashboard;

use Illuminate\Support\Facades\Redirect;
use WL\Controllers\ControllerDashboard;

class SettingsController extends ControllerDashboard
{
	public function getIndex()
	{
		return Redirect::route('dashboard-settings-account');
	}
}

<?php
namespace App\Http\Controllers\Dashboard;

use WL\Controllers\ControllerDashboard;

class HomeController extends ControllerDashboard
{
	public function getIndex()
	{
		return $this->render('dashboard.home.index');
	}
}

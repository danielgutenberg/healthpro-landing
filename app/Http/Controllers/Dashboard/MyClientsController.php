<?php
namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use WL\Controllers\ControllerDashboard;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;

class MyClientsController extends ControllerDashboard
{
	public function show(Request $request)
	{
		if ($this->currentProfile->type == ModelProfile::CLIENT_PROFILE_TYPE) {
			return redirect(route('dashboard-my-providers'));
		}

		$data['couponAmount'] = session('coupon_amount') ?: false;
		$data['couponType'] = session('coupon_type') ?: false;
		$data['couponValid'] = session('coupon_valid') ?: false;
		$data['popup'] = $request->session()->pull('open_popup', false);
		$data['currentProfile'] = ProfileService::getCurrentProfile();

		return $this->render('dashboard.my-clients.show', $data);
	}
}

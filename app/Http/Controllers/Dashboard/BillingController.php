<?php namespace App\Http\Controllers\Dashboard;

use WL\Controllers\ControllerDashboard;

class BillingController extends ControllerDashboard
{
	public function showInfo()
	{
		return $this->render('dashboard.billing.info');
	}
	public function showHistory()
	{
		return $this->render('dashboard.billing.history');
	}
}

<?php namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\ApiRequest;
use Carbon\Carbon;
use Dompdf\Dompdf;
use WL\Controllers\ControllerDashboard;
use WL\Modules\Appointment\Commands\GetProviderAppointmentsCommand;
use WL\Modules\Appointment\Transformers\FullAppointmentTransformer;
use WL\Modules\Appointment\Transformers\ReportFullAppointmentTransformer;

class AppointmentsController extends ControllerDashboard
{
	public function show()
	{
		$profile = $this->currentProfile;

		return $this->render('dashboard.appointments.show-' . $profile->type, compact('profile'));
	}

	public function showMyPackages()
	{
		$profile = $this->currentProfile;

		return $this->render('dashboard.appointments.show-packages-' . $profile->type, compact('profile'));
	}


	public function showMyClients()
	{
		$profile = $this->currentProfile;

		return $this->render('dashboard.appointments.clients', compact('profile'));
	}

	public function showSetup()
	{
		$profile = $this->currentProfile;

		return $this->render('dashboard.appointments.setup', compact('profile'));
	}

	public function showSuitabilityConditions()
	{
		$profile = $this->currentProfile;

		return $this->render('dashboard.appointments.suitability-conditions', compact('profile'));
	}

	public function report(ApiRequest $request)
	{
		$statuses = $request->inputArr('status', []);
		$type = $request->input('type');

		$data['provider_id'] = $this->currentProfile->id;
		$data['statuses'] = $statuses;
		$data['type'] = $type;
		$data['date'] = $request->input('date') ? new Carbon($request->input('date')) : new Carbon();

		$result = $this->dispatch(new GetProviderAppointmentsCommand($data));
		$result = (new ReportFullAppointmentTransformer())->transformItems($result);

		$domPdf = new Dompdf();

		$view = view('dashboard.schedule.pdf.report', array_merge($result, $data));

		$html = $view->render();

		$domPdf->getOptions()->set('isHtml5ParserEnabled', true);
		$domPdf->getOptions()->set('isRemoteEnabled', true);
		$domPdf->getOptions()->setChroot(app_path('public'));

		if (($request->orientation !== null) && (strtolower($request->orientation) == 'landscape')) {
			$domPdf->setPaper('A4', $request->orientation);
		}

		$domPdf->loadHtml($html);
		$domPdf->render();

		return response($domPdf->output(), 200, ['Content-Type' => 'application/pdf']);
	}
}

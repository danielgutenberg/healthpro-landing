<?php
namespace App\Http\Controllers\Dashboard;

use Carbon\Carbon;
use Illuminate\Http\Request;
use WL\Controllers\ControllerDashboard;
use WL\Helpers\PdfHelper;
use WL\Modules\Order\Commands\ProfessionalBillingInvoiceCommand;
use WL\Modules\Order\Commands\ProfileTransactionsReportCommand;
use WL\Modules\Order\Transformers\InvoiceFullTransformer;
use WL\Modules\Order\Transformers\OrderItemReportTransformer;
use WL\Modules\Payment\Facades\PaymentCreditService;


class MyTransactionsController extends ControllerDashboard
{
	public function completed()
	{
		$credit = PaymentCreditService::getCredit($this->currentProfile->id);

		return $this->render('dashboard.transactions.completed', [
			'type' => $this->currentProfile->type,
			'credit' => $credit
		]);
	}

	public function future()
	{
		$credit = PaymentCreditService::getCredit($this->currentProfile->id);

		return $this->render('dashboard.transactions.future', [
			'type' => $this->currentProfile->type,
			'credit' => $credit
		]);
	}

	public function report(Request $request)
	{
		$inputData = [
			'profile_id' => $request->profileId,
			'type' => $request->type,
			'begin' => Carbon::parse($request->begin),
			'end' => Carbon::parse($request->end),
			'print' => $request->print
		];

		if ($request->profileId == null) {
			$inputData['profile_id'] = $this->currentProfile->id;
		}

		if ($inputData['type'] === null) {
			$inputData['type'] = 'all';
		}

		$inputData['type'] = strtolower($inputData['type']);

		$result = $this->dispatch(new ProfileTransactionsReportCommand($inputData));
		$result = (new OrderItemReportTransformer())->transform($result);
		$result['type'] = $this->currentProfile->type;


		$view = view('dashboard.transactions.pdf.report', array_merge($result, $inputData));

		if ($request->view!==null && strtolower($request->view) ==='schedule') {
			$view = view('dashboard.schedule.pdf.report', array_merge($result, $inputData));
		}

        $content = PdfHelper::renderView($view, $request->orientation);
        return response($content, 200, ['Content-Type' => 'application/pdf']);
	}

    public function invoice(Request $request)
    {
        $result = $this->dispatch(new ProfessionalBillingInvoiceCommand(['order_id' => $request->orderId]));
        $result = (new InvoiceFullTransformer())->transform($result);
        $view = view('pdf.orders.invoice', $result);
        $content = PdfHelper::renderView($view, $request->orientation);
        return response($content, 200, ['Content-Type' => 'application/pdf']);
	}
}

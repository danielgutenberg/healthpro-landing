<?php namespace App\Http\Controllers\Site;

use App\Http\Controllers\Api\TaxonomyApiController;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use WL\Controllers\ControllerFront;
use WL\Modules\Blog\AttachedImageResponseTransformer;
use WL\Modules\Blog\AttachImageToBlogPostCommand;
use WL\Modules\Blog\BlogPost;
use WL\Modules\Blog\BlogServiceInterface;
use WL\Modules\Blog\CreateBlogPostCommand;
use WL\Modules\Blog\DeleteBlogPostCommand;
use WL\Modules\Blog\GetBlogPostCommand;
use WL\Modules\Blog\ListBlogPostCommand;
use WL\Modules\Blog\LoadPostsByTagCommand;
use WL\Modules\Blog\SearchBlogPostCommand;
use WL\Modules\Blog\UpdateBlogPostCommand;
use WL\Modules\Profile\Commands\GetProfileDetailsCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Taxonomy\Facades\TagService;
use WL\Modules\Taxonomy\LoadTaxonomyTagsCommand;
use WL\Settings\Facades\Setting;

/**
 * Class BlogController
 * @package App\Http\Controllers\Blog
 */
class BlogController extends ControllerFront
{

	/**
	 * @var BlogServiceInterface
	 */
	private $blogService;

	public function __construct(BlogServiceInterface $blogService)
	{

		$this->blogService = $blogService;
		$this->blogPerPage = $this->blogService->getConfiguration('perPage');
	}

	/**
	 * Admin dashboard
	 *
	 */
	public function getIndex() {
		return view('admin/dashboard');
	}

	/**
	 * Main blog page, lists posts
	 *
	 * @param Request $request
	 * @param int $page_num
	 * @return \WL\Controllers\Response
	 */
	public function index(Request $request, $page_num = 0)
	{

		list($perPage) = $this->getParams(['perPage' => $this->blogPerPage]);

		$inpData = [
			'perPage' => $perPage,
			'page' => $page_num,
			'profileId' => ProfileService::getCurrentProfileId()
		];

		$data = $this->runCommandWithErrorHandle(new ListBlogPostCommand($inpData), $request);

		$data['page'] = $page_num;
		$data['perPage'] = $perPage;
		$data['controllerMethod'] = 'blog-index-page';
		$data['tagSlug'] = !($data['posts']->isEmpty()) && isset($data['posts']->first()->category->slug) ? $data['posts']->first()->category->slug : '';
        $data['tags'] = $this->runCommandWithErrorHandle(new LoadTaxonomyTagsCommand('post-tags'), $request);

       // dd($data['tags']);
		return $this->render('site.blog.posts', $data);
	}

	/**
	 * Main blog page, lists posts
	 *
	 * @param Request $request
	 * @param int $page_num
	 * @return \WL\Controllers\Response
	 */
	public function getArchive(Request $request, $year, $month, $page_num = 0)
	{
		list($perPage) = $this->getParams(['perPage' => $this->blogPerPage]);

		$inpData = [
			'perPage' => $perPage,
			'page' => $page_num,
			'profileId' => ProfileService::getCurrentProfileId(),
			'month' => $month,
			'year' => $year
		];

		$data = $this->runCommandWithErrorHandle(new ListBlogPostCommand($inpData), $request);

		$data['page'] = $page_num;
		$data['perPage'] = $perPage;
		$data['controllerMethod'] = 'blog-index-archive';
		$data['tagSlug'] = !($data['posts']->isEmpty()) && isset($data['posts']->first()->category->slug) ? $data['posts']->first()->category->slug : '';
        $data['tagName'] = $data['archiveName'] = $this->getArchiveName($month, $year);
		$data['chosenMonth'] = $month;
		$data['chosenYear'] = $year;
        $data['tags'] = $this->runCommandWithErrorHandle(new LoadTaxonomyTagsCommand('post-tags'), $request);
		return $this->render('site.blog.posts', $data);
	}

    protected function getArchiveName($month, $year){
	    return $month . ' ' . $year;
    }

	/**
	 * Main blog feed, lists posts
	 *
	 * @param Request $request
	 * @param int $page_num
	 * @return \WL\Controllers\Response
	 */
	public function feed(Request $request, $page_num = null)
	{
		//If $page_num was passed, it will be a 0 string.
		if($page_num === "0")
			return Redirect::route('blog-feed');

		if($page_num === null)
			$page_num = 0;

		list($perPage) = $this->getParams(['perPage' => $this->blogPerPage]);

		$inpData = [
			'perPage' => $perPage,
			'page' => $page_num,
			'profileId' => ProfileService::getCurrentProfileId()
		];

		$data = $this->runCommandWithErrorHandle(new ListBlogPostCommand($inpData), $request);

		$data['page'] = $page_num;
		$data['perPage'] = $perPage;
		$data['controllerMethod'] = 'blog-feed';

		if($data['posts']->count() == 0)
			App::abort(404);

		return response()
				->view('site.blog.feed', $data)
				->header('Content-Type', 'application/rss+xml');
	}

	/**
	 * Lists post by category
	 *
	 * @param Request $request
	 * @param $slug
	 * @param int $page_num
	 * @return \WL\Controllers\Response
	 */
	public function categoryPosts(Request $request, $slug, $page_num = 0)
	{
		list($perPage) = $this->getParams(['perPage' => $this->blogPerPage]);

		$inpData = [
			'perPage' => $perPage,
			'page' => $page_num,
			'slug' => $slug,
			'taxonomyName' => BlogPost::$TAXONOMY_CATEGORIES,
			'profileId' => ProfileService::getCurrentProfileId()
		];

		$data = $this->runCommandWithErrorHandle(new LoadPostsByTagCommand($inpData), $request);

		if (!$data['success'])
			App::abort(404);

		$data['page'] = $page_num;
		$data['perPage'] = $perPage;
		$data['tagSlug'] = $slug;
		$data['controllerMethod'] = 'blog-category-page';
        $data['tags'] = $this->runCommandWithErrorHandle(new LoadTaxonomyTagsCommand('post-tags'), $request);
		return $this->render('site.blog.posts', $data);
	}

	/**
	 * Lists post by tag
	 *
	 * @param Request $request
	 * @param $slug
	 * @param int $page_num
	 * @return \WL\Controllers\Response
	 */
	public function tagPosts(Request $request, $slug, $page_num = 0)
	{
		list($perPage) = $this->getParams(['perPage' => $this->blogPerPage]);
		$inpData = [
			'perPage' => $perPage,
			'page' => $page_num,
			'slug' => $slug,
			'taxonomyName' => BlogPost::$TAXONOMY_POST_TAGS,
			'profileId' => ProfileService::getCurrentProfileId()
		];

		$data = $this->runCommandWithErrorHandle(new LoadPostsByTagCommand($inpData), $request);

		if (!$data['success'])
			App::abort(404);

		$data['page'] = $page_num;
		$data['perPage'] = $perPage;
		$data['tagSlug'] = $slug;
		$data['controllerMethod'] = 'blog-category-page';
        $data['tags'] = $this->runCommandWithErrorHandle(new LoadTaxonomyTagsCommand('post-tags'), $request);
		return $this->render('site.blog.posts', $data);
	}


	/**
	 * Lists post by search term
	 * @param Request $request
	 * @param int $page_num
	 * @return \WL\Controllers\Response
	 */
	public function search(Request $request, $page_num = 0)
	{
		list($perPage, $term) = $this->getParams(['perPage' => $this->blogPerPage, 'term' => '']);
		$inpData = [
			'perPage' => $perPage,
			'page' => $page_num,
			'profileId' => ProfileService::getCurrentProfileId(),
			'term' => $term
		];

		$data = $this->runCommandWithErrorHandle(new SearchBlogPostCommand($inpData), $request);

		$data['page'] = $page_num;
		$data['term'] = $term;
		$data['perPage'] = $perPage;
		$data['controllerMethod'] = 'blog-search-page';
		$data['tagSlug'] = !($data['posts']->isEmpty()) ? $data['posts']->first()->category->slug : '';
        $data['tags'] = $this->runCommandWithErrorHandle(new LoadTaxonomyTagsCommand('post-tags'), $request);
		return $this->render('site.blog.posts', $data);
	}

	/**
	 * Show single post
	 *
	 * @param Request $request
	 * @param $category
	 * @param $slug
	 * @return \WL\Controllers\Response
	 */
	public function blogpost(Request $request, $category, $slug, $editMode=false)
	{
		$data = $this->runCommandWithErrorHandle(new GetBlogPostCommand(['slug' => $slug, 'profileId' => ProfileService::getCurrentProfileId(), 'categorySlug' => $category]), $request);
		try {
			$author = ProfileService::getProfileById($data['post']->author_id);
			$details = ProfileService::loadProfileBasicDetailsModel($author);
			$data['professional_name'] = $details->full_name;
		} catch (\Exception $e) {
			$data['professional_name'] = '';
		}
		$data['isEditMode'] = (!empty($data['editor']) && $editMode == true);
		if (empty($data['editor']) && $editMode == true) {
			return redirect(route("blog-get", [$category, $slug]));
		}

		if (isset($data['post']->author_id)) {
			$serviceTags = ProviderService::getServicesTags($data['post']->author_id);
			$entityType = \WL\Modules\Provider\Models\ProviderService::class;
			$serviceTags = TagService::condenseTags($serviceTags, $entityType);
			$services = [];
			foreach ($serviceTags as $tag) {
				$services[] = $tag->name;
			}

			$data['post']->author->services = $services;
		}

		if (isset($data['rightCategory'])) {
			return Redirect::route('blog-get', ['category' => $data['rightCategory'], 'slug' => $data['post']->slug]);
		}

		if ($data['post'] == null) {
			App::abort(404);
		}

		$data['metaTitlePrefix'] = Setting::get('blog_post_title_prefix');
		$data['metaTitleSuffix'] = Setting::get('blog_post_title_suffix');
		$data['metaTitle']       = isset($data['post']->meta['title'])      ? $data['post']->meta['title']      : $data['post']->title;
		$data['metaDescription'] = isset($data['post']->meta['description'])? $data['post']->meta['description']: $data['post']->excerptPlain(30);
		$data['metaKeywords']    = isset($data['post']->meta['keywords'])   ? $data['post']->meta['keywords']   : implode(',',array_map(function($item){return $item['name'];},$data['post']->tags->toArray())). ($data['post']->category?','.$data['post']->category->name:'');
        $data['tags'] = $this->runCommandWithErrorHandle(new LoadTaxonomyTagsCommand('post-tags'), $request);
		return $this->render('site.blog.post', $data);
	}

	/**
	 * Show single post in edit mode
	 *
	 * @param Request $request
	 * @param $category
	 * @param $slug
	 * @return \WL\Controllers\Response
	 */
	public function blogPostEdit(Request $request, $category, $slug)
	{
		return $this->blogpost($request, $category, $slug, true);
	}
	/**
	 * Updates post
	 *
	 * @param Request $request
	 * @param $slug
	 * @return mixed
	 */
	public function updateblogpost(Request $request, $slug)
	{
		$inpData = Input::all();

		if (Input::has('cover_image')) {
			$inpData['cover_image'] = Input::get('cover_image');
		}
		$inpData['author_id'] = ProfileService::getCurrentProfileId();
		$inpData['slug'] = $slug;

		$data = $this->runCommandWithErrorHandle(new UpdateBlogPostCommand($inpData), $request);


		if ($data['post'] != null) {
			return Redirect::route('blog-edit', ['category' => ($data['post']->category ? $data['post']->category->slug : 'not-set'), 'slug' => $data['post']->slug]);
		}


		return Redirect::route('blog-index', []);
	}

	/**
	 * Create post, and redirect to edit post
	 *
	 * @param Request $request
	 * @return mixed
	 */
	public function createblogpost(Request $request)
	{
		$data = $this->runCommandWithErrorHandle(new CreateBlogPostCommand(['profileId' => ProfileService::getCurrentProfileId()]), $request);

		return Redirect::route('blog-edit', ['not-set', $data['post']->slug]);
	}

	/**
	 * Remove post
	 *
	 * @param Request $request
	 * @param $id
	 * @return mixed
	 */
	public function deleteblogpost(Request $request, $id)
	{
		$this->runCommandWithErrorHandle(new DeleteBlogPostCommand(['id' => $id]), $request);
		return Redirect::route('blog-index', []);
	}

	/**
	 * Attach images to post and return urls ( medium editor, medium-editor-insert-plugin)
	 *
	 * @param Request $request
	 * @return mixed
	 */
	public function attachImage(Request $request)
	{
		$inpData = Input::all();
		$inpData['images'] = Input::file('files');

		$data = $this->runCommandWithErrorHandle(new AttachImageToBlogPostCommand($inpData), $request);
		return Response::json((new AttachedImageResponseTransformer())->transform($data));

	}
}

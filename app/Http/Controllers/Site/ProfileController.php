<?php namespace App\Http\Controllers\Site;


use App\Http\Requests\Request;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Whoops\Exception\ErrorException;
use WL\Controllers\ControllerFront;
use WL\Modules\GiftCertificates\Facade\GiftCertificatesService;
use WL\Modules\Order\Models\OrderItem;
use WL\Modules\Profile\Commands\GetClientFullDetailsCommand;
use WL\Modules\Profile\Commands\GetProviderFullDetailsCommand;
use WL\Modules\Profile\Exceptions\ProviderClientNotFound;
use WL\Modules\Profile\Exceptions\WrongProfileTypeException;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Modules\Search\Facades\OrderService;
use WL\Modules\User\Commands\LoadCurrentUserCommand;
use WL\Modules\User\Commands\ForceLoginUserCommand;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Appointment\Models\ProviderAppointment;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Contracts\Validation\ValidationException;
use InvalidArgumentException;
use WL\Modules\Appointment\Facades\AppointmentService;
use WL\Modules\User\Exceptions\ActivationException;
use WL\Security\Services\Exceptions\AuthorizationException;
use Illuminate\Support\Facades\Crypt;
use WL\Wizard\Services\Facade\WizardService;

class ProfileController extends ControllerFront
{
	public function show(Request $request, $idOrSlug, $editMode = false)
	{
		try {
			$data = $this->runCommandWithErrorHandle(new GetProviderFullDetailsCommand([
				'profile_id' => $idOrSlug,
			]), $request);

		} catch (ModelNotFoundException $e) {
			return Redirect::route('home');
		} catch (WrongProfileTypeException $e) {
			return Redirect::route('providers-blog');
		}

		$profileId = $data['profile']->id;

        $bookable = ProfileService::isBookable($profileId);
        if($bookable) {
            $bookable = ProviderMembershipService::hasBookings($profileId);
        }
        # TODO - need to uncomment when not available route is ready
        if(!$bookable) {
            return Redirect::route('professional-not-available', ['id' => $data['profile']->slug]);
        }
        $data['bookable'] = $bookable;
        $manualPasswordRequired = false;
		if($loggedIn = Sentinel::check()) {
			list($user, $manualPasswordRequired) = $this->dispatch(new LoadCurrentUserCommand());
		}
		$data['manualPasswordRequired'] = $manualPasswordRequired;
		/*
		 * variables required on page that are used to open up booking wizard when clicking on link from email
		 */
		$data['showBookingWizard'] = 0;
		$data['appointmentId'] = null;

		$isCurrent = $this->isCurrent($profileId);
		$data['isOwnProfile'] = $isCurrent['profile'];
		$data['ownUser'] = $isCurrent['user'];

		$data['editMode'] = $editMode;

		$this->getMetaData($data);

		return $this->render('site.profile.provider', $data);
	}

	private function isCurrent($profileId)
	{
		$currentProfileId = ProfileService::getCurrentProfileId();

		$result = [
			'profile' => $profileId == $currentProfileId,
			'user' => false,
		];

		if ($currentProfileId) {
			$profileUser = ProfileService::getOwner($profileId);
			$currentUser = ProfileService::getOwner($currentProfileId);
			$result['user'] = $profileUser->id == $currentUser->id;
		}
		return $result;
	}

    public function showNotAvailable($idOrSlug)
    {
        $profile = ProfileService::getProfileByIdOrSlug($idOrSlug);
        return $this->render('site.profile.provider_not_available', ['profileId' => $profile->id]);
	}

	private function getMetaData(&$data)
	{
		$names = [];
		$data['services']->pluck('serviceTypes')->each(function($collection) use (&$names) {
			$names = array_merge($collection->pluck('name')->toArray(), $names);
		});

		$data['serviceNames'] = $names;
		$serviceString = '';
		if (count($names) > 1) {
			$last = array_pop($names);
			$serviceString = ', for ' . implode(', ', $names) . ' or ' . $last;
		} elseif (count($names) == 1) {
			$serviceString = ', for ' . $names[0];
		}
		$data['locationNames'] = $data['locations']->pluck('name')->toArray();
		$data['serviceDescription'] = $serviceString;
	}

	public function me(Request $request, $editMode = false)
	{
		if ($editMode != 'edit') {
			$editMode = false;
		}

		return $this->show($request, ProfileService::getCurrentProfileId(), $editMode);
	}

	public function clientShow(Request $request, $id)
	{
		try {
			$data = $this->runCommandWithErrorHandle(new GetClientFullDetailsCommand([
				'profile_id' => $id,
			]), $request);

			$isCurrent = $this->isCurrent($data['profile']->id);
			$data['isOwnProfile'] = $isCurrent['profile'];
			$data['ownUser'] = $isCurrent['user'];

		} catch (ModelNotFoundException $e) {
			return Redirect::route('home');
		} catch (WrongProfileTypeException $e) {
			return Redirect::route('providers-blog');
		} catch (ProviderClientNotFound $e) {
			return Redirect::route('home');
		} catch (AuthorizationException $e) {
			return Redirect::route('home');
		}

		return $this->render('site.profile.client', $data);
	}

	public function clientMe(Request $request)
	{
		return $this->clientShow($request, ProfileService::getCurrentProfileId());
	}

	public function openWizard(Request $request, $code)
	{
		$validStatuses = [ProviderAppointment::STATUS_PENDING, ProviderAppointment::STATUS_LOCKED];
		$showBookingWizard = 0;
		try {
			$params = Crypt::decrypt(base64_decode($code));
			$appointment = AppointmentService::getAppointment($params['appointmentId']);
			$this->dispatch(new ForceLoginUserCommand($params['userId'], ModelProfile::CLIENT_PROFILE_TYPE));

			if (!$appointment) {
				return Redirect::route('profile-show', [$params['professionalProfileId']])->with('cancelled_appointment', '');
			}
			if (!isset($appointment->status) || !in_array($appointment->status, $validStatuses)) {
				if ($appointment->status == ProviderAppointment::STATUS_CONFIRMED) {
					return Redirect::route('profile-show', [$appointment->provider_id])->with('confirmed_appointment', '');
				}
				return Redirect::route('profile-show', [$appointment->provider_id])->with('cancelled_appointment', '');
			}

			if ($appointment->from_utc < Carbon::now()) {
				AppointmentService::forceUpdateAppointmentStatus($appointment->id, ProviderAppointment::STATUS_CANCELLED);
				return Redirect::route('profile-show', [$appointment->provider_id])->with('past_appointment', '');
			}

			if ($appointment->status == ProviderAppointment::STATUS_PENDING) {
				AppointmentService::forceUpdateAppointmentStatus($appointment->id, ProviderAppointment::STATUS_LOCKED);
			}

			/*
			 * if wizard was previously opened and closed on the confirmation step we need to cancel the order
			 */

			$orderItem = OrderService::getOrderFromItemEntity($appointment->id, 'appointment', true);
			if (($orderItem instanceof OrderItem) && $orderItem->status == null) {
				$orderItem->cancelled();
				$orderItem->save();
			}

			WizardService::setCurrentLockedAppointmentId($appointment->id);
			WizardService::setSessionRegistrationIds([$appointment->registration->id]);

			$showBookingWizard = 1;

		} catch (DecryptException $e) {
			Session::flash('error', $e->getMessage());
		} catch (AuthorizationException $e) {
			Session::flash('error', $e->getMessage());
		} catch (ValidationException $e) {
			Session::flash('error', $e->getMessage());
		} catch (InvalidArgumentException $e) {
			Session::flash('error', $e->getMessage());
		} catch (ActivationException $e) {
			Session::flash('error', $e->getMessage());
		} catch (ErrorException $e) {
			return Redirect::route('providers-blog');
		}

		try {
			$data = $this->runCommandWithErrorHandle(new GetProviderFullDetailsCommand([
				'profile_id' => $appointment->provider_id,
			]), $request);

			$profileUser = ProfileService::getProfileById($data['profile']->id)->owner();
			$data['ownUser'] = $profileUser->id == $params['userId'];
			$bookable = ProfileService::isBookable($appointment->provider_id);
			if($bookable) {
				$bookable = ProviderMembershipService::hasBookings($appointment->provider_id);
			}
			$data['bookable'] = $bookable;

		} catch (ModelNotFoundException $e) {
			abort(404);
		} catch (WrongProfileTypeException $e) {
			return Redirect::route('providers-blog');
		} catch (ErrorException $e) {
			return Redirect::route('providers-blog');
		}

		$manualPasswordRequired = false;
		if($loggedIn = Sentinel::check()) {
			list($user, $manualPasswordRequired) = $this->dispatch(new LoadCurrentUserCommand());
		}
		$data['manualPasswordRequired'] = $manualPasswordRequired;
		$data['showBookingWizard'] = $showBookingWizard;
		$data['appointmentId'] = $appointment->id;
		$data['editMode'] = false;
		$data['isOwnProfile'] = false;

		$this->getMetaData($data);

		return $this->render('site.profile.provider', $data);
	}

	public function redeemGiftCertificate(Request $request, $code)
	{
		try {

			$params = Crypt::decrypt(base64_decode($code));

			$this->dispatch(new ForceLoginUserCommand($params['userId'], ModelProfile::CLIENT_PROFILE_TYPE));

			$certificate = GiftCertificatesService::getCertificate($params['certificateId']);

			// no certificate found or certificate was not purchased
			if (!$certificate || !$certificate->purchased_date) {
				return Redirect::route('profile-show', [$params['providerId']])->with('gift_certificate_declined', '');
			}

			// certificate was already used
			if ($certificate->redeemed_date) {
				return Redirect::route('profile-show', [$params['providerId']])->with('gift_certificate_redeemed', '');
			}

		} catch (DecryptException $e) {
			Session::flash('error', $e->getMessage());
		} catch (AuthorizationException $e) {
			Session::flash('error', $e->getMessage());
		} catch (ValidationException $e) {
			Session::flash('error', $e->getMessage());
		} catch (InvalidArgumentException $e) {
			Session::flash('error', $e->getMessage());
		} catch (ActivationException $e) {
			Session::flash('error', $e->getMessage());
		} catch (ErrorException $e) {
			return Redirect::route('providers-blog');
		}

		return Redirect::route('dashboard-my-gift-certificates')->with('gift_certificate_use', $certificate->id);
	}
}

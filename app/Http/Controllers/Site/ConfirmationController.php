<?php namespace App\Http\Controllers\Site;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use InvalidArgumentException;
use WL\Controllers\ControllerBase;
use WL\Modules\Appointment\Commands\CancelAppointmentCommand;
use WL\Modules\Profile\Commands\ConfirmClientInvitationCommand;
use WL\Modules\Profile\Commands\ConfirmClientInvitationExternalCommand;
use WL\Modules\Profile\Exceptions\ProfileException;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\User\Commands\ForceLoginUserCommand;
use WL\Modules\User\Exceptions\ActivationException;
use WL\Modules\User\Facades\User;
use WL\Security\Services\Exceptions\AuthorizationException;

/**
 * Class ConfirmationController
 * @package App\Http\Controllers\Comment
 */
class ConfirmationController extends ControllerBase
{
	use DispatchesJobs;

	public function confirmEmailInvite($code)
	{
		$data = [];
		try {
			$data = $this->dispatch(new ConfirmClientInvitationExternalCommand(['code' => $code]));
		} catch (AuthorizationException $e) {
			Session::flash('error', $e->getMessage());
		} catch (ValidationException $e) {
			Session::flash('error', $e->getMessage());
		} catch (InvalidArgumentException $e) {
			Session::flash('error', $e->getMessage());
		} catch (ActivationException $e) {
			Session::flash('error', $e->getMessage());
		} catch (ProfileException $e) {
			Session::flash('error', $e->getMessage());
		}

		if (empty($data)) {
			return redirect(route('home'));
		}

		$params = [
			'id' => ProfileService::getProfileById($data['providerId'])->slug,
		];
		if(User::isManualPasswordRequired($data['email'])){
			$this->dispatch(new ForceLoginUserCommand($data['userId'], ModelProfile::CLIENT_PROFILE_TYPE));
			$params['referrer'] = 'confirm';
		}
		return redirect(route('profile-show', $params));
	}

	/**
	 * Cancel confirmation
	 *
	 * @param string $code
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function cancelAppointmentConfirmation($code)
	{
		try {
			/**
			 * @TODO: add a page with textarea to write reasoon for cancellation.
			 * Good to have because user could have clicked the link accidentally.
			 */
			$params = Crypt::decrypt(base64_decode($code));

			$this->dispatch(new ForceLoginUserCommand($params['userId'], ModelProfile::CLIENT_PROFILE_TYPE));
			$this->dispatch(new CancelAppointmentCommand($params['appointmentId'], ''));

			Session::flash('success', 'The appointment was successfully cancelled');

		} catch (DecryptException $e) {
			Session::flash('error', $e->getMessage());
		} catch (AuthorizationException $e) {
			Session::flash('error', $e->getMessage());
		} catch (ValidationException $e) {
			Session::flash('error', $e->getMessage());
		} catch (InvalidArgumentException $e) {
			Session::flash('error', $e->getMessage());
		} catch (ActivationException $e) {
			Session::flash('error', $e->getMessage());
		}

		return redirect(route('dashboard-home'));
	}
}

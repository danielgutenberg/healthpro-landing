<?php namespace App\Http\Controllers\Site;

use App\Http\Requests\Request;
use WL\Controllers\ControllerFront;
use WL\Modules\Location\Commands\LoadLocationTypesCommand;

class SearchController extends ControllerFront
{
	public function getIndex(Request $request)
	{
		$locationTypes = $this->runCommandWithErrorHandle(new LoadLocationTypesCommand(), $request);

		return view('site/search/index', compact('locationTypes'));
	}
}

<?php namespace App\Http\Controllers\Site;

use App\Http\Requests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use WL\Controllers\ControllerFront;
use WL\HelpPost\Commands\LoadHelpPostsCommand;

class HelpController extends ControllerFront
{
	use DispatchesJobs;

	/**
	 * Display a listing of the posts.
	 *
	 * @param $sectionSlug string
	 * @param $categorySlug string
	 * @return Response
	 */
	public function index($sectionSlug = null, $categorySlug = null)
	{
		list($sections, $categories, $sectionSlug, $categorySlug, $categoryName, $posts) =
			$this->dispatch(
				new LoadHelpPostsCommand($sectionSlug, $categorySlug)
			);

		return view('site.help.index', compact('sectionSlug', 'categorySlug', 'categoryName', 'sections', 'categories', 'posts'));
	}

}

<?php namespace App\Http\Controllers\Site;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;
use WL\Modules\Comment\Commands\AddCommentCommand;
use WL\Modules\Comment\Commands\RemoveCommentCommand;
use WL\Modules\Comment\Commands\UpdateCommentContentCommand;
use WL\Modules\Comment\Commands\UpdateCommentStatusCommand;
use WL\Controllers\ControllerBase;

/**
 * Class CommentController
 * @package App\Http\Controllers\Comment
 */
class CommentController extends ControllerBase
{
	use DispatchesJobs;

	/**
	 * Add comment
	 * @return mixed
	 */
	public function addComment()
	{
		$data = Input::all();
		$data['profile_id'] = Cookie::get('profile_id');

		$this->dispatch(new AddCommentCommand($data));

		return redirect()->back();
	}

	/**
	 * Remove comment
	 * @param $commentId
	 * @return mixed
	 */
	public function removeComment($commentId)
	{
		$this->dispatch(new RemoveCommentCommand($commentId));

		return redirect()->back();
	}

	/**
	 * Update comment content
	 * @param $commentId
	 * @return mixed
	 */
	public function updateContent($commentId)
	{
		$content = Input::get('content');

		$this->dispatch(new UpdateCommentContentCommand($commentId, $content));

		return redirect()->back();
	}

	/**
	 * Update comment status
	 * @param $commentId
	 * @return mixed
	 */
	public function updateStatus($commentId)
	{
		$status = Input::get('status');

		$this->dispatch(new UpdateCommentStatusCommand($commentId, $status));

		return redirect()->back();
	}

}

<?php
namespace App\Http\Controllers\Site;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Helpers\MetaParser;
use WL\Modules\Page\Repositories\Exceptions\InvalidPathException;
use Redirect;
use WL\Controllers\ControllerFront;
use WL\Modules\Page\Repositories\PageRepository;
use WL\Modules\Provider\Commands\GetActiveProviderPlanCommand;
use WL\Modules\Provider\Commands\GetSignUpProductsCommand;
use WL\Modules\Provider\Transformers\ProductSelectionTransformers;

class PageController extends ControllerFront
{
	/**
	 * @param string $slug
	 * @param \WL\Modules\Page\Repositories\PageRepository $repository
	 * @return \Illuminate\View\View
	 */
	public function getShow($slug, PageRepository $repository)
	{
		try {
			$page = $repository->getByUri($slug);

		// there is no page with given slug
		} catch (ModelNotFoundException $e) {
			abort(404);

		// if the slug is correct but the full path isn't - redirect to the correct path
		} catch (InvalidPathException $e) {

			return Redirect::route(
				'page.show',
				[
					'slug' => $e->getCorrectPath()
				]
			);
		}

		return $this->render($page->getViewName(), compact('page'));
	}

	/**
	 * @return \WL\Controllers\Response
	 */
	public function getFeatures()
	{
		return $this->render('site.page.features');
	}

	/**
	 * @param null|string $service
	 * @return \WL\Controllers\Response
	 */
	public function getHowItWorks($service = null)
	{
		// ideally this should be in a command
		$fullService = $service ? MetaParser::parseAssoc('content/services', 'services', $service) : null;

		// redirect to blank service if invalid service provided
		if ($service && !$fullService) {
			return redirect(route('how-it-works'));
		}
        $products = $this->getProducts();

		return $this->render('site.page.how-it-works', compact('fullService', 'blockSignup', 'products'));
	}

	/**
	 * @return \WL\Controllers\Response
	 */
	public function getPricing()
	{
		$popup = session('payment_popup');
		$socialSignup = session('socialSignup');
		$productId = session('productId');

		if (!$this->user) {
			session()->forget('productId');
			session()->forget('payment_popup');
			session()->forget('socialSignup');
		}
		$products = $this->getProducts();

		return $this->render('site.page.pricing', compact('popup', 'socialSignup', 'productId', 'blockSignup', 'products'));
	}

	private function getTestProducts()
	{
		return [
			[
				'membership' => [
					'id' => 1,
					'name' => 'Part Timer',
					'description' => 'Part Timer plan includes 12 bookings per month. Additional bookings will be charged at $1.45 each. UPGRADE ANYTIME!',
					'slug' => 'part_timer',
					'features' => [
						0 => "calendar_integration",
						1 => "classes",
						2 => "client_notes",
						3 => "intake_forms",
						4 => "intro_session",
						5 => "monthly_packages",
					],
					'bookings_limit' => 12,
					'over_limit_fee' => 1.45,
					'rank' => 3,
				],
				'action' => 'BUY NOW',
				'products' => collect([[
					"id" => 1,
					"name" => "$18.89/month",
					"price" => 18.89,
					"billing_cycles" => 1,
					"allowed" => true
				],[
					"id" => 2,
					"name" => "$189.00/year",
					"price" => 189.0,
					"billing_cycles" => 12,
					"allowed" => true
				]])
			],
			[
				'membership' => [
					'id' => 1,
					'name' => 'Healthpro',
					'description' => 'HealthPRO plan includes 26 bookings per month. Additional bookings will be charged at $1.45 each.',
					'slug' => 'healthpro',
					'features' => [
						0 => "calendar_integration",
						1 => "classes",
						2 => "client_notes",
						3 => "gift_certificates",
						4 => "intake_forms",
						5 => "intro_session",
						6 => "messaging",
						7 => "monthly_packages"
					],
					'bookings_limit' => 26,
					'over_limit_fee' => 1.45,
					'rank' => 2,
				],
				'action' => 'BUY NOW',
				'products' => collect([[
					"id" => 3,
					"name" => "$27.00/month",
					"price" => 27.0,
					"billing_cycles" => 1,
					"allowed" => true
				],[
					"id" => 4,
					"name" => "$270.00/year",
					"price" => 270.0,
					"billing_cycles" => 12,
					"allowed" => true
				]])
			],
			[
				'membership' => [
					'id' => 1,
					'name' => 'Healthpro Plus',
					'description' => 'HealthPro Plus has unlimited booking',
					'slug' => 'healthpro_plus',
					'features' => [
						0 => "calendar_integration",
						1 => "classes",
						2 => "client_notes",
						3 => "intake_forms",
						4 => "intro_session",
						5 => "monthly_packages",
					],
					'bookings_limit' => null,
					'over_limit_fee' => null,
					'rank' => 1,
				],
				'action' => 'BUY NOW',
				'products' => collect([[
					"id" => 1,
					"name" => "$36.00/month",
					"price" => 36.0,
					"billing_cycles" => 1,
					"allowed" => true
				],[
					"id" => 2,
					"name" => "$360.00/year",
					"price" => 360.0,
					"billing_cycles" => 12,
					"allowed" => true
				]])
			]
		];
	}

    private function getProducts()
    {
        $data = [];
		try {
			if ($plan = $this->dispatch(new GetActiveProviderPlanCommand())) {
				$data['plan_id'] = $plan->id;
			}
			$products = $this->dispatch(new GetSignUpProductsCommand($data));
		} catch (\Exception $e) {
			return $this->getTestProducts();
		}

        return (new ProductSelectionTransformers())->transformCollection($products);
	}

	/**
	 * @return \WL\Controllers\Response
	 */
	public function getAbout()
	{
		return $this->render('site.page.about');
	}

	public function getAffiliate()
	{
		return $this->render('site.page.affiliate');
	}

	public function getContactUs()
	{
		return $this->render('site.page.contactus');
	}

	/**
	 * @return \WL\Controllers\Response
	 */
	public function getPartner()
	{
		return $this->render('site.page.partner');
	}

	public function getFacebookBookMe(){
        $data = [
            'title' => 'HealthPRO | Add a Book Me Button to Facebook Page',
            'description' => 'Learn how to add a Book Me button to your Facebook page. Adding a Book Me button to your Facebook page will direct friends and followers to HealthPRO where they can book an appointment with you.',
            'keywords' => ''
        ];
        return $this->render('site.page.facebook-book-me', $data);
    }

}

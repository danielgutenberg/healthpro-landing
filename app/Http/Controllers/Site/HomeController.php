<?php namespace App\Http\Controllers\Site;

use Carbon\Carbon;
use WL\Controllers\ControllerFront;
use WL\Modules\Coupons\Facades\CouponService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Models\ProviderProfile;
use WL\Modules\Search\Facades\AvailableProviderSearchService;
use WL\Modules\Search\Transformers\RecentProfessionalsTransformer;
use WL\Wizard\Helpers\RenderWizard;
use WL\Modules\Schedule\Commands\GetSiteStatistics;
use Illuminate\Support\Facades\Crypt;
use WL\Yaml\Facades\Yaml;
use Redirect;
use Location;

class HomeController extends ControllerFront
{
	public function getIndex()
	{
		//check if we can redirect professional to wizard
		if ($redirect = RenderWizard::maybeRedirectToWizard('profile_setup')) {
			return $redirect;
		}

		/*
		 * get site statistics to display on home page
		 */
		$data = $this->dispatch(new GetSiteStatistics());
		$location = Location::get();

		$city = $location->cityName ?: 'New York';

		$postalCode = $location->postalCode;

		$searchResults = AvailableProviderSearchService::search(null, $city, null,
			false, 100, null, [], [], [], [], Carbon::now(), Carbon::parse('+ 3 days'), 30, 0,
			1, 9, 'featured', 'DESC', true);

        $transformedResults = (new RecentProfessionalsTransformer())->transformCollection($searchResults->results);

		$professionals = array_filter($transformedResults, function($result) {
			return $result->profile['avatar'];
		});

		$data['prosToDisplay'] = $professionals;

		$data['location'] = $postalCode;
		$data['city'] = $city;

		$data['popup'] = session()->has('popup') ? session('popup')[0] : null;

		$professionalRoute = function($profile) {
			if (!$profile) {
				return route('auth-register-pro');
			}
			if ($profile->type == ModelProfile::PROVIDER_PROFILE_TYPE) {
				return route('dashboard-home');
			} else {
				return route('dashboard-get-register-as', ['type' => 'provider']);
			}
		};
		$data['hasAvatar'] = $this->currentProfile && $this->currentProfile->avatar ? true : false;
		$data['registerAsProfessionalRoute'] = $professionalRoute($this->currentProfile);
		$data['slides'] = Yaml::get('items', 'wl.home_slider.meta');
		return $this->render('site.home.index', $data);
	}

	public function getProfessionals()
	{
		$data['popup'] = session()->has('popup') ? session('popup')[0] : null;
		$data['slides'] = Yaml::get('items', 'wl.home_slider.meta');
		return $this->render('site.home.professionals', $data);
	}

	public function openEmailLink($code = '')
	{
		$params = Crypt::decrypt(base64_decode($code));

		$currentProfileId = ProfileService::getCurrentProfileId();
		$currentUser = ProfileService::getOwner($currentProfileId);

		$intendedUser = ProfileService::getOwner($params['profileId']);

		/*
		 * if no user is logged in, then save redirect url to the session and redirect to the login page
		 */

		if (!$currentProfileId) {
			session(['login_profile' => $params['profileId']]);
			session(['redirect_url' => $params['route']]);
			return Redirect::route('auth-login');
		}
		if ($currentProfileId == $params['profileId']) {
			return Redirect::to($params['route']);
		}

		if ($currentUser->id == $intendedUser->id) {
			ProfileService::setCurrentProfileId($params['profileId']);
			return Redirect::to($params['route']);
		}


		$data['popup'] = null;
		$data['slides'] = Yaml::get('items', 'wl.home_slider.meta');

		#todo - create popup for user to ask if they would like to sign out of the current account and log in with new profile

		return $this->render('site.home.professionals', $data);
	}

	public function uploadClients($code = '')
	{
		$professionalProfile = false;
		$profile = ProfileService::getCurrentProfile();
		$coupon = CouponService::getCoupon($code);

		if (!$coupon) {
			return redirect(route('dashboard-my-clients'));
		}
		$date = Carbon::parse($coupon->validUntil)->format('F jS, Y');

		if ($profile) {
			$currentUser = ProfileService::getOwner($profile->id);
			$professionalProfile = $currentUser->profiles(ProviderProfile::class)->first();

			if ($profile->type == ModelProfile::PROVIDER_PROFILE_TYPE) {

				session([
					'coupon_amount' => $coupon->amount,
					'coupon_type' => $coupon->amountType,
					'coupon_valid' => $date
				]);
				return redirect(route('dashboard-my-clients'))->with('open_popup', $code);
			}
		}

		/*
		 * if logged in as a client and the user has a professional profile we switch to the professional
		 */

		if ($professionalProfile) {
			session([
				'coupon_amount' => $coupon->amount,
				'coupon_type' => $coupon->amountType,
				'coupon_valid' => $date
			]);
			ProfileService::setCurrentProfileId($professionalProfile->id);
			return redirect(route('dashboard-my-clients'))->with('open_popup', $code);
		}

		session([
			'profile_type' => ModelProfile::PROVIDER_PROFILE_TYPE,
			'redirect_url' => route('dashboard-my-clients'),
			'open_popup' => $code,
			'coupon_amount' => $coupon->amount,
			'coupon_type' => $coupon->amountType,
			'coupon_valid' => $date
		]);

		return Redirect::route('auth-login');

	}
}

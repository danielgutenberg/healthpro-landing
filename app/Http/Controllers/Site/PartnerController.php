<?php namespace App\Http\Controllers\Site;

use App\Exceptions\GenericException;
use Illuminate\Http\Request;
use WL\Controllers\ControllerFront;
use Illuminate\Support\Facades\Crypt;
use WL\Yaml\Facades\Yaml;
use Redirect;
use Location;


class PartnerController extends ControllerFront
{

    private function getHasOffersCode($affiliateId)
    {
        $dataObject = [
            'affiliate_id' => $affiliateId,
            'offer_id' => 12
        ];
        return base64_encode(Crypt::encrypt($dataObject));
    }


    public function getPartner($name, Request $request)
    {
        $items = Yaml::get('items', 'wl.partner.meta');
        if (empty($name) || empty($items[$name])) {
            throw new GenericException(404);
        }

        $text_paragraph_1 = '';
        if(!empty($items[$name]['text_paragraph_1'])){
            $text_paragraph_1 = $items[$name]['text_paragraph_1'];
        }
        $text_paragraph_2 = '';
        if(!empty($items[$name]['text_paragraph_2'])){
            $text_paragraph_2 = $items[$name]['text_paragraph_2'];
        }
        $text_paragraph_3 = '';
        if(!empty($items[$name]['text_paragraph_3'])){
            $text_paragraph_3 = $items[$name]['text_paragraph_3'];
        }

        $data = [
            'partner_name' => $name,
            'title' => $items[$name]['title'],
            'description' => $items[$name]['description'],
            'keywords' => $items[$name]['keywords'],
            'text_title' => $items[$name]['text_title'],
            'text_paragraph_1' => $text_paragraph_1,
            'text_paragraph_2' => $text_paragraph_2,
            'text_paragraph_3' => $text_paragraph_3,
			'slides' => Yaml::get('items', 'wl.home_slider.meta'),
        ];

        return $this->render('site.home.partner', $data);
    }
}

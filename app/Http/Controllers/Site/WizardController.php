<?php namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use WL\Controllers\ControllerFront;
use WL\Wizard\Commands\LoadWizardCommand;
use WL\Wizard\Commands\LoadWizardRenderInfoCommand;
use WL\Wizard\Commands\RenderCommand;
use WL\Wizard\Commands\StoreStepCommand;
use WL\Wizard\Exceptions\InvalidStepException;
use WL\Wizard\Exceptions\ValidationWizardException;
use WL\Wizard\Exceptions\WizardException;
use WL\Wizard\WizardPageRenderHelper;

class WizardController extends ControllerFront
{

	/**
	 * Render wizard .
	 *
	 * @param string $wizard
	 * @param array $step
	 * @return mixed
	 */
	public function renderWizard($wizard, $step)
	{

		try {

			$data = $this->dispatch(new LoadWizardRenderInfoCommand($wizard, $step));
			return WizardPageRenderHelper::render($data);

		} catch (ValidationWizardException $e) {
			return redirect(route('home'));
		} catch (WizardException $e) {
			return redirect(route('home'));
		} catch (InvalidStepException $e) {

			$info = $this->dispatch(new LoadWizardCommand($wizard));

			return redirect(
				route('wizard_route', [
					'wizard' => $wizard,
					'step' => $info['current_step']//$step->getSlug(),
				])
			);
		}
	}

	public function continueWizard($wizard)
	{
		try {
			$info = $this->dispatch(new LoadWizardCommand($wizard));
			
			return redirect(
				route('wizard_route', [
					'wizard' => $wizard,
					'step' => $info['current_step']
				])
			);

		} catch (WizardException $e) {
			return redirect(route('home'));
		}


		return redirect(route('home'));
	}

	/**
	 * Store wizard ..
	 *
	 * @param $wizard
	 * @param $step
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
//	public function store($wizard, $step, Request $request) {
//
//		$result = $this->dispatch(new StoreStepCommand(
//			array(
//				'wizard' => $wizard,
//				'step'   => $step,
//				'params' => $request->all()
//			)
//		));
//
//		return redirect($result['route']);
//
//	}

}

<?php
namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Validator;
use Response;
use Session;
use WL\Contracts\Subscriber;
use WL\Controllers\ControllerFront;

class LandingController extends ControllerFront
{
	protected $newsletter;

	public function __construct( Subscriber $subscriber )
	{
		$this->subscriber = $subscriber;

		return parent::__construct();
	}

	/**
	* @Get("/", as="landing")
	*/
	public function getIndex()
	{
		return view('site/landing/index');
	}

	/**
	 * @Post("/subscribe", as="landing-subscribe")
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function postSubscribe( Request $request )
	{
		$email =  $request->get( 'email' );

		$validator = Validator::make(
			[
				'email' => $email
			],
			[
				'email' => 'required|email'
			],
			[
				'required' => __('Please enter a valid email address'),
				'email'    => __('Please enter a valid email address'),
			]
		);

		$success = 0;
		$message = __('Thank you for the interest to our service. An email has been sent to you that contains a verification link.');

		if ($validator->fails()) {
			$message = $validator->messages()->first('email');
		} else {

			$this->subscriber->subscribe( [
				'email' => $email,
			] );

			$success = $this->subscriber->success();

			if ( !$success ) {
				$message = $this->subscriber->message();
			}
		}

		if ($request->ajax()) {
			return Response::json([
				'success' => $success,
				'message' => $message,
			]);
		} else {
			Session::flash( $success ? 'success' : 'error', $message );

			return redirect()->route('landing');
		}
	}
}

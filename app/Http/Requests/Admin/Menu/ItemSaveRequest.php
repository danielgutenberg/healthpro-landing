<?php namespace App\Http\Requests\Admin\Menu;

use App\Http\Requests\AdminRequest;

class ItemSaveRequest extends AdminRequest
{
	protected $rules = [
		'label'		=> 'required',
	];
}

<?php namespace App\Http\Requests\Admin\Menu;

use App\Http\Requests\AdminRequest;

class UpdateRequest extends AdminRequest
{
	protected $rules = [
		'name'					=> 'required',
	];
}

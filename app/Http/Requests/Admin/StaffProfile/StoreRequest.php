<?php namespace App\Http\Requests\Admin\StaffProfile;

use App\Http\Requests\AdminRequest;
use WL\Modules\User\Models\User;

class StoreRequest extends AdminRequest
{
	public function meta()
	{
		return $this->get('meta', null);
	}

	public function users()
	{
		return $this->get('users', []);
	}
}

<?php namespace App\Http\Requests\Admin\Content;

use App\Http\Requests\AdminRequest;

class StoreRequest extends AdminRequest
{
	protected $rules = [
		'title' => 'required',
		'type'  => 'required',
		'url'   => 'required_if:type,embed-video|embedvideo'
	];

	/**
	 * 	Custom validator error messages ...
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'url.embedvideo' => __('Please enter an valid video url'),
			'url.required_if'=> __('Please enter an valid video url'),
			'title.required'  => __('Please enter title'),
			'type.required'	  => __('Internal error')
		];
	}

}

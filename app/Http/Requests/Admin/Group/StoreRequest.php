<?php namespace App\Http\Requests\Admin\Group;

use App\Http\Requests\AdminRequest;

class StoreRequest extends AdminRequest
{
	protected $attachmentFields = ['thumbnail', 'image'];

	public function meta()
	{
		return $this->get('meta', []);
	}

	public function phones()
	{
		return $this->get('phones', []);
	}

	public function socialPages()
	{
		return $this->get('social_pages', []);
	}

	public function attachments()
	{
		$attachments = [];
		foreach ($this->attachmentFields as $field) {
			if ($this->hasFile($field)) {
				$attachments[$field] = $this->file($field);
			}
		}

		return $attachments;
	}

}

<?php namespace App\Http\Requests\Admin\Comment;

use App\Http\Requests\AdminRequest;
use WL\Modules\Comment\Models\Comment;
use WL\Modules\Comment\Facades\CommentService;

class UpdateRequest extends AdminRequest
{
	protected $rules = [
		'parent_id' => 'required|integer',
		'user_id' => 'required|integer',
		'content'   => '',
		'status'    => '',
	];

	public function __construct()
	{
		$this->rules['status'] = 'in:'.implode(',', CommentService::getAvailableStatuses());
	}

	public function fields()
	{
		return $this->except('_token', '_method');
	}
}

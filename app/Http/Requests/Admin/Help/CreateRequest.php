<?php namespace App\Http\Requests\Admin\Help;

use App\Http\Requests\AdminRequest;
use WL\Modules\HelpPost\Models\HelpPost;

class CreateRequest extends AdminRequest
{
	protected $rules = [
		'subject'	=> 'required',
		'content'	=> 'required',
		HelpPost::SECTION_SLUG => 'required',
		HelpPost::CATEGORY_SLUG => 'required',
	];
}

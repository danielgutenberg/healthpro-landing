<?php namespace App\Http\Requests\Admin\Page;

use App\Http\Requests\AdminRequest;

class UpdateRequest extends AdminRequest
{
	protected $rules = [
		'slug'      => 'required|alpha_dash|min:3',
		'parent_id' => 'required|integer',
		'title'     => 'required',
		'content'   => '',
		'active'    => 'integer',
		'rank'      => 'integer',
//		'image'		=> 'image',
//		'thumbnail'	=> 'image',
	];

	protected $attachmentFields = ['thumbnail', 'image'];

	protected $dontFlash = ['image', 'thumbnail'];

	public function fields()
	{
		return $this->except('_token', 'meta', 'translation', 'image', 'thumbnail');
	}

	public function invalidFields()
	{
		return $this->fields();
	}

	public function meta()
	{
		return $this->get('meta');
	}

	public function attachments()
	{
		$attachments = [];
		foreach( $this->attachmentFields as $field ) {
			if ($this->hasFile($field)) {
				$attachments[$field] = $this->file($field);
			}
		}

		return $attachments;
	}

	public function translation()
	{
		return $this->get('translation');
	}
}

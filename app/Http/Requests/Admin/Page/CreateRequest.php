<?php namespace App\Http\Requests\Admin\Page;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends UpdateRequest
{
	protected $rules = [
		'slug'      => 'required|alpha_dash|min:3',
		'parent_id' => 'required|integer',
		'title'     => 'required',
		'content'   => '',
		'active'    => 'integer',
		'rank'      => 'integer',
//		'image'		=> 'image',
//		'thumbnail'	=> 'image',
	];
}

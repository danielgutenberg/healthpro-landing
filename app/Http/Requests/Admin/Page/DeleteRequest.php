<?php namespace App\Http\Requests\Admin\Page;

use App\Http\Requests\AdminRequest;

class DeleteRequest extends AdminRequest
{
	protected $rules = [
		'ids' => '',
	];

	public function fields()
	{
		$ids = [];

		foreach( $this->get('ids') as $id => $checked ) {
			if ($checked) {
				$ids[] = $id;
			}
		}

		return $ids;
	}
}

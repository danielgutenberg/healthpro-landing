<?php namespace App\Http\Requests\Admin\Role;

use App\Http\Requests\AdminRequest;

class EditRoleRequest extends AdminRequest
{
	protected $rules = [
		'name' => 'required'
	];
}

<?php namespace App\Http\Requests\Admin\Education;

use App\Http\Requests\AdminRequest;

class StoreRequest extends AdminRequest
{
	protected $rules = [
		'degree' => 'required|min:3|max:100',
	];
}

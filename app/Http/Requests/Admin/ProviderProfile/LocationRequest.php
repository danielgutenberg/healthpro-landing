<?php namespace App\Http\Requests\Admin\ProviderProfile;

use App\Http\Requests\AdminRequest;

class LocationRequest extends AdminRequest
{
	protected $rules = [
		'location_id' => 'required|integer|exists:locations,id',
		'accept_appointments' => 'boolean',
		'is_manager' => 'boolean',
		'is_owner' => 'boolean',
		'approved' => 'boolean',
	];
}

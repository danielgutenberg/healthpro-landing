<?php namespace App\Http\Requests\Admin\ProviderProfile;

use App\Http\Requests\AdminRequest;

class EducationRequest extends AdminRequest
{
	protected $rules = [
		'education_id' => 'required|integer|exists:educations,id',
		'degree' => 'required|sometimes',
		'from' => 'required',
		'to' => 'required|sometimes',
	];
}

<?php namespace App\Http\Requests\Admin\ProviderProfile;

use App\Http\Requests\AdminRequest;

class StoreRequest extends AdminRequest
{
	protected $attachmentFields = ['upload_certifications'];

	public function certifications()
	{
		return $this->get('certifications', null);
	}

	public function educations()
	{
		return $this->get('educations', null);
	}

	public function phones()
	{
		return $this->get('phones', null);
	}

	public function meta()
	{
		return $this->get('meta', null);
	}

	public function translation()
	{
		return $this->get('translation', null);
	}

	public function tags()
	{
		return $this->get('tags', null);
	}

	public function attachments()
	{
		$attachments = [];
		foreach ($this->attachmentFields as $field) {
			if ($this->hasFile($field)) {
				$attachments[$field] = $this->file($field);
			}
		}

		return $attachments;
	}

	public function users()
	{
		return $this->get('users', []);
	}
}

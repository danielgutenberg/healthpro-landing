<?php namespace App\Http\Requests\Admin\Certification;

use App\Http\Requests\AdminRequest;
use Illuminate\Support\Facades\Validator;
use WL\Modules\Coupons\Facades\CouponService;

class CouponRequest extends AdminRequest
{
	protected $rules = [
		'name' => 'required',
		'description' => 'required',
		'amount' => 'required|numeric',
		'valid_from' => 'required|date',
		'valid_until' => 'required|date',
		'type' => "in:staff,provider"
	];

	public function validator()
	{
		$validator = Validator::make($this->all(), $this->rules);
		$validator->after(function ($validator) {
			$coupon = CouponService::getCoupon($this->input('name'));
			if ($coupon !== null) {
				$validator->errors()->add('name', 'This code is already exist');
			}
		});

		return $validator;
	}
}

<?php namespace App\Http\Requests\Admin\Taxonomy;

use App\Http\Requests\AdminRequest;

class CreateRequest extends AdminRequest
{
	protected $rules = [
		'name'					=> 'required',
		'act_like'				=> '',
		'allow_custom_tags'	=> 'integer',
	];
}

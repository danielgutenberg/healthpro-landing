<?php namespace App\Http\Requests\Admin\Location;

use App\Http\Requests\AdminRequest;

class StoreRequest extends AdminRequest
{
	public function meta()
	{
		return $this->get('meta', []);
	}

	public function locationType()
	{
		return $this->get('location_type_id', null);
	}

	public function phones()
	{
		return $this->get('phones', []);
	}

	public function address()
	{
		return $this->get('address', []);
	}
}

<?php namespace App\Http\Requests\Admin\ClientProfile;

use App\Http\Requests\AdminRequest;

class StoreRequest extends AdminRequest
{
	public function meta()
	{
		return $this->get('meta', null);
	}

	public function users()
	{
		return $this->get('users', []);
	}

}

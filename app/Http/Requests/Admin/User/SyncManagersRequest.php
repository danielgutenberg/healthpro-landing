<?php namespace App\Http\Requests\Admin\User;

use App\Http\Requests\AdminRequest;

class SyncManagersRequest extends AdminRequest
{
	protected $rules = [
		'user_id' => 'required'
	];
}

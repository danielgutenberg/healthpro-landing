<?php namespace App\Http\Requests\Admin\User;

use App\Http\Requests\AdminRequest;

class UpdateRequest extends AdminRequest
{
	protected $rules = [
		'email' => 'required|email'
	];
}

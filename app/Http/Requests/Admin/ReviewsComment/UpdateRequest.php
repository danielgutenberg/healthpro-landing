<?php namespace App\Http\Requests\Admin\ReviewsComment;

use App\Http\Requests\AdminRequest;
use WL\Modules\Comment\Facades\CommentService;

class UpdateRequest extends AdminRequest
{
	protected $rules = [
		'profile_id' => 'required|integer',
		'content'   => '',
		'status'    => '',
	];

	public function __construct()
	{
		$this->rules['status'] = 'in:'.implode(',', CommentService::getAvailableStatuses());
	}

	public function fields()
	{
		return $this->except('_token', '_method');
	}
}

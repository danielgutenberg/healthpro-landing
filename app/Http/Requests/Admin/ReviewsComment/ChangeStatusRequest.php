<?php namespace App\Http\Requests\Admin\ReviewsComment;

use App\Http\Requests\AdminRequest;
use WL\Modules\Comment\Facades\CommentService;

class ChangeStatusRequest extends AdminRequest
{
	protected $rules = [
		'ids' => '',
		'status' => '',
	];

	public function __construct()
	{
		$this->rules['status'] = 'in:'.implode(',', CommentService::getAvailableStatuses());
	}

	public function fields()
	{
		$ids = [];

		foreach( $this->get('ids') as $id => $checked ) {
			if ($checked) {
				$ids[] = $id;
			}
		}

		return $ids;
	}
}

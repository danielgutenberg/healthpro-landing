<?php namespace App\Http\Requests\Admin\Certification;

use App\Http\Requests\AdminRequest;

class StoreRequest extends AdminRequest
{
	protected $rules = [
		'name' => 'required|min:3|max:100',
	];
}

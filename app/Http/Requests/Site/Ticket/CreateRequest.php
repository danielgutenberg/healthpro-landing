<?php namespace App\Http\Requests\Site\Ticket;

use App\Http\Requests\RequestBase;

class CreateRequest extends RequestBase
{
	protected $rules = [
		'user_name' => 'required',
		'user_email' => 'required|email',
		'message' => 'required',
	];

	public function messages()
	{
		return [
			'user_name.required'	=> __('Please provide your name'),
			'user_email.required'	=> __('Please provide a valid email address'),
			'user_email.email'		=> __('Please provide a valid email address'),
			'message.required'		=> __('Please write a message'),
		];
	}

	public function fields()
	{
		return $this->only('user_name', 'user_email', 'message');
	}

	public function speciality()
	{
		return $this->get('speciality', '');
	}

	public function authorize() {
		return ($this->input('unexpected')===null);
	}
}

<?php namespace App\Http\Requests;

class AdminRequest extends RequestBase
{
	public function authorize()
	{
		return $this->isAuthorized('administrator');
	}
}

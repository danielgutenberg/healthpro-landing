<?php namespace App\Http\Requests\Auth;

use App\Http\Requests\RequestBase;

class ResetPasswordRequest extends RequestBase
{
	protected $rules = [
		'password' => 'confirmed|required|between:5,20',
	];

	public function password()
	{
		return $this->get('password');
	}
}

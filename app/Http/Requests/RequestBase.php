<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Sentinel;

class RequestBase extends FormRequest
{
	protected $rules = [];

	/**
	 * Get the validation rules that apply to the request.
	 * @return array
	 */
	public function rules()
	{
		return $this->rules;
	}

	/**
	 * Determine if the user is authorized to make this request.
	 * By default we don't need this functionality.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	protected function isAuthorized($role)
	{
		return Sentinel::inRole($role);
	}

	public function fields()
	{
		return $this->except(['_token', 'meta', 'translation']);
	}

	public function invalidFields()
	{
		return $this->only(['_token', 'meta', 'translation']);
	}

	public function taxonomyTags($key = null, $default = null)
	{
		$tags = [];
		foreach ((array)$this->input($key,[]) as $tagId) {
			if (!is_numeric($tagId)) {
				foreach (preg_split("/\s*,\s*/", $tagId) as $tagName) {
					if (!$tagName) continue;
					$tags[] = (object)['name'=>$tagName, 'is_custom'=>true];
				}
			} else {
				$tags[] = (object)['id'=>(int)$tagId];
			}
		}
		return $tags;
	}

}

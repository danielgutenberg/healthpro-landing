<?php namespace App\Http\Requests;

class ApiRequest extends RequestBase
{
	/**
	 * Retrieve an input item from the request as array
	 *
	 * @param  string $key
	 * @param  mixed $default
	 * @param  string $delimiter
	 * @return array
	 */
	public function inputArr($key = null, $default = null, $delimiter = ',')
	{
		$value = parent::input($key);
		return $this->asArray($value, $default, $delimiter);
	}

	/**
	 * Return an value as array event if delimiter isn't specified
	 * @param $value
	 * @param $default
	 * @param string $delimiter
	 * @return array
	 */
	protected function asArray($value, $default, $delimiter = ',')
	{
		$result = $default;
		if (!empty($value)) {
			$result = explode($delimiter, $value);
			array_walk($result, 'trim');
			$result = array_unique($result);
		}

		return $result;
	}


}

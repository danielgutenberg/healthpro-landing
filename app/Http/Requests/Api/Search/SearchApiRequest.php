<?php namespace Http\Requests\Api\Search;

use App\Http\Requests\ApiRequest;

class SearchApiRequest extends ApiRequest
{
	const REQUIRED_SIGN = '!';
	const DESC_SIGN = '-';

	public function requiredServiceTags()
	{
		return $this->extractProfileServices('service', true);
	}

	public function optionalServiceTags()
	{
		return $this->extractProfileServices('service', false);
	}

	public function requiredProfileTags()
	{
		return $this->extractTags('query.tag', true);
	}

	public function optionalProfileTags()
	{
		return $this->extractTags('query.tag', false);
	}

	public function extractOrderFromSort(&$sort)
	{
		if (strpos($sort,self::DESC_SIGN) === 0) {
			$sort = substr($sort,1);
			return 'desc';
		}
		return 'asc';
	}

	private function extractProfileServices($paramRoot, $required)
	{
		$ids = $this->inputArr($paramRoot, []);
		$result = $this->extractIds($ids, $required);
		$result = array_unique($result);

		return $result;
	}

	private function extractTags($paramRoot, $required = true)
	{
		$result = [];
		$taxonomies = array_get($this->all(), $paramRoot, []);

		foreach ($taxonomies as $taxonomy => $ids) {
			$ids = $this->asArray($ids, []);
			$result = array_merge($result, $this->extractIds($ids, $required));
		}

		$result = array_unique($result);

		return $result;
	}

	private function extractIds($ids, $required = true)
	{
		$result = [];

		foreach ($ids as $tagId) {
			if (strlen($tagId) > 0) {
				$isRequired = $this->isRequired($tagId);
				$tagId = $this->removeRequiredSign($tagId);
				if ($isRequired == $required) {
					$result[] = $tagId;
				}
			}
		}
		return $result;
	}

	private function isRequired($id)
	{
		return $id[0] === self::REQUIRED_SIGN;
	}

	private function removeRequiredSign($id)
	{
		return str_replace('!', '', $id);
	}

}

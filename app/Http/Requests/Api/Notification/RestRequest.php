<?php namespace App\Http\Requests\Api\Notification;

use App\Http\Requests\RequestBase;

class RestRequest extends RequestBase
{
	public function extractApiDataFromParams()
	{
		$filters = [];
		foreach($this->get('filters',[]) as $name => $params){
			$filters[$name] = array_merge([$name],explode("|",$params));
		}
		$fields   = explode(",",$this->get('fields','*'));
		if($pagination = $this->get('pagination',null)){
			$pagination = explode("-",$pagination);
			$page     = $pagination[0];
			$perPage  = $pagination[1];
		}else{
			$page     = $this->get('page',0);
			$perPage  = $this->get('perPage',50);
		}

		$sorts    = [];

		foreach(explode(",",$this->get('sorts','-id')) as $sort){
			if(strpos($sort,"-")===0){
				$sorts[] = [substr($sort,1),'desc'];
			}else{
				$sorts[] = [$sort,'asc'];
			}
		}

		return compact('filters','fields','page','perPage','sorts');
	}
}

<?php namespace App\Http\Requests\Dashboard\Settings\Account;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Http\Requests\RequestBase;

class SaveSettingsRequest extends RequestBase
{
	protected $rules = [
		'email'			=> 'required|email|unique:users',
		'first_name'	=> 'required',
		'last_name'		=> 'required',
		'title'			=> 'required|metafieldexists:user,title'
	];

	public function __construct()
	{
		$this->rules['email'] .= ',email,' . Sentinel::check()->id;
	}

	public function fields()
	{
		return [
			'user'	=> $this->only('first_name', 'last_name', 'email'),
			'meta'	=> $this->only('title'),
		];
	}
}

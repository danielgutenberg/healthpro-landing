<?php namespace App\Http\Requests\Dashboard\Settings\Account;

use App\Http\Requests\RequestBase;

class ChangePasswordRequest extends RequestBase
{
	protected $rules = [
		'password_current'	=> 'required|passwordchecker',
		'password' 			=> 'required|confirmed|between:5,20',
	];

	public function password()
	{
		return $this->get('password');
	}

	public function credentials()
	{
		return $this->only('password');
	}
}

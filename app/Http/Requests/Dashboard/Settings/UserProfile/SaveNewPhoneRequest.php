<?php namespace App\Http\Requests\Dashboard\Settings\UserProfile;

use App\Http\Requests\RequestBase;

class SaveNewPhoneRequest extends RequestBase
{
	protected $rules = [
		'phone'			=> 'required',
		'country'		=> 'required|integer|exists:countries,id',
	];

	public function fields()
	{
		return $this->only('phone', 'country');
	}

	public function phone()
	{
		return $this->get('phone');
	}

	public function country()
	{
		return (int) $this->get('country');
	}
}

<?php namespace App\Http\Requests\Dashboard\Settings\UserProfile;

use App\Http\Requests\RequestBase;

class SaveSettingsRequest extends RequestBase
{
	protected $rules = [
		'birth_day'		=> 'required|integer',
		'birth_month'	=> 'required|integer',
		'birth_year'	=> 'required|integer',
		'gender'		=> 'required|metafieldexists:user,gender',
		'country'		=> 'required|integer|exists:countries,id',
	];

	public function fields()
	{
		return [
			'gender'	=> $this->get('gender'),
			'country'	=> $this->get('country'),
			'birthdate'	=> $this->birthdate(),
		];
	}

	public function birthdate()
	{
		$date = [];
		foreach(['year', 'month', 'day'] as $f) {
			$field = $this->get('birth_' . $f);
			if (!$field) {
				return '';
			}
			$date[] = sprintf('%02d', $field);
		}

		return implode('-', $date);
	}
}

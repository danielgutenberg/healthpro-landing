<?php namespace App\Http\Requests\Dashboard\Settings\UserProfile;

use App\Http\Requests\RequestBase;

class UploadPhotoRequest extends RequestBase
{
	protected $rules = [
		'avatar' => 'required|mimes:jpeg,png,gif',
	];

	protected $dontFlash = ['avatar'];

	public function avatar()
	{
		return $this->file('avatar');
	}
}

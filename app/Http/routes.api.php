<?php

Route::group(['prefix' => 'wizard'], function () use ($base){


//	Route::get('{wizard}/next', [
//		'uses' => 'Api\WizardApiController@next',
//		'as' => $base . '-wizard-step-next'
//	]);


	Route::get('{wizard}/{step}', [
		'uses' => 'Api\WizardApiController@wizardStep',
		'as' => $base . '-wizard-step-get'
	]);

	Route::get('{wizard}', [
		'uses' => 'Api\WizardApiController@wizard',
		'as' => $base . '-wizard-get'
	]);

	Route::post('{wizard}', [
		'uses' => 'Api\WizardApiController@storeFrontData',
		'as' => $base . '-wizard-store-front-data'
	]);


});

Route::get('wizards', [
	'uses' => 'Api\WizardApiController@startedWizards',
	'as' => $base . '-wizard-get-started'
]);

/////////////// AJAX APPOINTMENTS /////////////////////////////

Route::get('providers/{providerId}/availabilities', [
	'uses' => 'Api\AppointmentApiController@providerAvailability',
	'as' => $base . '-provider-availability-get',
]);

Route::get('appointments/{appointmentId}', [
	'uses' => 'Api\AppointmentApiController@getAppointment',
	'as' => $base . '-provider-appointment-get',
]);

Route::get('appointments/{appointmentId}/paymentOptions', [
	'uses' => 'Api\AppointmentApiController@getAppointmentPaymentOptions',
	'as' => $base . '-provider-appointment-get-payment-methods',
]);

Route::group(['prefix' => 'providers/{providerId}'], function() use ($base) {
	$base .= '-provider';

    Route::get('full', [
        'uses' => 'Api\ProviderApiController@getProviderDetails',
        'as' => $base . '-full-get',
    ]);
	Route::get('paymentOptions', [
		'uses' => 'Api\AppointmentApiController@getProviderPaymentOptions',
		'as' => $base . '-get-payment-methods',
	]);
	Route::get('appointments', [
		'uses' => 'Api\AppointmentApiController@getProviderAppointments',
		'as' => $base . '-appointments-get',
	]);
	Route::get('blocked', [
		'uses' => 'Api\AppointmentApiController@getProviderBlockedTimes',
		'as' => $base . '-blocked-time-get',
	]);
	Route::get('clients/{clientId}/appointments', [
		'uses' => 'Api\ProviderClientsApiController@getClientHistory',
		'as' => $base . '-client-history-get',
	]);
	Route::get('can-invite/{clientId}', [
		'uses' => 'Api\ProviderClientsApiController@canInviteClient',
		'as' => $base . '-can-invite-client',
	]);

	Route::get('clients/{clientId}/packages', [
		'uses' => 'Api\ProviderClientsApiController@getClientPackageHistory',
		'as' => $base . '-client-history-get',
	]);
	Route::post('availabilities', [
		'uses' => 'Api\AppointmentApiController@providerBlockOwnTime',
		'as' => $base . '-availability-block',
	]);


	Route::delete('availabilities', [
		'uses' => 'Api\AppointmentApiController@providerUnBlockOwnTime',
		'as' => $base . '-availability-unblock',
	]);

	Route::put('registrations/{registrationId}', [
		'uses' => 'Api\AppointmentApiController@providerEditBlockedTime',
		'as' => $base . '-block-edit',
	]);
	Route::put('acceptPayment', [
		'uses' => 'Api\ProviderApiController@updateProviderAcceptedMethods',
		'as' => $base . '-accept-methods-update',
	]);

});

Route::group(['prefix' => 'appointments'], function() use ($base){
	$appointment = $base . '-appointment';
	Route::get('/', [
		'uses' => 'Api\AppointmentApiController@getAppointments',
		'as' => $base . '-appointments-client-get',
	]);

	Route::post('/', [
		'uses' => 'Api\AppointmentApiController@lockAvailability',
		'as' => $appointment . '-lock-post',
	]);

	Route::delete('{appointmentId}', [
		'uses' => 'Api\AppointmentApiController@cancelAppointment',
		'as' => $appointment . '-cancel-delete',
	]);

	Route::put('{appointmentId}', [
		'uses' => 'Api\AppointmentApiController@editAppointment',
		'as' => $appointment . '-edit-put',
	]);

	Route::put('{appointmentId}/location', [
		'uses' => 'Api\AppointmentApiController@changeAppointmentLocation',
		'as' => $appointment . '-change-location-put',
	]);
});


// Profile notes group
Route::group(['prefix' => 'profiles/{profileId}/notes/{elmType}/{elmId}'], function() use ($base) {
	$base .= '-profile-notes-';
	Route::get('', [
		'uses' => 'Api\ProfileNotesApiController@getNotes',
		'as' => $base . 'get',
	]);
	Route::post('', [
		'uses' => 'Api\ProfileNotesApiController@createNote',
		'as' => $base . 'create',
	]);
	Route::put('{noteId}', [
		'uses' => 'Api\ProfileNotesApiController@updateNote',
		'as' => $base . 'update',
	]);
	Route::delete('{noteId}', [
		'uses' => 'Api\ProfileNotesApiController@deleteNote',
		'as' => $base . 'delete',
	]);
});


//////////////////// AJAX SEARCH //////////////////////

Route::get('providers/search', [
	'uses' => 'Api\SearchApiController@searchAvailableProviders',
	'as' => $base . '-search-available-providers-get',
]);

//////////////////// AJAX ORDER //////////////////////

Route::get('orders/{orderId}', [
	'uses' => 'Api\OrderApiController@getOrder',
	'as' => $base . '-order-items-get',
]);

Route::put('orders/{orderId}', [
	'uses' => 'Api\OrderApiController@addItemsToOrder',
	'as' => $base . '-order-add-items-put',
]);

Route::put('orders/{orderId}/payment', [
	'uses' => 'Api\OrderApiController@changeOrderPaymentMethod',
	'as' => $base . '-order-change-payment-method',
]);

Route::post('orders', [
	'uses' => 'Api\OrderApiController@createOrder',
	'as' => $base . '-order-create-post',
]);

Route::post('orders/{orderId}/book', [
	'uses' => 'Api\OrderApiController@bookOrder',
	'as' => $base . '-order-book-post',
]);

Route::delete('orders/{orderId}', [
	'uses' => 'Api\OrderApiController@cancelOrder',
	'as' => $base . '-order-cancel-delete',
]);

Route::get('profiles/{profileId}/transactions/', [
	'uses' => 'Api\OrderApiController@getProfileTransactions',
	'as' => $base . '-order-transaction-get',
]);

Route::get('profiles/{profileId}/billing/', [
	'uses' => 'Api\OrderApiController@getProfileBilling',
	'as' => $base . '-order-billing-get',
]);

//////////////////// AJAX PAYMENT //////////////////////

Route::group(['prefix' => 'profiles/{profileId}'], function() use ($base){
	Route::post('credit_cards', [
		'uses' => 'Api\PaymentApiController@addCreditCard',
		'as' => $base . '-credit-card-add',
	]);
	Route::post('payment_options', [
		'uses' => 'Api\PaymentApiController@savePaymentOptionToSession',
		'as' => $base . '-save-payment-option-to-session',
	]);
	Route::put('credit_cards/{creditCardId}', [
		'uses' => 'Api\PaymentApiController@updateCreditCard',
		'as' => $base . '-credit-card-update',
	]);
	Route::delete('credit_cards/{creditCardId}', [
		'uses' => 'Api\PaymentApiController@removeCreditCard',
		'as' => $base . '-credit-card-delete',
	]);
	Route::get('credit_cards', [
		'uses' => 'Api\PaymentApiController@getCreditCards',
		'as' => $base . '-credit-cards-get',
	]);
	Route::get('payment_methods', [
		'uses' => 'Api\PaymentApiController@getProfessionalsPaymentMethods',
		'as' => $base . '-payment-methods-get',
	]);
	Route::delete('paypal/{paypalId}', [
		'uses' => 'Api\PaymentApiController@removePaypal',
		'as' => $base . '-paypal-delete',
	]);
	Route::delete('stripe', [
		'uses' => 'Api\PaymentApiController@removeStripe',
		'as' => $base . '-stripe-delete',
	]);
	Route::get('credit_cards/{creditCardId}', [
		'uses' => 'Api\PaymentApiController@getCreditCard',
		'as' => $base . '-credit-card-get',
	]);
    Route::post('phones', [
        'uses'  => 'Api\PhoneApiController@addPhone',
        'as'    => $base . '-add-phone',
    ]);
    Route::post('phones/{phoneId}/verify', [
        'uses'  => 'Api\PhoneApiController@verifyPhone',
        'as'    => $base . '-verify-phone',
    ]);
    Route::post('phones/{phoneId}/resend', [
        'uses'  => 'Api\PhoneApiController@resendCode',
        'as'    => $base . '-resend-phone-code',
    ]);
	Route::put('phones/{phoneId}/', [
		'uses'  => 'Api\PhoneApiController@editPhone',
		'as'    => $base . '-edit-phone',
	]);
	Route::post('bank_accounts', [
		'uses' => 'Api\PaymentApiController@addBankAccount',
		'as' => $base . '-bank-account-add',
	]);
	Route::get('bank_accounts/{bankAccountId}', [
		'uses' => 'Api\PaymentApiController@getBankAccount',
		'as' => $base . '-bank-account-get',
	]);
	Route::get('bank_accounts', [
		'uses' => 'Api\PaymentApiController@getBankAccounts',
		'as' => $base . '-bank-accounts-get',
	]);
	Route::delete('bank_accounts/{bankAccountId}', [
		'uses' => 'Api\PaymentApiController@removeBankAccount',
		'as' => $base . '-bank-account-delete',
	]);
	Route::get('payment_account', [
		'uses' => 'Api\PaymentApiController@getAccountInfo',
		'as' => $base . '-payment-account-get',
	]);
	Route::post('credit_balance', [
		'uses' => 'Api\PaymentApiController@addCreditToProfile',
		'as' => $base . '-payment-account-credit-add-post',
	]);
	Route::get('credit_balance', [
		'uses' => 'Api\PaymentApiController@getProfileCreditBalance',
		'as' => $base . '-payment-account-credit-get',
	]);
	Route::get('payment_account/verification_status', [
		'uses' => 'Api\PaymentApiController@getAccountVerification',
		'as' => $base . '-payment-account-verification-get',
	]);
	Route::put('payment_account', [
		'uses' => 'Api\PaymentApiController@updateAccountInfo',
		'as' => $base . '-payment-account-put',
	]);
});

Route::put('appointments/{appointmentId}/confirm', [
	'uses' => 'Api\AppointmentApiController@confirmAppointment',
	'as' => $base . '-appointment-confirm-put',
]);

Route::put('appointments/{appointmentId}/mark', [
	'uses' => 'Api\AppointmentApiController@markAppointment',
	'as' => $base . '-appointment-mark-put',
]);

Route::put('appointments/{appointmentId}/paid', [
	'uses' => 'Api\AppointmentApiController@appointmentPaid',
	'as' => $base . '-appointment-paid-put',
]);

Route::put('appointments/{appointmentId}/notify', [
	'uses' => 'Api\AppointmentApiController@notifyClientAboutAppointment',
	'as' => $base . '-appointment-notify-put',
]);

///////////////GIFT CERTIFICATES //////////

Route::put('gift-certificates/{id}', [
	'uses' => 'Api\GiftCertificateController@redeem',
	'as' => $base . '-redeem-gift-certificate'
]);

Route::post('gift-certificates', [
	'uses' => 'Api\GiftCertificateController@createGiftCertificate',
	'as' => $base . '-create-gift-certificate'
]);

Route::get('gift-certificates', [
	'uses' => 'Api\GiftCertificateController@getProfileCertificates',
	'as' => $base . '-get-profile-gift-certificates'
]);

/////////////// PROFILE ///////////////////

Route::put('auth/profile/{profileId}', [
	'uses' => 'Api\ProfileApiController@setCurrentProfile',
	'as' => $base . '-profile-switch-profile',
]);

/////////////// LOCATION SERVICE ///////////////////

Route::get('core/locations/types', [
	'uses' => 'Api\LocationApiController@getLocationTypes',
	'as' => $base . '-location-types-get',
]);

Route::get('core/conversations/receivers', [
	'uses' => 'Api\ConversationApiController@searchForParticipants',
	'as' => $base . '-conversation-search-participants-get',
]);


Route::get('core/locations/countries', [
	'uses' => 'Api\LocationApiController@getCountries',
	'as' => $base . '-location-countries',
]);

Route::group(['prefix' => 'providers/{providerId}'], function() use ($base){
	Route::get('locations', [
		'uses' => 'Api\LocationApiController@getProviderLocations',
		'as' => $base . '-provider-location-get',
	]);
	Route::post('locations', [
		'uses' => 'Api\LocationApiController@addProviderLocation',
		'as' => $base . '-provider-location-add',
	]);
	Route::delete('locations/{locationId}', [
		'uses' => 'Api\LocationApiController@removeProviderLocation',
		'as' => $base . '-provider-location-remove',
	]);

	Route::put('locations/{locationId}', [
		'uses' => 'Api\LocationApiController@updateProviderLocation',
		'as' => $base . '-provider-location-update',
	]);

	Route::post('locations/{locationId}/availabilities', [
		'uses' => 'Api\LocationApiController@addLocationAvailabilities',
		'as' => $base . '-location-availabilities-add',
	]);

	Route::get('locations/{locationId}/availabilities', [
		'uses' => 'Api\LocationApiController@getLocationAvailabilities',
		'as' => $base . '-location-availabilities-get',
	]);

	Route::put('locations/{locationId}/availabilities', [
		'uses' => 'Api\LocationApiController@updateLocationAvailabilities',
		'as' => $base . '-location-availabilities-update',
	]);
});

Route::get('locations/check-radius/{locationId}/{homeLocationId}', [
	'uses' => 'Api\LocationApiController@checkHomeLocationRadius',
	'as' => $base . '-check-home-location-radius',
]);

/////////////// AJAX PROVIDER SERVICE ///////////////////

//todo rename to providers/taxonomies/tags add providers/taxonomies/:taxonomy/tags and providers/taxonomies/
Route::get('profiles/available-tags', [
	'uses' => 'Api\ProfileApiController@getProfileAvailableTags',
	'as' => $base . '-profiles-available-tags',
]);

Route::get('providers/services', [
	'uses' => 'Api\ProviderServiceApiController@getProviderServiceTypes',
	'as' => $base . '-provider-service-types',
]);

Route::get('providers/{providerId}/services', [
	'uses' => 'Api\ProviderServiceApiController@getProviderServices',
	'as' => $base . '-profile-provider-services',
]);

Route::group(['prefix' => 'providers/{providerId}'], function() use ($base){
	$base .= '-provider';
	Route::post('services', [
		'uses' => 'Api\ProviderServiceApiController@addProviderService',
		'as' => $base . '-service-add',
	]);

	Route::put('services', [
		'uses' => 'Api\ProviderServiceApiController@sortProviderServices',
		'as' => $base . '-services-sort',
	]);

	Route::put('services/{serviceId}', [
		'uses' => 'Api\ProviderServiceApiController@updateProviderService',
		'as' => $base . '-service-update',
	]);

	Route::delete('services/{serviceId}', [
		'uses' => 'Api\ProviderServiceApiController@removeProviderService',
		'as' => $base . '-service-delete',
	]);

	Route::delete('servicesessions/{serviceSessionId}', [
		'uses' => 'Api\ProviderServiceApiController@removeProviderServiceSession',
		'as' => $base . '-service-session-delete',
	]);
});

/////////////// AJAX PROVIDER CLIENTS ///////////////////
Route::group(['prefix' => 'providers/{providerId}'], function() use ($base){
	Route::get('clients', [
		'uses' => 'Api\ProviderClientsApiController@getProviderClients',
		'as' => $base . '-provider-clients-get',
	]);

	Route::get('invites', [
		'uses' => 'Api\ProviderClientsApiController@getProviderClientInvites',
		'as' => $base . '-provider-invites-get',
	]);

	Route::put('clients/{clientId}/invite', [
		'uses' => 'Api\ProviderClientsApiController@reinviteClient',
		'as' => $base . '-provider-client-reinvite',
	]);

	Route::post('clients/{clientId}/invite', [
		'uses' => 'Api\ProviderClientsApiController@inviteClient',
		'as' => $base . '-provider-client-invite',
	]);

	Route::get('clients/{clientId}/meta', [
		'uses' => 'Api\ProviderClientsApiController@getClientMeta',
		'as' => $base . '-provider-client-meta-get',
	]);

	Route::put('clients/{clientId}/meta/toggle', [
		'uses' => 'Api\ProviderClientsApiController@toggleClientMeta',
		'as' => $base . '-provider-client-meta-toggle',
	]);
});

Route::group(['prefix' => 'clients/{clientId}'], function() use ($base){
	$base .= '-client';
/////////////// AJAX CLIENT PROVIDERS ///////////////////
	Route::get('providers', [
		'uses' => 'Api\ClientProvidersApiController@getClientProviders',
		'as' => $base . '-providers-get',
	]);
	Route::get('invites', [
		'uses' => 'Api\ClientProvidersApiController@getClientInvites',
		'as' => $base . '-invites-get',
	]);
	Route::get('providers/{providerId}', [
		'uses' => 'Api\ClientProvidersApiController@getClientProvider',
		'as' => $base . '-provider-get',
	]);
	Route::get('providers/{providerId}/status', [
		'uses' => 'Api\ClientProvidersApiController@getProviderInvite',
		'as' => $base . '-provider-status',
	]);
	Route::put('providers/{providerId}', [
		'uses' => 'Api\ClientProvidersApiController@confirmClientProvider',
		'as' => $base . '-provider-confirm',
	]);
	Route::post('providers/{providerId}/add', [
		'uses' => 'Api\ClientProvidersApiController@addProvider',
		'as' => $base . '-provider-add',
	]);

/////////////// AJAX CLIENT LOCATIONS ///////////////////
	Route::post('locations', [
		'uses' => 'Api\LocationApiController@addClientLocation',
		'as' => $base . '-location-add',
	]);
	Route::put('locations/{locationId}', [
		'uses' => 'Api\LocationApiController@updateClientLocation',
		'as' => $base . '-location-update',
	]);
	Route::delete('locations/{locationId}', [
		'uses' => 'Api\LocationApiController@removeClientLocation',
		'as' => $base . '-location-remove',
	]);
	Route::get('locations', [
		'uses' => 'Api\LocationApiController@getClientLocations',
		'as' => $base . '-location-get',
	]);
});


Route::post('profiles', [
	'uses' => 'Api\ProfileApiController@createProfile',
	'as' => $base . '-profiles-create',
]);

Route::get('profiles', [
	'uses' => 'Api\ProfileApiController@getProfiles',
	'as' => $base . '-profiles-get-all-user',
]);



Route::get('profiles/professionals/search/name', [
	'uses' => 'Api\ProfileApiController@searchProfessionalByName',
	'as' => $base . '-profiles-search-professional-by-name',
]);

Route::get('coupons/{couponName}', [
	'uses' => 'Api\CouponApiController@get',
	'as' => $base . '-coupon-get',
]);


Route::group(['prefix' => 'profiles/{profileId}'], function () use ($base){


	$groupBase = $base . '-profiles-';

	Route::post('import/csv', [
		'uses' => 'Api\ProfileApiController@importCsvProviderClients',
		'as' => $groupBase . 'import-csv',
	]);

	Route::post('import/list', [
		'uses' => 'Api\ProfileApiController@importListProviderClients',
		'as' => $groupBase . 'import-list',
	]);

	Route::group(['prefix' => 'videos'], function () use ($groupBase){
		$localBase = '-asset-video';

		Route::delete('{assetId}', [
			'uses' => 'Api\ProfileApiController@deleteProfileAsset',
			'as' => $groupBase . 'delete' . $localBase,
		]);

		Route::put('{assetId}', [
			'uses' => 'Api\ProfileApiController@updateProfileAsset',
			'as' => $groupBase . 'update' . $localBase,
		]);

		Route::post('', [
			'uses' => 'Api\ProfileApiController@uploadProfileAsset',
			'as' => $groupBase . 'upload' . $localBase,
		]);

		Route::get('', [
			'uses' => 'Api\ProfileApiController@getProfileVideo',
			'as' => $groupBase . 'all-get' . $localBase,
		]);

		Route::get('{assetId}', [
			'uses' => 'Api\ProfileApiController@getProfileAsset',
			'as' => $groupBase . 'get' . $localBase,
		]);


	});

	Route::group(['prefix' => 'photos'], function () use ($groupBase){
		$localBase = '-asset-photo';

		Route::delete('{assetId}', [
			'uses' => 'Api\ProfileApiController@deleteProfileAsset',
			'as' => $groupBase . 'delete' . $localBase,
		]);

		Route::put('{assetId}', [
			'uses' => 'Api\ProfileApiController@updateProfileAsset',
			'as' => $groupBase . 'update' . $localBase,
		]);

		Route::post('', [
			'uses' => 'Api\ProfileApiController@uploadProfileAsset',
			'as' => $groupBase . 'upload' . $localBase,
		]);

		Route::get('', [
			'uses' => 'Api\ProfileApiController@getProfilePhoto',
			'as' => $groupBase . 'all-get' . $localBase,
		]);

		Route::get('{assetId}', [
			'uses' => 'Api\ProfileApiController@getProfileAsset',
			'as' => $groupBase . 'get' . $localBase,
		]);
	});

	Route::group(['prefix' => 'certificates'], function () use ($groupBase){
		$localBase = '-asset-certificate';

		Route::delete('{assetId}', [
			'uses' => 'Api\ProfileApiController@deleteProfileAsset',
			'as' => $groupBase . 'delete' . $localBase,
		]);

		Route::put('{assetId}', [
			'uses' => 'Api\ProfileApiController@updateProfileAsset',
			'as' => $groupBase . 'update' . $localBase,
		]);

		Route::post('', [
			'uses' => 'Api\ProfileApiController@uploadProfileAsset',
			'as' => $groupBase . 'upload' . $localBase,
		]);

		Route::get('', [
			'uses' => 'Api\ProfileApiController@getProfileCertificate',
			'as' => $groupBase . 'all-get' . $localBase,
		]);

		Route::get('{assetId}', [
			'uses' => 'Api\ProfileApiController@getProfileAsset',
			'as' => $groupBase . 'get' . $localBase,
		]);

	});


	Route::group(['prefix' => 'articles'], function () use ($groupBase){
		$localBase = '-asset-article';

		Route::delete('{assetId}', [
			'uses' => 'Api\ProfileApiController@deleteProfileAsset',
			'as' => $groupBase . 'delete' . $localBase,
		]);

		Route::put('{assetId}', [
			'uses' => 'Api\ProfileApiController@updateProfileAsset',
			'as' => $groupBase . 'update' . $localBase,
		]);

		Route::post('', [
			'uses' => 'Api\ProfileApiController@uploadProfileAsset',
			'as' => $groupBase . 'upload' . $localBase,
		]);

		Route::get('', [
			'uses' => 'Api\ProfileApiController@getProfileArticle',
			'as' => $groupBase . 'all-get' . $localBase,
		]);

		Route::get('{assetId}', [
			'uses' => 'Api\ProfileApiController@getProfileAsset',
			'as' => $groupBase . 'get' . $localBase,
		]);

	});

	Route::group(['prefix' => 'podcasts'], function () use ($groupBase){
		$localBase = '-asset-podcast';

		Route::delete('{assetId}', [
			'uses' => 'Api\ProfileApiController@deleteProfileAsset',
			'as' => $groupBase . 'delete' . $localBase,
		]);

		Route::put('{assetId}', [
			'uses' => 'Api\ProfileApiController@updateProfileAsset',
			'as' => $groupBase . 'update' . $localBase,
		]);

		Route::post('', [
			'uses' => 'Api\ProfileApiController@uploadProfileAsset',
			'as' => $groupBase . 'upload' . $localBase,
		]);

		Route::get('', [
			'uses' => 'Api\ProfileApiController@getProfilePodcast',
			'as' => $groupBase . 'all-get' . $localBase,
		]);

		Route::get('{assetId}', [
			'uses' => 'Api\ProfileApiController@getProfileAsset',
			'as' => $groupBase . 'get' . $localBase,
		]);

	});

	Route::group(['prefix' => 'files'], function () use ($groupBase){

		Route::post('', [
			'uses' => 'Api\ProfileApiController@uploadFileToProfile',
			'as' => $groupBase . 'upload-file',
		]);

		Route::delete('{fileId}', [
			'uses' => 'Api\ProfileApiController@deleteFileFromProfile',
			'as' => $groupBase . 'delete-file',
		]);
	});

	Route::put('', [
		'uses' => 'Api\ProfileApiController@updateProfile',
		'as' => $groupBase . 'update',
	]);

	//todo, switch order of these: profiles/{id} should return simple
	Route::get('simple', [
		'uses' => 'Api\ProfileApiController@getProfile',
		'as' => $groupBase . 'get-simple',
	]);

	Route::get('billing_details', [
		'uses' => 'Api\ProfileApiController@getBillingDetails',
		'as' => $groupBase . 'billing-details-get',
	]);

	Route::put('billing_details', [
		'uses' => 'Api\ProfileApiController@setBillingDetails',
		'as' => $groupBase . 'billing-details-update',
	]);
});

Route::get('profiles/{profileId}', [
	'uses' => 'Api\ProfileApiController@getProfilePublicDetails',
	'as' => $base . '-profiles-get-detailed',
]);

/////////////// AJAX EMAIL VERIFICATION ///////////////////

Route::get('profiles/exists/{profileType}/{email}', [
	'uses' => 'Api\ProfileApiController@emailExists',
	'as' => $base . 'profile-exists',
]);

/////////////// AJAX PROVIDER VERIFICATION ///////////////////

Route::post('providers/{providerId}/verification', [
	'uses' => 'Api\ProviderVerificationApiController@verification',
	'as' => $base . '-provider-verification',
]);

/////////////// AJAX PROVIDER REVIEWS ///////////////////
Route::group(['prefix' => 'providers/{providerId}/reviews'], function () use ($base) {
    $base .= '-provider-review';
    Route::get('', [
        'uses' => 'Api\ProviderReviewApiController@getProviderReviews',
        'as' => $base . 's',
    ]);
    Route::post('', [
        'uses' => 'Api\ProviderReviewApiController@createProviderReview',
        'as' => $base . '-add',
    ]);
    Route::put('{reviewId}', [
        'uses' => 'Api\ProviderReviewApiController@updateProviderReview',
        'as' => $base . '-update',
    ]);
    Route::delete('{reviewId}', [
        'uses' => 'Api\ProviderReviewApiController@deleteProviderReview',
        'as' => $base . '-delete',
    ]);

    Route::get('{reviewId}/ratings', [
        'uses' => 'Api\ProviderReviewApiController@getProviderReviewRatings',
        'as' => $base . '-ratings',
    ]);
    Route::post('{reviewId}/ratings', [
        'uses' => 'Api\ProviderReviewApiController@setProviderReviewRatings',
        'as' => $base . '-rating-add',
    ]);
    Route::put('{reviewId}/ratings/{ratingId}', [
        'uses' => 'Api\ProviderReviewApiController@updateProviderReviewRating',
        'as' => $base . '-rating-update',
    ]);
    Route::delete('{reviewId}/ratings/{ratingId}', [
        'uses' => 'Api\ProviderReviewApiController@deleteProviderReviewRating',
        'as' => $base . '-rating-delete',
    ]);

    Route::get('{reviewId}/helpful', [
        'uses' => 'Api\ProviderReviewApiController@getProviderReviewsHelpfulValue',
        'as' => $base . '-helpful',
    ]);
    Route::post('{reviewId}/helpful', [
        'uses' => 'Api\ProviderReviewApiController@setProviderReviewsHelpfulValue',
        'as' => $base . '-helpful-set',
    ]);
    Route::delete('{reviewId}/helpful', [
        'uses' => 'Api\ProviderReviewApiController@unsetProviderReviewHelpfulRating',
        'as' => $base . '-helpful-unset',
    ]);
});

//////////// PROVIDER MEMBERSHIP ///////////////
Route::group(['prefix' => 'providers/{providerId}/membership'], function () use ($base) {
    $base .= '-provider-membership';

    Route::get('', [
        'uses' => 'Api\ProviderMembershipApiController@membershipDetails',
        'as' => $base . '-details',
    ]);
    Route::get('estimate', [
        'uses' => 'Api\ProviderMembershipApiController@billingEstimate',
        'as' => $base . '-estimate',
    ]);
	Route::put('', [
		'uses' => 'Api\ProviderMembershipApiController@editProviderPlan',
		'as' => $base . '-edit',
	]);
	Route::put('change', [
		'uses' => 'Api\ProviderMembershipApiController@changeProviderPlan',
		'as' => $base . '-change',
	]);
	Route::delete('', [
		'uses' => 'Api\ProviderMembershipApiController@terminateProviderPlan',
		'as' => $base . '-terminate',
	]);
	Route::put('revive', [
		'uses' => 'Api\ProviderMembershipApiController@reviveProviderPlan',
		'as' => $base . '-revive',
	]);
	Route::put('cancel', [
		'uses' => 'Api\ProviderMembershipApiController@cancelProviderPlan',
		'as' => $base . '-cancel',
	]);
});

/////////// AJAX SIGN UP – AUTH ///////////////
Route::post('auth/register', array(
	'uses' => 'Api\AuthApiController@signUp',
	'as' => $base . '-auth-sign-up',
));

Route::post('auth/login', array(
	'uses' => 'Api\AuthApiController@signIn',
	'as' => $base . '-auth-login',
));

Route::post('auth/reset', array(
	'uses' => 'Api\AuthApiController@resetPassword',
	'as' => $base . '-auth-reset-password',
));

Route::post('auth/set_password', array(
	'uses' => 'Api\AuthApiController@setNewPassword',
	'as' => $base . '-auth-set-new-password',
));

Route::get('auth/logout', array(
	'uses' => 'Api\AuthApiController@logout',
	'as' => $base . '-auth-logout',
));

Route::post('auth/send-activation', array(
	'uses' => 'Api\AuthApiController@sendActivation',
	'as' => $base . '-auth-send-activation',
));



/////////////// STRIPE WEBHOOK LISTENER ///////////////

Route::post('stripe/webhook', [
	'uses' => 'Api\PaymentApiController@stripeWebhook',
	'as' => $base . '-payment-stripe-webhook',
]);

/*
 * refresh session
 */
if ($base == 'ajax') {
	Route::get('auth/caffeine', array(

		'uses' => 'Api\AuthApiController@refreshSession',
		'as' => $base . '-auth-caffeine',
	));
}

/*
 * User Routes
 */

Route::get('/user/me', [
	'uses' => 'Api\UserApiController@get',
	'as' => $base . '-user-get',
]);
Route::put('/user/me', [
	'uses' => 'Api\UserApiController@update',
	'as' => $base . '-user-update-put',
]);

//////////////////////////////////////////////

Route::get('core/taxonomy/{taxonomySlug}/tags', [
	'uses' => 'Api\TaxonomyApiController@getTags',
	'as' => $base . '-core-taxonomy-tags',
]);

Route::get('core/taxonomy/{taxonomySlug}/{tagSlug}/tags', [
	'uses' => 'Api\TagApiController@getChildTags',
	'as' => $base . '-core-taxonomy-tags-childs',
])->where('tagSlug', '(\/?[\w\-]+)+');

Route::get('core/educations/search', [
	'uses' => 'Api\EducationApiController@searchEducations',
	'as' => $base . '-core-educations-search',
]);

Route::get('core/locations/search', [
	'uses' => 'Api\LocationApiController@searchLocations',
	'as' => $base . '-core-locations-search',
]);

Route::get('core/services/search', [
	'uses' => 'Api\SearchApiController@searchProviderServicesByTag',
	'as' => $base . '-core-services-search',
]);

/// core profiles

Route::get('core/profile/slug/{slug}', [
	'uses' => 'Api\ProfileApiController@checkSlug',
	'as' => $base . '-profile-check-slug',
]);


//Questionnaire routes
Route::group(['prefix' => 'questionnaire'], function (){
	Route::get('{questionnaireId}', [
		'uses' => 'Api\QuestionnaireApiController@getQuestionnaire',
		'as' => 'api-questionnaire-get',
	]);

	Route::get('results/{questionnaireId}/owner-{ownerId}', [
		'uses' => 'Api\QuestionnaireApiController@getQuestionnaireResultByOwner',
		'as' => 'api-questionnaire-results-by-owner',
	]);

	Route::post('/', [
		'uses' => 'Api\QuestionnaireApiController@setQuestionnaire',
		'as' => 'api-questionnaire-create',
	]);

	Route::put('{questionnaireId}', [
		'uses' => 'Api\QuestionnaireApiController@updateQuestionnaire',
		'as' => 'api-questionnaire-update',
	]);

	Route::put('questions/{questionnaireId}', [
		'uses' => 'Api\QuestionnaireApiController@setTemplate',
		'as' => 'api-questionnaire-template-update',
	]);

	Route::get('copy/{questionnaireId}/owner-{ownerProfileId}', [
		'uses' => 'Api\QuestionnaireApiController@copyQuestionnaire',
		'as' => 'api-questionnaire-copy',
	]);


	Route::get('results/{questionnaireId}/paginate-{page}-{perPage}', [
		'uses' => 'Api\QuestionnaireApiController@paginateResults',
		'as' => 'api-questionnaire-results-paginate',
	]);

	Route::put('result/{questionnaireId}', [
		'uses' => 'Api\QuestionnaireApiController@setResult',
		'as' => 'api-questionnaire-result-update',
	]);

	Route::get('search/owner-{profileId}/{term}/paginate-{page}-{perPage}', [
		'uses' => 'Api\QuestionnaireApiController@searchMyQuestionnaire',
		'as' => 'api-questionnaire-search-my',
	]);

	Route::get('search/{term}/paginate-{page}-{perPage}', [
		'uses' => 'Api\QuestionnaireApiController@searchPublicQuestionnaire',
		'as' => 'api-questionnaire-search-public',
	]);


	Route::delete('/{questionnaireId}', [
		'uses' => 'Api\QuestionnaireApiController@removeQuestionnaire',
		'as' => 'api-questionnaire-delete',
	]);

});

/////////// NOTIFICATIONS SERVICE ///////////////
Route::group(['prefix' => 'notifications'], function() use ($base){
	$base .= '-notifications';
	Route::get('', [
		'uses' => 'Api\NotificationApiController@getList',
		'as' => $base . '-get-list',
	]);

	Route::post('', [
		'uses' => 'Api\NotificationApiController@createNew',
		'as' => $base . '-create-new',
	]);

//disabled
//Route::put('', [
//	'uses'  => 'Api\NotificationApiController@updateListPartial',
//	'as'    => $base.'-update-list-partial',
//]);

//scary stuff
//Route::delete('', [
//	'uses'  => 'Api\NotificationApiController@deleteList',
//	'as'    => $base.'-delete-list',
//]);


	Route::get('me/inbox', [
		'uses' => 'Api\NotificationApiController@getMyInbox',
		'as' => $base . '-get-my-inbox',
	]);

	Route::get('me/outbox', [
		'uses' => 'Api\NotificationApiController@getMyOutbox',
		'as' => $base . '-get-my-outbox',
	]);

	Route::put('me/inbox/read', [
		'uses' => 'Api\NotificationApiController@readMyInboxList',
		'as' => $base . '-read-my-inbox-list',
	]);

	Route::put('{profileId}/inbox/read', [
		'uses' => 'Api\NotificationApiController@readInboxList',
		'as' => $base . '-read-inbox-list',
	]);
/////////////////////////////////////////////////

	Route::get('me/inbox/{notificationId}', [
		'uses' => 'Api\NotificationApiController@getMyInboxSingle',
		'as' => $base . '-get-my-inbox-single',
	]);

	Route::get('me/outbox/{notificationId}', [
		'uses' => 'Api\NotificationApiController@getMyOutboxSingle',
		'as' => $base . '-get-my-outbox-single',
	]);

	Route::get('{profileId}/inbox/{notificationId}', [
		'uses' => 'Api\NotificationApiController@getInboxSingle',
		'as' => $base . '-get-inbox-single',
	]);

	Route::get('{profileId}/outbox/{notificationId}', [
		'uses' => 'Api\NotificationApiController@getOutboxSingle',
		'as' => $base . '-get-outbox-single',
	]);

//Route::put('me/inbox/{notificationId}', [
//	'uses'  => 'Api\NotificationApiController@updateMyInboxSingle',
//	'as'    => $base.'-update-my-inbox-single',
//]);

//Route::put('me/outbox/{notificationId}', [
//	'uses'  => 'Api\NotificationApiController@updateMyOutboxSingle',
//	'as'    => $base.'-update-my-outbox-single',
//]);

	Route::put('me/inbox/{notificationId}/read', [
		'uses' => 'Api\NotificationApiController@readMyInboxSingle',
		'as' => $base . '-read-my-inbox-single',
	]);

// temporarily disabled
//Route::delete('notifications/me/inbox/{notificationId}', [
//	'uses'  => 'Api\NotificationApiController@deleteMyInboxSingle',
//	'as'    => $base.'-notifications-delete-my-inbox-single',
//]);
//
// temporarily disabled
//Route::delete('notifications/me/outbox/{notificationId}', [
//	'uses'  => 'Api\NotificationApiController@deleteMyOutboxSingle',
//	'as'    => $base.'-notifications-delete-my-outbox-single',
//]);

	Route::put('{profileId}/inbox/{notificationId}/read', [
		'uses' => 'Api\NotificationApiController@readSingle',
		'as' => $base . '-read-single',
	]);

	Route::get('{profileId}/inbox', [
		'uses' => 'Api\NotificationApiController@getInbox',
		'as' => $base . '-get-inbox',
	]);

	Route::get('{profileId}/outbox', [
		'uses' => 'Api\NotificationApiController@getOutbox',
		'as' => $base . '-get-outbox',
	]);

	Route::get('{notificationId}', [
		'uses' => 'Api\NotificationApiController@getDetails',
		'as' => $base . '-get-details',
	]);

// temporarily disabled
//Route::post('{notificationId}', [
//	'uses'  => 'Api\NotificationApiController@updateCreate',
//	'as'    => $base.'-update-create',
//]);

	Route::put('{notificationId}', [
		'uses' => 'Api\NotificationApiController@updateSingle',
		'as' => $base . '-update-single',
	]);

	Route::delete('{notificationId}', [
		'uses' => 'Api\NotificationApiController@deleteSingle',
		'as' => $base . '-delete-single',
	]);
});

/////////// NOTIFICATIONS SERVICE END ///////////////

Route::group(['prefix' => 'conversations'], function (){

	Route::post('', [
		'uses' => 'Api\ConversationApiController@createConversation',
		'as' => 'api-conversation-create-post',
	]);

	Route::get('', [
		'uses' => 'Api\ConversationApiController@getConversations',
		'as' => 'api-conversations-get',
	]);

	Route::get('{conversationId}', [
		'uses' => 'Api\ConversationApiController@getConversation',
		'as' => 'api-conversation-get',
	]);

	Route::delete('{conversationId}', [
		'uses' => 'Api\ConversationApiController@deleteConversation',
		'as' => 'api-conversation-delete',
	]);

	Route::post('{conversationId}', [
		'uses' => 'Api\ConversationApiController@addMessageToConversation',
		'as' => 'api-conversation-add-message-post',
	]);
	Route::put('{conversationId}', [
		'uses' => 'Api\ConversationApiController@markMessageAsRead',
		'as' => 'api-conversation-mark-message-as-read-put',
	]);
});

/*
 * Classes Routes
 */

Route::get('provider/{providerId}/classes', [
	'uses' => 'Api\ClassesApiController@getClasses',
	'as' => 'api-profile-classes-get',
]);


Route::get('providers/{idOrSlug}', [
	'uses' => 'Api\ProfileApiController@getProvider',
	'as' => $base . '-profile-provider-get-summary',
]);

Route::group(['prefix' => 'classes'], function (){

	Route::post('', [
		'uses' => 'Api\ClassesApiController@createClass',
		'as' => 'api-profile-class-create',
	]);

	Route::post('{classId}/packages', [
		'uses' => 'Api\ClassesApiController@addClassesPricePackage',
		'as' => 'api-classes-price-package-add',
	]);

	Route::put('{classId}/packages/{packageId}', [
		'uses' => 'Api\ClassesApiController@updateClassesPricePackage',
		'as' => 'api-classes-price-package-update',
	]);

	Route::delete('{classId}/packages/{packageId}', [
		'uses' => 'Api\ClassesApiController@deleteClassesPricePackage',
		'as' => 'api-classes-price-package-delete',
	]);

	Route::get('{classId}/packages/{packageId}', [
		'uses' => 'Api\ClassesApiController@getClassesPricePackage',
		'as' => 'api-classes-price-package-get',
	]);


	Route::group(['prefix' => '{classId}/asset'], function (){

		Route::post('', [
			'uses' => 'Api\ClassesApiController@uploadClassAsset',
			'as' => 'api-profile-class-asset-upload',
		]);

		Route::put('{id}', [
			'uses' => 'Api\ClassesApiController@updateClassAsset',
			'as' => 'api-profile-class-asset-update',
		]);

		Route::delete('{id}', [
			'uses' => 'Api\ClassesApiController@deleteClassAsset',
			'as' => 'api-profile-class-asset-delete',
		]);

	});

	Route::group(['prefix' => '{classId}/schedule'], function (){

		Route::post('', [
			'uses' => 'Api\ClassesApiController@addClassAvailability',
			'as' => 'api-profile-class-schedule-add',
		]);

		Route::put('{id}', [
			'uses' => 'Api\ClassesApiController@updateClassAvailability',
			'as' => 'api-profile-class-schedule-update',
		]);

		Route::delete('{id}', [
			'uses' => 'Api\ClassesApiController@deleteClassAvailability',
			'as' => 'api-profile-class-schedule-delete',
		]);

	});

	Route::get('{classId}/registrations/{date}}', [
		'uses' => 'Api\ClassesApiController@getClassRegistrationsOnDate',
		'as' => 'api-profile-classes-get-registrations',
	]);

	Route::put('{classId}/tags', [
		'uses' => 'Api\ClassesApiController@associateTagsWithClass',
		'as' => 'api-profile-class-tags-associate',
	]);

	Route::delete('{id}', [
		'uses' => 'Api\ClassesApiController@deleteClass',
		'as' => 'api-profile-class-delete',
	]);

	Route::put('{id}', [
		'uses' => 'Api\ClassesApiController@updateClass',
		'as' => 'api-profile-class-update',
	]);

	Route::get('{id}', [
		'uses' => 'Api\ClassesApiController@getClass',
		'as' => 'api-profile-class-get',
	]);

});

//// CLIENTS Сlasses ////

Route::post('clients/{clientId}/classes/{classId}', [
	'uses' => 'Api\ClassesApiController@bookClassesAvailability',
	'as' => 'api-client-classes-book',
]);


Route::DELETE('clients/{clientId}/classes', [
	'uses' => 'Api\ClassesApiController@cancelMultipleClassRegistration',
	'as' => 'api-client-class-cancel-multiply',
]);


Route::get('clients/{clientId}/classes/{classId}/credits', [
	'uses' => 'Api\ClassesApiController@getClassesChargePricePackage',
	'as' => 'api-classes-price-package-charge-get',
]);

Route::get('clients/{clientId}/classes', [
	'uses' => 'Api\ClassesApiController@getUpcomingClasses',
	'as' => 'api-client-class-upcoming',
]);


////////////////


Route::group(['prefix' => 'rating/{entityType}/{entityId}'], function (){

	Route::get('overall/{ratingType}', [
		'uses' => 'Api\RatingApiController@getOverallEntityRatings',
		'as' => 'api-rating-get-overall',
	]);

	Route::get('', [
		'uses' => 'Api\RatingApiController@getEntityRatings',
		'as' => 'api-rating-get-ratings',
	]);

	Route::post('', [
		'uses' => 'Api\RatingApiController@rateEntityRatings',
		'as' => 'api-rating-rate-ratings',
	]);


	Route::get('{ratingLabel}', [
		'uses' => 'Api\RatingApiController@getEntityRating',
		'as' => 'api-rating-get',
	]);

	Route::post('{ratingLabel}', [
		'uses' => 'Api\RatingApiController@rateEntity',
		'as' => 'api-rating-rate',
	]);
});

// PRICE PACKAGE ///
Route::group(['prefix' => 'packages'], function () {

	Route::get('', [
		'uses' => 'Api\PricePackageApiController@getPurchasedPricePackages',
		'as' => 'api-purchased-packages',
	]);

	Route::get('{id}', [
		'uses' => 'Api\PricePackageApiController@getPurchasedPricePackage',
		'as' => 'api-purchased-packages-get',
	]);

	Route::put('{id}/schedule/{scheduleId}', [
		'uses' => 'Api\PricePackageApiController@updateSchedule',
		'as' => 'api-purchased-packages-update-schedule',
	]);

	Route::post('{id}/schedule', [
		'uses' => 'Api\PricePackageApiController@addSchedule',
		'as' => 'api-purchased-packages-add-schedule',
	]);

	Route::put('{id}/status', [
		'uses' => 'Api\PricePackageApiController@updateAutoRenew',
		'as' => 'api-purchased-packages-update-auto-renew',
	]);

	Route::put('{id}/active', [
		'uses' => 'Api\PricePackageApiController@cancelPackage',
		'as' => 'api-purchased-packages-cancel-package',
	]);


	Route::get('{id}/can-purchase', [
		'uses' => 'Api\PricePackageApiController@canPurchasePackage',
		'as' => 'api-purchased-packages-can-purchase-package',
	]);

});

Route::group(['prefix' => 'profiles/{profileId}/packages'], function () use ($base){
	$base .= '-price-package';
	Route::post('', [
		'uses' => 'Api\PricePackageApiController@addPricePackage',
		'as' => $base . '-add',
	]);
//	Route::get('entity/{entityId}/{entityType}', [
//		'uses' => 'Api\PricePackageApiController@getEntityPricePackages',
//		'as' => $base . '-entity-packages',
//	]);
	Route::get('', [
		'uses' => 'Api\PricePackageApiController@getProfilePricePackages',
		'as' => $base . '-profile-packages',
	]);
	Route::get('{id}', [
		'uses' => 'Api\PricePackageApiController@getPricePackage',
		'as' => $base . '-get',
	]);
	Route::get('credits/{entityType}/{entityId}', [
		'uses' => 'Api\PricePackageApiController@getChargePricePackage',
		'as' => 'api-charge-price-package-get',
	]);
	Route::put('{id}', [
		'uses' => 'Api\PricePackageApiController@updatePricePackage',
		'as' => $base . '-update',
	]);
	Route::delete('{id}', [
		'uses' => 'Api\PricePackageApiController@deletePricePackage',
		'as' => $base . '-delete',
	]);
});

Route::group(['prefix' => 'schedule'], function () use ($base){
	Route::put('calendar/disconnect/{calendarId}', [
		'uses' => 'Api\ProfileCalendarApiController@disconnect',
		'as' => $base . '-calendar-disconnect',
	]);
	Route::post('calendar/connect/{remoteId}', [
		'uses' => 'Api\ProfileCalendarApiController@connect',
		'as' => $base . '-calendar-connect',
	]);
	Route::put('calendar/mark/{calendarId}', [
		'uses' => 'Api\ProfileCalendarApiController@mark',
		'as' => $base . '-calendar-mark',
	]);
	Route::get('calendar/calendars', [
		'uses' => 'Api\ProfileCalendarApiController@calendars',
		'as' => $base . '-calendar-calendars',
	]);
});

Route::group(['prefix' => 'oauth/{oauthId}'], function() use ($base) {
    $base .= '-oauth';
    Route::put('disconnect', [
        'uses' => 'Api\OAuthApiController@disconnect',
        'as' => $base . '-disconnect',
    ]);
});

Route::group(['prefix' => 'cs-requests'], function() use ($base) {
	Route::post('add-service-request', [
		'uses' => 'Api\CustomerServiceRequestsController@addServiceRequest',
		'as' => $base . '-add-service-request',
	]);
	Route::post('demo-request', [
		'uses' => 'Api\CustomerServiceRequestsController@demoRequest',
		'as' => $base . '-demo-request',
	]);
	Route::post('partner-request', [
		'uses' => 'Api\CustomerServiceRequestsController@partnerRequest',
		'as' => $base . '-partner-request',
	]);
	Route::post('affiliate-request', [
		'uses' => 'Api\CustomerServiceRequestsController@affiliateRequest',
		'as' => $base . '-affiliate-request',
	]);
	Route::post('contact', [
		'uses' => 'Api\CustomerServiceRequestsController@contactRequest',
		'as' => $base . '-contact-request',
	]);
	Route::post('newsletter', [
		'uses' => 'Api\CustomerServiceRequestsController@newsletterRequest',
		'as' => $base . '-newsletter-request',
	]);
	Route::post('submit-a-question', [
		'uses' => 'Api\CustomerServiceRequestsController@submitAQuestion',
		'as' => $base . '-submit-question',
	]);
});

Route::get('pages/{slug}/{lang?}', [
	'uses' => 'Api\PagesApiController@getPage',
	'as' => $base . '-page-get',
]);

Route::post('paypal/process', array(
	'uses' => 'Api\PaymentApiController@paypal',
	'as' => 'paypal-complete-payment'
));

Route::get('paypal/complete', 'Api\PaymentApiController@completePaypal');

Route::get('paypal/access', 'Api\PaymentApiController@paypalAccount');

Route::group(['prefix' => 'payments'], function () use ($base) {
    $base .= '-payments';
    Route::put('refund/{operationId}', [
        'uses' => 'Api\PaymentApiController@refundOperation',
        'as' => $base . '-refund',
    ]);
});

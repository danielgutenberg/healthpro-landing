<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
use Illuminate\Support\Facades\Route;
Route::model('user',    'WL\Modules\User\Models\User');
Route::model('comment', 'Comment');
Route::model('post',    'Post');
Route::model('roles',   'WL\Modules\Role\Models\Role');
Route::model('certifications',   'WL\Modules\Certification\Models\Certification');
Route::model('certifications',   \WL\Modules\Coupons\Models\Coupon::class);
Route::model('educations',   \WL\Modules\Education\Models\Education::class);
Route::model('page',    'WL\Modules\Page\Models\Page');
Route::model('option',  'WL\Settings\Models\Option');


/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('comment', '[0-9]+');
Route::pattern('post',    '[0-9]+');
Route::pattern('user',    '[0-9]+');
Route::pattern('roles',   '[0-9]+');
Route::pattern('client',   '[0-9]+');
Route::pattern('certifications',   '[0-9]+');
Route::pattern('coupon',   '[0-9]+');
Route::pattern('reviews-comments',   '[0-9]+');
Route::pattern('educations',   '[0-9]+');
Route::pattern('groups',   '[0-9]+');
Route::pattern('token',   '[0-9a-z]+');
Route::pattern('page',    '[0-9]+');


/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'admin', 'middleware' => ['admin','force.https']), function()
{
include 'routes.admin.php';
});


Route::any('/test', array('as' => 'test', function(\Illuminate\Http\Request $request) {
	if( $request->isMethod('GET') ) {
		return 'content wizard';
	}

	return json_encode(
		[
			'status' => 'success',
			'content' => 'success',
		]
	);
}));


/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */

/**
 * this is not the best implementation but right now
 * there's no way to do it differently
 */
Route::group(array('before' => 'TranslatorRedirectFilter'), function() {

	Route::get('referral/{code}/', [
		'as'   => 'confirm.referral',
		'uses' => 'Referral\ReferralController@confirm',
	]);

	Route::get('/invite/{code}', [
		'uses'  => 'Site\ConfirmationController@confirmEmailInvite',
		'as'    => 'user-confirm-invite',
	]);


	Route::group(['prefix' => 'wizard'], function() {
		Route::get('continue/{wizard}', ['as' => 'wizard_continue', 'uses' => 'Site\WizardController@continueWizard']);
		Route::get('{wizard}/{step}', ['as' => 'wizard_route', 'uses' => 'Site\WizardController@renderWizard']);
	});


	Route::get('activation/send', [
		'as'   => 'send_activation',
		'uses' => 'Auth\AuthController@sendActivation',
	]);

	Route::get('auth/paypal/register', [
		'as'   => 'signup-paypal-redirect',
		'uses' => 'Auth\AuthController@getPaypalRegister',
	]);

	Route::get('paypal/fail', [
		'as'   => 'signup-paypal-fail',
		'uses' => 'Auth\AuthController@getPaypalFailAuthorize',
	]);

	Route::get('paypal/account', [
		'as'   => 'signup-paypal-connect-account',
		'uses' => 'Auth\AuthController@paypalAccountConfirm',
	]);

	Route::get('activate/{code}/{address}/{hash}', [
		'as'   => 'activate_user',
		'uses' => 'Auth\AuthController@activate',
	]);

	Route::get('activate/old-professional/free-year', [
		'as'   => 'activate_free_year',
		'uses' => 'Auth\AuthController@upgradeOldProfessionals',
	]);


	Route::get('/appointment/confirm/{code}', [
		'uses'  => 'Site\ProfileController@openWizard',
		'as'    => 'appointment-confirm',
	]);

	Route::get('/appointment/cancel/{code}', [
		'uses'  => 'Site\ConfirmationController@cancelAppointmentConfirmation',
		'as'    => 'appointment-cancel',
	]);

	Route::get('/redeem-gift-certificate/{code}', [
		'uses'  => 'Site\ProfileController@redeemGiftCertificate',
		'as'    => 'redeem-gift-certificate',
	]);

	// Authorized routes
	Route::group(array('prefix' => 'auth', 'middleware' => 'auth'), function() {

		// User logout
		Route::get('logout', array(
			'uses'  => 'Auth\AuthController@getLogout',
			'as'    => 'auth-logout',
		));

		// User logout
		Route::get('switch-user/{userId}', array(
			'uses'  => 'Auth\AuthController@switchCurrentUser',
			'as'    => 'auth-switch-user',
		));

		Route::get('oauth/{provider}/authorize', array(
			'uses'  => 'Auth\OAuthController@authorize',
			'as'    => 'oauth2-authorize',
		));
		Route::get('oauth/{provider}/authenticate', [
			'uses'  => 'Auth\OAuthController@authenticate',
			'as'    => 'oauth2-authenticate',
		]);

	});

	// Push routes
	Route::group(['prefix' => 'push'], function() {
		Route::post('google/calendar', [
			'uses'  => 'Push\GoogleController@calendarPush',
			'as'    => 'google-calendar-push',
		]);
	});

	Route::group(['prefix' => 'review', 'namespace' => 'Site', 'middleware' => 'review'], function() {

		Route::get('provider/{provider}', [
			'uses' => 'ReviewController@reviewProvider',
			'as' => 'review-provider'
		]);

		Route::get('class/{class}', [
			'uses' => 'ReviewController@reviewClass',
			'as' => 'review-class'
		]);

		Route::post('rate/{type}/{entityId}', [
			'uses' => 'ReviewController@rate',
			'as' => 'review-rate-post'
		]);

		Route::get('rate/{type}/{entityId}/rating/{rating}', [
			'uses' => 'ReviewController@quickRate',
			'as' => 'review-rate-get'
		]);

		Route::post('comment/{type}/{entityId}', [
			'uses' => 'ReviewController@postReview',
			'as' => 'review-comment-post'
		]);

	});

	// Guest routes
	Route::group(array('prefix' => 'auth', 'middleware' => 'guest'), function() {

		// User create
		Route::get('register', array(
			'uses'  => 'Auth\AuthController@getCreate',
			'as'    => 'auth-register',
		));
		Route::get('register/professional', array(
			'uses'  => 'Auth\AuthController@getCreateProfessional',
			'as'    => 'auth-register-pro',
		));
		Route::get('register/client', array(
			'uses'  => 'Auth\AuthController@getCreateClient',
			'as'    => 'auth-register-client',
		));
		Route::post('register', array(
			'uses'  => 'Auth\AuthController@postCreate',
			'as'    => 'auth-sign-up-post',
		));

		// User login
		Route::get('login', array(
			'https' => isset($_ENV['FORCE_HTTPS']) && $_ENV['FORCE_HTTPS'],
			'uses'  => 'Auth\AuthController@getLogin',
			'as'    => 'auth-login',
		));
		Route::post('login', array(
			'uses'  => 'Auth\AuthController@postLogin',
			'as'    => 'auth-sign-in-post',
		));

		// User forgot password
		Route::get('reset', array(
			'uses'  => 'Auth\AuthController@getForgot',
			'as'    => 'auth-reset-password',
		));
		Route::post('forgot', array(
			'uses'  => 'Auth\AuthController@postForgot',
			'as'    => 'auth-reset-password-post',
		));
	});

	/** ------------------------------------------
	 *  Social Authorization Routes
	 *  ------------------------------------------
	 */
	Route::group(['prefix' => 'auth', 'middleware' => 'guest'], function() {

		$socials = [
			[
				'name'	=> 'Facebook',
				'slug'	=> 'facebook',
			],
			[
				'name'	=> 'Twitter',
				'slug'	=> 'twitter',
			],
			[
				'name'	=> 'Linkedin',
				'slug'	=> 'linkedin',
			],
			[
				'name'	=> 'Google',
				'slug'	=> 'google',
			],
		];

		foreach ($socials as $social) {
			Route::group(['prefix' => $social['slug']], function() use($social) {
				Route::get('authorize', array(
					'uses'  => 'Auth\\' . $social['name'] . 'Controller@getAuthorize',
					'as'    => $social['slug'] . '-authorize',
				));
				Route::get('authenticate', [
					'uses'  =>'Auth\\' . $social['name'] . 'Controller@getAuthenticate',
					'as'    => $social['slug'] . '-authenticate',
				]);
			});
		}
	});

	/** ------------------------------------------
	 *  Temporary User Settings Routes
	 *  ------------------------------------------
	 */
	// Authorized routes
	Route::group(['prefix' => 'dashboard', 'https' => isset($_ENV['FORCE_HTTPS']) && $_ENV['FORCE_HTTPS'], 'middleware' => ['auth', 'member.redirector']], function() {

		// User Profile
		Route::get('/', [
			'as'    => 'dashboard-home',
			function() {
				return redirect(route('dashboard-myprofile-aboutme'));
			}
		]);


		Route::get('register-as/{type}', [
			'uses'  => 'Dashboard\ProfilesController@getRegister',
			'as'    => 'dashboard-get-register-as',
		]);

		Route::post('register-as/{type}', [
			'uses'  => 'Dashboard\ProfilesController@postRegister',
			'as'    => 'dashboard-post-register-as',
		]);



		Route::group(['prefix' => 'my-profile'], function() {
			Route::get('/', [
				'as'    => 'dashboard-myprofile',
				function() {
					return redirect(route('dashboard-myprofile-personalinfo'));
				}
			]);

			Route::get('about-me', [
				'uses'  => 'Dashboard\MyProfileController@index',
				'as'    => 'dashboard-myprofile-aboutme',
			]);

			Route::get('personal-info', [
				'uses'  => 'Dashboard\MyProfileController@getProfileInfo',
				'as'    => 'dashboard-myprofile-personalinfo',
			]);

			Route::post('save', [
				'uses'  => 'Dashboard\MyProfileController@updateProfile',
				'as'    => 'dashboard-myprofile-update',
			]);

			Route::get('on-the-web', [
				'uses'  => 'Dashboard\MyProfileController@getProfileSocial',
				'as'    => 'dashboard-myprofile-web',
			]);

			Route::get('suitability', [
				'uses'  => 'Dashboard\MyProfileController@getProfileSuitability',
				'as'    => 'dashboard-myprofile-suitability',
			]);

			Route::get('credentials', [
				'uses'  => 'Dashboard\MyProfileController@getProfileCredentials',
				'as'    => 'dashboard-myprofile-credentials',
			]);

			Route::get('media', [
				'uses'  => 'Dashboard\MyProfileController@getProfileMedia',
				'as'    => 'dashboard-myprofile-media',
			]);
			Route::get('locations', [
				'uses'  => 'Dashboard\MyLocationsController@show',
				'as'    => 'dashboard-myprofile-locations',
			]);

			Route::get('switch-profile/{profileId}', [
				'uses'  => 'Dashboard\MyProfileController@setCurrentProfile',
				'as'    => 'dashboard-myprofile-switch-profile',
			]);

			Route::get('social-avatars-authorize/{socialProviderSlug}', [
				'uses'  => 'Dashboard\MyProfileController@getSocialAvatarsAuthorize',
				'as'    => 'dashboard-myprofile-socialavatars-authorize',
			]);
			Route::get('social-avatars-authenticate/{socialProviderSlug}', [
				'uses'  => 'Dashboard\MyProfileController@getSocialAvatarsAuthenticate',
				'as'    => 'dashboard-myprofile-socialavatars-authenticate',
			]);
			Route::get('social-avatars', [
				'uses'  => 'Dashboard\MyProfileController@getSocialAvatars',
				'as'    => 'dashboard-myprofile-socialavatars',
			]);
		});

		Route::group(['prefix' => 'schedule', 'middleware' => 'auth'], function() {
			Route::get('/', [
				'uses'  => 'Dashboard\ScheduleController@show',
				'as'    => 'dashboard-schedule',
			]);
		});

		Route::group(['prefix' => 'my-clients', 'middleware' => 'auth'], function() {
			Route::get('/', [
				'uses'  => 'Dashboard\MyClientsController@show',
				'as'    => 'dashboard-my-clients',
			]);
		});

		Route::group(['prefix' => 'my-professionals', 'middleware' => 'auth'], function() {
			Route::get('/', [
				'uses'  => 'Dashboard\MyProvidersController@show',
				'as'    => 'dashboard-my-providers',
			]);

			Route::get('/invite/{code}', [
				'uses'  => 'Site\ConfirmationController@confirmEmailInvite',
				'as'    => 'client-confirm-invite',
			]);
		});

		Route::group(['prefix' => 'conversations', 'middleware' => 'auth'], function() {
			Route::get('/', [
				'uses'  => 'Dashboard\ConversationsController@show',
				'as'    => 'dashboard-conversations',
			]);
		});

		Route::group(['prefix' => 'my-transactions', 'middleware' => 'auth'], function() {
			Route::get('/', [
				'uses'  => 'Dashboard\MyTransactionsController@completed',
				'as'    => 'dashboard-my-transactions',
			]);
			Route::get('future', [
				'uses'  => 'Dashboard\MyTransactionsController@future',
				'as'    => 'dashboard-my-transactions-future',
			]);
			Route::get('report', [
				'uses'  => 'Dashboard\MyTransactionsController@report',
				'as'    => 'dashboard-my-transactions-report',
			]);
		});

		Route::group(['prefix' => 'billing', 'middleware' => 'auth'], function() {
			Route::get('invoice/{orderId}', [
				'uses'  => 'Dashboard\MyTransactionsController@invoice',
				'as'	=> 'dashboard-billling-invoice',
			]);
			Route::get('/info', [
				'uses'  => 'Dashboard\BillingController@showInfo',
				'as'	=> 'dashboard-billing-info',
			]);
			Route::get('/history', [
				'uses'  => 'Dashboard\BillingController@showHistory',
				'as'	=> 'dashboard-billing-history',
			]);
		});

		Route::group(['prefix' => 'appointments', 'middleware' => 'auth'], function() {
			Route::get('/', [
				'uses'  => 'Dashboard\AppointmentsController@show',
				'as'    => 'dashboard-appointments',
			]);
			Route::get('/my-clients', [
				'uses'  => 'Dashboard\AppointmentsController@showMyClients',
				'as'    => 'dashboard-appointments-my-clients',
			]);
			Route::get('/my-packages', [
				'uses'  => 'Dashboard\AppointmentsController@showMyPackages',
				'as'    => 'dashboard-appointments-my-packages',
			]);

			Route::get('/setup', [
				'uses'  => 'Dashboard\AppointmentsController@showSetup',
				'as'    => 'dashboard-appointments-setup',
			]);
			Route::get('/setup/suitability-conditions', [
				'uses'  => 'Dashboard\AppointmentsController@showSuitabilityConditions',
				'as'    => 'dashboard-appointments-suitability-conditions',
			]);
			Route::get('/report', [
				'uses' => 'Dashboard\AppointmentsController@report',
				'as' => 'dashboard-appointments-report',
			]);
		});

		Route::group(['prefix' => 'my-gift-certificates', 'middleware' => 'auth'], function() {
			Route::get('/', [
				'uses' => 'Dashboard\MyGiftCertificatesController@show',
				'as' => 'dashboard-my-gift-certificates',
			]);
		});


		// SETTINGS TAB
		Route::group(['prefix' => 'settings', 'middleware' => 'auth'], function() {

			Route::get('notifications', [
				'uses'  => 'Dashboard\Settings\NotificationsController@index',
				'as'    => 'dashboard-notifications',
			]);
			Route::post('notifications', [
				'uses'  => 'Dashboard\Settings\NotificationsController@save',
				'as'    => 'dashboard-save-notifications',
			]);


			// Settings Index Page
			Route::get('/', [
				'uses'  => 'Dashboard\SettingsController@getIndex',
				'as'    => 'dashboard-settings',
			]);

			// Account
			Route::group(['prefix' => 'account'], function() {
				Route::get('/', [
					'uses'  => 'Dashboard\Settings\AccountController@getIndex',
					'as'    => 'dashboard-settings-account',
				]);
			});

			// Payments
			Route::get('payments', [
				'uses'  => 'Dashboard\Settings\PaymentsController@getIndex',
				'as'    => 'dashboard-settings-payment',
			]);

			// Sync calendar
			Route::get('calendar', [
				'uses'  => 'Dashboard\Settings\CalendarController@getIndex',
				'as'    => 'dashboard-settings-calendar',
			]);


			// User Profile
			Route::group(['prefix' => 'user-profile'], function() {
				Route::get('/', [
					'uses'  => 'Dashboard\Settings\UserProfileController@getIndex',
					'as'    => 'dashboard-settings-user-profile',
				]);
				Route::post('upload-photo', [
					'uses'  => 'Dashboard\Settings\UserProfileController@postUploadPhoto',
					'as'    => 'dashboard-settings-user-profile-upload-photo',
				]);
				Route::get('delete-photo', [
					'uses'  => 'Dashboard\Settings\UserProfileController@getDeletePhoto',
					'as'    => 'dashboard-settings-user-profile-delete-photo',
				]);
				Route::post('save-settings', [
					'uses'  => 'Dashboard\Settings\UserProfileController@postSaveSettings',
					'as'    => 'dashboard-settings-user-profile-save-settings',
				]);
				Route::post('save-phone', [
					'uses'  => 'Dashboard\Settings\UserProfileController@postSaveNewPhone',
					'as'    => 'dashboard-settings-user-profile-save-new-phone',
				]);
			});

			/*Route::get('group-profile', [
				'uses'  => 'Dashboard\Settings\GroupProfileController@getIndex',
				'as'    => 'dashboard-settings-group-profile',
			]);*/

			/*Route::get('public-profile', [
				'uses'  => 'Dashboard\Settings\PublicProfileController@getIndex',
				'as'    => 'dashboard-settings-public-profile',
			]);*/

			// User Social Provider Manipulations
			Route::group(['prefix' => 'social'], function() {
				Route::get('/', [
					'uses'  => 'Dashboard\Settings\SocialProviderController@getIndex',
					'as'    => 'dashboard-settings-social',
				]);
				Route::get('disconnect/{provider}', [
					'uses'  => 'Dashboard\Settings\SocialProviderController@getDisconnect',
					'as'    => 'dashboard-settings-social-disconnect',
				]);
				Route::get('connect/{provider}', [
					'uses'  => 'Dashboard\Settings\SocialProviderController@getConnect',
					'as'    => 'dashboard-settings-social-connect',
				]);
				Route::get('authenticate/{provider}', [
					'uses'  => 'Dashboard\Settings\SocialProviderController@getAuthenticate',
					'as'    => 'dashboard-settings-social-authenticate',
				]);
			});

		});

		/** ------------------------------------------
		 *  Import profile data from Social by socialProviderSlug
		 *  ------------------------------------------
		 */
		Route::group(['prefix' => 'import'], function() {
			Route::get('confirm', [
				'uses'  => 'Dashboard\SocialImportController@getConfirmation',
				'as'    => 'dashboard-import-confirm',
			]);
			Route::post('confirm', [
				'uses'  => 'Dashboard\SocialImportController@postConfirmation',
				'as'    => 'dashboard-import-save',
			]);
			Route::get('/{socialProviderSlug}', [
				'uses'  => 'Dashboard\SocialImportController@getAuthorize',
				'as'    => 'dashboard-import-authorize',
			]);
			Route::get('{socialProviderSlug}/authenticate', [
				'uses'  => 'Dashboard\SocialImportController@getAuthenticate',
				'as'    => 'dashboard-import-authenticate',
			]);
		});

	});

	/**
	 *
	 *  providers redirect to blog
	 *
	 */
	Route::get('providers/', [
		'as' => 'providers-blog',
		function () {
			return redirect(route('blog-index'));
		}]);


	/**
	 * Profiles Controller
	 */

	Route::group(['prefix' => 'p'], function() {

		Route::get('/me/{editMode?}', [
			'uses' => 'Site\ProfileController@me',
			'as' => 'profile-me-show',
		]);
		Route::get('/{id}', [
			'uses' => 'Site\ProfileController@show',
			'as' => 'profile-show',
		]);
        //Professional is not available page
        Route::get('not-available/{id}', [
            'uses' => 'Site\ProfileController@showNotAvailable',
            'as' => 'professional-not-available',
        ]);
	});
	/**
	 * Client Profiles Controller
	 */

	Route::group(['prefix' => 'c'], function() {

		Route::get('/me', [
			'uses' => 'Site\ProfileController@clientMe',
			'as' => 'client-profile-me-show',
		]);

		Route::get('/{id}', [
			'uses' => 'Site\ProfileController@clientShow',
			'as' => 'client-profile-show',
		]);
	});


	/** ------------------------------------------
	 *  Blog Post Routes
	 *  ------------------------------------------
	 */
	Route::group(['prefix' => 'providers/blog'], function() {


		Route::post('/image/', [
			'uses' => 'Site\BlogController@attachImage',
			'as' => 'blog-image',
		]);

		Route::get('/search/page/{page_num?}', [
			'uses' => 'Site\BlogController@search',
			'as' => 'blog-search-page',
		]);

		#Fix this so it's one route with the page or without.
		Route::get('/search/', [
			'uses' => 'Site\BlogController@search',
			'as' => 'blog-search-non-page',
		]);

		Route::get('/category/{slug}/page/{page_num}', [
			'uses' => 'Site\BlogController@categoryPosts',
			'as' => 'blog-category-page',
		]);

		Route::get('/category/{slug}', [
			'uses' => 'Site\BlogController@categoryPosts',
			'as' => 'blog-category-non-page',
		]);

		Route::get('/tag/{slug}/page/{page_num}', [
			'uses' => 'Site\BlogController@tagPosts',
			'as' => 'blog-tag-page',
		]);

		Route::get('/tag/{slug}', [
			'uses' => 'Site\BlogController@tagPosts',
			'as' => 'blog-tag-non-page',
		]);

		Route::get('/year/{year}/month/{month}/{page_num?}', [
			'uses' => 'Site\BlogController@getArchive',
			'as' => 'blog-index-archive',
		]);

		Route::get('/page/{page_num}', [
			'uses' => 'Site\BlogController@index',
			'as' => 'blog-index-page',
		]);

		Route::get('/', [
			'uses' => 'Site\BlogController@index',
			'as' => 'blog-index',
		]);


		Route::get('/feed/{page_num?}', [
			'uses' => 'Site\BlogController@feed',
			'as' => 'blog-feed',
		])->where('page_num', '[0-9]+');;


		Route::get('/{category}/{slug}', [
			'uses' => 'Site\BlogController@blogpost',
			'as' => 'blog-get',
		]);

		Route::get('/{category}/{slug}/edit', [
			'uses' => 'Site\BlogController@blogPostEdit',
			'as' => 'blog-edit',
		]);

		Route::post('/{slug}/edit', [
			'uses' => 'Site\BlogController@updateblogpost',
			'as' => 'blog-update',
		]);

		Route::post('/', [
			'uses' => 'Site\BlogController@createblogpost',
			'as' => 'blog-create',
		]);


		Route::delete('/{id}', [
			'uses' => 'Site\BlogController@deleteblogpost',
			'as' => 'blog-delete',
		]);

	});



	Route::group(['prefix' => 'comment'], function() {

		Route::post('/', [
			'uses' => 'Site\CommentController@addComment',
			'as' => 'comment-add',
		]);

		Route::delete('/{commentId}', [
			'uses' => 'Site\CommentController@removeComment',
			'as' => 'comment-remove',
		]);

		Route::put('/{commentId}/content', [
			'uses' => 'Site\CommentController@updateContent',
			'as' => 'comment-set-content',
		]);

		Route::put('/{commentId}/status', [
			'uses' => 'Site\CommentController@updateStatus',
			'as' => 'comment-set-status',
		]);

	});


	/** ------------------------------------------
	 *  API Routes
	 *  ------------------------------------------
	 */

	Route::group(['prefix' => 'api/v1', 'middleware' => ['api.auth', 'me.resolve']],function(){
		$base = 'api';
		include 'routes.api.php';
	});


	Route::group(['prefix' => 'ajax/v1', 'middleware' => ['me.resolve']], function () {
		$base = 'ajax';
		include 'routes.api.php';
	});

	/** ------------------------------------------
	 *  * Rating
	 *  ------------------------------------------
	 */
	Route::group(['prefix' => 'rating'], function() {

		Route::get('/rate/{entityType}/{entityId}/{ratingType}/{ratingLabel}/{ratingValue}', [
			'uses' => 'Rating\RatingController@rate',
			'as' => 'rate-entity',
		]);

	});


	/** ------------------------------------------
	 *  Gettext Locale Routes
	 *  ------------------------------------------
	 */
	//Route::get('set-locale/{locale}', function($locale) {
	//	// set locale
	//	Gettext::setLocale($locale);
	//
	//	// redirect to referrer or home
	//	if (Request::server('HTTP_REFERER')) {
	//		return Redirect::to($_SERVER['HTTP_REFERER']);
	//	} else {
	//		return Redirect::to('/');
	//	}
	//});
	//
	//Route::get('set-encoding/{encoding}', function($encoding) {
	//	// set encoding
	//	Gettext::setEncoding($encoding);
	//
	//	// redirect to referrer or home
	//	if (Request::server('HTTP_REFERER')) {
	//		return Redirect::to($_SERVER['HTTP_REFERER']);
	//	} else {
	//		return Redirect::to('/');
	//	}
	//});

	Route::get('/help/{sectionSlug?}/{categorySlug?}', ['as' => 'help', 'uses' => 'Site\HelpController@index']);

	Route::post('/ticket', [
		'uses' => 'TicketsController@store',
		'as' => 'tickets.store'
	]);

	/** ------------------------------------------
	 *  Landing Page
	 *  ------------------------------------------
	 */
	Route::post('/subscribe', [
		'uses' => 'Site\LandingController@postSubscribe',
		'as' => 'landing-subscribe',
	]);

	/** ------------------------------------------
	 *  Sitemaps
	 *  ------------------------------------------
	 */
	Route::get('sitemap.xml', [
		'uses' => 'SitemapsController@index',
		'as' => 'sitemap.index',
	]);
	Route::get('sitemap_{name}.xml', [
		'uses' => 'SitemapsController@page',
		'as' => 'sitemap',
	]);


	/** ------------------------------------------
	 *  Complete Booking (e.g. a professional books a client and the client needs to perform the final steps to confirm it, etc...)
	 *  ------------------------------------------
	 */
	Route::get('/complete-booking/{hash}', [
		'uses' => 'Site\HomeController@getIndex',
		'as' => 'complete.booking',
	]);

	Route::get('/stripe/authorize', [
		'uses' => 'Auth\StripeOauthController@authorize',
		'as' => 'stripe.authorize',
	]);

	/** ------------------------------------------
	 *  Open link from email (e.g. a client gets an email informing them of a new message with a link to the dashboard - ensure user is logged into correct account)
	 *  ------------------------------------------
	 */
	Route::get('/email-link/{hash}', [
		'uses' => 'Site\HomeController@openEmailLink',
		'as' => 'email-link',
	]);

	Route::get('/client-upload/{code}', [
		'uses' => 'Site\HomeController@uploadClients',
		'as' => 'marketing-client-upload',
	]);

	/** ------------------------------------------
	 *  Page and Homepage routes
	 *  IMPORTANT Please add all routes BEFORE
	 *  NO NEW ROUTES AFTER THIS LINE
	 *  ------------------------------------------
	 */

	// Search Page
	Route::get('/search', [
		'uses' => 'Site\SearchController@getIndex',
		'as' => 'search',
	]);

//	// Landing Page
//	Route::get('/landing', [
//		'uses' => 'Site\LandingController@getIndex',
//		'as' => 'landing',
//	]);

	# show the beta page index
	Route::get('/software-free-online-registration', [
		'uses' => 'Site\HomeController@getProfessionals',
		'as' => 'home',
	]);

	Route::get('/partner/{name}', [
		'uses' => 'Site\PartnerController@getPartner',
		'as' => 'partner-{name}',
	]);

	Route::get('health-professional', [
		function() {
			return redirect(route('home'));
		}
	]);

	// temp disable find a pro page
	// Route::get('/find-my-professional', [
	// 	'uses' => 'Site\HomeController@getIndex',
	// 	'as' => 'home-client',
	// ]);



	// temp how it works page
	Route::get('/how-it-works/{service?}', [
		'uses' => 'Site\PageController@getHowItWorks',
		'as' => 'how-it-works',
	]);
	// temp how it works page
	Route::get('/pricing', [
		'uses' => 'Site\PageController@getPricing',
		'as' => 'pricing',
	]);
	// temp about page
	Route::get('/about', [
		'uses' => 'Site\PageController@getAbout',
		'as' => 'about',
	]);

	// Affiliate page
	Route::get('/affiliate-program', [
		'uses' => 'Site\PageController@getAffiliate',
		'as' => 'affiliate',
	]);

	// Affiliate page
	Route::get('/contact-us', [
		'uses' => 'Site\PageController@getContactUs',
		'as' => 'contact-us',
	]);

	// Partner page
	Route::get('/partner', [
		'uses' => 'Site\PageController@getPartner',
		'as' => 'partner',
	]);

	// Facebook 'book me' button
	Route::get('/facebook-book-me-button', [
		'uses' => 'Site\PageController@getFacebookBookMe',
		'as' => 'facebook_book_me',
	]);

	// General page
	Route::get('{slug}', [
		'uses'  => 'Site\PageController@getShow',
		'as'    => 'page.show',
	])->where(['slug' => '[\w\d\-\/]+']);

	// facebook redirect annoyance that causes errors on refresh
	Route::get('/_=_', [
		function() {
			return redirect(route('home'));
		}
	]);

	// Home Page - Last route, no matches
	Route::get('/', [
		'uses' => 'Site\HomeController@getProfessionals',
		'as' => 'home',
	]);


});

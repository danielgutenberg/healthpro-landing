<?php namespace App\Commands\Exceptions;

/**
 * This is base for command exceptions
 *
 * We need this to unify possible exceptions that can be thrown by command handle method when services have issues
 * with data or internal implementation. We loose call stack here so be careful.
 *
 * Class CommandExecutionException
 * @package Commands\Exceptions
 */
class CommandExecutionException extends \Exception
{
	private $data;

	function __construct($message = null, $data = null)
	{
		$this->message = $message;
		$this->data = $data;
		parent::__construct($message);
	}

	/**
	 * @return null
	 */
	public function getData()
	{
		return $this->data;
	}
}

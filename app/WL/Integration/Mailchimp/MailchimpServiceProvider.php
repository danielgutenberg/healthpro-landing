<?php namespace WL\Integration\Mailchimp;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;
use Mailchimp;

class MailchimpServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		////$this->package('wl/mailchimp');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('mailchimp',  function() {
			$mc = new Mailchimp( Config::get('services.mailchimp.api_key'), [
				'CURLOPT_FOLLOWLOCATION' => false, // we have to keep this line - homestead throws an error because there's no isset check for this option.
			] );

			return new Mailchimper( $mc );
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('mailchimp');
	}

}

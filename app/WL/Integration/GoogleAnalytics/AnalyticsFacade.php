<?php namespace WL\Integration\GoogleAnalytics;

use Illuminate\Support\Facades\Facade;

/**
 * Class AnalyticsFacade
 * @package WL\Integration\GoogleAnalytics
 */
class AnalyticsFacade extends Facade
{
	/**
	 * facade accessor
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'analytics';
	}
}
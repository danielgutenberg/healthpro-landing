<?php namespace WL\Integration\GoogleAnalytics\Contracts;

/**
 * Interface TrackingBagInterface
 * @package WL\Integration\GoogleAnalytics\Contracts
 */
interface TrackingBagInterface
{
	/**
	 * adds a tracking
	 *
	 * @param string $tracking
	 *
	 * @return void
	 */
	public function add($tracking);

	/**
	 * returns all trackings
	 *
	 * @return array
	 */
	public function get();
}
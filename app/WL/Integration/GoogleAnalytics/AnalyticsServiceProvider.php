<?php namespace WL\Integration\GoogleAnalytics;

use Config;
use Illuminate\Support\ServiceProvider;

class AnalyticsServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		////$this->package('wl/google-analytics', null, __DIR__);
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('analytics', function () {

			//	get analytics provider name
			$provider = Config::get('google-analytics.analytics.provider');

			//	make it a class
			$providerClass = 'WL\Integration\GoogleAnalytics\Providers\\' . $provider;

			//	getting the config
			$providerConfig = [];
			if (Config::has('google-analytics.analytics.configurations.' . $provider)) {
				$providerConfig = Config::get('google-analytics.analytics.configurations.' . $provider);
			}

			//	return an instance
			return new $providerClass($providerConfig);
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}
}

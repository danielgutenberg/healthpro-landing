<?php namespace WL\Transformers;

use Illuminate\Support\Collection;

abstract class Transformer
{
	public abstract function transform($item);

	public function transformCollection($items)
	{
		if (!is_null($items)) {
			if (!is_array($items)) {
				if ($items instanceof Collection) {
					$items = $items->all();
				} else
					throw new \Exception('not array given');
			}
			return array_map([$this, 'transform'], $items);
		}

		return [];
	}

	public function transformHashedCollection($items, $pluralizeKeys = false)
	{
		if (!is_null($items)) {

			if (!is_array($items)) {
				if ($items instanceof Collection) {
					$items = $items->all();
				} else
					throw new \Exception('not array given');
			}

			$result = [];
			foreach ($items as $key => $keyItems) {
				$result[$key . ($pluralizeKeys ? 's' : '')] = $this->transformCollection($keyItems);
			}

			return $result;
		}

		return [];
	}

}

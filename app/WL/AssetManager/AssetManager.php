<?php
namespace WL\AssetManager;

use WL\AssetManager\Asset\AssetScript;
use WL\AssetManager\Asset\AssetStyle;
use WL\AssetManager\Collection\Collection;

class AssetManager
{
	const ASSETS_HEADER 	= 'header';
	const ASSETS_FOOTER 	= 'footer';

	const EXT_CSS 			= 'css';
	const EXT_JS 			= 'js';

	/**
	 * @var array
	 */
	private $js = [];

	/**
	 * @var array
	 */
	private $css = [];

	/**
	 * @var string
	 */
	private $prefix = '';

	/**
	 * @param array $assets
	 * @param string $place
	 * @throws InvalidAssetException
	 * @throws InvalidFileExtensionException
	 * @return $this
	 */
	public function add($assets, $place = self::ASSETS_HEADER)
	{
		foreach($assets as $asset) {
			$this->processAdd($asset, $place);
		}

		return $this;
	}

	/**
	 * @param string $prefix
	 * @return $this
	 */
	public function setPrefix($prefix)
	{
		$this->prefix = trim(trim($prefix, '/'));
		return $this;
	}

	/**
	 * @return bool
	 */
	public function hasPrefix()
	{
		return $this->prefix ? true : false;
	}

	/**
	 * @return string
	 */
	public function getPrefix()
	{
		return $this->prefix;
	}

	/**
	 * @return $this
	 */
	public function clearPrefix()
	{
		$this->prefix = '';

		return $this;
	}

	/**
	 * @param $asset
	 * @param array $params
	 * @param string $place
	 */
	public function addScript($asset, $params = [], $place = self::ASSETS_HEADER)
	{
		$this->addToCollection(
			new AssetScript($asset, $params, $this->getPrefix()),
			$place,
			'js'
		);

		return $this;
	}

	/**
	 * @param $asset
	 * @param array $params
	 * @param string $place
	 * @return $this
	 */
	public function addStyle($asset, $params = [], $place = self::ASSETS_HEADER)
	{
		$this->addToCollection(
			new AssetStyle($asset, $params, $this->getPrefix()),
			$place,
			'css'
		);

		return $this;
	}

	/**
	 * @param string $place
	 * @return string
	 */
	public function css($place)
	{

		if (!isset($this->css[$place])) {
			return;
		}

		$assets = [];
		foreach( $this->css[$place]->all() as $asset ) {
			$assets[] = $asset->generate();
		}

		return implode('', $assets);
	}

	/**
	 * @param string $place
	 * @return string
	 */
	public function js($place)
	{
		if (!isset($this->js[$place])) {
			return;
		}

		$assets = [];
		foreach( $this->js[$place]->all() as $asset ) {
			$assets[] = $asset->generate();
		}

		return implode('', $assets);
	}

	/**
	 * @param $asset
	 * @param string $place
	 * @throws InvalidAssetException
	 * @throws InvalidFileExtensionException
	 * @return $this
	 */
	protected function processAdd($asset, $place = self::ASSETS_HEADER)
	{
		$params = [];
		if (is_array($asset)) {
			if (!isset($asset['asset'])) {
				throw new InvalidAssetException;
			}
			if (isset($asset['params'])) {
				$params = $asset['params'];
			}
			$asset = $asset['asset'];
		}

		switch ($this->getAssetExtension($asset)) {
			case self::EXT_CSS:
				$this->addStyle($asset, $params, $place);
				break;

			case self::EXT_JS:
				$this->addScript($asset, $params, $place);
				break;

			default:
				throw new InvalidFileExtensionException;
		}

		return $this;
	}

	/**
	 * @param string $asset
	 * @return bool|string
	 */
	protected function getAssetExtension($asset)
	{
		if (preg_match("/(\.css|\/css\?)/i", $asset)) {
			return self::EXT_CSS;
		} elseif (preg_match("/(\.js|\/js\?)/i", $asset)) {
			return self::EXT_JS;
		} else {
			return false;
		}
	}

	protected function addToCollection($assetObject, $collectionName, $collectionType)
	{
		$this->collection($collectionName, $collectionType)->push($assetObject);
	}

	/**
	 * @param $collectionName
	 * @param $collectionType
	 * @return $this
	 */
	protected function collection($collectionName, $collectionType)
	{
		if (!isset($this->{$collectionType}[$collectionName])) {
			$this->{$collectionType}[$collectionName] = new Collection();
		}

		return $this->{$collectionType}[$collectionName];
	}
}

<?php
namespace WL\AssetManager\Asset;

interface AssetInterface
{
	public function generate();
}

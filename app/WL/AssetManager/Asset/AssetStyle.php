<?php
namespace WL\AssetManager\Asset;

class AssetStyle extends Asset
{
	/**
	 * Available attributes
	 *
	 * @var array
	 */
	private $attributes = ['rel', 'type', 'rev', 'target', 'media', 'hreflang', 'charset'];

	const DEFAULT_TYPE = 'text/css';

	/**
	 * Generates an asset url
	 *
	 * @return string
	 */
	public function generate()
	{
		$this->fillDefault();

		$attrs = [
			$this->generateAttribute($this->getUrl(), 'href')
		];

		foreach ($this->attributes as $attr) {
			if (isset($this->params[$attr]) && $this->params[$attr]) {
				$attrs[] = $this->generateAttribute($this->params[$attr], $attr);
			}
		}

		return '<link ' . implode(' ', $attrs) . '>';
	}

	/**
	 * Fill default values for styles
	 *
	 * @return $this
	 */
	private function fillDefault()
	{
		if (!isset($this->params['rel'])) {
			$this->params['rel'] = 'stylesheet';
		}

		if (!isset($this->params['type'])) {
			$this->params['type'] = 'text/css';
		}

		return $this;
	}
}

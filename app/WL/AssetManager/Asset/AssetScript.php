<?php
namespace WL\AssetManager\Asset;

class AssetScript extends Asset
{
	/**
	 * Available attributes
	 *
	 * @var array
	 */
	private $attributes = ['async', 'charset', 'defer', 'type'];

	/**
	 * Generates an asset url
	 *
	 * @return string
	 */
	public function generate()
	{
		$attrs = [
			$this->generateAttribute($this->getUrl(), 'src')
		];

		foreach ($this->attributes as $attr) {
			if (isset($this->params[$attr]) && $this->params[$attr]) {
				$attrs[] = $this->generateAttribute($this->params[$attr], $attr);
			}
		}

		$before = '';
		$after = '';

		if (isset($this->params['if'])) {
			$before = '<!--[if ' . $this->params['if'] . ']>';
			$after = '<![endif]-->';
		}

		return $before . '<script ' . implode(' ', $attrs) . '></script>' . $after;
	}
}

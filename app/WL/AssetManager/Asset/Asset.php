<?php
namespace WL\AssetManager\Asset;

abstract class Asset implements AssetInterface
{
	/**
	 * @var string
	 */
	protected $asset;

	/**
	 * @var array
	 */
	protected $params;
	/**
	 * @var string
	 */
	protected $prefix;

	/**
	 * @param $asset
	 * @param array $params
	 * @param string $prefix
	 */
	public function __construct($asset, array $params = [], $prefix = '')
	{
		$this->asset = trim($asset);
		$this->params = $params;
		$this->prefix = trim(trim($prefix), '/');
	}

	/**
	 * @return array
	 */
	public function getParams()
	{
		return $this->params;
	}

	/**
	 * @param array $params
	 */
	public function setParams($params)
	{
		$this->params = $params;
	}

	/**
	 * @return mixed
	 */
	public function getAsset()
	{
		return $this->asset;
	}

	/**
	 * @param mixed $asset
	 */
	public function setAsset($asset)
	{
		$this->asset = $asset;
	}

	protected function getUrl()
	{
		return preg_match('~\/\/~si', $this->asset) ?
			$this->asset :
			asset($this->prefix . '/' . trim($this->asset, '/'));
	}

	/**
	 * @param $value
	 * @param $attribute
	 * @return string
	 */
	protected function generateAttribute($value, $attribute)
	{
		return $attribute . '="' . $value . '"';
	}
}

<?php

namespace WL\AssetManager\Facades;

use Illuminate\Support\Facades\Facade;

class AssetManager extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'assetmanager';
	}
}

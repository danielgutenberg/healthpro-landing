<?php
namespace WL\AssetManager;

// include exception namespace
use Exception;

class InvalidFileExtensionException extends Exception {}

class InvalidAssetManagerMethodException extends Exception {}

class InvalidAssetException extends Exception {}



<?php
namespace WL\Yaml;

use Illuminate\Support\ServiceProvider;
use WL\Yaml\Contracts\Parsable;

class YamlServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	public function boot()
	{
		////$this->package('wl/yaml');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('yaml', function() {
			return new Parser();
		});

		$this->app->bind(Parsable::class, Parser::class);
	}


	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('yaml');
	}
}

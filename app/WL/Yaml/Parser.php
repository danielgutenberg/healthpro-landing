<?php

namespace WL\Yaml;

use File;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Parser as YamlParser;
use WL\Yaml\Contracts\Parsable;

class Parser implements Parsable
{

	private $path;

	private $parser;

	public function __construct($path = 'config')
	{
		self::setPath($path);

		$this->parser = new YamlParser();
	}

	public function parse($file)
	{
		static $result = [];

		if (empty($result[$file])) {
			// strip extension if exists
			$file = str_replace('.yaml', '', $file);

			$file = $this->path . DIRECTORY_SEPARATOR . str_replace('.', DIRECTORY_SEPARATOR, $file) . '.yaml';
			$parsed = [];
			try {
				if (File::exists($file)) {
					$parsed = $this->parser->parse(File::get($file));
				} else {
					throw new ParseException('File not found');
				}
			} catch (ParseException $e) {
				dd("Unable to parse [" . $file . "], %s", $e->getMessage());
			}

			$result[$file] = $parsed;
		}

		return $result[$file];
	}

	public function get($path, $file, $default = null)
	{
		$config = $this->parse($file);

		if (!$config) {
			return $default;
		}

		return array_get($config, $path, $default);
	}

	/**
	 * Set default parser path ..
	 *
	 * @param string $path
	 * @return $this
	 */
	public function setPath($path = 'config')
	{
		$this->path = base_path() . DIRECTORY_SEPARATOR . trim($path, DIRECTORY_SEPARATOR);

		return $this;
	}

	/**
	 * Get parser default path ..
	 *
	 * @return mixed
	 */
	public function getPath()
	{
		return $this->path;
	}
}

<?php namespace WL\Yaml\Contracts;

interface Parsable {

	/**
	 * Parse an yaml file ..
	 *
	 * @param $file
	 * @return mixed
	 */
	public function parse($file);
}

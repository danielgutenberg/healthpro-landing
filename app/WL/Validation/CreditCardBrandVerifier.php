<?php namespace WL\Validation;

use WL\Modules\Payment\Helpers\CreditCardDetector;

class CreditCardBrandVerifier implements ValidationInterface
{
	/**
	 * Validate  Credit Card brands as parameter
	 * @param $field
	 * @param $value
	 * @param array $params
	 * @return bool
	 */
	public function validate($field, $value, array $params = [])
	{
		if (count($params) > 0) {
			$brand = CreditCardDetector::getBrand($value);
			return in_array($brand, $params);
		}

		return true;
	}
}

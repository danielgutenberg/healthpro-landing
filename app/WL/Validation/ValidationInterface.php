<?php namespace WL\Validation;

interface ValidationInterface
{
	public function validate($field, $value, array $params);
}

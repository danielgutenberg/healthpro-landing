<?php namespace WL\Validation;

use WL\Payment\CreditCardDetector;

class CreditCardNumberVerifier implements ValidationInterface
{
	/**
	 * Validate Credit Card using Luhn algorithm and Credit Card brands as parameter
	 * @param $field
	 * @param $value
	 * @param array $params
	 * @return bool
	 */
	public function validate($field, $value, array $params = [])
	{
		$isValidNumber = $this->validateLuhn($value);
		if (!$isValidNumber) {
			return false;
		}

		return true;
	}

	/**
	 * Validate a card number according to the Luhn algorithm.
	 *
	 * @param  string $number The card number to validate
	 * @return boolean True if the supplied card number is valid
	 */
	private static function validateLuhn($number)
	{
		$str = '';
		foreach (array_reverse(str_split($number)) as $i => $c) {
			$str .= $i % 2 ? $c * 2 : $c;
		}

		return array_sum(str_split($str)) % 10 === 0;
	}
}

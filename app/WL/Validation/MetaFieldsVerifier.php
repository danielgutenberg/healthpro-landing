<?php namespace WL\Validation;

use WL\MetaFields\Facades\MetaFields;
use WL\Validation\Exceptions\InvalidParametersException;

class MetaFieldsVerifier implements ValidationInterface
{
	public function validate($field, $value, array $params = [])
	{
		if (count($params) < 2) {
			throw new InvalidParametersException('You need to provide two parameters when using the `metafieldexists` validation rule - model and meta names.');
		}

		list($model, $meta) = $params;

		return in_array($value, MetaFields::keys($model, $meta));
	}
}

<?php namespace WL\Validation;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Contracts\Foundation\Application;

class PasswordCheckerVerifier implements ValidationInterface
{
	/**
	 * @var App
	 */
	private $app;

	public function __construct(Application $app)
	{
		$this->app = $app;
	}

	public function validate($field, $value, array $params = [])
	{
		return $this->app['sentinel.hasher']->check($value, Sentinel::check()->getUserPassword());
	}
}

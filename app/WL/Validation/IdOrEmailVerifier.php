<?php namespace WL\Validation;

use Illuminate\Contracts\Foundation\Application;

class IdOrEmailVerifier implements ValidationInterface
{
	/**
	 * @var App
	 */
	private $app;

	public function __construct(Application $app) {
		$this->app = $app;
	}

	/**
	 * 	If is set an value than than check for video availability and for url regex ... else pass the validation ...
	 *
	 * @param $field
	 * @param $value
	 * @param array $params
	 * @return bool
	 */
	public function validate($field, $value, array $params = [])
	{
		echo 'inside IdOrEmailVerifier';
		dd($field);

		if(! $value)
			return false;

		if( filter_var($value, FILTER_VALIDATE_URL) == false)
			return false;

		if(! $video = Embed::create($value))
			return false;

		return true;
	}
}

<?php
namespace WL\Populators;

use WL\Modules\Page\Models\Page;
use Former\Facades\Former;

abstract class FormerPopulator implements Populator
{
	/**
	 * @var mixed
	 */
	protected $model;

	/**
	 * @return mixed
	 */
	public function populate()
	{
		return Former::populate( $this->getFields() );
	}

	/**
	 * @param array $fields
	 * @return mixed
	 */
	public function populateFields(array $fields)
	{
		return Former::populate( $fields );
	}

	/**
	 * @param string $field
	 * @param mixed $value
	 * @return mixed
	 */
	public function populateField( $field, $value )
	{
		return Former::populateField( $field, $value );
	}
}

<?php

namespace WL\Populators;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface Populator
 * @package WL\Populators
 */
interface Populator
{
	/**
	 * @return mixed
	 */
	public function populate();

	/**
	 * @param array $fields
	 * @return mixed
	 */
	public function populateFields(array $fields);

	/**
	 * Populate a single field
	 *
	 * @param $field
	 * @param $value
	 * @return mixed
	 */
	public function populateField( $field, $value );

	/**
	 * @return mixed
	 */
	public function getFields();
}

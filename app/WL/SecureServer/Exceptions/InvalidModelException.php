<?php namespace WL\SecureServer\Exceptions;

use Exception;
/** Used auditor when model isn't auditable
 * Class InvalidModelException
 * @package WL\SecureServer\Exceptions
 */
class InvalidModelException extends Exception {

}

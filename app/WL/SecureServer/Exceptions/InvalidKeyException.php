<?php namespace WL\SecureServer\Exceptions;

use Exception;

/** Used for SecureServer key errors
 * Class InvalidKeyException
 */
class InvalidKeyException extends Exception
{

}

<?php namespace WL\SecureServer\Exceptions;

use Exception;
/** Used auditor when event isn't valid
 * Class InvalidEventTypeException
 * @package WL\SecureServer\Exceptions
 */
class InvalidEventTypeException extends Exception{

}

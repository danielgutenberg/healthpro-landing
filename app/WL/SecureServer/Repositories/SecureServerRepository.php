<?php namespace WL\SecureServer\Repositories;

/**
 * Interface SecureServerRepository
 */
interface SecureServerRepository
{
	/** Add object to store
	 * @param mixed $data Object or primitive
	 * @param string $key your secret key
	 * @param null|string $objectType type name
	 * @return integer object id
	 */
	public function create($data, $key, $objectType = null);

	/** Update or add object to store
	 * @param integer|null $id secure object id
	 * @param mixed $data Object or primitive
	 * @param string $key 16, 24, 32 key
	 * @param null|string $objectType type name
	 * @return integer object id
	 */
	public function update($id, $data, $key, $objectType = null);

	/** Load php primitive or object from store
	 * @param integer $id secure object id
	 * @param string $key 16, 24, 32 key
	 * @return mixed
	 */
	public function get($id, $key);

	/** Count of objects in store
	 * @return mixed
	 */
	public function count();
}

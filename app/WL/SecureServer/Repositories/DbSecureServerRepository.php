<?php namespace WL\SecureServer\Repositories;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use WL\SecureServer\Auditor;
use WL\SecureServer\Exceptions\InvalidKeyException;
use WL\SecureServer\Models\SecureObject;

class DbSecureServerRepository implements SecureServerRepository
{
	private $auditor;

	function __construct()
	{
		$this->auditor = new Auditor();
	}

	public function create($data, $key, $objectType = null)
	{
		return $this->putRawData(null, $data, $objectType, $key);
	}

	public function update($id, $data, $key, $objectType = null)
	{
		return $this->putRawData($id, $data, $objectType, $key);
	}

	/** Add or update php primitive or object
	 * @param null|integer $id secure object id
	 * @param $data
	 * @param string $dataType data alias
	 * @param string $key
	 * @return integer object id
	 */
	private function putRawData($id, $data, $dataType, $key)
	{
		$this->checkKey($key);
		$key = $this->normalizeKey($key);

		if (!isset($dataType)) {
			$isObject = is_object($data);
			$dataType = $isObject ? get_class($data) : 'not_set';
		}

		Crypt::setKey($key);
		$encryptedData = Crypt::encrypt($data);

		$secureObj = SecureObject::findOrNew($id);
		$secureObj->object_type = $dataType;
		$secureObj->data = $encryptedData;
		$secureObj->save();
		return $secureObj->id;
	}

	public function get($id, $key)
	{
		$this->checkKey($key);
		$key = $this->normalizeKey($key);

		$secureObj = SecureObject::find($id);
		if (!$secureObj) {
			return null;
		}

		$encryptedData = $secureObj->data;
		Crypt::setKey($key);

		try {
			$decryptedData = Crypt::decrypt($encryptedData);
		} catch (DecryptException $e) {
			$this->auditor->record($secureObj, 'unauthorized-read', null, $e->getMessage());
			throw new InvalidKeyException(__("provided key isn't valid", 'secure.server.repo.invalid.key'));
		}

		$this->auditor->record($secureObj, 'read');
		return $decryptedData;
	}

	public function count()
	{
		return SecureObject::count();
	}


	/** Check key for validity
	 * @param string $key
	 * @throws InvalidKeyException
	 */
	private function checkKey($key)
	{
		if (!isset($key)) {
			throw new InvalidKeyException(__('Provided key is empty', 'secure.server.repo.empty.key'));
		}
	}


	/** Normilize key to 32 symbols
	 * @param string $key
	 * @return string
	 */
	private function normalizeKey($key) {
		//we don't need more complex algorithm here, because we don't save this anywhere
		//see Crypt::encrypt method
		return md5($key);
	}
}

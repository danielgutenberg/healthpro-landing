<?php namespace WL\SecureServer\Models;

use WL\Models\ModelBase;

class SecureObject extends ModelBase implements AuditableInterface
{
	use AuditTrait;

	protected $connection = 'auditdb';
}

<?php namespace WL\SecureServer\Models;

use WL\SecureServer\Observers\AuditTraitObserver;

trait AuditTrait
{
	public static function bootAuditTrait() {
		static::observe(new AuditTraitObserver());
	}
}

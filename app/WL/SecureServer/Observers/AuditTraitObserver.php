<?php namespace WL\SecureServer\Observers;

use Illuminate\Database\Eloquent\Model;
use WL\SecureServer\Auditor;

/**
 * Trait for logging audit events
 * Class AuditTraitObserver
 */
class AuditTraitObserver
{

	/**
	 * @var Auditor
	 */
	private $auditor;

	/**
	 *
	 */
	function __construct()
	{
		$this->auditor = new Auditor();
	}

	/**
	 * @param $model
	 */
	public function created($model)
	{
		$this->addAuditLog('create', $model);
	}

	/**
	 * @param $model
	 */
	public function updated($model)
	{
		$this->addAuditLog('update', $model);
	}

	/**
	 * @param $model
	 */
	public function deleted($model)
	{
		$this->addAuditLog('delete', $model);
	}

	/**
	 * Save audit record
	 * @param string $eventName
	 * @param Model $model
	 * @throws \WL\SecureServer\Exceptions\InvalidEventTypeException
	 * @throws \WL\SecureServer\Exceptions\InvalidModelException
	 */
	private function addAuditLog($eventName, $model)
	{
		$this->auditor->record($model, $eventName);
	}

}

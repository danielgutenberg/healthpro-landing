<?php namespace WL\SecureServer;

use Sentinel;
use WL\SecureServer\Exceptions\InvalidEventTypeException;
use WL\SecureServer\Exceptions\InvalidModelException;
use WL\SecureServer\Models\AuditableInterface;
use WL\SecureServer\Models\AuditLog;
/**
 * Class Auditor
 * @package WL\SecureServer
 */
class Auditor
{
	/** Available audit events
	 * @var array
	 */
	protected $eventNames = ['create', 'update', 'delete', 'read', 'unauthorized-read'];

	/** Store audit records to db
	 * @param AuditableInterface $model
	 * @param string $eventName
	 * @param null|string $objectType
	 * @param null|integer $userId
	 * @param null|string $comment
	 * @throws InvalidEventTypeException
	 * @throws InvalidModelException
	 */
	public function record($model, $eventName, $objectType = null, $userId = null, $comment = null)
	{
		if (!$model instanceof AuditableInterface) {
			throw new InvalidModelException(__('model must implement AuditableInterface', "audit.model.not.valid"));
		}

		if (!in_array($eventName, $this->eventNames)) {
			$eventNamesJoined = implode(', ', $this->eventNames);
			throw new InvalidEventTypeException(__("eventName must be on of $eventNamesJoined", "audit.event.not.valid"));
		}

		$userId = isset($userId) ?: Sentinel::check()->id;
		$objectId = $model->getKey();
		$auditType = $eventName;

		$this->createAuditLog($userId, $objectId, $objectType ?: $model->object_type, $auditType, $comment);
	}

	/** Available audit events
	 * @return array
	 */
	public function getEventNames()
	{
		return $this->eventNames;
	}

	/** Create audit log
	 * @param integer $userId
	 * @param integer $objectId
	 * @param string $objectType
	 * @param string $auditType
	 * @param string $comment
	 */
	private function createAuditLog($userId, $objectId, $objectType, $auditType, $comment)
	{
		$auditLog = new AuditLog();
		$auditLog->object_type = $objectType;
		$auditLog->user_id = $userId;
		$auditLog->object_id = $objectId;
		$auditLog->audit_type = $auditType;
		$auditLog->comment = $comment;
		$auditLog->save();
	}

}

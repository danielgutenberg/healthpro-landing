<?php namespace WL\Presenters;

use WL\Presenters\Exceptions\ColumnNotFoundException;

abstract class Presenter
{
	/**
	 * @var
	 */
	protected $entity;

	/**
	 * @param $entity
	 */
	function __construct($entity)
	{
		$this->entity = $entity;
	}

	/**
	 * @param $property
	 * @return mixed
	 */
	public function __get($property)
	{
		if (method_exists($this, $property)) {
			return $this->{$property}();
		}

		return $this->entity->{$property};
	}

    public function __call($method, array $args)
    {
        return call_user_func_array(array($this->entity, $method), $args);
    }

	/**
	 * @return mixed
	 * @throws ColumnNotFoundException
	 */
	public function createdAt()
	{
		if (!isset($this->entity->created_at)) {
			throw new ColumnNotFoundException('you cannot use `created_at` column within current model');
		}

		return $this->entity->created_at;
	}

	/**
	 * @return mixed
	 * @throws ColumnNotFoundException
	 */
	public function updatedAt()
	{
		if (!isset($this->entity->updated_at)) {
			throw new ColumnNotFoundException('you cannot use `updated_at` column within current model');
		}

		return $this->entity->updated_at;
	}
}

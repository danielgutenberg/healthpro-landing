<?php
namespace WL\Translator;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class TranslatorServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	public function boot()
	{
	//	//$this->package('wl/translator');

		include_once __DIR__ . '/functions.php';
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerFilters();


		$this->registerTranslator();
	}

	protected function registerFilters()
	{
		/**
		 * 	This filter would set the translated route name
		 */
		//TODO: filter was removed in L 5.2
//		Route::filter('TranslatorRedirectFilter', function()
//		{
//			global $app;
//			return $app['thetranslator']->checkRedirect();
//		});
	}

	protected function registerTranslator()
	{
		$this->app['thetranslator'] = $this->app->share(function($app) {
			return new Translator();
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('thetranslator');
	}
}

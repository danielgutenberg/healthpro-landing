<?php
namespace WL\Translator;

// include exception namespace
use Exception;

/**
 * thrown when no languages provided in yaml config
 */
class NoLanguagesProvidedException extends Exception {}

/**
 * thrown when there is no default language in yaml config
 */
class NoDefaultLanguageException extends Exception {}

/**
 * thrown when there is no language configuration in yaml config
 */
class NoLanguageConfigException extends Exception {}

/**
 * thrown when the model doesn't have a $fillable property
 */
class NoFillablePropertyException extends Exception {}

/**
 * thrown when the translation model couldn't be saved
 */
class SaveTranslationException extends Exception {}

/**
 * thrown when the translation could not be found
 */
class TranslationNotFoundException extends Exception {}
<?php
if ( !function_exists('__') ) {
	function __( $string, $location = '' ) {
		if ($string===null || $string==='') {
			if (!app()->environment('production', 'staging'))
				throw new \Exception("Empty string attempted to be translated.");

			return '';
		}
		return DBTranslator::translate( _( $string ), $location );
	}
}

if ( !function_exists('_e') ) {
	function _e( $string, $location = '' ) {
		echo __( $string, $location );
	}
}

if ( !function_exists('_n') ) {
	function _n ($msg1, $msg2, $n, $location = '') {
		return ngettext($msg1, $msg2, $n);
	}
}

if ( !function_exists('_x') ) {
	function _x( $string, $location = '' ) {
		// do something
	}
}

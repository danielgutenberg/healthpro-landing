<?php
namespace WL\Translator\Models;

use DBTranslator;
use WL\Translator\TranslationNotFoundException;

trait TranslatableTrait
{
	/**
	 *
	 * @var string|null
	 */
	public static $modelLanguage = null;

	/**
	 * @return string|null
	 */
	public static function getModelLanguage()
	{
		return self::$modelLanguage;
	}

	/**
	 * @param $language
	 * @return $this
	 */
	public static function setModelLanguage( $language )
	{
		self::$modelLanguage = $language;
	}

	/**
	 * Overwrite the `translatable` method to return `true` on checking.
	 *
	 * @return bool
	 */
	public function translatable()
	{
		return true;
	}

	/**
	 * Check if current object has a translation
	 *
	 * @param $language
	 * @return bool
	 */
	public function hasTranslation( $language )
	{
		return $this->translator( $language ) ? true : false;
	}

	/**
	 * Morph to the Translation model
	 * @return mixed
	 */
	public function translation()
	{
		return $this->morphOne('WL\Translator\Models\Translation', 'elm');
	}

	/**
	 * Check if the current translation is the source translation
	 *
	 * @return bool
	 */
	public function isSource()
	{
		if ( null === $this->translation ) {
			return false;
		}

		return $this->translation->source_id === $this->translation->elm_id;
	}

	/**
	 * Get source translation
	 * @return mixed
	 */
	public function source()
	{
		if ( null === $this->translation ) {
			return false;
		}

		// check if the current object has the source translation
		if ( $this->isSource() ) {
			return $this;
		}

		// find the source translation
		return self::find( $this->translation->source_id );
	}

	/**
	 * Get current translation language
	 *
	 * @return mixed
	 */
	public function language()
	{
		if ( null === $this->translation ) {
			return false;
		}

		return $this->translation->lang;
	}

	/**
	 * Get source translation language
	 * @return mixed
	 */
	public function sourceLanguage()
	{
		if ( null === $this->translation ) {
			return false;
		}

		return $this->translation->lang_src;
	}

	/**
	 * Update translation
	 *
	 * @param string $language
	 * @param mixed $source
	 * @param mixed $languageSource
	 * @return bool
	 */
	public function updateTranslation( $language, $source = null, $languageSource = '' )
	{
		if (null === $this->translation) {

			// check if the language exists
			$language = $this->filterLanguage( $language );

			// check source language if one exits
			if ( $languageSource ) {
				$languageSource = $this->filterLanguage( $languageSource );
			}

			if ( !$this->hasTranslation( $language ) ) {

				if ( null === $source ) {
					$source = $this->id;
				}

				// create the translation
				$translation = $this->translation()->create([
					'lang'      => $language,
					'lang_src'  => $languageSource,
					'name'      => $this->generateUniqueId(),
					'source_id' => $source,
				]);

				// load translation from db
				$this->load('translation');
			}
		}

		return true;
	}

	/**
	 * Save the translated object to database
	 * Method copies the properties from the $translateFillable array
	 * to the new object to make sure such data as `title`, `content` exists.
	 *
	 * @param $language
	 * @return mixed
	 * @throws \WL\Translator\NoFillablePropertyException
	 * @throws \WL\Translator\SaveTranslationException
	 */
	public function translate( $language, $ignoreEvents = true )
	{
		if ( !$this->hasTranslation( $language ) ) {

			// create new entry
			$translation = $this->getStatic();

			// ignore events while saving the model
			$translation->ignoreEvents = $ignoreEvents;

			if ( !$this->translateFillable || !is_array( $this->translateFillable ) || !count( $this->translateFillable ) ) {
				throw new \WL\Translator\NoFillablePropertyException( 'Please add the `translateFillable` property to the model. ' );
			}

			foreach ( $this->translateFillable as $field) {
				$translation->$field = $this->$field;
			}

			if ( !$translation->save() ) {
				throw new \WL\Translator\SaveTranslationException( 'Can\'t save translation. Please check your configuration for the `' . __CLASS__ . '` model. ' );
			}

			// add the row to the translation table with the source
			$translation->updateTranslation( $language, $this->id, $this->translation->lang );

		} else {
			$translation = $this->translator( $language );
		}

		return $translation;
	}

	/**
	 * Get the translation
	 *
	 * @param $language
	 * @return mixed
	 */
	public function translator( $language )
	{
		if ( null === $this->translation ) {
			return false;
		}

		$language = $this->filterLanguage( $language );

		// check if current entity is in the requested language
		if ( $this->translation && $this->translation->lang === $language ) {
			return $this;
		}

		// find and return object translation
		return $this->getObjTranslation( $language );
	}

	/**
	 * Method to find the translation for the current object
	 *
	 * @param $language
	 *
	 * @return mixed
	 */
	protected function getObjTranslation( $language )
	{
		$language = $this->filterLanguage( $language );
		$table = $this->getTableName();
		$joinTable = $this->translation()->getModel()->getTableName();

		return $this
			->select($table . '.*')
			->join($joinTable, $table . '.id', '=', $joinTable . '.elm_id')
			->where($joinTable . '.lang',      '=', $language)
			->where($joinTable . '.source_id', '=', $this->source()->id)
			->where($joinTable . '.elm_type',  '=', $this->morphClass)
			->first();
	}

	/**
	 * Check if language exists in the languages stack
	 * Return the default language if not
	 *
	 * @param string $language
	 * @return string
	 */
	protected function filterLanguage( $language )
	{
		$codes = DBTranslator::getLanguagesCodes();

		if ( !in_array( $language, $codes ) ) {
			$language = DBTranslator::getDefaultLanguage();
		}

		return $language;
	}

	/**
	 * Extends the findOrFail method to find the proper translator
	 *
	 * @param $id
	 * @param $language
	 * @param array $columns
	 * @throws TranslationNotFoundException
	 */
	public function findTranslatorOrFail( $id, $language, $columns = ['*'] )
	{
		$language = $this->filterLanguage( $language );

		// find the object by ID
		$translation = $this->findOrFail( $id );

		// check if the current translation has the needed language
		if ( $translation->language() === $language ) {
			return $translation;
		}

		// if not, we have to find if the translation exists
		if ( $translation->hasTranslation( $language ) ) {
			return $translation->translator( $language );
		}

		throw new TranslationNotFoundException( 'We could not find the translation for the ID `' . $id . '`' );

	}

	/**
	 * Find all the translations for the current object
	 *
	 * @return array
	 */
	public function translations()
	{

	}
}

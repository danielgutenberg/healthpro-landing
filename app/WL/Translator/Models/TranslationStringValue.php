<?php
namespace WL\Translator\Models;

use WL\Models\ModelBase;

class TranslationStringValue extends ModelBase
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'translations_strings_values';

	/**
	 * Disable timestamps
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * Fillable model params
	 *
	 * @var array
	 */
	protected $fillable = ['translation_id', 'lang', 'content', 'frequency'];

	/**
	 * Default Validation Rules
	 *
	 * @var array
	 */
	protected $rules = [

	];

	public function translation()
	{
		return $this->belongsTo('TranslationString', 'string_id');
	}

	public function generateUnique()
	{
		$this->uniq = $this->generateUniqueId();
	}
}

<?php
namespace WL\Translator\Models;

use WL\Models\ModelBase;

class Translation extends ModelBase
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'translations';

	/**
	 * Disable timestamps
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * Fillable model params
	 *
	 * @var array
	 */
	protected $fillable = ['source_id', 'lang', 'lang_src', 'elm_id', 'elm_type', 'name'];

	/**
	 * Default Validation Rules
	 *
	 * @var array
	 */
	protected $rules = [];

	/**
	 * @return mixed
	 */
	public function translation()
	{
		return $this->morphTo();
	}
}

<?php
namespace WL\Translator\Models;

use WL\Models\ModelBase;

class TranslationStringGroup extends ModelBase
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'translations_strings_groups';

	/**
	 * Disable timestamps
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * Fillable model params
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	/**
	 * Default Validation Rules
	 *
	 * @var array
	 */
	protected $rules = [
		'name' => 'required|regex:/^([\d\w\-\.]+)$/|unique:translations_strings_groups',
	];

	public function translations()
	{
		return $this->hasMany('\WL\Translator\Models\TranslationString', 'group_id');
	}
}

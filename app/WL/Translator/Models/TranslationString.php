<?php
namespace WL\Translator\Models;

use WL\Translator\Models\TranslationStringGroup;
use WL\Translator\Models\TranslationStringValue;
use WL\Models\ModelBase;
use DBTranslator;

class TranslationString extends ModelBase
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'translations_strings';

	/**
	 * Disable timestamps
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * Fillable model params
	 *
	 * @var array
	 */
	protected $fillable = ['group_id', 'lang', 'elm_id', 'elm_type', 'name', 'content'];

	/**
	 * Default Validation Rules
	 *
	 * @var array
	 */
	protected $rules = [

	];

	public function group()
	{
		return $this->belongsTo('\WL\Translator\Models\TranslationStringGroup', 'group_id');
	}

	public function values()
	{
		return $this->hasMany('\WL\Translator\Models\TranslationStringValue', 'string_id');
	}

	public function scropeGroup($query, $group)
	{
		return $query->where('group_id', '=', $group);
	}

	public function saveValue($valueData, $valueId = 0)
	{
		$translationValue = null;

		if ($valueId) {
			$translationValue = TranslationStringValue::find($valueId);
		}

		if (null === $translationValue) {
			$translationValue = new TranslationStringValue();
			$translationValue->string_id = $this->id;
			$translationValue->generateUnique();
		}

		$translationValue->fill($valueData);

		return $translationValue->save();
	}

	/**
	 * Retreive translation value
	 *
	 * @param $translation
	 * @param string $group
	 * @return string
	 */
	public static function value($translation, $group = '')
	{
		if (!$group) {
			return $translation;
		}

		$groupTable = TranslationStringGroup::getTableName();
		$transTable = self::getTableName();
		$valueTable = TranslationStringValue::getTableName();

		$query = self::join($groupTable, $transTable . '.group_id', '=', $groupTable . '.id')
			->join($valueTable, $transTable . '.id', '=', $valueTable . '.string_id')
			->where($transTable . '.name', '=', $translation)
			->where($groupTable . '.name', '=', $group)
			->where($valueTable . '.lang', '=', DBTranslator::currentLanguage())
			->where($valueTable . '.frequency', '>', 0);


		$query->orderByRaw('-LOG(1.0 - RAND()) / ' . $valueTable . '.frequency DESC');

		$value = $query->pluck('content')->first();

		return $value ? $value : $translation;
	}

	public function translatedValues($lang)
	{
		return $this->values()
			->where('lang', '=', $lang)
			->get();
	}
}

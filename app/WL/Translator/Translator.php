<?php
namespace WL\Translator;

use Yaml;
use Gettext;
use App;
use URL;
use Request;
use Redirect;
use WL\Translator\Models\TranslationString;

class Translator
{
	protected $config;

	protected $language = null;

	public function __construct()
	{
		$this->config = Yaml::parse( 'wl.translation.languages' );
	}

	public function getLanguages()
	{
		if (!isset( $this->config['languages'] ) || !is_array( $this->config['languages'] ) || !count($this->config['languages']) ) {
			throw new NoLanguagesProvidedException( 'No languages provided' );
		}

		return $this->config['languages'];
	}

	public function getLanguagesCodes()
	{
		return array_keys( $this->getLanguages() );
	}

	public function setLanguage( $language = null )
	{
		if (null === $language) {
			$language = $this->getDefaultLanguage();
		}

		$languageConfig = $this->getLanguageConfig( $language );

		// set Gettext locale and encoding for translation
		Gettext::setLocale( $languageConfig['locale'] );
		Gettext::setEncoding( $languageConfig['encoding'] );

		$this->language = $language;

		return $this;
	}

	public function getDefaultLanguage()
	{
		if (!isset( $this->config['default'] )) {
			throw new NoDefaultLanguageException( 'No default language provided' );
		}

		return $this->config['default'];
	}

	public function getLanguageConfig( $language = null )
	{
		if (null === $language) {
			$language = $this->getDefaultLanguage();
		}

		$languages = $this->getLanguages();

		if ( !isset( $languages[$language] ) ) {
			throw new NoLanguageConfigException( 'No language configuration provided' );
		}

		return $languages[$language];
	}

	public function currentLanguage()
	{
		if (null === $this->language) {
			$this->setLanguage();
		}

		return $this->language;
	}

	public function hasLanguage()
	{
		return null === $this->language ? false : true;
	}

	public function prefix()
	{
		$languages = $this->getLanguagesCodes();

		$default = $this->getDefaultLanguage();

		# check if app is running in console and set the default language
		if ( App::runningInConsole() ) {
			$language = $default;

		# grab the language from the request segment
		} else {
			$segment = Request::segment(1);

			$language = in_array( $segment, $languages ) ? $segment : $this->getDefaultLanguage();
		}

		$this->setLanguage( $language );

		return $default === $language ? null : $language;
	}

	public function switchLanguageUrl( $language )
	{
		// strip the language from the path
		$noLangPath = preg_replace( '~^(' . implode( '|', $this->getLanguagesCodes() ) . ')~si', '', Request::path() );
		// we wanna make sure there are no double backslashes in the path
		$noLangPath = ltrim( $noLangPath, '/' );

		// find the right prefix for the language
		$prefix = $language === $this->getDefaultLanguage() ? '' : $language . '/';

		return URL::to( $prefix . $noLangPath );
	}

	/**
	 * Check if we need to redirect the page
	 *
	 * @return mixed|null
	 * @throws NoDefaultLanguageException
	 */
	public function checkRedirect()
	{
		$segment = Request::segment(1);

		$default = $this->getDefaultLanguage();

		return $segment === $default ? $this->redirectUrl( $default ) : null;
	}

	/**
	 * Get language redirect URL
	 *
	 * @param $language
	 * @return mixed
	 */
	public function redirectUrl( $language )
	{
		return Redirect::to( $this->switchLanguageUrl( $language ) );
	}

	/**
	 * Translate a string
	 *
	 * @param $value
	 * @param string $group
	 * @return string
	 */
	public function translate( $value, $group = '' )
	{
		return TranslationString::value( $value, $group );
	}

	/**
	 * Get languages array for select / dropdown
	 *
	 * @return array
	 */
	public function getLanguageOptions()
	{
		$options = [];

		foreach( $this->getLanguages() as $lang => $langConfig ) {
			$options[$lang] = $langConfig['name'];
		}

		return $options;
	}
}

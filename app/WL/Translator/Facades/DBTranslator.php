<?php
namespace WL\Translator\Facades;

use Illuminate\Support\Facades\Facade;

class DBTranslator extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'thetranslator';
	}
}
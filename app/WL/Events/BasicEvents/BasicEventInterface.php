<?php namespace WL\Events\BasicEvents;

interface BasicEventInterface
{
	public function getEventName();
}

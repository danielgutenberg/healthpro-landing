<?php namespace WL\Events\BasicEvents;

class EmailHealthProToClientEmailEvent extends BasicEmailEvent
{
    protected $sendFromSystem = true;
	/**
	 * EmailProviderToClientEvent constructor.
	 *
	 * @param $bodyBladeName
	 * @param $subjectBladeName
	 * @param $bladeData
	 * @param $providerName
	 * @param $providerEmail
	 * @param $clientName
	 * @param $clientEmail
     */
	function __construct($bodyBladeName, $subjectBladeName, $bladeData, $providerName, $providerEmail, $clientName, $clientEmail, EmailAttachment $attachment=null)
	{
		if($this->sendFromSystem) {
            $providerName = $providerEmail = null;
        }
	    parent::__construct($bodyBladeName, $subjectBladeName, $bladeData, $providerName, $providerEmail, $clientName, $clientEmail, $attachment);
	}

	public function getEventName()
	{
		return "event-mail-provider-client";
	}
}

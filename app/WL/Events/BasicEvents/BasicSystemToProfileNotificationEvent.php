<?php namespace WL\Events\BasicEvents;


use Carbon\Carbon;
use ReflectionClass;
use WL\Modules\Profile\Facades\ProfileService;

class BasicSystemToProfileNotificationEvent extends BasicNotificationEvent
{

	function __construct($profile, $bladeData, $expireAt = null)
	{
		if ($expireAt == null) {
			$expireAt = Carbon::now()->addMonth();
		}

		parent::__construct(null, null, $bladeData, ['id' => 0, 'type' => 'system'], $profile, $expireAt, (new ReflectionClass($this))->getShortName());
	}
}

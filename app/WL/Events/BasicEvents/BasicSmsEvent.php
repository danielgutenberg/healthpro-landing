<?php namespace WL\Events\BasicEvents;

class BasicSmsEvent implements BasicEventInterface
{
	public $data;
	public $sendTo;

	/**
	 * BasicMailEvent constructor.
	 *
	 * @param $bodyBladeName
	 * @param $subjectBladeName
	 * @param $bladeData
	 * @param $nameFrom
	 * @param $emailFrom
	 * @param $nameTo
	 * @param $emailTo
	 * @param EmailAttachment $attachment
	 */
	public function __construct($data, $sendTo)
	{
		$this->data = $data;
		$this->sendTo = $sendTo;
	}

	public function getEventName()
	{
		return 'event-sms-basic';
	}
}

<?php namespace WL\Events\BasicEvents;

use WL\Settings\Facades\Setting;

class EmailHealthProToAdminEmailEvent extends BasicEmailEvent
{
	public $shouldQueue = true;

	/**
	 * EmailHealthProToAdminEmailEvent constructor.
	 *
	 * @param $bodyBladeName
	 * @param $subjectBladeName
	 * @param $bladeData
     */
	function __construct($bodyBladeName, $subjectBladeName, $bladeData)
	{
		$nameFrom = trim(Setting::get('default_from_name'));
		$emailFrom = trim(Setting::get('default_from_email'));
		$nameTo = 'System Admin';
		$emailTo = trim(Setting::get('ticket_admin_emails'));
		parent::__construct($bodyBladeName, $subjectBladeName, $bladeData, $nameFrom, $emailFrom, $nameTo, $emailTo);
	}

	public function getEventName()
	{
		return env('LIVE') == 'true' ? "event-mail-provider-client" : "do-not-trigger-event";
	}
}

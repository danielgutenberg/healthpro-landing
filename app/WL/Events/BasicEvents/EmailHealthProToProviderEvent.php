<?php namespace WL\Events\BasicEvents;

class EmailHealthProToProviderEmailEvent extends BasicEmailEvent
{

	/**
	 * EmailProviderToProviderEvent constructor.
	 *
	 * @param $bladeData
	 * @param $providerName
	 * @param $providerEmail
	 */
	
	//see blade aliases in config/wl/emailBlades
	function __construct($bladeData, $providerName, $providerEmail)
	{
		parent::__construct(null, null, $bladeData, null, null, $providerName, $providerEmail);
	}

	public function getEventName()
	{
		return "event-mail-provider-client";
	}
}

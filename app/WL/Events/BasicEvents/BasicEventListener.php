<?php namespace WL\Events\BasicEvents;

class BasicEventListener
{
	public function handle(BasicEventInterface $event)
	{
		$this->dispatchEventToPrivateMethod($event);
	}

	protected function dispatchEventToPrivateMethod(BasicEventInterface $event)
	{
		$className = lcfirst((new \ReflectionClass($event))->getShortName());
		$this->$className($event);

	}
}

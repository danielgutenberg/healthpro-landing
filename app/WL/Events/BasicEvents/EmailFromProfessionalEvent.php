<?php namespace WL\Events\BasicEvents;


use WL\Modules\Profile\Facades\ProfileService;

class EmailFromProfessionalEvent extends EmailHealthProToClientEmailEvent
{
    public $shouldQueue = true;
    protected $sendFromSystem = false;

    function __construct($providerId, $clientId, $bladeData)
    {
        $proEmail = ProfileService::getProfessionalEmail($providerId);
        $clientEmail = ProfileService::getNameAndEmail($clientId);
        parent::__construct(null, null, $bladeData, $proEmail->name, $proEmail->email, $clientEmail->name, $clientEmail->email);
    }
}
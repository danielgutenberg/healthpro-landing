<?php namespace WL\Events\BasicEvents;

class BasicEmailEvent implements BasicEventInterface
{
	public $bodyBlade;
	public $subjectBlade;
	public $bladeData;
	public $nameFrom;
	public $emailFrom;
	public $nameTo;
	public $emailTo;
	public $shouldQueue = false;
    public $attachment;
    public $copyCs = false;

	/**
	 * BasicMailEvent constructor.
	 *
	 * @param $bodyBladeName
	 * @param $subjectBladeName
	 * @param $bladeData
	 * @param $nameFrom
	 * @param $emailFrom
	 * @param $nameTo
	 * @param $emailTo
	 * @param EmailAttachment $attachment
     */
	public function __construct($bodyBladeName, $subjectBladeName, $bladeData, $nameFrom, $emailFrom, $nameTo, $emailTo, EmailAttachment $attachment=null)
	{
		$this->bodyBlade = $bodyBladeName;
		$this->subjectBlade = $subjectBladeName;
		$this->bladeData = $bladeData;
		$this->nameFrom = $nameFrom;
		$this->emailFrom = $emailFrom;
		$this->nameTo = $nameTo;
		$this->emailTo = $emailTo;
        $this->attachment = $attachment;
	}

	public function getEventName()
	{
		return 'event-mail-basic';
	}
}

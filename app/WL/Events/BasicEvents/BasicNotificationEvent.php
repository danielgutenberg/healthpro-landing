<?php namespace WL\Events\BasicEvents;

class BasicNotificationEvent implements BasicEventInterface
{
	public $sender;
	public $dispatchers;
	public $expireAt;
	public $type;
	public $descriptionBladeName;
	public $titleBladeName;
	public $bladeData;

	/**
	 * BasicNotificationEvent constructor.
	 * @param $descriptionBladeName
	 * @param $titleBladeName
	 * @param $bladeData
	 * @param $sender
	 * @param $dispatchers
	 * @param $expireAt
	 * @param null $type
	 */
	public function __construct($descriptionBladeName, $titleBladeName, $bladeData, $sender, $dispatchers, $expireAt, $type = null)
	{
		$this->descriptionBladeName = $descriptionBladeName ?: $this->descriptionBladeName;
		$this->titleBladeName = $titleBladeName ?: $this->titleBladeName;
		$this->bladeData = $bladeData;
		$this->sender = $sender;
		$this->dispatchers = $dispatchers;
		$this->expireAt = $expireAt;
		$this->type = $type;
	}

	public function getEventName()
	{
		return 'event-notification';
	}
}

<?php namespace WL\Events\BasicEvents;


class EmailAttachment
{
    const TYPE_PATH = 'path';
    const TYPE_DATA = 'data';

    protected $path;
    protected $name;
    protected $mimeType;
    protected $content;
    protected $type;

    public function __construct($data, $name, $mime, $type=self::TYPE_PATH)
    {
        $this->type = $type;
        if($this->type === self::TYPE_PATH) {
            $this->path = $data;
        } else {
            $this->content = $data;
        }
        $this->name = $name;
        $this->mimeType = $mime;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getName()
    {
        return $this->name;
    }
    public function getMimeType()
    {
        return $this->mimeType;
    }
    public function getContent()
    {
        return $this->content;
    }
    public function getType()
    {
        return $this->type;
    }
}
<?php namespace WL\Events\Listeners;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use ReflectionClass;
use WL\Events\BasicEvents\BasicNotificationEvent;
use WL\Modules\Notification\Facades\NotificationService;
use WL\Settings\Facades\Setting;
use WL\Yaml\Facades\Yaml;


class BasicNotificationDispatcher
{

	public function handle(BasicNotificationEvent $notificationEvent)
	{
		$className = (new ReflectionClass($notificationEvent))->getShortName();

		$loadedBlades = null;
		$descriptionBladeName = $notificationEvent->descriptionBladeName;
		$titleBladeName = $notificationEvent->titleBladeName;

		if (empty($descriptionBladeName)) {
			$loadedBlades = $this->loadBladeConfig($className);
			$descriptionBladeName = $loadedBlades['description'];
		}

		if (empty($titleBladeName)) {
			$loadedBlades = !$loadedBlades ?: $this->loadBladeConfig($className);
			$titleBladeName = $loadedBlades['title'];
		}

		$title = View::make($titleBladeName, $notificationEvent->bladeData)->render();
		$description = View::make($descriptionBladeName, $notificationEvent->bladeData)->render();

		NotificationService::createNotification(
			$notificationEvent->sender,
			$notificationEvent->dispatchers,
			$title,
			$description,
			$notificationEvent->expireAt,
			$notificationEvent->type
		);
	}

	private function loadBladeConfig($className)
	{
		return Yaml::get($className, 'wl.notificationEvents.notificationBladesAliases');
	}
}

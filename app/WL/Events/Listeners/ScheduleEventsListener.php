<?php namespace WL\Events\Listeners;

use Illuminate\Foundation\Bus\DispatchesJobs;
use WL\Events\BasicEvents\BasicEventListener;
use WL\Modules\ProfileCalendar\Commands\PushCalendarEventCommand;
use WL\Schedule\Events\BlockedTimeCreate;
use WL\Schedule\Events\BlockedTimeRemove;
use WL\Schedule\Events\BlockedTimeUpdate;
use WL\Schedule\Events\ScheduleRegistrationLocationChanged;

class ScheduleEventsListener extends BasicEventListener
{
	use DispatchesJobs;

	public function ScheduleRegistrationLocationChanged(ScheduleRegistrationLocationChanged $event)
	{

	}

	public function BlockedTimeCreate(BlockedTimeCreate $event)
	{
		$this->pushCalendarEvent($event->registrationId);
	}

	public function BlockedTimeUpdate(BlockedTimeUpdate $event)
	{
		$this->pushCalendarEvent($event->registrationId);
	}

	public function BlockedTimeRemove(BlockedTimeRemove $event)
	{
		$this->pushCalendarEvent($event->registrationId);
	}

	private function pushCalendarEvent($registrationId)
	{
		$this->dispatch(new PushCalendarEventCommand($registrationId));
	}
}

<?php namespace WL\Events\Listeners;

use Guzzle\Http\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use WL\Yaml\Facades\Yaml;
use WL\Modules\Profile\Facades\ProfileService;

class UpdateHasOffers
{
	public function handle($event)
	{
		$profile = $event->getProfile();
		$goal = $event->getGoal();
		if ($goal == 'registered') {
			$this->saveHasOffersValues($profile);
		}
		$hasOffersHash = ProfileService::loadProfileMetaField($profile->id, 'has_offers_id', false);
		if ($hasOffersHash) {
			$callbacks = Yaml::parse('wl.has_offers.has_offers')['callbacks'];
			$urlSuffix = '&aff_sub=' . $profile->owner()->email;
			switch ($goal) {
				case 'searchable':
					$url = env('has_offers_searchable', $callbacks['searchable']) . $this->getAffiliateQueryString($hasOffersHash) . $urlSuffix . '&aff_sub2=' . $event->getRegistered();
					break;
				case 'registered':
					$url = env('has_offers_register', $callbacks['register']) . $this->getAffiliateQueryString($hasOffersHash)  . $urlSuffix . '&aff_sub2=' . $event->getRegistered();
					break;
				case 'payment':
					$amount = $event->getFee();
					$appointmentCost = $event->getAppointmentCost();
					$url = env('has_offers_payment', $callbacks['payment']) . $this->getAffiliateQueryString($hasOffersHash)  . '&amount=' . $amount . $urlSuffix . '&aff_sub3=' . $appointmentCost;
					break;
				default:
					return false;
			}

			$this->updateHasOffers($url);
		}

		return false;
	}

	private function getAffiliateQueryString($hasOffersHash)
	{
		$params = Crypt::decrypt(base64_decode($hasOffersHash));
		$affiliateId = $params['affiliate_id'];
		$offerId = $params['offer_id'];
		return '&aff_id=' . $affiliateId . '&offer_id=' . $offerId;
	}

	private function saveHasOffersValues($profile)
	{
		$hasOffersHash = session()->has('has_offers_id') ? session('has_offers_id') : null;
		if ($hasOffersHash) {
			ProfileService::storeProfileMetaFields($profile->id, ['has_offers_id' => $hasOffersHash], ['has_offers_id']);
			return $hasOffersHash;
		}
		return false;
	}

	private function updateHasOffers($url)
	{
		$client = new Client();
		$request = $client->get($url);
		$request->send();
	}
}

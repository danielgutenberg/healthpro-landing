<?php namespace WL\Events\Listeners;


use WL\Events\BasicEvents\BasicEventListener;
use WL\Events\Facades\EventHelper;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Provider\Events\BaseProviderPlanEvent;
use WL\Modules\Provider\Events\PlanCancelledAdminMailEvent;
use WL\Modules\Provider\Events\PlanCancelledMailEvent;
use WL\Modules\Provider\Facades\ProviderMembershipService;

class ProviderMembershipListener extends BasicEventListener
{
    public function cancelled(BaseProviderPlanEvent $event)
    {
        $plan = $event->getPlan();
        $profileEmail = ProfileService::getNameAndEmail($plan->profile_id);
        $profileDetails = ProfileService::loadProfileBasicDetails($plan->profile_id);
        $bladeData = [
            'id' => $profileDetails->id,
            'slug' => $profileDetails->slug,
            'firstName' => $profileDetails->first_name,
            'lastName' => $profileDetails->last_name,
            'endDate' => ProviderMembershipService::getPlanEndDate($plan->id)->toDateString(),
            'planName' => $plan->product->description,
        ];

        EventHelper::fire(new PlanCancelledMailEvent($bladeData, $profileEmail->name, $profileEmail->email));
        EventHelper::fire(new PlanCancelledAdminMailEvent($bladeData));
    }
}
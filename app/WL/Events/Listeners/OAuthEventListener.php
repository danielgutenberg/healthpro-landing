<?php namespace WL\Events\Listeners;

use Illuminate\Foundation\Bus\DispatchesJobs;
use WL\Events\BasicEvents\BasicEventListener;
use WL\Events\Facades\EventHelper;
use WL\Modules\Helpers\Facades\EventInfoBuilder;
use WL\Modules\OAuth\Events\AuthenticateProfileEvent;
use WL\Modules\OAuth\Events\Mail\OAuthTokenFailureMail;
use WL\Modules\OAuth\Events\RemoveScopeEvent;
use WL\Modules\OAuth\Events\TokenFailureEvent;
use WL\Modules\OAuth\Models\OauthToken;
use WL\Modules\ProfileCalendar\Commands\DisconnectAccountCalendarsCommand;

class OAuthEventListener extends BasicEventListener
{

	use DispatchesJobs;

	public function AuthenticateProfileEvent(AuthenticateProfileEvent $event)
	{

	}

	public function RemoveScopeEvent(RemoveScopeEvent $event)
	{
		if($event->getScope() === OauthToken::SCOPE_CALENDAR) {
			try {
				$this->dispatch(new DisconnectAccountCalendarsCommand($event->getOauthId()));
			} catch (\Exception $e) {}
		}
	}

	public function TokenFailureEvent(TokenFailureEvent $event)
	{
		$data = EventInfoBuilder::collectOauthData($event->getOauthId());
		EventHelper::fire(new OAuthTokenFailureMail($data, $data['profileName'], $data['profileEmail']));
	}
}

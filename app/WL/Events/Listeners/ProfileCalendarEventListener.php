<?php namespace WL\Events\Listeners;


use WL\Modules\ProfileCalendar\Events\RemoteCalendarDisabledEvent;
use WL\Modules\ProfileCalendar\Facades\ProfileCalendarService;

class ProfileCalendarEventListener
{
	public function calendarDisabled(RemoteCalendarDisabledEvent $event)
	{
		ProfileCalendarService::disconnectProfileCalendar($event->getCalendar());
	}
}

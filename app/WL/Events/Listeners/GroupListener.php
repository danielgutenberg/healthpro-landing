<?php namespace WL\Events\Listeners;

use Illuminate\Mail\Mailer;
use WL\Modules\User\Models\User;
use WL\Modules\ProfileGroup\Models\ProfileGroup;

class GroupListener {

	protected $mailer;

	/**
	 * @param Mailer $mailer
	 */
	public function __construct(Mailer $mailer) {
		$this->mailer = $mailer;
	}

	/**
	 * Invite to the group, sending
	 */
	public function invite(User $user, $code) {
		#@todo Change template ..
		// $this->mailer->send('emails.user.welcome', compact('activation', 'user'), function ($message) use ($user, $code) {
		// 	$message->from('noreply@healthpro.com', 'HealthPro')
		// 		->to($user->email, $user->present()->name)
		// 		->subject('Code confirmation is: '. $code);
		// });
	}

	/**
	 * Reject from the group, sending
	 */
	public function reject(User $user, ProfileGroup $group) {
		#@todo Change template ..
		// $this->mailer->send('emails.user.welcome', compact('activation', 'user'), function ($message) use ($user, $group) {
		// 	$message->from('noreply@healthpro.com', 'HealthPro')
		// 		->to($user->email, $user->present()->name)
		// 		->subject('You has been rejected from group: ', $group->name);
		// });
	}

	/**
	 * Accept an owner to group ..
	 *
	 * @param \WL\Modules\User\Models\User $user
	 * @param ProfileGroup $group
	 */
	public function accept(User $user, ProfileGroup $group) {
		#@todo Change template ..
		// $this->mailer->send('emails.user.welcome', compact('activation', 'user'), function ($message) use ($user, $group) {
		// 	$message->from('noreply@healthpro.com', 'HealthPro')
		// 		->to($user->email, $user->present()->name)
		// 		->subject('You have been accepted to group: ', $group->name);
		// });
	}

	/**
	 * Set owner to group ..
	 *
	 * @param \WL\Modules\User\Models\User $user
	 * @param ProfileGroup $group
	 */
	public function setOwner(User $user, ProfileGroup $group) {
		#@todo Change template ..
		// $this->mailer->send('emails.user.welcome', compact('activation', 'user'), function ($message) use ($user, $group) {
		// 	$message->from('noreply@healthpro.com', 'HealthPro')
		// 		->to($user->email, $user->present()->name)
		// 		->subject('You are owner to group: ', $group->name);
		// });
	}
}

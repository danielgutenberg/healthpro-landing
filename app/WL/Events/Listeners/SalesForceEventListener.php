<?php namespace WL\Events\Listeners;

use Illuminate\Foundation\Bus\DispatchesJobs;
use WL\Modules\SalesForce\Commands\SalesForcePullIdCommand;
use WL\Modules\SalesForce\Commands\SalesForceQueueProvider;
use WL\Modules\SalesForce\Commands\SalesForceUpdateEmail;
use WL\Modules\SalesForce\Commands\SalesForceUpsertProvider;
use WL\Modules\SalesForce\Commands\SalesForceUpsertReview;

class SalesForceEventListener
{
	use DispatchesJobs;

	public function handle($event)
	{
		return $this->dispatch(new SalesForceUpsertProvider($event->getProfileId()));
	}

	public function queue($event)
	{
		return $this->dispatch(new SalesForceQueueProvider($event->getProfileId()));
	}

	public function review($event)
	{
		return $this->dispatch(new SalesForceUpsertReview($event->getReviewId()));
	}

	public function changeEmail($event)
	{
		return $this->dispatch(new SalesForceUpdateEmail($event->getProfileId(), $event->getEmail()));
	}

    public function getRemoteId($event)
    {
        return $this->dispatch(new SalesForcePullIdCommand(['profile_id' => $event->getProfileId()]));
	}
}

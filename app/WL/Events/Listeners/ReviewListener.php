<?php namespace WL\Events\Listeners;


use WL\Events\BasicEvents\BasicEventListener;
use WL\Events\Facades\EventHelper;
use WL\Modules\Client\Events\ProviderReviewNotifyAdminEmail;
use WL\Modules\Client\Events\ProviderReviewNotifyClientEmail;
use WL\Modules\Client\Events\ProviderReviewNotifyProviderEmail;
use WL\Modules\Client\Events\ProviderReviewNotifyProviderNotification;
use WL\Modules\Review\Events\BaseReviewEvent;

class ReviewListener extends BasicEventListener
{
	public function firstComment(BaseReviewEvent $event)
	{
		EventHelper::fire(new ProviderReviewNotifyProviderEmail($event->getReviewId()));
		EventHelper::fire(new ProviderReviewNotifyProviderNotification($event->getReviewId()));
		EventHelper::fire(new ProviderReviewNotifyClientEmail($event->getReviewId()));
		EventHelper::fire(new ProviderReviewNotifyAdminEmail($event->getReviewId()));
	}
}

<?php namespace WL\Events\Listeners;

use Twilio;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use ReflectionClass;
use WL\Events\BasicEvents\BasicSmsEvent;
use WL\Events\BasicEvents\EmailAttachment;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\ProfilesMeta\SmsNotificationsProvider;
use WL\Settings\Facades\Setting;
use WL\Yaml\Facades\Yaml;


class BasicSmsDispatcher
{

	public function handle(BasicSmsEvent $smsEvent)
	{
		// will remove when sms is set up and working
		return true;
		$testingNumbers = [
			'6171111111' => ['972', '586699088'],
			'6172222223' => ['972', '526255076'],
			'6173333333' => ['972', '587627241'],
			'6174444444' => ['972', '525610702'],
			'6175555555' => ['64', '220951245'],
			'8439011978' => ['1', '8439011978']
		];


		$profile = ProfileService::getProfileById($smsEvent->sendTo);
		if ($profile->type == ModelProfile::CLIENT_PROFILE_TYPE) {

		} else {
			
		}
		$phone = $profile->phone;

		//only send out sms to specific testing numbers or if we are in production
		if (!app()->environment('production') && !array_key_exists($phone->number, $testingNumbers)) {
			return true;
		}

		$className = (new ReflectionClass($smsEvent))->getShortName();
		$loadedBlades = $this->loadBladeConfig($className);

		$bodyBlade = $loadedBlades['body'];

		$smsSettings = (new SmsNotificationsProvider($profile))->provide();


		$text = View::make($bodyBlade, $smsEvent->data)->render();

		dd($text);

		/*
		 * check if user has verified phone befores sending
		 */
		Twilio::message($profile->phone, $text);

	}

	private function loadBladeConfig($className)
	{
		return Yaml::get($className, 'wl.smsEvents.smsBladesAliases');
	}

	private function replaceVariables($string, $data)
	{
		$replace = $string;
		foreach ($data as $key => $value){
			$replace = str_replace('%' . $key . '%', $value, $replace);
		}
		return $replace;
	}
}

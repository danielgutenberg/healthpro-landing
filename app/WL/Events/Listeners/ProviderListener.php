<?php namespace WL\Events\Listeners;


use WL\Events\BasicEvents\BasicEventListener;
use WL\Modules\Payment\Models\PaymentMethod;
use WL\Modules\Provider\Events\AcceptPaymentMethodUpdateEvent;
use WL\Modules\Provider\Facades\ProviderService;

class ProviderListener extends BasicEventListener
{
    public function acceptPayments(AcceptPaymentMethodUpdateEvent $event)
    {
        if($event->getMethod() == PaymentMethod::ONLY_IN_PERSON) {
            ProviderService::removeAllGiftCertificates($event->getProfileId());
        }
    }
}
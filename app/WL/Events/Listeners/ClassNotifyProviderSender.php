<?php namespace WL\Events\Listeners;

use Carbon\Carbon;
use Illuminate\Mail\Mailer;
use WL\Modules\Classes\Events\ClassNotifyProvider;
use WL\Modules\Notification\Facades\NotificationService;
use WL\Modules\Profile\Facades\ProfileService;

class ClassNotifyProviderSender
{
	protected $mailer;

	/**
	 * @param Mailer $mailer
	 */
	public function __construct(Mailer $mailer)
	{
		$this->mailer = $mailer;
	}

	public function handle(ClassNotifyProvider $classNotify)
	{
		if ($classNotify != null) {
			$profile = ProfileService::loadProfileBasicDetails($classNotify->clientId);

			NotificationService::createNotification(
				ProfileService::getProfileById($classNotify->clientId),
				ProfileService::getProfileById($classNotify->class->profile_id),
				$profile->full_name . _(" has booked class"),
				sprintf(
					$classNotify->class->name . _(' booked') . ' %s ' . _('times'),
					count($classNotify->registrationIdsDates)
				),
				new Carbon('next week'),
				ClassNotifyProviderSender::class
			);
		}
	}


}

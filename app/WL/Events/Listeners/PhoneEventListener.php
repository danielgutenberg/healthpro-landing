<?php namespace WL\Events\Listeners;

use Illuminate\Support\Facades\Mail;
use Log;
use Twilio;
use WL\Modules\Phone\Phone;
use Authy\AuthyApi;
use WL\Yaml\Facades\Yaml;

class PhoneEventListener {

	function verifying($phone)
	{
		$testingNumbers = [
			'6171111111' => ['972', '586699088'],
			'6172222223' => ['972', '526255076'],
			'6173333333' => ['972', '587627241'],
			'6174444444' => ['972', '525610702'],
			'6175555555' => ['64', '220951245'],
			'6176666666' => ['972', '529663644'],
			'8439011978' => ['1', '8439011978']
		];
		if (env('LIVE') == 'true') {
			$message = 'Your verification code is ' . $phone->activation_hash;
			$number = '+' . $phone->prefix . $phone->number;
			if ($phone->verified_by == Phone::VERIFIED_BY_SMS) {
				return Twilio::message($number, $message);
			} else {
				// send out the voice call
				$authy = new AuthyApi(Yaml::get('key','wl.phone.settings'));
				$authy->phoneVerificationStart($phone->number, $phone->prefix, 'call');
			}
        } else if (array_key_exists($phone->number, $testingNumbers)) {
			$message = 'Your verification code is ' . $phone->activation_hash;
			$number = '+' . $testingNumbers[$phone->number][0] . $testingNumbers[$phone->number][1];
			if ($phone->verified_by == Phone::VERIFIED_BY_SMS) {
				return Twilio::message($number, $message);
			} else {
				// send out the voice call
				$authy = new AuthyApi(Yaml::get('key','wl.phone.settings'));
				$authy->phoneVerificationStart($testingNumbers[$phone->number][1], $testingNumbers[$phone->number][0], 'call');
			}
		} else {
			Mail::send('emails.testing.sms_code', ['code' => $phone->activation_hash], function ($m) use ($phone) {
				$m->from('healthpro@healthpro.com', 'HealthPro');
				$m->to($phone->rel->owner()->email, 'Professional')->subject($phone->activation_hash  . ' Phone Activation code');
			});
		}
	}

}

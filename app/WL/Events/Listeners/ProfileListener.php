<?php namespace WL\Events\Listeners;


use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Support\Facades\Cookie;
use WL\Modules\Appointment\Facades\AppointmentService;
use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Modules\Profile\Models\ModelProfile;

class ProfileListener
{
	public function profileCreated($profile)
	{
		if ($profile->type == ModelProfile::CLIENT_PROFILE_TYPE) {
			AppointmentService::convertLockedNullClientAppointmentToReal($profile->id);
		}
	}


	public function profileChanged($profile)
	{
		if ($profile->type == ModelProfile::CLIENT_PROFILE_TYPE) {
			AppointmentService::convertLockedNullClientAppointmentToReal($profile->id);
			ProviderOriginatedService::approveOriginatorProvider($profile->id);
		}

		app(EncryptCookies::class)->disableFor('profile_type');

		Cookie::queue('profile_type', $profile->type, 0, null, null, false, false);
	}
}

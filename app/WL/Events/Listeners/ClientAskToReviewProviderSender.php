<?php namespace WL\Events\Listeners;

use Carbon\Carbon;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\View;
use WL\Modules\Client\Events\ClientAskToReviewProvider;
use WL\Modules\Notification\Facades\NotificationService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Settings\Facades\Setting;
use WL\Yaml\Facades\Yaml;

class ClientAskToReviewProviderSender
{
	protected $mailer;

	/**
	 * @param Mailer $mailer
	 */
	public function __construct(Mailer $mailer)
	{
		$this->mailer = $mailer;
	}

	public function handle(ClientAskToReviewProvider $askToReviewProvider)
	{
		if ($askToReviewProvider !== null) {
			$configuration = Yaml::parse('wl.reviews.reviews');

			$clientProfile = ProfileService::loadProfileBasicDetails($askToReviewProvider->client->id);
			$clientOwner = ProfileService::getOwner($askToReviewProvider->client->id);

			$providerProfile = ProfileService::loadProfileBasicDetails($askToReviewProvider->provider->id);
			$providerOwner = ProfileService::getOwner($askToReviewProvider->provider->id);
			// add notification
			NotificationService::createNotification(
				$providerOwner,
				$clientOwner,
				$configuration['emails']['provider']['title'],
				sprintf(
					'<a href="%s" data-review-notification-link>%s</a>',
					route('dashboard-my-providers') . '#review:' . $askToReviewProvider->provider->slug,
					sprintf($configuration['emails']['provider']['description'], $providerProfile->full_name)
				),
				new Carbon( $configuration['days_to_expire'] . ' days'),
				ClientAskToReviewProviderSender::class
			);

			// @TODO send email to the client
//			$emailData = [
//				'providerName' 	=> $providerProfile->full_name,
//				'providerSlug'	=> $providerProfile->slug,
//				'ratings'		=> range(1, 5),
//				'clientName'	=> $clientProfile->full_name,
//				'sender'		=> Setting::get('default_from_name'),
//			];
//
//			$this->mailer->queue('emails.reviews.review-provider', $emailData, function ($message) use ($clientProfile, $clientOwner, $configuration) {
//				$message->from('noreply@healthpro.com', 'HealthPro')
//					->to($clientOwner->email, $clientProfile->full_name)
//					->subject($configuration['review-email']['provider']['title']);
//			});
		}
	}
}

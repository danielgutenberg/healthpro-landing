<?php namespace WL\Events\Listeners;


use WL\Modules\Payment\Events\PaymentRefundEvent;
use WL\Modules\Search\Facades\OrderService;

class OrderEventListener
{
	public function paymentRefund(PaymentRefundEvent $event)
	{
		$orderItems = OrderService::getOrderItemsByOperationId($event->operationId);
		foreach ($orderItems as $orderItem) {
			OrderService::markOrderItemCancelled($orderItem);
		}
	}
}

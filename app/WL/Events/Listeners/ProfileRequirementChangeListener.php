<?php namespace WL\Events\Listeners;

use WL\Modules\Profile\Events\SearchableEventInterface;
use WL\Modules\Provider\Facades\ProviderService;

class ProfileRequirementChangeListener
{
	public function handle(SearchableEventInterface $sessionAddedRemoved)
	{
		ProviderService::adjustProviderStatus($sessionAddedRemoved->getProfileId());
	}
}

<?php
namespace WL\Events\Listeners;

use WL\Modules\User\Models\User as UserModel;
use Illuminate\Mail\Mailer;
use WL\Modules\User\Models\User;
use WL\Modules\User\Models\Reminder;
use WL\Modules\User\Models\Activation;
use Illuminate\Support\Facades\Session;
use WL\Modules\Appointment\Facades\AppointmentService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Models\ProviderProfile;
use Setting;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Wizard\Services\Facade\WizardService;

class UserListener
{
	protected $mailer;

	/**
	 * @param Mailer $mailer
	 */
	public function __construct(Mailer $mailer)
	{
		$this->mailer = $mailer;
	}

	public function register(User $user)
	{
		$activation = $user->activation();

		// make sure we have the activation model
		if (!$activation) {
			return;
		}

//		$this->mailer->send('emails.user.welcome', compact('activation', 'user'), function ($message) use ($user) {
//			/**
//			 * TODO grab the variables from the options
//			 */
//			$message->from('noreply@healthpro.com', 'HealthPro')
//				->to($user->email, $user->present()->name)
//				->subject('Welcome to HealthPro');
//		});
	}

	public function login(User $user)
	{
		// set current profile
		$profiles = ProfileService::getUserProfiles($user->id);
		if ($profile = $profiles->first()) {
			ProfileService::setCurrentProfileId($profile->id);
			if ($profile->type == ModelProfile::CLIENT_PROFILE_TYPE) {
				AppointmentService::convertLockedNullClientAppointmentToReal($profile->id);
			}
		}

	}

	public function forgot(Reminder $reminder)
	{
		$user = UserModel::find($reminder->user_id);

		$fromEmail = trim(Setting::get('default_from_email'));
		$fromName = Setting::get('default_from_name');
		$this->mailer->alwaysFrom($fromEmail, $fromName);

		$this->mailer->send('emails.user.reset', compact('reminder', 'user'), function ($message) use ($user) {
			$message
				->to($user->email, $user->present()->name)
				->subject('HealthPRO Member Password Reset');
		});
	}

	public function searchable($event)
	{
		$profile = $event->getProfile();
		$details = ProfileService::getNameAndEmail($profile->id);

		$fromEmail = trim(Setting::get('default_from_email'));
		$fromName = Setting::get('default_from_name');
		$this->mailer->alwaysFrom($fromEmail, $fromName);

		// dont send email to seeded data users
		if (strpos($details->email, 'healthpro+') == false && strpos($details->email, '@healthpro.com') == false) {
			$this->mailer->send('emails.user.searchable', ['name' => $details->name, 'slug' => $profile->slug], function ($message) use ($profile, $details) {
				$message
					->to($details->email, $details->name)
					->subject('Your HealthPRO profile is up and running!');
			});
		}
	}

	public function reset(Reminder $reminder)
	{
		if ($reminder->getOriginal('completed') !== $reminder->getAttribute('completed')) {

			// do something
		}
	}

	public function activate(User $user, Activation $activation)
	{
		// do something
	}

	/**
	 * Invite an user to manage profile ...
	 *
	 * @param User $user
	 */
	public function invite(User $user)
	{
		//@TODO send invitation to mail or use current notification system ...
	}

	/**
	 * Accept user to manage profile ...
	 *
	 * @param User $user
	 */
	public function accept(User $user)
	{
		//@TODO send invitation to mail or use current notification system ...
	}

	/**
	 * Exclude User to manage profile ...
	 *
	 * @param User $user
	 */
	public function exclude(User $user)
	{
		//@TODO send invitation to mail or use current notification system ...
	}

	/**
	 * Decline an invitation ...
	 *
	 * @param User $user
	 */
	public function decline(User $user, ProviderProfile $profile)
	{
		//@TODO send invitation to mail or use current notification system ...
	}

	public function logout()
	{
		ProfileService::invalidateCurrentProfileId();
		WizardService::cleanUpSession();
	}
}

<?php namespace WL\Events\Listeners;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use ReflectionClass;
use WL\Events\BasicEvents\BasicEmailEvent;
use WL\Events\BasicEvents\EmailAttachment;
use WL\Settings\Facades\Setting;
use WL\Yaml\Facades\Yaml;


class BasicEmailHealthproToClientDispatcher
{

	public function handle(BasicEmailEvent $mailEvent)
	{
		$className = (new ReflectionClass($mailEvent))->getShortName();
		$loadedBlades = $this->loadBladeConfig($className);
		$bodyBlade = $mailEvent->bodyBlade;
		$subjectBlade = $mailEvent->subjectBlade;

		if (empty($bodyBlade) && isset($loadedBlades['body'])) {
			$bodyBlade = $loadedBlades['body'];
		}

		if (empty($subjectBlade) && isset($loadedBlades['subject'])) {
			$subjectBlade = $loadedBlades['subject'];
		}

		$subject = View::make($subjectBlade, $mailEvent->bladeData)->render();

        if(is_null($mailEvent->emailFrom)) {
            $mailEvent->emailFrom = trim(Setting::get('default_from_email'));
            $mailEvent->nameFrom = trim(Setting::get('default_from_name'));
        }

		$mailEvent->bladeData['sourceEventClassName'] = $className;
		/*
		 * don't send emails if domain is @example.com -
		 */
		if (strpos($mailEvent->emailTo, '@example.com') == false || app()->environment('testing')) {
			$bladeData = $mailEvent->bladeData;
			if(isset($bladeData['customBody'])){
				$bladeData['content'] = $this->replaceVariables($bladeData['customBody'], $bladeData);
				$bodyBlade = 'emails.blank';
			}
			$prepMessage = function ($message) use ($mailEvent, $subject) {
				$message->from($mailEvent->emailFrom, $mailEvent->nameFrom)
					->to($mailEvent->emailTo, $mailEvent->nameTo)
					->subject($subject);

				if($mailEvent->copyCs && env('LIVE') == 'true') {
					$message->bcc(Yaml::get('customer_service_email', 'wl.mailEvents.config'));
				}

                if($mailEvent->attachment instanceof EmailAttachment) {
                    $attachment = $mailEvent->attachment;
                    $options = [
                        'as' => $attachment->getName(),
                        'mime' => $attachment->getMimeType(),
                    ];
                    if($attachment->getType() === EmailAttachment::TYPE_PATH) {
                        $message->attach($attachment->getPath(), $options);
                    } else {
                        $message->attachData($attachment->getContent(), $attachment->getName(), $options);
                    }
                }
			};

			if (!$mailEvent->shouldQueue) {
				Mail::send($bodyBlade, $bladeData, $prepMessage);
			} else {
				Mail::queue($bodyBlade, $bladeData, $prepMessage);
			}
		}

	}

	private function loadBladeConfig($className)
	{
		return Yaml::get($className, 'wl.mailEvents.mailBladesAliases');
	}

	private function replaceVariables($string, $data)
	{
		$replace = $string;
		foreach ($data as $key => $value){
			$replace = str_replace('%' . $key . '%', $value, $replace);
		}
		return $replace;
	}
}

<?php namespace WL\Events\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Events\Services\EventHelperServiceInterface;

class EventHelper extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */

	protected static function getFacadeAccessor()
	{
		return EventHelperServiceInterface::class;
	}
}

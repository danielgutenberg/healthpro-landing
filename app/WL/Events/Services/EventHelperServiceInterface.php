<?php namespace WL\Events\Services;

use WL\Events\BasicEvents\BasicEventInterface;

interface EventHelperServiceInterface
{
	public function fire(BasicEventInterface $event);
}

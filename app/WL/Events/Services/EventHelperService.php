<?php namespace WL\Events\Services;

use WL\Events\BasicEvents\BasicEventInterface;

class EventHelperService implements EventHelperServiceInterface
{
	public function fire(BasicEventInterface $event)
	{
		return event($event->getEventName(), $event);
	}
}

<?php namespace WL\Repositories;

abstract class DbRepository implements Repository
{
	protected $modelClassName;

	protected $model;

	/**
	 * @param $id
	 * @return mixed
	 */
	public function getById( $id )
	{
		return $this->model->findOrFail( $id );
	}

	/**
	 * @return mixed
	 */
	public function getModel()
	{
		return $this->model;
	}

	public function getModelClass()
	{
		return $this->model->getMorphClass();
	}

	/**
	 * @param array $data
	 * @return mixed
	 */
	public function createNew($data = [])
	{
		return new $this->model($data);
	}

	public function destroy( $id )
	{
		return $this->getById($id)->delete();
	}

	public function delete( array $ids )
	{
		$this->model->destroy( $ids );
	}

	public function getBy($field, $value)
	{
		return $this->model->where($field, '=', $value)->firstOrFail();
	}

	public function save($object)
	{
		return $object->save();
	}

	public function remove($object)
	{
		return $object->delete();
	}
}

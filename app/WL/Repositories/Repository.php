<?php namespace WL\Repositories;

interface Repository
{
	/**
	 * @param $id
	 * @return mixed
	 */
	public function getById($id);

	/**
	 * Creates a new object from a model class
	 * @return mixed
	 */
	public function createNew();

	/**
	 * Destroys the object and removes it from DB
	 *
	 * @param $id
	 * @return mixed
	 */
	public function destroy($id);

	/**
	 * Deletes multiple entries at once
	 *
	 * @param array $ids
	 * @return mixed
	 */
	public function delete(array $ids);

	/**
	 * @param $field
	 * @param $value
	 * @return mixed
	 */
	public function getBy($field, $value);


	/**
	 * Save entity
	 * @param $object
	 * @return mixed
	 */
	public function save($object);


	/**
	 * Remove entity
	 * @param $object
	 * @return mixed
	 */
	public function remove($object);
}

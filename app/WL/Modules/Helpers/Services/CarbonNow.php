<?php namespace WL\Modules\Helpers\Services;

use Carbon\Carbon;

class CarbonNow
{
	public static function now($tz = null)
	{
		return Carbon::now($tz);
	}
}

<?php namespace WL\Modules\Helpers\Services;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Crypt;
use WL\Modules\Appointment\Facades\AppointmentService;
use WL\Modules\Appointment\Models\ProviderAppointment;
use WL\Modules\GiftCertificates\Models\GiftCertificate;
use WL\Modules\Location\Facades\LocationService;
use WL\Modules\Location\Models\Location;
use WL\Modules\Location\Models\LocationType;
use WL\Modules\OAuth\Facades\OAuthService;
use WL\Modules\Order\Models\OrderItem;
use WL\Modules\Payment\Adapters\EntityType;
use WL\Modules\Payment\Facades\PaymentMethodService;
use WL\Modules\PricePackage\Facade\PricePackageService;
use WL\Modules\PricePackage\Models\PricePackage;
use WL\Modules\PricePackage\Models\PricePackageCharge;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\User\Facades\User;
use WL\Schedule\ScheduleAvailability;
use WL\Schedule\ScheduleRegistration;

class EventInfoBuilderService
{

	public function collectProfileInfo($profileId, $dataNamePrefix = 'client')
	{
		$profile = ProfileService::loadProfileBasicDetails($profileId);
		$profileOwner = ProfileService::getOwner($profileId);

		$data = [
			$dataNamePrefix . 'Name' => $profile->full_name,
			$dataNamePrefix . 'FirstName' => $profile->first_name,
			$dataNamePrefix . 'LastName' => $profile->last_name,
			$dataNamePrefix . 'Email' => $profileOwner->email,
			$dataNamePrefix . 'Slug' => $profile->slug,
			$dataNamePrefix . 'Id' => $profile->id,
			$dataNamePrefix . 'UserId' => $profileOwner->id,
			$dataNamePrefix . 'ItemType' => $profile->type,
			$dataNamePrefix . 'ProfileId' => $profile->id,
			$dataNamePrefix . 'User' => $profileOwner
		];

		return $data;
	}

	public function collectProviderAppointmentInfoId($appointmentId)
	{
		return $this->collectProviderAppointmentInfo(AppointmentService::getAppointment($appointmentId));
	}

	public function collectProviderAppointmentInfo(ProviderAppointment $appointment)
	{
		$providerData = $this->collectProfileInfo($appointment->provider_id, 'provider');
		$clientData = $this->collectProfileInfo($appointment->client_id, 'client');

		$location = LocationService::getLocation($appointment->location_id);

		if ($location->locationType->type == LocationType::VIRTUAL) {
			$locationAddress = $location->virtualAddress->note;
		} else {
			$address = $location->address;
			$postalCode = $address->postalCode;

			$locationAddress = $address->address . ', ' . $postalCode->city . ', ' . $postalCode->province . ', ' . $postalCode->code;
		}

		$serviceNames = [];
		$appointmentService = $appointment->service()->first();
		if ($appointmentService != null) {
			$tags = $appointmentService->serviceTypes()->get();
			foreach ($tags as $tag) {
				$serviceNames[] = $tag->name;
			}
		}
		$serviceAllNames = implode(',', $serviceNames);
		$clientUser = $clientData['clientUser'];
		$data = [
			'appointment' => $appointment,
			'appointmentId' => $appointment->id,

			'day' => $appointment->date->format('D, M jS, Y'),
			'from' => $appointment->from->setDate($appointment->date->year, $appointment->date->month, $appointment->date->day)->format('h:iA'),
			'until' => $appointment->until->setDate($appointment->date->year, $appointment->date->month, $appointment->date->day)->format('h:iA'),
			'formattedStartTime' => $appointment->from->setDate($appointment->date->year, $appointment->date->month, $appointment->date->day)->format('Y-m-d\TH:i:s'),

			'address' => $locationAddress,
			'locationType' => $location->locationType->type,
			'serviceNames' => $serviceNames,
			'serviceAllNames' => $serviceAllNames,
			'newUser' => User::isManualPasswordRequired($clientData['clientEmail']) && !(count($clientUser->getActiveSocials()) > 0)
		];

		return array_merge($data, $clientData, $providerData);
	}

	public function collectProviderPackageInfo(PricePackage $package, OrderItem $orderItem)
	{
		$clientId = $orderItem->order->profile_id;

		$providerData = $this->collectProfileInfo($package->profile_id, 'provider');
		$clientData = $this->collectProfileInfo($clientId, 'client');

		$serviceNames = [];
		$packageService = $package->entity->service;
		if ($packageService != null) {
			$serviceNames = $packageService->serviceTypes()->get()->pluck('name')->toArray();
		}

		$data = [
			'packageId' => $package->id,
			'serviceNames' => $serviceNames,
			'serviceAllNames' => implode(',', $serviceNames)
		];

		$firstAppointment = $orderItem->order->items->filter(function($item) {
			return $item->type == OrderItem::PURCHASABLE_TYPE && $item->entity_type == 'appointment';
		})->map(function($item) {
			return $item->entity;
		})->sortBy(function($appointment) {
			return $appointment->from_utc;
		})->first();

		if ($firstAppointment) {
			$data = array_merge($data, $this->collectLocationInfoId($firstAppointment->location_id), [
				'day' => $firstAppointment->date->format('D, M jS, Y'),
				'from' => $firstAppointment->from->setDate($firstAppointment->date->year, $firstAppointment->date->month, $firstAppointment->date->day)->format('h:iA'),
				'until' => $firstAppointment->until->setDate($firstAppointment->date->year, $firstAppointment->date->month, $firstAppointment->date->day)->format('h:iA'),
				'formattedStartTime' => $firstAppointment->from->setDate($firstAppointment->date->year, $firstAppointment->date->month, $firstAppointment->date->day)->format('Y-m-d\TH:i:s'),
			]);
		}

		$charge = PricePackageService::getChargedPackage($clientId, $package->entity_id, $package->entity_type);
		if ($charge) {
			$data = array_merge($data, [
				'chargePackageId' => $charge->id,
				'autoRenew' => $charge->auto_renew,
				'expiresOn' => Carbon::parse($charge->expires_on)->format('D, M jS, Y'),
				'bookedThrough' => Carbon::parse($charge->booked_through)->format('D, M jS, Y'),
			]);
		}

		return array_merge($data, $clientData, $providerData);
	}

	public function collectLocationInfoId($locationId)
	{
		$location = LocationService::getLocation($locationId);

		return $this->collectLocationInfo($location);
	}

	public function collectLocationInfo(Location $location)
	{
		if ($location->locationType->type == LocationType::VIRTUAL) {
			$locationAddress = $location->virtualAddress->note;
		} else {
			$address = $location->address;
			$postalCode = $address->postalCode;

			$locationAddress = $address->address . ', ' . $postalCode->city . ', ' . $postalCode->province . ', ' . $postalCode->code;
		}

		return [
			'address' => $locationAddress,
			'locationType' => $location->locationType->type,
		];
	}

	public function collectPaymentMethodInfo($paymentMethodId)
	{
		$paymentMethodContainer = PaymentMethodService::getPaymentMethod($paymentMethodId);
		$clientCardNumber = '-';
		if ($paymentMethodContainer->getPaymentMethod()->entity_type == EntityType::CARD) {
			$clientCardNumber = $paymentMethodContainer->getSourceEntity()->card_number;
		}

		return [
			'clientCardNumber' => $clientCardNumber,
		];
	}


	public function collectScheduleRegistrationInfo(ScheduleRegistration $registration)
	{
		$providerData = $this->collectProfileInfo($registration->provider_id, 'provider');

		$location = LocationService::getLocation($registration->location_id);

		if ($location->locationType->type == LocationType::VIRTUAL) {
			$locationAddress = $location->virtualAddress->note;
		} else {
			$address = $location->address;
			$postalCode = $address->postalCode;

			$locationAddress = $address->address . ', ' . $postalCode->city . ', ' . $postalCode->province . ', ' . $postalCode->code;
		}

		$data = [

			'registrationId' => $registration->id,
			'day' => $registration->date->format('D, M jS, Y'),
			'from' => $registration->from->setDate($registration->date->year, $registration->date->month, $registration->date->day)->format('h:iA'),
			'until' => $registration->until->setDate($registration->date->year, $registration->date->month, $registration->date->day)->format('h:iA'),
			'locationType' => $location->locationType->type,
			'address' => $locationAddress,

		];

		return array_merge($data, $providerData);


	}

	public function collectProviderAppointmentConfirmationInfo(ProviderAppointment $appointment, $userRegisteredByProvider)
	{
		$appointmentData = $this->collectProviderAppointmentInfo($appointment);

		$confirmUrlCodeObject = [
			'appointmentId' => $appointment->id,
			'userRegisteredByProvider' => $userRegisteredByProvider,
			'userId' => $appointmentData['clientUserId'],
			'professionalProfileId' => $appointment->provider_id,
		];

		$data = [
			'userRegisteredByProvider' => $userRegisteredByProvider,
			'appointmentUrlData' => ['code' => base64_encode(Crypt::encrypt($confirmUrlCodeObject))],
		];

		return array_merge($appointmentData, $data);

	}

    public function collectProviderAppointmentConfirmation(ProviderAppointment $appointment)
    {
        $appointmentData = $this->collectProviderAppointmentInfo($appointment);

        $code =  base64_encode(Crypt::encrypt([
            'appointmentId' => $appointment->id,
            'userId' => $appointmentData['clientUserId'],
            'professionalProfileId' => $appointment->provider_id,
        ]));

        $appointmentData['appointmentUrlData'] = ['code' => $code];

        if($appointmentData['locationType'] === LocationType::VIRTUAL) {
            $appointmentData['address'] = 'Online - ' . $appointmentData['address'];
        }

        return $appointmentData;
	}

	public function collectProviderAppointmentUpdatedInfo(ProviderAppointment $appointment)
	{
		$appointmentData = $this->collectProviderAppointmentInfo($appointment);
		$userRegisteredByProvider = User::isRegisteredByProvider($appointmentData['clientEmail']);

		$confirmUrlCodeObject = [
			'appointmentId' => $appointment->id,
			'userRegisteredByProvider' => $userRegisteredByProvider,
			'userId' => $appointmentData['clientUserId'],
			'professionalProfileId' => $appointment->provider_id,
		];

		$data = [
			'isAppointmentConfirmationNeed' => $appointment->status === ProviderAppointment::STATUS_PENDING,
			'appointmentUrlData' => ['code' => base64_encode(Crypt::encrypt($confirmUrlCodeObject))],
			'userRegisteredByProvider' => $userRegisteredByProvider,
		];

		return array_merge($appointmentData, $data);
	}

	public function collectClientInvite($clientProfileId, $providerProfileId, $customBody = null, $couponDiscount = null, $newClient = false)
	{
		$data = array_merge(
			$this->collectProfileInfo($clientProfileId),
			$this->collectProfileInfo($providerProfileId, 'provider')
		);
		$data['clientName'] = explode(" ", $data['clientName'])[0];
		$profileUrl = route('profile-show', [$data['providerSlug']]);
		$data['profileLink'] = '<a href="' . $profileUrl . '">' . $profileUrl . '</a>';

		if(!$newClient){
			$confirmCode = Crypt::encrypt([
				'email' => $data['clientEmail'],
				'providerId' => $providerProfileId,
			]);
			$confirmUrl = route('client-confirm-invite', base64_encode($confirmCode));
			$data['confirmLink'] = '<a href="' . $confirmUrl . '">Click here</a>';

			$declineCode = Crypt::encrypt([
				'email' => $data['clientEmail'],
				'providerId' => $providerProfileId,
				'decline' => 1,
			]);
			$declineUrl = route('client-confirm-invite', base64_encode($declineCode));
			$data['declineLink'] = '<a href="' . $declineUrl . '">Decline</a>';
		} else {
			$confirmCode = Crypt::encrypt([
				'email' => $data['clientEmail'],
				'providerId' => $providerProfileId,
			]);
			$confirmUrl = route('user-confirm-invite', base64_encode($confirmCode));
			$data['confirmLink'] = '<a href="' . $confirmUrl . '">Click here</a>';
		}
		$contactUsLink = route('contact-us');
		$data['contactUsLink'] = '<a href="' . $contactUsLink . '">Contact our friendly support team today for assistance.</a>';
		$data['customBody'] = $this->escapeCustomBody($customBody);
		$data['couponDiscount'] = $couponDiscount;
		return $data;
	}

	public function collectProviderGiftCertificateInfo(GiftCertificate $certificate)
	{
		$providerData = $this->collectProfileInfo($certificate->provider_id, 'provider');
		$senderData = $this->collectProfileInfo($certificate->sender_id, 'sender');
		$receiverData = $this->collectProfileInfo($certificate->client_id, 'receiver');

		$session = ProviderService::getServiceSession($certificate->entity_id);

		$service = $session->service;

		$sendDate = Carbon::parse($certificate->send_date);

		$duration = [
			'm' => $session->duration % 60,
			'h' => $session->duration / 60 % 60,
		];

		$durationLabel = '';
		if ($duration['h']) {
			$durationLabel .= ($duration['h'] . ' ' . ($duration['h'] > 1 ? 'hrs' : 'hr') . ' ');
		}
		if ($duration['m']) {
			$durationLabel .= $duration['m'] . ' min';
		}

		$bookUrlCodeObject = [
			'providerId'    => $certificate->provider_id,
			'userId'	    => $receiverData['receiverUserId'],
			'clientId'	    => $receiverData['receiverProfileId'],
			'certificateId' => $certificate->id,
		];

		$data = [
			'msg' 				=> nl2br(trim($certificate->message)),
			'serviceName' 		=> $service->name,
			'sessionDuration' 	=> $durationLabel,
			'sendImmidiately'	=> $sendDate->isToday(),
			'deliveryDate'		=> $sendDate->format('D, M jS, Y'),
			'bookNowUrl'		=> route('redeem-gift-certificate', [
				'code' => base64_encode(Crypt::encrypt($bookUrlCodeObject))
			]),
		];

		return array_merge($data, $providerData, $senderData, $receiverData);
	}


	private function escapeCustomBody($customBody)
	{
		if ($customBody == null) {
			return null;
		}

		$replacements = [
			'client' => '%clientName%',
			'professional_name' => '%providerName%',
			'professional_public_url' => '%profileLink%',
			'confirm_link' => '%confirmLink%',
			'contact_us_link' => '%contactUsLink%',
			'p' => '<p>',
			'br' => '<br>',
		];

		$replacedBody = $customBody;

		foreach ($replacements as $tag => $placeholder){
			$replacedBody = str_replace('<' . $tag . '>', $placeholder, $replacedBody);
			$replacedBody = str_replace('&lt;' . $tag . '&gt;', $placeholder, $replacedBody);
		}

		# allow only <p> and <br> tags
		return strip_tags($replacedBody, '<p><br><a>');
	}

	public function collectOauthData($oauthId)
	{
	    $oauth = OAuthService::getProfileOAuth($oauthId);
		$data = $this->collectProfileInfo($oauth->profile_id, 'profile');
		$data['provider'] = $oauth->provider;
		return $data;
	}

	public function collectProviderPricePackageInfo(PricePackageCharge $pricePackageCharge)
	{
		$package = $pricePackageCharge->package()->withTrashed()->first();

		$providerData = $this->collectProfileInfo($package->profile_id, 'provider');
		$clientData = $this->collectProfileInfo($pricePackageCharge->client_id, 'client');


		$serviceNames = [];
		$packageService = $package->entity->service;
		if ($packageService != null) {
			$serviceNames = $packageService->serviceTypes()->get()->pluck('name')->toArray();
		}

		$data = [
			'pricePackageId' => $pricePackageCharge->id,
			'packageId' => $package->id,
			'serviceNames' => $serviceNames,
			'serviceAllNames' => implode(',', $serviceNames),
			'expiresOn' => Carbon::parse($pricePackageCharge->expires_on)->format('D, M jS, Y'),
			'bookedThrough' => Carbon::parse($pricePackageCharge->booked_through)->format('D, M jS, Y'),
		];

		$lastAppointment = $this->getPricePackageChargeAppointments($pricePackageCharge)
			->sortBy(function($appointment) {
				return $appointment->from_utc;
			})->last();

		if ($lastAppointment) {
			$data = array_merge($data, $this->collectLocationInfoId($lastAppointment->location_id), [
				'day' => $lastAppointment->date->format('D, M jS, Y'),
				'from' => $lastAppointment->from->setDate($lastAppointment->date->year, $lastAppointment->date->month, $lastAppointment->date->day)->format('h:iA'),
				'until' => $lastAppointment->until->setDate($lastAppointment->date->year, $lastAppointment->date->month, $lastAppointment->date->day)->format('h:iA'),
				'formattedStartTime' => $lastAppointment->from->setDate($lastAppointment->date->year, $lastAppointment->date->month, $lastAppointment->date->day)->format('Y-m-d\TH:i:s'),
			]);
		}

		return array_merge($data, $clientData, $providerData);
	}


	protected function getPricePackageChargeAppointments($pricePackageCharge)
	{
		$appointments = new Collection();
		foreach ($pricePackageCharge->operations as $operation) {
			if ($operation->entity && $operation->entity instanceof ProviderAppointment && $operation->entity->registration !== null) {
				$appointments->push($operation->entity);
			}
		}

		return $appointments;
	}

	public function getRegistrationIcsData(ScheduleRegistration $registration)
	{
		$result = (object) $registration->toArray();
		$result->location = $this->collectLocationInfo($registration->location)['address'];
		$result->summary = '';
		$result->description = '';
		switch ($registration->availability_type) {
			case ScheduleAvailability::TYPE_APPOINTMENT:
				$appointment = AppointmentService::getByRegistrationId($registration->id);
				$result->entity = $this->collectProviderAppointmentInfo($appointment);
				break;
		}
		return $result;
	}

}

<?php namespace WL\Modules\Helpers\Providers;

use Illuminate\Support\ServiceProvider;
use WL\Modules\Appointment\Models\ProviderAppointment;
use WL\Modules\Appointment\Repositories\ProviderAppointmentRepository;
use WL\Modules\Appointment\Services\AppointmentService;
use WL\Modules\Appointment\Services\AppointmentServiceInterface;
use WL\Modules\Helpers\Services\CarbonNow;
use WL\Modules\Helpers\Services\EventInfoBuilderService;

class HelpersServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton(EventInfoBuilderService::class, function () {
			return new EventInfoBuilderService();
		});

		$this->app->singleton(CarbonNow::class, function () {
			return new CarbonNow();
		});
		
		
	}
}

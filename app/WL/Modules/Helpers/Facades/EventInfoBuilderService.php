<?php namespace WL\Modules\Helpers\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Helpers\Services\EventInfoBuilderService;


class EventInfoBuilder extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return EventInfoBuilderService::class;
	}
}

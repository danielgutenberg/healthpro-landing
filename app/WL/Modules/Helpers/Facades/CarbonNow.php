<?php namespace WL\Modules\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class CarbonNow extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return \WL\Modules\Helpers\Services\CarbonNow::class;
	}
}

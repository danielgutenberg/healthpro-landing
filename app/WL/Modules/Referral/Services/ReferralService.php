<?php namespace WL\Modules\Referral\Services;

use WL\Modules\User\Models\User;
use Event;
use Mail;
use WL\Modules\Referral\Contracts\ReferrableContract;
use WL\Modules\Referral\Contracts\ReferralRepositoryContract;
use WL\Modules\Referral\Contracts\ReferralServiceContract;
use WL\Modules\Referral\ReferralException;
use WL\Modules\User\Services\UserServiceInterface;

class ReferralService implements ReferralServiceContract {

	const REFERRAL_EMAIL_INVITATION_PATH = 'emails.referral.invitation';

	/**
	 * @var ReferralRepositoryContract
	 */
	private $referralRepository;

	/**
	 * @var UserServiceInterface
	 */
	private $userService;

	public function __construct(ReferralRepositoryContract $referralRepository, UserServiceInterface $userService) {

		$this->referralRepository = $referralRepository;
		$this->userService = $userService;
	}

	/**
	 * Check if user is registered as referred .
	 *
	 * @param \WL\Modules\User\Models\User $user
	 * @return mixed
	 */
	public function isReferred(User $user) {
		$referral = $this->referralRepository
			->getReferred($user);

		if( isset($referral->id) )
			return true;

		return false;
	}

	/**
	 * Get all referrer referrals .
	 *
	 * @param \WL\Modules\User\Models\User $referrer
	 */
	public function getReferrals(User $referrer) {
		$referrals = $this->referralRepository
			->getReferrals($referrer);

		return $referrals->map(function($referral) {
			return $this->userService->getUserById(
				$referral->referred_id
			);
		});
	}

	/**
	 * Get referrer by code ..
	 *
	 * @param $code
	 * @return mixed
	 */
	public function getReferrerByCode($code) {
		return $this->userService->getUserByReferral($code);
	}

	/**
	 * Get / generate user referral code ..
	 *
	 * @param User|ReferrableContract $user
	 * @param bool $generateIfEmpty
	 * @return bool
	 */
	public function myReferralCode(ReferrableContract $user, $generateIfEmpty = true) {
		if(! $user->getReferralCode()) {
			if( $generateIfEmpty ) {
				$code = bin2hex(openssl_random_pseudo_bytes(16));

				$user->setReferralCode($code);
			}
		}

		return $user->getReferralCode();
	}

	/**
	 * Activate referral ..
	 *
	 * @param $code
	 * @param \WL\Modules\User\Models\User $referred
	 * @return mixed
	 * @throws ReferralException
	 */
	public function confirmReferral($code, User $referred) {
		if( $this->isReferred($referred))
			return false;

		$referrer = $this->getReferrerByCode($code);

		if(! isset($referrer->id))
			throw new ReferralException(_('Invalid code'));

		$this->referralRepository
			->registerReferral($referrer, $referred);

		return Event::fire('referral.registered', ['referred' => $referred]);
	}

	/**
	 * Send an invitation to mail ...
	 *
	 * @param ReferrableContract $referrable
	 * @param array $emails
	 * @return $this
	 * @throws ReferralException
	 */
	public function sendReferralCode(ReferrableContract $referrable, array $emails = []) {
		if( ! $emails )
			throw new ReferralException('Invalid receivers');

		$code = $this->myReferralCode($referrable);

		array_walk($emails, function($email) use($code, $referrable) {
			list($name, $suffix) = explode('@', $email);

			if(! $referrableName = $referrable->full_name)
				list($referrableName, $referrableSuffix) = explode('@', $referrable->email);

			Mail::send(self::REFERRAL_EMAIL_INVITATION_PATH, compact('code', 'name', 'referrableName'), function ($message) use ($email, $referrable)  {
				$message
					->from($referrable->email)
					->to($email)
					->subject(_('Invitation to register'));
			});
		});

		return $this;
	}
}

<?php namespace WL\Modules\Referral\ServiceProviders;

use Illuminate\Support\ServiceProvider;
use WL\Modules\Referral\Contracts\ReferralRepositoryContract;
use WL\Modules\Referral\Contracts\ReferralServiceContract;
use WL\Modules\Referral\Repositories\ReferralRepository;
use WL\Modules\Referral\Services\ReferralService;

class ReferralServiceProvider extends ServiceProvider {

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->singleton(ReferralRepositoryContract::class, ReferralRepository::class);

		$this->app->singleton(ReferralServiceContract::class, function($app) {
			return new ReferralService(
				$app[ReferralRepositoryContract::class],
				$app['user']
			);
		});
	}
}

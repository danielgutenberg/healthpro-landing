<?php namespace WL\Modules\Referral\Contracts;

interface ReferrableContract {

	/**
	 * Set new referrer code ..
	 *
	 * @param $code
	 * @return mixed
	 */
	public function setReferralCode($code);

	/**
	 * Get referrer code .
	 *
	 * @return mixed
	 */
	public function getReferralCode();

}

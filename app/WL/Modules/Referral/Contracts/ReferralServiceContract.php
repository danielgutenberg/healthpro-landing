<?php namespace WL\Modules\Referral\Contracts;

use WL\Modules\User\Models\User;

interface ReferralServiceContract {

	/**
	 * Check if user is registered as referred .
	 *
	 * @param User $user
	 * @return mixed
	 */
	public function isReferred(User $user);

	/**
	 * Get referrer by code ..
	 *
	 * @param $code
	 * @return mixed
	 */
	public function getReferrerByCode($code);

	/**
	 * Activate referral ..
	 *
	 * @param $code
	 * @param User $referred
	 * @return mixed
	 */
	public function confirmReferral($code, User $referred);
}

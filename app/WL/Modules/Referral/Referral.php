<?php namespace WL\Modules\Referral;

class Referral {

	public $id;
	public $referrer_id;
	public $referred_id;
	public $confirmed;

	public function __construct($id, $referrer_id, $referred_id, $confirmed) {
		$this->id = $id;
		$this->referrer_id = $referrer_id;
		$this->referred_id = $referred_id;
		$this->confirmed = $confirmed;
	}

	/**
	 * Is referral confirmed ..
	 *
	 * @return bool
	 */
	public function isConfirmed() {
		return (bool)$this->confirmed;
	}

	/**
	 * Confirm referral ..
	 */
	public function confirm() {
		$this->confirmed = 1;
	}
}

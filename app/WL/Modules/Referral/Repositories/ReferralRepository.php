<?php namespace WL\Modules\Referral\Repositories;

use WL\Modules\User\Models\User;
use DB;
use WL\Modules\Referral\Contracts\ReferralRepositoryContract;
use WL\Modules\Referral\Referral;
use WL\Repositories\DbRepository;

class ReferralRepository extends DbRepository implements ReferralRepositoryContract {

	const TABLE_NAME = 'referrals';

	/**
	 * Factory referral .
	 *
	 * @param $referralRaw
	 * @return Referral
	 */
	protected function factory($referralRaw) {
		return new Referral(
			$referralRaw->id,
			$referralRaw->referrer_id,
			$referralRaw->referred_id,
			$referralRaw->confirmed
		);
	}

	/**
	 * Get referred by user id ..
	 *
	 * @param User $user
	 * @return Referral
	 */
	public function getReferred(User $user) {
		$referralRaw = DB::table(self::TABLE_NAME)
			->where('referred_id', $user->id)
			->first();

		if( isset($referralRaw->id) )
			return self::factory($referralRaw);
	}

	/**
	 * Get all referrals by referrer .
	 *
	 * @param \WL\Modules\User\Models\User $referrer
	 * @return static
	 */
	public function getReferrals(User $referrer) {
		$referrals = collect(
			DB::table(self::TABLE_NAME)
				->where('referrer_id', $referrer->id)
				->get()
		);

		return $referrals->map(function($referral) {
			return $this->factory($referral);
		});
	}

	/**
	 * Get by id ..
	 *
	 * @param $id
	 * @return mixed|static
	 */
	public function getById($id) {
		return DB::table(self::TABLE_NAME)
			->where('id', $id)
			->first();
	}

	/**
	 * Register new referral .
	 *
	 * @param User $referrer
	 * @param User $referred
	 * @param bool $confirmed
	 * @return Referral
	 */
	public function registerReferral(User $referrer, User $referred, $confirmed = true) {
		$id = DB::table(self::TABLE_NAME)
			->insertGetId([
				'referrer_id' => $referrer->id,
				'referred_id' => $referred->id,
				'confirmed'   => $confirmed,
			]);

		$referralRaw = $this->getById($id);

		return $this->factory($referralRaw);
	}
}

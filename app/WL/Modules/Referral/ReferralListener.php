<?php namespace WL\Modules\Referral;

use WL\Modules\User\Models\User;
use Illuminate\Mail\Mailer;
use WL\Modules\Referral\Contracts\ReferralServiceContract;

class ReferralListener {

	const REFERRAL_CODE_SESSION_KEY = 'referral_code';

	protected $mailer;

	protected $configurations;

	/**
	 * @param Mailer $mailer
	 */
	public function __construct(Mailer $mailer) {
		$this->mailer = $mailer;
	}

	/**
	 * Register all events handler ...
	 *
	 * @param $events
	 */
	public function subscribe($events) {
		$events->listen('user.referral.registered', self::class. '@onUserRegister');
	}


	/**
	 * On User register add new row to referrals table to register referrer id  ...
	 *
	 * @internal param User $user
	 * @param User $referred
	 */
	public function onUserRegister(User $referred) {
		if( $code = session(self::REFERRAL_CODE_SESSION_KEY) )
			app(ReferralServiceContract::class)->confirmReferral($code, $referred);
	}

}

<?php namespace WL\Modules\Provider\SecurityPolicies;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\SecurityPolicies\ProfileAccessPolicy;
use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Security\SecurityPolicies\BaseAccessPolicy;

/**
 * Class ProviderServiceAccessPolicy
 * @package WL\Modules\Provider\SecurityPolicies
 */
class ProviderServiceAccessPolicy extends BaseAccessPolicy
{
    protected $profilePolicy;

    public function __construct()
    {
        $this->profilePolicy = new ProfileAccessPolicy();
    }

	/**
	 * Can add provider services ?
	 * @param $profileId
	 * @return bool
	 */
	public function canAddProviderService($profileId)
	{
		return $this->profilePolicy->isCurrentProfile($profileId);
	}


	/**
	 * Can load provider services ?
	 * @param $profileId
	 * @return bool
	 */
	public function canLoadProviderServices($profileId)
	{
		return $this->isOwner($profileId, ProfileService::getCurrentProfileId());
	}

	/**
	 * Can delete provider service?
	 * @param $providerId
	 * @param $providerServiceId
	 * @return bool
	 */
	public function canDeleteProviderService($providerId, $providerServiceId)
	{
		return $this->canUpdateProviderService($providerId, $providerServiceId);
	}

	/**
	 * Can update provider service?
	 * @param $providerId
	 * @param $providerServiceId
	 *
	 * @return bool
	 */
	public function canUpdateProviderService($providerId, $providerServiceId)
	{
		try {
			$providerService = ProviderService::getService($providerServiceId);
		} catch (ModelNotFoundException $ex) {
			return false;
		}

		return $this->isOwner($providerId, $providerService->profile_id);
	}

	/**
	 * Can update provider service session?
	 * @param $providerId
	 * @param $providerServiceSessionId
	 * @return bool
	 *
	 */
	public function canDeleteProviderServiceSession($providerId, $providerServiceSessionId)
	{

		try {
			$providerServiceSession = ProviderService::getServiceSession($providerServiceSessionId);
		} catch (ModelNotFoundException $ex) {
			return false;
		}

		return $this->canUpdateProviderService($providerId, $providerServiceSession->provider_service_id);
	}

	/**
	 * @param int $providerId
	 * @param string $feature
	 * @return bool
	 */
	public function hasFeature($providerId, $feature)
	{
		return ProviderMembershipService::hasFeature($providerId, $feature);
	}

    /**
     * @param int $providerId
     * @return bool
     */
    public function canAccessBillingDetails($providerId)
    {
        return $this->profilePolicy->isCurrentProfile($providerId);
	}

    /**
     * @param int $providerId
     * @return bool
     */
    public function canAccessActivePlanDetails($providerId)
    {
        return $this->profilePolicy->isCurrentProfile($providerId);
	}

    /**
     *
     * @param int $providerId
     * @return bool
     */
    public function canChangeActivePlan($providerId)
    {
        return $this->profilePolicy->isCurrentProfile($providerId);
	}

    /**
     *
     * @param int $providerId
     * @return bool
     */
    public function canCancelActivePlan($providerId)
    {
        return $this->profilePolicy->isCurrentProfile($providerId);
	}
}

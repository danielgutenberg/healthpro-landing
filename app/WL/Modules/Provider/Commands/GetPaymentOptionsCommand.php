<?php namespace WL\Modules\Appointment\Commands;

use Illuminate\Contracts\Validation\ValidationException;
use Validator;
use WL\Modules\Payment\Adapters\EntityType;
use WL\Modules\Payment\Facades\PaymentService;
use WL\Modules\Payment\Models\PaymentMethod;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Security\Commands\AuthorizableCommand;

class GetPaymentOptionsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'provider.loadProviderServices';

	private $profileId;
	private $validator;

	function __construct($profileId)
	{
		$this->profileId = $profileId;

		$this->validator = Validator::make(
			[
				'profile_id' => $this->profileId,
			],
			[
				'profile_id' => 'required',],
			[
				'profile_id.required' => __('Profile Id must be supplied.'),
			]
		);
	}

	public function handle(ModelProfileService $service)
	{
		if ($this->validator->fails()) {
			throw new ValidationException($this->validator);
		}

		$acceptPaymentTypes = PaymentService::professionalAccepts($this->profileId);

		$professionalSettings = $service->loadProfileMetaField($this->profileId, 'accepted_payment_methods', PaymentMethod::ONLY_IN_PERSON);

		$result = (object)[
			'in_person' => $professionalSettings != PaymentMethod::ONLY_ONLINE,
			'card'   => in_array(EntityType::CARD, $acceptPaymentTypes),
			'paypal' => in_array(EntityType::PAYPAL, $acceptPaymentTypes),
		];

		return $result;
	}
}

<?php namespace WL\Modules\Provider\Commands;


use WL\Modules\Profile\Commands\ValidatableCommand;
use WL\Modules\Provider\SecurityPolicies\ProviderServiceAccessPolicy;


class ProviderBaseCommand extends ValidatableCommand
{
    protected $securityPolicy;

	public function __construct($inpData = [])
	{
		parent::__construct($inpData);
        $this->securityPolicy = new ProviderServiceAccessPolicy();
	}
}

<?php namespace WL\Modules\Provider\Commands;


use WL\Modules\Provider\Exceptions\MembershipException;
use WL\Modules\Provider\Exceptions\ProviderAccessException;
use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Validation\Exceptions\InvalidParametersException;

class ChangeProviderPlanCommand extends ProviderBaseCommand
{
    const IDENTIFIER = 'membership.changeProviderPlan';

	protected $rules = [
		'profileId' => 'required|int',
		'productId' => 'required|int',
	];

	public function validHandle()
    {
        $profileId = $this->inputData['profileId'];
        $productId = $this->inputData['productId'];

        if(!$this->securityPolicy->canChangeActivePlan($profileId)) {
            throw new ProviderAccessException();
        }

        $plan = ProviderMembershipService::getActivePlan($profileId);
        if(!$plan) {
            throw new MembershipException('No active plan for this professional');
        }
        $product = ProviderMembershipService::getProduct($productId);
        if(!$product) {
            throw new InvalidParametersException('Product not found');
        }

        ProviderMembershipService::changeProviderPlan($plan, $product);

        if($plan = ProviderMembershipService::getActivePlan($profileId)) {
            return ProviderMembershipService::getPlanDetails($plan);
        }
    }
}

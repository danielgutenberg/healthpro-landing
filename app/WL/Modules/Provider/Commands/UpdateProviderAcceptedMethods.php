<?php namespace WL\Modules\Provider\Commands;


use WL\Modules\Payment\Models\PaymentMethod;
use WL\Modules\Profile\Commands\ValidatableCommand;
use WL\Modules\Provider\Facades\ProviderService;

class UpdateProviderAcceptedMethods extends ValidatableCommand
{
    const IDENTIFIER = 'provider.updateAcceptedMethods';

    protected $rules = [
        'profileId' => 'required|int',
        'method' => 'required|in:' . PaymentMethod::ONLY_IN_PERSON . ',' . PaymentMethod::ONLY_ONLINE . ',' . PaymentMethod::ALL_PAYMENTS,
    ];

    public function validHandle()
    {
        ProviderService::setAcceptPaymentMethods($this->get('profileId'), $this->get('method'));
    }
}
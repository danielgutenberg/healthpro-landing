<?php namespace WL\Modules\Provider\Commands;


use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Provider\SecurityPolicies\ProviderServiceAccessPolicy;
use WL\Modules\Provider\ValueObjects\PartialAmount;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Services\Exceptions\AuthorizationException;

class UpdateProviderServiceCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'provider.updateProviderService';

	private $serviceId;
	private $name;
	private $serviceTypeIds;
	private $locationIds;
	private $providerId;
	private $sessions;
	private $deposit;
	private $depositType;
	private $validator;
	private $securityPolicies;
	private $certificates;
	private $description;

	function __construct($providerId, $serviceId, $name, $serviceTypeIds, $locationIds, $sessions, $deposit, $depositType, $certificates, $description = null)
	{
		$this->providerId = $providerId;
		$this->serviceId = $serviceId;
		$this->name = $name;
		$this->serviceTypeIds = $serviceTypeIds;
		$this->locationIds = $locationIds;
		$this->sessions = $sessions;
		$this->deposit = $deposit;
		$this->depositType = $depositType;
		$this->certificates = $certificates;
		$this->description = $description;

		$this->securityPolicies = new ProviderServiceAccessPolicy();

		$this->validator = Validator::make(
			[
				'providerId' => $this->providerId,
				'service_id' => $this->serviceId,
				'name' => $this->name,
				'service_types' => $this->serviceTypeIds,
				'location_ids' => $this->locationIds,
				'sessions' => $this->sessions,
				'deposit' => $this->deposit,
				'deposit_type' => $this->depositType,
				'gift_certificates' => $this->certificates
			],
			[
				'providerId' => 'required',
				'service_id' => 'required',
				'name' => 'sometimes|string',
				'service_types' => 'sometimes|array',
				'location_ids' => 'sometimes|array',
				'sessions' => 'sometimes|array|provider_session_duplication',
				'deposit' => 'sometimes|digits_between:0,100',
				'deposit_type' => 'sometimes|in:' . PartialAmount::PERCENTAGE . ',' . PartialAmount::FLAT_RATE,
				'gift_certificates' => 'sometimes|boolean'
			],
			[
				'providerId.required' => __('Provider id is not supplied.'),
				'service_id.required' => __('Provider Service id is not supplied.'),
				'sessions.array' => __('Provider Service sessions must be supplied.'),
				'sessions.provider_session_duplication' => __('Provider Service session duplicated.'),
				'numeric' => __('The :attribute field must be numeric.'),
				'min' => __('The :attribute field minimum value is ":min".'),
				'boolean' => __('The :attribute field must be true or false.')
			]
		);
	}

	public function handle()
	{
		if (!$this->securityPolicies->canUpdateProviderService($this->providerId, $this->serviceId)) {
			throw new AuthorizationException("Can't update provider service for this profile.");
		}

		if ($this->validator->fails()) {
			throw new ValidationException($this->validator);
		}

		ProviderService::updateService($this->serviceId, $this->name, $this->serviceTypeIds, $this->locationIds, $this->certificates, $this->deposit, $this->depositType, $this->description);

		if (!empty($this->sessions)) {
			ProviderService::updateServiceSessions($this->serviceId, $this->sessions);
		}

		return ProviderService::getService($this->serviceId);
	}
}

<?php namespace WL\Modules\Provider\Commands;


use WL\Modules\Provider\Exceptions\ProviderAccessException;
use WL\Modules\Provider\Facades\ProviderMembershipService;

class GetProviderPlanDetailsCommand extends ProviderBaseCommand
{
	const IDENTIFIER = 'membership.getActivePlanDetails';

	protected $rules = [
		'profileId' => 'required|int',
	];

	public function validHandle()
	{
		if(!$this->securityPolicy->canAccessActivePlanDetails($this->inputData['profileId'])) {
			throw new ProviderAccessException();
		}

		if($plan = ProviderMembershipService::getActivePlan($this->inputData['profileId'])) {
			return ProviderMembershipService::getPlanDetails($plan);
		}
	}
}

<?php namespace WL\Modules\Provider\Commands\Review;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Review\Commands\LoadElementReviewsCommand;
use WL\Security\Commands\AuthorizableCommand;

class GetProviderReviewsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.GetProviderReviewsCommand';

	protected $providerId;

	public function __construct($providerId)
	{
		$this->providerId = $providerId;
	}

	public function handle()
	{
		$provider = ProfileService::getProfileById($this->providerId, true);

		if (!$provider || $provider->type != ModelProfile::PROVIDER_PROFILE_TYPE)
			throw new ModelNotFoundException();

		return (new LoadElementReviewsCommand($provider->id, $provider->typeInstance()->getMorphClass() ))->handle();
	}

}

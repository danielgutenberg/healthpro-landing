<?php namespace WL\Modules\Provider\Commands\Review;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Review\Commands\DeleteReviewRatingCommand;
use WL\Modules\Review\Facade\ReviewService;
use WL\Security\Commands\AuthorizableCommand;

class DeleteProviderReviewRatingCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.DeleteProviderReviewRatingCommand';

	protected $providerId;
	protected $reviewId;
	protected $ratingId;

	public function __construct($providerId, $reviewId, $ratingId)
	{
		$this->providerId = $providerId;
		$this->reviewId = $reviewId;
		$this->ratingId = $ratingId;
	}

	public function handle()
	{
		$provider = ProfileService::getProfileById($this->providerId);

		if (!$provider || $provider->type != ModelProfile::PROVIDER_PROFILE_TYPE)
			throw new ModelNotFoundException();

		if (!$review = ReviewService::getReviewById($this->reviewId))
			throw new ModelNotFoundException();

		if ($provider->id != $review->elm_id || $review->elm_type != $provider->typeInstance()->getMorphClass())
			throw new ModelNotFoundException();

		return (new DeleteReviewRatingCommand($this->reviewId, $this->ratingId))->handle();
	}

}

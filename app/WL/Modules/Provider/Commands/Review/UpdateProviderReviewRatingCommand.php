<?php namespace WL\Modules\Provider\Commands\Review;


use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Review\Commands\SetReviewRatingsCommand;
use WL\Modules\Review\Exceptions\ReviewException;
use WL\Modules\Review\Facade\ReviewService;
use WL\Security\Commands\AuthorizableCommand;

class UpdateProviderReviewRatingCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.UpdateProviderReviewRatingCommand';

	protected $providerId;
	protected $reviewId;
	protected $ratingId;
	protected $value;

	public function __construct($providerId, $reviewId, $ratingId, $value)
	{
		$this->providerId = $providerId;
		$this->reviewId = $reviewId;
		$this->ratingId = $ratingId;
		$this->value = $value;
	}

	public function handle()
	{
		$provider = ProfileService::getProfileById($this->providerId);

		if (!$provider || $provider->type != ModelProfile::PROVIDER_PROFILE_TYPE)
			throw new ModelNotFoundException();

		if (!$review = ReviewService::getReviewById($this->reviewId))
			throw new ModelNotFoundException();

		if ($provider->id != $review->elm_id || $review->elm_type != $provider->typeInstance()->getMorphClass())
			throw new ModelNotFoundException();

		if (!$rating = ReviewService::getRatingById($this->reviewId, $this->ratingId))
			throw new ReviewException();

		return (new SetReviewRatingsCommand($this->reviewId, [$rating->rating_label => $this->value]))->handle();
	}

}

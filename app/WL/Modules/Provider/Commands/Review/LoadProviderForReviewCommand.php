<?php namespace WL\Modules\Provider\Commands\Review;


use WL\Modules\Client\Facades\ClientService;
use WL\Modules\Profile\Exceptions\WrongProfileTypeException;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Review\Exceptions\ReviewException;
use WL\Security\Commands\AuthorizableCommand;

class LoadProviderForReviewCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.LoadProviderForReviewCommand';

	protected $providerIdOrSlug;
	protected $checkIfCanBeReviewed;

	public function __construct($providerIdOrSlug, $checkIfCanBeReviewed)
	{
		$this->providerIdOrSlug = $providerIdOrSlug;
		$this->checkIfCanBeReviewed = $checkIfCanBeReviewed;
	}

	public function handle()
	{
		$provider = ProfileService::getProfileByIdOrSlug($this->providerIdOrSlug);

		$currentProfileId = ProfileService::getCurrentProfileId();

		if (!$provider || $provider->type != ModelProfile::PROVIDER_PROFILE_TYPE)
			throw new WrongProfileTypeException();

		if ($this->checkIfCanBeReviewed && !ClientService::providerNeedsReview($currentProfileId, $provider))
			throw new ReviewException('You cannot rate this provider');

		return $provider;
	}

}

<?php namespace WL\Modules\Provider\Commands\Review;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Modules\Client\Facades\ClientService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Provider\Exceptions\ProviderSelfReviewNotAllowedException;
use WL\Modules\Review\Commands\CreateReviewCommand;
use WL\Modules\Review\Exceptions\ReviewException;
use WL\Modules\Review\Models\Review;
use WL\Security\Commands\AuthorizableCommand;

class CreateProviderReviewsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.CreateProviderReviewsCommand';

	protected $providerId;
	protected $comment;
	protected $ratings;

	public function __construct($providerId, $comment, array $ratings = [])
	{
		$this->providerId = $providerId;
		$this->comment = $comment;
		$this->ratings = $ratings;
	}

	public function handle()
	{
		$provider = ProfileService::getProfileById($this->providerId);

		if (!$provider || $provider->type != ModelProfile::PROVIDER_PROFILE_TYPE) {
			throw new ModelNotFoundException();
		}

		$currentProfile = ProfileService::getCurrentProfile();


		$loggedOwner = ProfileService::getOwner($currentProfile->id);
		$reviewOwner = ProfileService::getOwner($provider->id);

		if ($reviewOwner->id == $loggedOwner->id) {
			throw new ProviderSelfReviewNotAllowedException('Self review not allowed');
		}

		if($currentProfile->type == ModelProfile::CLIENT_PROFILE_TYPE) {
			if(!ClientService::providerNeedsReview($currentProfile->id, $provider)) {
				throw new ReviewException('You cannot rate this provider');
			}
			ProviderOriginatedService::confirmProviderClient($provider->id, $currentProfile->id);
		}

		return (new CreateReviewCommand(Review::TYPE_PROVIDER_REVIEW, $currentProfile->id, $provider->id, $provider->typeInstance()->getMorphClass(), $this->comment, $this->ratings))->handle();
	}

}

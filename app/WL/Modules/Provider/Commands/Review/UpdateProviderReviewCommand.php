<?php namespace WL\Modules\Provider\Commands\Review;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Review\Commands\UpdateReviewCommand;
use WL\Security\Commands\AuthorizableCommand;

class UpdateProviderReviewCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.UpdateProviderReviewCommand';

	protected $providerId;
	protected $reviewId;
	protected $comment;
	protected $ratings;

	public function __construct($providerId, $reviewId, $comment='', array $ratings=[])
	{
		$this->providerId = $providerId;
		$this->reviewId = $reviewId;
		$this->comment = $comment;
		$this->ratings = $ratings;
	}

	public function handle()
	{
		$provider = ProfileService::getProfileById($this->providerId);

		if (!$provider || $provider->type != ModelProfile::PROVIDER_PROFILE_TYPE)
			throw new ModelNotFoundException();

		return (new UpdateReviewCommand($this->reviewId, $this->comment, $this->ratings))->handle();
	}

}

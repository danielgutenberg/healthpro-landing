<?php namespace WL\Modules\Provider\Commands;


use WL\Modules\Profile\Commands\ValidatableCommand;
use WL\Modules\Provider\Facades\ProviderMembershipService;

class UpdateProviderPlanCommand extends ValidatableCommand
{
	const IDENTIFIER = 'membership.updateProviderPlan';

	protected $rules = [
		'profileId' => 'required|int',
		'cycles' => 'required|int',
	];

	public function validHandle()
	{
		$plan = ProviderMembershipService::getActivePlan($this->inputData['profileId']);
		return ProviderMembershipService::updateProviderPlan($plan, $this->inputData['cycles']);
	}
}

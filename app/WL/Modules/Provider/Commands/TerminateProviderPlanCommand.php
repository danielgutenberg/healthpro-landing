<?php namespace WL\Modules\Provider\Commands;


use WL\Modules\Provider\Exceptions\MembershipException;
use WL\Modules\Provider\Facades\ProviderMembershipService;

class TerminateProviderPlanCommand extends ProviderBaseCommand
{
    const IDENTIFIER = 'membership.terminateProviderPlan';

	protected $rules = [
		'profileId' => 'required|int',
	];

	public function validHandle()
    {
        $profileId = $this->get('profileId');

        $plan = ProviderMembershipService::getActivePlan($profileId);
        if(!$plan) {
            throw new MembershipException('No active plan for this professional');
        }
        ProviderMembershipService::terminatePlan($plan);
    }
}

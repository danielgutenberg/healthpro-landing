<?php namespace WL\Modules\Provider\Commands;

use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Modules\PricePackage\Facade\PricePackageService;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Provider\Models\ProviderServiceSession;
use WL\Modules\Provider\SecurityPolicies\ProviderServiceAccessPolicy;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Services\Exceptions\AuthorizationException;

class RemoveProviderServiceSessionCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'provider.removeProviderServiceSession';

	private $providerId;
	private $validator;
	private $securityPolicies;

	function __construct($providerId, $serviceSessionId)
	{
		$this->providerId = $providerId;
		$this->serviceSessionId = $serviceSessionId;

		$this->securityPolicies = new ProviderServiceAccessPolicy();

		$this->validator = Validator::make(
			[
				'providerId' => $this->providerId,
				'service_session_id' => $this->serviceSessionId,
			],
			[
				'providerId' => 'required',
				'service_session_id' => 'required',
			],
			[
				'providerId.required' => __('Provider Id must be supplied.'),
				'service_session_id.required' => __('Provider Service Session Id must be supplied.'),
			]
		);
	}

	public function handle()
	{
		if (!$this->securityPolicies->canDeleteProviderServiceSession($this->providerId, $this->serviceSessionId)) {
			throw new AuthorizationException("Can't delete provider service for this profile.");
		}

		if ($this->validator->fails()) {
			throw new ValidationException($this->validator);
		}

		$session = ProviderService::getServiceSession($this->serviceSessionId);
        ProviderService::checkCanDeleteDependant($session->provider_service_id);

		// Delete packages attached to the session
		foreach (PricePackageService::getEntityPricePackages($this->providerId, $this->serviceSessionId, ProviderServiceSession::PACKAGE_ENTITY_TYPE) as $package) {
            PricePackageService::deletePricePackage($package->id);
		}

		return ProviderService::removeServiceSession($this->serviceSessionId);
	}
}

<?php namespace WL\Modules\Provider\Commands;


use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Provider\Exceptions\ProviderAccessException;
use WL\Modules\Provider\Facades\ProviderMembershipService;

class GetNextCycleEstimate extends ProviderBaseCommand
{
    const IDENTIFIER = 'membership.getPlanBillingEstimate';

    protected $rules = [
        'profileId' => 'required|int',
    ];

    public function validHandle()
    {
        if(!$this->securityPolicy->canAccessBillingDetails($this->inputData['profileId'])) {
            throw new ProviderAccessException();
        }

        $cycle = ProviderMembershipService::getProviderCurrentCycle($this->inputData['profileId']);
        return ProviderMembershipService::getCycleBilling($cycle, true);
    }
}

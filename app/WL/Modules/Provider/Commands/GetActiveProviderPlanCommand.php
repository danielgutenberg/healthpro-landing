<?php namespace WL\Modules\Provider\Commands;


use WL\Modules\Profile\Commands\ValidatableCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Provider\Facades\ProviderMembershipService;

class GetActiveProviderPlanCommand extends ValidatableCommand
{
	const IDENTIFIER = 'membership.getActivePlan';

	public function validHandle()
	{
	    $profile = ProfileService::getCurrentProfile();
	    if(!$profile || $profile->type !== ModelProfile::PROVIDER_PROFILE_TYPE) {
	        return null;
        }

		return ProviderMembershipService::getActivePlan($profile->id);
	}
}

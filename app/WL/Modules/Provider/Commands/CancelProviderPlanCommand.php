<?php namespace WL\Modules\Provider\Commands;


use WL\Modules\Provider\Exceptions\MembershipException;
use WL\Modules\Provider\Exceptions\ProviderAccessException;
use WL\Modules\Provider\Facades\ProviderMembershipService;

class CancelProviderPlanCommand extends ProviderBaseCommand
{
    const IDENTIFIER = 'membership.cancelProviderPlan';

	protected $rules = [
		'profileId' => 'required|int',
	];

	public function validHandle()
    {
        $profileId = $this->get('profileId');

        if(!$this->securityPolicy->canCancelActivePlan($profileId)) {
            throw new ProviderAccessException();
        }

        $plan = ProviderMembershipService::getActivePlan($profileId);
        if(!$plan) {
            throw new MembershipException('No active plan for this professional');
        }
        return ProviderMembershipService::cancelProviderPlan($plan);
    }
}

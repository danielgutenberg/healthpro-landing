<?php namespace WL\Modules\Provider\Commands;


use Illuminate\Console\Command;
use WL\Modules\Order\Commands\Helpers\OrderItemsToObject;
use WL\Modules\Payment\Facades\PaymentService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Modules\Provider\Models\BillingCycle;
use WL\Modules\Search\Facades\OrderService;

class BillingCycleExpirationCommand extends Command
{
    protected $signature = 'member:cycle_expiration';

    public function handle()
    {
        $cycles = ProviderMembershipService::getExpiringBillingCycles();
        $cycles->each(function(BillingCycle $cycle) {
            $profileId = $cycle->plan->profile_id;
            $userId = ProfileService::getOwner($profileId)->id;
            $next = ProviderMembershipService::expireBillingCycle($cycle);
            $nextCycle = $next['cycle'];

			$items = ProviderMembershipService::collectBillingCycleItems($cycle->fresh(), $nextCycle);
			if(empty($items)) {
			    return false;
            }
            $paymentMethodId = PaymentService::getProfilePaymentMethod($profileId)->getId();
			$order = OrderService::makeOrder($userId, $profileId, $paymentMethodId, OrderItemsToObject::convert($items));
            OrderService::createBillingCharge($order->id);
        });
    }


}

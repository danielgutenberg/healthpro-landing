<?php namespace WL\Modules\Provider\Commands;


use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Provider\SecurityPolicies\ProviderServiceAccessPolicy;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Services\Exceptions\AuthorizationException;

class RemoveProviderServiceCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'provider.removeProviderService';

	private $providerId;
	private $serviceId;
	private $validator;
	private $securityPolicies;

	function __construct($providerId, $serviceId)
	{
		$this->providerId = $providerId;
		$this->serviceId = $serviceId;

		$this->securityPolicies = new ProviderServiceAccessPolicy();

		$this->validator = Validator::make(
			[
				'providerId' => $this->providerId,
				'service_id' => $this->serviceId,
			],
			[
				'providerId' => 'required',
				'service_id' => 'required',
			],
			[
				'providerId.required' => __('Provider Id must be supplied.'),
				'service_id.required' => __('Provider Service Id must be supplied.'),
			]
		);
	}

	public function handle()
	{
		if (!$this->securityPolicies->canDeleteProviderService($this->providerId, $this->serviceId)) {
			throw new AuthorizationException("Can't delete provider service for this profile.");
		}

		if ($this->validator->fails()) {
			throw new ValidationException($this->validator);
		}

		return ProviderService::removeService($this->serviceId);
	}
}

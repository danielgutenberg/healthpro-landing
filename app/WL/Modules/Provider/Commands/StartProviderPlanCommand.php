<?php namespace WL\Modules\Provider\Commands;


use WL\Modules\Coupons\Commands\ApplyCouponCommand;
use WL\Modules\Coupons\Exceptions\CouponCannotBeAppliedException;
use WL\Modules\Coupons\Facades\CouponService;
use WL\Modules\Coupons\ValueObjects\ApplyCouponRequest;
use WL\Modules\Order\Adapters\EntityName;
use WL\Modules\Order\Commands\Helpers\OrderItemsToObject;
use WL\Modules\Order\Models\Order;
use WL\Modules\Profile\Commands\ValidatableCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Modules\Provider\Models\ProviderPlan;
use WL\Modules\Search\Facades\OrderService;

class StartProviderPlanCommand extends ValidatableCommand
{
    const IDENTIFIER = 'membership.startMemberShipCommand';

	protected $rules = [
		'profileId' => 'required|int',
		'paymentMethodId' => 'sometimes|int',
		'productId' => 'required|int',
		'couponId' => 'sometimes|int',
	];

	public function validHandle()
    {
        $profileId = $this->inputData['profileId'];
        $user = ProfileService::getOwner($profileId);
        $paymentMethodId = $this->inputData['paymentMethodId'];
        $productId = $this->inputData['productId'];
        $couponId = array_get($this->inputData, 'couponId');

        $billingCycles = null;

        $plan = ProviderMembershipService::startProviderPlan($profileId, $productId, $billingCycles);
        $items = [
            [
                'name' => EntityName::MEMBERSHIP,
                'id' => $plan->id,
            ],
        ];

        if($couponId) {
            try {
                $entityTypes = [Order::class, ProviderPlan::class];
                $request = new ApplyCouponRequest($couponId, $entityTypes, null, null, null, true, ModelProfile::PROVIDER_PROFILE_TYPE);
                $coupon = CouponService::getValidCoupon($request);
                CouponService::applyCoupon($coupon, $plan->id);
                $items[] = [
                    'name' => EntityName::COUPON,
                    'id' => $coupon->id,
                ];
            } catch (CouponCannotBeAppliedException $e) {}
        }

        return OrderService::makeOrder($user->id, $profileId, $paymentMethodId, OrderItemsToObject::convert($items));
    }
}

<?php namespace WL\Modules\Provider\Commands;


use WL\Modules\Provider\Facades\ProviderService;
use WL\Security\Commands\AuthorizableCommand;

class LoadProviderServiceTypesCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'provider.loadProviderServiceTypes';

	public function __construct( $sortBy = null )
	{
		$this->sortBy = $sortBy;
	}

	public function handle()
	{
		if($this->sortBy == 'top')
			return ProviderService::sortByCount('Services');

		$serviceTypes = ProviderService::getServiceTypes();

		if($this->sortBy)
			$serviceTypes = $serviceTypes->sortBy($this->sortBy)->values();

		return $serviceTypes;
	}
}

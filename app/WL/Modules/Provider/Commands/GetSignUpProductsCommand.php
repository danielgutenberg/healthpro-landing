<?php namespace WL\Modules\Provider\Commands;


use WL\Modules\Profile\Commands\ValidatableCommand;
use WL\Modules\Provider\Facades\ProviderMembershipService;

class GetSignUpProductsCommand extends ValidatableCommand
{
	const IDENTIFIER = 'membership.getSignUpProducts';

	protected $rules = [
	    'plan_id' => 'sometimes|int',
    ];

	public function validHandle()
	{
	    $plan = ProviderMembershipService::getProviderPlan($this->get('plan_id'));
		return ProviderMembershipService::getProducts($plan);
	}
}

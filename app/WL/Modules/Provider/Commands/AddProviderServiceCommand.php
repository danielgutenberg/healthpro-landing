<?php namespace WL\Modules\Provider\Commands;


use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Provider\SecurityPolicies\ProviderServiceAccessPolicy;
use WL\Modules\Provider\ValueObjects\PartialAmount;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Services\Exceptions\AuthorizationException;

class AddProviderServiceCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'provider.addProviderService';

	private $providerId;
	private $name;
	private $providerServiceTypeIds;
	private $locationIds;
	private $sessions;
	private $deposit;
	private $depositType;
	private $certificates;
	private $description;

	private $validator;

	private $securityPolicies;

	function __construct($providerId, $name, $providerServiceTypeIds, $locationIds, $sessions, $deposit, $depositType, $certificates, $description = null)
	{
		$this->providerId = $providerId;
		$this->name = $name;
		$this->providerServiceTypeIds = $providerServiceTypeIds;
		$this->locationIds = $locationIds;
		$this->sessions = $sessions;
		$this->deposit = $deposit;
		$this->depositType = $depositType;
		$this->certificates = $certificates;
		$this->description = $description;

		$this->securityPolicies = new ProviderServiceAccessPolicy();

		$this->validator = Validator::make(
			[
				'providerId' => $this->providerId,
				'name' => $this->name,
				'service_type_ids' => $this->providerServiceTypeIds,
				'location_ids' => $this->locationIds,
				'sessions' => $this->sessions,
				'deposit' => $this->deposit,
				'deposit_type' => $this->depositType,
				'gift_certificates' => $this->certificates
			],
			[
				'providerId' => 'required',
				'name' => 'required',
				'service_type_ids' => 'required|array',
				'location_ids' => 'sometimes|array',
				'sessions' => 'sometimes|array',
				'deposit' => 'sometimes|digits_between:0,100',
				'deposit_type' => 'sometimes|in:' . PartialAmount::PERCENTAGE . ',' . PartialAmount::FLAT_RATE,
				'gift_certificates' => 'sometimes|boolean'
			],
			[
				'providerId.required' => __('Provider id is not supplied'),
				'name.required' => __('Name is not supplied'),
				'service_type_ids.array' => __('Provider Service types must be supplied.'),
				'sessions.array' => __('Provider Service sessions must be supplied.'),
				'sessions.provider_session_duplication' => __('Provider Service session duplicated.'),
				'numeric' => __('The :attribute field must be numeric.'),
				'min' => __('The :attribute field minimum value is ":min".'),
				'boolean' => __('The :attribute field must be true or false.')
			]
		);
	}

	public function handle()
	{
		if ($this->validator->fails()) {
			throw new ValidationException($this->validator);
		}

		if (!$this->securityPolicies->canAddProviderService($this->providerId)) {
			throw new AuthorizationException(__("Can't add provider service"));
		}

		$providerService = ProviderService::addService(
			$this->providerId,
			$this->name,
			$this->providerServiceTypeIds,
			$this->locationIds,
			$this->sessions,
			[],
			$this->certificates,
			$this->deposit,
			$this->depositType,
			$this->description
		);

		// we need sessions and locations
		return ProviderService::getService($providerService->id);
	}
}

<?php namespace WL\Modules\Provider\Commands;


use WL\Modules\Provider\Exceptions\MembershipException;
use WL\Modules\Provider\Exceptions\ProviderAccessException;
use WL\Modules\Provider\Facades\ProviderMembershipService;

class ReviveProviderPlanCommand extends ProviderBaseCommand
{
    const IDENTIFIER = 'membership.reviveProviderPlan';

	protected $rules = [
		'profileId' => 'required|int',
	];

	public function validHandle()
    {
        $profileId = $this->get('profileId');

        if(!$this->securityPolicy->canCancelActivePlan($profileId)) {
            throw new ProviderAccessException();
        }

        $plan = ProviderMembershipService::getActivePlan($profileId);
        if(!$plan) {
            throw new MembershipException('No active plan for this professional');
        }
        return ProviderMembershipService::reviveProviderPlan($plan);
    }
}

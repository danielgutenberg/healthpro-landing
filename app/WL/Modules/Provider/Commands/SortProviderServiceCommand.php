<?php namespace WL\Modules\Provider\Commands;


use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Provider\SecurityPolicies\ProviderServiceAccessPolicy;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Services\Exceptions\AuthorizationException;

class SortProviderServiceCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'provider.sortProviderService';

	private $serviceIds;
	private $providerId;

	function __construct($providerId, $serviceIds)
	{
		$this->providerId = $providerId;
		$this->serviceIds = $serviceIds;

		$this->securityPolicies = new ProviderServiceAccessPolicy();

		$this->validator = Validator::make(
			[
				'providerId' => $providerId,
				'service_ids' => $this->serviceIds,
			],
			[
				'providerId' => 'required',
				'service_ids' => 'required|array',
			],
			[
				'providerId.required' => __('Provider id is not supplied.'),
				'service_ids.required' => __('Provider Service ids were not supplied.'),
			]
		);
	}

	public function handle()
	{
		if ($this->validator->fails()) {
			throw new ValidationException($this->validator);
		}

		foreach ($this->serviceIds as $serviceId) {
			if (!$this->securityPolicies->canUpdateProviderService($this->providerId, $serviceId)) {
				throw new AuthorizationException("Can't update provider service for this profile.");
			}
		}

		ProviderService::sortServices($this->serviceIds);

		return ProviderService::getServices($this->providerId);
	}
}

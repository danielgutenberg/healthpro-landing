<?php namespace WL\Modules\Provider\Commands;


use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Provider\SecurityPolicies\ProviderServiceAccessPolicy;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Services\Exceptions\AuthorizationException;

class LoadProviderServicesCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'provider.loadProviderServices';

	private $profileId;
	private $validator;
	private $securityPolicies;

	function __construct($profileId)
	{
		$this->profileId = $profileId;

		$this->securityPolicies = new ProviderServiceAccessPolicy();

		$this->validator = Validator::make(
			[
				'profile_id' => $this->profileId,
			],
			[
				'profile_id' => 'required',
			],
			[
				'profile_id.required' => __('Profile Id must be supplied.'),
			]
		);
	}

	public function handle()
	{
		#if (!$this->securityPolicies->canLoadProviderServices($this->profileId)) {
		#	throw new AuthorizationException("Can't load provider services for this profile.");
		#}

		if ($this->validator->fails()) {
			throw new ValidationException($this->validator);
		}

		return ProviderService::getServices($this->profileId);
	}
}

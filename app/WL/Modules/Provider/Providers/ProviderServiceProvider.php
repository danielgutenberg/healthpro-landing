<?php namespace WL\Modules\Provider\ServiceProviders;


use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use WL\Modules\Provider\Repositories\DbProviderServiceRepository;
use WL\Modules\Provider\Repositories\DbProviderServiceSessionRepository;
use WL\Modules\Provider\Repositories\ProviderMembershipRepository;
use WL\Modules\Provider\Services\ProviderMembershipService;
use WL\Modules\Provider\Services\ProviderMembershipServiceInterface;
use WL\Modules\Provider\Services\ProviderService;

class ProviderServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('provider_service', function () {
			return new ProviderService(new DbProviderServiceRepository(), new DbProviderServiceSessionRepository());
		});

		$this->app->singleton(ProviderMembershipServiceInterface::class, function () {
			return new ProviderMembershipService(
				new ProviderMembershipRepository()
			);
		});

		$this->app->booting(function() {
			AliasLoader::getInstance()->alias('ProviderServiceHtml', 'WL\Modules\Provider\Helpers\Html');
		});
	}
}


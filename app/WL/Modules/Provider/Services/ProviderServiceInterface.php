<?php namespace WL\Modules\Provider\Services;
use Illuminate\Support\Collection;
use WL\Modules\Provider\Exceptions\ProviderServiceOnlySessionException;
use WL\Modules\Provider\Models\Membership;
use WL\Modules\Provider\Models\MembershipProduct;
use WL\Modules\Provider\Models\ProviderPlan;

/**
 * Interface ProviderServiceInterface
 * @package WL\Modules\Provider\Services
 */
interface ProviderServiceInterface
{
	/**
	 * Get all service types
	 * @return array<Tag>
	 */
	public function getServiceTypes();

	/**
	 * Get provider service by id
	 *
	 * @param $providerServiceId
	 * @return mixed
	 */
	public function getService($providerServiceId);

	/**
	 * Adds provider's service to a profile
	 *
	 * @param int $profileId
	 * @param string $name
	 * @param array $serviceTypeIds
	 * @param array $locationIds
	 * @param array $sessions
	 * @param array $packages
	 * @param $certificates
	 * @param null $deposit
	 * @param null $depositType
	 * @return mixed
	 */
	public function addService($profileId, $name, $serviceTypeIds, $locationIds = [], $sessions = [], $packages = [], $certificates, $deposit = null, $depositType = null);

	/**
	 *
	 * Updates service type
	 *
	 * @param $providerServiceId number
	 * @param $serviceTypeId number
	 * @param $locationIds array<number>
	 * @return mixed
	 */
	public function updateService($providerServiceId, $serviceTypeId, $locationIds);

	/**
	 * @param array $providerServiceIds array on service Ids in order
	 * @return mixed
	 */
	public function sortServices($providerServiceIds);

	/**
	 * Removes provider's service
	 *
	 * @param $providerServiceId number
	 * @return mixed
	 */
	public function removeService($providerServiceId);


	/**
	 * Gets provider services by profile id
	 * @param $profileId number
	 * @return mixed
	 */
	public function getServices($profileId);

    /**
     * @param int $profileId
     * @return bool
     */
    public function offerGiftCertificates($profileId);

    /**
     * @param int $profileId
     * @return int
     */
    public function removeAllGiftCertificates($profileId);

    /**
     * @param int $profileId
     * @param string $method
     * @return bool
     */
    public function setAcceptPaymentMethods($profileId, $method);

	/**
	 * Gets provider services tags by profile id
	 * @param $profileId number
	 * @return mixed
	 */
	public function getServicesTags($profileId);

	/**
	 * Adds provider's service location
	 *
	 * @param $providerServiceId number
	 * @param $locationId number
	 * @return mixed
	 */
	public function addLocation($providerServiceId, $locationId);

	/**
	 * Removes provider's service location
	 *
	 * @param $providerServiceId number
	 * @param $locationId number
	 * @return mixed
	 */
	public function removeLocation($providerServiceId, $locationId);

	/**
	 * Check existence of session
	 * @param $providerServiceId
	 * @param $price
	 * @param $duration
	 * @param $isFirstTime
	 * @return mixed
	 */
	public function isSessionExists($providerServiceId, $price, $duration, $isFirstTime);

	/**
	 * Adds provider's service session
	 *
	 * @param $providerServiceId number
	 * @param $price float
	 * @param $duration int
	 * @param $isFirstTime
	 * @return mixed
	 */
	public function addServiceSession($providerServiceId, $price, $duration, $isFirstTime);

	/**
	 * Updates provider's session
	 *
	 * @param $providerServiceSessionId number
	 * @param $price float
	 * @param $duration int
	 * @return mixed
	 */
	public function updateServiceSession($providerServiceSessionId, $price, $duration);

	/**
	 * Update provider's sessions
	 *
	 * @param $providerServiceId number
	 * @param array $sessions
	 * @return boolean
	 */
	public function updateServiceSessions($providerServiceId, array $sessions);

	/**
	 * Removes provider's service session by id
	 *
	 * @param $providerServiceSessionId number
	 * @return mixed
	 */
	public function removeServiceSession($providerServiceSessionId);

	/**
	 * @param int $profileId
	 * @return array
	 */
	public function getProgress($profileId);

    /**
     * @param int $providerId
     * @return array
     */
    public function collectRequirements($providerId);

	/**
	 * Get provider's service session by id
	 *
	 * @param integer $providerServiceSessionId
	 * @return mixed
     */
	public function getServiceSession($providerServiceSessionId);

	/**
	 * Get list of services ordered by most professionals
	 *
	 * @param string $taxonomyType
	 * @return mixed
	 */
	public function sortByCount($taxonomyType);

	/**
	 * Gets professional introductory session
	 *
	 * @param $providerId
	 * @return mixed
	 */
	public function getIntroductorySession($providerId);

    /**
     * @param int $providerId
     * @return void
     */
    public function adjustProviderStatus($providerId);

	/**
	 * @param int $providerId
	 * @param string $method
	 * @return bool
	 */
	public function acceptsPaymentMethod($providerId, $method);

    /**
     * @param int $sessionId
     * @return Collection
     */
    public function getServiceSessions($sessionId);

    /**
     * @param int $sessionId
     * @return void
     * @throws ProviderServiceOnlySessionException
     */
    public function checkCanDeleteDependant($sessionId);
}

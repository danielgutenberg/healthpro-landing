<?php namespace WL\Modules\Provider\Services;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use WL\Modules\Provider\Exceptions\InvalidNumberOfBillingCycles;
use WL\Modules\Provider\Models\BillingCycle;
use WL\Modules\Provider\Models\Membership;
use WL\Modules\Provider\Models\MembershipProduct;
use WL\Modules\Provider\Models\ProviderPlan;
use WL\Modules\Provider\Models\ProviderPlanAction;
use WL\Modules\Provider\ValueObjects\PlanChange;

interface ProviderMembershipServiceInterface
{
    /**
     * @param int $providerId
     * @param int $productId
     * @return mixed
     */
    public function setProviderRenewal($providerId, $productId);

    /**
     * @param int $providerId
     * @return MembershipProduct
     */
    public function getProviderRenewalProduct($providerId);

    /**
     * @param int $providerId
     * @return ProviderPlan
     */
    public function getActivePlan($providerId);

	/**
	 * @param int $providerId
	 * @return Collection
	 */
	public function getProfilePlanHistory($providerId);

    /**
     * @param ProviderPlan $plan
     * @return ProviderPlan
     */
    public function addCalculatedAttributes(ProviderPlan $plan);

	/**
	 * @param int $profileId
	 * @param int $productId
	 * @param int|null $billingCycles
	 * @return ProviderPlan
	 */
	public function startProviderPlan($profileId, $productId, $billingCycles=null);

    /**
     * @param BillingCycle $cycle
     * @return mixed
     */
    public function expireBillingCycle($cycle);

	/**
	 * @param $providerId
	 * @return bool
	 */
	public function hasBookings($providerId);

    /**
     * @param int $providerId
     * @return void
     */
    public function checkBookingLimitNotification($providerId);

	/**
	 * @param int $providerId
	 * @return \stdClass
	 */
	public function getProviderBookings($providerId);

	/**
	 * @param ProviderPlan $original
	 * @param MembershipProduct $product
	 * @return BillingCycle
	 */
	public function changeProviderPlan(ProviderPlan $original, MembershipProduct $product);

    /**
     * @param ProviderPlan $originalPlan
     * @param MembershipProduct $upgradeProduct
     * @return float
     */
    public function calculateUpgradePrice(ProviderPlan $originalPlan, MembershipProduct $upgradeProduct);

    /**
     * @param ProviderPlan $originalPlan
     * @param MembershipProduct $newProduct
     * @return PlanChange
     */
    public function resolvePlanChange(ProviderPlan $originalPlan, MembershipProduct $newProduct);

    /**
     * @param ProviderPlan $plan
     * @return bool
     */
    public function cancelProviderPlan(ProviderPlan $plan);

    /**
     * @param ProviderPlan $plan
     * @return bool
     */
    public function reviveProviderPlan(ProviderPlan $plan);

    /**
     * @param int $providerId
     * @return BillingCycle
     */
    public function getProviderCurrentCycle($providerId);

    /**
     * @param BillingCycle $cycle
	 * @param bool|false $update
     * @return array
     */
    public function getCycleBilling(BillingCycle $cycle, $update=false);

    /**
     * @param int $cycleId
     * @return ProviderPlanAction
     */
    public function getCycleBillableActions($cycleId);

    /**
     * @return Collection
     */
    public function getExpiringBillingCycles();

    /**
	 * @param ProviderPlan $plan
	 * @return ProviderPlan
	 */
    public function activatePlan($plan);

    /**
     * @param int $productId
     * @return MembershipProduct
     */
    public function getProduct($productId);

    /**
     * @param int $planId
     * @return ProviderPlan
     */
    public function getProviderPlan($planId);

    /**
     * @param int $providerId
     * @param string|array $feature
     * @param bool|array $forceStatus
     * @return bool
     */
    public function hasFeature($providerId, $feature, $forceStatus=false);

    /**
     * @param int $providerId
     * @param bool $forceStatus
     * @return array
     */
    public function getProviderFeatures($providerId, $forceStatus=false);

    /**
     * @param string $slug
	 * @param string $name
	 * @param string $description
     * @param array $features
     * @param int|null $limit
     * @param float|null $fee
     * @return Membership
     */
    public function createMembership($slug, $name, $description=null, $features=[], $limit=null, $fee=null);

    /**
     * @param string $slug
     * @return bool
     */
    public function validateMembershipSlug($slug);

    /**
     * @param int $membership_id
     * @param string $name
     * @param float $price
     * @param string $description
     * @param string $months
     * @return MembershipProduct
     */
    public function createMembershipProduct(
        $membership_id,
        $name,
        $price,
        $description='',
        $months=1
    );

    /**
     * @param int $providerId
     * @return string
     */
    public function adjustProviderStatus($providerId);

    /**
     * @param int $providerId
     * @param string $status
     * @return mixed
     */
    public function setProviderStatus($providerId, $status);

    /**
     * @param int $id
     * @return BillingCycle
     */
    public function getBillingCycle($id);

    /**
     * @param int $id
     * @return ProviderPlanAction
     */
    public function getActionRecord($id);

	/**
	 * @param ProviderPlan|null $plan
	 * @return array
	 */
	public function getProducts(ProviderPlan $plan = null);

	/**
	 * @param ProviderPlan $plan
	 * @return arr
	 */
	public function getPlanDetails(ProviderPlan $plan);

	/**
	 * @param ProviderPlan $plan
	 * @param int $cycles
	 * @return bool
	 * @throws InvalidNumberOfBillingCycles
	 */
	public function updateProviderPlan(ProviderPlan $plan, $cycles);

    /**
     * @param int $planId
     * @param int $cycles
     * @return bool
     * @throws InvalidNumberOfBillingCycles
     * @throws ModelNotFoundException
     */
    public function addCycles($planId, $cycles);

	/**
	 * @param ProviderPlan $plan
	 * @param int $id
	 * @return bool
	 */
	public function saveSubscriptionToPlan(ProviderPlan $plan, $id);

    /**
     * @param BillingCycle $cycle
     * @param BillingCycle $nextCycle
     * @return array
     */
    public function collectBillingCycleItems(BillingCycle $cycle, BillingCycle $nextCycle = null);

    /**
     * @param int $providerId
     * @return Carbon
     */
    public function getFirstActivation($providerId);

    /**
     * @param int $providerId
     * @return Collection
     */
    public function getProviderActions($providerId);

	/**
	 * @param ProviderPlan $plan
	 * @param string $status
	 * @return mixed
	 */
	public function terminatePlan(ProviderPlan $plan, $status = ProviderPlan::STATUS_CANCELLED);

    /**
     * @param int $planId
     * @return Carbon
     */
    public function getPlanEndDate($planId);
}

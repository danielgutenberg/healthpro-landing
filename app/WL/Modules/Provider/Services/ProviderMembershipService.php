<?php namespace WL\Modules\Provider\Services;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Event;
use WL\Events\Facades\EventHelper;
use WL\Modules\Appointment\Facades\AppointmentService;
use WL\Modules\Order\Adapters\EntityName;
use WL\Modules\Payment\Models\PaymentCurrency;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Provider\Events\BookingsLimitAdminMailEvent;
use WL\Modules\Provider\Events\BookingsLimitCloseMailEvent;
use WL\Modules\Provider\Events\BookingsLimitReachedMailEvent;
use WL\Modules\Provider\Events\EndOfBillingCycleEvent;
use WL\Modules\Provider\Events\EndOfProviderPlanEvent;
use WL\Modules\Provider\Events\ProviderPlanCancelledEvent;
use WL\Modules\Provider\Events\ProviderPlanTerminatedEvent;
use WL\Modules\Provider\Exceptions\InvalidNumberOfBillingCycles;
use WL\Modules\Provider\Exceptions\PlanChangeNotAllowed;
use WL\Modules\Provider\Models\Membership;
use WL\Modules\Provider\Models\MembershipProduct;
use WL\Modules\Provider\Models\BillingCycle;
use WL\Modules\Provider\Models\ProviderPlan;
use WL\Modules\Provider\Models\ProviderPlanAction;
use WL\Modules\Provider\Repositories\ProviderMembershipRepositoryInterface;
use DateInterval;
use WL\Modules\Provider\Repositories\ProviderStatusRepository;
use WL\Modules\Provider\ValueObjects\MembershipActions;
use WL\Modules\Provider\ValueObjects\PlanChange;
use WL\Modules\Search\Facades\OrderService;
use WL\Yaml\Facades\Yaml;
use WL\Modules\Provider\Facades\ProviderService as ProviderServiceFacade;

class ProviderMembershipService implements ProviderMembershipServiceInterface
{
    protected $repo;
    private $tierRepository;
    private $config;

    public function __construct(ProviderMembershipRepositoryInterface $repository)
    {
        $this->repo = $repository;
        $statuses = Yaml::get(null, 'wl.membership.statuses');
        $this->tierRepository = new ProviderStatusRepository($statuses);
        $this->config = Yaml::get(null, 'wl.membership.config');
    }

    public function setProviderRenewal($providerId, $productId)
    {
        if($plan = $this->getActivePlan($providerId)) {
            $plan->renewal_product_id = $productId;
            $this->repo->save($plan);
        }
    }

    public function getProviderRenewalProduct($providerId)
    {
        if($plan = $this->getActivePlan($providerId)) {
            return $this->getProduct($plan->renewal_product_id);
        }
    }

    public function getActivePlan($providerId)
    {
        return $this->repo->getProfileActivePlan($providerId);
    }

	public function getProfilePlanHistory($providerId)
	{
		return $this->repo->getProfilePlanHistory($providerId);
	}

    public function addCalculatedAttributes(ProviderPlan $plan)
    {
        $plan->end_date = $this->calculatePlanEnd($plan);
        return $plan;
    }

	public function startProviderPlan($profileId, $productId, $billingCycles=null)
	{
	    $product = $this->repo->getProductById($productId);
		$billingCycles = $billingCycles ?: $product->billing_cycles;

		$plan = $this->repo->createProviderPlan($profileId, $product->id, $billingCycles);

		$dates = $this->getDates(null, $product->interval);
		$this->repo->createPlanBillingCycle($plan->id, $dates->start, $dates->end);
        return $plan;
    }

    public function expireBillingCycle($cycle)
    {
        $plan = $cycle->plan;
        $product = $plan->product;

        $this->updateCycleBookings($cycle);
        $cycle->status = BillingCycle::STATUS_ENDED;
        $this->repo->save($cycle);

		$cyclesLeft = $plan->billing_cycles - $this->countPlanCycles($plan);
		Event::fire(new EndOfBillingCycleEvent($cycle));

		if($cyclesLeft <= 0) {
			Event::fire(new EndOfProviderPlanEvent($plan));
			$action = $this->repo->getPlanPendingAction($plan->id);
			$nextPlanId = null;
			if($plan->renewal_product_id) {
				// Create new plan pending action
                $product = $this->repo->getProductById($plan->renewal_product_id);
                $plan = $this->repo->createProviderPlan($plan->profile_id, $product->id, $product->billing_cycles, $plan->id);

                $dates = $this->getDates($cycle->end_date, $product->interval);
                $nextCycle = $this->repo->createPlanBillingCycle($plan->id, $dates->start, $dates->end);
                $nextPlanId = $plan->id;
			} else {
				// Terminate current plan
				$this->terminatePlan($plan, ProviderPlan::STATUS_ENDED);
				$plan = null;
                $nextCycle = null;
			}
			if($action) {
				$this->markActionAsExecuted($action, $nextPlanId);
			}
		} else {
            // Create new billing cycle for the same plan
            $dates = $this->getDates($cycle->end_date, $product->interval);
            $nextCycle = $this->repo->createPlanBillingCycle($plan->id, $dates->start, $dates->end, BillingCycle::STATUS_ACTIVE);
		}
		return [
		    'plan' => $plan ? $plan->fresh() : null,
            'cycle' => $nextCycle,
        ];
    }

	public function hasBookings($providerId)
	{
		$plan = $this->getActivePlan($providerId);
		if(!$plan) {
			return false;
		}
		$cycle = $this->repo->getPlanCurrentCycle($plan);
		$bookings = $this->getCycleBookings($cycle);

		if($bookings->left > 0) {
			return true;
		}

		return $plan->allow_over_booking;
    }

    public function checkBookingLimitNotification($providerId)
    {
        if(!$cycle = $this->getProviderCurrentCycle($providerId)) {
            return;
        }
        if($bookings = $this->getCycleBookings($cycle)) {
            $notify_checkpoints = $this->getConfig('booking_limit_notifications');
            if($bookings->over > 0) {
                // Every time when over the limit
            } elseif(in_array($bookings->left, $notify_checkpoints) || $bookings->left === 0) {
                $profileEmail = ProfileService::getNameAndEmail($providerId);
                $profileDetails = ProfileService::loadProfileBasicDetails($providerId);
                $bladeData = array_merge((array) $bookings, [
                    'id' => $profileDetails->id,
                    'slug' => $profileDetails->slug,
                    'firstName' => $profileDetails->first_name,
                    'lastName' => $profileDetails->last_name,
                    'endDate' => $cycle->end_date->toDateString(),
                    'planName' => $cycle->plan->product->description,
                ]);
                $mailEvent = $bookings->left === 0 ?
                    BookingsLimitReachedMailEvent::class :
                    BookingsLimitCloseMailEvent::class;
                EventHelper::fire(new $mailEvent($bladeData, $profileEmail->name, $profileEmail->email));
                EventHelper::fire(new BookingsLimitAdminMailEvent($bladeData));
            }
        }
    }

    public function getProviderBookings($providerId)
	{
		if($cycle = $this->getProviderCurrentCycle($providerId)) {
			return $this->getCycleBookings($cycle);
		}
    }

    private function updateCycleBookings($cycle)
    {
        $bookings = $this->getCycleBookings($cycle);
        $cycle->extra_bookings = $bookings->over;
        $cycle->extra_booking_price = $bookings->price;
        return $bookings;
    }

	private function getCycleBookings($cycle)
	{
		$plan = $cycle->plan;
		$membership = $plan->product->membership;

		$used = AppointmentService::countProviderAppointments($plan->profile_id, $cycle->start_date, $cycle->end_date);

		$result = (object) [
			'price' => 	$membership->over_limit_fee,
			'limit' => $membership->bookings_limit,
			'used' => $used,
			'left' => null,
			'over'=> null,
		];
		if(!is_null($result->limit)) {
            $result->left = max($result->limit - $used, 0);
            $result->over = max($used - $result->limit, 0);
        }
        return $result;
    }

	public function changeProviderPlan(ProviderPlan $original, MembershipProduct $product)
	{
        $cycle = $this->repo->getPlanCurrentCycle($original);
        $change = $this->resolvePlanChange($original, $product);
        if(!$change->allowed) {
            throw new PlanChangeNotAllowed();
        }

        if($pending = $this->repo->getPlanPendingAction($original->id)) {
            if($pending->action === ProviderPlanAction::ACTION_CANCEL) {
                throw new PlanChangeNotAllowed('plan is cancelled');
            }
            $this->cancelPendingAction($pending);
        }

        $amount = $this->calculateUpgradePrice($original, $product);
		$immediate = $change->membership === PlanChange::UPGRADE;

        $action = $this->repo->createPlanAction(
            $immediate ? ProviderPlanAction::ACTION_CHANGE : ProviderPlanAction::ACTION_SWITCH,
            $original->profile_id,
            $original->id,
			$cycle->id,
            $product->id,
            $amount,
			null,
			$immediate
        );

        if($immediate) {
            $plan = $this->repo->createProviderPlan($original->profile_id, $product->id, $original->billing_cycles, $original->id);
            $cycle->plan_id = $plan->id;
            $this->repo->save($cycle);
            $this->activatePlan($plan);
            $this->markActionAsExecuted($action, $plan->id);
        } else {
            $original->renewal_product_id = $product->id;
            $this->repo->save($original);
        }

	    return $cycle;
    }

	public function cancelProviderPlan(ProviderPlan $plan)
	{
		if($pending = $this->repo->getPlanPendingAction($plan->id)) {
            if($pending->action === ProviderPlanAction::ACTION_CANCEL) {
                throw new PlanChangeNotAllowed('plan is already cancelled');
            }
			$this->cancelPendingAction($pending);
		}

		$cycle = $this->repo->getPlanCurrentCycle($plan);

		$this->repo->createPlanAction(
			ProviderPlanAction::ACTION_CANCEL,
			$plan->profile_id,
			$plan->id,
            $cycle->id
		);

		$plan->renewal_product_id = null;
		$plan->allow_over_booking = false;
		$result = $this->repo->save($plan);
		if($result) {
		    Event::fire(new ProviderPlanCancelledEvent($plan));
        }
        return $result;
	}

    public function reviveProviderPlan(ProviderPlan $plan)
    {
        $action = $this->repo->getPlanPendingAction($plan->id);

        if(!$action || $action->action !== ProviderPlanAction::ACTION_CANCEL) {
            throw new PlanChangeNotAllowed('plan not cancelled');
        }
        $this->cancelPendingAction($action);

        $plan->renewal_product_id = $plan->product_id;
        $plan->allow_over_booking = true;
        return $this->repo->save($plan);
	}

	private function cancelPendingAction(ProviderPlanAction $action)
	{
		$action->cancel_date = Carbon::now();
		return $this->repo->save($action);
	}

    private function markActionAsExecuted(ProviderPlanAction $action, $afterPlanId=null)
    {
        $action->action_date = Carbon::now();
        $action->after_plan_id = $afterPlanId;
        return $this->repo->save($action);
    }

    public function calculateUpgradePrice(ProviderPlan $originalPlan, MembershipProduct $upgradeProduct)
    {
        $originalProduct = $originalPlan->product;
        $usedCycles = $this->countPlanCycles($originalPlan) - 1;
        $remainingCycles = $originalProduct->billing_cycles - $usedCycles;
        $priceDelta = $upgradeProduct->pricePerCycle - $originalProduct->pricePerCycle;

        return max($remainingCycles * $priceDelta, 0);
    }

    public function resolvePlanChange(ProviderPlan $originalPlan, MembershipProduct $newProduct)
    {
        $result = new PlanChange();
        $originalProduct = $originalPlan->product;
        if($originalProduct->membership_id !== $newProduct->membership_id) {
            $result->membershipUpgrade($originalProduct->membership->rank > $newProduct->membership->rank);
        }

        if($originalProduct->interval !== $newProduct->interval) {
			$originalEndDate = $this->addCyclePeriod($originalProduct->billing_cycles, $originalProduct->interval);
			$newEndDate = $this->addCyclePeriod($newProduct->billing_cycles, $newProduct->interval);
            $result->termUpgrade($newEndDate->lt($originalEndDate));
        } elseif($originalProduct->billing_cycles !== $newProduct->billing_cycles) {
            $result->termUpgrade($newProduct->billing_cycles > $originalProduct->billing_cycles);
        }
        $result->checkAllowed();

        return $result;
    }

    public function getProviderCurrentCycle($providerId)
    {
        if($plan = $this->getActivePlan($providerId)) {
           return $this->repo->getPlanCurrentCycle($plan);
        }
    }

    public function getCycleBilling(BillingCycle $cycle, $update=false)
    {
    	if($update) {
    		$this->updateCycleBookings($cycle);
		}

        $plan = $cycle->plan;
        $hasCycleCredit = $plan->billing_cycles > $this->countPlanCycles($plan);
        $nextProduct = $hasCycleCredit ? null : $this->repo->getProductById($plan->renewal_product_id);

        $items = [];

        if($nextProduct) {
            $items[] = [
                'type' => EntityName::MEMBERSHIP,
                'description' => $nextProduct->name,
                'qty' => 1,
                'amount' => $nextProduct->price,
                'subtotal' => $nextProduct->price,
            ];
        }

        if($cycle->getExtraBookingTotal()) {
            $items[] = [
                'type' => EntityName::BILLING_CYCLE_FEES,
                'description' => 'Booking Over Limit',
                'qty' => $cycle->extra_bookings,
                'amount' => $cycle->extra_booking_price,
                'subtotal' => $cycle->getExtraBookingTotal(),
            ];
        }

        $upgrades = $this->getUpgradesForBilling($cycle->id);
        $upgrades->each(function($upgrade) use (&$items) {
            $items[] = [
                'type' => EntityName::MEMBERSHIP_UPGRADE,
                'description' => 'upgrade to ' . $upgrade->product->name,
                'qty' => 1,
                'amount' => $upgrade->amount,
                'subtotal' => $upgrade->amount,
            ];
        });

        return [
            'invoice' => null,
            'date' => $cycle->end_date,
            'status' => 'Not Paid',
            'amount' => collect($items)->sum('subtotal'),
            'currency' => PaymentCurrency::CURRENCY_USD,
            'items' => $items,
        ];
    }

    public function getCycleBillableActions($cycleId)
    {
        return $this->repo->getCycleBillableActions($cycleId);
    }

    public function getExpiringBillingCycles()
    {
        return $this->repo->getExpiringBillingCycles(Carbon::now());
    }

    public function activatePlan($plan)
    {
		if($previous = $plan->previous) {
			$status = $previous->product_id === $plan->product_id ? ProviderPlan::STATUS_RENEWED : ProviderPlan::STATUS_UPGRADED;
			$this->repo->terminateProviderPlan($previous, $status);
		}
		$plan->status = ProviderPlan::STATUS_ACTIVE;
		$plan->activate_date = Carbon::now();
		if($this->repo->save($plan)) {
            $cycle = $plan->cycles->first();
            if($cycle->status != BillingCycle::STATUS_ACTIVE) {
                $cycle->status = BillingCycle::STATUS_ACTIVE;
                $this->repo->save($cycle);
            }
            $this->adjustProviderStatus($plan->profile_id);
		    return $plan;
        }
        return false;
    }

    public function getProduct($productId)
    {
        return $this->repo->getProductById($productId);
	}

    public function getProviderPlan($planId)
    {
        return $this->repo->getProviderPlan($planId);
	}

    public function hasFeature($providerId, $feature, $forceStatus=false)
    {
        $features = $this->getProviderFeatures($providerId, $forceStatus);
        if(!is_array($feature)) {
			return in_array($feature, $features);
		}
		$result = [];
		foreach ($feature as $key) {
        	$result[$key] = in_array($key, $features);
		}
		return $result;
    }

    public function getProviderFeatures($providerId, $forceStatus=false)
    {
        $provider = ProfileService::getProfileById($providerId);

        if($provider->type !== ModelProfile::PROVIDER_PROFILE_TYPE) {
        	return [];
		}

		$status = $this->calculateProviderStatus($providerId);
		if($forceStatus) {
			$this->setProviderStatus($providerId, $status->name);
			$provider = $provider->fresh();
		} else {
			$provider->status = $status->name;
		}

		$plan = $this->getActivePlan($providerId);

        $status = $this->tierRepository->getStatusByName($provider->status);
        $features = $status ? $status->features : [];

        if($plan) {
        	$membership = $plan->product->membership;
            $features = array_merge($features, $membership->features ?: []);
        }
        return $features;
    }

    public function createMembership($slug, $name, $description=null, $features=[], $limit=null, $fee=null, $rank=null)
    {
    	if(is_null($rank)) {
			$rank = $this->repo->getLowestRank() + 1;
		}
        $membership = new Membership([
        	'name' => $name,
			'description' => $description,
            'slug' => $slug,
            'features' => $features,
            'bookings_limit' => $limit,
            'over_limit_fee' => $fee,
            'rank' => $rank,
        ]);
        return $this->repo->save($membership) ? $membership : null;
	}

    public function validateMembershipSlug($slug)
    {
        if(!$slug) {
            return false;
        }
        $membership = $this->repo->getMemebershipBySlug($slug);
        return is_null($membership);
	}

    public function createMembershipProduct(
        $membership_id,
        $name,
        $price,
        $description = '',
        $billing_cycles = 1,
        $interval = MembershipProduct::INTERVAL_1MONTH
    )
    {
        if(!$this->makeInterval($interval)) {
            return false;
        }

        $product = new MembershipProduct([
            'membership_id' => $membership_id,
            'name' => $name,
            'description' => $description,
            'price' => $price,
            'billing_cycles' => $billing_cycles,
            'interval' => $interval,
        ]);
        return $this->repo->save($product) ? $product : null;
    }

    public function adjustProviderStatus($providerId)
    {
        $status = $this->calculateProviderStatus($providerId);
        return $this->setProviderStatus($providerId, $status->name) ? $status->name : false;
    }

    public function setProviderStatus($providerId, $status)
    {
        return ProfileService::setProfileStatus($providerId, $status);
    }

    public function getBillingCycle($id)
    {
        return $this->repo->getBillingCycleById($id);
    }

    public function getActionRecord($id)
    {
        return $this->repo->getPlanActionById($id);
    }

	public function getProducts(ProviderPlan $plan = null)
	{
		$profileId = $plan ? $plan->profile_id : null;
		$products = $this->repo->getAvailableProducts($profileId);
		$products = $products->map(function($product) use ($plan) {
			return [
				'product' => $product,
				'change' => $plan ? $this->resolvePlanChange($plan, $product) : null,
			];
		});

		return $products->groupBy('product.membership_id')->map(function($group) {
			$first = $group->first();
			if($change = $first['change']) {
				$dictionary = [
					PlanChange::UPGRADE => MembershipActions::UPGRADE,
					PlanChange::DOWNGRADE => MembershipActions::DOWNGRADE,
					PlanChange::NO_CHANGE => MembershipActions::SWITCHTO,
				];
				$action = array_get($dictionary, $change->membership);
			} else {
				$action = MembershipActions::BUY_NOW;
			}

			return [
				'type' => $first['product']->membership,
				'action' => $action,
				'products' => $group,
			];
		});
	}

	public function getPlanDetails(ProviderPlan $plan)
	{
	    $plan = $this->addCalculatedAttributes($plan);
		$products = $this->getProducts($plan);
		$action = $this->repo->getPlanPendingAction($plan->id);
		if($action && $action->action === ProviderPlanAction::ACTION_CANCEL) {
			$actions = [PlanChange::ACTION_REVIVE];
		} else {
			$actions = [PlanChange::ACTION_CANCEL];
			if(count($products)) {
				$actions[] = PlanChange::ACTION_CHANGE;
			}
		}

		$cycle = $this->repo->getPlanCurrentCycle($plan);
		$bookings = $this->updateCycleBookings($cycle);

		return [
			'plan' => $plan,
			'bookings' => $bookings,
			'pending_action' => $action,
			'products' => $products,
			'actions' => $actions,
			'estimate' => $this->getCycleBilling($cycle),
		];
	}

	public function updateProviderPlan(ProviderPlan $plan, $cycles)
	{
		if($this->countPlanCycles($plan) > $cycles) {
			throw new InvalidNumberOfBillingCycles();
		}

		$plan->billing_cycles = $cycles;
		return $this->repo->save($plan);
	}

    public function addCycles($planId, $cycles)
    {
        $plan = $this->getProviderPlan($planId);
        if(!$plan) {
            throw new ModelNotFoundException();
        }
        $totalCycles = $plan->billing_cycles + $cycles;
        return $this->updateProviderPlan($plan, $totalCycles);
	}

    public function collectBillingCycleItems(BillingCycle $cycle, BillingCycle $nextCycle=null)
    {
        $items = [];

        if($nextCycle && $cycle->plan_id !== $nextCycle->plan_id && $nextCycle->status !== BillingCycle::STATUS_ACTIVE) {
            $items[] = [
                'name' => EntityName::MEMBERSHIP,
                'id' => $nextCycle->plan_id,
            ];
        }

        if($cycle->getExtraBookingTotal()) {
            $items[] = [
                'name' => EntityName::BILLING_CYCLE_FEES,
                'id' => $cycle->id,
            ];
        }

        $upgrades = $this->getUpgradesForBilling($cycle->id);
        $upgrades->each(function($upgrade) use (&$items) {
            $items[] = [
                'name' => EntityName::MEMBERSHIP_UPGRADE,
                'id' => $upgrade->id,
            ];
        });

        return $items;
	}

    public function getFirstActivation($providerId)
    {
        return $this->repo->getFirstActivation($providerId);
	}

    public function getProviderActions($providerId)
    {
        return $this->repo->getProviderActions($providerId, $hideCanceled=true);
	}

	public function terminatePlan(ProviderPlan $plan, $status = ProviderPlan::STATUS_CANCELLED)
	{
		$this->repo->terminateProviderPlan($plan, $status);
		$this->adjustProviderStatus($plan->profile_id);
		Event::fire(new ProviderPlanTerminatedEvent($plan));
	}

    public function getPlanEndDate($planId)
    {
        $plan = $this->repo->getProviderPlan($planId);
        return $this->calculatePlanEnd($plan);
	}

    private function getUpgradesForBilling($cycleId)
    {
        $upgrades = $this->getCycleBillableActions($cycleId);
        return $upgrades->filter(function($upgrade) {
            if($upgrade->amount <= 0) {
                return false;
            }
            $orderItem = OrderService::getOrderContainingEntity($upgrade->id, EntityName::MEMBERSHIP_UPGRADE);
            return is_null($orderItem);
        });
	}

    private function countPlanCycles(ProviderPlan $plan)
    {
        $count = $plan->cycles->count();
        $prePlan = $this->repo->getUpgradedFromPlan($plan->id);
        if($prePlan) {
            $count += $this->countPlanCycles($prePlan);
        }
        return $count;
    }

    private function calculateProviderStatus($providerId)
    {
        $requirements = ProviderServiceFacade::collectRequirements($providerId);
        return $this->tierRepository->getStatusByRequirements($requirements);
    }

	private function calculatePlanEnd(ProviderPlan $plan)
	{
		$cyclesLeft = $plan->billing_cycles - $this->countPlanCycles($plan);
		$cycle = $this->repo->getPlanCurrentCycle($plan);
		return $this->addCyclePeriod($cyclesLeft, $plan->product->interval, $cycle->end_date);
    }

	private function addCyclePeriod($cycles, $interval, Carbon $start = null)
	{
		$interval = $this->makeInterval($interval);
		$end = $start ? $start->copy(): Carbon::now();

		for($i=0;$i<$cycles;$i++) {
			$end->add($interval);
		}

		return $end;
    }

    private function getDates($start=null, $interval=MembershipProduct::INTERVAL_1MONTH)
    {
        if(!$start instanceof Carbon) {
            $start = Carbon::now();
        }
        $end = $start->copy()->add($this->makeInterval($interval));
        return (object) compact('start', 'end');
    }

    private function makeInterval($interval)
    {
        try {
            return new DateInterval('P' . $interval);
        } catch (\Exception $e) {

        }
    }

	public function saveSubscriptionToPlan(ProviderPlan $plan, $id)
	{
		$plan->subscription_id = $id;
		return $this->repo->save($plan);
	}

    private function getConfig($key)
    {
        return array_get($this->config, $key);
	}
}

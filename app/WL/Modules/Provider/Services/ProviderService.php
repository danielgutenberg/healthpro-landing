<?php namespace WL\Modules\Provider\Services;

use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;
use WL\Modules\Location\Facades\LocationService;
use WL\Modules\Payment\Facades\PaymentService;
use WL\Modules\Payment\Models\PaymentMethod;
use WL\Modules\Payment\Models\PaymentMethodApplication;
use WL\Modules\PricePackage\Facade\PricePackageService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Provider\Events\AcceptPaymentMethodUpdateEvent;
use WL\Modules\Provider\Events\ProgressUpdate;
use WL\Modules\Provider\Events\ServiceAdded;
use WL\Modules\Provider\Events\SessionAdded;
use WL\Modules\Provider\Events\SessionRemoved;
use WL\Modules\Provider\Exceptions\ProviderConfigurationNotAllowed;
use WL\Modules\Provider\Exceptions\ProviderException;
use WL\Modules\Provider\Exceptions\ProviderServiceOnlySessionException;
use WL\Modules\Provider\Models\ProviderService as Service;
use WL\Modules\Provider\Models\ProviderServiceSession;
use WL\Modules\Provider\Facades\ProviderMembershipService as ProviderMembershipServiceFacade;
use WL\Modules\Provider\Repositories\ProviderServiceRepositoryInterface;
use WL\Modules\Provider\Repositories\ProviderServiceSessionRepositoryInterface;
use WL\Modules\Provider\Services\Exceptions\WrongProfileType;
use WL\Modules\Provider\ValueObjects\PartialAmount;
use WL\Modules\Provider\ValueObjects\ProviderFeatures;
use WL\Modules\Provider\ValueObjects\ProviderRequirements;
use WL\Modules\Taxonomy\Facades\TagService;
use WL\Modules\Taxonomy\Facades\TaxonomyService;
use WL\Modules\User\Facades\User;

class ProviderService implements ProviderServiceInterface
{
	private $providerServiceRepository;
	private $providerServiceSessionRepository;
	private $parentTaxonomyId;

	function __construct(
		ProviderServiceRepositoryInterface $providerServiceRepository,
		ProviderServiceSessionRepositoryInterface $providerServiceSessionRepository
	) {
		$this->providerServiceRepository = $providerServiceRepository;
		$this->providerServiceSessionRepository = $providerServiceSessionRepository;
		$this->parentTaxonomyId = TaxonomyService::getBySlug('Services')->id;
	}

	public function getServiceTypes()
	{
		return TagService::getTaxonomyTags($this->parentTaxonomyId, $loadAllTags = true);
	}

	public function sortByCount($taxonomyType)
	{
		return TagService::sortByCount($taxonomyType);
	}

	public function getService($providerServiceId, $deleted = false)
	{
		return $this->providerServiceRepository->getByIdEager($providerServiceId, $deleted);
	}

	public function addService($profileId, $name, $serviceTypeIds, $locationIds = [], $sessions = [], $packages = [], $certificates = null, $deposit = null, $depositType = null, $description = null)
	{
		$profile = ProfileService::getProfileById($profileId);

		if ($profile->type != ModelProfile::PROVIDER_PROFILE_TYPE) {
			throw new WrongProfileType("Only provider profile type supported. Wrong profile type is [$profile->type]");
		}

		$this->validateDeposit($deposit, $depositType);

		$deposit = $deposit === null ? 0 : $deposit;
		$depositType = $depositType === null ? PartialAmount::FLAT_RATE : $depositType;
		$certificates = $certificates === null ? 0 : $certificates;

        $this->checkGiftCeretificateAllowed($profileId, $certificates);

		$providerService = new Service([
			'profile_id' => $profileId,
			'name' => $name,
			'deposit' => $deposit,
			'deposit_type' => $depositType,
			'gift_certificates' => $certificates,
			'description' => $description
		]);

		$this->providerServiceRepository->save($providerService);

		// we don't use transaction here because on mysql we have deadlock in mysql
		// these isn't good solution because we expose repository persistence logic to service
		// Problem details:
		// When we create service in transaction and immediately use it in 'select' MySQL see lock of provider id because
		// of unfinished transaction. This can be solved by transaction isolation level but this is not service part at all.
		try {
			if (!empty($sessions)) {
				$this->updateServiceSessions($providerService->id, $sessions, $suppressEvents = true);
			}

			if (!empty($locationIds)) {
				$services = $this->getServices($profileId);
				$this->validateProviderLocation($services, $serviceTypeIds, $locationIds);
				foreach ($locationIds as $locationId) {
					$this->addLocation($providerService->id, $locationId);
				}
			}

		} catch (QueryException $qe) {
			$this->removeService($providerService->id);
			throw $qe;
		} catch (ValidationException $ex) {
			$this->removeService($providerService->id);
			throw $ex;
		} catch (InvalidArgumentException $iae) {
			$this->removeService($providerService->id);
			throw $iae;
		}

		TagService::explicitSetTagIdsWithEntity($providerService, $serviceTypeIds, $this->parentTaxonomyId);

		event(new ServiceAdded($profileId));

		return $providerService;
	}

	public function updateService($providerServiceId, $name, $serviceTypeIds, $locationIds = [], $certificates = null, $deposit = null, $depositType = null, $description = null)
	{
		$providerService = $this->providerServiceRepository->getById($providerServiceId);

		// this can must fail before we update tags
		if (!(empty($locationIds))) {
			$services = $this->getServices($providerService->profile_id);
			$services = $services->reject(function ($item) use ($providerService) {
				return $item->id == $providerService->id;
			});
			$this->validateProviderLocation($services, $serviceTypeIds, $locationIds);

			$this->updateLocations($providerService, $locationIds);
		}

		if (!($serviceTypeIds === null)) {
			TagService::explicitSetTagIdsWithEntity($providerService, $serviceTypeIds, $this->parentTaxonomyId);
		}

		if (!($deposit === null)) {
			$depositType = $depositType === null ? PartialAmount::FLAT_RATE : $depositType;
			$this->validateDeposit($deposit, $depositType);
			$providerService->deposit = $deposit;
			$providerService->deposit_type = $depositType;
		}

		if (!($description === null)) {
			$providerService->description = $description;
		}

		if (!($certificates === null)) {
            $this->checkGiftCeretificateAllowed($providerService->profile_id, $certificates);
			$providerService->gift_certificates = $certificates;
        }

        $this->providerServiceRepository->save($providerService);

        return $providerService;
	}

    private function checkGiftCeretificateAllowed($profileId, $certificates)
    {
        $accepts = ProfileService::loadProfileMetaField($profileId, 'accepted_payment_methods', PaymentMethod::ONLY_IN_PERSON);
        if($certificates !== 0 && $accepts === PaymentMethod::ONLY_IN_PERSON) {
            throw new ProviderConfigurationNotAllowed();
        }
	}

	public function sortServices($providerServiceIds)
	{
		foreach ($providerServiceIds as $index => $id){
			$providerService = $this->providerServiceRepository->getById($id);
			$providerService->sort = $index;
			$this->providerServiceRepository->save($providerService);
		}
	}

	public function removeService($providerServiceId)
	{
		$providerService = $this->providerServiceRepository->getById($providerServiceId);

		$this->providerServiceRepository->destroy($providerServiceId);

		event(new SessionRemoved($providerService->profile_id));

		return true;
	}

	public function getServices($profileId)
	{
		return $this->providerServiceRepository->getByProfileId($profileId);
	}

    public function offerGiftCertificates($profileId)
    {
        $service = $this->providerServiceRepository->getWithGiftCertificate($profileId);
        return !$service->isEmpty();
	}

    public function removeAllGiftCertificates($profileId)
    {
        $service = $this->providerServiceRepository->getWithGiftCertificate($profileId);
        $service->each(function($service) {
            $service->gift_certificates = 0;
            $this->providerServiceRepository->save($service);
        });
        return $service->count();
	}

    public function setAcceptPaymentMethods($profileId, $method)
    {
        $success = ProfileService::storeProfileMetaField($profileId, 'accepted_payment_methods', $method);
        if($success) {
            Event::fire(new AcceptPaymentMethodUpdateEvent($profileId, $method));
        }
        return $success;
	}

	public function getServicesByIds($profileIds)
	{
		return $this->providerServiceRepository->getByProfileIds($profileIds);
	}

	public function getSessions($profileId)
	{
		return $this->providerServiceSessionRepository->getByProviderId($profileId);
	}


	public function getIntroductorySession($providerId)
	{
		return $this->getSessions($providerId)->filter(function($session) {
			return $session->is_first_time && $session->service && !$session->service->deleted_at;
		})->first();
	}

	public function isIntroductorySessionService(Service $service)
	{
		$serviceType = $service->serviceTypes->first();

		if ($serviceType) {
			return $serviceType->slug == Service::INTRODUCTORY_SESSION_SLUG;
		}

		return false;
	}

	public function getServicesTags($profileId)
	{
		$services = $this->getServices($profileId);
		$serviceTags = [];

		foreach ($services as $service) {
			if (count($service->serviceTypes)) {
				$serviceTags[$service->serviceTypes[0]->id] = $service->serviceTypes[0];
			}
		}

		return $serviceTags;
	}

	public function addLocation($providerServiceId, $locationId)
	{
		$this->providerServiceRepository->addLocation($providerServiceId, $locationId);
	}

	public function removeLocation($providerServiceId, $locationId)
	{
		$this->providerServiceRepository->removeLocation($providerServiceId, $locationId);
	}

	/**
	 * Updates locations for a Provider Service
	 *
	 * @param $providerService ProviderService
	 * @param $locationIds array<number>
	 */
	private function updateLocations($providerService, array $locationIds)
	{
		$oldLocation = $providerService->locations;
		foreach ($oldLocation as $location) {
			$this->removeLocation($providerService->id, $location->id);
		}

		foreach ($locationIds as $locationId) {
			$this->addLocation($providerService->id, $locationId);
		}
	}

	public function isSessionExists($providerServiceId, $price, $duration, $isFirstTime)
	{
		return $this->providerServiceSessionRepository->exists($providerServiceId, $price, $duration, $isFirstTime);
	}

	public function addServiceSession($providerServiceId, $price, $duration, $isFirstTime = false, $title = null)
	{
		$validator = Validator::make(
			[
				'price' => $price,
				'duration' => $duration
			],
			[
				'price' => 'required|min:0',
				'duration' => 'required|min:0'
			]
		);

		if ($validator->fails()) {
			throw new ValidationException($validator);
		}

		$providerServiceSession = new ProviderServiceSession([
			'provider_service_id' => $providerServiceId,
			'price' => $price,
			'duration' => $duration,
			'is_first_time' => $isFirstTime,
			'title' => $title
		]);

		$this->providerServiceSessionRepository->save($providerServiceSession);

		$providerService = $this->providerServiceRepository->getById($providerServiceId);
		event(new SessionAdded($providerService->profile_id));

		return $providerServiceSession;
	}

	public function updateServiceSession($providerServiceSessionId, $price, $duration = null, $isFirstTime = null)
	{
		$providerServiceSession = $this->providerServiceSessionRepository->getById($providerServiceSessionId);

		if ($price != null) {
			$providerServiceSession->price = $price;
		}

		if ($duration != null) {
			$providerServiceSession->duration = $duration;
		}

		if ($isFirstTime != null) {
			$providerServiceSession->is_first_time = $isFirstTime;
		}

		$this->providerServiceSessionRepository->save($providerServiceSession);

		return $providerServiceSession;
	}

	private function validateServiceSessions(array $sessions)
	{
		$validator = Validator::make(
			[
				'sessions' => $sessions
			],
			[
				'sessions' => 'array|provider_session_duplication'
			]
		);

		$validator->each('sessions', ['price' => 'numeric|min:0']);
		$validator->each('sessions', ['duration' => 'numeric|min:0']);
		$validator->each('sessions', ['is_first_time' => 'boolean']);

		return $validator;
	}

	public function updateServiceSessions($providerServiceId, array $sessions, $suppressEvents = false)
	{
		if (empty($sessions)) {
			throw new InvalidArgumentException('Session array is empty');
		}

		$validator = $this->validateServiceSessions($sessions);

		if ($validator->fails()) {
			throw new ValidationException($validator);
		}

		$providerService = $this->providerServiceRepository->getById($providerServiceId);
		$profile_id = $providerService->profile_id;

		foreach ($sessions as $session) {
			$session = (array)$session;
			if (isset($session['id'])) {
				$serviceSession = $this->providerServiceSessionRepository->getById($session['id']);

				$serviceSession->active = $session['active'];
				$serviceSession->price = $session['price'];
				$serviceSession->duration = $session['duration'];
				$serviceSession->is_first_time = $session['is_first_time'];
				$serviceSession->title = $session['title'];
			} else {
				$serviceSession = ProviderServiceSession::firstOrCreate([
					'active' => $session['active'],
					'price' => $session['price'],
					'duration' => $session['duration'],
					'is_first_time' => $session['is_first_time'],
					'provider_service_id' => $providerServiceId,
					'title' => $session['title']
				]);
			}

			$serviceSession->save();

			if (isset($session['packages']) && count($session['packages'])) {
				foreach ($session['packages'] as &$package) {
					$package['entity_id'] = $serviceSession->id;
					$package['entity_type'] = ProviderServiceSession::PACKAGE_ENTITY_TYPE;
					$package['rules'] = isset($package['rules']) ? $package['rules'] : [];
					$package['number_of_months'] = isset($package['number_of_months']) ? $package['number_of_months'] : null;
				}

				PricePackageService::updateSessionPackages($providerService->profile_id, $session['packages']);
			}
		}

		event(new SessionAdded($providerService->profile_id));

		return true;
	}

	public function getServiceSession($providerServiceSessionId)
	{
		return $this->providerServiceSessionRepository->getById($providerServiceSessionId);
	}

	public function removeServiceSession($providerServiceSessionId)
	{
		$service_session = $this->providerServiceSessionRepository->getById($providerServiceSessionId);
		$profileId = $service_session->service->profile_id;

		$this->providerServiceSessionRepository->deactivate($providerServiceSessionId);

		event(new SessionRemoved($profileId));
	}

	public function getProgress($profileId)
	{
		$profile = ProfileService::getProfileById($profileId);

		if (!$profile) {
			throw new ProviderException("Profile not found");
		}

		if ($profile->type != ModelProfile::PROVIDER_PROFILE_TYPE) {
			throw new WrongProfileType("Only provider profile type supported. Wrong profile type is [$profile->type]");
		}

		$paymentOptions = PaymentService::getProfessionalPaymentOptions($profileId);
		$inPerson = $this->acceptsPaymentMethod($profileId, PaymentMethod::ONLY_IN_PERSON);

		$locations = LocationService::getLocations($profileId);

		$avatar = ProfileService::loadUploadedFileUrl($profile, 'avatar', '250x250');

		$hasService = false;
		$hasSession = false;

		// introductory session doesn't count as a service
		foreach (self::getServices($profileId) as $service) {
			// simple check for introductory session
			if ($service->name == 'Introductory-Session' || self::isIntroductorySessionService($service)) {
				continue;
			}

			$hasService = true;
			if (!$service->sessions->isEmpty()) {
				$hasSession = true;
				break;
			}
		}

		$aboutMeta = ProfileService::loadProfileMetaFields($profileId, [
			'about_self_background',
			'about_self_specialities',
			'about_services_benefit',
			'about_what_i_do',
		]);
		$hasAbout = strlen(implode('', $aboutMeta)) >= 20;
		$user = ProfileService::getOwner($profileId);
		return [
			'acceptedTerms' => !is_null(ProfileService::loadProfileMetaField($profileId, 'accepted_terms_conditions')),
			'hasPhone' => $profile->phone && $profile->phone->verified_at,
			'emailVerified' => $user ? User::isUserActivated($user) : false,
			'hasAvatar' => !is_null($avatar),
			'hasService' => $hasService,
			'hasLocation' => !$locations->isEmpty(),
			'hasSession' => $hasSession,
			'hasAbout' => $hasAbout,
			'hasPayment' => !empty($paymentOptions) || $inPerson,
		];
	}

    public function collectRequirements($providerId)
    {
        $profile = ProfileService::getProfileById($providerId);
        $progress = $this->getProgress($providerId);
        $plan = ProviderMembershipServiceFacade::getActivePlan($providerId);
        return [
            ProviderRequirements::ABOUT_ME => $progress['hasAbout'],
			ProviderRequirements::ACCEPTED_TERM => $progress['acceptedTerms'],
			ProviderRequirements::ACTIVE => is_null($profile->deleted_at),
			ProviderRequirements::PROVIDER => $profile->type === ModelProfile::PROVIDER_PROFILE_TYPE,
			ProviderRequirements::EMAIL_VERIFIED => $progress['emailVerified'],
			ProviderRequirements::LOCATIONS => $progress['hasLocation'] && $progress['hasService'] && $progress['hasSession'],
			ProviderRequirements::MEMBERSHIP => !is_null($plan),
			ProviderRequirements::RECIEVE_PAYMENT => $progress['hasPayment'],
			ProviderRequirements::HIDDEN => false, //TODO
        ];
    }

    public function adjustProviderStatus($providerId)
    {
        $features = ProviderMembershipServiceFacade::getProviderFeatures($providerId, true);
        ProfileService::setIsSearchable($providerId, in_array(ProviderFeatures::SEARCHABLE, $features));
        ProfileService::setIsBookable($providerId, in_array(ProviderFeatures::BOOKABLE, $features));
		event(new ProgressUpdate($providerId));
    }

	public function acceptsPaymentMethod($providerId, $method)
	{
		$accepts = ProfileService::loadProfileMetaField($providerId, 'accepted_payment_methods');
		if (!$accepts && $method == PaymentMethod::ONLY_IN_PERSON) {
			return true;
		}
		if($accepts === PaymentMethod::ALL_PAYMENTS) {
			return true;
		}
		return $accepts === $method;
    }

    public function getServiceSessions($sessionId)
    {
        return $this->providerServiceSessionRepository->getServiceSessions($sessionId);
    }

    public function checkCanDeleteDependant($sessionId)
    {
        $sessionCount = $this->getServiceSessions($sessionId)->count();
        $packageCount = PricePackageService::getServiceMonthlyPackages($sessionId)->count();
        if($sessionCount + $packageCount === 1) {
            throw new ProviderServiceOnlySessionException();
        }
    }

	private function validateProviderLocation($services, $serviceTypeIds, $locationIds)
	{
		if(!is_array($serviceTypeIds) || !is_array($locationIds)){
			return;
		}
		foreach ($services as $service) {
			$usedLocationIds = $service->locations->pluck('id')->all();
			$usedServiceTypeIds = $service->serviceTypes->pluck('id')->all();
			$locationFound = array_intersect($usedLocationIds, $locationIds);
			$serviceTypesFound = array_intersect($usedServiceTypeIds, $serviceTypeIds);
			if (!empty($locationFound) && !empty($serviceTypesFound)) {
				throw new InvalidArgumentException("Location already used.");
			}
		}
	}

	private function validateDeposit($deposit, $depositType)
	{
		if (!is_numeric($deposit)) {
			return;
		}

		if ($deposit < 0) {
			throw new InvalidArgumentException("Deposit must be > 0");
		}

		if ($depositType == PartialAmount::PERCENTAGE && $deposit > 100) {
			throw new InvalidArgumentException("Deposit must be in [0..100]");
		}
	}

}

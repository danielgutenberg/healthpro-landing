<?php namespace WL\Modules\Provider\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Provider\Services\ProviderMembershipServiceInterface;

class ProviderMembershipService extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return ProviderMembershipServiceInterface::class;
	}
}

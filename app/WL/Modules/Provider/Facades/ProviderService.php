<?php namespace WL\Modules\Provider\Facades;

use Illuminate\Support\Facades\Facade;

class ProviderService extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'provider_service';
	}
}

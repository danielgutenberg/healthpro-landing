<?php namespace WL\Modules\Provider\Repositories;


use Carbon\Carbon;
use Illuminate\Support\Collection;
use WL\Modules\Provider\Models\BillingCycle;
use WL\Modules\Provider\Models\Membership;
use WL\Modules\Provider\Models\MembershipProduct;
use WL\Modules\Provider\Models\ProviderPlan;
use WL\Modules\Provider\Models\ProviderPlanAction;
use WL\Repositories\Repository;

interface ProviderMembershipRepositoryInterface extends Repository
{
    /**
     * @param int $profileId
     * @return ProviderPlan
     */
    public function getProfileActivePlan($profileId);

	/**
	 * @param int $profileId
	 * @return Collection
	 */
	public function getProfilePlanHistory($profileId);

    /**
     * @param int $planId
     * @return ProviderPlan
     */
    public function getProviderPlan($planId);

	/**
	 * @param ProviderPlan $plan
	 * @param string $status
	 * @return ProviderPlan
	 */
	public function terminateProviderPlan(ProviderPlan $plan, $status = ProviderPlan::STATUS_ENDED);

    /**
     * @param int $productId
     * @return MembershipProduct
     */
    public function getProductById($productId);

    /**
	 * @param Carbon|null $before
     * @return Collection
     */
    public function getExpiringBillingCycles(Carbon $before);

    /**
     * @param int $membershipId
     * @return Membership
     */
    public function getMembership($membershipId);

    /**
     * @param string $slug
     * @return Membership
     */
    public function getMemebershipBySlug($slug);

    /**
     * @return int
     */
    public function getLowestRank();

    /**
     * @param int $id
     * @return BillingCycle
     */
    public function getBillingCycleById($id);

    /**
     * @param int $id
     * @return ProviderPlanAction
     */
    public function getPlanActionById($id);

    /**
     * @param int $profileId
     * @param int $productId
     * @param int $billingCycles
     * @param int|null $previousId
     * @return ProviderPlan
     */
    public function createProviderPlan($profileId, $productId, $billingCycles=1, $previousId=null);

    /**
     * @param int $planId
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @param string $status
     * @return BillingCycle
     */
    public function createPlanBillingCycle($planId, Carbon $startDate, Carbon $endDate, $status=BillingCycle::STATUS_PENDING);

    /**
     * @param string $actionType
     * @param int $profileId
     * @param int $planId
     * @param int $cycleId
     * @param int|null $productId
     * @param float $amount
     * @param int|null $couponId
     * @param bool $billable
     * @return ProviderPlanAction
     */
    public function createPlanAction($actionType, $profileId, $planId, $cycleId, $productId=null, $amount=0.00, $couponId=null, $billable=false);

	/**
	 * @param ProviderPlan $plan
	 * @return BillingCycle
	 */
    public function getPlanCurrentCycle(ProviderPlan $plan);

    /**
     * @param int $cycleId
     * @return ProviderPlanAction
     */
    public function getCycleBillableActions($cycleId);

	/**
	 * @param int $planId
	 * @return ProviderPlanAction
	 */
	public function getPlanPendingAction($planId);

    /**
     * @param int $planId
     * @return ProviderPlan
     */
    public function getUpgradedFromPlan($planId);

	/**
	 * @param int $profileId (optional)
	 * @return Collection
	 */
    public function getAvailableProducts($profileId=null);

    /**
     * @param int $providerId
     * @return Carbon
     */
    public function getFirstActivation($providerId);

    /**
     * @param int $providerId
     * @param bool $hideCanceled
     * @return Collection
     */
    public function getProviderActions($providerId, $hideCanceled=true);
}

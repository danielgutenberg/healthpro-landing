<?php namespace WL\Modules\Provider\Repositories;


use Illuminate\Support\Collection;
use WL\Modules\Provider\ValueObjects\ProviderStatus;

interface ProviderStatusRepositoryInterface
{
    /**
     * @return Collection
     */
    public function getAllStatuses();

    /**
     * @param array $requirements
     * @return ProviderStatus
     */
    public function getStatusByRequirements($requirements);

    /**
     * @param $name
     * @return ProviderStatus
     */
    public function getStatusByName($name);

}

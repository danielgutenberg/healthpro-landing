<?php namespace WL\Modules\Provider\Repositories;


use Illuminate\Support\Collection;
use WL\Modules\Provider\ValueObjects\ProviderStatus;

class ProviderStatusRepository implements ProviderStatusRepositoryInterface
{
    protected $statuses;

    public function __construct($statuses)
    {
        $this->statuses = $statuses;
    }

    public function getAllStatuses()
    {
        $statuses = new Collection();
        foreach ($this->statuses as $name => $config) {
            $statuses->push($this->makeStatusModel($name, $config));
        }
        return $statuses->sortBy('priority');
    }

    public function getStatusByRequirements($requirements)
    {
        $statuses = $this->getAllStatuses();
        $name = $statuses->search(function($status) use ($requirements){
            return $status->checkRequirements($requirements);
        });
        return $name !== false ? $statuses->get($name) : null;
    }

    public function getStatusByName($name)
    {
        if(isset($this->statuses[$name])) {
            return $this->makeStatusModel($name, $this->statuses[$name]);
        }
    }

    private function makeStatusModel($name, $config)
    {
        $model = new ProviderStatus();
        $model->name = $name;
        $model->priority = $config['priority'];
        $model->features = $config['features'] ?: [];
        $model->requirements = $config['requirements'] ?: [];
        return $model;
    }
}

<?php namespace WL\Modules\Provider\Repositories;


use Illuminate\Support\Facades\DB;
use WL\Modules\Provider\Models\ProviderService;
use WL\Modules\Provider\Models\ProviderServiceSession;
use WL\Repositories\DbRepository;

class DbProviderServiceSessionRepository extends DbRepository implements ProviderServiceSessionRepositoryInterface
{
	public function getById($id)
	{
		return ProviderServiceSession::find($id);
	}

	public function exists($providerServiceId, $price, $duration, $isFirstTime)
	{
		return ProviderServiceSession::where('provider_service_id', $providerServiceId)
			->where('duration', $duration)
			->where('price', $price)
			->where('is_first_time', $isFirstTime)
			->exists();
	}

	public function getByProviderId($profileId)
	{
		$sessionsTableName = ProviderServiceSession::getTableName();
		$servicesTableName = ProviderService::getTableName();

		$query = DB::table($sessionsTableName)
			->select("$sessionsTableName.*")
			->join($servicesTableName, "$sessionsTableName.provider_service_id", '=', "$servicesTableName.id")
			->where("$servicesTableName.profile_id", $profileId);

		return ProviderServiceSession::hydrate($query->get());
	}

    public function deactivate($sessionId)
    {
        return ProviderServiceSession::find($sessionId)->update(['active' => false]);
	}

    public function getServiceSessions($serviceId)
    {
        return ProviderServiceSession::where("provider_service_id", $serviceId)->where("active", 1)->get();
	}
}

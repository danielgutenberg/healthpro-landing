<?php namespace WL\Modules\Provider\Repositories;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use WL\Modules\Provider\Models\BillingCycle;
use WL\Modules\Provider\Models\Membership;
use WL\Modules\Provider\Models\MembershipProduct;
use WL\Modules\Provider\Models\ProviderPlan;
use WL\Modules\Provider\Models\ProviderPlanAction;
use WL\Repositories\DbRepository;

class ProviderMembershipRepository extends DbRepository implements ProviderMembershipRepositoryInterface
{
    public function getProfileActivePlan($profileId)
    {
        return ProviderPlan::where('profile_id', $profileId)
            ->where('status', ProviderPlan::STATUS_ACTIVE)->first();
    }

	public function getProfilePlanHistory($profileId)
	{
		return ProviderPlan::where('profile_id', $profileId)->get();
	}

	public function getProviderPlan($planId)
	{
		return ProviderPlan::find($planId);
    }

    public function terminateProviderPlan(ProviderPlan $plan, $status = ProviderPlan::STATUS_ENDED)
    {
        $plan->status = $status;
        $plan->terminate_date = Carbon::now();
        return $this->save($plan) ? $plan : null;
    }

    public function getProductById($productId)
    {
        return MembershipProduct::find($productId);
    }

    public function getExpiringBillingCycles(Carbon $before)
    {
        $table = BillingCycle::getTableName();
        $planTable = ProviderPlan::getTableName();
        $query = DB::table($table . ' AS t')->select('t.*')
            ->join($planTable . ' AS p', 't.plan_id', '=', 'p.id')
            ->where('t.status', BillingCycle::STATUS_ACTIVE)
            ->where('t.end_date', '<', $before)
            ->where('p.status', ProviderPlan::STATUS_ACTIVE);
        return BillingCycle::hydrate($query->get());
    }

    public function getMembership($membershipId)
    {
        return Membership::find($membershipId);
    }

	public function getMemebershipBySlug($slug)
	{
        return Membership::where('slug', $slug)->first();
    }

    public function getLowestRank()
    {
        return (int) Membership::max('rank');
    }

    public function getBillingCycleById($id)
    {
        return BillingCycle::find($id);
    }

    public function createProviderPlan($profileId, $productId, $billingCycles=1, $previousId=null)
    {
        return ProviderPlan::create([
            'profile_id' => $profileId,
            'product_id' => $productId,
            'previous_plan_id' => $previousId,
            'status' => ProviderPlan::STATUS_PENDING,
            'renewal_product_id' => $productId,
			'billing_cycles' => $billingCycles,
        ]);
    }

    public function createPlanBillingCycle($planId, Carbon $startDate, Carbon $endDate, $status=BillingCycle::STATUS_PENDING)
    {
        return BillingCycle::create([
            'plan_id' => $planId,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'status' => $status,
        ]);
    }

    public function createPlanAction($actionType, $profileId, $planId, $cycleId, $productId=null, $amount=0.00, $couponId=null, $billable=false)
    {
        return ProviderPlanAction::create([
            'profile_id' => $profileId,
            'before_plan_id' => $planId,
            'billing_cycle_id' => $cycleId,
            'amount' => $amount,
            'action' => $actionType,
            'request_date' => Carbon::now(),
            'product_id' => $productId,
            'coupon_id' => $couponId,
			'billable' => $billable,
        ]);
    }

	public function getPlanCurrentCycle(ProviderPlan $plan)
	{
		return $plan->cycles->where('status', BillingCycle::STATUS_ACTIVE)->first();
	}

    public function getCycleBillableActions($cycleId)
    {
        return ProviderPlanAction::where('billing_cycle_id', $cycleId)
            ->whereNotNull('action_date')
            ->where('billable', 1)
            ->get();
    }

    public function getPlanPendingAction($planId)
    {
        return ProviderPlanAction::where('before_plan_id', $planId)
			->whereNull('action_date')
			->whereNull('cancel_date')
			->first();
    }

    public function getPlanActionById($id)
    {
        return ProviderPlanAction::find($id);
    }

    public function getUpgradedFromPlan($planId)
    {
        $table = ProviderPlan::getTableName();
        $actionTable = ProviderPlanAction::getTableName();
        $result = DB::table($table . ' AS t')
            ->join($actionTable . ' AS a', 'a.before_plan_id', '=', 't.id')
            ->where('a.action', ProviderPlanAction::ACTION_CHANGE)
            ->where('a.after_plan_id', $planId)->select('t.*')->get();
        return ProviderPlan::hydrate($result)->first();
    }

	public function getAvailableProducts($profileId=null)
	{
		//TODO - impliment private product for specified profile id
		return MembershipProduct::where('status', MembershipProduct::STATUS_PUBLIC)
			->with(['membership' => function($query) {
				$query->orderBy('rank', 'desc');
			}])->orderBy('billing_cycles')->get();
	}

    public function getFirstActivation($providerId)
    {
        $string = DB::table(ProviderPlan::getTableName())->where('profile_id', 468)->max('activate_date');
        if($string) {
            return new Carbon($string);
        }
    }

    public function getProviderActions($providerId, $hideCanceled = true)
    {
        $query = ProviderPlanAction::where('profile_id', $providerId)
            ->orderBy('request_date', 'desc');
        if($hideCanceled) {
           $query->whereNull('cancel_date');
        }
        return $query->get();
    }


}

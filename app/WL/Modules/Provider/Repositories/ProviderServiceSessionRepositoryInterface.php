<?php namespace WL\Modules\Provider\Repositories;


use Illuminate\Support\Collection;
use WL\Repositories\Repository;

interface ProviderServiceSessionRepositoryInterface extends Repository
{
	/**
	 * Check existence of session
	 * @param $providerServiceId
	 * @param $price
	 * @param $duration
	 * @param $isFirstTime
	 * @return mixed
	 */
	public function exists($providerServiceId, $price, $duration, $isFirstTime);

    /**
     * @param int $profileId
     * @return Collection
     */
    public function getByProviderId($profileId);

    /**
     * @param int $sessionId
     * @return bool
     */
    public function deactivate($sessionId);

    /**
     * @param int $serviceId
     * @return Collection
     */
    public function getServiceSessions($serviceId);
}

<?php namespace WL\Modules\Provider\Repositories;

use Illuminate\Database\Eloquent\Collection;
use WL\Modules\Provider\Models\ProviderService;
use WL\Repositories\Repository;

/**
 * Interface ProviderServiceRepositoryInterface
 * @package WL\Modules\Provider\Repositories
 */
interface ProviderServiceRepositoryInterface extends Repository
{
	/**
	 * Get provider service by id with sessions
	 * @param $id number
	 * @return ProviderService
	 */
	public function getByIdEager($id, $deleted = false);

	/**
	 * Add relation between provider service and location
	 * @param $providerServiceId number
	 * @param $locationId number
	 */
	public function addLocation($providerServiceId, $locationId);

	/**
	 * Remove relation between provider service and location
	 * @param $providerServiceId number
	 * @param $locationId number
	 */
	public function removeLocation($providerServiceId, $locationId);

	/**
	 * Get provider service by profile id
	 * @param $profileId number
	 * @return ProviderService
	 */
	public function getByProfileId($profileId);

	/**
	 * Get provider services by profiles ids
	 * @param $profileIds array
	 * @return mixed
	 */
	public function getByProfileIds($profileIds);

	/**
	 * Remove and set sessions for a provider service
	 * @param $providerServiceId number
	 * @param array<ProviderServiceSession> $sessions
	 * @return mixed
	 */
	public function updateSessions($providerServiceId, array $sessions);

    /**
     * @param int $profileId
     * @return Collection
     */
	public function getWithGiftCertificate($profileId);
}

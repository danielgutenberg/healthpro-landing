<?php namespace WL\Modules\Provider\Repositories;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Models\ProviderProfile;
use WL\Modules\Provider\Models\ProviderService;
use WL\Repositories\DbRepository;

class DbProviderServiceRepository extends DbRepository implements ProviderServiceRepositoryInterface
{
	const PROVIDER_SERVICE_LOCATION_TABLE = 'provider_services_locations';

	public function getById($id)
	{
		return ProviderService::findOrFail($id);
	}

	public function getByIdEager($id, $deleted = false)
	{
		$providerService = ProviderService::with('sessions');

		if ($deleted) {
			$providerService->withTrashed();
		}

		return $providerService->with('locations')
			->findOrFail($id);
	}

	public function addLocation($providerServiceId, $locationId)
	{
		DB::table(static::PROVIDER_SERVICE_LOCATION_TABLE)->insert([
			'provider_service_id' => $providerServiceId,
			'location_id' => $locationId
		]);
	}

	public function removeLocation($providerServiceId, $locationId)
	{
		DB::table(static::PROVIDER_SERVICE_LOCATION_TABLE)
			->where('provider_service_id', '=', $providerServiceId)
			->where('location_id', '=', $locationId)
			->delete();
	}

	public function getByProfileId($profileId)
	{
		return ProviderService::where('profile_id', $profileId)
			->orderBy('sort')
			->with('sessions')
			->with('locations')
			->with('serviceTypes')
			->get();
	}

	public function getByProfileIds($profileIds)
	{
		return ProviderService::whereIn('profile_id', $profileIds)
			->with('sessions')
			->with('locations')
			->with('serviceTypes')
			->get();
	}

	public function updateSessions($providerServiceId, array $sessions)
	{
		$providerService = $this->getById($providerServiceId);

		$providerService->sessions()->delete();
		return $providerService->sessions()->saveMany($sessions);
	}

    public function getWithGiftCertificate($profileId)
    {
        return ProviderService::where('profile_id', $profileId)->where('gift_certificates', 1)->get();
    }


}

<?php namespace WL\Modules\Provider\ValueObjects;


class PlanChange
{
    const UPGRADE = 1;
    const DOWNGRADE = -1;
    const NO_CHANGE = 0;

	const ACTION_CANCEL = 'Cancel Plan';
	const ACTION_REVIVE = 'Re-activate Plan';
	const ACTION_CHANGE = 'Change Plan';

    public $membership;
    public $term;
    public $allowed;

    protected $disallowed = [
        //[Membership, Term]
        [self::UPGRADE, self::DOWNGRADE],
        [self::DOWNGRADE, self::DOWNGRADE],
        [self::NO_CHANGE, self::NO_CHANGE],
    ];

    public function __construct($membership=self::NO_CHANGE, $term=self::NO_CHANGE, $allowed=true)
    {
        $this->membership = $membership;
        $this->term = $term;
        $this->allowed = $allowed;
    }

    public function membershipUpgrade($isUpgrade)
    {
        $this->membership = $isUpgrade ? self::UPGRADE : self::DOWNGRADE;
    }

    public function termUpgrade($isUpgrade)
    {
        $this->term = $isUpgrade ? self::UPGRADE : self::DOWNGRADE;
    }

    public function checkAllowed()
    {
        $this->allowed = true;
        foreach ($this->disallowed as $item) {
            if($this->membership === $item[0] && $this->term === $item[1]) {
                $this->allowed = false;
                break;
            }
        }
    }
}

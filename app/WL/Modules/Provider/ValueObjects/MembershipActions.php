<?php namespace WL\Modules\Provider\ValueObjects;


class MembershipActions
{
	const BUY_NOW = 'BUY NOW';
	const UPGRADE = 'UPGRADE';
	const DOWNGRADE = 'DOWNGRADE';
	const SWITCHTO = 'SWITCH';
}

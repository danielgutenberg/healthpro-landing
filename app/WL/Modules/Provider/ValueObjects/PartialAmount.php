<?php namespace WL\Modules\Provider\ValueObjects;

use InvalidArgumentException;

class PartialAmount
{
	const FLAT_RATE = 'flat_rate';
	const PERCENTAGE = 'percentage';

	private $value;
	private $type;

	public function __construct($value, $type)
	{
		$this->value = $value;
		$this->type = $type;
	}

	public function getAmount($total)
	{
		$this->validate();

		if ($this->type == self::PERCENTAGE) {
			return ($this->value * $total) / 100;
		}

		return min($total, $this->value);
	}

	public function validate()
	{
		if (!is_numeric($this->value)) {
			return;
		}

		if ($this->value < 0) {
			throw new InvalidArgumentException("Value must be > 0");
		}

		if ($this->type == self::PERCENTAGE && $this->value > 100) {
			throw new InvalidArgumentException("Value must be in [0..100]");
		}
	}

	public function getValue()
	{
		return $this->value;
	}

	public function getType()
	{
		return $this->type;
	}

    public static function create($data)
    {
        if(!isset($data['value']) || !isset($data['type']) || !in_array($data['type'], [self::FLAT_RATE, self::PERCENTAGE])) {
            return null;
        }
        return new static($data['value'], $data['type']);
	}
}

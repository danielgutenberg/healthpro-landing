<?php namespace WL\Modules\Provider\ValueObjects;


class ProviderStatus
{
	public $features;
	public $requirements;
	public $name;
	public $priority;

	public function checkRequirements($exists)
	{
		$neededRequirements = array_intersect_key($exists, array_flip($this->requirements));
		return array_sum($neededRequirements) === count($neededRequirements);
	}
}

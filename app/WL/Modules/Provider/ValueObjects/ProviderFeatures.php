<?php namespace WL\Modules\Provider\ValueObjects;


class ProviderFeatures
{
	const GIFT_CERTIFICATE = 'gift_certificates';
	const MONTHLY_PACKAGES = 'monthly_packages';
	const INTRO_SESSION = 'intro_session';
	const MESSAGING = 'messaging';
	const CALENDAR_INTEGRATION = 'calendar_integration';
	const CLIENT_NOTES = 'client_notes';
	const INTAKE_FORMS = 'intake_forms';
	const CLASSES = 'classes';
	const VISIBLE = 'visible';
	const SEARCHABLE = 'searchable';
	const BOOKABLE = 'bookable';
}

<?php namespace WL\Modules\Provider\ValueObjects;


class ProviderRequirements
{
	const ABOUT_ME = 'about_me_filled';
	const ACCEPTED_TERM = 'accepted_terms';
	const ACTIVE = 'is_active';
	const PROVIDER = 'is_provider';
	const EMAIL_VERIFIED = 'email_verified';
	const LOCATIONS = 'locations_services';
	const MEMBERSHIP = 'active_membership';
	const RECIEVE_PAYMENT = 'payment_options_set';
	const HIDDEN = 'is_private';
}

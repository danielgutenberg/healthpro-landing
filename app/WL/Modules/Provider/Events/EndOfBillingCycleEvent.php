<?php namespace WL\Modules\Provider\Events;


use WL\Modules\Provider\Models\BillingCycle;

class EndOfBillingCycleEvent
{
    protected $cycle;

    public function __construct(BillingCycle $cycle)
    {
        $this->cycle = $cycle;
    }

    public function getCycle()
    {
        return $this->cycle;
    }
}
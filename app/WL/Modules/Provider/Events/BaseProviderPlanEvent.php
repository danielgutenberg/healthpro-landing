<?php namespace WL\Modules\Provider\Events;

use WL\Modules\Provider\Models\ProviderPlan;

class BaseProviderPlanEvent
{
	protected $plan;

	public function __construct(ProviderPlan $plan)
	{
		$this->plan = $plan;
	}

	public function getPlan()
	{
		return $this->plan;
	}
}

<?php namespace WL\Modules\Provider\Events;


interface ProviderLocationEventInterface
{
	/**
	 * Returns provider id
	 * @return int
	 */
	public function getProfileId();

	/**
	 * Returns location id
	 * @return int
	 */
	public function getLocationId();

}

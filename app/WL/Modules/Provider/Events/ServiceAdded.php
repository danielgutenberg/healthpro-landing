<?php namespace WL\Modules\Provider\Events;

use WL\Modules\Profile\Events\SearchableEventInterface;

class ServiceAdded implements SearchableEventInterface
{
	private $profileId;
	private $serviceId;

	public function __construct($profileId, $serviceId = null)
	{
		$this->profileId = $profileId;
		$this->serviceId = $serviceId;
	}

	public function getProfileId()
	{
		return $this->profileId;
	}

	public function getServiceId()
	{
		return $this->serviceId;
	}
}

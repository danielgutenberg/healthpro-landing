<?php namespace WL\Modules\Provider\Events;

use WL\Modules\Profile\Events\SearchableEventInterface;

class BaseLocationEvent implements ProviderLocationEventInterface
{
	private $profileId;
	private $locationId;

	public function __construct($profileId, $locationId = null)
	{
		$this->profileId = $profileId;
		$this->locationId = $locationId;
	}

	public function getProfileId()
	{
		return $this->profileId;
	}

	public function getLocationId()
	{
		return $this->locationId;
	}

}

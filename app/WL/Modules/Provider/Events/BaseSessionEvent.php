<?php namespace WL\Modules\Provider\Events;

use WL\Modules\Profile\Events\SearchableEventInterface;

class BaseSessionEvent implements SearchableEventInterface
{
	private $profileId;

	/**
	 * SessionAdded constructor.
	 * @param $profileId
	 */
	public function __construct($profileId)
	{
		$this->profileId = $profileId;
	}

	/**
	 * @return mixed
	 */
	public function getProfileId()
	{
		return $this->profileId;
	}

}

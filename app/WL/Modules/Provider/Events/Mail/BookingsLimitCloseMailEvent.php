<?php namespace WL\Modules\Provider\Events;

use WL\Events\BasicEvents\EmailHealthProToProviderEmailEvent;

class BookingsLimitCloseMailEvent extends EmailHealthProToProviderEmailEvent
{
}

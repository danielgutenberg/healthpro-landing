<?php namespace WL\Modules\Provider\Events;

use WL\Events\BasicEvents\EmailHealthProToAdminEmailEvent;

class PlanCancelledAdminMailEvent extends EmailHealthProToAdminEmailEvent
{
    public function __construct($bladeData)
    {
        $bodyBladeName = 'emails.membership.plan-cancelled-admin-body';
        $subjectBladeName = 'emails.membership.plan-cancelled-admin-subject';
        parent::__construct($bodyBladeName, $subjectBladeName, $bladeData);
    }
}

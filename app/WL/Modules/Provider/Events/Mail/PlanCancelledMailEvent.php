<?php namespace WL\Modules\Provider\Events;

use WL\Events\BasicEvents\EmailHealthProToProviderEmailEvent;

class PlanCancelledMailEvent extends EmailHealthProToProviderEmailEvent
{
}

<?php namespace WL\Modules\Provider\Events;

use WL\Events\BasicEvents\EmailHealthProToProviderEmailEvent;

class BookingsLimitReachedMailEvent extends EmailHealthProToProviderEmailEvent
{
}

<?php namespace WL\Modules\Provider\Events;

use WL\Events\BasicEvents\EmailHealthProToAdminEmailEvent;

class BookingsLimitAdminMailEvent extends EmailHealthProToAdminEmailEvent
{
    public function __construct($bladeData)
    {
        $bodyBladeName = 'emails.membership.booking-limit-admin-body';
        $subjectBladeName = 'emails.membership.booking-limit-admin-subject';
        parent::__construct($bodyBladeName, $subjectBladeName, $bladeData);
    }
}

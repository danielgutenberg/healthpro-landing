<?php namespace WL\Modules\Provider\Events;


class AcceptPaymentMethodUpdateEvent
{
    protected $profileId;
    protected $method;

    public function __construct($profileId, $method)
    {
        $this->profileId = $profileId;
        $this->method = $method;
    }

    /**
     * @return mixed
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }


}
<?php namespace WL\Modules\Provider\Helpers;

use Illuminate\Support\Collection;
use WL\Modules\PricePackage\Models\PricePackageRule;

class Html
{
	public static function showService($service, $editMode = false, $view = 'site.profile.helper.service-render')
	{
		$hasPackages = false;
		foreach ($service->sessions as $session) {
			if (count($session->packages)) {
				$hasPackages = true;
			}
		}

		$serviceType = $service->serviceTypes->first();
		if ($serviceType) {
			if ($serviceType->slug == 'introductory-session') {
				$primaryServiceType = $service->sessions->first()->title;
			} else {
				$primaryServiceType = $serviceType->name;
			}
		} else {
			$primaryServiceType = $service->name;
		}
		$secondaryServiceTypes = $service->serviceTypes->splice(1)->pluck('name')->implode(', ');

		return view($view, compact('service', 'primaryServiceType', 'secondaryServiceTypes', 'hasPackages', 'editMode'));
	}

	public static function showServiceLocations($service, $view = 'site.profile.helper.service-locations-render')
	{
		if (!count($service->locations)) {
			return '';
		}

		return view($view, [
			'locations' => $service->locations
		]);
	}

	public static function showPackagesLabel($service, $view = 'site.profile.helper.service-packages-label')
	{
		if (!count($service->sessions)) {
			return '';
		}

		$hasPackages = false;

		foreach ($service->sessions as $session) {
			if (count($session->packages)) {
				$hasPackages = true;
			}
		}

		return view($view, compact('hasPackages'));
	}

	public static function showFromPrice($service, $displayTime = false, $view = 'site.profile.helper.service-from-price')
	{
		$fromPrice = $service->sessions->filter(function($session) {
			return $session->price > 0;
		})->min('price');

		if (!$fromPrice) {
			return '';
		}

		$session = $service->sessions->where('price', $fromPrice)->first();

		if ($displayTime) {
			$displayTime = [
				'm' => $session->duration % 60,
				'h' => $session->duration / 60 % 60,
			];
		}

		setlocale(LC_MONETARY, 'en_US');
		return view($view, compact('fromPrice', 'displayTime', 'session'));
	}

	public static function showFromPriceMonthly($service, $view = 'site.profile.helper.service-from-price-monthly')
	{
		$priceCollection = new Collection();
		foreach ($service->sessions as $serviceSession) {
			foreach ($serviceSession->packages as $serviceSessionPackage) {
				if ($serviceSessionPackage->number_of_entities == -1) {
					$priceCollection->push([
						'session' => $serviceSession,
						'price' => $serviceSessionPackage->price,
					]);
				}
			}
		}

		$item = $priceCollection->sortBy('price')->first();

		$fromPrice = $item === null ? null : $item['price'];
		$session = $item === null ? null : $item['session'];

		setlocale(LC_MONETARY, 'en_US');
		return view($view, compact('fromPrice', 'session'));
	}

	public static function showServiceContent($service, $view = 'site.profile.helper.service-content')
	{
		$packages = [];

		$reccuringPackages = [];

		$sessions = [];

		foreach ($service->sessions as $session) {

			$m = $session->duration % 60;
			$h = $session->duration / 60 % 60;
			$duration = '';
			if($h) {
				$duration .= $h . ' hr ' . ($h > 1 ? 's' : '');
			}
			if ($m) {
				$duration .= $m . ' min';
			}

			/*
			 * don't show sessions that are only used for monthly packages if they dont have a single session price
			 */
			if ($session->active) {
				$sessions[] = [
					'price' => money_format('%n', $session->price),
					'duration' => $duration,
				];
			}

			foreach ($session->packages as $package) {

				if ($session->price && $package->number_of_entities > 0) {
					$sale = 100 - intval($package->price / ($session->price * $package->number_of_entities) * 100);

					$packages[] = [
						'price' => money_format('%n', $package->price),
						'duration' => $duration,
						'sessions' => $package->number_of_entities,
						'sale' => $sale . '%',
					];

				} else {
					$rule = $package->rules->where('rule', PricePackageRule::MAX_WEEKLY)->first();
					if ($rule) {
						$reccuringPackages[] = [
							'price' => money_format('%n', $package->price) . ' / month',
							'duration' => $duration,
							'frequency' => ($rule->value ? $rule->value : 'unlimited') . ' time' . ($rule->value == 1 ? '' : 's') . ' per week',
							'months' => $package->number_of_months . ' month' . ($package->number_of_months > 1 ? 's' : ''),
						];
					}

				}
			}
		}

		return view($view, compact('packages', 'reccuringPackages', 'sessions', 'service'));
	}
}

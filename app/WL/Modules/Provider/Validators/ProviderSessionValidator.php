<?php namespace WL\Modules\Provider\Validators;

use Illuminate\Validation\Validator;

class ProviderSessionValidator extends Validator
{
	public function validateProviderSessionDuplication($attribute, $value, $parameters)
	{
		$collection = collect($value);

		$firstTimeItems = $collection->filter(function ($value) {
			return ($value['is_first_time'] == true);
		});

		if (($firstTimeItems->count() > 1)) {
			$this->errors()->add($attribute, __('Duplicated is_first_time'));
		}

		return true;
	}
}

<?php namespace WL\Modules\Provider\Models;


use WL\Models\ModelBase;

class MembershipProduct extends ModelBase
{
	const INTERVAL_1MONTH = '1M';
	const INTERVAL_3MONTH = '3M';
	const INTERVAL_1WEEK = '1W';

	const STATUS_PUBLIC = 'public';
	const STATUS_PRIVATE = 'private';
	const STATUS_INACTIVE = 'inactive';

	protected $table = 'membership_products';

	protected $rules = [
		'name' => 'required',
		'membership_id' => 'integer|required',
		'interval' => 'required',
	];

    public function getPricePerCycleAttribute()
    {
        return $this->price / $this->billing_cycles;
	}

    public function membership()
    {
        return $this->belongsTo(Membership::class, 'membership_id');
	}
}

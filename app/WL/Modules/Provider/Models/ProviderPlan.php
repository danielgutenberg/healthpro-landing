<?php namespace WL\Modules\Provider\Models;


use WL\Models\ModelBase;
use WL\Modules\Payment\Models\PaymentPaypalSubscription;

class ProviderPlan extends ModelBase
{
    const STATUS_ACTIVE = 'active';
    const STATUS_UPGRADED = 'upgraded';
    const STATUS_DELETED = 'deleted';
    const STATUS_PENDING = 'pending';
    const STATUS_ENDED = 'ended';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_RENEWED = 'renewed';

    protected $table = 'provider_plans';

    protected $rules = [
        'profile_id' => 'required',
        'product_id' => 'required',
        'status' => 'required',
    ];

    protected $dates = ['activate_date', 'terminate_date'];

    protected $casts = [
        'auto_renew' => 'boolean',
    ];

	public function previous()
	{
		return $this->belongsTo(self::class, 'previous_plan_id');
    }

    public function product()
    {
		return $this->belongsTo(MembershipProduct::class, 'product_id');
    }

    public function nextProduct()
    {
		return $this->belongsTo(MembershipProduct::class, 'renewal_product_id');
    }

    public function cycles()
    {
		return $this->hasMany(BillingCycle::class, 'plan_id');
    }

	public function subscription()
	{
		return $this->hasOne(PaymentPaypalSubscription::class, 'subscription_id');
	}
}

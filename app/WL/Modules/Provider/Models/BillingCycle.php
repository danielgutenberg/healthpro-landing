<?php namespace WL\Modules\Provider\Models;


use WL\Models\ModelBase;

class BillingCycle extends ModelBase
{
    const STATUS_PENDING = 'pending';
    const STATUS_ACTIVE = 'active';
    const STATUS_ENDED = 'ended';

	protected $table = 'billing_cycles';

    protected $dates = ['start_date', 'end_date'];

    protected $rules = [
        'plan_id' => 'required',
    ];

    public function plan()
    {
        return $this->belongsTo(ProviderPlan::class, 'plan_id');
    }

	public function getExtraBookingTotal()
	{
		return $this->extra_bookings * $this->extra_booking_price;
    }

	public function actions()
	{
		return $this->hasMany(ProviderPlanAction::class, 'billing_cycle_id');
    }
}

<?php namespace WL\Modules\Provider\Models;

use WL\Models\ModelBase;

class ProviderPlanAction extends ModelBase
{
    const ACTION_CHANGE = 'change';
    const ACTION_CANCEL = 'cancellation';
    const ACTION_SWITCH = 'switch';
    const ACTION_RENEWAL = 'renewal';

    protected $table = 'provider_plan_actions';

    protected $rules = [
        'profile_id' => 'required',
        'before_plan_id' => 'required',
        'billing_cycle_id' => 'required',
    ];

    protected $dates = ['request_date', 'effective_date', 'action_date'];

    protected $casts = [
    	'billable' => 'boolean',
	];

	public function cycle()
	{
		return $this->belongsTo(BillingCycle::class, 'billing_cycle_id');
    }

    public function upgradePlan()
    {
        return $this->belongsTo(ProviderPlan::class, 'after_plan_id');
    }

    public function originalPlan()
    {
        return $this->belongsTo(ProviderPlan::class, 'before_plan_id');
    }

	public function product()
	{
        return $this->belongsTo(MembershipProduct::class, 'product_id');
    }
}

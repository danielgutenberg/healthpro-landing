<?php namespace WL\Modules\Provider\Models;


use WL\Models\ModelBase;

class Membership extends ModelBase
{
	protected $table = 'memberships';

	protected $casts = [
		'features' => 'array',
	];

}

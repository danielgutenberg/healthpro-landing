<?php namespace WL\Modules\Provider\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use WL\Models\ModelBase;
use WL\Modules\Location\Models\Location;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Taxonomy\Models\Tag;

class ProviderService extends ModelBase
{
	use SoftDeletes;

	const INTRODUCTORY_SESSION_SLUG = 'introductory-session';

	protected $table = 'provider_services';

	protected $throwValidationExceptions = true;

	protected $rules = [
		'profile_id' => 'required',
		'name' => 'required',
	];

	public function serviceTypes()
	{
		return $this->morphToMany(Tag::class, 'taggable', 'tag_relations');
	}

	public function profile()
    {
        return $this->belongsTo(ModelProfile::class);
    }

	public function locations()
	{
		return $this->belongsToMany(Location::class, 'provider_services_locations', 'provider_service_id', 'location_id');
	}

	public function sessions()
	{
		return $this->hasMany(ProviderServiceSession::class, 'provider_service_id', 'id');
	}

	public function requiresIntroductorySession()
	{
		return $this->sessions->contains(function($key, $session) {
			return $session->is_first_time == true;
		});
	}
}

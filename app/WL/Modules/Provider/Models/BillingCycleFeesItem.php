<?php namespace WL\Modules\Provider\Models;


use WL\Models\ModelBase;

class BillingCycleFeesItem extends ModelBase
{
	protected $table = 'billing_cycle_fees_items';

    public function entity()
    {
        return $this->morphTo();
    }
}

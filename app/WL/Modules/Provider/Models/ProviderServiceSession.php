<?php namespace WL\Modules\Provider\Models;


use WL\Models\ModelBase;
use WL\Modules\PricePackage\Models\PricePackage;

class ProviderServiceSession extends ModelBase
{
	protected $table = 'provider_service_sessions';

	protected $throwValidationExceptions = true;

	const PACKAGE_ENTITY_TYPE = 'service_session';

	protected $rules = [
		'provider_service_id' => 'required',
		'price' => 'required',
		'duration' => 'required'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function service()
	{
		return $this->belongsTo(ProviderService::class, 'provider_service_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\MorphMany
	 */
	public function packages()
	{
		return $this->morphMany(PricePackage::class, 'entity');
	}
}

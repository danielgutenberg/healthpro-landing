<?php namespace WL\Modules\Provider\Exceptions;


use App\Exceptions\GenericException;

class ProviderException extends GenericException {};
class MembershipException extends ProviderException {};
class ProviderAccessException extends ProviderException {};

class ProviderSelfReviewNotAllowedException extends ProviderException {};
class FeatureNotEnabledException extends ProviderException {};
class ProviderConfigurationNotAllowed extends ProviderException {};
class ProviderServiceOnlySessionException extends ProviderException {};

class PlanChangeNotAllowed extends MembershipException {};
class InvalidNumberOfBillingCycles extends MembershipException {};
class NoActivePlanFound extends MembershipException {};

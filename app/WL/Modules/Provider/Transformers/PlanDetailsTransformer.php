<?php namespace WL\Modules\Provider\Transformers;

use WL\Transformers\Transformer;

class PlanDetailsTransformer extends Transformer
{
	public function transform($item)
	{
		return [
			'plan' => (new ProviderPlanTransformer())->transform($item['plan']),
			'bookings' => (array) $item['bookings'],
            'pending_action' => $item['pending_action'] ? $item['pending_action']->action : null,
            'available_actions' => $item['actions'],
            'available_product_changes' => (new ProductSelectionTransformers())->transformCollection($item['products']),
            'next_billing_estimate' => $item['estimate'],
		];
	}
}

<?php namespace WL\Modules\Provider\Transformers;


use WL\Transformers\Transformer;

class ProductSelectionTransformers extends Transformer
{
    public function transform($item)
    {
        return [
            'membership' => (new MembershipTransformer())->transform($item['type']),
            'action' => $item['action'],
            'products' => $item['products']->map(function($productItem) {
                return $this->transformProductOption($productItem);
            }),
        ];
    }

    private function transformProductOption($item)
    {
        $product = (new MembershipProductTransformer())->transform($item['product']);
        $product['allowed'] = $item['change'] ? $item['change']->allowed : true;
        return $product;
    }
}

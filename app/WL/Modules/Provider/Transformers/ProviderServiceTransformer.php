<?php namespace WL\Modules\Provider\Transformers;

use WL\Modules\Location\Transformer\LocationTransformer;
use WL\Modules\PricePackage\Facade\PricePackageService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Schedule\Facades\ScheduleService;
use WL\Schedule\ScheduleAvailability;
use WL\Transformers\Transformer;
use Illuminate\Support\Collection;

class ProviderServiceTransformer extends Transformer
{
	private $names;

	public function transformCollection($items)
	{
		if (!is_null($items)) {
			if (!is_array($items)) {
				if ($items instanceof Collection) {
					$items = $items->all();
				} else
					throw new \Exception('not array given');
			}
			$this->names = array_column($items, 'name');

			return array_map([$this, 'transform'], $items);
		}

		return [];
	}

	public function transform($item, $withDeleted = false)
	{
		$data = [
			'service_id' => $item->id,
			'sessions' => $this->transformSession($item->sessions, $withDeleted),
			'name' => $item->name,
			'locations' => $this->transformLocations($item->locations,$item->profile_id),
			'deposit' => $item->deposit,
			'deposit_type' => $item->deposit_type,
			'description' => $item->description,
			'certificates' => $item->gift_certificates,
			'sub_services' => (new ProviderServiceTypeTransformer())->transformCollection($item->serviceTypes),
		];

		if ($this->names) {
			$serviceNameCount = array_count_values($this->names);
			$data['display_name'] = $serviceNameCount[$item->name] > 1 ? $item->name . ' - ' . $item->locations->first()->name : $item->name;
		}

		return $data;
	}

	public function transformSession($items, $withDeleted = false)
	{

		return $items->map(function($item) use ($withDeleted) {
			if ($withDeleted) {
				$packages = $item->packages()->withTrashed()->get();
			} else {
				$packages = $item->packages;
			}

			$data = [
				'session_id'    => $item->id,
				'active'        => $item->active,
				'is_first_time' => $item->is_first_time,
          		'duration'      => $item->duration,
          		'price'         => number_format($item->price, 2),
				'packages'		=> $this->transformPackages($packages),
				'title'			=> $item->title,
			];

			$profile = ProfileService::getCurrentProfile();
			if ($profile && $profile->type == ModelProfile::CLIENT_PROFILE_TYPE) {
				$purchased = PricePackageService::getChargedPackage($profile->id, $item->id, 'service_session');
				if ($purchased && $purchased->entities_left > 0) {
					$data['entities_left'] = $purchased->entities_left;
				}
			}
			return $data;
		})->filter(function($item) {
			return count($item['packages']) > 0 || $item['active'] == 1;
		})->values();
	}

	public function transformLocations($locations,$providerId)
	{
		foreach ($locations as $location) {
			$location->availabilities = ScheduleService::getProviderAvailability($providerId, $location->id, ScheduleAvailability::TYPE_APPOINTMENT);
		}
		return (new LocationTransformer())->transformCollection($locations);
	}

	public function transformPackages($packages)
	{
		return $packages->map(function($package){
			return [
				'package_id'       => $package->id,
				'number_of_visits' => $package->number_of_entities,
				'number_of_months' => $package->number_of_months,
				'price'            => number_format($package->price, 2),
				'rules'            => $package->rules,
			];
		});
	}

	public function transformIndividualSession($item)
	{
		return [
			'session_id'    => $item->id,
			'active'        => $item->active,
			'duration'      => $item->duration,
			'price'         => number_format($item->price, 2),
		];
	}
}

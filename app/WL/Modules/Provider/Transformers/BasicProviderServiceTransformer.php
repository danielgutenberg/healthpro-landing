<?php namespace WL\Modules\Provider\Transformers;

use WL\Modules\Location\Transformer\LocationTransformer;
use WL\Schedule\Facades\ScheduleService;
use WL\Schedule\ScheduleAvailability;
use WL\Transformers\Transformer;

class BasicProviderServiceTransformer extends Transformer
{
	public function transform($item)
	{
		if ($item->serviceTypes[0]->slug == 'introductory-session') {
			$name = $item->sessions->first()->title;
			$detailedName = $name;
		} else {
			$name = $item->serviceTypes[0]->name;
			$detailedName = $item->serviceTypes->pluck('name')->implode(', ');
		}

		return [
			'service_id' => $item->id,
			'service_type_id' => $item->serviceTypes[0]->id,
			'slug' => $item->serviceTypes[0]->slug,
			'name' => $name,
			'detailed_name' => $detailedName,
			'description' => $item->description
		];
	}
}

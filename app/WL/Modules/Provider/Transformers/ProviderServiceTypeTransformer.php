<?php namespace WL\Modules\Provider\Transformers;

use WL\Modules\Taxonomy\TagTransformer;
use WL\Transformers\Transformer;

class ProviderServiceTypeTransformer extends Transformer
{

	public function transform($item)
	{
		$return = array_merge(
			(new TagTransformer())->transform($item),
			(new ProviderServiceTypeParentTransformer())->transform($item)
		);

		return $return;
	}
}

<?php namespace WL\Modules\Provider\Transformers;


use WL\Modules\Provider\Models\MembershipProduct;
use WL\Transformers\Transformer;

class MembershipProductTransformer extends Transformer
{
	public function transform($item)
	{
	    if(!$item instanceof MembershipProduct) {
	        return null;
        }

		return [
		    'id' => $item->id,
		    'name' => $item->name,
		    'price' => $item->price,
		    'billing_cycles' => $item->billing_cycles,
			'membership' => (new MembershipTransformer())->transform($item->membership),
		];
	}

}

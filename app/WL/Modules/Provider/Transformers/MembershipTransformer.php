<?php namespace WL\Modules\Provider\Transformers;


use WL\Transformers\Transformer;

class MembershipTransformer extends Transformer
{
	public function transform($item)
	{
		return [
			'id' => $item->id,
			'name' => $item->name,
			'description' => $item->description,
			'slug' => $item->slug,
			'features' => $item->features,
			'bookings_limit' => $item->bookings_limit,
			'over_limit_fee' => $item->over_limit_fee,
			'rank' => $item->rank,
		];
	}

}

<?php namespace WL\Modules\Provider\Transformers;


use WL\Modules\Provider\Models\ProviderPlanAction;
use WL\Transformers\Transformer;

class ProviderPlanTransformer extends Transformer
{
    public function transform($item)
    {
        $billing_cycles = [
            'total' => $item->billing_cycles,
            'used' => $item->cycles->count(),
        ];
        $billing_cycles['left'] = $billing_cycles['total'] - $billing_cycles['used'];
        if(is_null($item->renewal_product_id)) {
			$expiration_action = ProviderPlanAction::ACTION_CANCEL;
		} else {
			$expiration_action = $item->renewal_product_id == $item->product_id ? ProviderPlanAction::ACTION_RENEWAL : ProviderPlanAction::ACTION_CHANGE;
		}

        return [
            'id' => $item->id,
            'status' => $item->status,
            'allow_over_booking' => $item->allow_over_booking,
            'activate_date' => $item->activate_date,
            'terminate_date' => $item->terminate_date,
            'billing_cycles' => $billing_cycles,
			'end_date' => $item->end_date,
			'expiration_action' => $expiration_action,
            'product' => (new MembershipProductTransformer())->transform($item->product),
            'next_product' => (new MembershipProductTransformer())->transform($item->nextProduct),
        ];
    }

}

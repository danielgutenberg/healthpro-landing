<?php namespace WL\Modules\Provider\Transformers;


use WL\Transformers\Transformer;

class ProviderServiceTypeParentTransformer extends Transformer
{
	public function transform($item)
	{
		return [
			'tag_id' => $item->id,
			'name' => $item->name,
			'parent_name' =>  ($item->parent_id) ? $item->parent->name : null,
			'parent_slug' => ($item->parent_id) ? $item->parent->slug : null,
		];
	}
}

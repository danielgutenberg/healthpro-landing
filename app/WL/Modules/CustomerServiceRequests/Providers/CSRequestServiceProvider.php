<?php namespace WL\Modules\CustomerServiceRequests\Providers;

use Illuminate\Support\ServiceProvider;
use WL\Modules\CustomerServiceRequests\Services\CSRequestService;
use WL\Modules\CustomerServiceRequests\Services\CSRequestServiceInterface;

class CSRequestServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton(CSRequestServiceInterface::class, function () {
			return new CSRequestService();
		});
	}
}

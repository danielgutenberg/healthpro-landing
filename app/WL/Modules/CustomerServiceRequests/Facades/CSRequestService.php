<?php namespace WL\Modules\CustomerServiceRequests\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\CustomerServiceRequests\Services\CSRequestServiceInterface;

class CSRequestService extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return CSRequestServiceInterface::class;
	}
}

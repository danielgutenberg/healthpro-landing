<?php namespace WL\Modules\CustomerServiceRequests\Events;

use WL\Events\BasicEvents\BasicEventInterface;
use WL\Modules\Appointment\Models\ProviderAppointment;

class CustomerServiceRequestEvent implements BasicEventInterface
{
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function getEventName()
	{
		return 'customer-service-request-events';
	}
}

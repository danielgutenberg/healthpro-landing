<?php namespace WL\Modules\CustomerServiceRequests\Events;

use WL\Events\BasicEvents\BasicEventListener;
use Illuminate\Support\Facades\Mail;
use WL\Modules\SalesForce\Objects\Contact;
use WL\Settings\Facades\Setting;

class CustomerServiceRequestEventListener extends BasicEventListener
{

	public function AddServiceRequest(AddServiceRequest $event)
	{
		$data = $event->data;

		$this->sendEmailToCustomerSupport('emails.cs-requests.add-service-request', $data, 'New Service Request');
	}

	public function DemoRequest(DemoRequest $event)
	{
		$data = $event->data;

		$this->sendEmailToCustomerSupport('emails.cs-requests.demo-request', $data, 'New Demo Requested');
	}

	public function PartnerRequest(PartnerRequest $event)
	{
		$data = $event->data;

		$this->sendEmailToCustomerSupport('emails.cs-requests.partner-request', $data, 'New Partner Requested');
	}

	public function AffiliateRequest(AffiliateRequest $event)
	{
		$data = $event->data;

		$this->sendEmailToCustomerSupport('emails.cs-requests.affiliate-request', $data, 'New Affiliate Requested');
	}

	public function ContactRequest(ContactRequest $event)
	{
		$data = $event->data;

		$this->sendEmailToCustomerSupport('emails.cs-requests.contact-request', $data, 'Someone Tried to Contact Support');
	}

	public function NewsletterRequest(NewsletterRequest $event)
	{
		$data = $event->data;

		$this->sendEmailToCustomerSupport('emails.cs-requests.newsletter-request', $data, 'Request to Be Added to Newsletter');
	}

	public function SubmitAQuestion(SubmitAQuestion $event)
	{
		$data = $event->data;

		$this->sendEmailToCustomerSupport('emails.cs-requests.submit-a-question', $data, 'Someone Submitted a Question to Customer Service');
	}

	private function sendEmailToCustomerSupport($blade, $data, $subject)
	{
		Mail::send($blade, $data, function($message) use ($subject) {
			$message->from(trim(Setting::get('default_from_email')), trim(Setting::get('default_from_name')))
				->to('support@healthpro.com', 'Healthpro Support')
				->cc('jonathan@healthpro.com', 'Jonathan Richey')
				->subject($subject);
		});
	}


}

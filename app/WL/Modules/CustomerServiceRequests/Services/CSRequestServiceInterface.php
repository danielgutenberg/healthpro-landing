<?php namespace WL\Modules\CustomerServiceRequests\Services;

/**
 * Interface CSRequestInterface
 * @package WL\Modules\CustomerServiceRequests\Services
 */
interface CSRequestServiceInterface
{
	public function addServiceRequest($email, $service);

	public function demoRequest($data);

	public function partnerRequest($data);

	public function affiliateRequest($data);

	public function contactRequest($data);

	public function newsletterRequest($data);

	public function submitAQuestion($data);
}

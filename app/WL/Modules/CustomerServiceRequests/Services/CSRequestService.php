<?php namespace WL\Modules\CustomerServiceRequests\Services;

use WL\Events\Facades\EventHelper;
use WL\Modules\CustomerServiceRequests\Events\AddServiceRequest;
use WL\Modules\CustomerServiceRequests\Events\AffiliateRequest;
use WL\Modules\CustomerServiceRequests\Events\ContactRequest;
use WL\Modules\CustomerServiceRequests\Events\DemoRequest;
use WL\Modules\CustomerServiceRequests\Events\NewsletterRequest;
use WL\Modules\CustomerServiceRequests\Events\PartnerRequest;
use WL\Modules\CustomerServiceRequests\Events\SubmitAQuestion;

class CSRequestService implements CSRequestServiceInterface
{
	public function addServiceRequest($email, $service)
	{
		EventHelper::fire(new AddServiceRequest(compact('email', 'service')));
	}

	public function demoRequest($data)
	{
		EventHelper::fire(new DemoRequest($data));
	}

	public function partnerRequest($data)
	{
		$data['interested_in'] = implode(',', $data['interested_in']);
		EventHelper::fire(new PartnerRequest($data));
	}

	public function affiliateRequest($data)
	{
		EventHelper::fire(new AffiliateRequest($data));
	}

	public function contactRequest($data)
	{
		EventHelper::fire(new ContactRequest($data));
	}

	public function newsletterRequest($data)
	{
		EventHelper::fire(new NewsletterRequest($data));
	}

	public function submitAQuestion($data)
	{
		EventHelper::fire(new SubmitAQuestion($data));
	}
}

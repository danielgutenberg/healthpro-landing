<?php namespace WL\Modules\Appointment\Commands;

use WL\Modules\CustomerServiceRequests\Facades\CSRequestService;
use WL\Modules\Profile\Commands\ValidatableCommand;

class SubmitAQuestionCommand extends ValidatableCommand
{
	const IDENTIFIER = 'cs.submitAQuestion';

	protected $rules = [
		'question' => 'required',
		'email' => 'required|email'
	];

	public function validHandle()
	{
		return CSRequestService::submitAQuestion($this->inputData);
	}
}

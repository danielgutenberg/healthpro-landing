<?php namespace WL\Modules\Appointment\Commands;

use WL\Modules\CustomerServiceRequests\Facades\CSRequestService;
use WL\Modules\Profile\Commands\ValidatableCommand;

class AffiliateRequestCommand extends ValidatableCommand
{
	const IDENTIFIER = 'cs.affiliateRequest';

	protected $rules = [
		'first_name' => 'required',
		'last_name' => 'required',
		'company_name' => 'required',
		'website' => 'required',
		'email' => 'required|email',
		'phone' => 'required',
		'title' => 'required',
		'comment' => 'required'
	];

	public function validHandle()
	{
		return CSRequestService::affiliateRequest($this->inputData);
	}
}

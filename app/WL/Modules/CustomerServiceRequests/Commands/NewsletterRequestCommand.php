<?php namespace WL\Modules\Appointment\Commands;

use WL\Modules\CustomerServiceRequests\Facades\CSRequestService;
use WL\Modules\Profile\Commands\ValidatableCommand;

class NewsletterRequestCommand extends ValidatableCommand
{
	const IDENTIFIER = 'cs.newsletterRequest';

	protected $rules = [
		'email' => 'required|email'
	];

	public function validHandle()
	{
		return CSRequestService::newsletterRequest($this->inputData);
	}
}

<?php namespace WL\Modules\Appointment\Commands;

use WL\Modules\CustomerServiceRequests\Facades\CSRequestService;
use WL\Modules\Profile\Commands\ValidatableCommand;

class PartnerRequestCommand extends ValidatableCommand
{
	const IDENTIFIER = 'cs.partnerRequest';

	protected $rules = [
		'first_name' => 'required',
		'last_name' => 'required',
		'company_name' => 'required',
		'website' => 'required',
		'email' => 'required|email',
		'phone' => 'required',
		'number_of_members' => 'required',
		'country' => 'required',
		'comment' => 'required',
		'interested_in' => 'required'
	];

	public function validHandle()
	{
		return CSRequestService::partnerRequest($this->inputData);
	}
}

<?php namespace WL\Modules\Appointment\Commands;

use WL\Modules\CustomerServiceRequests\Facades\CSRequestService;
use WL\Modules\Profile\Commands\ValidatableCommand;

class ContactRequestCommand extends ValidatableCommand
{
	const IDENTIFIER = 'cs.contactRequest';

	protected $rules = [
		'first_name' => 'required',
		'last_name' => 'required',
		'email' => 'required|email',
		'phone' => 'required',
		'comment' => 'required'
	];

	public function validHandle()
	{
		return CSRequestService::contactRequest($this->inputData);
	}
}

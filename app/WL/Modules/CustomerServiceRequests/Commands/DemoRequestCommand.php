<?php namespace WL\Modules\Appointment\Commands;

use WL\Modules\CustomerServiceRequests\Facades\CSRequestService;
use WL\Modules\Profile\Commands\ValidatableCommand;

class DemoRequestCommand extends ValidatableCommand
{
	const IDENTIFIER = 'cs.requestDemo';

	protected $rules = [
		'first_name' => 'required',
		'last_name' => 'required',
		'phone' => 'required',
		'email' => 'required|email'
	];

	public function validHandle()
	{
		return CSRequestService::demoRequest($this->inputData);
	}
}

<?php namespace WL\Modules\Appointment\Commands;

use WL\Modules\CustomerServiceRequests\Facades\CSRequestService;
use WL\Modules\Profile\Commands\ValidatableCommand;

class AddServiceRequestCommand extends ValidatableCommand
{
	const IDENTIFIER = 'cs.requestService';

	protected $rules = [
		'service' => 'required',
		'email' => 'required|email'
	];

	protected $messages = [
		'service.required' => 'Please include the name of the new service'
	];

	public function validHandle()
	{
		$email = $this->inputData['email'];
		$service = $this->get('service');

		return CSRequestService::addServiceRequest($email, $service);
	}
}

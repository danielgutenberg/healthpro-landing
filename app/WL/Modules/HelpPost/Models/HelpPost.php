<?php namespace WL\Modules\HelpPost\Models;

use WL\Models\ModelBase;
use Sitemap;
use \WL\Modules\Taxonomy\Facades\TagService;
use WL\Sortable\SortableTrait;


class HelpPost extends ModelBase
{

	use SortableTrait;

	const SECTION_SLUG = 'help-section';
	const CATEGORY_SLUG = 'help-category';

	protected $fillable = ['subject', 'content'];

	protected $rules = [
		'subject' => 'required',
		'content' => 'required',
	];

	/**
	 * Include self to sitemap index
	 * @return none
	 */
	public function handleSitemapIndex()
	{
		Sitemap::addSitemap(route('sitemap', ['helps']), self::max('updated_at'));
	}

	/**
	 * Generate sitemap page
	 * @return none
	 */
	public function handleSitemapPage()
	{
		foreach (self::all() as $help) {
			$section = TagService::getEntityTagsForTaxonomySlug($help,static::SECTION_SLUG);
			$category = TagService::getEntityTagsForTaxonomySlug($help,static::CATEGORY_SLUG);
			Sitemap::addTag(route('help', [$section[0]->slug, $category[0]->slug]), $help->updated_at);
		}
	}
}

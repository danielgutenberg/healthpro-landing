<?php namespace WL\HelpPost\Commands;

use WL\Modules\HelpPost\Facades\HelpPostService;
use WL\Security\Commands\AuthorizableCommand;

/**
 * Class LoadHelpPostsCommand
 *
 * Load combination of section / category
 * @package WL\HelpPost\Commands
 */
class LoadHelpPostsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'help.loadHelpPosts';
	private $sectionTagSlug;
	private $categoryTagSlug;

	function __construct($sectionTagSlug, $categoryTagSlug)
	{
		$this->sectionTagSlug = $sectionTagSlug;
		$this->categoryTagSlug = $categoryTagSlug;
	}

	public function handle()
	{
		// Getting sections and categories
		$sections = HelpPostService::getSections();
		$categories = HelpPostService::getCategories();

		// Get first section if not passed
		if (empty($this->sectionTagSlug)) {
			$this->sectionTagSlug = $sections->first()->slug;
		}

		$categoriesToKeep = HelpPostService::getUsedCategoriesOfSection($this->sectionTagSlug)->pluck('slug')->all();

		$categories = $categories->reject(function ($item) use ($categoriesToKeep) {
			return !in_array($item->slug, $categoriesToKeep);
		});

		// Get first category if it was not passed in the request
		if (!$categories->isEmpty() && (empty($this->categoryTagSlug) || !in_array($this->categoryTagSlug, $categoriesToKeep))) {
			$this->categoryTagSlug = $categories->first()->slug;
		}

		$namesBySlug = $categories->pluck('name', 'slug')->all();
		$categoryName = $namesBySlug && isset($namesBySlug[$this->categoryTagSlug]) ? $namesBySlug[$this->categoryTagSlug] : '';

		//Loading posts
		$posts = HelpPostService::getHelpPosts($this->sectionTagSlug, $this->categoryTagSlug);

		return [$sections, $categories, $this->sectionTagSlug, $this->categoryTagSlug, $categoryName, $posts];
	}

}

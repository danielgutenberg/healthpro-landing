<?php namespace WL\Modules\HelpPost\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\HelpPost\Services\HelpPostServiceInterface;

class HelpPostService extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return HelpPostServiceInterface::class;
	}
}

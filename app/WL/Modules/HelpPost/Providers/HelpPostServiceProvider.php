<?php namespace WL\Modules\HelpPost\Providers;

use Illuminate\Support\ServiceProvider;
use WL\Modules\HelpPost\Services\HelpPostServiceInterface;
use WL\Modules\HelpPost\Services\HelpPostServiceImpl;

class HelpPostServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton(HelpPostServiceInterface::class, function () {
			return new HelpPostServiceImpl();
		});

	}
}

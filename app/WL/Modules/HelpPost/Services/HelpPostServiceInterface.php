<?php namespace WL\Modules\HelpPost\Services;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface HelpPostServiceInterface
 * @package WL\HelpPost\Service
 */
interface HelpPostServiceInterface
{

	/**
	 * Get Help Post sections
	 * @return mixed
	 */
	public function getSections();

	/**
	 * Get Help Post categories
	 * @return mixed
	 */
	public function getCategories();

	/**
	 * Get taxonomy record for help post categories
	 *
	 * @return mixed
	 */
	public function getCategoryTaxonomy();

	/**
	 * Get taxonomy record for the help post sections
	 *
	 * @return mixed
	 */
	public function getSectionTaxonomy();

	/**
	 * Get a collection of sections with their help posts
	 *
	 * @return mixed
	 */
	public function getUsedSections();

	/**
	 * Get a collection of categories with their help posts
	 *
	 * @return mixed
	 */
	public function getUsedCategories();

	/**
	 * Get a collection of categories with their help posts for the given section slug
	 *
	 * @param $sectionSlug
	 * @return mixed
	 */
	public function getUsedCategoriesOfSection($sectionSlug);

	/**
	 * Get Section tag by its slug
	 * @param $slug
	 * @return mixed
	 */
	public function getSectionTagBySlug($slug);

	/**
	 * Get Category tag by its slug
	 * @param $slug
	 * @return mixed
	 */
	public function getCategoryTagBySlug($slug);

	/**
	 * Get help posts by section tag slug and category tag slug
	 * @param $sectionTagSlug
	 * @param $categoryTaxonomySlug
	 * @return mixed
	 */
	public function getHelpPosts($sectionTagSlug, $categoryTaxonomySlug);

	/**
	 * @param array|Collection $tags [id,id,id..]|['slug','slug',..]|['name','name',..]|[Tag,Tag,..]|[[item],[item],..]
	 * @param array $options ['withAnyTag'=>true|false,'tagsField'=>'id'|'slug'|'name','taxonomyId'=>int|null,'page'=>int|0,'perPage'=>int|1000,'filters'=>[ [column,'=','value], ['column2,'>',2] ]|null,'sortField'=>string|'updated_at','sortDirection'=>'ASC'|'DESC']
	 * @return mixed
	 */
	public function getByTags($tags = [],$options = []);

}

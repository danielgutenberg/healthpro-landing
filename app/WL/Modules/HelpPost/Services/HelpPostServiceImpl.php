<?php namespace WL\Modules\HelpPost\Services;

use WL\Modules\HelpPost\Models\HelpPost;
use WL\Modules\Taxonomy\Facades\TaxonomyService;
use \WL\Modules\Taxonomy\Facades\TagService;

class HelpPostServiceImpl implements HelpPostServiceInterface
{
	const SECTION_SLUG = 'help-section';
	const CATEGORY_SLUG = 'help-category';

	public function getSectionSlug(){
		return static::SECTION_SLUG;
	}

	public function getCategorySlug(){
		return static::CATEGORY_SLUG;
	}

	public function getSections()
	{
		return TagService::getTaxonomyTagsBySlug(static::SECTION_SLUG);
	}

	public function getCategories()
	{
		return TagService::getTaxonomyTagsBySlug(static::CATEGORY_SLUG);
	}

	public function getCategoryTaxonomy(){
		return TaxonomyService::getTaxonomyBySlug(static::CATEGORY_SLUG);
	}

	public function getSectionTaxonomy(){
		return TaxonomyService::getTaxonomyBySlug(static::SECTION_SLUG);
	}

	public function getUsedSections()
	{
		return TagService::getTaxonomyTagsForEntityType($this->getSectionTaxonomy()->id, HelpPost::class);
	}

	public function getUsedCategories()
	{
		return TagService::getTaxonomyTagsForEntityType($this->getCategoryTaxonomy()->id, HelpPost::class);
	}

	public function getUsedCategoriesOfSection($sectionSlug)
	{
		return TagService::getTaxonomyTagsForEntityType($this->getCategoryTaxonomy()->id, HelpPost::class, $sectionSlug);
	}

	public function getSectionTagBySlug($slug)
	{
		return TagService::getTaxonomyTagByTagSlug($slug, $this->getSectionTaxonomy()->id);
	}

	public function getCategoryTagBySlug($slug)
	{
		return TagService::getTaxonomyTagByTagSlug($slug, $this->getCategoryTaxonomy()->id);
	}

	public function getHelpPosts($sectionTagSlug, $categoryTagSlug)
	{
		$helpPosts = [];

		$sectionTag = $this->getSectionTagBySlug($sectionTagSlug);
		$categoryTag = $this->getCategoryTagBySlug($categoryTagSlug);

		if ($sectionTag && $categoryTag) {
			$helpPosts = TagService::getEntitiesByTags([$sectionTag->name, $categoryTag->name], HelpPost::getTableName(), []);
			if (!empty($helpPosts))
				$helpPosts = $helpPosts[1];
		}
		return $helpPosts;
	}

	public function getByTags($tags = [],$options = [])
	{
		if(!$tags){
			$tags = $this->getSections()->merge($this->getCategories());
		}
		$helpPosts = TagService::getEntitiesByTags(
			$tags,
			HelpPost::getTableName(),
			array_merge(['withAnyTag'=>true,'tagsField'=>'id','taxonomyId'=>[TaxonomyService::getTaxonomyBySlug(static::SECTION_SLUG)->id,TaxonomyService::getTaxonomyBySlug(static::CATEGORY_SLUG)->id]],$options)
		);

		if(!empty($helpPosts)){
			$helpPosts = $helpPosts[1];
		}

		return $helpPosts;
	}

}

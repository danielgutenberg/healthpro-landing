<?php namespace WL\Modules\Coupons\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use WL\Modules\Coupons\Repositories\CouponRepository;
use WL\Modules\Coupons\Services\CouponService;
use WL\Modules\Coupons\Services\CouponServiceInterface;

class CouponServiceProvider extends ServiceProvider
{
	public function register()
	{
		$this->app->singleton(CouponServiceInterface::class, function () {
			return new CouponService(new CouponRepository());
		});
	}
}


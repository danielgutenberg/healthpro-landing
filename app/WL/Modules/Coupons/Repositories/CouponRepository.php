<?php namespace WL\Modules\Coupons\Repositories;

use Carbon\Carbon;
use WL\Modules\Coupons\Models\Coupon;
use WL\Repositories\DbRepository;

/**
 * Class CouponRepository
 * @package App\Coupons
 */
class CouponRepository extends DbRepository implements CouponRepositoryInterface
{
	function getById($id)
	{
		return Coupon::find($id);
	}

	function removeById($id)
	{
		return Coupon::where('id', $id)->delete();
	}

	function removeByOwner($ownerId, $ownerType)
	{
		return Coupon::where('ownerId', $ownerId)
			->where('ownerType', $ownerType)
			->delete();
	}

	function getByName($name)
	{
		return Coupon::where('name', $name)->first();
	}

	function getAllPaginate($perPage)
	{
		return Coupon::paginate($perPage);
	}

	function getAvailableCouponsByApplicable($applicable)
	{
		$now = Carbon::now();
		return Coupon::where('applyTo', $applicable)
			->where('validFrom', '<=', $now)
			->where('validUntil', '>=', $now)
			->get();
	}

	function getAvailableCouponsByOwner($ownerId, $ownerType)
	{
		$now = Carbon::now();
		return Coupon::where('ownerId', $ownerId)
			->where('ownerType', $ownerType)
			->where('validFrom', '<=', $now)
			->where('validUntil', '>=', $now)
			->get();
	}
}

<?php namespace WL\Modules\Coupons\Repositories;

use Carbon\Carbon;
use WL\Repositories\Repository;

/**
 * Class CouponRepository
 * @package App\Coupons
 */
interface CouponRepositoryInterface extends Repository
{
	function removeById($id);

	/**
	 * Remove coupons by owner
	 * @param $ownerId number
	 * @param $ownerType string
	 * @return mixed
	 */
	function removeByOwner($ownerId, $ownerType);

	/**
	 * @param $name
	 * @return mixed
	 */
	function getByName($name);

	/**
	 * @param $perPage
	 * @return mixed
	 */
	function getAllPaginate($perPage);

	/** Get available Coupons by applyTo column
	 * @param string $applicable
	 * @return mixed
	 */
	function getAvailableCouponsByApplicable($applicable);

	/** Get available Coupons by his Owner
	 * @param $ownerId number
	 * @param $ownerType string
	 * @return mixed
	 */
	function getAvailableCouponsByOwner($ownerId, $ownerType);

}

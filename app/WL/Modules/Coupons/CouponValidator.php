<?php namespace WL\Modules\Coupons;

use Illuminate\Validation\Validator;
use WL\Modules\Coupons\Facades\CouponService;

class CouponValidator extends Validator
{
	public function validateCoupon($attribute, $value, $parameters)
	{
		if (empty($value)) {
			$this->messages()->add($attribute, 'Coupon field is empty');
			return false;
		}

		if (empty(CouponService::getCoupon($value))) {
			$this->messages()->add($attribute, 'Coupon is not exists');
			return false;
		}

		return true;
	}
}

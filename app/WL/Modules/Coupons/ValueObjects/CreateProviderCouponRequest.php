<?php namespace WL\Modules\Coupons\ValueObjects;


use WL\Modules\Profile\Models\ModelProfile;

class CreateProviderCouponRequest extends CreateCouponRequest
{
	public function __construct($coupon, $description, $amount, $amountType, $applyTo, $validFrom, $validUntil)
	{
	    $issuedBy = ModelProfile::STAFF_PROFILE_TYPE;
		parent::__construct($coupon, $description, $amount, $amountType, $applyTo, null, null, $issuedBy, $validUntil, $validFrom, 0, null);
        $this->availableFor = ModelProfile::PROVIDER_PROFILE_TYPE;
	}
}

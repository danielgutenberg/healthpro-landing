<?php namespace WL\Modules\Coupons\ValueObjects;

class ApplyCouponRequest
{
	private $coupon;
	private $applicableEntityType;
	private $applicablePrice;
	private $ownerEntityType;
	private $ownerEntityId;
	private $decrementUsages = true;
	private $availableFor;

	/**
	 * ApplyCouponRequest constructor.
	 * @param $coupon
	 * @param $applicableEntityType
	 * @param $applicablePrice
	 * @param $ownerEntityId
	 * @param $ownerEntityType
	 * @param bool $decrementUsages
	 */
	public function __construct($coupon, $applicableEntityType, $applicablePrice=null, $ownerEntityId=null, $ownerEntityType=null, $decrementUsages = true, $availableFor = null)
	{
		$this->coupon = $coupon;
		$this->applicableEntityType = $applicableEntityType;
		$this->applicablePrice = $applicablePrice;
		$this->ownerEntityType = $ownerEntityType;
		$this->ownerEntityId = $ownerEntityId;
		$this->decrementUsages = $decrementUsages;
		$this->availableFor = $availableFor;
	}

	/**
	 * @return mixed
	 */
	public function getCoupon()
	{
		return $this->coupon;
	}

	/**
	 * @param mixed $coupon
	 */
	public function setCoupon($coupon)
	{
		$this->coupon = $coupon;
	}

	/**
	 * @return mixed
	 */
	public function getApplicableEntityType()
	{
		return $this->applicableEntityType;
	}

	/**
	 * @param mixed $applicableEntityType
	 */
	public function setApplicableEntityType($applicableEntityType)
	{
		$this->applicableEntityType = $applicableEntityType;
	}

	/**
	 * @return mixed
	 */
	public function getApplicablePrice()
	{
		return $this->applicablePrice;
	}

	/**
	 * @param mixed $applicablePrice
	 */
	public function setApplicablePrice($applicablePrice)
	{
		$this->applicablePrice = $applicablePrice;
	}

	/**
	 * @return mixed
	 */
	public function getOwnerEntityType()
	{
		return $this->ownerEntityType;
	}

	/**
	 * @param mixed $ownerEntityType
	 */
	public function setOwnerEntityType($ownerEntityType)
	{
		$this->ownerEntityType = $ownerEntityType;
	}

	/**
	 * @return mixed
	 */
	public function getOwnerEntityId()
	{
		return $this->ownerEntityId;
	}

	/**
	 * @param mixed $ownerEntityId
	 */
	public function setOwnerEntityId($ownerEntityId)
	{
		$this->ownerEntityId = $ownerEntityId;
	}

	/**
	 * @return boolean
	 */
	public function isDecrementUsages()
	{
		return $this->decrementUsages;
	}

	/**
	 * @param boolean $decrementUsages
	 */
	public function setDecrementUsages($decrementUsages)
	{
		$this->decrementUsages = $decrementUsages;
	}

	/**
	 * @return string
	 */
	public function getAvailableFor()
	{
		return $this->availableFor;
	}

	/**
	 * @param string $availableFor
	 */
	public function setAvailableFor($availableFor)
	{
		$this->availableFor = $availableFor;
	}
}

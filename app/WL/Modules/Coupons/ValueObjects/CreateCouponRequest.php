<?php namespace WL\Modules\Coupons\ValueObjects;

class CreateCouponRequest
{
	protected $coupon;
	protected $amountType;
	protected $description;
	protected $applicableEntityType;
	protected $ownerEntityType;
	protected $ownerEntityId;
	protected $issuedBy;
	protected $availableFor;
	protected $validFrom;
	protected $validUntil;
	protected $amount;
	protected $minimumTotal;
	protected $maxUsages;

	/**
	 * CreateCouponRequest constructor.
	 * @param $coupon
	 * @param $description
	 * @param $amount
	 * @param $amountType
	 * @param $applicableEntityType
	 * @param $ownerEntityId
	 * @param $ownerEntityType
	 * @param $issuedBy
	 * @param $validUntil
	 * @param $validFrom
	 * @param $minimumTotal
	 * @param int $maxUsages
	 */
	public function __construct($coupon, $description, $amount, $amountType, $applicableEntityType, $ownerEntityId, $ownerEntityType, $issuedBy, $validUntil, $validFrom, $minimumTotal, $maxUsages = 1)
	{
		$this->coupon = $coupon;
		$this->amountType = $amountType;
		$this->description = $description;
		$this->applicableEntityType = $applicableEntityType;
		$this->ownerEntityType = $ownerEntityType;
		$this->ownerEntityId = $ownerEntityId;
		$this->issuedBy = $issuedBy;
		$this->validFrom = $validFrom;
		$this->validUntil = $validUntil;
		$this->amount = $amount;
		$this->minimumTotal = $minimumTotal;
		$this->maxUsages = $maxUsages;
	}

	/**
	 * @return mixed
	 */
	public function getCoupon()
	{
		return $this->coupon;
	}

	/**
	 * @param mixed $coupon
	 */
	public function setCoupon($coupon)
	{
		$this->coupon = $coupon;
	}

	/**
	 * @return mixed
	 */
	public function getAmountType()
	{
		return $this->amountType;
	}

	/**
	 * @param mixed $amountType
	 */
	public function setAmountType($amountType)
	{
		$this->amountType = $amountType;
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param mixed $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return mixed
	 */
	public function getApplicableEntityType()
	{
		return $this->applicableEntityType;
	}

	/**
	 * @param mixed $applicableEntityType
	 */
	public function setApplicableEntityType($applicableEntityType)
	{
		$this->applicableEntityType = $applicableEntityType;
	}

	/**
	 * @return mixed
	 */
	public function getOwnerEntityType()
	{
		return $this->ownerEntityType;
	}

	/**
	 * @param mixed $ownerEntityType
	 */
	public function setOwnerEntityType($ownerEntityType)
	{
		$this->ownerEntityType = $ownerEntityType;
	}

	/**
	 * @return mixed
	 */
	public function getOwnerEntityId()
	{
		return $this->ownerEntityId;
	}

	/**
	 * @param mixed $ownerEntityId
	 */
	public function setOwnerEntityId($ownerEntityId)
	{
		$this->ownerEntityId = $ownerEntityId;
	}

	/**
	 * @return mixed
	 */
	public function getIssuedBy()
	{
		return $this->issuedBy;
	}

	/**
	 * @param mixed $issuedBy
	 */
	public function setIssuedBy($issuedBy)
	{
		$this->issuedBy = $issuedBy;
	}

	/**
	 * @return mixed
	 */
	public function getValidFrom()
	{
		return $this->validFrom;
	}

	/**
	 * @param mixed $validFrom
	 */
	public function setValidFrom($validFrom)
	{
		$this->validFrom = $validFrom;
	}

	/**
	 * @return mixed
	 */
	public function getValidUntil()
	{
		return $this->validUntil;
	}

	/**
	 * @param mixed $validUntil
	 */
	public function setValidUntil($validUntil)
	{
		$this->validUntil = $validUntil;
	}

	/**
	 * @return mixed
	 */
	public function getAmount()
	{
		return $this->amount;
	}

	/**
	 * @param mixed $amount
	 */
	public function setAmount($amount)
	{
		$this->amount = $amount;
	}

	/**
	 * @return mixed
	 */
	public function getMinimumTotal()
	{
		return $this->minimumTotal;
	}

	/**
	 * @param mixed $minimumTotal
	 */
	public function setMinimumTotal($minimumTotal)
	{
		$this->minimumTotal = $minimumTotal;
	}

	/**
	 * @return int
	 */
	public function getMaxUsages()
	{
		return $this->maxUsages;
	}

	/**
	 * @param int $maxUsages
	 */
	public function setMaxUsages($maxUsages)
	{
		$this->maxUsages = $maxUsages;
	}

	/**
	 * @return mixed
	 */
	public function getAvailableFor()
	{
		return $this->availableFor;
	}

	/**
	 * @param mixed $availableFor
	 */
	public function setAvailableFor($availableFor)
	{
		$this->availableFor = $availableFor;
	}

}

<?php namespace WL\Modules\Coupons\ValueObjects;


use WL\Modules\Order\Models\Order;
use WL\Modules\Profile\Models\ModelProfile;

class CreateClientCouponRequest extends CreateCouponRequest
{
	public function __construct($coupon, $description, $amount, $amountType, $issuedBy, $validFrom, $validUntil)
	{
		parent::__construct($coupon, $description, $amount, $amountType, Order::class, null, null, $issuedBy, $validUntil, $validFrom, 0, null);
        $this->availableFor = ModelProfile::CLIENT_PROFILE_TYPE;
	}
}

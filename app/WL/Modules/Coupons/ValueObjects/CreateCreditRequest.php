<?php namespace WL\Modules\Coupons\ValueObjects;


use WL\Modules\Coupons\Models\Coupon;
use WL\Modules\Profile\Models\ModelProfile;

class CreateCreditRequest extends CreateCouponRequest
{
	public function __construct($coupon, $description, $amount, $applyTo, $validFrom, $validUntil)
	{
		$amountType = Coupon::AMOUNT_TYPE_FIXED;
		$issuedBy = ModelProfile::STAFF_PROFILE_TYPE;

		parent::__construct($coupon, $description, $amount, $amountType, $applyTo, null, null, $issuedBy, $validUntil, $validFrom, 0, null);
		$this->availableFor = $applyTo;
	}
}

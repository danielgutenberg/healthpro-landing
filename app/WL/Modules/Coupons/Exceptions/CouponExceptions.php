<?php namespace WL\Modules\Coupons\Exceptions;

use Exception;

class CouponNotFoundException extends Exception{}
class CouponCannotBeAppliedException extends Exception{}
class CouponCannotBeCreatedException extends Exception{}
class CouponAdaptorNotFoundException extends Exception{}


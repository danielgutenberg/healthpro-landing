<?php namespace WL\Modules\Coupons\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use WL\Models\ModelBase;

/**
 * Class Coupon
 * @package App\Coupons
 */
class Coupon extends ModelBase
{
	use SoftDeletes;

    const AMOUNT_TYPE_FIXED = 'fixed';
    const AMOUNT_TYPE_PERCENT = 'percent';
    const AMOUNT_TYPE_QUANTITY = 'quantity';
	/**
	 * @var string
	 */
	protected $table = 'coupons';


	/**
	 * Default Validation Rules
	 *
	 * @var array
	 */
	protected $rules = array(
		'name' => 'required',
		'applyTo' => 'required',
		'validFrom' => 'required',
		'validUntil' => 'required',
		'amount' => 'required',
		'amountType' => 'required',
	);

	public function isApplicable($to)
	{
		$applicable = ($this->applyTo == $to);
		$now = Carbon::now();
		if ($this->validFrom > $now || $this->validUntil < $now) {
			return false;
		}

		return $applicable;
	}
}

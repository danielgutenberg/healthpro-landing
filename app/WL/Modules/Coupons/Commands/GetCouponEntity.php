<?php namespace WL\Modules\Coupons\Commands;

use WL\Modules\Coupons\CouponValidator;
use WL\Modules\Coupons\Facades\CouponService;
use WL\Modules\Coupons\ValueObjects\ApplyCouponRequest;
use WL\Modules\Profile\Commands\ValidatableCommand;
use WL\Modules\Profile\Models\ModelProfile;

class GetCouponEntity extends ValidatableCommand
{
	const IDENTIFIER = 'coupon.getCouponEntity';
	protected $customValidator = CouponValidator::class;

	protected $rules = [
		'coupon' => 'coupon',
		'types' => 'array',
		'for' => 'sometimes|in:' . ModelProfile::CLIENT_PROFILE_TYPE . ',' . ModelProfile::PROVIDER_PROFILE_TYPE,
	];

	public function validHandle()
	{
	    $types = CouponService::typeToApplication($this->get('types'));

		$request = new ApplyCouponRequest($this->get('coupon'), $types);
		if($this->get('for')) {
			$request->setAvailableFor($this->get('for'));
		}

		return CouponService::getValidCoupon($request);
	}
}

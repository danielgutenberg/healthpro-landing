<?php namespace WL\Modules\Coupons\Commands;


use WL\Modules\Coupons\CouponValidator;
use WL\Modules\Coupons\Facades\CouponService;
use WL\Modules\Coupons\ValueObjects\ApplyCouponRequest;
use WL\Modules\Payment\Facades\PaymentService;
use WL\Modules\Payment\Models\PaymentCurrency;
use WL\Modules\Payment\ValueObjects\IncreaseCreditRequest;
use WL\Modules\Profile\Commands\ValidatableCommand;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Modules\Provider\Models\ProviderPlan;

class ApplyCouponCommand extends ValidatableCommand
{
	const IDENTIFIER = 'coupon.applyCouponEntity';
	protected $customValidator = CouponValidator::class;

	protected $rules = [
		'coupon' => 'coupon',
		'applyToType' => 'required|string',
		'applyTo' => 'required|int',
	];

	public function validHandle()
	{
        $request = new ApplyCouponRequest($this->get('coupon'), $this->get('applyToType'));

		$coupon = CouponService::getValidCoupon($request);
        CouponService::applyCoupon($coupon, $this->get('applyTo'));

	}
}

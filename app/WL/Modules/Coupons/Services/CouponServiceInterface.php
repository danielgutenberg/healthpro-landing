<?php namespace WL\Modules\Coupons\Services;

use WL\Modules\Coupons\Models\Coupon;
use WL\Modules\Coupons\ValueObjects\CreateCouponRequest;

interface CouponServiceInterface
{
	/**
	 * Get coupon by id
	 *
	 * @param $couponId
	 * @return mixed
	 */
	public function getCoupon($couponId);

	/** Create Coupon by options, owner is optional, if owner presented, than coupon will be available only for that owner service
	 * @param $createCouponRequest CreateCouponRequest
	 * @return Coupon
	 */
	public function createCoupon(CreateCouponRequest $createCouponRequest);


	/**
	 * Get Coupons by category specified in applyTo field
	 * @param $applicable
	 * @return mixed
	 */
	public function getAvailableCouponsByApplicable($applicable);

	/**
	 * Get Coupons by owner
	 * @param $ownerId number
	 * @param $ownerType string
	 * @return mixed
	 */
	public function getAvailableCouponsByOwner($ownerId, $ownerType);


	/** Apply coupon and decrement usage
	 * @param Coupon $coupon
     * @param int $applyToId
     * @param bool $decrementUsages
	 */
	public function applyCoupon(Coupon $coupon, $applyToId=null, $decrementUsages=true);

    /**
     * @param Coupon $coupon
     * @param float $price
     * @return float
     */
    public function getCouponDiscount(Coupon $coupon, $price);

	/**
	 * Decrement usages of coupon
	 * @param $couponEntity
	 * @param int $amount
	 * @return mixed
	 */
	public function decrementUsages($couponEntity, $amount = 1);


	/** Paginate all coupons
	 * @param $perPage
	 * @return mixed
	 */
	public function getAllPaginate($perPage);

	/**
	 * Remove coupon by id
	 * @param $id
	 */
	public function removeById($id);

	/**
	 * Remove user coupons
	 * @param $ownerId number
	 * @param $ownerType string
	 */
	public function removeAllByOwner($ownerId, $ownerType);

	/**
	 * @param string $type
	 * @return string
	 */
	public function typeToApplication($type);

}

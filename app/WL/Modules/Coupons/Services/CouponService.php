<?php namespace WL\Modules\Coupons\Services;

use Carbon\Carbon;
use WL\Modules\Coupons\Adaptors\AbstractCouponAdaptor;
use WL\Modules\Coupons\Adaptors\ClientCouponAdaptor;
use WL\Modules\Coupons\Adaptors\OrderCouponAdaptor;
use WL\Modules\Coupons\Adaptors\PlanCouponAdaptor;
use WL\Modules\Coupons\Adaptors\ProviderCouponAdaptor;
use WL\Modules\Coupons\Exceptions\CouponAdaptorNotFoundException;
use WL\Modules\Coupons\Exceptions\CouponCannotBeAppliedException;
use WL\Modules\Coupons\Models\Coupon;
use WL\Modules\Coupons\Repositories\CouponRepositoryInterface;
use WL\Modules\Coupons\ValueObjects\ApplyCouponRequest;
use WL\Modules\Coupons\ValueObjects\CreateCouponRequest;
use WL\Modules\Profile\Models\ModelProfile;

/**
 * Class CouponService
 * @package App\Coupons
 */
class CouponService implements CouponServiceInterface
{
	public $couponRepository;

	public $amountCalc = [];

	protected $adaptors = [
		ClientCouponAdaptor::class,
		ProviderCouponAdaptor::class,
		PlanCouponAdaptor::class,
		OrderCouponAdaptor::class,
	];

	private $registered = [];

	public function __construct(CouponRepositoryInterface $couponRepository)
	{
		$this->couponRepository = $couponRepository;
		$this->amountCalc[Coupon::AMOUNT_TYPE_PERCENT] = function ($price, $amount) {
			return ($price * ($amount / 100.0));
		};

		$this->amountCalc[Coupon::AMOUNT_TYPE_FIXED] = function ($price, $amount) {
			return min($price, $amount);
		};
		$this->registerAdaptors();
	}

	private function registerAdaptors()
	{
		foreach ($this->adaptors as $class) {
			$adaptor = new $class;
			$this->registered['by_type'][$adaptor->getType()] = $adaptor;
			$this->registered['by_application'][$adaptor->getApplyTo()] = $adaptor;
		}

	}

	public function getCoupon($couponId)
	{
		if (is_numeric($couponId)) {
			return $this->couponRepository->getById($couponId);
		} else {
			return $this->couponRepository->getByName($couponId);
		}

	}

    public function getValidCoupon(ApplyCouponRequest $applyCouponRequest)
    {
        $coupon = $this->getCoupon($applyCouponRequest->getCoupon());
        $this->canApplyCoupon($coupon, $applyCouponRequest);
        return $coupon;
	}

	public function createCoupon(CreateCouponRequest $createCouponRequest)
	{
		$coupon = new Coupon();
		$coupon->name = $createCouponRequest->getCoupon();
		$coupon->description = $createCouponRequest->getDescription();
		$coupon->applyTo = $createCouponRequest->getApplicableEntityType();
		$coupon->amount = $createCouponRequest->getAmount();
		$coupon->amountType = $createCouponRequest->getAmountType();
		$coupon->minimumTotal = $createCouponRequest->getMinimumTotal();
		$coupon->validFrom = $createCouponRequest->getValidFrom();
		$coupon->validUntil = $createCouponRequest->getValidUntil();
		$coupon->ownerId = $createCouponRequest->getOwnerEntityId();
		$coupon->ownerType = $createCouponRequest->getOwnerEntityType();
		$coupon->max_usages = $createCouponRequest->getMaxUsages();
		$coupon->issued_by = $createCouponRequest->getIssuedBy();
		$coupon->available_for = $createCouponRequest->getAvailableFor() ?: ModelProfile::CLIENT_PROFILE_TYPE;

		$result = $this->couponRepository->save($coupon);

		return $result ? $coupon : null;
	}


	public function getAvailableCouponsByApplicable($applicable)
	{
		return $this->couponRepository->getAvailableCouponsByApplicable($applicable);
	}

	public function getAvailableCouponsByOwner($ownerId, $ownerType)
	{
		return $this->couponRepository->getAvailableCouponsByOwner($ownerId, $ownerType);
	}

	public function applyCoupon(Coupon $coupon, $applyToId=null, $decrementUsages=true)
	{
        $this->getAdaptorByApplication($coupon->applyTo)->apply($coupon, $applyToId);

		if ($decrementUsages) {
			$this->decrementUsages($coupon);
		}
	}

    public function getCouponDiscount(Coupon $coupon, $price)
    {
        return $this->calcCoupon($price, $coupon);
	}

	public function decrementUsages($couponEntity, $amount = 1)
	{
		$couponEntity->actual_usages += $amount;

		return $this->couponRepository->save($couponEntity);
	}

	private function calcCoupon($applicablePrice, $coupon)
	{
		if (!isset($this->amountCalc[$coupon->amountType])) {
			return 0;
		}

		return $this->amountCalc[$coupon->amountType]($applicablePrice, $coupon->amount);
	}

	private function canApplyCoupon($coupon, ApplyCouponRequest $applyCouponRequest)
	{
        $applicableEntityTypes = $applyCouponRequest->getApplicableEntityType();
        if(!is_array($applicableEntityTypes)) {
			$applicableEntityTypes = [$applicableEntityTypes];
		}
        $applicablePrice = $applyCouponRequest->getApplicablePrice();
        $ownerId = $applyCouponRequest->getOwnerEntityId();
        $ownerType = $applyCouponRequest->getOwnerEntityType();
        $decrementUsages = $applyCouponRequest->isDecrementUsages();
        $availableFor = $applyCouponRequest->getAvailableFor();

		if (!isset($coupon)) {
			throw new CouponCannotBeAppliedException('Coupon not found');
		}

		if ($decrementUsages && $coupon->max_usages && ($coupon->max_usages - $coupon->actual_usages) < 1) {
			throw new CouponCannotBeAppliedException('Coupon usages are exhausted');
		}

		if (!in_array($coupon->applyTo , $applicableEntityTypes)) {
			throw new CouponCannotBeAppliedException("$coupon->applyTo is not applicable");
		}

		if (isset($coupon->ownerId, $ownerType)) {
			if ($ownerId != $coupon->ownerId || $ownerType != $coupon->ownerType) {
				throw new CouponCannotBeAppliedException('owner of item not equal coupon owner');
			}
		}

        if ($availableFor && $coupon->available_for !== $availableFor) {
            throw new CouponCannotBeAppliedException('Coupon is not available for ' . $availableFor);
        }

		$now = Carbon::now();
		if ($coupon->validFrom > $now || $coupon->validUntil < $now) {
			throw new CouponCannotBeAppliedException('out of date');
		}

		if ($coupon->minimumTotal != null) {
			if (intval($coupon->minimumTotal) > $applicablePrice) {
				throw new CouponCannotBeAppliedException('price lower than available');
			}
		}
	}

	public function getAllPaginate($perPage)
	{
		return $this->couponRepository->getAllPaginate($perPage);
	}

	public function removeById($id)
	{
		$this->couponRepository->removeById($id);
	}

	public function removeAllByOwner($ownerId, $ownerType)
	{
		$this->couponRepository->removeByOwner($ownerId, $ownerType);
	}

	public function typeToApplication($type)
	{
	    if(is_array($type)) {
	        return array_map(function($type) {
                return $this->typeToApplication($type);
            }, $type);
        }
		return $this->getAdaptor($type)->getApplyTo();
	}

	private function getAdaptor($type)
	{
		return $this->findAdaptor('by_type', $type);
	}

	private function getAdaptorByApplication($applyTo)
	{
		return $this->findAdaptor('by_application', $applyTo);
	}

	/**
	 * @param string $by
	 * @param string $key
	 * @return AbstractCouponAdaptor
	 * @throws CouponAdaptorNotFoundException
	 */
	private function findAdaptor($by, $key)
	{
		if(!array_key_exists($key, $this->registered[$by])) {
			throw new CouponAdaptorNotFoundException();
		}

		return $this->registered[$by][$key];
	}
}

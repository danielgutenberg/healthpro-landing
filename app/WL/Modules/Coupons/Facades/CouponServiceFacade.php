<?php namespace WL\Modules\Coupons\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Coupons\Services\CouponServiceInterface;

class CouponService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return CouponServiceInterface::class;
	}
}

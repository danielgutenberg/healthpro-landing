<?php namespace WL\Modules\Comment\Populators;

use Carbon\Carbon;
use WL\Modules\Coupons\Models\Coupon;
use WL\Populators\FormerPopulator;

class CouponPopulator extends FormerPopulator
{
	/**
	 * @param Coupon $model
	 * @return $this
	 */
	public function setModel(Coupon $model)
	{
		$this->model = $model;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getFields()
	{
		return [
			'name' => $this->model->name,
			'description' => $this->model->description,
			'amount' => $this->model->amount,
			'apply_to' => $this->model->applyTo,
			'type' => $this->model->issued_by,
			'valid_from' => (new Carbon($this->model->validFrom))->toDateString(),
			'valid_until' => (new Carbon($this->model->validUntil))->toDateString()
		];
	}
}

<?php namespace WL\Modules\Coupons\Adaptors;


use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Modules\Provider\Models\ProviderPlan;

class PlanCouponAdaptor extends AbstractCouponAdaptor
{
	protected $applyTo = ProviderPlan::class;
	protected $type = self::PROVIDER_PLAN;

	public function apply($coupon, $applyToId)
	{
		ProviderMembershipService::addCycles($applyToId, $coupon->amount);
	}
}

<?php namespace WL\Modules\Coupons\Adaptors;


use WL\Modules\Payment\Facades\PaymentService;
use WL\Modules\Payment\Models\PaymentCurrency;
use WL\Modules\Payment\ValueObjects\IncreaseCreditRequest;
use WL\Modules\Profile\Models\ModelProfile;

class ProviderCouponAdaptor extends AbstractCouponAdaptor
{
	protected $applyTo = ModelProfile::PROVIDER_PROFILE_TYPE;
	protected $type = self::PROVIDER;

	public function apply($coupon, $applyToId)
	{
		PaymentService::increaseCredit(new IncreaseCreditRequest($applyToId, $coupon->amount, PaymentCurrency::CURRENCY_USD, $coupon->name));
	}
}

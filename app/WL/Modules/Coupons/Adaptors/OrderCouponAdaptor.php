<?php namespace WL\Modules\Coupons\Adaptors;


use WL\Modules\Order\Models\Order;

class OrderCouponAdaptor extends AbstractCouponAdaptor
{
	protected $applyTo = Order::class;
	protected $type = self::ORDER;
}

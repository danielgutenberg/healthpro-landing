<?php namespace WL\Modules\Coupons\Adaptors;


use WL\Modules\Profile\Models\ModelProfile;

class ClientCouponAdaptor extends AbstractCouponAdaptor
{
	protected $applyTo = ModelProfile::CLIENT_PROFILE_TYPE;
	protected $type = self::CLIENT;

}

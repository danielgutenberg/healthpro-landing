<?php namespace WL\Modules\Coupons\Adaptors;


abstract class AbstractCouponAdaptor
{
	const ORDER = 'order';
	const PROVIDER_PLAN = 'plan';
	const PROVIDER = 'provider';
	const CLIENT = 'client';

	protected $type;
	protected $applyTo;

	public function getApplyTo()
	{
		return $this->applyTo;
	}

	public function getType()
	{
		return $this->type;
	}

	public function apply($coupon, $applyToId)
	{
		return null;
	}
}

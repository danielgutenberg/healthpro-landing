<?php namespace WL\Modules\Attachment\Repositories;

use WL\Repositories\Repository;

interface AttachmentRepositoryInterface extends Repository
{

	/**
	 * Find file
	 * @param type $fileType
	 * @param type $model
	 * @return type
	 */
	public function findFile($fileType, $model);

	/**
	 * Find File by id with checking association
	 *
	 * @param $id
	 * @param $entityId
	 * @param $entityType
	 * @return mixed
	 */
	public function findByIdSecure($id, $entityId, $entityType);

	/**
	 * Create new Instance of Attachment
	 * @param type $fileType
	 * @param type $model
	 * @param type array $params
	 * @param type $category
	 * @return type
	 */
	public function createFile($fileType, $model, array $params = array(), $category=null);

	/**
	 * Return a collections of attachments
	 * @param type $fileType
	 * @param type $elmType
	 * @param type $elmId
	 * @return type
	 */
	public function attachmentCollection($fileType, $elmType, $elmId);

	/**
	 * Remove all entity attachments
	 * @param type $elmType
	 * @param type $elmId
	 * @return type
	 */
	public function removeEntityAttachments($elmType, $elmId);

}

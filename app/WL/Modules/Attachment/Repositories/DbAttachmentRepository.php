<?php namespace WL\Modules\Attachment\Repositories;

use WL\Repositories\DbRepository;
use WL\Modules\Attachment\Models\Attachment;

class DbAttachmentRepository extends DbRepository implements AttachmentRepositoryInterface
{

	protected $modelClassName = Attachment::class;

	public function __construct()
	{
		$this->model = new Attachment();
	}

	public function findFile($fileType, $model)
	{
		return Attachment::where('elm_type', '=', $model->getMorphClass())
			->where('elm_id',   '=', $model->id)
			->where('file',     '=', $fileType)
			->orderBy('id','DESC')
			->first();
	}

	public function findByIdSecure($id, $entityId, $entityType)
	{
		return Attachment::where('elm_type', '=', $entityType)
			->where('elm_id', '=', $entityId)
			->where('id', '=', $id)
			->first();
	}

	public function createFile($fileType, $model, array $params = array(), $category=null)
	{
		return $this->createNew(array_merge([
			'elm_type'      => $model->getMorphClass(),
			'elm_id'        => $model->id,
			'file'          => $fileType,
			'category'      => $category,
		], $params));
	}

	public function attachmentCollection($fileType, $elmType, $elmId)
	{
		return Attachment::where('elm_type', '=', $elmType)
			->where('elm_id',   '=', $elmId)
			->where('file',     '=', $fileType)
			->get();
	}

	public function removeEntityAttachments($elmType, $elmId)
	{
		return Attachment::where('elm_type', '=', $elmType)
			->where('elm_id',   '=', $elmId)
			->delete();
	}

}

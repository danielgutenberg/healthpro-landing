<?php namespace WL\Modules\Attachment\Models;

use Illuminate\Support\Facades\File;
use WL\Contracts\Presentable;
use WL\Models\ModelBase;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File as SavedFile;
use WL\Models\PresentableTrait;
use WL\Modules\Attachment\Exceptions\InvalidConfigAttachmentException;
use WL\Modules\Attachment\Presenters\AttachmentPresenter;
use WL\Yaml\Facades\Yaml;
use Input;
use Imager;

class Attachment extends ModelBase implements Presentable
{
	use PresentableTrait;

	protected $table = 'attachments';

	protected $presenter = AttachmentPresenter::class;

	/**
	 * @param bool $path
	 * @param null $default
	 * @return null
	 * @throws InvalidConfigAttachmentException
	 */
	public function config($path = false, $default = null)
	{
		static $cache = [];

		if (!array_key_exists($this->elm_type, $cache)) {
			$cache[$this->elm_type] = Yaml::get($this->elm_type, 'wl.files.attachment');
		}

		if (null === $cache[$this->elm_type]) {
			throw new InvalidConfigAttachmentException("Can't load '{$this->elm_type}' attachment config");
		}

		return array_get(
			$cache[$this->elm_type],
			$this->file . ($path ? '.' . $path : '')
		) ?: $default;
	}

	/**
	 * @param string|null $size
	 * @return string
	 */
	public function getFilename($size = null)
	{
		$name = $this->name;

		if (null !== $size) {
			list($w, $h) = $this->getImageSize($size);
			$name .= '-' . $w . 'x' . $h;
		}

		return $name . '.' . $this->extension;
	}

	/**
	 * @param mixed $size
	 * @return array
	 */
	public function getImageSize($size)
	{
		$dimensions = $this->config('options.sizes.' . $size);
		$dimensions = is_array($dimensions) ? $dimensions['size'] : $dimensions;

		list($width, $height) = explode('x', $dimensions);

		if (!$height) {
			$height = $width;
		}

		return [$width, $height];
	}

	/**
	 * @param $size
	 * @param string $default
	 * @return string
	 */
	public function getImageResizeFunc($size, $default = 'fit')
	{
		$dimensions = $this->config('options.sizes.' . $size);

		return is_array($dimensions) && isset($dimensions['func']) ? $dimensions['func'] : $default;
	}

	/**
	 * Delete all the attachment files
	 */
	public function deleteFiles()
	{
		// delete sizes if exists
		$sizes = $this->config('options.sizes');
		if (is_array($sizes) && count($sizes)) {
			foreach(array_keys($sizes) as $size ) {
				$this->deleteFile($size);
			}
		}

		// delete original
		$this->deleteFile();

		return true;
	}

	/**
	 * @param null|string $size
	 * @return bool
	 */
	public function deleteFile($size = null)
	{
		$path = $this->relativePath($size);

		if ($this->disk()->exists($path)) {
			$this->disk()->delete(
				$this->relativePath($size)
			);

			return true;
		}

		return false;
	}

	/**
	 * @param null $size
	 * @return null|string
	 */
	public function url($size = null)
	{
		return $this->diskConfig('url') . '/' . $this->relativePath($size);
	}

	/**
	 * @param null $size
	 * @return mixed
	 */
	public function exists($size = null)
	{
		return $this->disk()->exists($this->relativePath($size));
	}

	public function isImage()
	{
		return $this->config('options.image');
	}

	/**
	 * @param null $size
	 * @param string $separator
	 * @return string
	 */
	protected function relativePath($size = null, $separator = '/')
	{
		$folder = $this->config('options.folder');

		$fileName = $this->getFilename($size);

		return $folder . $separator . $fileName;
	}

	/**
	 * @return mixed
	 */
	protected function disk()
	{
		return app('filesystem')->disk($this->diskName());
	}

	/**
	 * @return type
	 */
	protected function diskName()
	{
		return $this->config('options.disk', app('filesystem')->getDefaultDriver());
	}

	/**
	 * @param null $key
	 * @return mixed
	 */
	protected function diskConfig($key = null)
	{
		$config = app('config')['filesystems']['disks'][$this->diskName()];

		return $key ? $config[$key] : $config;
	}

}

<?php namespace WL\Modules\Attachment\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Attachment\Services\AttachmentServiceInterface;

class AttachmentService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return AttachmentServiceInterface::class;
	}
}

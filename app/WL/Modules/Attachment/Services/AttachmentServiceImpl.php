<?php namespace WL\Modules\Attachment\Services;

use WL\Modules\Attachment\Exceptions\AttachmentException;
use WL\Modules\Attachment\Exceptions\AttachmentUploadException;
use WL\Modules\Attachment\Models\Attachment;
use WL\Modules\Attachment\Processors\FileProcessorInterface;
use WL\Modules\Attachment\Repositories\AttachmentRepositoryInterface;
use WL\Modules\Profile\Events\AvatarRemovedEvent;
use WL\Yaml\Facades\Yaml;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AttachmentServiceImpl implements AttachmentServiceInterface
{

	private $repo;

	public function __construct(AttachmentRepositoryInterface $repository)
	{
		$this->repo = $repository;
	}

	public function findById($attachmentId)
	{
		return $this->repo->getById($attachmentId);
	}

	public function findByIdSecure($attachmentId, $model)
	{
		return $this->repo->findByIdSecure($attachmentId, $model->id, $model->getMorphClass());
	}

	private function generateName()
	{
		return md5(uniqid() . time());
	}

	public function findFileSecure($fileType, $model, $createIfNull = true, array $params = array())
	{
		$attachment = $this->findFile($fileType, $model, $createIfNull, $params);
		if (!$attachment) {
			throw new AttachmentException("File not found", 404);
		}

		return $attachment;
	}

	public function findFile($fileType, $model, $createIfNull = true, array $params = array())
	{
		$attachment = $this->repo->findFile($fileType, $model);

		if (null === $attachment && $createIfNull) {
			$attachment = $this->createFile($fileType, $model, $params);
		}

		return $attachment;
	}

	public function uploadFile($fileType, $model, UploadedFile $file, $category = null)
	{
		if ($file->getSize() == 0) {
			throw new AttachmentUploadException("File is empty", 403);
		}

		$attachment = $this->createFromUploadedFile($fileType, $model, $file, $category);

		if (preg_match("/^image|application\/pdf/i", $file->getMimeType())) {
			app(FileProcessorInterface::class)->process($attachment, $file);
		} else {
			throw new AttachmentUploadException(__("Broken image file"), 403);
		}

		$attachment->save();

		return $attachment;
	}

	public function uploadImage($fileType, $model, UploadedFile $file, $category = null)
	{
		if (!preg_match("/^image\//i", $file->getMimeType())) {
			throw new AttachmentUploadException(__("File is not an image"), 403);
		}

		$orgFilePath = $file->getRealPath();
		list($width, $height, $type, $attr) = getimagesize($orgFilePath);
		if (!$width || !$height) {
			throw new AttachmentUploadException(__("Broken image file"), 403);
		}

		// Recreate file to Remove EXIF data from Image
		$newFilePath = tempnam(sys_get_temp_dir(), "uploadimage");
		$newFileCreated = false;

		// if Image have a damages, all works but GD show warnings "imagecreatefromjpeg(): gd-jpeg, libjpeg: recoverable error:""
		$oldIni = ini_set('gd.jpeg_ignore_warning', true);

		switch ($type) {
			case IMAGETYPE_GIF:
				$orgImg = imagecreatefromgif($orgFilePath);
				break;
			case IMAGETYPE_JPEG:
				$orgImg = imagecreatefromjpeg($orgFilePath);
				break;
			case IMAGETYPE_PNG:
				$orgImg = imagecreatefrompng($orgFilePath);
				break;
			default:
				throw new AttachmentUploadException(__("Unsupported image format"), 403);
		}

		// Check if this image is PNG or GIF, then set if Transparent
		if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_PNG) {
			$newImg = imagecreatetruecolor($width, $height);
			imagealphablending($newImg, false);
			imagesavealpha($newImg, true);
			$transparency_index = imagecolortransparent($orgImg);
            $palletsize = imagecolorstotal($orgImg);
			if ($transparency_index >= 0 && $transparency_index < $palletsize) {
				$transparent_color = imagecolorsforindex($orgImg, $transparency_index);
				$transparency_index = imagecolorallocatealpha($newImg, $transparent_color['red'],
					$transparent_color['green'], $transparent_color['blue'], 127);
				imagecolortransparent($newImg, $transparency_index);
				imagefill($newImg, 0, 0, $transparency_index);
			} elseif ($type == IMAGETYPE_PNG) {
				$transparency_index = imagecolorallocatealpha($newImg, 0, 0, 0, 127);
				imagefill($newImg, 0, 0, $transparency_index);
			}
			imagecopyresampled($newImg, $orgImg, 0, 0, 0, 0, $width, $height, $width, $height);
		} else {
			$newImg = $orgImg;
		}

		switch ($type) {
			case IMAGETYPE_GIF:
				$newFileCreated = imagegif($newImg, $newFilePath);
				break;
			case IMAGETYPE_JPEG:
				$newFileCreated = imagejpeg($newImg, $newFilePath);
				break;
			case IMAGETYPE_PNG:
				$newFileCreated = imagepng($newImg, $newFilePath);
				break;
		}

		ini_set('gd.jpeg_ignore_warning', $oldIni);

		if (!$newFileCreated) {
			throw new AttachmentUploadException(__("Temporarily unavailable"), 500);
		}

		// normalize extension if it's mised or differ from mimeType, it's important for thumbs
		$mimeExt = image_type_to_extension($type);
		$replaceExtension = [
			'.jpeg' => '.jpg',
			'.tiff' => '.tif',
		];
		if (array_key_exists($mimeExt, $replaceExtension)) {
			$mimeExt = $replaceExtension[$mimeExt];
		}

		$fileName = preg_replace('/' . preg_quote($mimeExt, '/') . '$/i', '',
				$file->getClientOriginalName()) . $mimeExt;
		$file = new UploadedFile($newFilePath, $fileName, $file->getClientMimeType(), filesize($newFilePath));
		$attachement = $this->uploadFile($fileType, $model, $file, $category);
		unlink($newFilePath);

		return $attachement;
	}

	public function createFile($fileType, $model, array $params = array(), $category = null)
	{
		return $this->repo->createFile($fileType, $model, $params, $category);
	}

	public function createFromUploadedFile($fileType, $model, UploadedFile $file, $category = null)
	{
		return $this->createFile($fileType, $model, [
			'name' => $this->generateName(),
			'extension' => $file->getClientOriginalExtension(),
			'original_name' => $file->getClientOriginalName(),
			'content_type' => $file->getMimeType(),
			'size' => $file->getSize(),
		], $category);
	}

	public function attachmentCollection($fileType, $model)
	{
		return $this->repo->attachmentCollection($fileType, $model->getMorphClass(), $model->id);
	}

	public function removeById($attachmentId, $profileId = null)
	{
		$destroy = $this->repo->destroy($attachmentId);
		if ($profileId) {
			event(new AvatarRemovedEvent($profileId));
		}

		return $destroy;
	}

	public function removeEntityAttachments($model)
	{
		return $this->repo->removeEntityAttachments($model->getMorphClass(), $model->id);
	}

	public function entityCategories($model)
	{
		$config = Yaml::get($model->getMorphClass(), 'wl.files.attachment');

		$categories = [0 => __('--None--')];
		if (!empty($config) && isset($config['categories'])) {
			foreach ($config['categories'] as $category => $status) {
				if ($status) {
					$categories[$category] = ucfirst($category);
				}
			}
		}

		return $categories;
	}

	public function save(Attachment $attachment)
	{
		return $this->repo->save($attachment);
	}

	public function loadAttachmentUrlsById($attachmentId)
	{
		$attach = $this->repo->getById($attachmentId);
		if ($attach) {

			$urls['original'] = $attach->content_type == 'url' ? $attach->original_name : $attach->url();
			$cfg = $attach->config();


			if (isset($cfg['options']['sizes'])) {
				foreach ($cfg['options']['sizes'] as $size) {
					$urls[$size] = $attach->url($size);
				}
			}

			return $urls;
		}

		return [];
	}
}

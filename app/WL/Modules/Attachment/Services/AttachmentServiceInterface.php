<?php namespace WL\Modules\Attachment\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use WL\Modules\Attachment\Models\Attachment;

/**
 * Interface AttachmentServiceInterface
 * @package WL\Modules\Attachment\Services
 */
interface AttachmentServiceInterface
{

	/**
	 * Find by ID
	 * @param type $attachmentId
	 * @return type
	 */
	public function findById($attachmentId);

	/**
	 * Find file by id with checking association to model
	 * @param $attachmentId
	 * @param $model
	 * @return mixed
	 */
	public function findByIdSecure($attachmentId, $model);

	/**
	 * Find attachment or throw an exception
	 * @param type $fileType
	 * @param type $model
	 * @param type $createIfNull
	 * @param type array $params
	 * @return type
	 */
	public function findFileSecure($fileType, $model, $createIfNull = true, array $params = array());

	/**
	 * Find attachment
	 * @param type $fileType
	 * @param type $model
	 * @param type $createIfNull
	 * @param type array $params
	 * @return type
	 */
	public function findFile($fileType, $model, $createIfNull = true, array $params = array());

	/**
	 * Upload file
	 * @param type $model
	 * @param type $fileType
	 * @param type UploadedFile $file
	 * @param type $category
	 * @return type
	 */
	public function uploadFile($fileType, $model, UploadedFile $file, $category = null);

	/**
	 * Upload image file
	 * @param type $model
	 * @param type $fileType
	 * @param type UploadedFile $file
	 * @param type $category
	 * @return type
	 */
	public function uploadImage($fileType, $model, UploadedFile $file, $category = null);

	/**
	 * Create instance of attachement
	 * @param type $fileType
	 * @param type $model
	 * @param type array $params
	 * @param type $category
	 * @return type
	 */
	public function createFile($fileType, $model, array $params = array(), $category=null);

	/**
	 * Create instance of attachment by UploadedFile
	 * @param type UploadedFile $file
	 * @param type $fileType
	 * @param type $model
	 * @param type $category
	 * @return type
	 */
	public function createFromUploadedFile($fileType, $model, UploadedFile $file, $category=null);

	/**
	 * Find entity attachments by type
	 * @param type $fileType
	 * @param type $model
	 * @return type
	 */
	public function attachmentCollection($fileType, $model);

	/**
	 * Delete attachment by ID
	 * @param type $attachmentId
	 * @return type
	 */
	public function removeById($attachmentId, $profileId = null);

	/**
	 * Delete all entity attachments
	 * @param type $model
	 * @return type
	 */
	public function removeEntityAttachments($model);


	/**
	 * Save model
	 * @param Attachment $attachment
	 * @return mixed
	 */
	public function save(Attachment $attachment);

	/**
	 * Load all size and original urls from attachments.yaml
	 *
	 * @param $attachmentId
	 * @return mixed
	 */
	public function loadAttachmentUrlsById($attachmentId);
}

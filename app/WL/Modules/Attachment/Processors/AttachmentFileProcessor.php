<?php namespace WL\Modules\Attachment\Processors;

use Illuminate\Contracts\Filesystem\Filesystem;
use Imager;
use Imagick;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AttachmentFileProcessor implements FileProcessorInterface
{
	/**
	 * @var Filesystem
	 */
	private $filesystem;

	/**
	 * @param Filesystem $filesystem
	 */
	public function __construct(Filesystem $filesystem)
	{
		#@todo check it ..
		$this->filesystem = $filesystem;
	}

	/**
	 * @param $attachment
	 * @param UploadedFile $file
	 * @return bool
	 */
	public function process($attachment, UploadedFile $file)
	{
		// there's no way to test file_get_contents function so we change the file contents to an empty string
		$moved = $this->filesystem->put($this->getFilePath($attachment), file_get_contents($file));

		if ($moved && $attachment->config('options.image')) {
			$this->processImage($attachment, $file);
		}

		return true;
	}

	/**
	 * @param $attachment
	 * @param $file
	 * @throws InvalidConfigException
	 * @return bool
	 */
	private function processImage($attachment, $file)
	{
		$sizes = $attachment->config('options.sizes');

		if (is_array($sizes) && count($sizes)) {
			foreach (array_keys($sizes) as $size) {
				$this->resizeImage($attachment, $file, $size);
			}
		}

		return true;
	}

	/**
	 * @param $attachment
	 * @param $file
	 * @param mixed $size
	 * @param null $quality
	 * @return mixed
	 */
	private function resizeImage($attachment, $file, $size, $quality = null)
	{
		$func = $attachment->getImageResizeFunc($size);

		list($w, $h) = $attachment->getImageSize($size);


		if ($this->isPdf($attachment->content_type)) {
			return $this->makePdfPreview($attachment, $w, $h, $func, $size, $quality);
		}

		return $this->makeImagePreview($attachment, $w, $h, $func, $size, $quality);
	}

	/**
	 * @param $attachment
	 * @param null $name
	 * @throws InvalidConfigException
	 * @return string
	 */
	private function getFilePath($attachment, $name = null)
	{
		if (null === $name) {
			$name = $attachment->getFileName();
		}

		return $attachment->config('options.folder', 'attachments') . DIRECTORY_SEPARATOR . $name;
	}

	private function makeImagePreview($attachment, $w, $h, $func, $size, $quality = null)
	{
		$resizedName = $attachment->getFileName($size);
		$image = Imager::make($this->filesystem->get($this->getFilePath($attachment)));

		$image->$func($w, $h)
			->encode($attachment->extension, $quality);

		return $this->filesystem->put($this->getFilePath($attachment, $resizedName), $image->encoded);
	}

	private function makePdfPreview($attachment, $w, $h, $func, $size, $quality = null)
	{
		$fileContent = $this->filesystem->get($this->getFilePath($attachment));
		$im = new Imagick();
		$im->readImageBlob($fileContent);
		$im->setImageFormat('jpg');
		$imageBlob = $im->getImageBlob();
		$im->clear();
		$im->destroy();

		$this->filesystem->delete($this->getFilePath($attachment));

		$image = Imager::make($imageBlob);

		$image->$func($w, $h)
			->encode('jpg', $quality);

		$attachment->extension = 'jpg';
		$attachment->original_name = $attachment->original_name . 'jpg';
		$attachment->content_type = 'image/jpeg';

		$imageFileName = $attachment->name . '.jpg';
		$this->filesystem->put($this->getFilePath($attachment, $imageFileName), $imageBlob);

		$resizedName = $attachment->getFileName($size);

		return $this->filesystem->put($this->getFilePath($attachment, $resizedName), $image->encoded);
	}

	private function isPdf($mime)
	{
		return $mime == 'application/pdf';
	}
}

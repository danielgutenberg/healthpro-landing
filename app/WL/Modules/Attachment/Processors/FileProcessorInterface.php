<?php namespace WL\Modules\Attachment\Processors;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileProcessorInterface
{
	public function process($model, UploadedFile $file);
}

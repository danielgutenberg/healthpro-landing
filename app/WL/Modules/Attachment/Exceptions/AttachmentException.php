<?php namespace WL\Modules\Attachment\Exceptions;

use App\Exceptions\GenericException;

class AttachmentException extends GenericException
{

}

<?php namespace WL\Modules\Attachment\Providers;

use Illuminate\Support\ServiceProvider;
use WL\Modules\Attachment\Models\Attachment;
use WL\Modules\Attachment\Observers\AttachmentObserver;

class AttachmentServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('\WL\Modules\Attachment\Services\AttachmentServiceInterface', '\WL\Modules\Attachment\Services\AttachmentServiceImpl');
		$this->app->bind('\WL\Modules\Attachment\Repositories\AttachmentRepositoryInterface', '\WL\Modules\Attachment\Repositories\DbAttachmentRepository');
		$this->app->bind('\WL\Modules\Attachment\Processors\FileProcessorInterface', '\WL\Modules\Attachment\Processors\AttachmentFileProcessor');
	}

	/**
	 * Register observers
	 *
	 * @return void
	 */
	public function boot()
	{
		Attachment::observe( new AttachmentObserver() );
	}
}

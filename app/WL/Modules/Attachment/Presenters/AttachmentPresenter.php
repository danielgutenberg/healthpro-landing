<?php namespace WL\Modules\Attachment\Presenters;

use WL\Presenters\Presenter;

class AttachmentPresenter extends Presenter
{
	public function url($size = null, $default = '')
	{
		if (!$this->entity->exists($size)) {
			return $default;
		}

		return $this->entity->url($size);
	}

	public function isImage()
	{
		return $this->entity->isImage();
	}
}

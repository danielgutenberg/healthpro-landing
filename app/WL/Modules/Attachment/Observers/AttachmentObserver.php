<?php namespace WL\Modules\Attachment\Observers;

use WL\Modules\Attachment\Models\Attachment;

class AttachmentObserver
{
	public function deleting( Attachment $model )
	{
		// on the raw save we don't need to handle any events
		if ( $model->ignoreEvents ) {
			return true;
		}

		return $model->deleteFiles();
	}
}

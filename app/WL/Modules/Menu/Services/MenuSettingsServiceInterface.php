<?php namespace WL\Modules\Menu\Services;

use WL\Modules\Menu\Models\BasicMenu;

/**
 * Interface MenuSettingsServiceInterface
 * @package WL\Modules\Menu\Services
 */
interface MenuSettingsServiceInterface
{
	/**
	 * Get menu by menu name
	 * @param $name string
	 * @return BasicMenu
	 */
	public function getRawData($name);
}

<?php namespace WL\Modules\Menu\Services;

use WL\Modules\Menu\Repositories\MenuSettingsRepositoryInterface;

class MenuSettingsSettingsService implements MenuSettingsServiceInterface
{
	private $menuRepository;

    const GLOBAL_TEMPLATE_SLUG = 'global_template';

	function __construct(MenuSettingsRepositoryInterface $menuRepository)
	{
		$this->menuRepository = $menuRepository;
	}

    /**
     * Get default template global ..
     *
     * @param string $slug
     * @return \WL\Modules\Menu\Models\BasicMenu
     */
    public function getDefaultTemplate($slug = self::GLOBAL_TEMPLATE_SLUG)
    {
        return $this->menuRepository->loadMenuData($slug);
    }

    /**
     * Get menu data ..
     *
     * @param string $name
     * @return \WL\Modules\Menu\Models\BasicMenu
     */
    public function getRawData($name)
	{
		return $this->menuRepository->loadMenuData($name);
	}

}

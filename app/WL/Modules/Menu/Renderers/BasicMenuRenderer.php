<?php namespace WL\Modules\Menu\Renderer;

use WL\Modules\Menu\Contracts\RendererInterface;

class BasicMenuRenderer implements RendererInterface {

	private $menu;

	public function __construct($menu)
	{
		$this->menu = $menu;
	}

	public function render($options = [])
	{
		$this->menu->setOptions($options);

		return $this->menu->render();
	}
}

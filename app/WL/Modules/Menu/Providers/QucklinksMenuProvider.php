<?php namespace WL\Modules\Menu\Providers;

use WL\Modules\Menu\Contracts\MenuProviderInterface;
use WL\Modules\Menu\Models\BasicMenu;
use WL\Modules\Menu\Models\BasicMenuItem;
use WL\Modules\Menu\Models\QuicklinkConversationsMenuItem;
use WL\Modules\Menu\Renderer\BasicMenuRenderer;

class QuicklinksMenuProvider extends AbstractProvider implements MenuProviderInterface
{
	protected $items = [
		'conversations' => [
			'label' => 'Messages',
			'link' => 'dashboard-conversations',
			'link_type' => 'route',
			'model' => QuicklinkConversationsMenuItem::class,
		]
	];

	public function provide($options = null) {
		$menu = new BasicMenu();
		$menu->setOptions(array_merge($options, (array)$this->getOptions()));

		$links = [];

		array_walk($this->items, function($item, $key) use(&$links) {
			$itemModel = isset($item['model']) ? $item['model'] : BasicMenuItem::class;
			$links[] = new $itemModel([
				'label' => $item['label'],
				'link' => $item['link'],
				'link_type' => $item['link_type'],
				'link_attr' => 'data-tooltip="dark" title="' . $item['label'] . '"',
				'profile_types' => ['provider', 'client'],
				'classes' => 'header--quicklinks--item m-' . $key,
			]);
		});

		$menu->setItems($links);

		return (new BasicMenuRenderer($menu))->render([
			'classes' => 'header--quicklinks',
		]);
	}
}

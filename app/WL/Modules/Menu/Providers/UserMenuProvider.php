<?php namespace WL\Modules\Menu\Providers;

use stdClass;
use WL\Modules\Menu\Contracts\MenuProviderInterface;
use WL\Modules\Menu\Models\BasicMenu;
use WL\Modules\Menu\Models\BasicMenuItem;
use WL\Modules\Menu\Renderer\BasicMenuRenderer;
use WL\Modules\Profile\Facades\ProfileService;

class UserMenuProvider extends AbstractProvider implements MenuProviderInterface
{

	public function provide($options = null)
	{
		if (!ProfileService::getCurrentProfile()) {
			$menu = new BasicMenu($this->getMenuItems());
			return (new BasicMenuRenderer($menu))->render($options);
		}

		return view(empty($options['user_menu_template']) ? 'menu.user-menu' : $options['user_menu_template']);
	}

	private function getMenuItems()
	{
		$menuItems = [];
		$items = [
			// [
			// 	'label' => 'Sign Up',
			// 	'attr' => 'class="header--user--item"',
			// 	'link_type' => 'route',
			// 	'link' => 'auth-register',
			// 	'link_attr' => 'class="btn m-blue m-border" data-toggle="popup" data-target="popup_auth" data-authpopup-toggle="sign_up"'
			// ],
			[
				'label' => 'Log In',
				'attr' => 'class="header--user--item"',
				'link_type' => 'route',
				'link' => 'auth-login',
				'link_attr' => 'class="btn m-blue m-link" data-toggle="popup" data-target="popup_auth" data-authpopup-toggle="login"'
			]
		];

		foreach ($items as $item) {
			$menuItems[] = new BasicMenuItem($item);
		}

		return $menuItems;
	}

}

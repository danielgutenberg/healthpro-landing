<?php namespace WL\Modules\Menu\Providers;

use Setting;
use WL\Modules\Menu\Contracts\MenuProviderInterface;
use WL\Modules\Menu\Models\BasicMenu;
use WL\Modules\Menu\Models\BasicMenuItem;
use WL\Modules\Menu\Renderer\BasicMenuRenderer;

class SocialProvider extends AbstractProvider implements MenuProviderInterface {

	protected $defaultSocials = array(
		'social_facebook' 	=> 'Facebook',
		'social_twitter' 	=> 'Twitter',
		'social_linkedin' 	=> 'LinkedIn',
		'social_googleplus' => 'Google+',
		'social_youtube' 	=> 'Youtube',
	);

	public function __construct($rawMenu) {
		if( isset($rawMenu['template']) )
			$this->template = array_pull($rawMenu, 'template');
	}


	public function provide($options = null) {
		$menu = new BasicMenu();
		$menu->setOptions(array_merge($options, (array)$this->getOptions()));

		if( $template = $this->template )
			$menu->setTemplate($template);

		$links = array();

		array_walk($this->defaultSocials, function($alias, $key) use(&$links) {
			$links[] = new BasicMenuItem(array(
				'label' => $alias,
				'link_attr'  => 'target="_blank" role="external"',
				'classes' => 'btn m-social m-icon_only ' . 'm-' . str_replace('social_', '', $key ),
				'link'   => Setting::get($key),
			));
		});

		$menu->setItems($links);

		return (new BasicMenuRenderer($menu))->render($options);
	}
}

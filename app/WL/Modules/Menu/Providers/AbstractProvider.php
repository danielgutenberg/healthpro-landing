<?php

namespace WL\Modules\Menu\Providers;

use WL\Modules\Menu\Security\SecurityTrait;

abstract class AbstractProvider {

	use SecurityTrait;

	protected $pages;

	protected $options;

	public function __construct(array $pages = array(), array $options = array()) {
		$this->pages = $pages;
		$this->options = $options;

		self::setCurrentProfileType();
	}

	/**
	 * Get pages ..
	 *
	 * @return array
	 */
	public function getPages() {
		return $this->pages;
	}

	/**
	 * Get options ..
	 *
	 * @return array
	 */
	public function getOptions() {
		return $this->options;
	}

	/**
	 * Set pages ..
	 *
	 * @param array $pages
	 */
	public function setPages(array $pages = array()) {
		$this->pages = $pages;
	}
}

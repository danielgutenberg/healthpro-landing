<?php namespace WL\Modules\Menu\Providers;

use WL\Modules\Blog\Facades\BlogService;
use WL\Modules\Menu\Contracts\MenuProviderInterface;
use WL\Modules\Menu\Renderer\BasicMenuRenderer;
use WL\Modules\Menu\Models\BasicMenu;
use WL\Modules\Menu\Models\BasicMenuItem;

/**
 * Load blog categories menu
 *
 * Class BlogCategoryMenuProvider
 * @package WL\Modules\Menu\MenuProviders
 */
class BlogArchiveMenuProvider extends AbstractProvider implements MenuProviderInterface
{
	public function provide($options = [])
	{
		$categories = BlogService::getCategoriesWithPosts();

		$items = [];
		foreach ($categories as $category) {
			$item = new BasicMenuItem();
			$item->setLabel($category->name);
			$item->set('link', route('blog-category-non-page', $category->slug));

			$items[] = $item;
		}

		return (new BasicMenuRenderer(new BasicMenu($items)))->render($options);
	}
}

<?php namespace WL\Modules\Menu\Providers;

use WL\Modules\Menu\Contracts\MenuProviderInterface;
use WL\Modules\Menu\Models\BasicMenu;
use WL\Modules\Menu\Models\BasicMenuItem;
use WL\Modules\Menu\Renderer\BasicMenuRenderer;

class BasicMenuProvider extends AbstractProvider implements MenuProviderInterface
{

	public function __construct(array $menu) {
		$items   = array_pull($menu, 'items');
		$options = (array)$menu;

		parent::__construct($items, $options);
	}

	public function makeBasicMenu($items, $options = array())
	{
		$menu = new BasicMenu();
		$menu->setOptions(array_merge($options, $this->getOptions()));

		$basicMenuItems = $this->makeBasicMenuItems($items);
		$menu->setItems($basicMenuItems);

		foreach ($basicMenuItems as $item) {
			if($item->submenu) {

				$submenu = $item->submenu;
				$items   = array_pull($submenu, 'items');

				$item->setSubMenu($this->makeBasicMenu($items, $submenu));
			}
		}

		return $menu;
	}

	private function makeBasicMenuItems(array $items)
	{
		$return = array();
		array_walk($items, function($item) use(&$return) {
			$return[] = new BasicMenuItem($item);
		});

		return $return;
	}

	public function provide($options = array())
	{
		return (new BasicMenuRenderer($this->makeBasicMenu($this->getPages(), $options)))->render($options);
	}
}

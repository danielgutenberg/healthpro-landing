<?php namespace WL\Modules\Menu\Repositories;

use WL\Modules\Menu\Models\MenuItemsContainer;
use WL\Yaml\Contracts\Parsable;

/**
 * Class YamlMenuSettingsRepository
 * @package WL\Modules\Menu\Repositories
 */
class YamlMenuSettingsRepository implements MenuSettingsRepositoryInterface
{
	private $parser;
	private $localCache;

	function __construct(Parsable $parser)
	{
		$this->parser = $parser;
	}

	public function loadMenuData($menuName)
	{
		$result = null;
		if ($this->localCache) {
			$data = $this->localCache;
		} else {
			$data = $this->parser->parse('wl.menu.menu');
			$this->localCache = $data;
		}

		$data = isset($data[$menuName]) ? $data[$menuName] : null;

		if (!empty($data)) {
			$result = $data;
		}

		return $result;
	}
}

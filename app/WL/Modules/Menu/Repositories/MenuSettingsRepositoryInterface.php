<?php namespace WL\Modules\Menu\Repositories;

use WL\Modules\Menu\Models\BasicMenu;

interface MenuSettingsRepositoryInterface
{
	/**
	 * Load menu by name
	 * @param $menuName string
	 * @return BasicMenu
	 */
	public function loadMenuData($menuName);
}

<?php

namespace WL\Modules\Menu\Models;

use URL;
use WL\Modules\Conversation\Facades\ConversationService;
use WL\Modules\Menu\Exceptions\MenuException;
use WL\Modules\Menu\Security\SecurityTrait;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Provider\Facades\ProviderMembershipService;

class BasicMenuItem
{

	use SecurityTrait;

	const LINK_TYPE_ROUTE = 'route';

	private $data;

	private $subMenu;

	function __construct(array $data = array())
	{
		$this->data = $data;
	}


	public function __get($name)
	{
		$value = isset($this->data[$name]) ? $this->data[$name] : '';
		if ($name == 'link') {
			$value = $this->generateLink($value);
		}
		return $value;
	}

	/**
	 * Render item .
	 *
	 * @param null $template
	 * @param bool $asRecursive
	 * @return null
	 */
	public function render($template, $asRecursive = true) {
		/** Is current profile / user authorized ? */
		if( ! $this->isAuthorized($this) )
			return false;

		/** Check if is visible */
		if( !$this->isVisible() )
			return false;

		if( $this->isActive() ) {
			if(! $this->get('active_classes'))
				$this->set('active_classes', 'm-current');
		} else {
			$this->set('active_classes', '');
		}

		$patterns = $this->getPatterns();

		if( $this->wrap_link )
			$template = str_replace("%content%", "<span>%content%</span>", $template);

		array_walk($patterns, function($pattern, $index) use(&$template, $asRecursive) {
			$template = preg_replace(sprintf('/%s/', $index), $pattern, $template);
			if (isset($this->data['notifications'])) {
				$template = str_replace("<span>%content%</span>", $this->getNotificationHtml() . "<span>%content%</span>" , $template);
			};
			if( $asRecursive ) {
				$submenuHtml = '';
				if ($submenu = $this->getSubMenu()) {
					$submenuHtml = $submenu->render($template, $asRecursive);
				}

				$template = str_replace('%submenu%', $submenuHtml, $template);
			}

		});
		return $template;
	}

	private function getNotificationHtml()
	{
		$unreadConversations = ConversationService::getUnreadConversationListByProfileId(ProfileService::getCurrentProfileId());
		$count = count($unreadConversations);

		if ($count > 5)
			$count = '5+';

		# append notification if there are conversations with unread messages
		return ($count) ? '<i>' . $count . '</i>' : '';
	}

	/**
	 * @return mixed
	 */
	public function getSubMenu()
	{
		return $this->subMenu;
	}

	/**
	 * @param mixed $subMenu
	 */
	public function setSubMenu($subMenu)
	{
		$this->subMenu = $subMenu;
	}


	/**
	 * Sets the given property
	 * @param $property
	 * @param $value
	 * @return $this
	 * @throws MenuException
	 */
	public function set($property, $value)
	{
		if (!is_string($property) || empty($property))
			throw new MenuException(_('Invalid property. It must be string'));

		$this->data[$property] = $value;

		return $this;
	}

	/**
	 * Returns the value of the given property
	 * @param $property
	 * @param string $default
	 * @return null
	 * @throws MenuException
	 */
	public function get($property, $default = '')
	{
		if (!is_string($property) || empty($property))
			throw new MenuException(_('Invalid property. It must be string'));

		if( isset($this->data[$property]) && $value = $this->data[$property] )
			return $value;

		return $default;
	}


	/**
	 * Check if that item is active ..
	 * @return bool
	 */
	public function isActive() {
		$urls     = array_merge([$this->link], $this->getActivableRoutes());
		$isActive = false;

		array_walk($urls, function($url) use(&$isActive) {
			if( $url == URL::full() ) {
				$isActive = true;

				return false;
			}
		});

		return $isActive;
	}


	/**
	 * Sets whether the page should be visible or not
	 * @param bool $visible
	 * @return $this
	 */
	public function setVisible($visible = true)
	{
		if (is_string($visible) && 'false' == strtolower($visible)) {
			$visible = false;
		}
		$this->set('hide', (bool) $visible);

		return $this;
	}

	/**
	 * Returns a boolean value indicating whether the page is visible
	 * @return bool|string
	 */
	public function isVisible()
	{
		return !is_null($this->get('hide')) ? !(bool)$this->get('hide') : true;
	}

	/**
	 * Proxy to isVisible()
	 *
	 */
	public function getVisible()
	{
		return $this->isVisible();
	}


	/**
	 * Set label .
	 *
	 * @param $label
	 * @return $this
	 * @throws MenuException
	 */
	public function setLabel($label)
	{
		if (null !== $label && !is_string($label)) {
			throw new MenuException(_('Invalid argument: $label must be a string or null'));
		}

		$this->set('label', $label);
		return $this;
	}

	/**
	 * Returns page label
	 *
	 * @return string  page label or null
	 */
	public function getLabel()
	{
		return $this->get('label');
	}

	/**
	 * Get patterns ..
	 *
	 * @return array
	 * @throws MenuException
	 */
	protected function getPatterns() {
		return [
			'%label%' 			=> !empty($this->getLabel()) ? _($this->getLabel()) : '',
			'%content%' 		=> !empty($this->getLabel()) ? _($this->getLabel()) : '',
			'%attributes%'  	=> $this->get('attr'),
			'%link_attr%'  	    => $this->get('link_attr'),
			'%title%' 			=> !empty($this->get('title')) ? _($this->get('title')) : '',
			'%target%' 			=> $this->get('target'),
			'%classes%' 		=> $this->get('classes'),
			'%active_classes%' 	=> $this->get('active_classes'),
			'%link%' 			=> $this->link,
		];
	}


	/**
	 * @return mixed
	 */
	public function getRawData()
	{
		return $this->data;
	}

	/**
	 * Mark item active on routes .
	 *
	 * @return array
	 */
	public function getActivableRoutes()
	{
		$result = [];
		if (empty($this->get('activate_on_routes')))
			return $result;

		foreach($this->get('activate_on_routes') as $route) {
			if (is_array($route)) {
				$result[] = route($route['route'], $route['params']);
			} else {
				$result[] = route($route);
			}
		}


		return $result;
	}

	private function generateLink($link)
	{
		if ($link && isset($this->data['link_type']) && ($this->data['link_type'] == static::LINK_TYPE_ROUTE)) {
			$params = isset($this->data['link_params']) ? $this->data['link_params'] : [];
			return route($link, $params);
		}
		return $link;
	}

	private function isFeatureEnabled()
	{
		if(!$feature = $this->get('feature')) {
			return true;
		}
		$profile = ProfileService::getCurrentProfile();
		if(!$profile || $profile->type !== ModelProfile::PROVIDER_PROFILE_TYPE) {
			return true;
		}
		return ProviderMembershipService::hasFeature($profile->id, $feature);
	}

}

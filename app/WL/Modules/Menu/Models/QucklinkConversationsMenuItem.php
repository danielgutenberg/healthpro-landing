<?php namespace WL\Modules\Menu\Models;

use WL\Modules\Conversation\Facades\ConversationService;
use WL\Modules\Profile\Facades\ProfileService;

class QuicklinkConversationsMenuItem extends BasicMenuItem
{

	/**
	 * Render item .
	 *
	 * @param null $template
	 * @param bool $asRecursive
	 * @return null
	 */
	public function render($template, $asRecursive = true)
	{
		/** Is current profile / user authorized ? */
		if (!$this->isAuthorized($this))
			return false;

		/** Check if is visible */
		if (!$this->isVisible())
			return false;

		// we don't want to show conversations quicklink on the conversations page
		if ($this->isActive())
			return false;

		$patterns = $this->getPatterns();

		$template = str_replace("%content%", "<span>%content%</span>" . $this->getNotificationHtml(), $template);

		array_walk($patterns, function ($pattern, $index) use (&$template) {
			$template = preg_replace(sprintf('/%s/', $index), $pattern, $template);
		});

		// we don't have any submenus here
		$template = str_replace('%submenu%', '', $template);

		return $template;
	}

	private function getNotificationHtml()
	{
		$unreadConversations = ConversationService::getUnreadConversationListByProfileId(ProfileService::getCurrentProfileId());
		$count = count($unreadConversations);

		if ($count > 5)
			$count = '5+';

		# append notification if there are conversations with unread messages
		return ($count) ? '<i>' . $count . '</i>' : '';
	}
}

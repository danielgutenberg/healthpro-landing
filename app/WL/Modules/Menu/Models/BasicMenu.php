<?php

namespace WL\Modules\Menu\Models;

use Iterator;
use WL\Modules\Menu\Exceptions\MenuException;
use WL\Modules\Menu\Services\MenuSettingsServiceInterface;

class BasicMenu implements Iterator, \Countable
{
	private $menuItems;

	private $options = array();

    protected $defaultTemplate;

	function __construct(array $menuItems = [])
	{
		$this->menuItems = $menuItems;
	}

	public function __toString() {
		return $this->render();
	}

    /**
     * Render item .
     *
     * @param null $template
     * @param bool $asRecursive
     * @return string
     */
	public function render($template = null, $asRecursive = true) {
        if( is_null($template) )
            $template = $this->getTemplate();

		$class = !empty($this->get('classes')) ? 'class="'.$this->get('classes').'"': '';

		$html = "<ul {$this->get('attr')} {$class}>";
		foreach ($this as $item) {
			$html .= $item->render($template, $asRecursive);
		}

		$html .= "</ul>\n";

		return $html;
	}

	/**
	 * @return mixed
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * @param mixed $options
	 */
	public function setOptions($options)
	{
        $mergedOptions = array_merge($this->getOptions(), $options);

		if( isset($mergedOptions['template']) ) {
			$template = array_pull($mergedOptions, 'template');
			$this->setTemplate($template);
		}

		$this->options = $mergedOptions;
	}


	/**
	 * Gets menu items
	 * @return array
	 */
	public function getItems()
	{
		return $this->menuItems;
	}

	/**
	 * Sets menu items
	 * @param array $menuItems
	 */
	public function setItems($menuItems)
	{
		$this->menuItems = $menuItems;
	}

	/**
	 * Remove all items .
	 *
	 * @return $this
	 */
	public function removeItems() {
		$this->menuItems = [];

		return $this;
	}


	/**
	 * Sets the given property
	 * @param $property
	 * @param $value
	 * @return $this
	 * @throws MenuException
	 */
	public function set($property, $value)
	{
		if (!is_string($property) || empty($property))
			throw new MenuException(_('Invalid property. It must be string'));

		$this->options[$property] = $value;

		return $this;
	}

	/**
	 * Returns the value of the given property
	 * @param $property
	 * @return null
	 * @throws MenuException
	 */
	public function get($property)
	{
		if (!is_string($property) || empty($property))
			throw new MenuException(_('Invalid property. It must be string'));

		if( isset($this->options[$property]) && $value = $this->options[$property] )
			return $value;

		return null;
	}


    /**
     * Set template .
     *
     * @param string $template
     * @return $this
     * @throws MenuException
     */
    public function setTemplate($template) {
        if (!is_string($template)) {
            throw new MenuException(_('Invalid argument: $template must be a string '));
        }

        $this->defaultTemplate = $template;

        return $this;
    }

    /**
     * Get template .
     *
     * @return string
     */
    public function getTemplate() {
        if( $template = $this->defaultTemplate )
            return $template;

        $template = app(MenuSettingsServiceInterface::class)->getDefaultTemplate();

        $this->setTemplate($template);

        return $template;
    }


	/**
	 * (PHP 5 &gt;= 5.1.0)<br/>
	 * Count elements of an object
	 * @link http://php.net/manual/en/countable.count.php
	 * @return int The custom count as an integer.
	 * The return value is cast to an integer.
	 */
	public function count() {
		return count($this->menuItems);
	}

	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Return the current element
	 * @link http://php.net/manual/en/iterator.current.php
	 * @return mixed Can return any type.
	 */
	public function current() {
		return current($this->menuItems);
	}

	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Move forward to next element
	 * @link http://php.net/manual/en/iterator.next.php
	 * @return void Any returned value is ignored.
	 */
	public function next() {
		next($this->menuItems);
	}

	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Return the key of the current element
	 * @link http://php.net/manual/en/iterator.key.php
	 * @return mixed scalar on success, or null on failure.
	 */
	public function key() {
		return key($this->menuItems);
	}

	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Checks if current position is valid
	 * @link http://php.net/manual/en/iterator.valid.php
	 * @return boolean The return value will be casted to boolean and then evaluated.
	 * Returns true on success or false on failure.
	 */
	public function valid() {
		return current($this->menuItems) != false;
	}

	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Rewind the Iterator to the first element
	 * @link http://php.net/manual/en/iterator.rewind.php
	 * @return void Any returned value is ignored.
	 */
	public function rewind() {
		reset($this->menuItems);
	}
}

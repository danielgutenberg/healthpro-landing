<?php namespace WL\Modules\Menu\Security;

use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Modules\Provider\ValueObjects\ProviderFeatures;
use WL\Security\Facades\AuthorizationUtils;
use WL\Modules\Profile\Facades\ProfileService;

trait SecurityTrait {

	protected static $currentProfileType;

	/**
	 * Check if current profile is authorized to render that page .
	 *
	 * @param $page
	 * @return bool
	 */
	public function isAuthorized($page) {
		if ($this->isRolesPermitted($page->roles) &&
			$this->isProfilesPermitted($this->getCurrentProfileType(), $page->profile_types)
		)
			return true;

		return false;
	}

	public function isRolesPermitted($roles) {
		if (empty($roles))
			return true;

		return AuthorizationUtils::inRoles($roles);
	}

	public function isProfilesPermitted($currentProfile, $profiles) {
		$currentProfile = (array)$currentProfile;

		$result = false;
		if (empty($profiles))
			return true;

		if (array_intersect($currentProfile, $profiles))
			$result = true;

		return $result;
	}

	/**
	 * Set current profile .
	 *
	 */
	public function setCurrentProfileType() {
		$profile = ProfileService::getCurrentProfile();

		$result  = false;

		if ($profile) {
			if ($profile->type == ModelProfile::CLIENT_PROFILE_TYPE) {
				$result = $profile->type;
			} else {
				$features = ProviderMembershipService::getProviderFeatures($profile->id);
				if (in_array(ProviderFeatures::GIFT_CERTIFICATE, $features)) {
					$result = 'pro';
				} else {
					$result = 'basic';
				}
			}
		}

		self::$currentProfileType = $result;
	}

	/**
	 * Get current profile .
	 *
	 * @return mixed
	 */
	public function getCurrentProfileType() {
		if(! isset(self::$currentProfileType))
			$this->setCurrentProfileType();

		return self::$currentProfileType;
	}
}

<?php namespace WL\Modules\Menu\ServiceProviders;

use Illuminate\Support\ServiceProvider;
use WL\Modules\Menu\Repositories\YamlMenuSettingsRepository;
use WL\Modules\Menu\Services\MenuSettingsSettingsService;
use WL\Modules\Menu\Services\MenuSettingsServiceInterface;

class MenuSettingsServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton(MenuSettingsServiceInterface::class, function ($app) {
			return new MenuSettingsSettingsService(new YamlMenuSettingsRepository($app['yaml']));
		});
	}
}

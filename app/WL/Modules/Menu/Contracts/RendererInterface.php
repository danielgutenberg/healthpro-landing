<?php namespace WL\Modules\Menu\Contracts;

interface RendererInterface
{

	/**
	 * Render menu .
	 *
	 * @param array $options
	 * @return mixed
	 */
	public function render($options = []);
}

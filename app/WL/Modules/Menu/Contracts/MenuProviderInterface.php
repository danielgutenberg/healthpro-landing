<?php namespace WL\Modules\Menu\Contracts;

use WL\Modules\Menu\Models\BasicMenu;

/**
 * Interface MenuProvider
 * @package WL\Modules\Menu\MenuProviders
 */
interface MenuProviderInterface
{
	/**
	 * Provide
	 * @param mixed|null $options options
	 * @return BasicMenu
	 */
	public function provide($options = null);
}

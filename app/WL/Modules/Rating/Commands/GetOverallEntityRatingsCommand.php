<?php namespace WL\Modules\Rating\Commands;

use WL\Modules\Rating\Services\RatingServiceInterface;


class GetOverallEntityRatingsCommand extends RatingBaseCommand
{
	const IDENTIFIER = 'rating.getOverallEntityRatingsCommand';

	protected $rules = [
		'entity_id' => 'required|integer',
		'entity_type' => 'required',
		'rating_type' => 'required|ratingtype',
	];

	public function validHandle(RatingServiceInterface $svc)
	{
		return $svc->getOverallRating(
			$this->inputData['entity_id'],
			$this->inputData['entity_type'],
			$this->inputData['rating_type']
		);
	}


}

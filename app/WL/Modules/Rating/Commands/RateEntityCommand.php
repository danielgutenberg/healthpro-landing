<?php namespace WL\Modules\Rating\Commands;

use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Rating\Services\RatingServiceInterface;

class RateEntityCommand extends RatingBaseCommand
{
	const IDENTIFIER = 'rating.rateEntityCommand';

	protected $rules = [
		'entity_id' => 'required|integer',
		'entity_type' => 'required',
		'rating_type' => 'sometimes|ratingtype',
		'rating_label' => 'required',
		'rating_value' => 'required',
	];

	public function validHandle(RatingServiceInterface $svc)
	{
		if (!isset($this->inputData['rating_type'])) {
			$this->inputData['rating_type'] = null;
		}

		$result = $svc->rate(
			$this->inputData['entity_id'],
			$this->inputData['entity_type'],
			$this->inputData['rating_type'],
			$this->inputData['rating_label'],
			$this->inputData['rating_value'],
			ProfileService::getCurrentProfileId()
		);

		return $result;
	}


}

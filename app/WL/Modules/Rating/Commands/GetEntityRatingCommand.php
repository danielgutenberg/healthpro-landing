<?php namespace WL\Modules\Rating\Commands;

use WL\Modules\Rating\Services\RatingServiceInterface;

class GetEntityRatingCommand extends RatingBaseCommand
{
	const IDENTIFIER = 'rating.getEntityRatingCommand';

	protected $rules = [
		'entity_id' => 'required|integer',
		'entity_type' => 'required',
		'rating_label' => 'required',
	];

	public function validHandle(RatingServiceInterface $svc)
	{
		return $svc->getRating(
			$this->inputData['entity_id'],
			$this->inputData['entity_type'],
			$this->inputData['rating_label']
		);
	}


}

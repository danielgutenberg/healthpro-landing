<?php namespace WL\Modules\Rating\Commands;

use WL\Modules\Rating\Services\RatingServiceInterface;


class GetEntityRatingsCommand extends RatingBaseCommand
{
	const IDENTIFIER = 'rating.getEntityRatingsCommand';

	protected $rules = [
		'entity_id' => 'required|integer',
		'entity_type' => 'required',
	];

	public function validHandle(RatingServiceInterface $svc)
	{

		$ratings = $svc->getEntityRatings(
			$this->inputData['entity_id'],
			$this->inputData['entity_type']
		);

		return $ratings;
	}


}

<?php namespace  WL\Modules\Rating\Commands;


use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\Rating\Facades\RatingService;

class GetRatingsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'rating.GetRatingsCommand';

	protected $elementId;
	protected $elementType;

	public function __construct($elementId, $elementType)
	{
		$this->elementId = $elementId;
		$this->elementType = $elementType;
	}

	public function handle()
	{
		return RatingService::getEntityRatings($this->elementId, $this->elementType);
	}

}

<?php namespace WL\Modules\Rating\Commands;

use Illuminate\Validation\Validator;
use WL\Modules\Rating\Models\Rating;

class RatingValidator extends Validator
{
	private function isArrayHasKeys($dataArray, $keys, $attribute = null, array $items = null, $msg = 'Invalid')
	{
		$ret = true;
		foreach ($keys as $key) {
			if (!isset($dataArray[$key]) || empty($dataArray[$key])) {
				if ($attribute != null && $items != null) {
					$tItems = array_merge($items, [$key]);
					$this->addInvalidMessage($attribute, $tItems, $msg);
				}
				$ret = false;
			}
		}

		return $ret;
	}

	private function addInvalidMessage($attribute, array $items, $msg = 'Invalid')
	{
		$this->messages->add($attribute . '-' . implode('-', $items), _($msg));
	}

	public function validateRatingtype($attribute, $value, $parameters)
	{
		$types = [Rating::TYPE_STAR, Rating::TYPE_THUMB, Rating::TYPE_LIKE];

		return !array_key_exists($value, $types);
	}

	public function validateRatings($attribute, $value, $parameters)
	{
		$valid = is_array($value);

		if ($valid) {
			$indx = 0;
			foreach ($value as $ratingData) {
				$valid = $valid && $this->isArrayHasKeys($ratingData, ['rating_label', 'rating_value'], $attribute, [$indx++]);
			}
		}
		return $valid;
	}
}

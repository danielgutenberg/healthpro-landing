<?php namespace WL\Modules\Rating\Commands;

use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Rating\Services\RatingServiceInterface;

class RateEntityRatingsCommand extends RatingBaseCommand
{
	const IDENTIFIER = 'rating.rateEntityRatingsCommand';

	protected $rules = [
		'entity_id' => 'required|integer',
		'entity_type' => 'required',
		'ratings' => 'required|ratings',
	];

	public function validHandle(RatingServiceInterface $svc)
	{
		$result = [];

		foreach ($this->inputData['ratings'] as $ratingData)
			$result[] = $svc->rate(
				$this->inputData['entity_id'],
				$this->inputData['entity_type'],
				null,
				$ratingData['rating_label'],
				$ratingData['rating_value'],
				ProfileService::getCurrentProfileId()
			);

		return $result;
	}


}

<?php namespace WL\Modules\Rating\Services;

use WL\Modules\Rating\Models\Rating;

class RatingCalculationLike extends RatingCalculationThumb
{
	static public function isValueValid($value)
	{
		return in_array($value, [0, 1]);
	}
}

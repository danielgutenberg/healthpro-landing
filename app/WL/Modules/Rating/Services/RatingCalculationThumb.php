<?php namespace WL\Modules\Rating\Services;

use WL\Modules\Rating\Models\Rating;

class RatingCalculationThumb implements RatingCalculationInterface
{
	static public function isValueValid($value)
	{
		return in_array($value, [-1, 0, 1]);
	}

	static public function refreshValue(Rating $rating)
	{
		$data = \DB::table('ratings_user_constrain')
			->select(\DB::raw('count(*) AS count, SUM(rating_value) AS value'))
			->where('rating_id', $rating->id)
			->first();

		$rating->count = $data->count;
		$rating->rating_value = $data->value;
		$rating->save();
	}
}

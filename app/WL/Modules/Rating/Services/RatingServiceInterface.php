<?php namespace WL\Modules\Rating\Services;
use WL\Modules\Rating\Models\Rating;

/**
 * Interface RatingServiceInterface
 * @package WL\Modules\Rating
 */
interface RatingServiceInterface
{
	/**
	 *
	 * Rate some entity
	 *
	 * @param $entityId
	 * @param $entityType
	 * @param $ratingType string Can be null if this label has config config.wl.rating.ratings
	 * @param $ratingLabel
	 * @param $ratingValue
	 * @param $profileId
	 * @return mixed
	 */
	public function rate($entityId, $entityType, $ratingType, $ratingLabel, $ratingValue, $profileId);


	/**
	 * Cancel rate
	 *
	 * @param $entityId
	 * @param $entityType
	 * @param $ratingType string Can be null if this label has config config.wl.rating.ratings
	 * @param $ratingLabel
	 * @param $profileId
	 * @return mixed
	 */
	public function unrate($entityId, $entityType, $ratingType, $ratingLabel, $profileId);

	/**
	 * Get  rating
	 *
	 * @param $entityId
	 * @param $entityType
	 * @param $ratingLabel
	 * @return Rating
	 */
	public function getRating($entityId, $entityType, $ratingLabel);

	/**
	 * Get overall rating for entity by type
	 * @param $entityId
	 * @param $entityType
	 * @param $type
	 * @return
	 */
	public function getOverallRating($entityId, $entityType, $type);

	/**
	 * Get ratings associated with entity
	 *
	 * @param $entityId
	 * @param $entityType
	 * @param null $type RatingType
	 * @return mixed
	 */
	public function getEntityRatings($entityId, $entityType, $type = null);

	/**
	 * Get sorted entities by rating
	 *
	 * @param $entityType
	 * @param $entityTableName
	 * @param $ratingLabel
	 * @param $page
	 * @param $perPage
	 * @param bool $sortDesc
	 * @return mixed
	 */
	public function getRatedEntities($entityType, $entityTableName, $ratingLabel, $page, $perPage, $sortDesc = true);

	/**
	 * Create new instance of rating
	 *
	 * @param  $entityId
	 * @param  $entityType
	 * @param  $ratingType
	 * @param  $ratingLabel
	 * @param  $ratingValue
	 * @return Rating
	 */
	public function getNewRating($entityId, $entityType, $ratingType, $ratingLabel, $ratingValue);

	/**
	 * Return rating value by profileId
	 * @param  $ratingId
	 * @param  $profileId
	 * @return mixed
	 */
	public function getRatingValueByProfileId($ratingId, $profileId);

}

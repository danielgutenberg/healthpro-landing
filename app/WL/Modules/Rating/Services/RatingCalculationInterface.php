<?php namespace WL\Modules\Rating\Services;

use WL\Modules\Rating\Models\Rating;
/**
 * Interface RatingCalculationInterface
 * @package WL\Modules\Rating
 */
interface RatingCalculationInterface
{
	/**
	 * Check is value allowed
	 *
	 * @param $value
	 * @return mixed
	 */
	static public function isValueValid($value);

	/**
	 * calculate result value
	 *
	 * @param $aggregatedValue
	 * @param $addValue
	 * @return mixed
	 */
	static public function refreshValue(Rating $rating);

}

<?php namespace WL\Modules\Rating\Services;

use WL\Modules\Rating\Models\Rating;
use WL\Modules\Rating\Repositories\RatingRepositoryInterface;
use WL\Yaml\Facades\Yaml;

class RatingService implements RatingServiceInterface
{
	static private $ratingConfig = null;

	static private $calculators = [
		Rating::TYPE_STAR => RatingCalculationStar::class,
		Rating::TYPE_THUMB => RatingCalculationThumb::class,
		Rating::TYPE_LIKE => RatingCalculationLike::class,
	];

	private function config()
	{
		if (static::$ratingConfig == null)
			static::$ratingConfig = Yaml::get('labels', 'wl.rating.ratings');

		return static::$ratingConfig;

	}

	private function getConfigLabelType($label)
	{
		if (array_key_exists($label, $this->config()))
			return $this->config()[$label]['type'];

		return null;
	}

	private function getConfigLabelName($label)
	{
		if (array_key_exists($label, $this->config()))
			return $this->config()[$label]['name'];

		return null;
	}

	public function __construct(RatingRepositoryInterface $ratingRepository)
	{
		$this->repo = $ratingRepository;
	}

	public function rate($entityId, $entityType, $ratingType, $ratingLabel, $ratingValue, $profileId)
	{
		if ($ratingType == null)
			$ratingType = $this->getConfigLabelType($ratingLabel);

		if (!array_key_exists($ratingType, self::$calculators))
			return false;


		$calc = self::$calculators[$ratingType];

		if (!$calc::isValueValid($ratingValue))
			return false;

		$rating = $this->repo->getRating($entityId, $entityType, $ratingLabel);
		if ($rating == null) {
			$rating = $this->repo->createNew([
				'entity_id' => $entityId,
				'entity_type' => $entityType,
				'rating_type' => $ratingType,
				'rating_label' => $ratingLabel,
				'rating_value' => $ratingValue,
			]);
//			//$rating = $this->getNewRating($entityId, $entityType, $ratingType, $ratingLabel, 0);
		}

		$this->repo->rate($rating, $profileId, $ratingValue);
		$calc::refreshValue($rating);
		return true;
	}

	public function unrate($entityId, $entityType, $ratingType, $ratingLabel, $profileId)
	{
		if ($ratingType == null)
			$ratingType = $this->getConfigLabelType($ratingLabel);

		if (!array_key_exists($ratingType, self::$calculators))
			return false;
		$calc = self::$calculators[$ratingType];

		$rating = $this->repo->getRating($entityId, $entityType, $ratingLabel);
		if (!$rating)
			return false;
		$this->repo->unrate($rating->id, $profileId);
		$calc::refreshValue($rating);
		return true;
	}

	private function loadRatingAdditions(&$rating)
	{
		if ($rating != null)
			$rating->name = $this->getConfigLabelName($rating->rating_label);
	}

	public function getRating($entityId, $entityType, $ratingLabel)
	{
		$rating = $this->repo->getRating($entityId, $entityType, $ratingLabel);
		$this->loadRatingAdditions($rating);
		return $rating;
	}

	public function getRatingById($id)
	{
		$rating = $this->repo->getById($id);
		$this->loadRatingAdditions($rating);
		return $rating;
	}

	public function getOverallRating($entityId, $entityType, $type)
	{
		return $this->repo->getOverallRating($entityId, $entityType, $type);
	}

	public function getEntityRatings($entityId, $entityType, $type = null)
	{
		$ratings = $this->repo->getEntityRatings($entityId, $entityType, $type);

		foreach ($ratings as &$rating) {
			$this->loadRatingAdditions($rating);
		}

		return $ratings;
	}

	public function getRatedEntities($entityType, $entityTableName, $ratingLabel, $page, $perPage, $sortDesc = true)
	{
		return $this->repo->getRatedEntities($entityType, $entityTableName, $ratingLabel, $page, $perPage, $sortDesc);
	}

	public function getNewRating($entityId, $entityType, $ratingType, $ratingLabel, $ratingValue)
	{
		$rating = $this->repo->createNew([
			'entity_id' => $entityId,
			'entity_type' => $entityType,
			'rating_type' => $ratingType,
			'rating_label' => $ratingLabel,
			'rating_value' => $ratingValue,
		]);

		$this->loadRatingAdditions($rating);
		return $rating;
	}

	public function getRatingValueByProfileId($ratingId, $profileId)
	{
		return $this->repo->getRatingValueByProfileId($ratingId, $profileId);
	}

	public function deleteRating($ratingId, $elementId = null, $elementType = null)
	{
		return $this->repo->deleteRating($ratingId, $elementId, $elementType);
	}

	public function getProfileRatings($profileId)
	{
		return $this->repo->getProfileRatings($profileId);
	}
}

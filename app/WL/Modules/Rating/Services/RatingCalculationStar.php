<?php namespace WL\Modules\Rating\Services;

use WL\Modules\Rating\Models\Rating;

class RatingCalculationStar implements  RatingCalculationInterface
{
	static public function isValueValid($value)
	{
		return $value<6 && $value>=0;
	}

	static public function refreshValue(Rating $rating)
	{
		$data = \DB::table('ratings_user_constrain')
			->select(\DB::raw('count(*) AS count, AVG(rating_value) AS avgValue'))
			->where('rating_id', $rating->id)
			->first();

		$rating->count = $data->count;
		$rating->rating_value = $data->avgValue;
		$rating->save();
	}
}

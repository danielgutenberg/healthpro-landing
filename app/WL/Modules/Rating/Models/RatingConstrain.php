<?php namespace WL\Modules\Rating\Models;

use WL\Models\ModelBase;

class RatingConstrain extends ModelBase
{
	protected $table = 'ratings_user_constrain';

	public function rating()
	{
		return $this->belongsTo(Rating::class);
	}

 }


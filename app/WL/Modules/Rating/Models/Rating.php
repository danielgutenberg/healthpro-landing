<?php namespace WL\Modules\Rating\Models;

use WL\Models\ModelBase;
use WL\Modules\Rating\Presenter\RatingPresenter;
use WL\Contracts\Presentable;
use WL\Models\PresentableTrait;

class Rating extends ModelBase implements Presentable
{
	use PresentableTrait;

	const TYPE_STAR  = 'star';
	const TYPE_THUMB = 'thumb';
	const TYPE_LIKE = 'like';

	protected $table = 'ratings';

	protected $presenter = RatingPresenter::class;

	public $countOfRatings = 1;

	public function constrains()
	{
		return $this->hasMany(RatingConstrain::class);
	}

}


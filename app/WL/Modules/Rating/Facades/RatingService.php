<?php namespace WL\Modules\Rating\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Rating\Services\RatingServiceInterface;

class RatingService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return RatingServiceInterface::class;
	}
}

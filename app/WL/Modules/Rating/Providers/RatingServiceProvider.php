<?php namespace WL\Modules\Rating\Providers;

use Cartalyst\Support\ServiceProvider;
use WL\Modules\Rating\Repositories\RatingRepositoryInterface;
use WL\Modules\Rating\Repositories\DbRatingRepository;
use WL\Modules\Rating\Services\RatingService;
use WL\Modules\Rating\Services\RatingServiceInterface;

class RatingServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(RatingRepositoryInterface::class, DbRatingRepository::class);
		$this->app->bind(RatingServiceInterface::class, RatingService::class);
	}

}

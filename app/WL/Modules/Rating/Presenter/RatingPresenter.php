<?php namespace WL\Modules\Rating\Presenter;

use WL\Presenters\Presenter;

/**
 * Here will be declared all presenter funcs which works by current object ..
 *
 * Class ProfilePresenter
 * @package WL\Modules\Profile
 */
class RatingPresenter extends Presenter
{
	public function valueToWord()
	{
		return str_replace('.', '_', round( $this->entity->rating_value * 2) / 2);
	}
}

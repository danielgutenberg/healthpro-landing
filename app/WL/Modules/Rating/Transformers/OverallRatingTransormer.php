<?php namespace WL\Modules\Rating\Transformers;

use WL\Transformers\Transformer;

class OverallRatingTransformer extends Transformer
{
	public function transform($item)
	{
		return [
			'rating_counts' => isset($item->countOfRatings) ? (int)$item->countOfRatings : null,
			'rating_value' => round($item->rating_value, 1),
			'rating_type'  => $item->rating_type,
			'rating_name'  => $item->name,
			'rating_label' => $item->rating_label,
		];
	}
}

<?php namespace WL\Modules\Rating\Transformers;

use WL\Transformers\Transformer;

class RatingTransformer extends Transformer
{
	public function transform($item)
	{
		return [
			'id'            => $item->id,
			'entity_id'     => $item->entity_id,
			'entity_type'   => $item->entity_type,
			'rating_type'   => $item->rating_type,
			'rating_label'  => $item->rating_label,
			'rating_value'  => round($item->rating_value, 1),
			'rating_name'   => isset($item->name) ? $item->name : null,
			'rating_counts' => isset($item->countOfRatings) ? (int)$item->countOfRatings : null,
		];
	}
}

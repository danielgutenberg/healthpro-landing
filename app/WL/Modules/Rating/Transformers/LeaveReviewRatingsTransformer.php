<?php namespace WL\Modules\Rating\Transformers;

use Illuminate\Support\Collection;
use WL\Transformers\Transformer;
use WL\Yaml\Facades\Yaml;

class LeaveReviewRatingsTransformer extends Transformer
{
	public function transform($overallRatings)
	{
		$ratingsConfig = Yaml::parse('wl.rating.ratings');
		$ratings = [];
		foreach($ratingsConfig['labels'] as $key => $label) {
			$ratingValue = $this->findRating($overallRatings, $key);
			$ratings[$key] = [
				'id' 		=> $key,
				'label'		=> $label['name'],
				'values'	=> $ratingsConfig['ratings'],
				'rating'	=> $ratingValue,
			];
		}

		return $ratings;
	}

	private function findRating($overallRatings, $key)
	{
		$ratingValue = null;

		if (is_array($overallRatings)) {
			$_ratings = array_filter($overallRatings, function($rating) use(&$key) {
				return $key == $rating->rating_label;
			});

			if (count($_ratings)) {
				sort($_ratings); // reset keys
				$ratingValue = $_ratings[0]->rating_value;
			}

		} else if ($overallRatings instanceof Collection) {
			$_ratings = $overallRatings->filter(function($rating) use(&$key) {
				return $key == $rating->rating_label;
			});

			if ($_ratings->count()) {
				$ratingValue = $_ratings->first()->rating_value;
			}
		}

		return $ratingValue;
	}
}

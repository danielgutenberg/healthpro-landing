<?php namespace WL\Modules\Rating\Repositories;

use Illuminate\Support\Facades\DB;
use WL\Modules\Rating\Models\Rating;
use WL\Modules\Rating\Models\RatingConstrain;
use WL\Repositories\DbRepository;

class DbRatingRepository extends DbRepository implements RatingRepositoryInterface
{

	public function __construct()
	{
		$this->model = new Rating();
	}

	private function setConstrain($rating, $profileId, $value)
	{
		if ($rc = $rating->constrains()->where('profile_id', $profileId)->first()) {
			$rc->update(['rating_value' => $value]);
			return $rc;
		}

		return $rating->constrains()->save(new RatingConstrain([
			'profile_id' => $profileId,
			'rating_value' => $value,
		]));
	}

	public function rate($rating, $profileId, $value)
	{
		if (!isset($rating->id)) {
			$result = $rating->save();
			$result = $result && $this->setConstrain($rating, $profileId, $value);
		} else {
			$result = $this->setConstrain($rating, $profileId, $value);
			$result = $result && $rating->save();
		}
		return $result;
	}

	public function unrate($ratingId, $profileId)
	{
		$affectedRows = RatingConstrain::where('rating_id', $ratingId)
			->where('profile_id', $profileId)
			->delete();

		return ($affectedRows > 0);
	}

	public function hasEntityVoted($entityId, $entityType, $ratingLabel, $profileId)
	{
		$result = Rating::where('entity_id', $entityId)
			->where('entity_type', $entityType)
			->where('rating_label', $ratingLabel)
			->join('ratings_user_constrain', 'ratings_user_constrain.rating_id', '=', 'ratings.id')
			->where('ratings_user_constrain.profile_id', $profileId)
			->count();

		return $result > 0;
	}

	public function getRating($entityId, $entityType, $ratingLabel)
	{
		return Rating::where('entity_id', $entityId)
			->where('entity_type', $entityType)
			->where('rating_label', $ratingLabel)->first();
	}

	public function getOverallRating($entityId, $entityType, $type)
	{
		$data = \DB::table('ratings')
			->distinct()
			->select(\DB::raw('SUM(ratings.count) AS count, AVG(rating_value) AS avgValue'))
			->where('entity_id', $entityId)
			->where('entity_type', $entityType)
			->where('rating_type', $type)
			->first();

		$rating = $this->createNew([
			'rating_type' => $type,
			'rating_value' => $data->avgValue,
		]);
		$rating->countOfRatings = $data->count;
		return $rating;
	}

	public function getEntityRatings($entityId, $entityType, $type = null)
	{
		$query = Rating::where('entity_id', $entityId)
			->where('entity_type', $entityType);

		if ($type)
			$query->where('rating_type', $type);

		return $query->get();
	}

	public function getRatedEntities($entityType, $entityTableName, $ratingLabel, $page, $perPage, $sortDesc = true)
	{
		$result = DB::table('ratings')
			->select($entityTableName . '.*')
			->where('entity_type', $entityType)
			->where('rating_label', $ratingLabel)
			->join($entityTableName, $entityTableName . '.id', '=', 'ratings.entity_id')
			->orderBy('ratings.rating_value', $sortDesc ? 'desc' : 'asc')
			->get();

		return $result;
	}

	public function getRatingValueByProfileId($ratingId, $profileId)
	{
		$data = \DB::table('ratings_user_constrain')
			->where('rating_id', $ratingId)
			->where('profile_id', $profileId)
			->first();

		return ($data)
			? $data->rating_value
			: null;
	}

	public function deleteRating($ratingId, $elementId = null, $elementType = null)
	{
		$query = Rating::where('id', $ratingId);
		if ($elementId || $elementType) {
			$query->where('entity_id', $elementId)
				->where('entity_type', $elementType);
		}
		$affectedRows = $query->delete();
		return ($affectedRows > 0);
	}

	public function getProfileRatings($profileId){
		$result = DB::table('ratings_user_constrain')
			->join('ratings','ratings.id','=','ratings_user_constrain.rating_id')
			->where('ratings_user_constrain.profile_id',$profileId)
			->get();
		return $result;
	}

}

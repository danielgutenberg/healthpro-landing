<?php namespace WL\Modules\Rating\Repositories;

use WL\Modules\Rating\Models\Rating;
use WL\Repositories\Repository;

/**
 * Interface RatingRepositoryInterface
 * @package WL\Modules\Rating
 */
interface RatingRepositoryInterface extends Repository
{
	/**
	 * Store Value
	 *
	 * @param Rating $rating
	 * @param int $profileId
	 * @param float $value historical saving
	 * @return mixed
	 */
	public function rate($rating, $profileId, $value);

	/**
	 * indicates when user already voted
	 *
	 * @param $entityId
	 * @param $entityType
	 * @param $ratingLabel
	 * @param $profileId
	 * @return boolean
	 */
	public function hasEntityVoted($entityId, $entityType, $ratingLabel, $profileId);

	/**
	 * Get Rating
	 *
	 * @param $entityId
	 * @param $entityType
	 * @param $ratingLabel
	 * @return Rating
	 */
	public function getRating($entityId, $entityType, $ratingLabel);

	/**
	 * Get overall rating by entity and rating type
	 * @param  $entityId
	 * @param  $entityType
	 * @param  $type
	 * @return mixed
	 */
	public function getOverallRating($entityId, $entityType, $type);


	/**
	 * Get Entity Ratings
	 * @param $entityId
	 * @param $entityType
	 * @param null $type Rating Type
	 * @return mixed
	 */
	public function getEntityRatings($entityId, $entityType, $type = null);

	/**
	 * Receive entities sorted by rating
	 *
	 * @param $entityType
	 * @param $entityTableName
	 * @param $ratingLabel
	 * @param $page
	 * @param $perPage
	 * @param bool $sortDesc
	 * @return mixed
	 */
	public function getRatedEntities($entityType, $entityTableName, $ratingLabel, $page, $perPage, $sortDesc = true);

	/**
	 * Return rating value by profileId
	 * @param  $ratingId
	 * @param  $profileId
	 * @return mixed
	 */
	public function getRatingValueByProfileId($ratingId, $profileId);
}

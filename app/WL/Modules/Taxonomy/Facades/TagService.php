<?php namespace WL\Modules\Taxonomy\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Taxonomy\Services\TagServiceInterface;

class TagService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return TagServiceInterface::class;
	}
}

<?php namespace WL\Modules\Taxonomy\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Taxonomy\Services\TaxonomyServiceInterface;

class TaxonomyService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return TaxonomyServiceInterface::class;
	}
}

<?php namespace WL\Modules\Taxonomy\Repositories;

/**
 * Interface TaxonomyRepositoryInterface
 * @package App\Taxonomy
 */
interface TaxonomyRepositoryInterface
{

	/**
	 * @param array $taxonomies
	 * @return mixed
	 */
	function bulkAddTaxonomies(array &$taxonomies);

	/**
	 *
	 * Returns count of tags belongs to taxonomy
	 *
	 * @param $taxonomyId
	 * @return mixed
	 */
	function getTaxonomyTagsCount($taxonomyId);


	/**
	 * @return mixed
	 */
	function getTaxonomies();

	/**
	 * @param $name
	 * @return mixed
	 */
	function getTaxonomyByName($name);

}

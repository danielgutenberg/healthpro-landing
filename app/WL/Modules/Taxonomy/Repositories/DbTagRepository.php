<?php namespace WL\Modules\Taxonomy\Repositories;

use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use WL\Modules\Taxonomy\Models\Tag;
use WL\Repositories\DbRepository;

class DbTagRepository extends DbRepository implements TagRepositoryInterface
{

	protected $modelClassName = Tag::class;

	public function __construct()
	{
		$this->model = new Tag();
	}

	function getByName($name, $taxonomyId, $isNameSlug = false)
	{
		return Tag::where('taxonomy_id', $taxonomyId)->where(($isNameSlug ? 'slug' : 'name'), $name)->first();
	}

	function getBySlug($slug, $taxonomyId, $parentId = null)
	{
		return Tag::where('taxonomy_id', $taxonomyId)
			->where('slug', $slug)
			->where('parent_id', $parentId)
			->first();
	}

	function taxonomyTagCounts($taxonomyId, $entityType)
	{
		$query = DB::table('tag_relations')
			->join('tags', 'tags.id', '=', 'tag_relations.tag_id')
			->where('tags.taxonomy_id', '=', $taxonomyId)
			->select(DB::raw('count(*) as used_count'), 'tags.*')
			->groupBy('tags.id');

		if ($entityType != null) {
			$query->where('taggable_type', $entityType);
		}

		return $query->get();
	}

	function tagCounts(array $tagIds, $entityType = null)
	{
		if (empty($tagIds)) {
			return [];
		}

		$query = DB::table('tag_relations')
			->join('tags', 'tags.id', '=', 'tag_relations.tag_id')
			->whereIn('tags.id', $tagIds)
			->select(DB::raw('count(*) as usage_count'), 'tags.*')
			->groupBy('tags.id')
			->orderBy('usage_count', 'DESC');

		if ($entityType != null) {
			$query->where('taggable_type', $entityType);
		}

		return $query->get();
	}

	function sortByCount($taxonomyType)
	{
		$query = DB::table('tag_relations')
			->select(DB::raw('count(*) as used_count'), 'tags.*')
			->join('tags', 'tags.id', '=', 'tag_relations.tag_id')
			->join('taxonomies', 'taxonomies.id', '=', 'tags.taxonomy_id')
			->where('taxonomies.name', $taxonomyType)
			->groupBy('tags.id')
			->orderBy('used_count', 'DESC');

		return $query->get();
	}

	function getTagsUsageGreaterThanNumber($taxonomyId, $entityType, $number)
	{
		$query = DB::table('tag_relations')
			->select(DB::raw('count(*) as used_count'), 'tags.*')
			->join('tags', 'tags.id', '=', 'tag_relations.tag_id')
			->where('tags.taxonomy_id', '=', $taxonomyId)
			->having('used_count', '>', $number)
			->groupBy('tags.id');

		if ($entityType != null) {
			$query->where('taggable_type', $entityType);
		}

		return $query->get();
	}

	function bulkRemove(array $tags)
	{
		DB::beginTransaction();
		foreach ($tags as $tag) {
			$tag->delete();
		}
		DB::commit();
	}

	function bulkSave(array $tagArray, $allowNewTags = true)
	{
		$tags = [];
		foreach ($tagArray as $tag) {
			$tagE = Tag::where('name', $tag->name)
				->where('taxonomy_id', $tag->taxonomy_id)
				->where('parent_id', $tag->parent_id)
				->first();

			if ($tagE != null) {
				$tags[] = $tagE;
			} else {
				if ($allowNewTags) {
					try {
						$tag->save();
						$tags[] = $tag;
					} catch (QueryException $ex) {

					}
				}
			}
		}

		return $tags;
	}

	function getTagsByNames(array $tags, $taxonomyId, $parentId)
	{

		return Tag::where('taxonomy_id', $taxonomyId)
			->where('parent_id', $parentId)
			->whereIn('name', $tags)
			->get();

	}

	function bulkSafeSave(array $tagArray)
	{
		DB::beginTransaction();
		foreach ($tagArray as $tag) {
			$tag->save();
		}
		DB::commit();
	}

	function bulkSafeAssociation(array $tagIds, $entityId, $entityType)
	{
		if (count($tagIds) <= 0) {
			return;
		}
		$data = [];

		$tagIds = array_unique($tagIds);
		foreach ($tagIds as $tagId) {
			$data[] = [
				'tag_id' => $tagId,
				'taggable_id' => $entityId,
				'taggable_type' => $entityType
			];
		}

		DB::beginTransaction();
		DB::table('tag_relations')->insert($data);
		DB::commit();
	}

	function bulkRemoveAssociation(array $tagNameArray, $entityId, $entityType, $taxonomyId)
	{
		$query = DB::table('tag_relations')
			->where('taggable_id', $entityId)
			->where('taggable_type', $entityType)
			->join('tags', function ($join) use ($tagNameArray, $taxonomyId) {
				$join->on('tags.id', '=', 'tag_relations.tag_id')
					->where('taxonomy_id', '=', $taxonomyId);
			})
			->whereIn('tags.name', $tagNameArray);

		return $query->delete();
	}

	function removeAllAssociations($entityId, $entityType, $taxonomyId = null)
	{
		$query = DB::table('tag_relations')
			->where('taggable_id', $entityId)
			->where('taggable_type', $entityType);


		if ($taxonomyId != null) {
			$query->join('tags', function ($join) use ($taxonomyId) {
				$join->on('tags.id', '=', 'tag_relations.tag_id')
					->where('taxonomy_id', '=', $taxonomyId);

			});
		}

		return $query->delete();
	}

	private function skip($query, $page, $perPage)
	{
		return $query->skip($page * $perPage)->take($perPage);
	}

	function getEntitiesByTags($tags, $entityTableName, array $options)
	{
		//sets up defaults if not provided
		$options['tagsField'] = !empty($options['tagsField']) ? $options['tagsField'] : 'name';
		$options['withAnyTag'] = isset($options['withAnyTag']) ? $options['withAnyTag'] : false;
		$options['taxonomyId'] = isset($options['taxonomyId']) ? $options['taxonomyId'] : null;
		$options['page'] = !empty($options['page']) ? $options['page'] : 0;
		$options['perPage'] = !empty($options['perPage']) ? $options['perPage'] : 1000;
		$options['filters'] = isset($options['filters']) ? $options['filters'] : null;
		$options['sortField'] = !empty($options['sortField']) ? $options['sortField'] : 'updated_at';
		$options['sortDirection'] = !empty($options['sortDirection']) ? $options['sortDirection'] : 'DESC';

		$taxonomyId = $options['taxonomyId'];
		$tagsArray = [];

		if (isset($tags[0])) {
			if (is_object($tags[0])) {
				foreach ($tags as $tag) {
					array_push($tagsArray, $tag->$options['tagsField']);
				}
			} elseif (is_array($tags[0])) {
				foreach ($tags as $tag) {
					array_push($tagsArray, $tag[$options['tagsField']]);
				}
			} else {
				$tagsArray = $tags;
			}
		}

		$query = DB::table($entityTableName)
			->select($entityTableName . '.*', DB::raw('count(*)'))
			->join('tag_relations', $entityTableName . '.id', '=', 'tag_relations.taggable_id')
			->join('tags', 'tags.id', '=', 'tag_relations.tag_id');

		if ($taxonomyId) {
			if (is_array($taxonomyId)) {
				$query->whereIn('tags.taxonomy_id', $taxonomyId);
			} else {
				$query->where('tags.taxonomy_id', $taxonomyId);
			}
		}


		if ($options['filters'] != null) {
			foreach ($options['filters'] as $filter) {
				$query->where($filter[0], $filter[1], $filter[2]);
			}
		}

		$query->whereIn('tags.' . $options['tagsField'], $tagsArray)
			->leftJoin('tags as jtags', function ($join) use ($taxonomyId) {

				$join->on('tags.id', '=', 'jtags.parent_id')
					->where('jtags.is_alias', '=', true);

				if ($taxonomyId) {
					if (is_array($taxonomyId)) {
						foreach ($taxonomyId as $id) {
							$join->where('jtags.taxonomy_id', '=', $id);
						}
					} else {
						$join->where('jtags.taxonomy_id', '=', $taxonomyId);
					}
				}

				$join->orOn('tags.parent_id', '=', 'jtags.parent_id')
					->where('jtags.is_alias', '=', true);

				if ($taxonomyId) {
					if (is_array($taxonomyId)) {
						foreach ($taxonomyId as $id) {
							$join->where('jtags.taxonomy_id', '=', $id);
						}
					} else {
						$join->where('jtags.taxonomy_id', '=', $taxonomyId);
					}
				}

			})
			->groupBy($entityTableName . '.id')
			->orderBy(DB::raw('`' . $entityTableName . '`.`' . $options['sortField'] . '`'), $options['sortDirection']);

		if (!$options['withAnyTag']) {
			$query->having(DB::raw('count(`' . $entityTableName . '`.`id`)'), '=', count($tagsArray));
		}

		$countQuery = DB::table(DB::raw("({$query->toSql()}) as sub"))
			->mergeBindings($query)
			->count();

		return [$countQuery, $this->skip($query, $options['page'], $options['perPage'])->get()];
	}

	function getEntitiesByTagIds(
		array $tagIdsArray,
		$entityTableName,
		$page,
		$perPage,
		$additionalFilter = null,
		$sortField = null
	) {
		$query = DB::table($entityTableName)
			->select($entityTableName . '.*', DB::raw('count(*)'))
			->join('tag_relations', $entityTableName . '.id', '=', 'tag_relations.taggable_id')
			->join('tags', 'tags.id', '=', 'tag_relations.tag_id');

		if ($additionalFilter != null) {
			foreach ($additionalFilter as $filter) {
				$query->where($filter[0], $filter[1], $filter[2]);
			}
		}

		$query->whereIn('tags.id', $tagIdsArray)
			->leftJoin('tags as jtags', function ($join) {

				$join->on('tags.id', '=', 'jtags.parent_id')
					->where('jtags.is_alias', '=', true);

				$join->orOn('tags.parent_id', '=', 'jtags.parent_id')
					->where('jtags.is_alias', '=', true);

			})
			->groupBy($entityTableName . '.id')
			->having(DB::raw('count(`' . $entityTableName . '`.`id`)'), '=', count($tagIdsArray))
			->orderBy(DB::raw('`' . $entityTableName . '`.`updated_at`'), 'DESC');


		$countQuery = DB::table(DB::raw("({$query->toSql()}) as sub"))
			->mergeBindings($query)
			->count();

		return [$countQuery, $this->skip($query, $page, $perPage)->get()];
	}

	function getAssociatedTags($entityIds, $entityType, $taxonomyId = null)
	{
		$entityIds = (array)$entityIds;

		$result = DB::table('tag_relations')
			->select('tags.*')
			->whereIn('taggable_id', $entityIds)
			->where('taggable_type', $entityType);

		if ($taxonomyId == null) {
			$result->join('tags', 'tags.id', '=', 'tag_relations.tag_id');
		} else {
			$result->join('tags', function ($join) use ($taxonomyId) {
				$join->on('tags.id', '=', 'tag_relations.tag_id')
					->where('taxonomy_id', '=', $taxonomyId);
			});
		}

		return Tag::hydrate($result->get());
	}


	function getTaxonomyTags($taxonomyId, $loadAllTags = false)
	{
		$query = Tag::where('taxonomy_id', $taxonomyId);
		if (!$loadAllTags) {
			$query->where('parent_id', null);
		}

		return $query->get();
	}

	function getTaxonomyTagsBySlug($slug, $isName = false, $parentId = null)
	{
		$results = DB::table('tags')
			->join('taxonomies', 'taxonomies.id', '=', 'tags.taxonomy_id')
			->where('taxonomies.' . ($isName ? 'name' : 'slug'), $slug)
			->where('tags.parent_id', $parentId)
			->select('tags.*')
			->orderBy('tags.id')
			->get();

		return Tag::hydrate($results);
	}

	public function getTaxonomyTagsForEntityType($taxonomyId, $entityType, $alsoTaggedBy = null)
	{
		$query = Tag::where('taxonomy_id', $taxonomyId)
			->join('tag_relations AS tags_relations_outer', 'tags_relations_outer.tag_id', '=', 'tags.id')
			->where('tags_relations_outer.taggable_type', $entityType);

		if ($alsoTaggedBy) {
			$query->whereExists(function ($query) use ($alsoTaggedBy) {
				$query->select(DB::raw(1))
					->from('tags')
					->join('tag_relations', 'tag_relations.tag_id', '=', 'tags.id')
					->whereRaw('tag_relations.taggable_id = tags_relations_outer.taggable_id')
					->where('tags.slug', $alsoTaggedBy);
			});
		}

		return $query->get();
	}

	function getChildren($tagId, $isAliases = null)
	{
		$query = Tag::where('parent_id', $tagId);

		if ($isAliases !== null) {
			$query->where('is_alias', $isAliases);
		}

		return $query->get();
	}

	public function getParents(array $ids)
	{
		return DB::table('tags')
			->whereIn('id', $ids)
			->whereNotNull('parent_id')
			->distinct()
			->pluck('parent_id');
	}

	public function getTree($tagId, $isAliases = null, $result = null)
	{
		if (null === $result) {
			$result = new Collection();
		}

		$children = $this->getChildren($tagId, $isAliases);
		foreach ($children as $child) {
			$result = $this->getTree($child->id, $isAliases, $result);
		}

		return $result->merge($children);
	}

	public function getTagsByIds(array $ids)
	{
		return Tag::whereIn('id', $ids)
			->get();
	}

	function getByNameLike($name, $taxonomyId, $isNameSlug = false, $taggableTypes = null, $withChildren = false)
	{
		$searchField = $isNameSlug ? 'slug' : 'name';
		$query = DB::table('tags')
			->where('taxonomy_id', $taxonomyId)
			->whereRaw("$searchField sounds like ?", [$name])
			->orWhere($searchField, "rlike", $name)
			->select('tags.*')
			->distinct();
		if ($taggableTypes != null) {
			$query->join('tag_relations', 'tag_relations.tag_id', '=', 'tags.id')
				->whereIn('taggable_type', $taggableTypes);
		}

		$tags = Tag::hydrate($query->get());

		// a really boring impelementation of getting the children for the tags
		if ($withChildren) {
			foreach ($tags as $tag) {
				$tags = $tags->merge($this->getTree($tag->id));
			}
		}

		return $tags;

	}

}

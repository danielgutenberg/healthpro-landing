<?php namespace WL\Modules\Taxonomy\Repositories;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface TagRepositoryInterface
 * @package App\Tag
 */
interface TagRepositoryInterface
{
	/**
	 * Get Tag by Slug in Taxonomy
	 *
	 * @param $slug string
	 * @param $taxonomyId int
	 * @param $parentId
	 * @return Collection<Tag>
	 */
	function getBySlug($slug, $taxonomyId, $parentId = null);

	/**
	 *
	 * Get Tag by Name in Taxonomy
	 *
	 * @param $name
	 * @param $taxonomyId
	 * @param $isNameSlug
	 * @return mixed
	 */
	function getByName($name, $taxonomyId, $isNameSlug=false);

	/**
	 * Returns count of tag count of associations
	 *
	 * @param $taxonomyId
	 * @param null $entityType
	 * @return mixed
	 */
	function taxonomyTagCounts($taxonomyId, $entityType);

	/**
	 * Returns the tag usage count for each tag in $tagsIds
	 *
	 * @param array $tagIds
	 * @param null $entityType
	 * @return mixed
	 */
	function tagCounts(array $tagIds, $entityType = null);

	/**
	 *
	 * Bulk Remove tags
	 *
	 * @param array $tags
	 * @return mixed
	 */
	function bulkRemove(array $tags);

	/**
	 *
	 * Bulk save with returning ids into models
	 *
	 * @param array $tagArray
	 * @param bool $allowNewTags
	 * @return mixed
	 */
	function bulkSave(array $tagArray, $allowNewTags = true);

	/**
	 * @param array $tags
	 * @param $taxonomyId
	 * @param $parentId
	 * @return mixed
	 */
	function getTagsByNames(array $tags, $taxonomyId, $parentId);

	/**
	 *
	 * Bulk saving, no returning ids
	 *
	 * @param array $tagArray
	 * @return mixed
	 */
	function bulkSafeSave(array $tagArray);

	/**
	 *
	 * Associate tags to Entity, no matter if tags duplicates, duplication will eliminates
	 *
	 * @param array $tagIds
	 * @param $entityId
	 * @param $entityType
	 * @return mixed
	 */
	function bulkSafeAssociation(array $tagIds, $entityId, $entityType);

	/**
	 *
	 * Remove association from Entity
	 *
	 * @param array $tagArray
	 * @param $entityId
	 * @param $entityType
	 * @param $taxonomyId
	 * @return mixed
	 */
	function bulkRemoveAssociation(array $tagArray, $entityId, $entityType, $taxonomyId);

	/**
	 *
	 * Removing any association to Entity
	 *
	 * @param $entityId
	 * @param $entityType
	 * @param $taxonomyId
	 * @return mixed
	 */
	function removeAllAssociations($entityId, $entityType, $taxonomyId = null);

	/**
	 * @param array|Collection $tags Array of tagNames|tagSlugs|tagIds|
	 * @param string $entityTableName
	 * @param array $options ['withAnyTag'=>true|false,'tagsField'=>'id'|'slug'|'name','taxonomyId'=>int|null,'page'=>int|0,'perPage'=>int|1000,'filters'=>[ [column,'=','value], ['column2,'>',2] ]|null,'sortField'=>string|'updated_at','sortDirection'=>'ASC'|'DESC']
	 * @return array [count,items_array[]]
	 */
	function getEntitiesByTags( $tags, $entityTableName,array $options);

	/**
	 * Return paginated Entities which have chosen tags
	 *
	 * @param array $tagIdsArray
	 * @param $entityTableName
	 * @param $page
	 * @param $perPage
	 * @param null $additionalFilter
	 * @param null $sortField
	 * @return mixed
	 */
	function getEntitiesByTagIds(array $tagIdsArray, $entityTableName, $page, $perPage, $additionalFilter = null, $sortField = null);

	/**
	 *
	 * Return all tags belongs to Taxonomy
	 *
	 * @param $taxonomyId
	 * @param $loadAllTags
	 * @return mixed
	 */
	function getTaxonomyTags($taxonomyId, $loadAllTags = false);


	/**
	 * @param $slug
	 * @param bool $isName
	 * @param $parentId
	 * @return mixed
	 */
	function getTaxonomyTagsBySlug($slug, $isName = false, $parentId = null);

	/**
	 * Return all tags belonging to the given taxonomy id and associated with the given entity type - you can optionally pass in a tag slug that the entity must also be tagged by
	 *
	 * @param $taxonomyId
	 * @param $entityType
	 * @param null $alsoTaggedBy
	 * @return mixed
	 */
	public function getTaxonomyTagsForEntityType($taxonomyId, $entityType, $alsoTaggedBy = null);

	/**
	 * Returns all child tags
	 * @param $tagId
	 * @param $isAliases
	 * @return mixed
	 */
	function getChildren($tagId, $isAliases = null);

	/**
	 *
	 * Get associated tags with Entity
	 *
	 * @param $entityId
	 * @param $entityType
	 * @param null $taxonomyId
	 * @return mixed
	 */
	function getAssociatedTags($entityId, $entityType, $taxonomyId = null);

	/**
	 * Returns tags where usage count greater than $number
	 *
	 * @param $taxonomyId
	 * @param $entityType
	 * @param int $number
	 * @return mixed
	 */
	function getTagsUsageGreaterThanNumber($taxonomyId, $entityType, $number);

	/**
	 * Returns tags sorted by count
	 *
	 * @param $taxonomyType
	 * @return mixed
	 * @internal param $taxonomyId
	 */
	function sortByCount($taxonomyType);

	/**
	 * @param string $name
	 * @param int $taxonomyId
	 * @param bool $isNameSlug
	 * @param null $taggableTypes
	 * @param bool $withChildren
	 * @return mixed
     */
	public function getByNameLike($name, $taxonomyId, $isNameSlug = false, $taggableTypes = null, $withChildren = false);

}

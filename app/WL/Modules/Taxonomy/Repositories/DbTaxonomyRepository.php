<?php namespace WL\Modules\Taxonomy\Repositories;

use WL\Modules\Taxonomy\Models\Taxonomy;
use WL\Repositories\DbRepository;
use WL\Modules\Taxonomy\Models\Tag;

class DbTaxonomyRepository extends DbRepository implements TaxonomyRepositoryInterface
{
	protected $modelClassName = Taxonomy::class;

	public function __construct()
	{
		$this->model = new Taxonomy();
	}

	function bulkAddTaxonomies(array &$taxonomies)
	{
		foreach ($taxonomies as &$tax) {
			$tax->save();
		}
	}

	function getTaxonomyByName($name)
	{
		return Taxonomy::where('name',$name)->first();
	}

	function getTaxonomyTagsCount($taxonomyId)
	{
		return Tag::where('taxonomy_id', $taxonomyId)->count();
	}

	function getTaxonomies()
	{
		return Taxonomy::all();
	}

	public function getList()
	{
		return $this->model->all();
	}

	/**
	 * Delete taxonomy by id ..
	 *
	 * @param $taxonomy_id
	 * @return mixed
	 */
	public function deleteTaxonomy($taxonomy_id) {
		return Taxonomy::where('id', $taxonomy_id)
			->delete();
	}

}

<?php namespace WL\Modules\Taxonomy;


use WL\Modules\Taxonomy\Facades\TagService;
use WL\Modules\Taxonomy\Facades\TaxonomyService;
use WL\Security\Commands\AuthorizableCommand;


class LoadTaxonomyTagsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'taxonomy.loadTaxonomyTagsCommand';

	function __construct($taxonomySlug, $loadAllTags = true, $sortBy = null)
	{
		$this->taxonomySlug = $taxonomySlug;
		$this->loadAllTags = $loadAllTags;
		$this->sortBy = $sortBy;
	}

	public function handle()
	{
		$tax = TaxonomyService::getTaxonomyBySlug($this->taxonomySlug);

		$tags = TagService::getTaxonomyTags($tax->id, $this->loadAllTags);

		if($this->sortBy)
			$tags = $tags->sortBy($this->sortBy)->values();

		return $tags->all();

	}
}

<?php namespace WL\Modules\Taxonomy;


use WL\Modules\Taxonomy\Facades\TagService;
use WL\Modules\Taxonomy\Facades\TaxonomyService;
use WL\Security\Commands\AuthorizableCommand;

class LoadChildTagsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'taxonomy.loadChildTagsCommand';

	function __construct($taxonomySlug, $tagSlugs, $sortBy = null)
	{
		$this->taxonomySlug = $taxonomySlug;
		$this->tagSlugs = (array)$tagSlugs;
		$this->sortBy = $sortBy;
	}

	public function handle()
	{
		if (! ($taxonomy = TaxonomyService::getTaxonomyBySlug($this->taxonomySlug)) )
			return [];

		$tags = TagService::getChildTagsSlugTrain($this->tagSlugs, $taxonomy->id);

		if($this->sortBy)
			$tags = $tags->sortBy($this->sortBy)->values();

		return $tags;

	}
}

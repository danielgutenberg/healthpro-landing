<?php namespace WL\Modules\Taxonomy\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Models\ModelBase;
use WL\Modules\Taxonomy\Facades\TaxonomyService;
use WL\Modules\Taxonomy\Models\Tag;
use WL\Modules\Taxonomy\Models\Taxonomy;
use WL\Modules\Taxonomy\Repositories\TagRepositoryInterface;

class TagServiceImpl implements TagServiceInterface
{
	private $repo;

	public function __construct(TagRepositoryInterface $tagRepository)
	{
		$this->repo = $tagRepository;
	}

	private function makeTagsFromNames(array $names, $taxonomyId, $options = [])
	{
		$tags = [];
		foreach ($names as $tag) {
			$tags[] = new Tag(array_merge([
				'taxonomy_id' => $taxonomyId,
				'name' => $tag,
				'is_custom' => true,
			], $options));
		}
		return $tags;
	}

	function associateTagsWithEntity(ModelBase $entity, array $tagNameArray, $taxonomyId, $saveNewTags = true)
	{
		$tags = $this->repo->bulkSave($this->makeTagsFromNames($tagNameArray, $taxonomyId),
			TaxonomyService::isAllowedToAddTags($taxonomyId, true, count($tagNameArray)));

		return $this->repo->bulkSafeAssociation(array_map(function ($item) {
			return $item->id;
		}, $tags), $entity->id, get_class($entity));
	}

	function removeAssociateTagsWithEntity(ModelBase $entity, array $tagNameArray, $taxonomyId)
	{
		return $this->repo->bulkRemoveAssociation($tagNameArray, $entity->id, get_class($entity), $taxonomyId);
	}

	function explicitSetTagsWithEntity(ModelBase $entity, array $tagNameArray, $taxonomyId)
	{
		$this->repo->removeAllAssociations($entity->id, get_class($entity), $taxonomyId);
		return $this->associateTagsWithEntity($entity, $tagNameArray, $taxonomyId);
	}

	function explicitSetTagIdsWithEntity(ModelBase $entity, array $tagIds, $taxonomyId)
	{
		$this->repo->removeAllAssociations($entity->id, get_class($entity), $taxonomyId);
		return $this->repo->bulkSafeAssociation($tagIds, $entity->id, get_class($entity));
	}

	function getEntitiesByTags($tags, $entityTableName, $options, $taxonomies = null)
	{
		if ($taxonomies) $options['taxonomyID'] = $taxonomies;

		return $this->repo->getEntitiesByTags(
			$tags,
			$entityTableName,
			$options
		);
	}

	function getEntitiesByTagIds(array $tagIdsArray, $entityTableName, $page, $perPage, $additionalFilter = null, $sortField = null)
	{
		return $this->repo->getEntitiesByTagIds($tagIdsArray, $entityTableName, $page, $perPage, $additionalFilter);
	}

	function getEntityTags(ModelBase $entity)
	{
		return $this->repo->getAssociatedTags($entity->id, get_class($entity));
	}

	/**
	 * Get tags by array of id's by same entity_type ..
	 *
	 * @param array $ids
	 * @param $entity_type
	 * @return mixed
	 */
	public function getEntitiesTags(array $ids, $entity_type)
	{
		return $this->repo->getAssociatedTags($ids, $entity_type);
	}

	function getEntityTagsForTaxonomyId(ModelBase $entity, $taxonomyId)
	{
		return $this->repo->getAssociatedTags($entity->id, get_class($entity), $taxonomyId);
	}

	function getEntityTagsForTaxonomySlug(ModelBase $entity, $taxonomySlug)
	{
		$taxonomy = TaxonomyService::getTaxonomyBySlug($taxonomySlug);
		return ($taxonomy)
			? $this->getEntityTagsForTaxonomyId($entity, $taxonomy->id)
			: [];
	}

	public function getChildTagsByParentSlug($taxonomySlug, $tagSlug)
	{
		if (!($taxonomy = TaxonomyService::getTaxonomyBySlug($taxonomySlug)))
			return [];

		if (!($tag = $this->getTaxonomyTagByTagSlug($tagSlug, $taxonomy->id)))
			return [];

		return $this->getChildTags($tag->id);
	}

	function getTaxonomyTags($taxonomyId, $loadAllTags = false)
	{
		return $this->repo->getTaxonomyTags($taxonomyId, $loadAllTags);
	}

	function getTaxonomyTagByTagSlug($slug, $taxonomyId)
	{
		return $this->repo->getBySlug($slug, $taxonomyId);
	}

	function getTaxonomyTagsBySlug($slug, $isName = false)
	{
		return $this->repo->getTaxonomyTagsBySlug($slug, $isName);
	}

	public function getTaxonomyTagsForEntityType($taxonomyId, $entityType, $alsoTaggedBy = null)
	{
		return $this->repo->getTaxonomyTagsForEntityType($taxonomyId, $entityType, $alsoTaggedBy);
	}

	function getChildTags($tagId, $includeAliases = false)
	{
		if($includeAliases)
			return $this->repo->getChildren($tagId);
		else
			return $this->repo->getChildren($tagId, false);
	}

	function getAliases($tagId)
	{
		return $this->repo->getChildren($tagId, true);
	}

	function getChildTagsSlugTrain(array $tagTrainSlug, $taxonomyId)
	{
		$current = $this->getTaxonomyTagByTagSlug(array_shift($tagTrainSlug), $taxonomyId);
		if ($current) {
			foreach ($tagTrainSlug as $slug) {
				$current = $this->repo->getBySlug($slug, $taxonomyId, $current->id);
				if (!$current) {
					return [];
				}
			}

			return $this->repo->getChildren($current->id);
		}

		return [];
	}

	function addTagNames(array $tagNames, $taxonomyId, $parentTagId = null)
	{
		TaxonomyService::failIsTagAddNotAllowed($taxonomyId, true, count($tagNames));

		$tags = $this->repo->bulkSave($this->makeTagsFromNames($tagNames, $taxonomyId, ['parent_id' => $parentTagId]));

		return $tags;
	}

	function getTagsByNames(array $tagNames, $taxonomyId, $parentTagId = null)
	{
		return $this->repo->getTagsByNames($tagNames, $taxonomyId, $parentTagId);
	}

	function forceAddTagNames(array $tagNames, $taxonomyId, $options = [])
	{
		$tags = $this->repo->bulkSave($this->makeTagsFromNames($tagNames, $taxonomyId, $options));

		return $tags;
	}

	function addChildrenToTag($tagId, $taxonomyId, array $tagNames, $options = [])
	{
		try{
			$tag = $this->repo->getById($tagId);

			if(!$tag->is_alias){
				$tags = $this->makeTagsFromNames($tagNames, $taxonomyId, array_merge(['parent_id' => $tagId, 'is_alias' => false],$options));

			}else{
				$tags = $this->makeTagsFromNames($tagNames, $taxonomyId, array_merge(['parent_id' => $tag->parent_id, 'is_alias' => false],$options));
			}
			$tags = $this->repo->bulkSave($tags);
			return $tags;

		}catch(ModelNotFoundException $e){

		}
		return [];
	}

	function addAliasesToTag($tagId, $taxonomyId, array $aliasNames, $options = [])
	{
		try{
			$tag = $this->repo->getById($tagId);

			if(!$tag->is_alias){
				$tags = $this->makeTagsFromNames($aliasNames, $taxonomyId, array_merge(['parent_id' => $tagId, 'is_alias' => true],$options));

			}else{
				$tags = $this->makeTagsFromNames($aliasNames, $taxonomyId, array_merge(['parent_id' => $tag->parent_id, 'is_alias' => true],$options));
			}
			$tags = $this->repo->bulkSave($tags);
			return $tags;

		}catch(ModelNotFoundException $e){

		}
		return [];
	}

	function removeTags(array $tags)
	{
		return $this->repo->bulkRemove($tags);
	}

	function getByName($name, $taxonomyId, $isNameSlug = false)
	{
		return $this->repo->getByName($name, $taxonomyId, $isNameSlug);
	}


	function taxonomyTagCounts($taxonomyId, $entityType = null)
	{
		return $this->repo->taxonomyTagCounts($taxonomyId, $entityType);
	}

	function tagCounts(array $tagIds, $entityType = null)
	{
		return $this->repo->tagCounts($tagIds, $entityType);
	}


	function getTagsUsageGreaterThanNumber($taxonomyId, $entityType, $number = 0)
	{
		return $this->repo->getTagsUsageGreaterThanNumber($taxonomyId, $entityType, $number);
	}

	function sortByCount($taxanomoyType)
	{
		return $this->repo->sortByCount($taxanomoyType);
	}

	function removeAllEntityAssociations($entity, $taxonomyId)
	{
		$this->repo->removeAllAssociations($entity->id, get_class($entity), $taxonomyId);
	}

	function switchWithParent($tagId)
	{
		$newParent = $this->repo->getById($tagId);

		if (!$newParent->is_custom && $newParent->parent_id) {
			$tagBag = [];
			$oldParent = $this->repo->getById($newParent->parent_id);
			$oldParentChildren = $this->repo->getChildren($oldParent->id);

			foreach ($oldParentChildren as $tag) {
				$tag->parent_id = $newParent->id;
				$tagBag[] = $tag;
			}

			if (!$newParent->is_alias) {

				$newParentChildren = $this->repo->getChildren($newParent->id);
				foreach ($newParentChildren as $tag) {
					$tag->parent_id = $oldParent->id;
					$tagBag[] = $tag;
				}
			} else {
				$newParent->is_alias = 0;
				$oldParent->is_alias = 1;
			}

			$newParent->parent_id = $oldParent->parent_id;
			$oldParent->parent_id = $newParent->id;
			$tagBag[] = $newParent;
			$tagBag[] = $oldParent;

			$this->repo->bulkSafeSave($tagBag);
			return true;
		}
		return false;

	}

	public function setParent($tagId, $parentId)
	{
		$tag = $this->repo->getById($tagId);

		if ($tag->parent_id == $parentId)
			return true;

		if (!empty($parentId)) {
			$parentTag = $this->repo->getById($parentId);
			if (!$parentTag || $parentTag->taxonomy_id != $tag->taxonomy_id) {
				return false;
			}
		}

		$tag->parent_id = $parentId;
		$this->repo->bulkSafeSave([$tag]);
		return true;
	}

	public function setPositionAfterTag($tagId, $afterId)
	{
		$tag = $this->repo->getById($tagId);
		$afterTag = $this->repo->getById($afterId);
		$tag->moveAfter($afterTag);
	}

	public function setPositionBeforeTag($tagId, $beforeId)
	{
		$tag = $this->repo->getById($tagId);
		$beforeTag = $this->repo->getById($beforeId);
		$tag->moveBefore($beforeTag);
	}

	public function getParents(array $ids)
	{
		return $this->repo->getParents($ids);
	}

	public function getTagsByIds(array $ids)
	{
		return $this->repo->getTagsByIds($ids);
	}

	public function getTagsByNameLike($name, $taxonomy_id, $taggableTypes = null, $isNameSlug = false, $withChildren = false)
	{
		$tags = $this->repo->getByNameLike($name, $taxonomy_id, $isNameSlug, $taggableTypes, $withChildren);

		return $tags;
	}

	public function condenseTags($tagsArray, $entityType, $maxTags = 3, $minToCondense = 3)
	{
		#TODO - account for multiple levels of parent/children as the below code does not address this thus leaving the behavior as random/undefined

		if (count($tagsArray) <= $maxTags) {
			return $tagsArray;
		}

		$tagsGroupedByParent = [];
		foreach ($tagsArray as $tag) {
			$parentId = is_null($tag->parent_id) ? $tag->id : $tag->parent_id;
			$tagsGroupedByParent[$parentId][] = $tag;
		}

		foreach ($tagsArray as $key => $tag) {
			$parentId = $tag->parent_id;

			if (!is_null($parentId) && array_key_exists($parentId, $tagsGroupedByParent) && count($tagsGroupedByParent[$parentId]) >= $minToCondense) {
				unset($tagsArray[$key]);

				if (!array_key_exists($parentId, $tagsArray)) {
					$tagsArray[$parentId] = Tag::find($parentId);
				}
			}
		}

		$usageByTagId = array_pluck($this->tagCounts(array_pluck($tagsArray, 'id'), $entityType), 'usage_count','id');
		$tagsArraySorted = [];

		foreach ($usageByTagId as $tagId => $usage) {
			$tagsArraySorted[$tagId] = $tagsArray[$tagId];
		}

		return $tagsArraySorted;
	}
}

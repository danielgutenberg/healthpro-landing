<?php namespace WL\Modules\Taxonomy\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Modules\Taxonomy\Exceptions\TaxonomyExceptions;
use WL\Modules\Taxonomy\Models\Taxonomy;
use WL\Modules\Taxonomy\Repositories\TaxonomyRepositoryInterface;

class TaxonomyServiceImpl implements TaxonomyServiceInterface
{
	private $repo;

	function __construct(TaxonomyRepositoryInterface $taxonomyRepository)
	{
		$this->repo = $taxonomyRepository;
	}

	function getById($id)
	{
		try {
			return $this->repo->getById($id);
		} catch (ModelNotFoundException $e) {
			return null;
		}
	}

	function getBySlug($slug)
	{
		try {
			return $this->repo->getBy('slug', $slug);
		} catch (ModelNotFoundException $e) {
			return null;
		}
	}

	function failIsTagAddNotAllowed($id, $isCustomTag, $numOfTags)
	{
		if (!$this->isAllowedToAddTags($id, $isCustomTag, $numOfTags)) {
			throw new TaxonomyExceptions(__('adding tags not allowed by taxonomy configuration'));
		}
	}

	function isAllowedToAddTags($id, $isCustomTag, $numOfTags)
	{
		$result = true;
		$tax = $this->getById($id);
		$count = $this->repo->getTaxonomyTagsCount($id);

		$result = $result && ($tax->allow_custom_tags == $isCustomTag) || $tax->allow_custom_tags;

		#todo fix this. Otherwise we can't even seed the DB.
		//$result = $result && (($tax->act_like == Taxonomy::$ACT_ONE && ($count + $numOfTags) <= 1) || $tax->act_like == Taxonomy::$ACT_MANY);

		return $result;
	}

	function deleteTaxonomy($id)
	{
		return $this->repo->deleteTaxonomy($id);
	}

	function bulkAddTaxonomies(array $names, $options = [])
	{
		$taxonomies = [];
		foreach ($names as $name) {
			$taxonomies[] = new Taxonomy(array_merge([
				'name' => $name,
			], $options));
		}

		$this->repo->bulkAddTaxonomies($taxonomies);

		return $taxonomies;
	}

	function getTaxonomies()
	{
		return $this->repo->getTaxonomies();
	}

	function getTaxonomyByName($name)
	{
		return $this->repo->getTaxonomyByName($name);
	}

	function getTaxonomyBySlug($slug)
	{
		return $this->repo->getBy('slug', $slug);
	}
}

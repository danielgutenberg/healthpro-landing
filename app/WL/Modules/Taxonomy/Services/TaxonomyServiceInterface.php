<?php namespace WL\Modules\Taxonomy\Services;

/**
 * Interface TaxonomyServiceInterface
 * @package App\Taxonomy
 */
interface TaxonomyServiceInterface
{
	/**
	 * @param $id
	 * @return mixed
	 */
	function getById($id);

	/**
	 *
	 * Generates exception when adding new tags not allowed
	 *
	 * @param $id
	 * @param $isCustomTag
	 * @param $numOfTags
	 * @return mixed
	 */
	function failIsTagAddNotAllowed($id, $isCustomTag, $numOfTags);

	/**
	 *
	 * Returns bool when adding new tags not allowed
	 *
	 * @param $id
	 * @param $isCustomTag
	 * @param $numOfTags
	 * @return mixed
	 */
	function isAllowedToAddTags($id, $isCustomTag, $numOfTags);

	/**
	 * @param $id
	 * @return mixed
	 */
	function deleteTaxonomy($id);

	/**
	 * @param array $names
	 * @param array $options
	 * @return mixed
	 */
	function bulkAddTaxonomies(array $names, $options = []);


	/**
	 * @return mixed
	 */
	function getTaxonomies();

	/**
	 * @param $name
	 * @return mixed
	 */
	function getTaxonomyByName($name);

	/**
	 * @param $slug
	 * @return mixed
	 */
	function getTaxonomyBySlug($slug);
}

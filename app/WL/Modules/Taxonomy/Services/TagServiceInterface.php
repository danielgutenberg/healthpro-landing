<?php namespace WL\Modules\Taxonomy\Services;

use Illuminate\Database\Eloquent\Collection;
use WL\Models\ModelBase;
use WL\Modules\Taxonomy\Models\Tag;

/**
 * Interface TagServiceInterface
 * @package App\Tag
 */
interface TagServiceInterface
{

	/**
	 *
	 * Associate tags with Entity.
	 *
	 * @param ModelBase $entity
	 * @param array $tagNameArray
	 * @param $taxonomyId
	 * @return mixed
	 */
	function associateTagsWithEntity(ModelBase $entity, array $tagNameArray, $taxonomyId);

	/**
	 *
	 * Removes association
	 *
	 * @param ModelBase $entity
	 * @param array $tagNameArray
	 * @param $taxonomyId
	 * @return mixed
	 */
	function removeAssociateTagsWithEntity(ModelBase $entity, array $tagNameArray, $taxonomyId);

	/**
	 *
	 * Removes association and then set specified tags
	 *
	 * @param ModelBase $entity
	 * @param array $tagNameArray
	 * @param $taxonomyId
	 * @return mixed
	 */
	function explicitSetTagsWithEntity(ModelBase $entity, array $tagNameArray, $taxonomyId);

	/**
	 *
	 * Removes association and then set specified tags
	 *
	 * @param ModelBase $entity
	 * @param array $tagIds
	 * @param $taxonomyId
	 * @return mixed
	 */
	function explicitSetTagIdsWithEntity(ModelBase $entity, array $tagIds, $taxonomyId);

	/**
	 *
	 * Returns array of entities where specified chosen tags
	 *
	 * @param array|Collection $tags [id,id,id..]|['slug','slug',..]|['name','name',..]|[Tag,Tag,..]|[[item],[item],..]
	 * @param string $entityTableName
	 * @param array $options ['withAnyTag'=>true|false,'tagsField'=>'id'|'slug'|'name','taxonomyId'=>int|null,'page'=>int|0,'perPage'=>int|1000,'filters'=>[ [column,'=','value], ['column2,'>',2] ]|null,'sortField'=>string|'updated_at','sortDirection'=>'ASC'|'DESC']
	 * @param array|null $taxonomies [taxonomyId,taxonomyId..]|null
	 * @return mixed
	 */
	function getEntitiesByTags( $tags, $entityTableName, $options, $taxonomies = null);


	/**
	 *
	 * Returns array of entities where specified chosen tags
	 *
	 * @param array $tagIdsArray
	 * @param $entityTableName
	 * @param $page
	 * @param $perPage
	 * @param null $additionalFilter [ [column,'=','value], ['column2,'>',2] ]
	 * @param null $sortField
	 * @return mixed
	 */
	function getEntitiesByTagIds(array $tagIdsArray, $entityTableName, $page, $perPage, $additionalFilter = null, $sortField = null);

	/**
	 * Get child tags by parent taxonomySlug and tagSlug
	 * @param type $taxonomySlug
	 * @param type $tagSlug
	 * @return type
	 */
	public function getChildTagsByParentSlug($taxonomySlug, $tagSlug);

	/**
	 *
	 * Returns Tag by taxonomy Id and tag slug
	 *
	 * @param $slug string
	 * @param $taxonomyId int
	 * @return Tag
	 */
	function getTaxonomyTagByTagSlug($slug, $taxonomyId);

	/**
	 *
	 * Returns tags from Taxonomy
	 *
	 * @param $taxonomyId
	 * @param $loadAllTags
	 * @return mixed
	 */
	function getTaxonomyTags($taxonomyId, $loadAllTags = false);


	/**
	 * @param $slug
	 * @param bool $isName
	 * @return mixed
	 */
	function getTaxonomyTagsBySlug($slug, $isName = false);

	/**
	 * Return all tags belonging to the given taxonomy id and associated with the given entity type - you can optionally pass in a tag slug that the entity must also be tagged by
	 *
	 * @param $taxonomyId
	 * @param $entityType
	 * @param null $alsoTaggedBy
	 * @return mixed
	 */
	public function getTaxonomyTagsForEntityType($taxonomyId, $entityType, $alsoTaggedBy = null);

	/**
	 *
	 * Return child tags
	 *
	 * @param $tagId
	 * @return mixed
	 */
	function getChildTags($tagId);

	/**
	 * get child tags by slug-train-array
	 * @param array $tagTrainSlug
	 * @param $taxonomyId
	 * @return mixed
	 */
	function getChildTagsSlugTrain(array $tagTrainSlug, $taxonomyId);

	/**
	 *
	 * Returns Aliases with Tag
	 *
	 * @param $tagId
	 * @return mixed
	 */
	function getAliases($tagId);


	/**
	 *
	 * Save tags by names
	 *
	 * @param array $tagNames
	 * @param $taxonomyId
	 * @return mixed
	 */
	function addTagNames(array $tagNames, $taxonomyId);

	/**
	 * @param array $tagNames
	 * @param $taxonomyId
	 * @param null $parentTagId
	 * @return mixed
	 */
	function getTagsByNames(array $tagNames, $taxonomyId, $parentTagId = null);

	/**
	 *
	 * Force save tags by name
	 *
	 * @param array $tagNames
	 * @param $taxonomyId
	 * @param array $options
	 * @return mixed
	 */
	function forceAddTagNames(array $tagNames, $taxonomyId, $options = []);

	/**
	 *
	 * Save child tags to Tag
	 *
	 * @param $tagId
	 * @param $taxonomyId
	 * @param array $tagNames
	 * @param array $options
	 * @return mixed
	 */
	function addChildrenToTag($tagId, $taxonomyId, array $tagNames, $options = []);

	/**
	 *
	 * Save aliases to Tag
	 *
	 * @param $tagId
	 * @param $taxonomyId
	 * @param array $aliasNames
	 * @return mixed
	 */
	function addAliasesToTag($tagId, $taxonomyId, array $aliasNames);

	/**
	 *
	 * Remove tags
	 *
	 * @param array $tags
	 * @return mixed
	 */
	function removeTags(array $tags);


	/**
	 *
	 * return tags associated with entity
	 *
	 * @param ModelBase $entity
	 * @param $taxonomyId
	 * @return mixed
	 */
	function getEntityTagsForTaxonomyId(ModelBase $entity, $taxonomyId);

	/**
	 *
	 * Get Tag by Name in Taxonomy
	 *
	 * @param $name
	 * @param $taxonomyId
	 * @param $isNameSlug
	 * @return mixed
	 */
	function getByName($name, $taxonomyId, $isNameSlug = false);


	/**
	 * Returns count of tag count of associations
	 *
	 * @param $taxonomyId
	 * @param null $entityType
	 * @return mixed
	 */
	function taxonomyTagCounts($taxonomyId, $entityType = null);

	/**
	 * Returns the tag usage count for each tag in $tagIds
	 *
	 * @param array $tagIds
	 * @param null $entityType
	 * @return mixed
	 */
	function tagCounts(array $tagIds, $entityType = null);

	/**
	 * Returns tags where usage count greater than $number
	 *
	 * @param $taxonomyId
	 * @param $entityType
	 * @param int $number
	 * @return mixed
	 */
	function getTagsUsageGreaterThanNumber($taxonomyId, $entityType, $number = 0);

	/**
	 * Removes tag - entity association.
	 *
	 * @param $entity
	 * @param $taxonomyId|null If null then taxonomy will be ignored
	 * @return mixed
	 */
	function removeAllEntityAssociations($entity, $taxonomyId);

	/**
	 *
	 * Switch tag with it's parent if tag is not custom and has parent.
	 * Tags yield their children to each other.
	 * If tag is alias, then parent becomes tag's alias and yields children to tag.
	 *
	 *
	 * @param $tagId
	 * @return mixed
	 */
	function switchWithParent($tagId);

	/**
	 * Set new Tag's parent
	 * @param type $tagId
	 * @param type $parentId
	 * @return type
	 */
	public function setParent($tagId, $parentId);

	/**
	 * Set new tag's position after existing tag
	 * @param type $tagId
	 * @param type $afterId
	 * @return type
	 */
	public function setPositionAfterTag($tagId, $afterId);

	/**
	 * Set new tag's position before existing tag
	 * @param type $tagId
	 * @param type $beforeId
	 * @return type
	 */
	public function setPositionBeforeTag($tagId, $beforeId);

	/**
	 * Return tags sorted by count
	 * @param $taxanomoyType
	 * @return mixed
	 */
	function sortByCount($taxanomoyType);

	/**
	 * @param $tagsArray
	 * @param string $entityType
	 * @param int $maxTags maximum tags allowed before checking if we can replace groups of children with their parent
	 * @param int $minToCondense minimum children needed in order to replace those children with their parent
	 * @return mixed
	 */
	public function condenseTags($tagsArray, $entityType, $maxTags = 3, $minToCondense = 3);

	/**
	 * @param string $name
	 * @param int $taxonomy_id
	 * @param null $taggableTypes
	 * @param bool $isNameSlug
	 * @param bool $withChildren
	 * @return mixed
     */
	public function getTagsByNameLike($name, $taxonomy_id, $taggableTypes = null, $isNameSlug = false, $withChildren = false);
}

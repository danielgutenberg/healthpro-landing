<?php namespace WL\Modules\Taxonomy\Providers;

use Illuminate\Support\ServiceProvider;
use \Illuminate\Foundation\AliasLoader;

class TaxonomyServiceProvider extends ServiceProvider
{

	protected $list = [
		\WL\Modules\Taxonomy\Services\TaxonomyServiceInterface::class => \WL\Modules\Taxonomy\Services\TaxonomyServiceImpl::class,
		\WL\Modules\Taxonomy\Repositories\TaxonomyRepositoryInterface::class => \WL\Modules\Taxonomy\Repositories\DbTaxonomyRepository::class,

		\WL\Modules\Taxonomy\Services\TagServiceInterface::class => \WL\Modules\Taxonomy\Services\TagServiceImpl::class,
		\WL\Modules\Taxonomy\Repositories\TagRepositoryInterface::class => \WL\Modules\Taxonomy\Repositories\DbTagRepository::class,
	];

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

		foreach ($this->list as $interface => $class) {
			$this->app->bind($interface, $class);
		}

		$this->app->booting(function()
		{
			AliasLoader::getInstance()->alias('TaxonomyHtml', 'WL\Modules\Taxonomy\Helpers\Html');
		});

	}

}

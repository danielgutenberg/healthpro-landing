<?php namespace WL\Modules\Taxonomy;

use WL\Transformers\Transformer;

class TaxonomyTransformer extends Transformer {

	public function transform($item)
	{
		return [
			'id'=> $item->id,
			'name'=> $item->name,
			'slug'=> $item->slug,
			'act_like'=>$item->act_like,
			'allow_custom_tags'=>$item->allow_custom_tags
		];
	}
}

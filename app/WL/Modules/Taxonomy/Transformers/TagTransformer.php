<?php namespace WL\Modules\Taxonomy;

use WL\Transformers\Transformer;

class TagTransformer extends Transformer
{

	public function transform($item)
	{
		return [
			'id' => $item->id,
			'name' => $item->name,
			'slug' => $item->slug,
			'parent_id' => $item->parent_id,
			'is_alias' => $item->is_alias,
			'is_custom' => $item->is_custom
		];
	}
}

<?php namespace WL\Modules\Taxonomy;

use WL\Transformers\Transformer;

class TagDisplayTransformer extends Transformer
{

	public function transform($item)
	{
		return $item->name;
	}
}

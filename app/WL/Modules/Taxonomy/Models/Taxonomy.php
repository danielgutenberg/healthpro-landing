<?php namespace WL\Modules\Taxonomy\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use WL\Models\ModelBase;

class Taxonomy extends ModelBase implements SluggableInterface
{
	public static $ACT_ONE = 'selectOne';
	public static $ACT_MANY = 'selectMany';

	const SERVICES_TAXONOMY_SLUG = 'services';

	use SluggableTrait;

	protected $sluggable = array(
		'build_from' => 'name',
		'save_to' => 'slug',
		'on_update' => true,
		'unique' => true,
	);

	protected $table = 'taxonomies';
	public $timestamps = false;

	/**
	 * Resurrected from App/Taxonomy
	 * @return mixed
	 */
	public function tags(){
		return $this->hasMany(Tag::class);
	}

	/**
	 * Resurrected from App/Taxonomy
	 * @return mixed
	 */
	public function baseTags()
	{
		return $this->hasMany(Tag::class, 'taxonomy_id')->where(['is_alias' => 0, 'is_custom' => 0]);
	}

	public function allBaseTags($options = [])
	{
		return $this->hasMany(Tag::class, 'taxonomy_id')->where(array_merge(['is_alias' => 0],$options));
	}
}

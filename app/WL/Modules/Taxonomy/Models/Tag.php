<?php namespace WL\Modules\Taxonomy\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use WL\Models\ModelBase;
use WL\Modules\Classes\Model\Classes;
use WL\Modules\Provider\Models\ProviderService;
use WL\Sortable\SortableTrait;//Resurrected from App/Tags

class Tag extends ModelBase implements SluggableInterface
{
	use SluggableTrait;
	use SortableTrait;//Resurrected from App/Tags
	protected static $sortableGroupField = 'taxonomy_id';

	protected $sluggable = array(
		'build_from' => 'name',
		'save_to' => 'slug',
		'on_update' => true,
		'unique' => false,
	);

	public $timestamps = false;

	public function taxonomy()
	{
		return $this->belongsTo(Taxonomy::class);
	}

	/**
	 * Resurrected from App/Tags
	 * @return mixed
	 */
	public function baseTags()
	{
		return $this->hasMany(Tag::class, 'parent_id')->where(['is_alias' => 0, 'is_custom' => 0]);
	}

	public function allBaseTags($options = [])
	{
		return $this->hasMany(Tag::class, 'parent_id')->where(array_merge(['is_alias' => 0],$options));
	}

	/**
	 * Resurrected from App/Tags
	 * @return mixed
	 */
	public function aliases()
	{
		return $this->hasMany(Tag::class, 'parent_id')->where(['is_alias' => 1]);
	}

	public function children()
	{
		return $this->hasMany(Tag::class,'parent_id');
	}

	public function parent()
	{
		return $this->belongsTo(Tag::class,'parent_id','id');
	}

	public function classes(){
		return $this->morphedByMany(Classes::class,'taggable','tag_relations');
	}

	public function providerServices(){
		return $this->morphedByMany(ProviderService::class,'taggable','tag_relations');
	}
}

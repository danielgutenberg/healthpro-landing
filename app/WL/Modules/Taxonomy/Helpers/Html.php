<?php namespace WL\Modules\Taxonomy\Helpers;

use Log;
use WL\Modules\Taxonomy\Facades\TaxonomyService;
use WL\Modules\Taxonomy\Facades\TagService;
use WL\Modules\Taxonomy\Models\Tag;

class Html
{

	public static function input($inputName, $sourceSlug, $selectedTags = [], $args = [])
	{
		if (!is_array($sourceSlug)) $sourceSlug = [$sourceSlug];
		if ($selectedTags === null) $selectedTags = [];

		$slugs = $sourceSlug;

		$taxonomySlug = array_shift($slugs);
		if (null == ($taxonomy = TaxonomyService::getTaxonomyBySlug($taxonomySlug))) {
			Log::error("Can't find taxonomy '" . implode(": ", $sourceSlug) . "' in " . __FILE__ . ' line ' . __LINE__);
			return '';
		}
		$allTags = TagService::getTaxonomyTags($taxonomy->id);

		if (!empty($slugs)) {
			$tagSlug = array_shift($slugs);
			if (null == ($tag = TagService::getBySlug($tagSlug))) {
				Log::error("Can't find tag '" . implode(": ", $sourceSlug) . "' in " . __FILE__ . ' line ' . __LINE__);
				return '';
			}
			$allTags = TagService::getChildTags($tag->id);
		}

		if ($selectedTags instanceof \WL\Modules\Taxonomy\Models\Tag)
			$selectedTags = [$selectedTags];

		$hSelectedTags = [];
		foreach ($selectedTags as $tag) {
			if(is_numeric($tag)){$tag = Tag::find($tag);}
			$hSelectedTags[$tag->id] = $tag;
		}

		$viewName = isset($args['view']) ? $args['view'] : 'helpers.taxonomy.default-input';

		if (isset($args['sortOrder'])) {
			switch ($args['sortOrder']) {
				case 'natural':
					$allTags = (array)$allTags;
					$allTags = array_shift($allTags);

					usort($allTags, function ($a, $b) {
						return strnatcmp($a->name, $b->name);
					});
					break;
			}
		}

		return view($viewName, array_merge($args, compact('taxonomy', 'allTags', 'hSelectedTags', 'inputName')))->render();
	}

	public static function byParentTag($inputName, $taxonomySlug, $parentTagSlug, $selectedMainTags = [], $args = [])
	{

		$taxonomy = TaxonomyService::getTaxonomyBySlug($taxonomySlug);
		$allTags = TagService::getChildTagsSlugTrain($parentTagSlug, $taxonomy->id);


		if (count($allTags) == 0) {
			$allTags[] = TagService::getByName($parentTagSlug, $taxonomy->id, true);
		}

		$hSelectedTags = [];
		foreach ($selectedMainTags as $tag) {
			$hSelectedTags[$tag->id] = $tag;
		}

		return view("helpers.taxonomy.default-input", array_merge($args, compact('taxonomy', 'allTags', 'hSelectedTags', 'inputName')))->render();
	}

	public static function showTags($label, $cssClass, $tagSource, $tagField, $view = 'site.profile.helper.tag-render')
	{

		if (isset($tagSource[$tagField]) && count($item = ($tagSource[$tagField]))) {
			$taxonomySlug = $item['tax_slug'];
			$tags = $item['tags'];
			if (count($tags)) {
				return view($view, compact('label', 'cssClass', 'tags','taxonomySlug'));
			}
			
			return '';
		}

		return '';
	}
}

<?php namespace WL\Modules\Review\Transformers;

use WL\Modules\Rating\Transformers\OverallRatingTransformer;
use WL\Transformers\Transformer;
use WL\Modules\Rating\Transformers\RatingTransformer;

class ReviewTransformer extends Transformer
{

	public function transform($item)
	{
		$result = [
			'review_id'  => $item->id,
			'ratings'    => [],
			'created_at' => (string)$item->created_at,
		];

		if (isset($item->profile))
			$result['reviewer'] = $this->transformProfile($item->profile);

		if (isset($item->overallRating))
			$result['ratings']['overall'] = $this->transformOverallRating($item->overallRating);

		if (isset($item->overallRatings))
			$result['ratings']['details'] = $this->transformOverallRatings($item->overallRatings);

		if (isset($item->helpfulRating))
			$result['ratings']['helpful'] = $this->transformHelpfulRating($item->helpfulRating);

		if (isset($item->comments) && count($item->comments))
			$result['content'] = $this->transformContent($item->comments[0]);

		return $result;
	}

	public function transformProfile($item)
	{
		return [
			'url' => $item->present()->clientProfileUrl(),
			'avatar' => $item->present()->avatar(),
			'id' => $item->id,
			'first_name' => $item->getMeta('first_name'),
			'last_name'  => $item->getMeta('last_name'),
		];
	}

	public function transformOverallRating($item)
	{
		return round($item->rating_value, 1);
	}

	public function transformOverallRatings($items)
	{
		return (new OverallRatingTransformer())->transformCollection($items);
	}


	public function transformHelpfulRating($item)
	{
		return (new RatingTransformer())->transform($item);
	}

	/**
	 * @param $item
	 *
	 * @return string
	 */
	public function transformContent($item)
	{
		return $item->content;
	}
}

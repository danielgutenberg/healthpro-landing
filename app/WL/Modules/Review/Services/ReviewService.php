<?php namespace WL\Modules\Review\Services;

use WL\Modules\Profile\Models\ProviderProfile;
use WL\Modules\Review\Events\ReviewFirstCommentEvent;
use WL\Modules\Review\Repositories\ReviewRepositoryInterface;
use WL\Security\Services\Exceptions\AuthorizationException;
use WL\Modules\Comment\Facades\CommentService;
use WL\Modules\Comment\Models\Comment;
use WL\Modules\Rating\Facades\RatingService;
use WL\Modules\Rating\Models\Rating;
use WL\Modules\Review\Exceptions\ReviewException;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Yaml\Parser;
use WL\Modules\Review\Models\Review;

class ReviewService implements ReviewServiceInterface
{
	private $repo;

	public function __construct(ReviewRepositoryInterface $repo)
	{
		$this->repo = $repo;
	}

	public function getConfig($key, $default = null)
	{
		static $conf;
		if (empty($conf)) {
			$conf = (new Parser())->parse('wl.reviews.reviews');
		}

		return array_get($conf, $key, $default);
	}

	public function getReviewById($id)
	{
		if ($review = $this->repo->getReviewById($id)) {
			$review->profile = ProfileService::getProfileById($review->profile_id);
			$review->overallRating = $this->getReviewOverallRating($review->id, Rating::TYPE_STAR);
			$review->comments = $this->getComments($review->id);
		}
		return $review;
	}

	public function getReviewByProfileAndEntity($profileId, $entityId, $entityType)
	{
		return $this->getProfileReviewsByEntityAndEntityType($profileId, $entityId, $entityType)->last();
	}

	public function deleteReview($reviewId)
	{
		foreach ($this->getRatings($reviewId) as $rating) {
			$this->deleteRating($reviewId, $rating->id);
		}
		foreach ($this->getComments($reviewId) as $comment) {
			$this->deleteComment($comment->id);
		}
		$this->repo->destroy($reviewId);
		return true;
	}

	public function getEntityReviews($entity_id, $entity_type)
	{
		$reviews = $this->repo->getEntityReviews($entity_id, $entity_type);
		foreach ($reviews as &$review) {
			$review->profile = ProfileService::getProfileById($review->profile_id);
			$review->overallRating = $this->getReviewOverallRating($review->id, Rating::TYPE_STAR);
			$review->comments = $this->getComments($review->id);
		}
		return $reviews;
	}

	public function getProfileReviews($profile_id)
	{
		$reviews = $this->repo->getProfileReviews($profile_id);
		foreach ($reviews as &$review) {
			$review->elm = ProfileService::getProfileById($review->elm_id);
			$review->overallRating = $this->getReviewOverallRating($review->id, Rating::TYPE_STAR);
			$review->comments = $this->getComments($review->id);
		}
		return $reviews;
	}

	public function isLimitExceeded($profileId, $entityId, $entityType)
	{
		$profile = ProfileService::getProfileById( $profileId );

		if(! isset($profile->id)) {
			return true;
		}

		$reviews = $this->repo->getProfileReviewsByEntityAndEntityType($profileId, $entityId, $entityType);

		// we don't have a review yet
		if ($reviews->isEmpty()) {
			return false;
		}

		$reviewLimit = $this->getConfig('max_reviews');
		return $reviewLimit <= $reviews->count();
	}

	public function getProfileReviewsByEntityAndEntityType($profileId, $entityId, $entityType)
	{
		$reviews = $this->repo->getProfileReviewsByEntityAndEntityType($profileId, $entityId, $entityType);
		foreach ($reviews as &$review) {
			$review->elm = ProfileService::getProfileById($review->elm_id);
			$review->overallRating = $this->getReviewOverallRating($review->id, Rating::TYPE_STAR);
			$review->comments = $this->getComments($review->id);
		}
		return $reviews;
	}

	public function addReview($reviewType, $profile_id, $entity_id, $entity_type)
	{
		if ($this->isLimitExceeded($profile_id, $entity_id, $entity_type)) {
			throw new AuthorizationException(_('You cannot review again'));
		}

		if (!$this->getConfig($reviewType)) {
			throw new ReviewException(_("config not found for review '{$reviewType}'"));
		}

		$review = $this->repo->createNew([
			'type' => $reviewType,
			'profile_id' => $profile_id,
			'elm_id' => $entity_id,
			'elm_type' => $entity_type,
		]);
		return ($review->save()) ? $review : null;
	}

	public function setComment($reviewId, $profile_id, $content, $type = Review::COMMENT_TYPE_BODY)
	{
		$comment = CommentService::getCommentsForEntity($reviewId, $this->repo->getModelClass(), null, $type)->first();
		if($comment != null){
			return CommentService::updateContent($comment->id, $content);
		} else {
			$response = CommentService::addComment($content, $profile_id, $reviewId, $this->repo->getModelClass(), Comment::APPROVAL_STATUS_PENDING, $type);
			event(new ReviewFirstCommentEvent($reviewId));
			return $response;
		}
	}

	public function addComment($reviewId, $profileId, $content, $type = Review::COMMENT_TYPE_BODY)
	{
		$response = CommentService::addComment($content, $profileId, $reviewId, $this->repo->getModelClass(), Comment::APPROVAL_STATUS_PENDING, $type);

		if(CommentService::countCommentsForEntity($reviewId, $this->repo->getModelClass(), null, $type) === 1){
			event(new ReviewFirstCommentEvent($reviewId));
		}

		return $response;
	}

	public function getComments($reviewId, $profileId = null, $status = null, $type = Review::COMMENT_TYPE_BODY)
	{
		return CommentService::getCommentsForEntity($reviewId, $this->repo->getModelClass(), $status, $type);
	}

	public function updateComment($comment_id, $content)
	{
		return CommentService::updateContent($comment_id, $content);
	}

	public function deleteComment($comment_id)
	{
		return CommentService::removeComment($comment_id);
	}

	public function rateHelpful($reviewId, $profile_id)
	{
		if (!($review = $this->getReviewById($reviewId)))
			return null;

		if (!$this->getConfig("{$review->type}.rateHelpful"))
			throw new ReviewException(_("Review '{$review->type}' can't be rated as helpful"));

		return RatingService::rate($reviewId, $this->repo->getModelClass(), Rating::TYPE_LIKE, 'helpful', 1, $profile_id);
	}

	public function unrateHelpful($reviewId, $profile_id)
	{
		return RatingService::unrate($reviewId, $this->repo->getModelClass(), Rating::TYPE_LIKE, 'helpful', $profile_id);
	}

	public function rate($reviewId, $profile_id, $label, $value)
	{
		if (!($review = $this->getReviewById($reviewId)))
			return null;

		if (!($ratingConf = $this->getConfig("{$review->type}.ratings")))
			throw new ReviewException(_("Review '{$review->type}' don't have rating with labels "));

		if (!in_array($label, $ratingConf))
			throw new ReviewException(_("Review '{$review->type}': can't find  rating label '{$label}'"));

		return RatingService::rate($reviewId, $this->repo->getModelClass(), null, $label, $value, $profile_id);
	}

	public function getRatings($reviewId, $type = Rating::TYPE_STAR)
	{
		return RatingService::getEntityRatings($reviewId, $this->repo->getModelClass(), $type);
	}

	public function deleteRating($reviewId, $ratingId)
	{
		return RatingService::deleteRating($ratingId, $reviewId, $this->repo->getModelClass());
	}

	public function getReviewOverallRating($reviewId, $type)
	{
		return RatingService::getOverallRating($reviewId, $this->repo->getModelClass(), $type);
	}

	public function getRatingById($reviewId, $ratingId)
	{
		$rating = RatingService::getRatingById($ratingId);

		if ($rating->entity_id != $reviewId || $rating->entity_type != $this->repo->getModelClass())
			return false;

		return $rating;
	}

	public function getRatingByLabel($reviewId, $label)
	{
		return RatingService::getRating($reviewId, $this->repo->getModelClass(), $label);
	}

	public function getEntityOverallRating($reviewType, $entityId, $entityType, $type)
	{
		return $this->repo->getEntityOverallRating($reviewType, $entityId, $entityType, $type);
	}

	public function getEntityOverallRatings($reviewType, $entityId, $entityType, $type)
	{
		$ratings = $this->repo->getEntityOverallRatings($reviewType, $entityId, $entityType, $type);
		return $this->sortRatings($reviewType, $ratings);
	}

	private function sortRatings($reviewType, $ratings)
	{
		$list = $this->getConfig("{$reviewType}.ratings", []);
		usort($ratings, function ($a, $b) use ($list) {
			$pA = array_search($a->rating_label, $list);
			$pB = array_search($b->rating_label, $list);
			if ($pA == $pB) return 0;
			return ($pA < $pB) ? -1 : 1;
		});
		return $ratings;
	}

	public function getReviewModelClass()
	{
		return $this->repo->getModelClass();
	}

	public function getRecieverProfileId($id)
	{
		if ($review = $this->repo->getReviewById($id)) {
			switch($review->elm_type){
				case ProviderProfile::class:
					return $review->elm_id;
			}
		}
	}
}

<?php namespace WL\Modules\Review\Services;

interface ReviewServiceInterface
{

	/**
	 * Get review config value
	 * @param $key
	 * @param null $default
	 * @return type
	 */
	public function getConfig($key, $default = null);

	/**
	 * Get review by ID
	 * @param type $id
	 * @return type
	 */
	public function getReviewById($id);

	/**
	 * Get reviews by entity
	 * @param type $entity_id
	 * @param type $entity_type
	 * @return type
	 */
	public function getEntityReviews($entity_id, $entity_type);


	/**
	 * Get reviews by reviewer
	 * @param int $profile_id
	 * @return mixed
	 */
	public function getProfileReviews($profile_id);

	/**
	 * @param int $profileId
	 * @param int $entityId
	 * @param string $entityType
	 * @return bool
	 */
	public function isLimitExceeded($profileId, $entityId, $entityType);

	/**
	 * Get reviews by reviewer and entity
	 * @param $profileId
	 * @param $entityId
	 * @param $entityType
	 * @return mixed
	 */
	public function getProfileReviewsByEntityAndEntityType($profileId, $entityId, $entityType);


	/**
	 * Add new review comment
	 *
	 * @param $reviewId
	 * @param $profileId
	 * @param $content
	 * @param $type
	 * @return mixed
	 */
	public function addComment($reviewId, $profileId, $content, $type = Review::COMMENT_TYPE_BODY);

	/**
	 * Create new review
	 * @param $reviewType
	 * @param type $profile_id
	 * @param type $entity_id
	 * @param type $entity_type
	 * @return type
	 */
	public function addReview($reviewType, $profile_id, $entity_id, $entity_type);

	/**
	 * Add comment
	 *
	 * @param $reviewId
	 * @param $profile_id
	 * @param $content
	 * @param null $type
	 * @return mixed
	 */
	public function setComment($reviewId, $profile_id, $content, $type = null);

	/**
	 * Get review comments
	 * @param type $reviewId
	 * @param null $profile_id
	 * @param type $status
	 * @param type $type
	 * @return type
	 */
	public function getComments($reviewId, $profile_id = null, $status = null, $type = null);

	/**
	 * Edit comment ...
	 *
	 * @param $comment_id
	 * @param $content
	 * @return mixed
	 */
	public function updateComment($comment_id, $content);

	/**
	 * Delete comment by $comment_id .
	 *
	 * @param $comment_id
	 * @return mixed
	 */
	public function deleteComment($comment_id);

	/**
	 * Rate review by rating laber
	 *
	 * @param $reviewId
	 * @param $profile_id
	 * @param string $label
	 * @param $value
	 * @return mixed
	 */
	public function rate($reviewId, $profile_id, $label, $value);

	/**
	 * Get Ratings
	 *
	 * @param $reviewId
	 * @param null $type
	 * @return mixed
	 */
	public function getRatings($reviewId, $type = null);

	/**
	 * Get review overal rating and rating type
	 * @param type $reviewId
	 * @param type $type
	 * @return type
	 */
	public function getReviewOverallRating($reviewId, $type);

	/**
	 * Get review rating by laber;
	 * @param type $reviewId
	 * @param type $label
	 * @return type
	 */
	public function getRatingByLabel($reviewId, $label);

	/**
	 * Get review overal rating by entity and rating type
	 * @param $reviewType
	 * @param type $entityId
	 * @param type $entityType
	 * @param type $type
	 * @return type
	 */
	public function getEntityOverallRating($reviewType, $entityId, $entityType, $type);

	/**
	 * Get review overal ratings by entity and rating type, groupe by rating label
	 * @param $reviewType
	 * @param type $entityId
	 * @param type $entityType
	 * @param type $type
	 * @return type
	 */
	public function getEntityOverallRatings($reviewType, $entityId, $entityType, $type);

}

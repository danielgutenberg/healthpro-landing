<?php namespace WL\Modules\Review\SecurityPolicies;

use WL\Security\SecurityPolicies\BaseAccessPolicy;

/**
 * Class ReviewAccessPolicy
 * @package WL\Modules\Review
 */
class ReviewAccessPolicy extends BaseAccessPolicy
{

	/**
	 * @param $profileId
	 * @param $reviewProfileId
	 * @return bool
	 */
	public function canUpdateReview($profileId, $reviewProfileId)
	{
		return $this->isOwner($profileId, $reviewProfileId);
	}

	/**
	 * @param $profileId
	 * @param $reviewProfileId
	 * @return bool
	 */
	public function canDeleteReview($profileId, $reviewProfileId)
	{
		return $this->isOwner($profileId, $reviewProfileId);
	}

}

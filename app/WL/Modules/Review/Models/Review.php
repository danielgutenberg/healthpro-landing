<?php namespace WL\Modules\Review\Models;

use WL\Models\ModelBase;
use WL\Modules\Comment\Models\Comment;
use WL\Modules\Rating\Models\Rating;

class Review extends ModelBase
{
	const COMMENT_TYPE_BODY = 'review_body';
	const TYPE_PROVIDER_REVIEW = 'provider_profile';
}

<?php namespace  WL\Modules\Review\Commands;


use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Review\SecurityPolicies\ReviewAccessPolicy;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Services\Exceptions\AuthorizationException;

class DeleteReviewRatingCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.DeleteReviewRatingCommand';

	protected $reviewId;
	protected $ratingId;

	public function __construct($reviewId, $ratingId)
	{
		$this->accessPolicy = new ReviewAccessPolicy();
		$this->reviewId = $reviewId;
		$this->ratingId = $ratingId;
	}

	public function handle()
	{
		$currentProfileId = ProfileService::getCurrentProfileId();

		$review = ReviewService::getReviewById($this->reviewId);
		if (!$this->accessPolicy->canDeleteReview($currentProfileId, $review->profile_id))
			throw new AuthorizationException("Can't change review 'review[{$review->id}]'");

		return ReviewService::deleteRating($this->reviewId, $this->ratingId);
	}

}

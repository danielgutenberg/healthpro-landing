<?php namespace  WL\Modules\Review\Commands;


use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Rating\Facades\RatingService;
use WL\Modules\Rating\Models\Rating;
use WL\Modules\Review\SecurityPolicies\ReviewAccessPolicy;
use WL\Security\Services\Exceptions\AuthorizationException;

class UpdateReviewCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.UpdateReviewCommand';

	protected $reviewId;
	protected $comment;
	protected $ratings;

	public function __construct($reviewId, $comment='', array $ratings=[])
	{
		$this->accessPolicy = new ReviewAccessPolicy();
		$this->reviewId = $reviewId;
		$this->comment = $comment;
		$this->ratings = $ratings;
	}

	public function handle()
	{
		$currentProfileId = ProfileService::getCurrentProfileId();

		$review = ReviewService::getReviewById($this->reviewId);
		if (!$this->accessPolicy->canUpdateReview($currentProfileId, $review->profile_id))
			throw new AuthorizationException("Can't update review 'review[{$review->id}]'");

		foreach ($this->ratings as $label => $value) {
			ReviewService::rate($review->id, $currentProfileId, $label, $value);
		}
		if ($this->comment) {
			ReviewService::setComment($this->reviewId, $currentProfileId, $this->comment);
		}

		return ReviewService::getReviewById($review->id);
	}

}

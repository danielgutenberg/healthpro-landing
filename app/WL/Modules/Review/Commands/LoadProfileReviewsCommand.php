<?php namespace  WL\Modules\Review\Commands;


use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Rating\Facades\RatingService;
use WL\Modules\Rating\Models\Rating;

class LoadProfileReviewsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.LoadProfileReviewsCommand';

	protected $profileId;

	public function __construct($profileId)
	{
		$this->profileId = $profileId;
	}

	public function handle()
	{
		return ReviewService::getProfileReviews($this->profileId);
	}

}

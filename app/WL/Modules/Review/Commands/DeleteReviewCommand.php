<?php namespace  WL\Modules\Review\Commands;


use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Review\SecurityPolicies\ReviewAccessPolicy;
use WL\Security\Services\Exceptions\AuthorizationException;

class DeleteReviewCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.DeleteReviewCommand';

	protected $reviewId;

	public function __construct($reviewId)
	{
		$this->accessPolicy = new ReviewAccessPolicy();
		$this->reviewId = $reviewId;
	}

	public function handle()
	{
		$currentProfileId = ProfileService::getCurrentProfileId();

		$review = ReviewService::getReviewById($this->reviewId);
		if (!$this->accessPolicy->canDeleteReview($currentProfileId, $review->profile_id))
			throw new AuthorizationException("Can't remove review 'review[{$review->id}]'");

		return ReviewService::deleteReview($review->id);
	}

}

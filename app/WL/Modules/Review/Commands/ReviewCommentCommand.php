<?php namespace WL\Modules\Review\Commands;


use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Modules\Comment\Exceptions\CommentLimitExceededException;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Review\Models\Review;
use WL\Security\Commands\AuthorizableCommand;

class ReviewCommentCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.addComment';

	private $profileId;
	private $entityId;
	private $entityType;
	private $content;
	private $reviewId;

	private $validator;

	public function __construct($profileId, $entityId, $entityType, $content, $reviewId = null)
	{
		$this->profileId = $profileId;
		$this->entityId = $entityId;
		$this->entityType = $entityType;
		$this->reviewId = $reviewId;
		$this->content = $content;

		$this->validator = Validator::make(
			[
				'content' => $this->content,
			],
			[
				'content' => 'required',
			],
			[
				'content.required' => 'Please fill the comment field.',
			]
		);
	}

	public function handle()
	{
		if ($this->validator->fails()) {
			throw new ValidationException($this->validator);
		}

		if (ReviewService::isLimitExceeded( ProfileService::getCurrentProfileId(), $this->entityId, $this->entityType)) {
			throw new CommentLimitExceededException('You comment limit is exceeded');
		}

		// find review by profile and entity
		if (null === $this->reviewId) {
			$review = ReviewService::getReviewByProfileAndEntity($this->profileId, $this->entityId, $this->entityType);

			if (null === $review) {
				$review = ReviewService::addReview(
					'provider_profile',
					$this->profileId,
					$this->entityId,
					$this->entityType
				);
			}

		# grab review by ID
		} else {
			$review = ReviewService::getReviewById($this->reviewId);
		}

		return ReviewService::addComment($review->id, $this->profileId, $this->content, Review::COMMENT_TYPE_BODY);
	}

}

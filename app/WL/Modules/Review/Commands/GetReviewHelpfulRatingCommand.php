<?php namespace  WL\Modules\Review\Commands;


use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Rating\Facades\RatingService;
use WL\Modules\Rating\Models\Rating;
use WL\Modules\Review\Models\Review;

class GetReviewHelpfulRatingCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.GetReviewHelpfulRatingCommand';

	protected $reviewId;

	public function __construct($reviewId)
	{
		$this->reviewId = $reviewId;
	}

	public function handle()
	{
		$rating = RatingService::getRating($this->reviewId, Review::class, 'helpful');
		if ($rating) {
			$rating->currentProfileRatedValue = RatingService::getRatingValueByProfileId($rating->id, ProfileService::getCurrentProfileId());
		}
		return $rating;
	}

}

<?php namespace WL\Modules\Review\Commands;


use WL\Modules\Review\Facade\ReviewService;
use WL\Security\Commands\AuthorizableCommand;

class ReviewGetCommentCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.getComment';

	private $comment_id;

	public function __construct($comment_id)
	{
		$this->comment_id = $comment_id;
	}

	public function handle()
	{
		return ReviewService::getComment($this->comment_id);
	}

}

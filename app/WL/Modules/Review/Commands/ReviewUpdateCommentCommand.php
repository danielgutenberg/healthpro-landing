<?php namespace WL\Modules\Review\Commands;


use WL\Modules\Review\Facade\ReviewService;
use WL\Security\Commands\AuthorizableCommand;

class ReviewUpdateCommentCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.updateComment';

	private $comment_id;
	private $content;

	public function __construct($comment_id, $content)
	{
		$this->comment_id = $comment_id;
		$this->content = $content;
	}

	public function handle()
	{
		return ReviewService::updateComment($this->comment_id, $this->content);
	}

}

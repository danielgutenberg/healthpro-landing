<?php namespace WL\Modules\Review\Commands;


use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Profile\Facades\ProfileService;

class UnrateReviewHelpfulRatingCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.UnrateReviewHelpfulRatingCommand';

	public function __construct($reviewId)
	{
		$this->reviewId = $reviewId;
	}

	public function handle()
	{
		$currentProfileId = ProfileService::getCurrentProfileId();

		return ReviewService::unrateHelpful($this->reviewId, $currentProfileId);
	}
}

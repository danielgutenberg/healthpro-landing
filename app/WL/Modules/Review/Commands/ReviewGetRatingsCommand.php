<?php namespace WL\Modules\Review\Commands;


use WL\Modules\Review\Facade\ReviewService;
use WL\Security\Commands\AuthorizableCommand;

class ReviewGetRatingsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.getRatings';

	private $entity_id;
	private $entity_type;
	private $label;

	public function __construct($entity_id, $entity_type, $label)
	{
		$this->entity_id = $entity_id;
		$this->entity_type = $entity_type;
		$this->label = $label;
	}

	public function handle()
	{
		return ReviewService::getRatings($this->entity_id, $this->entity_type, $this->label);
	}

}

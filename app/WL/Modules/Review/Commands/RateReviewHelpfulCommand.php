<?php namespace WL\Modules\Review\Commands;


use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Profile\Facades\ProfileService;

class RateReviewHelpfulCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.RateReviewHelpfulCommand';

	public function __construct($reviewId)
	{
		$this->reviewId = $reviewId;
	}

	public function handle()
	{
		$currentProfileId = ProfileService::getCurrentProfileId();

		return ReviewService::rateHelpful($this->reviewId, $currentProfileId);
	}
}

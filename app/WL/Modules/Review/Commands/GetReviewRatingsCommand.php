<?php namespace  WL\Modules\Review\Commands;


use WL\Modules\Review\Facade\ReviewService;
use WL\Security\Commands\AuthorizableCommand;

class GetReviewRatingsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.GetReviewRatingsCommand';

	protected $reviewId;

	public function __construct($reviewId)
	{
		$this->reviewId = $reviewId;
	}

	public function handle()
	{
		return ReviewService::getRatings($this->reviewId);
	}

}

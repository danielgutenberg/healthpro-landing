<?php namespace WL\Modules\Review\Commands;


use WL\Modules\Review\Facade\ReviewService;
use WL\Security\Commands\AuthorizableCommand;

class SetReviewRatingCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.SetReviewRatingCommand';

	private $review_id;
	private $profile_id;
	private $label;
	private $value;

	public function __construct($review_id, $profile_id, $label, $value)
	{
		$this->review_id = $review_id;
		$this->profile_id = $profile_id;
		$this->label = $label;
		$this->value = $value;
	}

	public function handle()
	{
		return ReviewService::rate($this->review_id, $this->profile_id, $this->label, $this->value);
	}

}

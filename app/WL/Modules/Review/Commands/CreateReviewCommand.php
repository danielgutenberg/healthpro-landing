<?php namespace WL\Modules\Review\Commands;


use WL\Modules\Profile\Models\ModelProfile;
use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Rating\Facades\RatingService;
use WL\Modules\Rating\Models\Rating;

class CreateReviewCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.CreateReviewCommand';

	protected $reviewType;
	protected $elementId;
	protected $elementType;
	protected $comment;
	protected $ratings;

	public function __construct($reviewType, $byProfileId, $elementId, $elementType, $comment = '', array $ratings = [])
	{
		$this->reviewType = $reviewType;
		$this->byProfileId = $byProfileId;
		$this->elementId = $elementId;
		$this->elementType = $elementType;
		$this->comment = $comment;
		$this->ratings = $ratings;
	}

	public function handle()
	{
		$review = ReviewService::addReview($this->reviewType, $this->byProfileId, $this->elementId, $this->elementType);

		foreach ($this->ratings as $label => $value) {
			ReviewService::rate($review->id, $this->byProfileId, $label, $value);
		}
		if ($this->comment) {
			ReviewService::setComment($review->id, $this->byProfileId, $this->comment);
		}

		return ReviewService::getReviewById($review->id);
	}

}

<?php namespace  WL\Modules\Review\Commands;


use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Rating\Facades\RatingService;
use WL\Modules\Rating\Models\Rating;

class LoadElementReviewsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.LoadElementReviewsCommand';

	protected $elementId;
	protected $elementType;
	protected $reviewType;


	public function __construct($elementId, $elementType, $reviewType = 'provider_profile')
	{
		$this->elementId = $elementId;
		$this->elementType = $elementType;
		$this->reviewType = $reviewType;

	}

	public function handle()
	{
		$reviews = ReviewService::getEntityReviews($this->elementId, $this->elementType);

		foreach ($reviews as &$review) {
			$review->overallRatings = ReviewService::getEntityOverallRatings($this->reviewType, $this->elementId, $this->elementType, Rating::TYPE_STAR);
		}

		return $reviews;
	}

}

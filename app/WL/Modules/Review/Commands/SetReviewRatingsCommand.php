<?php namespace WL\Modules\Review\Commands;


use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Profile\Facades\ProfileService;

class SetReviewRatingsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.SetReviewRatingsCommand';

	public function __construct($reviewId, array $ratings)
	{
		$this->reviewId = $reviewId;
		$this->ratings = $ratings;
	}

	public function handle()
	{
		$currentProfileId = ProfileService::getCurrentProfileId();

		foreach ($this->ratings as $label=>$value) {
			ReviewService::rate($this->reviewId, $currentProfileId, $label, $value);
		}
		return true;
	}

}

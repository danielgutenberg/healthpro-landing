<?php namespace WL\Modules\Review\Commands;


use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Rating\Facades\RatingService;
use WL\Modules\Rating\Models\Rating;

class LoadOrCreateReviewCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'review.LoadOrCreateReviewCommand';

	protected $reviewType;
	protected $profileId;
	protected $entityId;
	protected $entityType;
	protected $comment;
	protected $ratings;

	public function __construct($reviewType, $profileId, $entityId, $entityType, $comment = null, array $ratings = [])
	{
		$this->reviewType = $reviewType;
		$this->profileId = $profileId;
		$this->entityId = $entityId;
		$this->entityType = $entityType;
		$this->comment = $comment;
		$this->ratings = $ratings;
	}

	public function handle()
	{
		// check if review already exists
		$review = ReviewService::getReviewByProfileAndEntity($this->profileId, $this->entityId, $this->entityType);
		$created = false;
		if (null === $review) {
			$review = ReviewService::addReview($this->reviewType, $this->profileId, $this->entityId, $this->entityType);
		}

		foreach ($this->ratings as $label => $value) {
			ReviewService::rate($review->id, $this->profileId, $label, $value);
		}

		if ($this->comment) {
			ReviewService::addComment($review->id, $this->profileId, $this->comment);
		}


		return ReviewService::getReviewById($review->id);
	}

}

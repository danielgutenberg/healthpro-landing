<?php namespace WL\Modules\Review\Commands;

use Carbon\Carbon;

use Illuminate\Console\Command;
use WL\Events\Facades\EventHelper;
use WL\Modules\Appointment\Facades\AppointmentService;
use WL\Modules\Appointment\Models\ProviderAppointment;
use WL\Modules\Client\Events\ClientAskToReviewProviderNotification;
use WL\Modules\Client\Events\ClientAskToReviewProvider;
use WL\Modules\Client\Facades\ClientService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Review\Facade\ReviewService;
use WL\Schedule\Facades\ScheduleMetaService;
use WL\Schedule\Models\BasicMetaKey;

class SendReviewRequestsCommand extends Command
{
	protected $signature = 'review:send_requests';

	public function __construct() {
		parent::__construct();
	}
	public function handle()
	{
		// get attended appointments with provider without review
		$until = Carbon::now();
		$from = new Carbon('-1 day');

		$appointments = AppointmentService::getFinishedAppointmentsBetween($from, $until);
		$alreadySent = [];

		foreach ($appointments as $appointment) {
			$profileIds = [$appointment->client_id, $appointment->provider_id];
			// Prevent duplicates
			if(in_array($profileIds, $alreadySent)) {
				continue;
			}
			// Missed appointments ignored
			if(AppointmentService::getAppointmentMark($appointment) == ProviderAppointment::STATUS_MISSED) {
				continue;
			}
			// Check if exceeded review limit
			if (ReviewService::isLimitExceeded($appointment->client_id, $appointment->provider_id, ModelProfile::class)) {
				continue;
			}

			EventHelper::fire(new ClientAskToReviewProvider($appointment->client_id, $appointment->provider_id));
			EventHelper::fire(new ClientAskToReviewProviderNotification($appointment->client_id, $appointment->provider_id));

			$alreadySent[] = $profileIds;
		}
	}
}

<?php namespace WL\Modules\Review\Providers;

use Illuminate\Support\ServiceProvider;
use WL\Modules\Review\Repositories\ReviewRepositoryInterface;
use WL\Modules\Review\Services\ReviewServiceInterface;
use WL\Modules\Review\Repositories\DbReviewRepository;
use WL\Modules\Review\Services\ReviewService;
use WL\Modules\Review\Authorization;

class ReviewServiceProvider extends ServiceProvider
{
	public function register()
	{
		$this->app->bind(ReviewRepositoryInterface::class, DbReviewRepository::class);
		$this->app->bind(ReviewServiceInterface::class, ReviewService::class);
	}
}

<?php namespace WL\Modules\Review\Repositories;

interface ReviewRepositoryInterface
{

	/**
	 * Get review by id
	 * @param type $id
	 * @return type
	 */
	public function getReviewById($id);

	/**
	 * Get entity reviews
	 * @param type $entity_id
	 * @param type $entity_type
	 * @return type
	 */
	public function getEntityReviews($entity_id, $entity_type);


	/**
	 * Get profile reviews
	 *
	 * @param $profileId
	 * @return mixed
     */
	public function getProfileReviews($profileId);

	/**
	 * Get review overal rating by entity and rating type
	 * @param $reviewType
	 * @param type $entityId
	 * @param type $entityType
	 * @param $ratingType
	 * @return mixed
	 */
	public function getEntityOverallRating($reviewType, $entityId, $entityType, $ratingType);

	/**
	 * Get review overal ratings by entity and rating type, groupe by rating label
	 * @param $reviewType
	 * @param type $entityId
	 * @param type $entityType
	 * @param $ratingType
	 * @return mixed
	 */
	public function getEntityOverallRatings($reviewType, $entityId, $entityType, $ratingType);

	/**
	 * @param $profileId
	 * @param $entityId
	 * @param $entityType
	 * @return mixed
     */
	public function getProfileReviewsByEntityAndEntityType($profileId, $entityId, $entityType);

}

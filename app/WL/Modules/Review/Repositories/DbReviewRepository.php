<?php namespace WL\Modules\Review\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Modules\Rating\Facades\RatingService;
use WL\Modules\Review\Models\Review;
use WL\Repositories\DbRepository;

class DbReviewRepository extends DbRepository implements ReviewRepositoryInterface
{
	protected $model;

	public function __construct()
	{
		$this->model = new Review();
	}

	public function getReviewById($id)
	{
		try {
			return $this->getById($id);
		} catch (ModelNotFoundException $e) {
			return null;
		}
	}

	public function getProfileReviewsByEntityAndEntityType($profileId, $entityId, $entityType)
	{
		return Review::where('profile_id', $profileId)
			->where('elm_id', $entityId)
			->where('elm_type', $entityType)
			->get();
	}

	public function getEntityReviews($entity_id, $entity_type)
	{
		return Review::where('elm_id', $entity_id)
			->where('elm_type', $entity_type)
			->get();
	}

	public function getProfileReviews($profileId)
	{
		return Review::where('profile_id', $profileId)
			->get();
	}

	public function getEntityOverallRating($reviewType, $entityId, $entityType, $ratingType)
	{
		$data = \DB::table('reviews')
			->select(\DB::raw('SUM(ratings.count) AS count, AVG(ratings.rating_value) AS avgValue'))
			->where('reviews.type', $reviewType)
			->where('reviews.elm_id', $entityId)
			->where('reviews.elm_type', $entityType)
			->join('ratings', function ($join) use ($ratingType) {
				$join->on('ratings.entity_id', '=', 'reviews.id')
					->where('ratings.entity_type', '=', Review::class)
					->where('ratings.rating_type', '=', $ratingType);
			})
			->first();

		$rating = RatingService::getNewRating(0, '', $ratingType, '', $data->avgValue);
		$rating->countOfRatings = $data->count;

		return $rating;
	}

	public function getEntityOverallRatings($reviewType, $entityId, $entityType, $ratingType)
	{
		$data = \DB::table('reviews')
			->distinct()
			->select(\DB::raw('ratings.rating_label AS label, SUM(ratings.count) AS count, AVG(ratings.rating_value) AS avgValue'))
			->where('reviews.type', $reviewType)
			->where('reviews.elm_id', $entityId)
			->where('reviews.elm_type', $entityType)
			->join('ratings', function ($join) use ($ratingType) {
				$join->on('ratings.entity_id', '=', 'reviews.id')
					->where('ratings.entity_type', '=', Review::class)
					->where('ratings.rating_type', '=', $ratingType);
			})
			->orderBy('ratings.rating_label')
			->groupBy('ratings.rating_label')
			->get();

		$result = [];
		foreach ($data as $row) {
			$result[] = $rating = RatingService::getNewRating(0, '', $ratingType, $row->label, $row->avgValue);
			$rating->countOfRatings = $row->count;
		}

		return $result;
	}

	public function getReviews($reviewType, $page = 1, $perPage = 20)
	{
		dd($page);
	}
}

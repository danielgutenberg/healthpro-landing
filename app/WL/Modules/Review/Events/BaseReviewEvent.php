<?php namespace WL\Modules\Review\Events;


class BaseReviewEvent
{
	private $reviewId;

	/**
	 * Event constructor.
	 * @param $reviewId
	 */
	public function __construct($reviewId)
	{
		$this->reviewId = $reviewId;
	}

	/**
	 * @return int
	 */
	public function getReviewId()
	{
		return $this->reviewId;
	}
}

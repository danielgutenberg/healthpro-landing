<?php namespace WL\Modules\Review\Facade;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Review\Services\ReviewServiceInterface;

class ReviewService extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return ReviewServiceInterface::class;
	}
}

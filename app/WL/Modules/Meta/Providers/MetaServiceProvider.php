<?php namespace WL\Modules\Meta\Providers;

use Illuminate\Support\ServiceProvider;
use WL\Modules\Meta\Repositories\YamlMetaFieldsRepository;
use WL\Modules\Meta\Services\MetaServiceImpl;
use WL\Modules\Meta\Services\MetaServiceInterface;
use WL\Yaml\Parser;


class MetaServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton(MetaServiceInterface::class, function () {
			return new MetaServiceImpl(new YamlMetaFieldsRepository(new Parser()));
		});
	}
}

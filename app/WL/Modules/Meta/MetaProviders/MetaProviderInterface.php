<?php namespace WL\Modules\Meta\MetaProviders;

interface MetaProviderInterface
{
	public function provide();
}

<?php namespace WL\Modules\Meta\MetaProviders;

use WL\Settings\Facades\Setting;

class BaseMetaProvider implements MetaProviderInterface
{
	private $routeName;

	function __construct($routeName)
	{
		$this->routeName = $routeName;
	}

	public function provide()
	{
		return [
			'page_title' => Setting::get('site_title'),
			'page_description' => Setting::get('site_description'),
			'page_keywords' => Setting::get('site_keywords'),
		];
	}
}

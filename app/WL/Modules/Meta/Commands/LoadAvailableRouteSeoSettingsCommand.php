<?php namespace WL\Modules\Meta\Commands;


use WL\Modules\Meta\Facades\MetaService;
use WL\Security\Commands\AuthorizableCommand;

/**
 * Class LoadAvailableRouteSeoSettingsCommand
 * @package WL\Modules\Meta\Commands
 */
class LoadAvailableRouteSeoSettingsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'meta.loadAvailableRouteSeoSettings';


	public function handle()
	{
		return MetaService::getConfigurableRouteNames();
	}
}

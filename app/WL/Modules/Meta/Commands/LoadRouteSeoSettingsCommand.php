<?php namespace WL\Modules\Meta\Commands;


use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Modules\Meta\Facades\MetaService;
use WL\Security\Commands\AuthorizableCommand;

/**
 * Class LoadRouteSeoSettingsCommand
 * @package WL\Modules\Meta\Commands
 */
class LoadRouteSeoSettingsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'meta.loadRouteSeoSettings';

	private $routeName;

	function __construct($routeName)
	{
		$this->routeName = $routeName;

		$this->validator = Validator::make(
			[
				'routeName' => $this->routeName,
			],
			[
				'routeName' => 'required',
			]
		);
	}

	public function handle()
	{
		if ($this->validator->passes())
			return MetaService::getRouteFieldTemplates($this->routeName);
		else
			throw new ValidationException($this->validator);
	}
}

<?php namespace WL\Modules\Meta\Commands;


use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Modules\Meta\Facades\MetaService;
use WL\Security\Commands\AuthorizableCommand;

/**
 * Class SaveRouteSeoSettingsCommand
 * @package WL\Modules\Meta\Commands
 */
class SaveRouteSeoSettingsCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'meta.saveRouteSeoSettings';

	private $routeName;
	private $fields;
	private $validator;

	function __construct($routeName, $fields)
	{
		$this->routeName = $routeName;
		$this->fields = $fields;

		$this->validator = Validator::make(
			[
				'routeName' => $this->routeName,
			],
			[
				'routeName' => 'required',
			]
		);
	}

	public function handle()
	{
		if ($this->validator->passes())
			return MetaService::saveRouteFields($this->routeName, $this->fields);
		else
			new ValidationException($this->validator);
	}
}

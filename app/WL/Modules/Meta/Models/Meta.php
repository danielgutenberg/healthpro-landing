<?php
namespace WL\Modules\Meta\Models;

use WL\Models\ModelMeta;

class Meta extends ModelMeta
{
	protected $table = 'meta';

	public function value()
	{
		return nl2br($this->meta_value);
	}

}

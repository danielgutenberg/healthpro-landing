<?php namespace WL\Modules\Meta\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Meta\Services\MetaServiceInterface;

class MetaService extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return MetaServiceInterface::class;
	}
}

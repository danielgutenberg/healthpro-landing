<?php namespace WL\Modules\Meta\Repositories;

use WL\Yaml\Contracts\Parsable;

class YamlMetaFieldsRepository implements MetaFieldsRepository
{

	/**
	 * @var Parsable
	 */
	private $parser;
	private $localCache;


	function __construct(Parsable $parser)
	{
		$this->parser = $parser;
		if (empty($this->localCache))
			$this->localCache = self::getParser()->parse('wl.meta.route-meta');
	}

	public function getMetaField($routeName, $fieldName)
	{
		$metaFields = $this->localCache;
		return array_get($metaFields, $routeName . '.' . $fieldName);
	}

	public function getRouteNames()
	{
		return array_keys($this->localCache);
	}

	public function getRouteFields($routeName)
	{
		return array_get($this->localCache, $routeName);
	}

	/**
	 * Get parser instance
	 *
	 * @return Parsable
	 */
	public function getParser()
	{
		return $this->parser;
	}

}

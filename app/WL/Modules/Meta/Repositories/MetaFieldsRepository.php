<?php namespace WL\Modules\Meta\Repositories;

/**
 * Interface MetaRepository
 * @package WL\Security
 */
interface MetaFieldsRepository
{
	/**
	 * Get meta fields by role
	 * @param $routeName
	 * @param $fieldName
	 * @return mixed
	 */
	public function getMetaField($routeName, $fieldName);

	/**
	 * Get available route names
	 * @return array
	 */
	public function getRouteNames();

	/**
	 * Get available fields from route
	 * @param $routeName string
	 * @return mixed
	 */
	public function getRouteFields($routeName);

}

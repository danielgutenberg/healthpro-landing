<?php namespace WL\Modules\Meta\Services;

/**
 * Interface MetaServiceInterface
 * @package WL\Modules\Meta\Services
 */
interface MetaServiceInterface
{

	/**
	 * Get configurable route names
	 * @return mixed
	 */
	public function getConfigurableRouteNames();

	/**
	 * Get array of field - template
	 * @param $routeName string
	 * @return mixed
	 */
	public function getRouteFieldTemplates($routeName);

	/**
	 * Save array of field - value(template)
	 * @param $routeName string
	 * @param $fields array
	 * @return mixed
	 */
	public function saveRouteFields($routeName, $fields);
}

<?php namespace WL\Modules\Meta\Services;

use Illuminate\Support\Facades\Route;
use WL\Gettext\Facades\BladeCompiler;
use WL\Modules\Meta\MetaProviders\BaseMetaProvider;
use WL\Modules\Meta\MetaProviders\MetaProviderInterface;
use WL\Settings\Facades\Setting;

class MetaServiceImpl implements MetaServiceInterface
{
	const SEO_NAMESPACE = 'seo';
	private $repository;

	function __construct($repository)
	{
		$this->repository = $repository;
	}

	public function getConfigurableRouteNames()
	{
		return $this->repository->getRouteNames();
	}

	public function getRouteFieldTemplates($routeName)
	{
		$result = [];
		$defaultFieldValues = $this->repository->getRouteFields($routeName);

		if (empty($defaultFieldValues))
			return $result;

		foreach ($defaultFieldValues as $fieldKey => $fieldValue) {
			$result[$fieldKey] = $this->loadTemplate($routeName, $fieldKey);
		}
		return $result;
	}

	public function saveRouteFields($routeName, $fields)
	{
		foreach ($fields as $fieldKey => $fieldValue) {
			Setting::set($this->getFieldNamespace($routeName, $fieldKey), $fieldValue);
		}
	}

	public function __call($function, $args)
	{
		$routeName = $this->routeName($args);

		$defaultProvider = new BaseMetaProvider($routeName);
		$providers = array_merge([$defaultProvider], $this->providers($args));
		$context = $this->callProviders($providers);

		$template = $this->loadTemplate($routeName, $function);

		return $this->compileTemplate($template, $context);
	}

	private function getRouteNameNamespace($routeName)
	{
		return static::SEO_NAMESPACE . '_' . $routeName;
	}

	private function getFieldNamespace($routeName, $fieldName)
	{
		return $this->getRouteNameNamespace($routeName) . '.' . $fieldName;
	}

	private function loadTemplate($routeName, $fieldName)
	{
		$fieldName = snake_case($fieldName);

		//try to load from database
		$template = Setting::get($this->getFieldNamespace($routeName, $fieldName));

		//try to load meta from yaml file
		if (is_null($template))
			$template = $this->repository->getMetaField($routeName, $fieldName);

		//last attempt to load meta from default settings
		if (is_null($template))
			$template = Setting::get($fieldName);

		return $template;
	}


	private function compileTemplate($template, $context)
	{
		$generated = BladeCompiler::compileString($template);


		ob_start() and extract($context, EXTR_SKIP);

		// We'll include the view contents for parsing within a catcher
		// so we can avoid any WSOD errors. If an exception occurs we
		// will throw it out to the exception handler.
		try {
			eval('?>' . $generated);
		}

			// If we caught an exception, we'll silently flush the output
			// buffer so that no partially rendered views get thrown out
			// to the client and confuse the user with junk.
		catch (\Exception $e) {
			ob_get_clean();
			throw $e;
		}

		$content = ob_get_clean();

		return $content;
	}


	private function routeName($args)
	{
		$result = Route::currentRouteName();
		if ($args && is_string($args[0])) {
			$result = $args[0];
		}

		return $result;
	}

	private function providers($args)
	{
		$result = [];
		if ($args) {
			// is first arg is an array of providers?
			if (is_array($args[0])) {
				$result = $args[0];
				// maybe second arg is an array of providers?
			} elseif (count($args) > 1 && is_array($args[0])) {
				$result = $args[1];
			}
		}

		return $result;
	}

	private function callProviders(array $providers)
	{
		$result = [];

		if (empty($providers))
			return $result;

		foreach ($providers as $provider) {
			if ($provider instanceof MetaProviderInterface) {
				$value = $provider->provide();
			} elseif (is_array($provider)) {
				$value = $provider;
			}
			$result = array_merge($result, $value);
		}

		return $result;
	}


}

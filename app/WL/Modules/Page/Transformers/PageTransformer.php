<?php namespace WL\Modules\Page\Transformers;

use WL\Transformers\Transformer;

class PageTransformer extends Transformer
{
	public function transform($item)
	{
		return array_except($item, ['password', 'redirect', 'rank' , 'active']);
	}
}

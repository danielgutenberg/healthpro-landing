<?php

namespace WL\Modules\Page\Populators;

use WL\Modules\Page\Models\Page;
use WL\Populators\FormerPopulator;
use WL\Modules\Page\Populators\Exceptions\PageNotLoadedException;
use WL\Modules\Page\Populators\PagePopulator;

class FormerPagePopulator extends FormerPopulator implements PagePopulator
{
	/**
	 * We accept only the Page model here
	 *
	 * @param \WL\Modules\Page\Models\Page $model
	 * @throws PageNotLoadedException
	 * @return $this
	 */
	public function setModel(Page $model)
	{
		$this->model = $model;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getFields()
	{
		// the magic happens here
		$fields = $this->model->toArray();
		$fields['meta'] = $this->model->getAllMeta(['title', 'keywords', 'description','view']);

		return $fields;
	}
}

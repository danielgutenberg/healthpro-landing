<?php

namespace WL\Modules\Page\Populators;

use WL\Modules\Page\Models\Page;

interface PagePopulator
{
	/**
	 * @param Page $page
	 *
	 * @return $this
	 */
	public function setModel(Page $page);
}

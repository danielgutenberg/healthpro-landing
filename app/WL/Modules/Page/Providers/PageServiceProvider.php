<?php namespace WL\Modules\Page\Providers;

use Cartalyst\Support\ServiceProvider;
use WL\Modules\Page\Repositories\DbPageRepository;
use WL\Modules\Page\Repositories\PageRepository;

class PageServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(PageRepository::class, DbPageRepository::class);
	}

}

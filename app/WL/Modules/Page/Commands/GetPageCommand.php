<?php namespace WL\Modules\Page\Commands;



use WL\Modules\Page\Facades\PageService;
use WL\Security\Commands\AuthorizableCommand;

class GetPageCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'pages.getPageCommand';

	function __construct($slug, $lang = null){
		$this->slug = $slug;
		$this->lang = $lang;
	}

	function handle(){
		return PageService::getBySlug($this->slug, $this->lang);
	}
}

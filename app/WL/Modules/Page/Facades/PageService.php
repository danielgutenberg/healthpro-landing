<?php namespace WL\Modules\Page\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Page\Services\PageServiceImpl;

class PageService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return PageServiceImpl::class;
	}
}

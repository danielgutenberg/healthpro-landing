<?php
namespace WL\Modules\Page\Models;

use App\Response;
use WL\Contracts\Presentable;
use WL\Models\ModelBase;
use WL\Models\MetaTrait;
use WL\Models\PresentableTrait;
use WL\Models\TreeTrait;
use Sentinel;
use WL\Modules\Meta\Models\Meta;
use WL\Translator\Models\TranslatableTrait;
use InvalidArgumentException;
use Illuminate\Support\Facades\URL;
use Sitemap;

class Page extends ModelBase implements Presentable
{
	const DEFAULT_VIEW = 'site.page.show';

	use MetaTrait;

	use TreeTrait;

	use TranslatableTrait;

	use PresentableTrait;


	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'pages';

	protected $morphClass = 'WL\Modules\Page\Models\Page';

	protected $metaClassName = Meta::class;

	protected $presenter = 'WL\Modules\Page\Presenters\PagePresenter';


	/**
	 * Fillable model params
	 *
	 * @var array
	 */
	protected $fillable = ['slug', 'parent_id', 'title', 'content', 'active', 'rank', 'password', 'redirect'];

	/**
	 * These fields will be filled while creating a translation
	 *
	 * @var array
	 */
	protected $translateFillable = ['user_id', 'slug', 'parent_id', 'title', 'content', 'active', 'rank', 'password', 'redirect'];

	/**
	 * Fields that will be copied to the translation on request from the source
	 *
	 * @var array
	 */
	protected $translatable = ['title', 'content'];

	/**
	 * Default Validation Rules
	 *
	 * @var array
	 */
	protected $rules = array(
		'slug'      => 'required|alpha_dash|min:3',
		'parent_id' => 'required|integer',
		'title'     => 'required',
		'content'   => '',
		'active'    => 'integer',
		'rank'      => 'integer',
	);

	/**
	 * Returns a formatted post content entry,
	 * this ensures that line breaks are returned.
	 *
	 * @return string
	 */
	public function filteredContent()
	{
		// do something with content
		return $this->content;
	}

	/**
	 * Get the URL to the post.
	 *
	 * @return string
	 */
	public function url()
	{
		return Url::to($this->slug);
	}

	/**
	 * Get full/short URL to the current page
	 *
	 * @param bool $ancestors
	 * @return string
	 */
	public function getUrl( $ancestors = true )
	{
		if (false === $ancestors) {
			return $this->slug;
		}

		$ancestors = array_reverse($this->getAncestors());

		$parts = array_map( function($ancestor) {
			return $ancestor->slug;
		}, $ancestors );

		$parts[] = $this->slug;

		return implode('/', $parts);
	}

	/**
	 * Include self to sitemap index
	 * @return Response
	 */
	public function handleSitemapIndex()
	{
		Sitemap::addSitemap(route('sitemap',['pages']), self::max('updated_at') );
	}

	/**
	 * Generate sitemap page
	 * @return Response
	 */
	public function handleSitemapPage()
	{
		foreach (self::all() as $page) {
			Sitemap::addTag(route('page.show', $page->slug), $page->created_at);
		}
	}

	public function getViewName()
	{
		return $this->getMeta('view') ?: self::DEFAULT_VIEW;
	}
}

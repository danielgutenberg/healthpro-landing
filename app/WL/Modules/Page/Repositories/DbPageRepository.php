<?php

namespace WL\Modules\Page\Repositories;

use WL\Modules\Page\Models\Page;
use WL\Repositories\DbRepository;
use WL\Modules\Page\Repositories\PageRepository;
use WL\Translator\Facades\DBTranslator;
use WL\Modules\Page\Repositories\Exceptions\InvalidPathException;

class DbPageRepository extends DbRepository implements PageRepository
{
	/**
	 * @var \WL\Modules\Page\Models\Page
	 */
	protected $model;

	protected $modelClassName = '\WL\Modules\Page\Models\Page';

	public function __construct( Page $model )
	{
		$this->model = $model;
	}

	/**
	 * @param string $slug
	 * @param string|null $lang
	 *
	 * @return \WL\Modules\Page\Models\Page
	 */
	public function getBySlug( $slug, $lang = null )
	{
		if (null === $lang) {
			$lang = DBTranslator::currentLanguage();
		}

		$table = $this->model->getTable();
		$langTable = $this->model->translation()->getModel()->getTable();

		return $this->model
			->select($table . '.*', $langTable . '.lang')
			->join($langTable, $table . '.id', '=', $langTable . '.elm_id')
			->where($table . '.slug', $slug)
			->where($langTable . '.lang',      '=', $lang)
			->where($langTable . '.elm_type',  '=', $this->model->getMorphClass())
			->firstOrFail();
	}

	/**
	 * @param string $uri
	 * @param null|string $lang
	 * @throws \WL\Modules\Page\Repositories\Exceptions\InvalidPathException
	 * @return Page
	 */
	public function getByUri( $uri, $lang = null )
	{
		$parts = array_reverse( explode('/', $uri) );

		$page = $this->getBySlug( $parts[0] );

		$correctUrl = $page->getUrl();

		if ( $uri !== $correctUrl ) {
			throw new InvalidPathException( $correctUrl, 'Page path is different' );
		}

		return $this->getBySlug( $parts[0] );
	}

	public function getFlatList( $filters = [] )
	{
		/**
		 * @TODO add filters later
		 */
		return $this->model->getOneLevelTree();
	}
}

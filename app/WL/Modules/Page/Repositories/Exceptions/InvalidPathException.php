<?php
namespace WL\Modules\Page\Repositories\Exceptions;

use Exception;

class InvalidPathException extends Exception
{
	/**
	 * @var string
	 */
	private $path;

	public function __construct( $path, $message = '', $code = 0 ) {
		parent::__construct( $message, $code );

		$this->path = $path;
	}

	/**
	 * Get correct URL for the Page
	 *
	 * @return string
	 */
	public function getCorrectPath()
	{
		return $this->path;
	}
}

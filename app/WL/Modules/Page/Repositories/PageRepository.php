<?php

namespace WL\Modules\Page\Repositories;

interface PageRepository
{
	/**
	 * Get page by slug
	 *
	 * @param $slug
	 * @param $lang
	 * @return mixed
	 */
	public function getBySlug( $slug, $lang );

	/**
	 * Get page by the full URI
	 *
	 * @param $lang
	 * @return mixed
	 */
	public function getByUri( $lang );

	/**
	 * Get a list of pages with filters
	 *
	 * @param $filters
	 * @return mixed
	 */
	public function getFlatList( $filters = [] );
}

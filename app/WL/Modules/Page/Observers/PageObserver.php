<?php
namespace WL\Modules\Page\Observers;

use WL\Modules\Page\Models\Page;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use WL\Modules\Attachment\Facades\AttachmentService;

class PageObserver
{
	public function creating( Page $model )
	{
		// on the raw save we don't need to handle any events
		if ( $model->ignoreEvents ) {
			return true;
		}

		if (!$model->user_id) {
			$model->user_id = Sentinel::check()->id;
		}
	}

	public function created( Page $model )
	{
		if ( $model->ignoreEvents ) {
			return true;
		}

		return $this->saveTranslation( $model );
	}

	public function saved(Page $model)
	{
		if ( $model->ignoreEvents ) {
			return true;
		}

		return $this->uploadFiles( $model ) && $this->saveMeta( $model );
	}

	public function deleting( Page $model )
	{
		// Re-assign page children parents, delete meta and only then delete the page
		$this->reassignChildren();
		$this->meta()->delete();

		return true;
	}

	/**
	 * @param Page $model
	 *
	 * @return bool
	 */
	public static function saveTranslation( Page $model )
	{
		if ( !$model->hasFeeder() ) {
			return true;
		}

		$translation = $model->getFeeder()->translation();

		// filter language
		$lang = isset( $translation['lang'] ) ? $translation['lang'] : '';

		return $model->updateTranslation( $lang );
	}

	/**
	 * Save meta
	 * @param Page $model
	 * @return mixed
	 */
	public static function saveMeta( Page $model )
	{
		if ( !$model->hasFeeder() ) {
			return true;
		}

		$meta = $model->getFeeder()->meta();

		return $model->updateMeta( $meta );
	}


	public static function uploadFiles(Page $model)
	{
		if ( !$model->hasFeeder() ) {
			return true;
		}

		foreach ($model->getFeeder()->attachments() as $name => $file) {
			try {
				AttachmentService::uploadImage($name, $model, $file);
			} catch (\Exception $e) {
				$model->errors()->add($name, $e->getMessage());

				return false;
			}
		}

		return true;
	}
}

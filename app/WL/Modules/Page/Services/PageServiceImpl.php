<?php namespace WL\Modules\Page\Services;

use WL\Modules\Page\Models\Page;
use WL\Modules\Page\Repositories\DbPageRepository;

class PageServiceImpl implements PageServiceInterface
{
	/**
	 * @var DbPageRepository
	 */
	public $repo;

	/**
	 *
	 */
	public function __construct()
	{
		$this->repo = new DbPageRepository(new Page());
	}

	public function getBySlug($slug, $lang = null)
	{
		return $this->repo->getBySlug($slug, $lang);
	}
}

<?php namespace WL\Modules\Notification\Commands;


use WL\Modules\Notification\Facades\NotificationService;
use WL\Security\Commands\AuthorizableCommand;

class DeleteNotificationCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'notification.deleteNotificationCommand';

	function __construct($notificationId){
		$this->notificationId = $notificationId;
	}

	function handle(){
		return NotificationService::destroyResultByIds([$this->notificationId]);
	}
}

<?php namespace WL\Modules\Notification\Commands;


use WL\Modules\Notification\Facades\NotificationService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Commands\AuthorizableCommand;

class LoadMyOutboxNotificationsListCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'notification.loadMyOutboxNotificationsListCommand';

	function __construct($fields = [],$filters = [],$pagination = [],$sort = []){
		$this->fields     = $fields;
		$this->filters    = $filters;
		$this->pagination = $pagination;
		$this->sort       = $sort;
	}

	function handle(){
		return NotificationService::getOutboxNotifications(ProfileService::getCurrentProfile(),$this->fields,$this->filters,$this->pagination,$this->sort);
	}
}

<?php namespace WL\Modules\Notification\Commands;


use WL\Modules\Notification\SecurityPolicies\NotificationsAccessPolicy;
use WL\Security\Commands\AuthorizableCommand;

class OtherProfileNotificationCommand extends AuthorizableCommand
{
    protected $securityPolicy;

    public function __construct()
    {
        $this->securityPolicy = new NotificationsAccessPolicy();
    }
}
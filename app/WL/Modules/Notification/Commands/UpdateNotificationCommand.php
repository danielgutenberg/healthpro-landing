<?php namespace WL\Modules\Notification\Commands;


use WL\Modules\Notification\Facades\NotificationService;
use WL\Security\Commands\AuthorizableCommand;

class UpdateNotificationCommand extends AuthorizableCommand
{

	const IDENTIFIER = 'notification.updateNotificationCommand';

	function __construct($notificationId,array $data){
		$this->notificationId = $notificationId;
		$this->data = $data;

	}

	function handle(){
		return NotificationService::updateById($this->notificationId,$this->data,true);
	}
}

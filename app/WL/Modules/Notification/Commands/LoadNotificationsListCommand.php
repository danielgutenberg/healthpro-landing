<?php namespace WL\Modules\Notification\Commands;


use WL\Modules\Notification\Facades\NotificationService;
use WL\Security\Commands\AuthorizableCommand;

class LoadNotificationsListCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'notification.loadNotificationsListCommand';

	function __construct($fields = [],$filters = [],$pagination = [],$sort = []){
		$this->fields     = $fields;
		$this->filters    = $filters;
		$this->pagination = $pagination;
		$this->sort       = $sort;
	}

	function handle()
	{
		return NotificationService::getAllNotifications($this->fields,$this->filters,$this->pagination,$this->sort);
	}

}

<?php namespace WL\Modules\Notification\Commands;


use Illuminate\Database\Eloquent\Collection;
use WL\Modules\Notification\Facades\NotificationService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Services\Exceptions\AuthorizationException;

class ReadNotificationsListByIdsCommand extends OtherProfileNotificationCommand
{
	const IDENTIFIER = 'notification.readNotificationsListByIdsCommand';

	function __construct($profileId, $params, $read)
	{
	    parent::__construct();
		$this->profileId = $profileId;
		$this->params = $params;
		$this->read = $read;
	}

	function handle()
	{
		$notifications = [];

		if ($profile = ProfileService::getProfileById($this->profileId)) {
			$notifications = NotificationService::getInboxNotifications(
				$profile,
				[],
				$this->params['filters'],
				[
					$this->params['page'],
					$this->params['perPage']
				],
				$this->params['sorts']);
		}

		return NotificationService::readNotifications(
			($notifications instanceof Collection) ? $notifications->toArray() : $notifications,
			$profile,
			$this->read
		);

	}
}

<?php namespace WL\Modules\Notification\Commands;


use WL\Modules\Notification\Facades\NotificationService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Services\Exceptions\AuthorizationException;

class LoadInboxNotificationsCommand extends OtherProfileNotificationCommand
{
	const IDENTIFIER = 'notification.loadInboxNotificationsCommand';

	function __construct($profileId, $fields = [], $filters = [], $pagination = [], $sort = [])
	{
		$this->profileId = $profileId;
		$this->fields = $fields;
		$this->filters = $filters;
		$this->pagination = $pagination;
		$this->sort = $sort;
	}

	function handle()
	{
        $profile = ProfileService::getProfileById($this->profileId);
		return NotificationService::getInboxNotifications($profile, $this->fields, $this->filters, $this->pagination, $this->sort);
	}
}

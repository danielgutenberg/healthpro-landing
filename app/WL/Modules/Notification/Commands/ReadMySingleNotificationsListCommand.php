<?php namespace WL\Modules\Notification\Commands;


use Illuminate\Support\Collection;
use WL\Modules\Notification\Facades\NotificationService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Facades\AuthorizationUtils;

class ReadMySingleNotificationsListCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'notification.readMySingleNotificationsListCommand';

	function __construct($params, $read)
	{
		$this->params = $params;
		$this->read = $read;
	}

	function handle()
	{
		$profile = ProfileService::getCurrentProfile();
		$notifications = NotificationService::getInboxNotifications(
			$profile,
			[],
			$this->params);

		return NotificationService::readNotifications(
			($notifications instanceof Collection) ? $notifications->toArray() : $notifications,
			$profile,
			$this->read
		);

	}
}

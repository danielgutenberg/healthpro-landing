<?php namespace WL\Modules\Notification\Commands;


use WL\Modules\Notification\Facades\NotificationService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Facades\AuthorizationUtils;

class LoadMyInboxNotificationsListCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'notification.loadMyInboxNotificationsListCommand';

	function __construct($fields = [],$filters = [],$pagination = [],$sort = []){
		$this->fields     = $fields;
		$this->filters    = $filters;
		$this->pagination = $pagination;
		$this->sort       = $sort;
	}

	function handle(){
		return NotificationService::getInboxNotifications(ProfileService::getCurrentProfile(),$this->fields,$this->filters,$this->pagination,$this->sort);
	}
}

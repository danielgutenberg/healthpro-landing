<?php namespace WL\Modules\Notification\Commands;


use WL\Modules\Notification\Facades\NotificationService;
use WL\Security\Commands\AuthorizableCommand;

class LoadNotificationCommand extends AuthorizableCommand {

	const IDENTIFIER = 'notification.loadNotificationCommand';

	function __construct($notificationId,$getParent = true,$relations = false){
		$this->notificationId = $notificationId;
		$this->getParent           = $getParent;
		$this->relations      = $relations;
	}

	function handle(){
		if($this->getParent){
			return NotificationService::getById($this->notificationId,$this->relations);
		}else{
			return NotificationService::getResultById($this->notificationId,$this->relations);
		}

	}
}

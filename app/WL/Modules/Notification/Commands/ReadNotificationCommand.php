<?php namespace WL\Modules\Notification\Commands;


use WL\Modules\Notification\Facades\NotificationService;
use WL\Security\Services\Exceptions\AuthorizationException;

class ReadNotificationCommand extends OtherProfileNotificationCommand
{
	const IDENTIFIER = 'notification.readNotificationCommand';

	function __construct($profileId, $notificationId, $read)
	{
		$this->profileId = $profileId;
		$this->notificationId = $notificationId;
		$this->read = $read;
	}

	function handle()
	{
		$result = NotificationService::readNotification(
			$this->notificationId,
			$this->profileId,
			$this->read
		);

		return $result;

	}
}

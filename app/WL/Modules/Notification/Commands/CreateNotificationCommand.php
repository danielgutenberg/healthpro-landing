<?php namespace WL\Modules\Notification\Commands;

use Carbon\Carbon;

use WL\Modules\Notification\Facades\NotificationService;
use WL\Security\Commands\AuthorizableCommand;

class CreateNotificationCommand extends AuthorizableCommand
{

	const IDENTIFIER = 'notification.createNotificationCommand';

	function __construct($data){
		$this->data = $data;
	}

	function handle()
	{
		return NotificationService::createNotification(
			['id'=>$this->data['sender_id'],'type'=>$this->data['sender_type']],
			$this->data['dispatchers'],
			$this->data['title'],
			$this->data['description'],
			isset($this->data['expire_at'])?$this->data['expire_at']:Carbon::now()->addDays(1)->toDateTimeString(),
			isset($this->data['type'])?$this->data['type']:''
		);

	}
}

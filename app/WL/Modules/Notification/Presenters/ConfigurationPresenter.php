<?php namespace WL\Modules\Notification\Presenters;

/**
 * Presentation for KeyValue config storage
 */
class ConfigurationPresenter
{
	/**
	 * @var array
	 */
	public $data = [];

	/**
	 * @param $name
	 * @param $value
	 */
	function __set($name, $value)
	{
		$data = &$this->data;
		if (!isset($data[$name])) {
			$data[$name] = [];
		}
		array_push($data[$name], $value);
	}

	/**
	 * @param $name
	 * @return null
	 */
	function __get($name)
	{
		$data = $this->data;
		$clean = false;
		if ($name[0] == '_') {
			$clean = true;
			$name = substr($name, 1);
		}

		if (isset($data[$name])) {
			if (count($data[$name]) > 1 || $clean) {
				return $data[$name];
			} else {
				return $data[$name][0];
			}
		}
		return null;
	}
}

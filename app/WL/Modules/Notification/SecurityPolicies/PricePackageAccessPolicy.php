<?php namespace WL\Modules\Notification\SecurityPolicies;

use WL\Modules\Notification\Facades\NotificationService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\SecurityPolicies\ProfileAccessPolicy;
use WL\Security\SecurityPolicies\BaseAccessPolicy;

class NotificationsAccessPolicy extends BaseAccessPolicy
{
	public function canReadProfileNotifications($profileId)
	{
        return (new ProfileAccessPolicy())->isCurrentProfile($profileId);
	}

	public function canLoadProfileNotifications($profileId)
	{
        return (new ProfileAccessPolicy())->isCurrentProfile($profileId);
	}

    public function canLoadOutboxProfileNotifications($profileId)
    {
        return $this->canLoadProfileNotifications($profileId);
    }

    public function canReadNotification($profileId, $notificationId)
    {
        return $this->isOwnedNotification($profileId, $notificationId) && (new ProfileAccessPolicy())->isCurrentProfile($profileId);
    }

    protected function isOwnedNotification($profileId, $notificationId)
    {
        $notification = NotificationService::getById($notificationId);
        return $notification->dispatcher_id == $profileId || $notification->dispatcher_type === ModelProfile::class;
    }
}

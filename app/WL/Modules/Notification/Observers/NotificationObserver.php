<?php namespace WL\Modules\Notification\Observers;

use WL\Modules\Notification\Facades\NotificationService;

class NotificationObserver
{

	public function created($model)
	{
		NotificationService::createByNotifierEntity($model,$model->notifierConfig(__FUNCTION__));
	}

	public function deleting($model)
	{
		NotificationService::removeBySender($model);
	}

	public function updated($model){
		NotificationService::updateByNotifierEntity($model,$model->notifierConfig(__FUNCTION__));
	}

}

<?php namespace WL\Modules\Notification\Observers;

use WL\Modules\Notification\Facades\NotificationService;

class NotificationSelfObserver
{

	public function created($model)
	{
		//implementing pull-principle, so disabling push-notifications
		//NotificationService::dispatchResults($model);

	}

	public function deleting($model)
	{
		NotificationService::removeNotificationResults($model);
	}

}

<?php namespace WL\Modules\Notification\Facades;

use Illuminate\Support\Facades\Facade;

class NotificationService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'NotificationService';
	}
}

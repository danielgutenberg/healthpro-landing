<?php namespace WL\Modules\Notification\Models;

/**
 * Interface NotifiableInterface
 */
interface NotifiableInterface {

    function getNotifications($seen,$since);

	public static function bootNotifiableTrait();


}

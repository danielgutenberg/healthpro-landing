<?php namespace WL\Modules\Notification\Models;

/**
 * Interface NotifierInterface
 * Used with NotifierTrait to observe and notify about model events
 */
interface NotifierInterface
{

	public static function bootNotifierTrait();

	/**
	 * What entities to notify about current entity model events
	 * @return mixed
	 */
	public function notifierDispatchers();

	public function notifierConfig($action);
}

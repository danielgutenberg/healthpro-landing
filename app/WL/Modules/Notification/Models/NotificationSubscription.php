<?php namespace WL\Modules\Notification\Models;

use Illuminate\Database\Eloquent\Model;
use WL\Models\ModelBase;

class NotificationSubscription extends ModelBase
{
	const STATUS_DISABLED = 0;
	const STATUS_ACTIVE   = 1;


	protected $table = 'notification_subscription';


	/**
	 * Default Validation Rules
	 *
	 * @var array
	 */
	protected $rules = array(
		'subscriber_id'   => 'required|integer',
		'subscriber_type' => 'required',
		'dispatcher_id'   => 'required|integer',
		'dispatcher_type' => 'required',
	);

	protected $fillable = ['subscriber_id','subscriber_type','dispatcher_id','dispatcher_type','status'];

	public $timestamps = false;


}

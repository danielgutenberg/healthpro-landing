<?php namespace WL\Modules\Notification\Models;

use WL\Modules\Notification\Observers\NotificationObserver;

/**
 * Class NotifierTrait
 * Used with NotifierInterface to observe and notify about current model events
 */
trait NotifierTrait
{
	/**
	 *
	 */
	public static function bootNotifierTrait()
	{
		static::observe(new NotificationObserver());
	}
}

<?php namespace WL\Modules\Notification\Models;

use WL\Models\ModelBase;

/** KeyValue store for configuration
 * Class NotificationConfiguration
 * @package App
 */
class NotificationConfig extends ModelBase
{

	/**
	 * @var string
	 */
	protected $table = 'notification_configuration';

	/**
	 * Default Validation Rules
	 *
	 * @var array
	 */
	protected $rules = array(
		'owner_id' => 'required|integer',
		'owner_type' => 'required',
		'key' => 'required|',
		'value' => 'required',
	);


}

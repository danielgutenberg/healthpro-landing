<?php namespace WL\Modules\Notification\Models;

use WL\Models\ModelBase;

class NotificationResult extends ModelBase
{

	protected $table = 'notification_results';

	protected $primaryKey = 'rid';

	/**
	 * Default Validation Rules
	 *
	 * @var array
	 */
	protected $rules = array(
		'receiver_id' => 'required|integer',
		'receiver_type' => 'required',
		'notification_id' => 'required|integer',
	);

	protected $fillable = ['notification_id','receiver_id','receiver_type'];

	public $timestamps = false;

	public function notification(){
		return $this->belongsTo('WL\Modules\Notification\Models\Notification','notification_id','id');
	}

}

<?php namespace WL\Modules\Notification\Models;

//use WL\Models\ModelTrait;
use WL\Models\ModelBase;

class Notification extends ModelBase
{
	//use ModelTrait;

	protected $table = 'notifications';

	/**
	 * Default Validation Rules
	 *
	 * @var array
	 */
	protected $rules = array(
		'sender_id' => 'required|integer',
		'sender_type' => 'required',
		'dispatcher_id' => 'required|integer',
		'dispatcher_type' => 'required',
		'title' => 'required',
		'description' => 'required',
		'type' => 'required'

	);


	public function results(){
		return $this->hasMany('WL\Modules\Notification\Models\NotificationResult','notification_id','id');
	}
}

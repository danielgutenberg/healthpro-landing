<?php namespace WL\Modules\Notification\Models;

use WL\Modules\Notification\Facades\NotificationService;
use WL\Modules\Notification\Models\NotificationSubscription;

/**
 * Class NotifiableTrait
 * @package WL\Models
 */
trait NotifiableTrait
{

	/** retrive notifications belongs to current Entity
	 * @param bool $seen
	 * @return array list of notifications
	 */
	function getNotifications($seen = false, $since = null)
	{
		return NotificationService::getNotifications($this, $seen);
	}


	public static function bootNotifiableTrait()
	{
		static::deleted(function ($model) {
			NotificationService::removeByReceiver($model);
			NotificationService::removeSubscriptions($model);
		});
	}

	public function fetchDispatchers($status = NotificationSubscription::STATUS_ACTIVE)
	{
		$subscriptions = NotificationService::getSubscriberDispatchers($this->id, get_class($this), $status);

		//entity is always self-dispatcher for non-group notifications compatibility
		$dispatchers[] = $this;

		foreach ($subscriptions->toArray() as $subscription) {
			$dispatchers[] = $subscription['dispatcher_type']::find($subscription['dispatcher_id']);
		}

		return $dispatchers;
	}

	public function subscribeForNotificationDispatchers($dispatchers)
	{
		NotificationService::subscribe($this, $dispatchers);
	}

}

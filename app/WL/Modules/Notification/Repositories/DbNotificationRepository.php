<?php namespace WL\Modules\Notification\Repositories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use WL\Modules\Notification\Models\Notification;
use WL\Modules\Notification\Models\NotificationResult;

use WL\Repositories\DbRepository;
use Illuminate\Support\Facades\DB;
use WL\Modules\Notification\Exceptions\NotificationException;


class DbNotificationRepository extends DbRepository implements NotificationRepositoryInterface
{

	use RepositoryHelperTrait;

	/**
	 * @var string
	 */
	protected $modelClassName = Notification::class;

	/**
	 * @param Notification $model
	 */
	public function __construct(Notification $model)
	{
		$this->model = $model;
	}


	public function saveNotification($notification)
	{
		if (isset($notification))

			if ($notification->save()) {
				return $notification;
			} else
				throw new NotificationException("model parameters required");

		throw new NotificationException('notification can`t be null');
	}


	public function getNotifications($receiver, $seen = false)
	{

		if (is_array($receiver) && isset($receiver['id'], $receiver['type'])) {
			$receiver = $receiver['type']::find($receiver['id']);
		} elseif (!(is_object($receiver) && is_subclass_of($receiver, Model::class))) {
			return [];
		}
		$dispatchers = $receiver->fetchDispatchers();
		$query = DB::table('notifications')->leftJoin('notification_results as results', 'notifications.id', '=', 'results.notification_id')
			->select('*')
			->where(function ($query) use ($dispatchers, $seen, $receiver) {
				$query->where('seen', $seen)
					->where('receiver_id', $receiver->id)
					->where('receiver_type', get_class($receiver))
					->where('expire_at', '>=', Carbon::now()->toDateTimeString());
				if (!$seen) {

					$query->orWhere(function ($query) use ($dispatchers) {

						$query->whereNull('notification_id')
							->where(function ($query) use ($dispatchers) {

								foreach ($dispatchers as $k => $dispatcher) {

									$query->orWhere(function ($query) use ($dispatcher) {
										$query->where('dispatcher_id', $dispatcher->id)
											->where('dispatcher_type', get_class($dispatcher));
									});
								}

							});
					});
				}

			});

		$rawData = $query->get();
		return $rawData;
	}

	public function updateByNotifierEntity($causingEntity, $notifierConfig)
	{
		if (isset($notifierConfig['title'], $notifierConfig['description'], $notifierConfig['type'], $notifierConfig['expire_at'])) {

			Notification::where('sender_id', $causingEntity->id)
				->where('sender_type', get_class($causingEntity))
				->update([

					'title' => $notifierConfig['title'],

					'description' => $notifierConfig['description'],

					'expire_at' => $notifierConfig['expire_at'],

					'type' => $notifierConfig['type']

				]);

		}

	}

	public function removeBySender($sender)
	{
		$notifications = Notification::where('sender_id', $sender->id)
			->where('sender_type', get_class($sender))
			->with('results')
			->get();
		foreach($notifications as $notification){
			$notification->delete();
		}

	}

	public function removeNotificationResult($notification)
	{
		if($notification instanceof Notification){
			return $notification->results()->delete();
		}elseif(is_numeric($notification)){
			return Notification::find($notification)->results()->delete();
		}

		return [];
	}

	public function removeReceiverNotification($receiver)
	{
		$options = $this->extractQueryParameters($receiver);

		NotificationResult::where('receiver_type', $options['type'])
			->where('receiver_id', $options['id'])
			->delete();
	}



}

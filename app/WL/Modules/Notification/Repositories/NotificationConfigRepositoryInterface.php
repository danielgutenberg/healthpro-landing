<?php namespace WL\Modules\Notification\Repositories;

use WL\Repositories\Repository;

interface NotificationConfigRepositoryInterface extends Repository {

	/** Get configuration for $owner, build into ConfigurationPresenter
	 * @param $owner
	 * @return ConfigurationPresenter
	 */
	public function getByOwner($owner);

	/** remove all configuration of $owner
	 * @param $owner
	 * @return mixed
	 */
	public function removeByOwner($owner);

	/** Remove and then fill up configuration for $owner, using ConfigurationPresenter
	 * @param $owner
	 * @param ConfigurationPresenter $configuration
	 */
	public function setClearFields($owner, $configuration);

	/** Update or Add configuration field to $owner
	 * @param $owner
	 * @param $fieldName
	 * @param $fieldValue
	 * @param bool $add
	 * @return bool
	 */
	public function setField($owner, $fieldName, $fieldValue, $add = false);
}

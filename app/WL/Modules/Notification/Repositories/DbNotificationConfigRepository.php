<?php namespace WL\Modules\Notification\Repositories;

use WL\Modules\Notification\Presenters\ConfigurationPresenter;
use WL\Modules\Notification\Models\NotificationConfig;
use WL\Repositories\DbRepository;


class DbNotificationConfigRepository extends DbRepository implements NotificationConfigRepositoryInterface
{
	use RepositoryHelperTrait;

	public function getByOwner($owner)
	{
		$options = $this->extractQueryParameters($owner);
		$items = NotificationConfig::where('owner_id', $options['id'])->where('owner_type', $options['type'])->get();

		$configp = new ConfigurationPresenter();
		foreach ($items as $item) {
			$configp->{$item->key} = $item->value;
		}
		return $configp;
	}


	public function removeByOwner($owner)
	{
		$options = $this->extractQueryParameters($owner);
		return NotificationConfig::where('owner_id', $options['id'])->where('owner_type', $options['type'])->delete();
	}


	public function setClearFields($owner, $configuration)
	{
		$this->removeByOwner($owner);
		foreach ($configuration->data as $key => $val) {

			foreach ($val as $item) {
				$this->setField($owner, $key, $item, true);
			}
		}
	}


	public function setField($owner, $fieldName, $fieldValue, $add = false)
	{
		$options = $this->extractQueryParameters($owner);
		$item = null;
		if (!$add) {
			$item = NotificationConfig::firstOrNew(array(
				'owner_id' => $options['id'],
				'owner_type' => $options['type'],
				'key' => $fieldName,
			));
		} else {
			$item = new NotificationConfig();
		}

		$item->fill([
			'owner_id' => $options['id'],
			'owner_type' => $options['type'],
			'key' => $fieldName,
			'value' => $fieldValue
		]);

		return $item->save();
	}
}

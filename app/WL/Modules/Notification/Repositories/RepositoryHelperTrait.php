<?php namespace WL\Modules\Notification\Repositories;

use Illuminate\Support\Facades\DB;

trait RepositoryHelperTrait
{
	/** Extracting parameters from var
	 * @param mixed $receiver accept Model or [id , type]
	 * @return array
	 */
	protected function extractQueryParameters($extractable)
	{
		if (isset($extractable)) {
			if (is_array($extractable)) {
				return ['id' => $extractable[0], 'type' => $extractable[1]];
			} else {
				return ['id' => $extractable->id, 'type' => get_class($extractable)];
			}
		}

		return ['id' => '', 'type' => ''];
	}

////////////////////////////////////////////////////
	function getAllNotificationsQuery($receiverId = null, $receiverType = null)
	{

		$query = DB::table('notifications');
		$query->leftJoin('notification_results', function ($query) use ($receiverId, $receiverType) {
			$query->on('notifications.id', '=', 'notification_results.notification_id');
			if ($receiverId != null && $receiverType != null) {
				$query->on("notification_results.receiver_id", '=', DB::raw($receiverId));
				$query->on("notification_results.receiver_type", '=', DB::raw(DB::connection()->getPdo()->quote($receiverType)));
			}
		});

		return $query;
	}

	function getAllOutboxNotificationsQuery($sender)
	{
		$query = $this->getAllNotificationsQuery();
		$this->applySenders($query, [$sender]);

		return $query;
	}

	function applyReceivers(&$query, array $receivers)
	{

		$query->where(function ($query) use ($receivers) {
			foreach ($receivers as $receiver) {
				$options = $this->extractQueryParameters($receiver);

				$query->orWhere(function ($query) use ($options) {
					$query->where('receiver_id', $options['id'])
						->where('receiver_type', $options['type']);
				});

			}

		});

	}

	function applyDispatchers(&$query, array $dispatchers)
	{
		$query->where(function ($query) use ($dispatchers) {
			foreach ($dispatchers as $dispatcher) {
				$options = $this->extractQueryParameters($dispatcher);

				$query->orWhere(function ($query) use ($options) {
					$query->where('dispatcher_id', $options['id'])
						->where('dispatcher_type', $options['type']);
				});

			}

		});

	}


	function applySenders(&$query, array $senders)
	{
		$query->where(function ($query) use ($senders) {
			foreach ($senders as $sender) {
				$options = $this->extractQueryParameters($sender);

				$query->orWhere(function ($query) use ($options) {
					$query->where('sender_id', $options['id'])
						->where('sender_type', $options['type']);
				});

			}

		});

	}

	/**
	 * @param $query
	 * @param array $filters [['field_name','=','field_value'],['field_name','!BETWEEN','value1,value2'],['field_name','IN','value1,value2'],['field_name','!NULL','']]
	 */
	function applyFilters(&$query, $filters)
	{
		foreach ($filters as $filter) {

			if ($filter[1] == 'IN') {
				if (is_string($filter[2])) $filter[2] = explode(",", $filter[2]);
				$query->whereIn($filter[0], $filter[2]);
			} elseif ($filter[1] == '!IN') {
				if (is_string($filter[2])) $filter[2] = explode(",", $filter[2]);
				$query->whereNotIn($filter[0], $filter[2]);
			} elseif ($filter[1] == 'BETWEEN') {
				if (is_string($filter[2])) $filter[2] = explode(",", $filter[2]);
				$query->whereBetween($filter[0], $filter[2]);
			} elseif ($filter[1] == '!BETWEEN') {
				if (is_string($filter[2])) $filter[2] = explode(",", $filter[2]);
				$query->whereNotBetween($filter[0], $filter[2]);
			} elseif ($filter[1] == 'NULL') {
				$query->whereNull($filter[0]);
			} elseif ($filter[1] == '!NULL') {
				$query->whereNotNull($filter[0]);
			} else {
				$query->where($filter[0], $filter[1], $filter[2]);
			}

		}

	}

	function applyRange(&$query, $start = 0, $end = 20)
	{
		$query->skip((int)$start)->take((int)($end - $start));
	}

	function applyPagination(&$query, $page = 0, $perPage = 20)
	{
		if ($perPage) $query->skip((int)($page * $perPage))->take((int)($perPage));
	}

	function applyFields(&$query, $fields = [])
	{
		if ($fields) {
			foreach ($fields as $field) {

				if (is_array($field)) {
					$query->addSelect($field[0] . " as " . $field[1]);
				} else {
					$query->addSelect($field);
				}

			}
		} else {
			$query->select("*");
		}

	}

	function applyOrder(&$query, $sorts = [])
	{
		if ($sorts) {
			foreach ($sorts as $sort) {
				$query->orderBy($sort[0], $sort[1]);
			}
		}

	}
}

<?php namespace WL\Modules\Notification\Repositories;

use WL\Modules\Notification\Models\Notification;
use WL\Modules\Notification\Exceptions\NotificationException;
use WL\Repositories\Repository;

interface NotificationRepositoryInterface extends Repository
{

	/** Saving Notificaiton
	 * @param Notification $notification
	 * @throws NotificationException
	 */
	public function saveNotification($notification);

	/** Get Seen or Unseen Notifications
	 * @param mixed $receiver accept Model or [id , type]
	 * @param bool $seen
	 * @return mixed
	 */
	public function getNotifications($receiver,$seen);


}





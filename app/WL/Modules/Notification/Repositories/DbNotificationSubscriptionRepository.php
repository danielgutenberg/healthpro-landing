<?php namespace WL\Modules\Notification\Repositories;

use WL\Modules\Notification\Models\NotificationSubscription;

class DbNotificationSubscriptionRepository implements NotificationSubscriptionRepositoryInterface
{
	public function getSubscriptions($subscriberId, $readerClass, $dispatcherId, $dispatcherType)
	{
		$result = NotificationSubscription::where('subscriber_id', $subscriberId)
			->where('subscriber_type', $readerClass)
			->where('dispatcher_id', $dispatcherId)
			->where('dispatcher_type', $dispatcherType)
			->get();

		return $result;
	}

	public function setSubscriptionStatus($subscriberId, $subscriberClass, $dispatcherId, $dispatcherType, $status)
	{
		$result = NotificationSubscription::where('subscriber_id', $subscriberId)
			->where('subscriber_type', $subscriberClass)
			->where('dispatcher_id', $dispatcherId)
			->where('dispatcher_type', $dispatcherType)
			->update(['status' => $status]);

		return $result;
	}

	public function getDispatchers($subscriberId, $subscriberType, $status)
	{
		$result = NotificationSubscription::where('subscriber_id', $subscriberId)
			->where('subscriber_type', $subscriberType)
			->where('status', $status)
			->get();

		return $result;
	}

	public function removeSubscriptions($subscriberId, $subscriberType)
	{
		$result = NotificationSubscription::where('subscriber_id', $subscriberId)
			->where('subscriber_type', $subscriberType)
			->delete();

		return $result;
	}


	public function removeSubscriptionByDispatcher($subscriberId, $subscriberClass, $dispatcherId, $dispatcherType)
	{
		$result = NotificationSubscription::where('subscriber_id', $subscriberId)
			->where('subscriber_type', $subscriberClass)
			->where('dispatcher_id', $dispatcherId)
			->where('dispatcher_type', $dispatcherType)
			->delete();

		return $result;
	}
}

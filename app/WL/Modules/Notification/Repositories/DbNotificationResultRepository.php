<?php namespace WL\Modules\Notification\Repositories;

use WL\Modules\Notification\Exceptions\NotificationException;
use WL\Modules\Notification\Models\NotificationResult;
use WL\Repositories\DbRepository;
use Illuminate\Support\Facades\DB;

class DbNotificationResultRepository extends DbRepository
{
	/**
	 * @var string
	 */
	protected $modelClassName = NotificationResult::class;

	/**
	 * @param NotificationResult $model
	 */
	public function __construct(NotificationResult $model)
	{
		$this->model = $model;
	}

}

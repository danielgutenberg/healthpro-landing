<?php namespace WL\Modules\Notification\Repositories;

/**
 * Interface NotificationSubscriptionRepositoryInterface
 * @package WL\Modules\Notification\Repositories
 */
interface NotificationSubscriptionRepositoryInterface
{
	/**
	 * @param $subscriberId
	 * @param $readerClass
	 * @param $dispatcherId
	 * @param $dispatcherType
	 * @return mixed
     */
	public function getSubscriptions($subscriberId, $readerClass, $dispatcherId, $dispatcherType);

	/**
	 * @param $subscriberId
	 * @param $subscriberClass
	 * @param $dispatcherId
	 * @param $dispatcherType
	 * @param $status
	 * @return mixed
     */
	public function setSubscriptionStatus($subscriberId, $subscriberClass, $dispatcherId, $dispatcherType, $status);

	/**
	 * @param $subscriberId
	 * @param $subscriberType
	 * @param $status
	 * @return mixed
     */
	public function getDispatchers($subscriberId, $subscriberType, $status);

	/**
	 * @param $subscriberId
	 * @param $subscriberType
	 * @return mixed
     */
	public function removeSubscriptions($subscriberId, $subscriberType);

	/**
	 * @param $subscriberId
	 * @param $subscriberClass
	 * @param $dispatcherId
	 * @param $dispatcherType
	 * @return mixed
     */
	public function removeSubscriptionByDispatcher($subscriberId, $subscriberClass, $dispatcherId, $dispatcherType);

}

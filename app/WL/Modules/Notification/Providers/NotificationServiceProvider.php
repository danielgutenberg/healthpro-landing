<?php namespace WL\Modules\Notification\Providers;

use WL\Modules\Notification\Models\Notification;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use WL\Modules\Notification\Models\NotificationResult;
use WL\Modules\Notification\Observers\NotificationResultSelfObserver;
use WL\Modules\Notification\Observers\NotificationSelfObserver;
use WL\Modules\Notification\Repositories\DbNotificationSubscriptionRepository;
use WL\Modules\Notification\Services\NotificationServiceImpl;

class NotificationServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		Event::listen('notification.add', NotificationServiceImpl::class . '@onNotificationAdd');

		$this->app->instance('NotificationService', new NotificationServiceImpl(new DbNotificationSubscriptionRepository()));
		$this->app->booting(function () {
			AliasLoader::getInstance()->alias('NotificationService', 'WL\Modules\Notification\Facades\NotificationService');
		});

	}

	public function boot()
	{
		Notification::observe(new NotificationSelfObserver());
		NotificationResult::observe(new NotificationResultSelfObserver());
	}
}

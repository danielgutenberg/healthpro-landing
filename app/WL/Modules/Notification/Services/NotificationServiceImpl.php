<?php namespace WL\Modules\Notification\Services;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use WL\Modules\Notification\Exceptions\NotificationException;
use WL\Modules\Notification\Models\Notification;
use WL\Modules\Notification\Models\NotificationResult;
use WL\Modules\Notification\Models\NotificationSubscription;
use WL\Modules\Notification\Repositories\DbNotificationRepository;
use WL\Modules\Notification\Repositories\DbNotificationResultRepository;
use WL\Modules\Notification\Repositories\NotificationSubscriptionRepositoryInterface;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Role\Models\Role;

class NotificationServiceImpl implements NotificationServiceInterface
{
	/**
	 * @var DbNotificationRepository
	 */
	public $repo;

	/**
	 * @var DbNotificationResultRepository
	 */
	public $resultRepo;

	public function __construct(NotificationSubscriptionRepositoryInterface $subscriptionRepository)
	{
		$this->repo = new DbNotificationRepository(new Notification());
		$this->resultRepo = new DbNotificationResultRepository(new NotificationResult());
		$this->subscriptionRepo = $subscriptionRepository;
	}


	public function onNotificationAdd($config)
	{
		return $this->createNotification(
			$config['sender'],
			$config['dispatchers'],
			$config['title'],
			$config['description'],
			$config['expire_at'],
			isset($config['type']) ? $config['type'] : ''
		);
	}


	public function getNotifications($receiver, $seen = false)
	{
		return $this->repo->getNotifications($receiver, $seen);
	}


	public function getAllNotifications($fields = [], $filters = [], $pagination = [], $sort = [], $withCounts = false)
	{
		$query = $this->repo->getAllNotificationsQuery();

		if ($fields) $this->repo->applyFields($query, $fields);

		if ($filters) $this->repo->applyFilters($query, $filters);

		if ($withCounts) $return['count']['total'] = $query->count();

		if ($pagination) {
			if (is_string($pagination)) $pagination = explode(",", $pagination);

			$this->repo->applyPagination($query, (int)$pagination[0], (int)$pagination[1]);
		}

		if ($withCounts) $return['count']['current'] = $query->count();

		if ($sort) $this->repo->applyOrder($query, $sort);

		if ($withCounts) {
			$return['results'] = $query->get();
			return $return;
		} else {
			return $query->get();
		}

	}


	public function getInboxNotifications($receiver, $fields = [], $filters = [], $pagination = [], $sort = [], $withCounts = false)
	{

		$query = $this->repo->getAllNotificationsQuery($receiver->id, get_class($receiver));
		if (is_array($receiver) && isset($receiver['id'], $receiver['type'])) {
			$receiver = $receiver['type']::find($receiver['id']);
		} elseif (!(is_object($receiver) && is_subclass_of($receiver, Model::class))) {
			return [];
		}

		if ($filters) {
			if (isset($filters['receiver_id'])) unset($filters['receiver_id']);
			if (isset($filters['receiver_type'])) unset($filters['receiver_type']);
		}

		$dispatchers = $receiver->fetchDispatchers();

		$query->where(function ($query) use ($dispatchers, $filters, $receiver) {

			if (isset($filters['seen'])) {
				$seen = $filters['seen'];
				unset($filters['seen']);
			} else {
				$seen = ['seen', '=', 0];
			}


			$query->where($seen[0], $seen[1], $seen[2]);
			$query->where('expire_at', '>=', Carbon::now()->toDateTimeString());
			//	$this->repo->applyReceivers($query, [$receiver]);
			$this->repo->applyFilters($query, $filters);


			if (!$seen[2]) {

				$query->orWhere(function ($query) use ($dispatchers, $filters) {

					$query->whereNull('notification_id')
						->where('expire_at', '>=', Carbon::now()->toDateTimeString())
						->where(function ($query) use ($dispatchers) {
							foreach ($dispatchers as $k => $dispatcher) {
								$query->orWhere(function ($query) use ($dispatcher) {
									$query->where('dispatcher_id', $dispatcher->id)
										->where('dispatcher_type', get_class($dispatcher));
								});
							}
						});

					$this->repo->applyFilters($query, $filters);
				});
			}

		});


		if ($fields)
			$this->repo->applyFields($query, $fields);

		if ($withCounts)
			$return['count']['total'] = $query->count();

		if ($pagination) {
			if (is_string($pagination))
				$pagination = explode(",", $pagination);

			$this->repo->applyPagination($query, $pagination[0], $pagination[1]);
		}

		if ($withCounts)
			$return['count']['current'] = $query->count();

		if ($sort)
			$this->repo->applyOrder($query, $sort);


		if ($withCounts) {
			$return['results'] = $query->get();
			return $return;
		} else {
			return $query->get();
		}

	}


	public function getOutboxNotifications($sender, $fields = [], $filters = [], $pagination = [], $sort = [], $withCounts = false)
	{
		$query = $this->repo->getAllOutboxNotificationsQuery($sender);

		if ($fields) $this->repo->applyFields($query, $fields);

		if ($filters) {
			if (isset($filters['sender_id'])) unset($filters['sender_id']);
			if (isset($filters['sender_type'])) unset($filters['sender_type']);
			$this->repo->applyFilters($query, $filters);
		}

		if ($withCounts) $return['count']['total'] = $query->count();

		if ($pagination) {
			if (is_string($pagination)) $pagination = explode(",", $pagination);

			$this->repo->applyPagination($query, $pagination[0], $pagination[1]);
		}

		if ($withCounts) $return['count']['current'] = $query->count();

		if ($sort) $this->repo->applyOrder($query, $sort);

		if ($withCounts) {
			$return['results'] = $query->get();
			return $return;
		} else {
			return $query->get();
		}

	}

	public function getById($notificationId, $loadRelations = false)
	{
		return ($loadRelations) ? $this->repo->getById($notificationId)->load('results') : $this->repo->getById($notificationId);
	}


	public function updateById($notificationId, $data, $return = false)
	{
		$entity = $this->repo->getById($notificationId);

		foreach ($data as $field => $value) {

			if (isset($entity->$field)) $entity->$field = $value;

		}

		$this->repo->save($entity);

		if ($return) return $entity;
	}


	public function createByNotifierEntity($causingEntity, $notifierConfig)
	{
		if (isset($notifierConfig['title'], $notifierConfig['description'], $notifierConfig['type'])) {
			return $this->createNotification($causingEntity, $causingEntity->notifierDispatchers(), $notifierConfig['title'], $notifierConfig['description'], $notifierConfig['expire_at'], $notifierConfig['type']);
		}
		return [];
	}


	public function getResultById($resultId, $loadRelation = false)
	{
		return ($loadRelation) ? $this->resultRepo->getById($resultId)->load('notification') : $this->resultRepo->getById($resultId);
	}


	public function destroyResultByIds(array $resultIds)
	{
		$this->resultRepo->delete($resultIds);
	}


	public function extractParams($data, $type)
	{
		if (is_subclass_of($data, Model::class)) {
			return [$type . '_id' => $data->id, $type . '_type' => get_class($data)];
		} elseif (is_array($data) && isset($data['type'])) {
			return [$type . '_id' => $data['id'], $type . '_type' => $data['type']];
		}
		return [];
	}

	public function createNotification($sender, $dispatchers, $title, $description, $expire_at, $type = '')
	{
		$sender = $this->extractParams($sender, 'sender');

		if ($sender) {

			if (!is_array($dispatchers) && !($dispatchers instanceof Collection)) $dispatchers = [$dispatchers];

			foreach ($dispatchers as $dispatcher) {
				$dispatcher = $this->extractParams($dispatcher, 'dispatcher');

				if ($dispatcher) {
					$notification = new Notification();
					$notification->fill(array_merge($sender, $dispatcher, ['title' => $title, 'description' => $description, 'expire_at' => $expire_at, 'type' => $type]));
					if ($notification->save()) {
						$result[] = $notification;
					}
				} else {
					throw new NotificationException('Bad dispatcher settings');
				}

			}
		}

		return isset($result) ? $result : [];
	}

	/**
	 * Alias getOutboxNotifications
	 * @param $sender
	 * @param array $fields
	 * @param array $filters
	 * @param array $pagination
	 * @param array $sort
	 * @param bool $withCounts
	 * @return mixed
	 */
	public function getSenderNotifications($sender, $fields = [], $filters = [], $pagination = [], $sort = [], $withCounts = false)
	{
		return $this->getOutboxNotifications($sender, $fields, $filters, $pagination, $sort, $withCounts);
	}


	public function readNotification($notificationId, $profileId, $read)
	{
		$reader = ProfileService::getProfileById($profileId);
		$readerSubscribed = false;
		$notification = $this->getById($notificationId);

		if ($notification && $reader) {

			if ($notification->dispatcher_id != $reader->id || $notification->dispatcher_type != get_class($reader)) {

				$subscriptions = $this->subscriptionRepo->getSubscriptions(
					$reader->id,
					get_class($reader),
					$notification->dispatcher_id,
					$notification->dispatcher_type
				);

				$readerSubscribed = count($subscriptions) > 0;

			} else {
				$readerSubscribed = true;
			}
		}

		if ($readerSubscribed) {
			return $this->readNotifications(
				[$notification],
				$reader,
				$read
			);
		}

		return false;
	}

	public function readNotifications(array $notifications, $reader, $read = true)
	{
		foreach ($notifications as $notification) {

			if (is_object($notification) && isset($notification->id)) {
				$notificationID = $notification->id;
			} elseif (is_array($notification) && isset($notification['id'])) {
				$notificationID = $notification['id'];
			} elseif (is_numeric($notification)) {
				$notificationID = $notification;
			} else {
				continue;
			}

			if (!empty($notificationID)) {
				$result = NotificationResult::firstOrNew([
					'notification_id' => $notificationID,
					'receiver_id' => $reader->id,
					'receiver_type' => get_class($reader)
				]);


				$result->seen = $read;

				$return[] = [
					'id' => $notificationID,
					'result' => $result->saveOrReturn()
				];
			}

		}

		return isset($return) ? $return : [];

	}

	/**
	 * @param Model $subscriber
	 * @param array|Collection $dispatchers
	 * @param int $status
	 * @return array
	 */
	public function subscribe(Model $subscriber, $dispatchers, $status = NotificationSubscription::STATUS_ACTIVE)
	{
		foreach ($dispatchers as $dispatcher) {
			$dispatcher = $this->extractParams($dispatcher, 'dispatcher');

			$subscriptions[] = NotificationSubscription::create([
				'subscriber_id' => $subscriber->id,
				'subscriber_type' => get_class($subscriber),
				'dispatcher_id' => $dispatcher['dispatcher_id'],
				'dispatcher_type' => $dispatcher['dispatcher_type'],
				'status' => $status
			]);
		}

		return isset($subscriptions) ? $subscriptions : [];
	}

	public function unSubscribe(Model $subscriber, $dispatchers)
	{
		foreach ($dispatchers as $dispatcher) {
			$unSubscriptions[] = $this->subscriptionRepo->setSubscriptionStatus(
				$subscriber->id,
				get_class($subscriber),
				$dispatcher->id,
				get_class($dispatcher),
				NotificationSubscription::STATUS_DISABLED
			);
		}

		return isset($unSubscriptions) ? $unSubscriptions : [];
	}

	public function removeSubscriptions(Model $subscriber)
	{
		return $this->subscriptionRepo->removeSubscriptions($subscriber->id, get_class($subscriber)) > 0;
	}

	public function getSubscriberDispatchers($subscriberId, $subscriberType, $status = NotificationSubscription::STATUS_ACTIVE)
	{
		return $this->subscriptionRepo->getDispatchers($subscriberId, $subscriberType, $status);
	}


	public function updateByNotifierEntity($causingEntity, $notifierConfig)
	{
		$this->repo->updateByNotifierEntity($causingEntity, $notifierConfig);
	}

	public function removeBySender($senderEntity)
	{
		$this->repo->removeBySender($senderEntity);
	}

	public function removeNotificationResults($notification)
	{
		return $this->repo->removeNotificationResult($notification);
	}

	public function removeByReceiver($receiver)
	{
		$this->repo->removeReceiverNotification($receiver);
	}

	public function broadcastNotificationByRole($senderProfileId, $title, $description, $roleId, $expireAt)
	{
		$role = Role::find($roleId);
		$profile = ProfileService::getProfileById($senderProfileId);

		return $this->createNotification($profile, $role, $title, $description, $expireAt);
	}

}


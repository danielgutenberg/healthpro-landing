<?php namespace WL\Modules\Notification\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use WL\Modules\Notification\Models\Notification;
use WL\Modules\Notification\Models\NotificationResult;

interface NotificationServiceInterface {


	/** Listener for 'notification.add' event
	 * @param array $config configuration array, see example NotifierTrait
	 * @return bool
	 */
	public function onNotificationAdd($config);

	/** Retreive seen or unseen Notifications from DB
	 * @param mixed $receiver accept Model or [id , type ]
	 * @param bool $seen
	 * @return mixed array of Notifications
	 */
	public function getNotifications($receiver, $seen = false);


	/**
	 *
	 * Returns Total count matching provided filters and notifications chunk for provided range
	 *
	 * @param array $fields ['field1','field2',...] If empty returns all fields
	 * @param array $filters [['field_name','=','field_value'],['field_name','!BETWEEN','value1,value2'],['field_name','IN','value1,value2']]
	 * @param array|string $pagination ['limit','offset'] OR "limit-offset"
	 * @param array $sort [['field1','direction'],['field2','direction']]
	 * @param bool  $withCounts Add counts to resulting array
	 * @return mixed [notifications] | ['counts'=>['total','current'],'results'=>notifications_array] total - without $pagination limits.
	 */
	public function getAllNotifications($fields = [],$filters = [],$pagination = [],$sort = [],$withCounts = false);

	/**
	 *
	 * Returns Incoming Notifications Total count matching provided filters for Receiver and notifications chunk for provided range
	 *
	 * @param array|object|Model $receiver
	 * @param array $fields ['field1','field2',...] If empty returns all fields
	 * @param array $filters [['field_name','=','field_value'],['field_name','!BETWEEN','value1,value2'],['field_name','IN','value1,value2']]
	 * @param array|string $pagination ['limit','offset'] OR "limit-offset"
	 * @param array $sort [['field1','direction'],['field2','direction']]
	 * @param bool  $withCounts Add counts to resulting array
	 * @return mixed [notifications] | ['counts'=>['total','current'],'results'=>notifications_array] total - without $pagination limits.
	 */
	public function getInboxNotifications($receiver,$fields = [],$filters = [],$pagination = [],$sort = [],$withCounts = false);

	/**
	 *
	 * Returns Incoming Notifications Total count matching provided filters for Sender and notifications chunk for provided range
	 *
	 * @param array|object|Model $sender
	 * @param array $fields ['field1','field2',...] If empty returns all fields
	 * @param array $filters [['field_name','=','field_value'],['field_name','!BETWEEN','value1,value2'],['field_name','IN','value1,value2']]
	 * @param array|string $pagination ['page','perPage'] OR "page-perPage"
	 * @param array $sort [['field1','direction'],['field2','direction']]
	 * @param bool  $withCounts Add counts to resulting array
	 * @return mixed [notifications] | ['counts'=>['total','current'],'results'=>notifications_array] total - without $pagination limits.
	 */
	public function getOutboxNotifications($sender,$fields = [],$filters = [],$pagination = [],$sort = [],$withCounts = false);

	/**
	 * @param $notificationId
	 * @return mixed
	 */
	public function getById($notificationId);

	/**
	 * @param $notificationId
	 * @param $data
	 * @param bool $return
	 * @return mixed
	 */
	public function updateById($notificationId,$data,$return = false);



	/**
	 * @param $resultId
	 * @param bool $loadRelation
	 * @return mixed
	 */
	public function getResultById($resultId,$loadRelation = false);


	/**
	 * @param $senderProfileId
	 * @param $title
	 * @param $description
	 * @param $roleId
	 * @param $expireAt
	 * @return mixed
     */
	public function broadcastNotificationByRole($senderProfileId, $title, $description, $roleId, $expireAt);



}

<?php namespace WL\Modules\OAuth\Services;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use WL\Events\Facades\EventHelper;
use WL\Modules\OAuth\Drivers\OAuthDriverInterface;
use WL\Modules\OAuth\Events\AuthenticateProfileEvent;
use WL\Modules\OAuth\Events\RemoveScopeEvent;
use WL\Modules\OAuth\Events\TokenFailureEvent;
use WL\Modules\OAuth\Exceptions\FailToProduceToken;
use WL\Modules\OAuth\Exceptions\FailToRefreshToken;
use WL\Modules\OAuth\Exceptions\InvalidOAuthToken;
use WL\Modules\OAuth\Exceptions\OAuthAccountAlreadyInUse;
use WL\Modules\OAuth\Models\OauthToken;
use WL\Modules\OAuth\Repositories\OAuthRepository;
use ReflectionClass;

class OAuthService implements OAuthServiceInterface
{
    protected $repo;

	public function __construct(OAuthRepository $repository)
	{
		$this->repo = $repository;
	}

	private function getConfig()
	{
		return config('oauth');
	}

	public function getAllProviders($scope = null)
	{
		$providers = [];
		foreach ($this->getConfig() as $provider => $conf) {
			if (is_null($scope) || in_array($scope, $conf['scopes'])) {
				$providers[] = $provider;
			}
		}
		return $providers;
	}

	public function getProfileOAuths($profileId, $scope = null)
	{
		$auths = $this->repo->getProfileAuths($profileId);
		if (!$auths->isEmpty() && $scope) {
			$auths = $auths->filter(function ($oauth) use ($scope){
				return in_array($scope, $oauth->scopes);
			});
		}
		return $auths;
	}

	public function getProfileOAuth($oauthId)
	{
		return $this->repo->getById($oauthId);
	}

	public function getAccess($tokenId)
	{
	    try{
            $oauth = $this->repo->getById($tokenId);
            if($oauth->status != OauthToken::STATUS_CONNECTED) {
                throw new FailToProduceToken();
            }
            $token = $this->getOAuthToken($oauth);
            $driver = $this->getDriver($oauth->provider);

            if($driver->isTokenExpired($token)){
                $token = $driver->refreshToken($token);
                $this->saveToken($oauth, $token);
            } else {
                $driver->verifyToken($token);
            }
            return $driver->getClient($token);

        } catch (FailToRefreshToken $e) {
            $this->forceDisconnect($tokenId);
            throw new FailToProduceToken();
        } catch (InvalidOAuthToken $e) {
            $this->forceDisconnect($tokenId);
            throw new FailToProduceToken();
        } catch (ModelNotFoundException $e) {
            throw new FailToProduceToken();
        }
	}

	public function getAuthorizationUrl($provider, $scopes = null)
	{
		if (is_array($scopes)) {
			$scopes = array_intersect($scopes, $this->getAllScopes());
		} else {
			$scopes = $this->getAllScopes();
		}

		if(!in_array(OauthToken::SCOPE_NAME, $scopes)){
			// For the process of fetching and saving the acount name
			$scopes[] = OauthToken::SCOPE_NAME;
		}
		return $this->getDriver($provider)->getAuthorizationUrl($scopes);
	}

	public function handleResponse($provider, Request $request, $profileId, $scope = null)
	{
		$driver = $this->getDriver($provider);
		$token = $driver->handleResponse($request);
		if (!$token) {
			return false;
		}

		if($accountName = $driver->getAccountName($token)) {
            $this->checkProviderAccount($profileId, $provider, $accountName);
            $oauth = $this->getOrNewToken($profileId, $provider, $accountName);

            if ($oauth = $this->saveToken($oauth, $driver->tokenToJson($token), $scope)) {
                EventHelper::fire(new AuthenticateProfileEvent($oauth->id, $scope));
            }
        }
	}

    private function checkProviderAccount($profileId, $provider, $accountName)
    {
        $oauth = $this->repo->getProviderActiveAccount($provider, $accountName);
        if($oauth) {
            if($oauth->profile_id != $profileId) {
                try {
                    // Check if oauth is active if not disconnect it and continue
                    $this->getAccess($oauth->id);
                    // If active throw error
                    throw new OAuthAccountAlreadyInUse();
                } catch (FailToProduceToken $e) {

                }
            }
        }
	}

	private function getOrNewToken($profileId, $provider, $accountName)
	{
		$oauth = $this->repo->getProfileAuth($profileId, $provider, $accountName);

		if (!$oauth) {
			$oauth = new OauthToken();
			$oauth->profile_id = $profileId;
			$oauth->provider = $provider;
			$oauth->name = $accountName;
			$oauth->scopes = [];
		}
		$oauth->status = OauthToken::STATUS_CONNECTED;

		return $oauth;
	}

	private function saveToken($oauth, $token, $scope = null)
	{
		$oauth->scopes = $this->appendScope($oauth->scopes, $scope);

		$oauth->token_json = $token;

		return $this->repo->saveOAuth($oauth) ? $oauth->fresh() : null;
	}

	private function appendScope($scopes, $scope)
	{
		if(!is_array($scopes)){
			$scopes = [];
		}

		if (!is_null($scope) && in_array($scope, $this->getAllScopes()) && !in_array($scope, $scopes)) {
			$scopes[] = $scope;
		}

		return $scopes;
	}

	public function getScopes($tokenId, $scope = null)
	{
	    try {
            $oauth = $this->repo->getById($tokenId);
            $scopes = $oauth->scopes;
        } catch (ModelNotFoundException $e) {
            $scopes = [];
        }

		return $this->appendScope($scopes, $scope);
	}

	public function removeScope($tokenId, $scope)
	{
		$oauth = $this->repo->getById($tokenId);
        return $this->removeOauthScope($oauth, $scope);
	}

	public function getToken($tokenId)
	{
		return $this->getOAuthToken($this->repo->getById($tokenId));
	}

    private function getOAuthToken(OauthToken $oauth)
    {
        $token = $this->getDriver($oauth->provider)->jsonToToken($oauth->token_json);
        if(!$token) {
            throw new FailToProduceToken();
        }
        return $token;
	}

	public function disconnect($tokenId)
	{
		$oauth = $this->repo->getById($tokenId);

        foreach ($oauth->scopes as $scope){
            EventHelper::fire(new RemoveScopeEvent($oauth->id, $scope));
        }

        $this->repo->deleteOAuth($oauth);
	}

    private function forceDisconnect($tokenId)
    {
        if($oauth = $this->repo->getById($tokenId)) {
            if($oauth->isConnected()) {
                $scopes = $oauth->scopes;
                if($this->repo->deleteOAuth($oauth)) {
                    EventHelper::fire(new TokenFailureEvent($oauth->id));
                }
                foreach ($scopes as $scope){
                    EventHelper::fire(new RemoveScopeEvent($oauth->id, $scope));
                }
            }
        }
	}

	private function removeOauthScope(OauthToken $oauth, $scope)
	{
		EventHelper::fire(new RemoveScopeEvent($oauth->id, $scope));
		if (($key = array_search($scope, $oauth->scopes)) !== false) {
			$scopes = $oauth->scopes;
			unset($scopes[$key]);
			if (empty($scopes)) {
				return $this->repo->deleteOAuth($oauth);
			} else {
				$oauth->scopes = $scopes;
				return $this->repo->saveOAuth($oauth);
			}
		}
	}

    /**
     * @param $provider
     * @return OAuthDriverInterface
     */
	private function getDriver($provider)
	{
		$config = $this->getConfig()[$provider];
		return new $config['class']($config);
	}

	private function getAllScopes()
	{
		return $this->extractConstants('SCOPE_');
	}

	private function extractConstants($prefix)
	{
		$oClass = new ReflectionClass(OauthToken::class);
		$scopes = [];
		foreach ($oClass->getConstants() as $name => $value) {
			if (substr($name, 0, 6) === $prefix) {
				$scopes[] = $value;
			}
		}
		return $scopes;
	}
}

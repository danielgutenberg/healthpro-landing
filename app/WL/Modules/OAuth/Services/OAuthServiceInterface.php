<?php namespace WL\Modules\OAuth\Services;

use Illuminate\Support\Collection;
use WL\Modules\OAuth\Models\OauthToken;
use Illuminate\Http\Request;

interface OAuthServiceInterface
{
	/**
	 * @param string || null $scope
	 * @return array
	 */
	public function getAllProviders($scope = null);

	/**
	 * @param int $profileId
	 * @param string || null $scope
	 * @return Collection
	 */
	public function getProfileOAuths($profileId, $scope = null);

	/**
	 * @param int $oauthId
	 * @return OauthToken
	 */
	public function getProfileOAuth($oauthId);

	/**
	 * @param int $tokenId
	 * @return mixed
	 */
	public function getAccess($tokenId);

	/**
	 * @param string $provider
	 * @param string || null $scopes
	 * @return string
	 */
	public function getAuthorizationUrl($provider, $scopes = null);

	/**
	 * @param string $provider
	 * @param Request $request
	 * @param int $profileId
	 * @param string || null $scope
	 * @return mixed
	 */
	public function handleResponse($provider, Request $request, $profileId, $scope=null);

	/**
	 * @param int $tokenId
	 * @param string $scope
	 * @return bool
	 */
	public function removeScope($tokenId, $scope);

	/**
	 * @param int $tokenId
	 * @return OauthToken
	 */
	public function getToken($tokenId);

	/**
	 * @param int $tokenId
	 * @return bool
	 */
	public function disconnect($tokenId);

	/**
	 * @param int $tokenId
	 * @param string || null $scope
	 * @return array
	 */
	public function getScopes($tokenId, $scope=null);
}

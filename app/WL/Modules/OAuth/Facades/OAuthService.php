<?php namespace WL\Modules\OAuth\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\OAuth\Services\OAuthServiceInterface;

class OAuthService extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */

	protected static function getFacadeAccessor()
	{
		return OAuthServiceInterface::class;
	}
}

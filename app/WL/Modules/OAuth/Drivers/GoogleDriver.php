<?php namespace WL\Modules\OAuth\Drivers;

use Illuminate\Http\Request;
use WL\Modules\OAuth\Exceptions\FailToRefreshToken;
use WL\Modules\OAuth\Exceptions\InvalidOAuthToken;
use WL\Modules\OAuth\Exceptions\OAuthAccessException;
use WL\Modules\OAuth\Models\OauthToken;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Oauth2;
use Google_Auth_Exception;
use Google_Service_Exception;

class GoogleDriver extends DriverAbstract implements OAuthDriverInterface
{
	protected $scopes = [
		OauthToken::SCOPE_CALENDAR => Google_Service_Calendar::CALENDAR,
		OauthToken::SCOPE_NAME => Google_Service_Oauth2::USERINFO_EMAIL,
	];

	public function __construct($config)
	{
		$this->provider = OauthToken::PROVIDER_GOOGLE;
		$this->config = $config;
	}

	public function getAuthorizationUrl($scopes)
	{
		$client = $this->getClient();
		$client->setScopes($this->translateScopes($scopes));
		return $client->createAuthUrl();
	}

	public function getClient($token=null)
	{
		$client = new Google_Client();
		$client->setAccessType('offline');
		$client->setApprovalPrompt('force');
		$client->setClientId($this->config['client_id']);
		$client->setClientSecret($this->config['client_secret']);
		$client->setRedirectUri($this->getRedirectUrl());
		if($token){
			$client->setAccessToken($token);
		}
		return $client;
	}

	public function getAccountName($token=null)
	{
        try {
            $oauth_client = new Google_Service_Oauth2($this->getClient($token));
            return $oauth_client->userinfo->get()->email;
        } catch (Google_Auth_Exception $e) {
            throw new OAuthAccessException();
        }
	}

	public function isTokenExpired($token)
	{
		$client = $this->getClient();
		$client->setAccessToken($token);
		return $client->isAccessTokenExpired();
	}

	public function refreshToken($token)
	{
		try {
			$client = $this->getClient();
			$client->setAccessToken($token);
			$client->refreshToken($client->getRefreshToken());
			return $client->getAccessToken();
		} catch (Google_Auth_Exception $e) {
			throw new FailToRefreshToken();
		}

	}

    public function verifyToken($token)
    {
        try {
            $oauth_client = new Google_Service_Oauth2($this->getClient($token));
            $oauth_client->userinfo->get();
            return true;
        } catch (Google_Service_Exception $e) {
            throw new InvalidOAuthToken();
        }
    }


    public function handleResponse(Request $request)
	{
		$code = $request->get('code');
		return $code ? $this->getClient()->authenticate($code) : false;
	}
}

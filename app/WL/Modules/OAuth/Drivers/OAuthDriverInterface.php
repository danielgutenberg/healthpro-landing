<?php
/**
 * Created by PhpStorm.
 * User: eli
 * Date: 4/14/16
 * Time: 2:17 PM
 */
namespace WL\Modules\OAuth\Drivers;

use Illuminate\Http\Request;

interface OAuthDriverInterface
{
	/**
	 * @param array $scopes
	 * @return string
	 */
	public function getAuthorizationUrl($scopes);

	/**
	 * @param mixed $token
	 * @return mixed
	 */
	public function getClient($token = null);

	/**
	 * @param null $token
	 * @return string
	 */
	public function getAccountName($token=null);

	/**
	 * @param mixed $token
	 * @return mixed
	 */
	public function refreshToken($token);

	/**
	 * @param mixed $token
	 * @return bool
	 */
	public function verifyToken($token);

	/**
	 * @param mixed $token
	 * @return bool
	 */
	public function isTokenExpired($token);

	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function handleResponse(Request $request);
}

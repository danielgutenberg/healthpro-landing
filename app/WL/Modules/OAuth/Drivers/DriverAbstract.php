<?php namespace WL\Modules\OAuth\Drivers;


abstract class DriverAbstract
{
	protected $provider;
	protected $scopes=[];

	protected function getRedirectUrl()
	{
		return route('oauth2-authenticate', ['provider' => $this->provider]);
	}

	protected function translateScopes($scopes)
	{
		$response = [];
		foreach($scopes as $scope){
			$s = array_get($this->scopes, $scope);
			if($s){
				$response = array_merge($response, is_array($s) ? $s : [$s]);
			}
		}
		return array_unique($response);
	}

	public function tokenToJson($token)
	{
		if (!is_string($token))
			$token = json_encode($token);
		return $token;
	}

	public function jsonToToken($json)
	{
		return $json;
	}
}

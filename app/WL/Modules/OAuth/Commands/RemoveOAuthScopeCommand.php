<?php namespace WL\Modules\OAuth\Commands;

use Illuminate\Console\Command;
use WL\Modules\OAuth\Facades\OAuthService;

class RemoveOAuthScopeCommand extends Command
{
	protected $name = 'oauth:remove';

	public function __construct($tokenId, $scope)
	{
		$this->tokenId = $tokenId;
		$this->scope = $scope;
	}

	public function handle()
	{
		OAuthService::removeScope($this->tokenId, $this->scope);
	}
}

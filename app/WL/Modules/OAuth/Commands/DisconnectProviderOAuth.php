<?php namespace WL\Modules\OAuth\Commands;


use Illuminate\Console\Command;
use WL\Modules\OAuth\Facades\OAuthService;

class DisconnectProviderOAuth extends Command
{
	protected $name = 'oauth:disconnect';

	public function __construct($tokenId)
	{
		$this->tokenId = $tokenId;
	}

	public function handle()
	{
		OAuthService::disconnect($this->tokenId);
	}
}

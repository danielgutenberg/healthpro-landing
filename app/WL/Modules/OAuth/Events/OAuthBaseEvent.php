<?php namespace WL\Modules\OAuth\Events;

use WL\Events\BasicEvents\BasicEventInterface;

class OAuthBaseEvent implements BasicEventInterface
{
	protected $oauthId;
	protected $scope;

	public function __construct($oauthId, $scope=null)
	{
		$this->oauthId = $oauthId;
		$this->scope = $scope;
	}

	public function getEventName()
	{
		return 'oauth-events';
	}

	public function getOauthId()
	{
		return $this->oauthId;
	}

	public function getScope()
	{
		return $this->scope;
	}

}

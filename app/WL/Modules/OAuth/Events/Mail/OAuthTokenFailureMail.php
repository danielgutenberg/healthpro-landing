<?php namespace WL\Modules\OAuth\Events\Mail;


use WL\Events\BasicEvents\EmailHealthProToClientEmailQueueEvent;

class OAuthTokenFailureMail extends EmailHealthProToClientEmailQueueEvent
{
	public function __construct($bladeData, $receiverName, $receiverEmail)
	{
		parent::__construct(
			null,
			null,
			$bladeData,
			null,
			null,
			$receiverName,
			$receiverEmail
		);
	}
}

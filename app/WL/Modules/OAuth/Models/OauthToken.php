<?php namespace WL\Modules\OAuth\Models;

use WL\Models\ModelBase;

class OauthToken extends ModelBase
{
	const PROVIDER_GOOGLE = 'google';

	const SCOPE_CALENDAR = 'calendar';
	const SCOPE_SOCIAL = 'social';
	const SCOPE_NAME = 'email';

	const STATUS_CONNECTED = 'connected';
	const STATUS_DISCONNECTED = 'disconnected';

	protected $table = 'oauth_tokens';

	protected $casts = [
		'scopes' => 'array',
	];

    public function isConnected()
    {
        return $this->status === self::STATUS_CONNECTED;
	}

}

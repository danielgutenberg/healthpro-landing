<?php namespace WL\Modules\OAuth\Exceptions;

use App\Exceptions\GenericException;

class OAuthAccessException extends GenericException {}
class FailToProduceToken extends OAuthAccessException {}
class FailToRefreshToken extends OAuthAccessException {}
class InvalidOAuthToken extends OAuthAccessException {}
class OAuthAccountAlreadyInUse extends OAuthAccessException {}
<?php namespace WL\Modules\OAuth\Repositories;

use Illuminate\Support\Collection;
use WL\Modules\OAuth\Models\OauthToken;

interface OAuthRepositoryInterface
{
    /**
     * @param string $provider
     * @param string $accountName
     * @return OauthToken
     */
    public function getProviderActiveAccount($provider, $accountName);

	/**
	 * @param int $profileId
	 * @param string $provider
	 * @param string $accountName
	 * @return OauthToken
	 */
	public function getProfileAuth($profileId, $provider, $accountName);

	/**
	 * @param int $profileId
	 * @return Collection
	 */
	public function getProfileAuths($profileId);

	/**
	 * @param OauthToken $oauth
	 * @return bool
	 */
	public function saveOAuth(OauthToken $oauth);

	/**
	 * @param OauthToken $oauth
	 * @return bool
	 */
	public function deleteOAuth(OauthToken $oauth);
}

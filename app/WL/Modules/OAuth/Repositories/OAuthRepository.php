<?php namespace WL\Modules\OAuth\Repositories;

use WL\Modules\OAuth\Models\OauthToken;
use WL\Repositories\DbRepository;

class OAuthRepository extends DbRepository implements OAuthRepositoryInterface
{
    public function __construct()
    {
        $this->model = new OauthToken();
    }

    public function getProviderActiveAccount($provider, $accountName)
    {
        return OauthToken::where('provider', $provider)->where('name', $accountName)->where('status', OauthToken::STATUS_CONNECTED)->first();
    }

	public function getProfileAuth($profileId, $provider, $accountName)
	{
		return OauthToken::where('profile_id', $profileId)->where('provider', $provider)->where('name', $accountName)->first();
	}

	public function getProfileAuths($profileId)
	{
		return OauthToken::where('profile_id', $profileId)->where('status', OauthToken::STATUS_CONNECTED)->get();
	}

	public function saveOAuth(OauthToken $oauth)
	{
		return $oauth->save();
	}

	public function deleteOAuth(OauthToken $oauth)
	{
		return $oauth->update(['status' => OauthToken::STATUS_DISCONNECTED, 'scopes' => []]);
	}

}

<?php namespace WL\Modules\OAuth\Providers;

use Illuminate\Support\ServiceProvider;
use WL\Modules\OAuth\Repositories\OAuthRepository;
use WL\Modules\OAuth\Services\OAuthService;
use WL\Modules\OAuth\Services\OAuthServiceInterface;

class OAuthServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton(OAuthServiceInterface::class, function () {
			return new OAuthService(new OAuthRepository());
		});
	}
}

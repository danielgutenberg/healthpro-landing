<?php namespace WL\Modules\HasOffers\Events;


class IsSearchableEvent
{
	private $profile;

	/**
	 * Event constructor.
	 * @param $profile
	 * @param string $registered
	 */
	public function __construct($profile, $registered = '')
	{
		$this->profile = $profile;
		$this->registered = $registered;
	}

	/**
	 * @return ModelProfile
	 */
	public function getProfile()
	{
		return $this->profile;
	}

	public function getGoal()
	{
		return 'searchable';
	}

	public function getRegistered()
	{
		return $this->registered;
	}
}

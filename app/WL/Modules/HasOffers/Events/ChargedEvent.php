<?php namespace WL\Modules\HasOffers\Events;


use WL\Modules\Payment\Facades\PaymentConfig;
use WL\Modules\Payment\Models\PaymentMethod;
use WL\Modules\Payment\Models\PaymentAccount;

class ChargedEvent
{
	private $profile;

	/**
	 * Event constructor.
	 * @param $profileId
	 */
	public function __construct($operation)
	{
		$this->operation = $operation;
	}

	/**
	 * @return ModelProfile
	 */
	public function getProfile()
	{
	    $paymentMethodId = $this->operation->receiver_payment_method_id ?: $this->operation->sender_payment_method_id;
		$method = PaymentMethod::where('id', $paymentMethodId)->first();
		return $method->account->profile;
	}

	public function getFee()
	{
		$totalFee = $this->operation->fee;
		$stripeFee = $this->operation->gateway_fee;
		return $totalFee - $stripeFee;
	}

	public function getGoal()
	{
		return 'payment';
	}

	public function getAppointmentCost()
	{
		return $this->operation->amount;
	}
}

<?php namespace WL\Modules\HasOffers\Events;


class RegisteredEvent
{
	private $profile;

	/**
	 * Event constructor.
	 * @param $profileId
	 */
	public function __construct($profile, $registered = '')
	{
		$this->profile = $profile;
		$this->registered = $registered;
	}

	/**
	 * @return ModelProfile
	 */
	public function getProfile()
	{
		return $this->profile;
	}

	public function getGoal()
	{
		return 'registered';
	}

	public function getRegistered()
	{
		return $this->registered;
	}
}

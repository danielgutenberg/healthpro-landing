<?php namespace WL\Modules\Slug\Facade;

use Illuminate\Support\Facades\Facade;

class SlugService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'SlugService';
	}
}

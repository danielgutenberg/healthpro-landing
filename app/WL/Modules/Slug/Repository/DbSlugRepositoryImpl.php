<?php namespace WL\Modules\Slug\Repository;

use Illuminate\Support\Facades\DB;

class DbSlugRepositoryImpl implements SlugRepositoryInterface
{
	public function isSlugExists($slug, $dbTableName, $slugColumn = 'slug', $excludeId = null)
	{
		$query = DB::table($dbTableName)
			->where($slugColumn, $slug);

		if ($excludeId != null) {
			$query->where('id', '<>', $excludeId);
		}
		return $query->exists();
	}

	public function getSameSlugs($slug, $dbTableName, $slugColumn, $excludeId)
	{
		$query = DB::table($dbTableName)
			->where($slugColumn, 'LIKE', "$slug%");
		if ($excludeId != null) {
			$query->where('id', '<>', $excludeId);
		}

		return $query->pluck($slugColumn);
	}
}

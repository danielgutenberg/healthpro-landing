<?php namespace WL\Modules\Slug\Repository;

/**
 * Interface SlugRepositoryInterface
 * @package WL\Modules\Slug\Repository
 */
interface SlugRepositoryInterface
{
	/**
	 * @param string $slug
	 * @param string $dbTableName
	 * @param string $slugColumn
	 * @param string $excludeId
	 * @return mixed
	 */
	
	public function isSlugExists($slug, $dbTableName, $slugColumn = 'slug', $excludeId = null);

	/**
	 * @param string $slug
	 * @param string $dbTableName
	 * @param string $slugColumn
	 * @param integer $excludeId
	 * @return mixed
	 */
	public function getSameSlugs($slug, $dbTableName, $slugColumn, $excludeId);
}

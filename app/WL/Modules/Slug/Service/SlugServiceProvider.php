<?php namespace WL\Modules\Slug\Service;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use WL\Modules\Slug\Repository\DbSlugRepositoryImpl;
use WL\Modules\Slug\Repository\SlugRepositoryInterface;


class SlugServiceProvider extends ServiceProvider
{
	public function register()
	{

		$this->app->instance('SlugService',new SlugServiceImpl(new DbSlugRepositoryImpl()));
		$this->app->booting(function () {
			AliasLoader::getInstance()->alias('SlugService', 'WL\Modules\Slug\Facade\SlugService');
		});

		$this->app->bind(SlugServiceInterface::class, SlugServiceImpl::class);
		$this->app->bind(SlugRepositoryInterface::class, DbSlugRepositoryImpl::class);
	}
}


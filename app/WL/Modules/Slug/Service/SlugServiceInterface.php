<?php namespace WL\Modules\Slug\Service;

/**
 * Interface SlugServiceInterface
 * @package WL\Modules\Slug\Service
 */
interface SlugServiceInterface
{
	/**
	 * @param $slug
	 * @param $dbTableName
	 * @param string $slugColumn
	 * @param string $excludeId
	 * @return mixed
	 */
	public function slugExists($slug, $dbTableName, $slugColumn = 'slug', $excludeId = null);

	/**
	 * @param $source
	 * @param $dbTableName
	 * @param integer $excludeId
	 * @param string $slugColumn
	 * @return mixed
	 */
	public function generateUniqueSlug($source, $dbTableName, $excludeId = null, $slugColumn = 'slug');

	/**
	 * @param $source
	 * @return mixed
	 */
	public function generateSlug($source);


}

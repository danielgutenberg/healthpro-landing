<?php namespace WL\Modules\Slug\Service;

use Illuminate\Support\Str;
use WL\Modules\Slug\Repository\SlugRepositoryInterface;

class SlugServiceImpl implements SlugServiceInterface
{
	public function __construct(SlugRepositoryInterface $repo, $separator = '-')
	{
		$this->separator = $separator;
		$this->repo = $repo;
	}

	public function slugExists($slug, $dbTableName, $slugColumn = 'slug', $excludeId = null)
	{
		return $this->repo->isSlugExists($slug, $dbTableName, $slugColumn, $excludeId);
	}


	public function generateUniqueSlug($source, $dbTableName, $excludeId = null, $slugColumn = 'slug')
	{
		$slug = $this->generateSlug($source);

		$sameSlugs = $this->repo->getSameSlugs($slug, $dbTableName, $slugColumn, $excludeId);

		if (count($sameSlugs) === 0 || !in_array($slug, $sameSlugs)) {
			return $slug;
		}

		$len = strlen($slug . $this->separator);
		array_walk($sameSlugs, function (&$value, $key) use ($len) {
			$value = intval(substr($value, $len));
		});

		rsort($sameSlugs);

		return $slug . $this->separator . (reset($sameSlugs) + 1);
	}

	public function generateSlug($source)
	{
		return Str::slug($source, $this->separator);

	}
}

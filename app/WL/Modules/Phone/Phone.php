<?php namespace WL\Modules\Phone;

use Carbon\Carbon;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberUtil;
use WL\Models\ModelBase;
use WL\Yaml\Contracts\Parsable;

class Phone extends ModelBase
{
	protected $table = 'phones';

	const CONFIG_FILE_PATH = 'wl/phone/settings';

	const VERIFIED_BY_SMS  = 'sms';
	const VERIFIED_BY_CALL = 'call';
    const MAX_VERIFICATIONS = 50;

    public function rel()
    {
        return $this->morphTo();
    }

	/**
	 * Formatting number ..
	 *
	 * @param int $format
	 * @return mixed
	 */
	public function format($format = PhoneNumberFormat::INTERNATIONAL) {
		return $this->phoneValidator->format(
			$this->getPhone(), $format
		);
	}

	/**
	 * Set phone ..
	 *
	 * @param $code
	 * @param $number
	 * @return $this
	 */
	public function setPhone($code, $number) {
		$this->prefix = $code;
		$this->number = $number;

		return $this;
	}

	/**
	 * Get phone number ..
	 *
	 * @param $template
	 * @return string
	 */
	public function getPhone($template = null) {
		if(! $template)
			return sprintf('%s%s', $this->prefix, $this->number);

		$vars = ['prefix', 'number'];
		array_walk($vars, function($value) use(&$template) {
			$template = str_replace('%' . $value .'%', $this->{$value}, $template);
		});

		return $template;
	}


	/**
	 * Generate code ..
	 *
	 * @param int $length
	 * @return string
	 */
	public function generateCode($length = 5) {
		$chars = '123456789';

		$charsLength = strlen($chars);
		$result = '';
		for ($i = 0; $i < $length; $i++) {
			$result .= $chars[rand(0, $charsLength - 1)];
		}

		return $result;
	}

	/**
	 * Set code ..
	 *
	 * @param $code
	 * @return $this
	 */
	public function setCode($code) {
		$this->activation_hash = $code;

		return $this;
	}

	/**
	 * Get generated code ..
	 *
	 * @return mixed
	 */
	public function getCode() {
		return $this->activation_hash;
	}

	public function getModeActivation() {
		return $this->verified_by;
	}

	/**
	 * @return bool
	 */
	public function isNotified() {
		return $this->is_notified == 1;
	}

	/**
	 * Is verified current activation ..
	 *
	 * @return bool
	 */
	public function isVerified() {
		return ($this->getModeActivation() && $this->verified_at);
	}

	/**
	 * Check is verified by ..
	 *
	 * @param string $mode
	 * @return bool
	 */
	public function isVerifiedBy($mode = self::VERIFIED_BY_SMS) {
		return $this->getModeActivation() == $mode;
	}

	/**
	 * Get expired time .
	 *
	 * @param int $default
	 * @return int
	 */
	protected function getExpiredTime($default = 6) {
        $parser = app(Parsable::class);
	    $verificationSettings = $parser->parse(
			self::CONFIG_FILE_PATH
		);

		if( isset($verificationSettings['expiration'][$this->getModeActivation()]) )
			return $verificationSettings['expiration'][$this->getModeActivation()];

		return $default;
	}

	/**
	 * Check if code expired ..
	 *
	 * @return bool
	 */
	public function isExpiredCode() {
		$expirationTime = $this->getExpiredTime();

		$created 		= Carbon::parse($this->created_at);
		$now  			= Carbon::now();

		if( $now->diffInHours($created) > $expirationTime)
			return true;

		return false;
	}
}

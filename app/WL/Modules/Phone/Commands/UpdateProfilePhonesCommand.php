<?php namespace WL\Modules\Phone\Commands;


use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;
use WL\Modules\Phone\Contracts\PhoneServiceContract;
use WL\Modules\Phone\Phone;
use WL\Modules\Phone\PhoneFactory;
use WL\Security\Commands\AuthorizableCommand;

class UpdateProfilePhonesCommand extends AuthorizableCommand {

	const IDENTIFIER = 'phone.syncPhones';

	protected $rules = [
		'phone' => 'sometimes|phone'
	];

	protected $messages = [
		'phone' => 'The :attribute is invalid'
	];

	/**
	 * @var
	 */
	private $profile;

	/**
	 * @var array
	 */
	private $phones;

	public function __construct($profile, array $phones = array()) {
		$this->profile = $profile;
		$this->phones = $phones;

		$errors = [];
		$validator = Validator::make([], $this->rules, $this->messages);

		foreach($this->phones as $key => $phone) {
			$validator->setData(['phone' => $phone]);

			if(! $validator->passes())
				$errors = $validator->errors()->merge($errors);
		}

		if( $errors )
			throw new ValidationException($errors);
	}

	/**
	 * Get profile phones
	 *
	 * @param PhoneServiceContract $phoneService
	 * @return mixed
	 */
	public function handle(PhoneServiceContract $phoneService) {
		$phones = [];

		if( empty($this->phones) )
			return $phoneService->removePhones(
				$this->profile->id, get_class($this->profile->typeInstance())
			);

		foreach ($this->phones as $phone) {
			if( ! $phone instanceof Phone ) {
				$phone = PhoneFactory::fromArray([
					'prefix'      => $phone['prefix'],
					'number'      => $phone['number'],
					'is_notified' => isset($phone['is_notified']) ? $phone['is_notified']  : 0,
					'rel_id'      => $this->profile->id,
					'rel_type'    => get_class($this->profile->typeInstance()),
				]);
			}

			$phones[] = $phone;
		}

		return $phoneService->syncPhones($phones, $this->profile->id, get_class($this->profile->typeInstance()));
	}
}

<?php namespace WL\Modules\Phone\Commands;


use WL\Modules\Phone\Contracts\PhoneServiceContract;
use WL\Security\Commands\AuthorizableCommand;

class GetProfilePhonesCommand extends AuthorizableCommand {

	const IDENTIFIER = 'phone.getPhones';

	/**
	 * @var
	 */
	private $profile;

	public function __construct($profile) {

		$this->profile = $profile;
	}

	/**
	 * Get profile phones
	 *
	 * @param PhoneServiceContract $phoneService
	 * @return mixed
	 */
	public function handle(PhoneServiceContract $phoneService) {
		return $phoneService->phones(
			$this->profile->id, get_class($this->profile->typeInstance())
		);

	}
}

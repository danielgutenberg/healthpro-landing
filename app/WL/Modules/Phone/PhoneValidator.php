<?php namespace WL\Modules\Phone;

use WL\Validation\ValidationInterface;

class PhoneValidator implements ValidationInterface
{
	/**
	 * Validate phone number ..
	 *
	 * @param $field
	 * @param $value
	 * @param array $params
	 * @return bool
	 */
	public function validate($field, $value, array $params = []) {
		$phone = PhoneFactory::fromArray([
			'prefix'      => isset($value['prefix']) ? $value['prefix'] : null,
			'number'      => isset($value['number']) ? $value['number'] : null,
		]);

		if(! $phone->isValid() )
			return false;

		return true;
	}

}

<?php namespace WL\Modules\Phone\Contracts;

use WL\Modules\Phone\Phone;

interface PhoneServiceContract {

	/**
	 * Add phones ..
	 *
	 * @param array $phones
	 * @return mixed
	 */
	public function addPhones(array $phones = array());

	/**
	 * Add phone ..
	 *
	 * @param Phone $phone
	 * @return mixed
	 */
	public function addPhone(array $data);

	/**
	 * Get phones by entity
	 *
	 * @param $elm_id
	 * @param $elm_type
	 * @return mixed
	 */
	public function phones($elm_id, $elm_type);

	/**
	 * Remove phones by id
	 *
	 * @param $phones
	 * @return mixed
	 */
	public function removePhonesById($phones);

	/**
	 * Remove phones by entity .
	 *
	 * @param $elm_id
	 * @param $elm_type
	 * @return mixed
	 */
	public function removePhones($elm_id, $elm_type);

}

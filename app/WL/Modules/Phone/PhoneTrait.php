<?php namespace WL\Modules\Phone;

use WL\Models\Exceptions;
use WL\Modules\Location\Models\Phone;
use WL\Modules\Phone\Exceptions\PhoneExistException;

trait PhoneTrait
{

	/**
	 * Phones relation
	 * @return Relation\MorphMany
	 */
	public function phones()
	{
		return $this->morphMany(Phone::class, 'rel');
	}

	/**
	 * @param Phone $phone
	 * @return mixed
	 */
	public function getPhoneById($phoneId)
	{
		return $this->phones()->whereId($phoneId)->first();
	}

	/**
	 * @param Phone $phone
	 * @return mixed
	 */
	public function addPhone(Phone $phone)
	{
		$phoneExist = $this->phones()
			->where('code',$phone->code)
			->where('locNumber',$phone->locNumber)
			->count();

		if ($phoneExist) {
			throw new PhoneExistException("Phone already exists");
		}

		return $this->phones()->save($phone);
	}

	/**
	 * @param Phone $phone
	 * @return bool
	 * @throws \Exception
	 */
	public function deletePhone(Phone $phone)
	{
		// check if this is our phone
		if ($this->phones()->whereId($phone->id)->count()==0) {
			return false;
		}

		$phone->delete();
		return true;
	}

	/**
	 * 	Delete all phones ..
	 */
	public function deletePhones() {
		$this->phones->each(function($phone) {
			$phone->delete();
		});
	}

	/**
	 * Sync Phones by fullNumber
	 * @param array $phones
	 * @return void
	 */
	public function syncPhones(array $phones)
	{
		$have = $need = array();

		foreach ($phones as $phone) {
			if (!$phone->isValid()) return false;
		}

		// prepare current phones
		foreach ($this->phones as $phone)
			$have[$phone->fullNumber] = $phone;

		// prepare needed phones
		foreach ($phones as $phone)
			$need[$phone->fullNumber] = $phone;

		// save new phones
		foreach ($need as $k => $phone) {
			if (array_key_exists($k, $have)) continue;

			try {
				$this->addPhone($phone);
			} catch (Exceptions\PhoneException $e) { }
		}

		// delete old phones
		foreach ($have as $k => $phone) {
			if (array_key_exists($k, $need)) continue;

			$this->deletePhone($phone);
		}
	}

	public function getPhoneObjects(array $phones = []) {
		$phonesTo = [];

		foreach($phones as $phone) {
			$phone = new Phone([
				'code' 		=> ltrim($phone['code'],'+'),
				'locNumber' => $phone['locNumber']
			]);


			if ($phone->isValid()) {
				$phonesTo[] = $phone;
			}
		}

		return $phonesTo;
	}

}

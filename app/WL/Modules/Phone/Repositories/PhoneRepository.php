<?php namespace WL\Modules\Phone\Repositories;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Modules\Phone\Contracts\PhoneRepositoryContract;
use WL\Modules\Phone\Phone;
use WL\Modules\Phone\PhoneFactory;
use WL\Repositories\DbRepository;

class PhoneRepository extends DbRepository implements PhoneRepositoryContract {

	const TABLE_NAME = 'phones';

	/**
	 * Add phone ..
	 *
	 * @param array $phones
	 * @return Phone $phone
	 */
	public function addPhone($data)
    {
		$phone = $this->getByPhone($data['rel_id'], $data['rel_type'], $data['prefix'], $data['number']);
		if(!$phone) {
            $phone = new Phone($data);
			$this->save($phone);
        }
		return $phone;
	}

	public function getByPhone($elm_id, $elm_type, $prefix, $number) {
		return Phone::where('rel_id', $elm_id)
			->where('rel_type', $elm_type)
			->where('prefix', $prefix)
			->where('number', $number)
            ->get()
			->first();
	}

	/**
	 * Get phones by entity ..
	 *
	 * @param $elm_id
	 * @param $elm_type
	 * @return static
	 */
	public function getPhones($elm_id, $elm_type) {
		return Phone::where('rel_id', $elm_id)
            ->where('rel_type', $elm_type)
            ->get();
	}

	/**
	 * Get phone by id .
	 *
	 * @param $id
	 * @return Phone
	 */
	public function getById($id)
	{
		return Phone::findOrFail($id);
	}

	/**
	 * Remove phones by id ..
	 *
	 * @param $phones
	 * @return int
	 */
	public function removePhones($phones) {
		if(! is_array($phones))
			$phones = (array)$phones;

		return DB::table(self::TABLE_NAME)
			->whereIn('id', (array)$phones)
			->delete();
	}

	/**
	 * Remove by entity ..
	 *
	 * @param $elm_id
	 * @param $elm_type
	 * @return int
	 */
	public function removeByEntity($elm_id, $elm_type) {
		return DB::table(self::TABLE_NAME)
			->where('rel_id', $elm_id)
			->where('rel_type', $elm_type)
			->delete();
	}

	/**
	 * Add activation ..
	 *
	 * @param Phone $phone
	 * @return int
	 */
	public function addActivation(Phone $phone) {
		return Phone::where('id', $phone->id)
			->update([
				'verified_by'        => $phone->getModeActivation(),
				'activation_hash'    => $phone->getCode()
			]);
	}

	public function getByCode($code) {
		if (!$code) {
			return null;
		}
		return $activationRaw = Phone::where('activation_hash', $code)
			->first();
	}

	/**
	 * Mark activation complete ..
	 *
	 * @param $code
	 * @return int
	 */
	public function setCompleteActivation($phone)
	{
		$phone->verified_at = Carbon::now();
		if ($phone->new_number) {
			$phone->number = $phone->new_number;
			$phone->new_number = null;
		}

		return $phone->save();
	}
}

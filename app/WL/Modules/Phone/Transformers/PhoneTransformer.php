<?php namespace WL\Modules\Phone\Transformers;

use Carbon\Carbon;
use WL\Transformers\Transformer;

class PhoneTransformer extends Transformer
{
    public function transform($item)
    {
        if (!$item) {
            return [];
        }
        $data = [
            'id' => $item->id,
            'number' => $item->prefix . $item->number,
            'verified' => (bool) $item->verified_at
        ];

        return $data;
    }
}

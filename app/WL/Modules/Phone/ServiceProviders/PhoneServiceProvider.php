<?php namespace WL\Modules\Phone\ServiceProviders;

use Illuminate\Support\ServiceProvider;
use WL\Modules\Phone\Contracts\PhoneRepositoryActivationsContract;
use WL\Modules\Phone\Contracts\PhoneRepositoryContract;
use WL\Modules\Phone\Contracts\PhoneServiceContract;
use WL\Modules\Phone\Repositories\PhoneActivationsRepository;
use WL\Modules\Phone\Repositories\PhoneRepository;
use WL\Modules\Phone\Services\PhoneService;

class PhoneServiceProvider extends ServiceProvider {

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->singleton(PhoneRepositoryContract::class, PhoneRepository::class);

		$this->app->singleton(PhoneServiceContract::class, function($app) {
			return new PhoneService(
				$app[PhoneRepositoryContract::class]
			);
		});
	}
}

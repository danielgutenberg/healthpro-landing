<?php namespace WL\Modules\Phone;

class PhoneFactory {

	/**
	 * Create phone instance from array ..
	 *
	 * @param array $data
	 * @return Phone
	 */
	public static function fromArray(array $data) {
		return new Phone(
			isset($data['id']) ? $data['id'] : null,
			isset($data['prefix']) ? $data['prefix'] : null,
			isset($data['number']) ? $data['number'] : null,
			isset($data['is_notified']) ? $data['is_notified'] : null,
			isset($data['activation_hash']) ? $data['activation_hash'] : null,
			isset($data['verified_by']) ? $data['verified_by'] : null,
			isset($data['verified_at']) ? $data['verified_at'] : null,
			isset($data['rel_id']) ? $data['rel_id'] : null,
			isset($data['rel_type']) ? $data['rel_type'] : null
		);
	}
}

<?php namespace WL\Modules\Phone\Services;

use Authy\AuthyApi;
use Closure;
use Event;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use WL\Modules\Phone\Contracts\PhoneRepositoryContract;
use WL\Modules\Phone\Contracts\PhoneServiceContract;
use WL\Modules\Phone\Phone;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Yaml\Facades\Yaml;

class PhoneService implements PhoneServiceContract {

	/**
	 * @var PhoneRepositoryContract
	 */
	private $phoneRepository;


	public function __construct(PhoneRepositoryContract $phoneRepository) {

		$this->phoneRepository = $phoneRepository;
	}

	/**
	 * Add phones ..
	 *
	 * @param array $phones
	 * @return mixed
	 */
	public function addPhones(array $phones = array()) {
		$cleanPhones = self::removeDuplicates($phones);

		return $this->phoneRepository->addPhones($cleanPhones);
	}

	/**
	 * Add phone ..
	 *
	 * @param Phone $phone
	 * @return mixed
	 */
	public function addPhone(array $data)
    {
		return $this->phoneRepository->addPhone($data);
	}

	/**
	 * Get phones by entity
	 *
	 * @param $elm_id
	 * @param $elm_type
	 * @return mixed
	 */
	public function phones($elm_id, $elm_type) {
		return $this->phoneRepository->getPhones($elm_id, $elm_type);
	}

	/**
	 * Remove phones by ids's
	 *
	 * @param $phoneIds
	 * @return mixed
	 */
	public function removePhonesById($phoneIds) {
		return $this->phoneRepository->removePhones($phoneIds);
	}

	/**
	 * Remove phones by entity .
	 *
	 * @param $elm_id
	 * @param $elm_type
	 * @return mixed
	 */
	public function removePhones($elm_id, $elm_type) {
		return $this->phoneRepository->removeByEntity($elm_id, $elm_type);
	}

	/**
	 * Sync Phones ..
	 *
	 * @param array $phones
	 * @param $elm_id
	 * @param $elm_type
	 * @return mixed
	 */
	public function syncPhones(array $phones = array(), $elm_id, $elm_type) {
		$entityPhones = $this->phones($elm_id, $elm_type);

		$phones 	  = collect($phones);

		$toAdd    = $this->getDiffPhones($phones, $entityPhones);
		$toRemove = $this->getDiffPhones($entityPhones, $phones);

		array_walk($toRemove, function(Phone $phone) {
			$this->removePhonesById(
				$phone->id
			);
		});

		return $this->addPhones($toAdd);
	}


	/**
	 * Check if exists code  ..
	 *
	 * @param $code
	 * @return bool
	 */
	public function isValidVerificationCode($code) {
		$phone = $this->phoneRepository
			->getByCode($code);

		if(! isset($phone->id) || ($phone->isVerified() && !$phone->new_number))
			return false;

		return true;
	}

	/**
	 * Check if code wasn't expired ..
	 *
	 * @param $code
	 * @return bool
	 */
	public function isExpiredVerificationCode($code) {
		$verification = $this->phoneRepository
			->getByCode($code);

		return $verification->isExpiredCode();
	}

	/**
	 * Complete verification ..
	 *
	 * @param $code
	 * @param bool $checkForExpiration
	 * @param callable $onComplete
	 * @return array|null
	 */
	public function completeVerification(Phone $phone, $code, $checkForExpiration = true)
	{
		$testingNumbers = [
			'6171111111' => ['972', '586699088'],
			'6172222223' => ['972', '526255076'],
			'6173333333' => ['972', '587627241'],
			'6174444444' => ['972', '525610702'],
			'6175555555' => ['64', '220951245'],
			'8439011978' => ['1', '8439011978']
		];

		$testing = array_key_exists($phone->number, $testingNumbers);

		if ($phone->verified_by == Phone::VERIFIED_BY_SMS || (!app()->environment('production') && !$testing)) {
			if (!$this->isValidVerificationCode($code)) {
				throw new ModelNotFoundException(_('Invalid code'));
			}
		} else {
			$authy = new AuthyApi(Yaml::get('key','wl.phone.settings'));
			if ($testing) {
				$response = $authy->phoneVerificationCheck($testingNumbers[$phone->number][1], $testingNumbers[$phone->number][0], $code);
			} else {
				$response = $authy->phoneVerificationCheck($phone->number, $phone->prefix, $code);
			}

			if(!$response->ok()) {
				throw new ModelNotFoundException(_($response->message()));
			};
		}
		// if phone does not yet have a verified_at value then it is the first time being verified
		if (!$phone->verified_at) {
			ProfileService::setProfilePhoneVerified($phone->rel_id);
		}
		$response = $this->phoneRepository->setCompleteActivation($phone);

		return $response;
	}

	/**
	 * @param $phone_id
	 * @param string $mode
	 * @param callable $onVerify
	 * @return array|null
	 */
	public function verifyPhone(Phone $phone, $mode = Phone::VERIFIED_BY_SMS, Closure $onVerify = null)
	{
		$phone->verified_by = $mode;

		$phone->setCode(
			$phone->generateCode()
		);

		if(! $phone)
			throw new ModelNotFoundException(_('Invalid phone'));

		$this->phoneRepository
			->addActivation($phone);

		$response = Event::fire('phone.verifying', [$phone]);

		if($onVerify)
			return $onVerify($response, $phone->getCode());

		return $phone->getCode();
	}

	public function editPhone(Phone $phone, $newNumber, $mode = Phone::VERIFIED_BY_SMS)
	{
		$phone->new_number = $newNumber;

		$this->verifyPhone($phone, $mode);

		$phone->save();

		return $phone;
	}



	/**
	 * Get diff phones ..
	 *
	 * @param Collection $phones
	 * @param Collection $phonesVerifier
	 * @return static
	 */
	protected function getDiffPhones(Collection $phones, Collection $phonesVerifier) {
		$toReturn = [];
		$phones->each(function($phone) use($phonesVerifier, &$toReturn) {
			if(! $phone instanceof Phone )
				return false;

			$isAlreadyExists = false;
			foreach ($phonesVerifier as $phonesVerify) {
				if(! $phonesVerify instanceof Phone )
					return false;

				if( $phonesVerify->getPhone() == $phone->getPhone() ) {
					$isAlreadyExists = true;
					break;
				}
			}

			if( ! $isAlreadyExists )
				$toReturn[] = $phone;
		});

		return $toReturn;
	}

	/**
	 * Extract phone duplicates ..
	 *
	 * @param array $phones
	 * @return array
	 */
	protected function removeDuplicates(array $phones = array()) {
		$cleanPhones = array();

		foreach ($phones as $key => $phone) {
			if(! $phone) continue;

			$fullNumber = sprintf('%s%s', $phone->prefix, $phone->number);

			if( in_array($fullNumber, $cleanPhones) ) {
				unset($phones[$key]);
				continue;
			}

			$cleanPhones[] = $fullNumber;
		}

		return $phones;
	}
}

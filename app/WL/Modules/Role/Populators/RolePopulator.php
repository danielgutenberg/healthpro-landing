<?php

namespace WL\Modules\Role\Populators;

use WL\Modules\Role\Models\Role;

interface RolePopulator {
	/**
	 * @param \WL\Modules\Role\Models\Role $page
	 *
	 * @return $this
	 */
	public function setModel(Role $page);
}

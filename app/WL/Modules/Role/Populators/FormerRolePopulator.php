<?php namespace WL\Modules\Role\Populators;

use WL\Modules\Role\Models\Role;
use WL\Populators\FormerPopulator;
use WL\Modules\Role\Populators\RolePopulator;

class FormerRolePopulator extends FormerPopulator implements RolePopulator {

	/**
	 * We accept only the Role model here
	 *
	 * @param \WL\Modules\Role\Models\Role $model
	 * @return $this
	 */
	public function setModel(Role $model) {
		$this->model = $model;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getFields() {
		$fields         = $this->model->toArray();

		return $fields;
	}
}

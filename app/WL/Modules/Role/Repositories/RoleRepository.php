<?php namespace WL\Modules\Role\Repositories;

interface RoleRepository {

	/**
	 * Get a list of pages with filters
	 *
	 * @param $filters
	 * @return mixed
	 */
	public function getFiltered( $filters = [] );
}

<?php namespace WL\Modules\Role\Repositories;

use WL\Modules\Role\Models\Role;
use WL\Repositories\DbRepository;
use WL\Modules\Role\Repositories\RoleRepository;

class DbRoleRepository extends DbRepository implements RoleRepository {

	/**
	 * @var Role
	 */
	protected $model;

	protected $modelClassName = '\WL\Modules\Role\Models\Role';

	public function __construct(Role $model) {
		$this->model = $model;
	}

	public function getFiltered($filters = []) {

	}
}

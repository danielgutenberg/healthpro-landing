<?php
namespace WL\Modules\Role\Models;

use Cartalyst\Sentinel\Roles\EloquentRole;
use WL\Modules\Notification\Models\NotifiableInterface;
use WL\Models\ModelTrait;

use WL\Contracts\Presentable;
use WL\Models\PresentableTrait;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use WL\Modules\Notification\Facades\NotificationService;

class Role extends EloquentRole implements SluggableInterface, Presentable
{
	use ModelTrait;
	use SluggableTrait;
	use PresentableTrait;

	protected $sluggable = array(
		'build_from' => 'name',
		'save_to'    => 'slug',
		'on_update'  => true,
		'unique'     => true,
	);

	protected $presenter = 'WL\Modules\Role\Presenters\RolePresenter';

	public function savePermissions($permissions = array()) {
		$this->clearPermissions();

		if (count($permissions)) {
			array_map(function ($permission) {

				# By default all permission are set to true.
				$this->addPermission($permission, true);
			}, $permissions);
		}

		return $this->save();
	}

	public function clearPermissions() {
		$this->permissions = array();

		return $this->save();
	}

}

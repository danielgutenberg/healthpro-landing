<?php namespace WL\Modules\Profile\ProfilesMeta;

use WL\Modules\Profile\Contracts\MetaProvideAble;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Services\ModelProfileService;

class SmsNotificationsProvider implements MetaProvideAble {

	public static $fields = [
		'receive_appointment_reminder',
		'receive_appointment_reminder_time',
		'phone_sms_activated',
	];

	/**
	 * @var ModelProfile
	 */
	private $profile;

	public function __construct(ModelProfile $profile) {
		$this->profile = $profile;
	}

	/**
	 * @param array $merged
	 * @return array
	 */
	public function provide(array $merged = array()) {
		$meta = array_merge($merged, self::$fields);

		return app(ModelProfileService::class)->loadProfileMetaFields($this->profile->id, $meta);
	}
}

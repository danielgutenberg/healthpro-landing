<?php namespace WL\Modules\Profile\ProfilesMeta;


use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Support\Collection;

abstract class MetaCast
{
	protected $fields = [];

	protected function asDateTime($value)
	{
		if ($value instanceof Carbon) {
			return $value;
		}

		if ($value instanceof DateTimeInterface) {
			return new Carbon(
				$value->format('Y-m-d H:i:s.u'), $value->getTimeZone()
			);
		}

		if (is_numeric($value)) {
			return Carbon::createFromTimestamp($value);
		}

		if (preg_match('/^(\d{4})-(\d{1,2})-(\d{1,2})$/', $value)) {
			return Carbon::createFromFormat('Y-m-d', $value)->startOfDay();
		}

		return Carbon::createFromFormat($this->getDateFormat(), $value);
	}

	public function cast($key, $value)
	{
		if(!array_key_exists($key, $this->fields)){
			return $value;
		}

		switch ($this->fields[$key]) {
			case 'int':
			case 'integer':
				return (int) $value;
			case 'real':
			case 'float':
			case 'double':
				return (float) $value;
			case 'string':
				return (string) $value;
			case 'bool':
			case 'boolean':
				return (bool) $value;
			case 'object':
				return json_decode($value);
			case 'array':
			case 'json':
				return json_decode($value, true);
			case 'collection':
				return collect(json_decode($value, true));
			case 'date':
			case 'datetime':
				return $this->asDateTime($value);
			case 'timestamp':
				return $this->asDateTime($value)->getTimestamp();
			default:
				return $value;
		}
	}

	public function getCast($key)
	{
		if(!isset($this->fields[$key])){
			return null;
		}
		return $this->fields[$key];
	}

	public static function hydrate($data)
	{
		if($data instanceof Collection){
			return $data->map(function($item){
				return self::hydrate($item);
			});
		}
		$model = new static();
		foreach ($data as $attribute => $value){
			$model->$attribute = $model->cast($attribute, $value);
		}
		return $model;
	}
}

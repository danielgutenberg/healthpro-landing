<?php namespace WL\Modules\Profile\ProfilesMeta;

use WL\Modules\Profile\Contracts\MetaProvideAble;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Services\ModelProfileService;

class EmailNotificationsProvider implements MetaProvideAble {

	public static $fields = [
		'notify_changes_by_email',
		'send_email_on_important_changes',
		'send_offers_by_email',
		'send_promotional_email',
	];

	/**
	 * @var ModelProfile
	 */
	private $profile;

	public function __construct(ModelProfile $profile) {
		$this->profile = $profile;
	}

	/**
	 * @param ModelProfile $profile
	 * @param array $merged
	 * @return array
	 */
	public function provide(array $merged = array()) {
		$meta = array_merge($merged, self::$fields);

		return app(ModelProfileService::class)->loadProfileMetaFields($this->profile->id, $meta);
	}
}

<?php namespace WL\Modules\Profile\ProfilesMeta;

class ProviderClientsMeta extends MetaCast
{

	const KEY_RANGE_EXEMPTION = 'range_exemption';
	const INTRO_SESSION_EXEMPTION = 'intro_session_exemption';

	protected $fields = [
		self::KEY_RANGE_EXEMPTION => 'boolean',
		self::INTRO_SESSION_EXEMPTION => 'boolean',
	];
}

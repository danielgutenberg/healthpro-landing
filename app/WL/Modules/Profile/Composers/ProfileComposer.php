<?php namespace WL\Modules\Profile\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Contracts\Composable;
use WL\Modules\Notification\Facades\NotificationService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Security\Facades\AuthorizationUtils;
use WL\Modules\User\Services\UserServiceImpl;

class ProfileComposer implements Composable {

	public function compose(View $view) {
		$profile = null;

		$user = AuthorizationUtils::loggedInUser();

		$view->currentUser = $user;
		$currentProfileId = ProfileService::getCurrentProfileId();


		if ($currentProfileId) {
			try {
				$profile = ProfileService::loadProfileBasicDetails($currentProfileId);
			} catch (ModelNotFoundException $ex) {
				// profile id is wrong or not found
			}
		}

		$view->notifications = NotificationService::getInboxNotifications(
			$user,
			[],
			[],
			[],
			[['updated_at','desc']]
		);

		//looks evil
		if (isset($profile->type) && $profile->type == ModelProfile::PROVIDER_PROFILE_TYPE) {
			$profile->type = 'professional';
		}

		$view->currentProfile = $profile;
	}
}

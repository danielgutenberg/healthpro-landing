<?php namespace WL\Modules\Profile\Events;


use WL\Events\BasicEvents\EmailFromProfessionalEvent;


class ProfileHasBeenAlreadyRegisteredEmailEvent extends EmailFromProfessionalEvent
{
}

<?php namespace WL\Modules\Profile\Events;


class BaseProfileEvent implements SearchableEventInterface
{
	private $profileId;

	/**
	 * Event constructor.
	 * @param $profileId
	 */
	public function __construct($profileId)
	{
		$this->profileId = $profileId;
	}

	/**
	 * @return int
	 */
	public function getProfileId()
	{
		return $this->profileId;
	}
}

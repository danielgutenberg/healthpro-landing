<?php namespace WL\Modules\Provider\Events;

use WL\Events\BasicEvents\BasicSystemToProfileNotificationEvent;

class ProviderAppointmentBookedNotification extends BasicSystemToProfileNotificationEvent
{
	public function __construct($provider, $bladeData)
	{
		parent::__construct($provider, $bladeData);
	}
}

<?php namespace WL\Modules\Provider\Events;

use WL\Events\BasicEvents\BasicSystemToProfileNotificationEvent;

class ProviderPendingAppointmentNotification extends BasicSystemToProfileNotificationEvent
{
	public function __construct($providerProfile, $bladeData)
	{
		parent::__construct($providerProfile, $bladeData);
	}
}

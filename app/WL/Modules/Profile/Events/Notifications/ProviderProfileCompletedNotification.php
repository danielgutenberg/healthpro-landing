<?php namespace WL\Modules\Profile\Events;

use WL\Events\BasicEvents\BasicSystemToProfileNotificationEvent;
use WL\Modules\Helpers\Facades\EventInfoBuilder;

class ProviderProfileCompletedNotification extends BasicSystemToProfileNotificationEvent
{
	function __construct($provider)
	{
		$data = EventInfoBuilder::collectProfileInfo($provider->id, 'provider');
		parent::__construct($provider, $data, null);
	}
}

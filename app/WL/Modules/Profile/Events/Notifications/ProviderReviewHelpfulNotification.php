<?php namespace WL\Modules\Provider\Events;

use WL\Events\BasicEvents\BasicSystemToProfileNotificationEvent;

class ProviderReviewHelpfulNotification extends BasicSystemToProfileNotificationEvent
{
	function __construct($providerProfile, $bladeData, $expireAt = null)
	{
		parent::__construct($providerProfile, $bladeData, $expireAt);
	}
}

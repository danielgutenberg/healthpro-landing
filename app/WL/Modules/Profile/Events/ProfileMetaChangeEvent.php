<?php namespace WL\Modules\Profile\Events;


class ProfileMetaChangeEvent extends BaseProfileEvent
{

    protected $fields;

    public function __construct($profileId, $fields = null)
    {
        parent::__construct($profileId);
        $this->fields = $fields;
    }

    /**
     * @return array|null
     */
    public function getFields()
    {
        return $this->fields;
    }

}

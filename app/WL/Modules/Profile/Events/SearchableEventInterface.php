<?php namespace WL\Modules\Profile\Events;

interface SearchableEventInterface
{
	public function getProfileId();
}

<?php namespace WL\Modules\Profile\Events;


class ProfileEmailChangeEvent extends BaseProfileEvent
{
	protected $email;

	public function __construct($profileId, $email)
	{
		parent::__construct($profileId);
		$this->email = $email;
	}

	public function getEmail()
	{
		return $this->email;
	}
}

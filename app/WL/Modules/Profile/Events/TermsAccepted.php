<?php namespace WL\Modules\Profile\Events;

class TermsAccepted implements SearchableEventInterface
{
    private $profileId;

    /**
     * SessionAdded constructor.
     * @param $profileId
     */
    public function __construct($profileId)
    {
        $this->profileId = $profileId;
    }

    /**
     * @return mixed
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

}
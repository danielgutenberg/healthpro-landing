<?php namespace WL\Modules\Profile\Events;

 use WL\Events\BasicEvents\EmailFromProfessionalEvent;

 class ProfileHasBeenRegisteredEmailEvent extends EmailFromProfessionalEvent
{
	public $shouldQueue = true;
     protected $sendFromSystem = false;

}

<?php namespace WL\Modules\Profile\Models;

use WL\Modules\User\Models\User;
use DB;
use Event;
use Illuminate\Support\Collection;
use WL\Modules\User\Exceptions\UserExceptions;
use WL\Modules\Profile\Exceptions\MainProfileNotFoundException;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Models\ProviderProfile;

trait ManageProfileTrait {

	/**
	 * Check if current user own profile by id ...
	 *
	 * @param $profile_id
	 * @return bool
	 * @throws UserExceptions
	 */
	public function owns($profile_id) {
		if(! $profile_id)
			throw new UserExceptions('Invalid profile id');

		$profile = $this->profiles()
			->where('profile_id', '=', $profile_id)
			->where('is_owner', '=', 1)
			->first();

		if( $profile )
			return true;

		return false;
	}

	/**
	 * Return main profile for current user ...
	 *
	 * @param bool $throwException
	 * @throws MainProfileNotFoundException
	 * @internal param string $type
	 * @return mixed
	 */
	public function profile($throwException = false) {
		$profile = $this->profiles(ModelProfile::class)
			->where('is_owner', '=', 1)
			->first();

		if(! isset($profile->id)) {
			if( $throwException )
				throw new MainProfileNotFoundException('Not found main profile');

			return false;
		}

		return $profile->typeInstance();
	}

	/**
	 * Check of User $user is invited by current user to manage their profile ...
	 *
	 * @param \WL\Modules\User\Models\User $user
	 * @return bool
	 */
	public function isInvited(User $user) {
		$result = $user->profiles(ProviderProfile::class)
			->where('profile_id', '=', $this->profile()->id)
			->where('is_owner', '!=', 1);

		if( count($result->get()) )
			return true;

		return false;
	}

	/**
	 * Check if user for current user profile is manager ...
	 *
	 * @param \WL\Modules\User\Models\User $user
	 * @return bool
	 * @throws MainProfileNotFoundException
	 */
	public function isManager(User $user) {
		$pivot = $user->profiles(ProviderProfile::class)
			->where('profile_id', '=', $this->profile(true)->id)
			->where('is_owner', '!=', 1);

		if( $pivot->first() )
			return true;

		return false;
	}


	/**
	 * Get all invitations ..
	 *
	 * @return mixed
	 */
	public function invitations() {
		return $this->profiles(ProviderProfile::class)
			->where('is_owner', '!=', 1)
			->where('accepted', '=', 0)->get();
	}

	/**
	 * Get all managers for current user profile ...
	 * @param bool $accepted
	 * @return bool|static
	 * @throws UserExceptions
	 * @throws MainProfileNotFoundException
	 */
	public function managers($accepted = true) {
		try {
			$profile = $this->profile(false);

			if(! isset($profile->id))
				return false;

			$query = DB::table('users_profiles')
				->join('users', 'users.id', '=', 'users_profiles.user_id')
				->join('profiles', 'profiles.id', '=', 'users_profiles.profile_id')
				->where('profile_id', '=', $profile->id)
				->where('is_owner', '=', 0);

			if( $accepted )
				$query->where('accepted', '=', 1);

			$managers = Collection::make($query->pluck('user_id'));

			$objects = $managers->map(function($manager) use($profile) {
				$user = User::find($manager);

				return $profile->users()->where('user_id', '=', $user->id)->where('is_owner', '=', 0)->first();
			}, $managers);

			return $objects;

		} catch(UserExceptions $e) {
			throw new UserExceptions($e);
		}
	}

	/**
	 * Return an list of non-managers for current user profile ...
	 *
	 * @return static
	 * @throws MainProfileNotFoundException
	 */
	public function nonManagers() {
		$profile = $this->profile(false);

		if(! isset($profile->id))
			return false;

		$nonManagers = Collection::make(
			DB::table('users')
				->select('users.id as user_id')
				->join('profiles', 'profiles.id', '=', 'profiles.id')
				->leftJoin('users_profiles', 'users.id', '=', 'users_profiles.user_id')
				->where('profiles.id', '=', $profile->id)
				->whereNull('users_profiles.profile_id')->pluck('user_id')
		);

		$objects = $nonManagers->map(function($manager) {
			return User::find($manager);
		}, $nonManagers);

		return $objects;
	}


	/**
	 * Invite user ...
	 *
	 * @param \WL\Modules\User\Models\User $user
	 * @param array $permissions
	 * @return bool
	 * @throws UserExceptions
	 * @throws MainProfileNotFoundException
	 */
	public function invite(User $user, array $permissions = [], $updateInvitation = false) {
		try {
			$jsonConvertedPermissions = [];
			foreach ($permissions as $permission) {
				$jsonConvertedPermissions[$permission] = true;
			}

			if( $this->isInvited($user) ) {
				if( $updateInvitation ) {
					$user->profiles(ProviderProfile::class)
						->updateExistingPivot($this->profile()->id, ['permissions' => json_encode($jsonConvertedPermissions, JSON_FORCE_OBJECT)]);

					return true;
				}

				throw new UserExceptions('User already invited');
			}

			if( $user->id == $this->id )
				throw new UserExceptions('Users are the same');

			$profileOwner = $this->profile(true);

			$user->profiles()->attach($profileOwner, [
				'is_owner'    => 0,
				'permissions' => json_encode($jsonConvertedPermissions, JSON_FORCE_OBJECT)
			]);

			Event::fire('user.invite', [$user]);

			return true;

		} catch(UserExceptions $e) {
			throw new UserExceptions($e);
		}
	}

	/**
	 * Reject user to manage current user profile ...
	 *
	 * @param User $user
	 * @return bool
	 * @throws UserExceptions
	 */
	public function exclude(User $user) {
		try {
			if( $user->id == $this->id )
				throw new UserExceptions('Users are the same');

			if( ! $this->isManager($user) )
				throw new UserExceptions('User are not manager to current user profile');

			$profileOwner = $this->profile(true);

			$user->profiles()->detach($profileOwner);

			Event::fire('user.exclude', [$user]);

			return true;

		} catch(UserExceptions $e) {
			throw new UserExceptions($e);
		}
	}

	/**
	 * Accept current user to manager an profile ...
	 *
	 * @param ProviderProfile $profile
	 * @return bool
	 * @throws UserExceptions
	 */
	public function accept(ProviderProfile $profile) {
		$pivot = $this->profiles(ProviderProfile::class)
			->where('profile_id', '=', $profile->id)
			->where('is_owner', '!=', 1);

		if( !isset($pivot->first()->pivot) )
			throw new UserExceptions('You aren`t invited to manage that profile');

		$this->profiles(ProviderProfile::class)
			->where('is_owner', '!=', 1)
			->updateExistingPivot($profile->id, ['accepted' => 1]);

		Event::fire('user.accept', [$this]);

		return true;
	}

	/**
	 * Accept all invitations ...
	 *
	 * @return bool
	 * @throws UserExceptions
	 */
	public function acceptAll() {
		$invitations = $this->invitations();

		foreach ($invitations as $invitation) {
			$this->accept(ProviderProfile::find($invitation->pivot->profile_id));
		}

		return true;
	}

	/**
	 * Decline an invitation ...
	 *
	 * @param ProviderProfile $profile
	 * @return bool
	 * @throws UserExceptions
	 */
	public function decline(ProviderProfile $profile) {
		$pivot = $this->profiles()
			->where('profile_id', '=', $profile->id)
			->where('is_owner', '!=', 1);

		if( ! $pivot->first() )
			throw new UserExceptions('You aren`t invited to manage that profile');

		$this->profiles()
			->where('is_owner', '!=', 1)
			->detach($profile->id);

		Event::fire('user.decline', [$this]);

		return true;
	}

	/**
	 * Decline all invitations ...
	 *
	 * @return bool
	 * @throws UserExceptions
	 */
	public function declineAll() {
		$invitations = $this->invitations();

		foreach ($invitations as $invitation) {
			$this->decline(ProviderProfile::find($invitation->pivot->profile_id));
		}

		return true;
	}
}

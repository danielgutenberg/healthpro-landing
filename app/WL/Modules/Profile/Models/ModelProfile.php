<?php namespace WL\Modules\Profile\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use WL\Modules\Location\Models\Location;
use WL\Modules\Notification\Models\NotifiableTrait;
use WL\Modules\Order\Models\Order;
use WL\Modules\Provider\Models\ProviderService;
use WL\Modules\Phone\Phone;
use WL\Modules\User\Models\User;
use WL\Modules\Profile\Exceptions\ProfileException;
use WL\Modules\Profile\Presenters\ProfilePresenter;
use WL\Models\ModelBase;
use WL\Models\PresentableTrait;
use WL\Models\ModelTrait;
use WL\Models\MetaTrait;

class ModelProfile extends ModelBase
{

	use PresentableTrait;

	use ModelTrait;

	use MetaTrait;

	use SoftDeletes;

	// again this shit TRAITS ):
	use NotifiableTrait;

	const CLIENT_PROFILE_TYPE = 'client';
	const PROVIDER_PROFILE_TYPE = 'provider';
	const STAFF_PROFILE_TYPE = 'staff';


	static $BASIC_INFO_PROFILE_META_DETAILS = [
		'value' => [
			'title',
			'gender',
			'first_name',
			'last_name',
			'birthday',
			'occupation',
		],
		'date_value' => [
			'birthday'
		],
		'image' => [
			'avatar' => '250x250'
		]
	];

	protected $presenter = ProfilePresenter::class;

	protected $table = 'profiles';

	protected $fillable = ['type', 'slug', 'is_bookable', 'is_searchable', 'verified', 'created_at', 'updated_at', 'featured', 'status'];

	protected $metaClassName = ProfileMeta::class;

	protected $rules = array(
		'is_searchable' => 'boolean',
		'verified'      => 'boolean',
		'type'			=> 'required|in:'.ModelProfile::CLIENT_PROFILE_TYPE.','.ModelProfile::PROVIDER_PROFILE_TYPE.','.ModelProfile::STAFF_PROFILE_TYPE,
	);

	/**
	 * Get attached users ...
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function users()
	{
		return $this->belongsToMany(User::class, 'users_profiles', 'profile_id', 'user_id')->withPivot('is_owner', 'permissions', 'accepted');
	}

	/**
	 * Get owner for current profile ...
	 *
	 * @deprecated
	 * @param bool $asObject
	 * @throws ProfileException
	 * @return mixed|null
	 */
	public function owner($asObject = false, $throwException = true)
	{
		$ownerPivot = $this->users()->where('is_owner', '=', 1)->get()->first();

		if (!isset($ownerPivot->id)){
			if($throwException){
				throw new ProfileException('Current profile do not have yet owner');
			}else{
				return null;
			}
		}

		if ($asObject)
			return User::find($ownerPivot->pivot->user_id)->first();

		return $ownerPivot;
	}

	/**
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Helpers                                                              *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */

	/**
	 * Set type profile ..
	 *
	 * @param $type
	 * @return bool
	 */
	public function setType($type)
	{
		$this->type = $type;

		return $this->save();
	}

	/**
	 * Get profile type ...
	 *
	 * @return null
	 */
	public function type()
	{
		return $this->type;
	}

	/**
	 * Assert type ..
	 *
	 * @param $type
	 * @return bool
	 */
	public function assertType($type)
	{
		if ($type == $this->type())
			return true;

		return false;
	}

	/**
	 * Return an type instance ..
	 *
	 * @return $this|mixed|null
	 */
	public function typeInstance()
	{
		if ($this->type() == self::CLIENT_PROFILE_TYPE) {
			return ClientProfile::typeCast($this);
		} elseif ($this->type() == self::PROVIDER_PROFILE_TYPE) {
			return ProviderProfile::typeCast($this);
		}elseif ($this->type() == self::STAFF_PROFILE_TYPE) {
			return StaffProfile::typeCast($this);
		}

		return $this;
	}

	public static function typeCast(ModelProfile $object)
	{
		$newObject = new static;

		foreach($object as $property => $value) {
			$newObject->$property = $value;
		}

		return $newObject;
	}

    public function services()
    {
        return $this->hasMany(ProviderService::class, 'profile_id');
    }

	public function locations()
	{
		return $this->belongsToMany(Location::class, 'locations_profiles', 'profile_id');
	}

	public function orders()
	{
		return $this->hasMany(Order::class, 'profile_id');
	}

    public function phone()
    {
        return $this->morphOne(Phone::class, 'rel');
    }

}

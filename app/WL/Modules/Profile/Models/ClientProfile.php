<?php namespace WL\Modules\Profile\Models;

class ClientProfile extends ModelProfile
{

	protected $attributes = [
		'type' => ModelProfile::CLIENT_PROFILE_TYPE
	];
}

<?php namespace WL\Modules\Profile\Models;

use WL\Models\MetaTrait;
use WL\Models\ModelBase;

class ProviderOriginated extends ModelBase
{
	const MAX_INVITES = 3;
	const INVITE_INTERVAL_HOURS = 24;

	public $table = 'provider_originated';

	protected $dates = [
		'last_invite',
	];

	protected $casts = [
		'approved' => 'boolean',
		'originated' => 'boolean',
	];

	public function provider()
	{
		return $this->belongsTo(ProviderProfile::class);
	}
}

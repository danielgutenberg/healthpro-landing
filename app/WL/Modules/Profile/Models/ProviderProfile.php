<?php namespace WL\Modules\Profile\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use WL\Contracts\Presentable;
use WL\Modules\Education\Traits\EducationTrait;
use WL\Modules\Attachment\Facades\AttachmentService;
use WL\Modules\Phone\Phone;
use WL\Modules\Phone\PhoneTrait;
use WL\Modules\Profile\Presenters\ProfilePresenter;

class ProviderProfile extends ModelProfile implements Presentable, SluggableInterface
{
	use EducationTrait;

	use PhoneTrait;

	use SluggableTrait;

    const STATUS_INACTIVE = 'inactive';
    const STATUS_INCOMPLETE = 'incomplete';
    const STATUS_REGISTERED = 'registered';
    const STATUS_MEMBER = 'member';
    const STATUS_PRIVATE = 'private';

	protected $attributes = [
		'type' => ModelProfile::PROVIDER_PROFILE_TYPE
	];

	static $PROVIDER_ITEMS_TAGS = [
		'educational_activity' => 'educational-activity',
		'languages' => 'language',
		'age_group' => 'age-group',
		'type_of_services' => 'type-of-services',
		'gender_identities' => 'gender-identities',
		'special_needs' => 'special-needs',
		'session_format' => 'session-format',
		'concerns_conditions' => 'concerns-conditions',
		'experience' => 'Experience'
	];


	const PROVIDER_CANCELLATION_POLICY_META_NAME = 'provider_cancellation_policy';

	protected $presenter = ProfilePresenter::class;

	/**
	 *    Get list of Provider Certifications ...
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function certifications()
	{
		return $this->belongsToMany('\WL\Modules\Certification\Models\Certification', 'profiles_certifications', 'profile_id', 'certification_id');
	}

	/**
	 *    Get list of uploaded Certifications
	 *
	 * @return mixed
	 */
	public function uploadCertifications()
	{
		return AttachmentService::attachmentCollection('upload_certifications', $this);
	}

	/**
	 *    Attach new certifications to that profile ...
	 *
	 * @param array $certifications
	 * @return array|bool
	 */
	public function saveCertifications($certifications = [])
	{
		return $this->syncManyToMany($certifications, 'certifications');
	}

	/**
	 * Set url for provider ..
	 *
	 * @param $url
	 * @return bool
	 */
	public function saveSlug($url) {
		$this->setSlug(
			$this->createSlug($url)
		);

		return $this->save();
	}
}

<?php

namespace WL\Modules\Profile\Models;

use WL\Models\ModelBase;

class ProfileJob extends ModelBase
{
	protected $table = 'profiles_jobs';

	protected $fillable = ['company_name', 'job_title', 'from_month', 'from_year', 'to_month', 'to_year'];
}

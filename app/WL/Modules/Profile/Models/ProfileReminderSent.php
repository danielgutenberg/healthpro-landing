<?php namespace WL\Modules\Profile\Models;

use WL\Models\ModelBase;

class ProfileReminderSent extends ModelBase
{
	protected $table = 'profile_reminder_sent';

}

<?php namespace WL\Modules\Profile\Models;

use WL\Models\ModelMeta;

class ProfileMeta extends ModelMeta
{

	/**
	 * @var string
	 */
	protected $table = 'profiles_meta';


}

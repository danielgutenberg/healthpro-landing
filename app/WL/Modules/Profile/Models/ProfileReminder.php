<?php namespace WL\Modules\Profile\Models;

use WL\Models\ModelBase;

class ProfileReminder extends ModelBase
{
	const TYPE_MAIL = 'mail';
	const TYPE_SMS = 'sms';

	protected $table = 'profile_reminder';

}

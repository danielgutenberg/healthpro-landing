<?php namespace WL\Modules\Profile\Models;

use WL\Models\ModelBase;

class ProfileEducation extends ModelBase
{
	protected $table = 'profiles_educations';

	protected $fillable = ['degree', 'education_id'];

	public function getDates()
	{
		$res = parent::getDates();
		array_push($res, 'from', 'to');
		return $res;
	}
}

<?php namespace WL\Modules\Profile\Models;

use WL\Models\ModelBase;

class ProfileAsset extends ModelBase
{
	protected $table = 'profile_assets';

	protected $fillable = ['title', 'description', 'type', 'storage'];

}

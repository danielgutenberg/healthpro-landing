<?php namespace WL\Modules\Profile\Models;

class StaffProfile extends ModelProfile
{
	protected $attributes = [
		'type' => ModelProfile::STAFF_PROFILE_TYPE
	];

}

<?php namespace WL\Modules\Profile\Models;

use WL\Models\ModelBase;

class ProfileProfessionalBody extends ModelBase
{
	protected $table = 'profile_professional_body';

	protected $fillable = ['title', 'description'];

	public function getDates()
	{
		$res = parent::getDates();
		array_push($res, "expiry_date");
		return $res;
	}

}

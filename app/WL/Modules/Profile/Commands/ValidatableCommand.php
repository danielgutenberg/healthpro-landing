<?php namespace WL\Modules\Profile\Commands;

use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Translation\TranslatorInterface;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Services\Exceptions\AuthorizationException;

class ValidatableCommand extends AuthorizableCommand
{
	use DispatchesJobs;

	public $validator;
	protected $passValidData = false;
	protected $rules = null;
	protected $messages = [];
	protected $inputData;
	private $silentValidation;
	protected $customValidator;

	public function __construct($inputData = [])
	{
		$this->inputData = $inputData;
	}

	public function beforeHandle()
	{

	}

	public function handle()
	{
		$this->beforeHandle();

		if ($this->rules != null) {
			if (!$this->passValidData) {
				$this->throwValidation($this->rules, $this->messages);
				return App::call([$this, 'validHandle']);
			} else {
				list($this->inputData, $validator) = $this->getValidData($this->rules, $this->messages);
				$result = App::call([$this, 'validHandle']);
				$this->throwValidationError($validator);

				return $result;
			}
		}

		return App::call([$this, 'validHandle']);
	}

	public function runSubCommand($command, $defaultReturnValue = null, $silentValidation = false)
	{
		$return = $defaultReturnValue;
		$command->silentValidation = $silentValidation;
		try {
			$return = $this->dispatch($command);
		} catch (AuthorizationException $ex) {
			\Log::error($ex);
		}

		return $return;
	}

	private function makeValidator(array $data, array $rules, array $messages = null)
	{
		$messages = $messages ?: [];
		if (isset($this->customValidator)) {

			return new $this->customValidator(App::make(TranslatorInterface::class), $data, $rules, $messages);
		}

		return Validator::make($data, $rules, $messages);
	}

	private function throwValidation(array $rules, array $messages = null, array $data = null)
	{
		if ($data == null) {
			$data = $this->inputData;
		}
		$validator = $this->makeValidator($data, $rules, $messages);

		if (!$this->silentValidation) {
			$this->throwValidationError($validator);
		} else {
			$this->validator = $validator;
		}

	}

	private function throwValidationError($validator)
	{
		if ($validator->fails()) {
			throw new ValidationException($validator);
		}
	}

	protected function getValidData(array $rules, array $messages = null, array $data = null)
	{
		if ($data == null) {
			$data = $this->inputData;
		}
		$validator = $this->makeValidator($data, $rules, $messages);

		return [$validator->valid(), $validator];
	}

	/**
	 * Shorthand for "array_get($this->inputData, $key, $default)"
	 * @param $key String with dot (.) support
	 * @param null $default Default value
	 * @return mixed
	 */
	protected function get($key, $default = null)
	{
		return array_get($this->inputData, $key, $default);
	}

	/**
	 * @param $key
	 * @return boolean
	 */
	protected function has($key)
	{
		return array_has($this->inputData, $key);
	}

}

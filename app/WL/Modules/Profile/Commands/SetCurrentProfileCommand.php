<?php namespace WL\Modules\Profile\Commands;


use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\SecurityPolicies\ProfileAccessPolicy;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Services\Exceptions\AuthorizationException;

/**
 * Sets current profile
 *
 * Class SetCurrentProfileCommand
 * @package WL\Modules\Profile\Commands
 */
class SetCurrentProfileCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'profile.setCurrentProfile';

	private $profileId;
	private $validator;
	private $accessPolicy;

	function __construct($profileId)
	{

		$this->profileId = $profileId;

		$this->accessPolicy = new ProfileAccessPolicy();

		$this->validator = Validator::make(
			[
				'profileId' => $this->profileId
			],
			[
				'profileId' => 'required'
			]
		);
	}

	public function handle()
	{

		if (!$this->accessPolicy->canSwitchProfileTo($this->profileId)) {
			throw new AuthorizationException("Can't switch profile.");
		}

		if ($this->validator->passes())
			ProfileService::setCurrentProfileId($this->profileId);
		else
			throw new ValidationException($this->validator);
	}
}

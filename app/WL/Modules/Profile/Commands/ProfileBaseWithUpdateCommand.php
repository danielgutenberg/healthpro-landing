<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Profile\SecurityPolicies\ProfileAccessPolicy;


class ProfileBaseWithUpdateCommand extends ProfileBaseCommand
{
	public function __construct($inpData)
	{
		(new ProfileAccessPolicy())->canUpdateProfile($inpData['profile_id']);
		parent::__construct($inpData);
	}
}

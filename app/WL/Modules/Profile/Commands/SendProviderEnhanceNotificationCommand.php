<?php namespace WL\Modules\Provider\Commands;

use WL\Modules\Profile\Facades\ProfileService;

class SendProviderEnhanceNotificationCommand
{
	public function handle()
	{
		return ProfileService::sendProvidersReadyForEnhanceNotification();
	}

}

<?php namespace WL\Modules\Profile\Commands;


use WL\Modules\Profile\SecurityPolicies\ProfileAccessPolicy;


class ProfileBaseCommand extends ValidatableCommand
{
	protected $customValidator = ProfileValidator::class;
    protected $securityPolicy;

	public function __construct($inpData = [])
	{
		parent::__construct($inpData);
        $this->securityPolicy = new ProfileAccessPolicy();
	}
}

<?php namespace WL\Modules\Profile\Commands\ClientMeta;

use WL\Modules\Profile\Commands\ProfileBaseCommand;
use WL\Modules\Profile\Services\ProviderOriginatedServiceInterface;
use WL\Security\Services\Exceptions\AuthorizationException;

class ProviderClientMetaToggleCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'client.clientMetaToggleCommand';

	protected $rules = [
		'provider_id' => 'required|integer',
		'client_id' => 'required|integer',
		'meta_key' => 'required|string',
		'force' => 'sometimes|boolean'
	];

	public function validHandle(ProviderOriginatedServiceInterface $clientMetaService)
	{
        if(!$this->securityPolicy->canFetchClientMeta($this->get('provider_id'))) {
            throw new AuthorizationException('You do not have access to this information');
        }
		$value = $clientMetaService->toggleMeta($this->get('provider_id'), $this->get('client_id'), $this->get('meta_key'), $this->get('force'));
		return (object) [
			'meta_key' => $this->get('meta_key'),
			'meta_value' => $value,
		];
	}
}

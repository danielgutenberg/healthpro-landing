<?php namespace WL\Modules\Profile\Commands\ClientMeta;

use WL\Modules\Profile\Commands\ProfileBaseCommand;
use WL\Modules\Profile\Services\ProviderOriginatedServiceInterface;
use WL\Security\Services\Exceptions\AuthorizationException;

class ProviderClientMetaGetCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'client.clientMetaGetCommand';

	protected $rules = [
		'provider_id' => 'required|integer',
		'client_id' => 'required|integer',
		'fields' => 'sometimes|array',
	];

	public function validHandle(ProviderOriginatedServiceInterface $clientMetaService)
	{
        if(!$this->securityPolicy->canFetchClientMeta($this->get('provider_id'))) {
            throw new AuthorizationException('You do not have access to this information');
        }
		return $clientMetaService->getMetaData($this->get('provider_id'), $this->get('client_id'), $this->get('fields', []));
	}
}

<?php namespace WL\Modules\Profile\Commands;


use WL\Modules\Profile\Services\ModelProfileService;

class UpdateProfileAssetCommand extends ProfileBaseWithUpdateCommand
{
	const IDENTIFIER = 'profile.updateProfileAssetCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
		'asset_id' => 'required|integer'
	];

	public function validHandle(ModelProfileService $svc)
	{
		return ['success' => $svc->assetsService()->updateProfileAsset($this->inputData['asset_id'], $this->inputData)];
	}
}

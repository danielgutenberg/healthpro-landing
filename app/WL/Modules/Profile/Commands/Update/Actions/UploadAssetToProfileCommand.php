<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Profile\Services\ModelProfileService;

class UploadAssetToProfileCommand extends ProfileBaseWithUpdateCommand
{
	const IDENTIFIER = 'profile.uploadAssetToProfileCommand';


	protected $rules = [
		'profile_id' => 'required|integer',
		'asset' => 'required|asset'
	];

	public function validHandle(ModelProfileService $svc)
	{
		$asset = $svc->assetsService()->storeProfileAsset($this->inputData['profile_id'], $this->inputData['asset']);

		return ['success' => $asset != null, 'asset' => $asset];
	}
}

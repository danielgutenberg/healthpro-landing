<?php namespace WL\Modules\Profile\Commands;


use WL\Modules\Attachment\Facades\AttachmentService;
use WL\Modules\Profile\Services\ModelProfileService;

class UploadFileToProfileCommand extends ProfileBaseWithUpdateCommand
{
	const IDENTIFIER = 'profile.uploadFileToProfileCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
	];

	protected $filesToUpload =
		[
			'avatar'
		];


	public function validHandle(ModelProfileService $svc)
	{
		$profileId = $this->inputData['profile_id']; //$svc->getProfileById($this->inputData['profile_id']);
		$urls = [];
		$success = false;

		if ($profileId!=null) {

			$attaches = [];
			foreach ($this->filesToUpload as $field) {
				if (array_key_exists($field, $this->inputData)) {
					$files = $this->inputData[$field];

					$urls[$field] = [];
					if (is_array($files)) {
						foreach ($files as $file) {
							$attaches[] = $svc->saveUploadedFile($file, $field, $profileId, false);
						}
					} else {
						$attaches[] = $svc->saveUploadedFile($files, $field, $profileId, false);

					}

					foreach ($attaches as $attach) {
						if ($attach) {
							$items = AttachmentService::loadAttachmentUrlsById($attach->id);
							$items['id'] = $attach->id;
							$urls[$field][] = $items;
						}
					}

					$success = true;

				}
			}


		}

		return ['urls' => $urls, 'success' => $success];
	}
}

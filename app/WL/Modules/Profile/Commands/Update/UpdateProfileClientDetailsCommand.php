<?php namespace WL\Modules\Profile\Commands;

use Carbon\Carbon;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Services\ModelProfileService;

class UpdateProfileClientDetailsCommand extends ProfileBaseWithUpdateCommand
{
	const IDENTIFIER = 'profile.updateProfileClientDetailsCommand';

	protected $rules = [
		'profile' => 'required',
		'phone' => 'sometimes|numeric',
	];

	protected $metaFields = [
		'accepted_terms_conditions',
		'phone'
	];

	protected $filesToUpload = [

	];

	public function validHandle(ModelProfileService $svc)
	{
		$profile = $this->inputData['profile'];


		if ($profile && $profile->type == ModelProfile::CLIENT_PROFILE_TYPE) {

			if(
				isset($this->inputData['accepted_terms_conditions'])
				&&
				$accepted = $this->inputData['accepted_terms_conditions'] )
			{
				$date = null;
				if(! empty($accepted))
					$date = Carbon::now();

				$this->inputData['accepted_terms_conditions'] = $date->format('Y-m-d');
			}

			$svc->storeProfileMetaFields($profile->id, $this->inputData, $this->metaFields);

			foreach ($this->filesToUpload as $field) {
				if (array_key_exists($field, $this->inputData))
					$svc->saveUploadedFile($this->inputData[$field], $field, $profile->id, false);
			}

		}

		return true;
	}
}

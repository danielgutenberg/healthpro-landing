<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Profile\Facades\ProfileService;

class CheckProviderSlugCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.checkProviderSlugCommand';

	protected $rules = [
		'slug' => 'required|regex:/^[a-zA-Z0-9\-]*$/',
	];

	protected $messages = [
		'slug.regex' => 'Wrong slug syntax, only letters, digits and dash are allowed',
		'slug.required' => 'An empty slug is not allowed'
	];

	public function validHandle()
	{
		$profileId = ProfileService::getCurrentProfileId();

		return ProfileService::checkProfileSlug($profileId, $this->inputData['slug']);
	}
}

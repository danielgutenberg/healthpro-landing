<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Services\ModelProfileService;

class UpdateProfileStaffDetailsCommand extends ProfileBaseWithUpdateCommand
{
	const IDENTIFIER = 'profile.updateProfileStaffDetailsCommand';

	protected $rules = [
		'profile' => 'required',
		'phone'   => 'sometimes|numeric'
	];

	protected $metaFields = [
		'phone'
	];

	protected $filesToUpload = [

	];

	public function validHandle(ModelProfileService $svc)
	{
		$profile = $this->inputData['profile'];

		if ($profile && $profile->type == ModelProfile::STAFF_PROFILE_TYPE) {

			$svc->storeProfileMetaFields($profile->id, $this->inputData, $this->metaFields);

			foreach ($this->filesToUpload as $field) {
				if (array_key_exists($field, $this->inputData))
					$svc->saveUploadedFile($this->inputData[$field], $field, $profile->id, false);
			}

		}

		return true;
	}
}

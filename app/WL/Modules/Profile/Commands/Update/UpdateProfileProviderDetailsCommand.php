<?php namespace WL\Modules\Profile\Commands;

use Carbon\Carbon;
use WL\Modules\Payment\Models\PaymentMethod;
use WL\Modules\Phone\Commands\UpdateProfilePhonesCommand;
use WL\Modules\Profile\Models\ProviderProfile;

use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\ProfilesMeta\EmailNotificationsProvider;
use WL\Modules\Profile\ProfilesMeta\SmsNotificationsProvider;
use WL\Modules\Profile\Services\ModelProfileService;

class UpdateProfileProviderDetailsCommand extends ValidatableCommand
{
	const IDENTIFIER = 'profile.updateProfileProviderDetailsCommand';

	protected $customValidator = ProfileValidator::class;

	protected $passValidData = true;

	protected $rules = [
		'profile' => 'required',
//		'about_self_background' => 'sometimes|required|max:1000',
//		'about_what_i_do' => 'sometimes|required|max:1000',
//		'about_services_benefit' => 'sometimes|required|max:1000',
//		'about_self_specialities' => 'sometimes|required|max:1000',
		'tags' => 'sometimes|required|tags',
		'experience' => 'sometimes|required|tags',
		'professional_body' => 'sometimes|profbody',
		'educations' => 'sometimes|education',
		'jobs' => 'sometimes|work',
		'slug' => 'sometimes|regex:/^[a-zA-Z0-9\-]*$/',
		'phone' => 'sometimes|regex:/^1?\d{10}$/',
		'unavailable_before_minutes' => 'sometimes|integer',
		'is_blogger' => 'sometimes|boolean',
		'requires_professional_approval' => 'sometimes|boolean',
	];

	protected $messages = [
		'slug.regex' => 'Wrong slug syntax, only letters, digits and dash are allowed',
		'slug.required' => 'An empty slug is not allowed'
	];

	protected $metaFields = [
		'about_self_background',
		'about_what_i_do',
		'about_services_benefit',
		'about_self_specialities',
		'business_name',
		'business_name_display',
		'accepted_terms_conditions',
		'phone',
		'requires_professional_approval'
	];

	protected $filesToUpload = [

	];


	public function validHandle(ModelProfileService $svc)
	{
		$tagFields = ProviderProfile::$PROVIDER_ITEMS_TAGS;
		$profile = $this->inputData['profile'];
		if ($profile && $profile->type == ModelProfile::PROVIDER_PROFILE_TYPE) {


			if (isset($this->inputData['slug'])) {

				$svc->updateProfileSlug($profile->id, $this->inputData['slug']);
			}

			if (isset($this->inputData['business_name_display'])) {
				if ($this->inputData['business_name_display']) {
					$slug = $this->inputData['business_name'];
				} else {
					$details = $svc->loadProfileBasicDetailsModel($profile);
					$slug = $details->full_name;
				}
				$svc->updateProfileSlug($profile->id, $slug, true);
			}

			if (isset($this->inputData['is_blogger'])) {
				$svc->setProfileBloggerStatus($profile->id, $this->inputData['is_blogger']);
			}


			foreach ($this->filesToUpload as $field) {
				if (array_key_exists($field, $this->inputData))
					$svc->saveUploadedFile($this->inputData[$field], $field, $profile, false);
			}

			if (array_key_exists('educations', $this->inputData)) {
				$svc->educationService()->explicitSetProfileEducations($profile->id, $this->inputData['educations']);
			}

			if (array_key_exists('jobs', $this->inputData)) {
				$svc->jobService()->explicitSetProfileJobs($profile->id, $this->inputData['jobs']);
			}

			if (isset($this->inputData['professional_body']))
				$svc->professionalBodyService()->storeProfileProfessionalBody($profile->id, $this->inputData['professional_body']);

			if (isset($this->inputData['notifications'])) {
				$email = isset($this->inputData['notifications']['email']) ? $this->inputData['notifications']['email'] : [];
				$sms = isset($this->inputData['notifications']['sms']) ? $this->inputData['notifications']['sms'] : [];

				$svc->storeProfileMetaFields($profile->id, array_merge($email, $sms), array_merge(
					EmailNotificationsProvider::$fields,
					SmsNotificationsProvider::$fields
				));
			}


			$accepted = false;
			if (
				isset($this->inputData['accepted_terms_conditions'])
				&&
				$accepted = $this->inputData['accepted_terms_conditions']
			) {
				$date = null;
				if (!empty($accepted))
					$date = Carbon::now();

				$this->inputData['accepted_terms_conditions'] = $date->format('Y-m-d');
			}

			if (isset($this->inputData['phone']) && strlen($this->inputData['phone']) == 10) {
				$this->inputData['phone'] = '1' . $this->inputData['phone'];
			}
			$svc->storeProfileMetaFields($profile->id, $this->inputData, $this->metaFields, true);

			/*
			 * if he accepted terms and conditions we trigger an event to update if the professional is searchable
			 */
			if ($accepted) {
				$svc->acceptedTerms($profile->id);
			}

			if (isset($this->inputData['tags']))
				$svc->associateTagHelper($profile->id, $this->inputData['tags'], $tagFields);

			if(isset($this->inputData['unavailable_before_minutes'])){
				$svc->setProviderUnavailableTimeBeforeAppointment($profile->id,$this->inputData['unavailable_before_minutes'] );
			}
		}


		return true;
	}
}

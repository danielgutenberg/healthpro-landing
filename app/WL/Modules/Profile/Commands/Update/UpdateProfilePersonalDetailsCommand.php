<?php namespace WL\Modules\Profile\Commands;

use Carbon\Carbon;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\Profile\Services\ProfileReminderServiceInterface;

class UpdateProfilePersonalDetailsCommand extends ProfileBaseWithUpdateCommand
{
	const IDENTIFIER = 'profile.updateProfilePersonalDetailsCommand';

	protected $passValidData = true;

	protected $rules = [
		'first_name' => 'sometimes|required|string',
		'last_name' => 'sometimes|required|string',
		'birthday' => 'sometimes|required|birthday',
		'gender' => 'sometimes|string',
		'country_id' => 'sometimes|required',
		'profile' => 'required',
		'title' => 'sometimes|string',
		'social_pages' => 'sometimes|required|social',
		'reminders' => 'sometimes|required|reminders',
		'occupation' => 'sometimes|string',
		//'avatar' => 'sometimes|required|uploadedfile'

	];

	protected $metaFields = ['first_name', 'last_name', 'gender', 'country_id', 'title', 'birthday', 'occupation'];


	public function validHandle(ModelProfileService $svc, ProfileReminderServiceInterface $reminderSvc)
	{
		$profileId = $this->inputData['profile_id'];
		$updateSlug = $this->get('update_slug', false);

		if (array_key_exists('birthday', $this->inputData)) {
			if($this->inputData['birthday']['year'] . $this->inputData['birthday']['month'] . $this->inputData['birthday']['day']){
				$this->inputData['birthday'] = Carbon::create($this->inputData['birthday']['year'], $this->inputData['birthday']['month'], $this->inputData['birthday']['day']);
			} else {
				$this->inputData['birthday'] = null;
			}
		}

		if (array_key_exists('avatar', $this->inputData)) {
			$svc->saveUploadedFile($this->inputData['avatar'], 'avatar', $profileId, true);
		}

		if (array_key_exists('social_pages', $this->inputData)) {
			$svc->storeSocialAccounts($profileId, $this->inputData['social_pages']);
		}

		if (array_key_exists('avatarUrl', $this->inputData)) {
			$svc->setProfileAvatarFromUrl($profileId, $this->inputData['avatarUrl']);
		}

		foreach ($this->get('reminders', []) as $reminder) {
			$reminderSvc->update($reminder['id'], $reminder['value']);
		}

		$svc->storeProfileMetaFields($profileId, $this->inputData, $this->metaFields);

		if (isset($this->inputData['first_name']) || isset($this->inputData['last_name'])) {
			$svc->updateProfile($profileId, $updateSlug);
		}

		return true;
	}
}

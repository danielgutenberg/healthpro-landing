<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Facades\ProfileService;

class UpdateProfileCommand extends ProfileBaseWithUpdateCommand
{
	const IDENTIFIER = 'profile.updateProfileCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
	];

	public function validHandle()
	{
		$success = false;

		$profile = ProfileService::getProfileById($this->inputData['profile_id']);

		if ($profile != null) {
			$this->inputData['profile'] = $profile;

			$this->runSubCommand(new UpdateProfilePersonalDetailsCommand($this->inputData));

			$updaters = [
				ModelProfile::CLIENT_PROFILE_TYPE => new UpdateProfileClientDetailsCommand($this->inputData),
				ModelProfile::PROVIDER_PROFILE_TYPE => new UpdateProfileProviderDetailsCommand($this->inputData),
				ModelProfile::STAFF_PROFILE_TYPE => new UpdateProfileStaffDetailsCommand($this->inputData)
			];

			if (array_key_exists($profile->type, $updaters)) {

				$command = $updaters[$profile->type];
				$this->runSubCommand($command);
			}

			$success = true;
		}

		return ['success' => $success];
	}
}

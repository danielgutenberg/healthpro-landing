<?php namespace WL\Modules\Profile\Commands;


use WL\Modules\Profile\Facades\ProfileService;

class UpdateProfileBillingDetailsCommand extends ProfileBaseWithUpdateCommand
{
	const IDENTIFIER = 'profile.updateProfileBillingDetailsCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
        'bill_to_name' => 'required|string',
        'bill_to_address' => 'required|string',
	];

	public function validHandle()
	{
		$profileId = $this->inputData['profile_id'];
		$meta = [
			'bill_to_name' => $this->inputData['bill_to_name'],
			'bill_to_address' => $this->inputData['bill_to_address'],
		];
		ProfileService::storeProfileMetaFields($profileId, $meta, array_keys($meta));
		return ProfileService::loadProfileMetaFields($profileId, array_keys($meta));
	}
}

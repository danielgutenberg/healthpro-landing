<?php namespace WL\Modules\Profile\Commands;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\User\Facades\User;
use WL\Security\Facades\AuthorizationUtils;

class GetUserProfilesCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getUserProfilesCommand';

	protected $rules = [];

	public function validHandle(ModelProfileService $svc)
	{
		$user = Sentinel::check();

		$profiles = $svc->getUserProfiles($user->id);
		$profilesDetails = [];

		foreach ($profiles as $profile) {
			$profilesDetails[] = $svc->loadProfileBasicDetails($profile->id);
		}

		return $profilesDetails;
	}
}

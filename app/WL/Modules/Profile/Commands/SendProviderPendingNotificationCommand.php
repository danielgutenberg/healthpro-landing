<?php namespace WL\Modules\Provider\Commands;

use Carbon\Carbon;
use WL\Modules\Profile\Facades\ProfileService;

class SendProviderPendingNotificationCommand
{
	public $currentDate;

	public function __construct($date = null)
	{
		$this->currentDate = $date != null ? $date: Carbon::now();
	}

	public function handle()
	{
		return ProfileService::sendProviderHasTodayPendingAppointmentNotifications($this->currentDate);
	}

}

<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Attachment\Facades\AttachmentService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Services\ModelProfileService;

class DeleteFileFromProfileCommand extends ProfileBaseWithUpdateCommand
{
	const IDENTIFIER = 'profile.deleteFileFromProfileCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
		'file_id' => 'required|integer'

	];

	public function validHandle(ModelProfileService $svc)
	{
		$profileId = $this->inputData['profile_id'];

		$model = new ModelProfile();
		$model->id = $profileId;
		$attach = AttachmentService::findByIdSecure($this->inputData['file_id'], $model);

		if ($attach)
			return ['success' => AttachmentService::removeById($attach->id, $profileId)];

		return ['success' => false];
	}
}

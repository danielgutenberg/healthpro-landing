<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Attachment\Facades\AttachmentService;

use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Services\ModelProfileService;

class DeleteAssetFromProfileCommand extends ProfileBaseWithUpdateCommand
{
	const IDENTIFIER = 'profile.deleteAssetFromProfileCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
		'asset_id' => 'required|integer'

	];

	public function validHandle(ModelProfileService $svc)
	{
		$profileId = $this->inputData['profile_id'];

		if ($profileId != null) {
			$asset = $svc->assetsService()->getProfileAssetById($profileId, $this->inputData['asset_id']);

			if ($asset) {
				$attach_id = $asset->attachment_id;
				$profile = new ModelProfile();
				$profile->id = $profileId;
				$attach = AttachmentService::findByIdSecure($attach_id, $profile);

				if ($attach) {
					AttachmentService::removeById($attach->id);
					$svc->assetsService()->removeProfileAsset($profileId, $asset->id);

					return ['success' => true];
				}


			}
		}
		return ['success' => false];
	}
}

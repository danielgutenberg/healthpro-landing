<?php namespace WL\Modules\Profile\Commands;


use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Security\Services\Exceptions\AuthorizationException;

class AddProviderClientCommand extends ProfileBaseCommand
{
	const IDENTIFIER = "client.addProviderClient";

	protected $rules = [
		'clientId' => 'int|required',
		'providerId' => 'int|required',
	];

	public function validHandle()
	{
        if(!$this->securityPolicy->canRespondClientInvites($this->get('clientId'))) {
            throw new AuthorizationException('You do not have access to this action');
        }

		return ProviderOriginatedService::addProviderClient($this->get('providerId'), $this->get('clientId'));
	}
}

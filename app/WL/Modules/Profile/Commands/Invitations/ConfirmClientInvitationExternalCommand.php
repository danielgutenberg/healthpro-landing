<?php namespace WL\Modules\Profile\Commands;

use Exception;
use Illuminate\Support\Facades\Crypt;
use WL\Modules\Client\Facades\ClientService;
use WL\Modules\Profile\Exceptions\ProfileException;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\User\Exceptions\ActivationException;
use WL\Modules\User\Facades\User;

class ConfirmClientInvitationExternalCommand extends ValidatableCommand
{
	const IDENTIFIER = "client.confirmExternalClientInvitation";

	protected $rules = [
		'code' => 'required',
	];

	public function validHandle()
	{
		try {
			$data = Crypt::decrypt(base64_decode($this->get('code')));
			$email = $data['email'];
			$providerId = $data['providerId'];
		} catch (Exception $e){
			throw new ActivationException("Invitation code is invalid");
		}

		$user = User::getUserByEmail($email);
		if (!$user) {
			throw new ActivationException("Client not found");
		}

		$client = ProfileService::getUserProfiles($user->id, ModelProfile::CLIENT_PROFILE_TYPE)->first();

		if (!ProviderOriginatedService::confirmProviderClient($providerId, $client->id, array_get($data, 'decline', 0) != 1)) {
			throw new ProfileException('Invitation not found');
		}

		$data['userId'] = $user->id;

		return $data;
	}
}

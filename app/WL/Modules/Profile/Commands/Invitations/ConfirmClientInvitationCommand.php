<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Security\Services\Exceptions\AuthorizationException;

class ConfirmClientInvitationCommand extends ProfileBaseCommand
{
	const IDENTIFIER = "client.confirmClientInvitation";

	protected $rules = [
		'clientId' => 'int|required',
		'ProviderId' => 'int|required',
		'approve' => 'boolean'
	];

	public function validHandle()
	{
        if(!$this->securityPolicy->canRespondClientInvites($this->get('clientId'))) {
            throw new AuthorizationException('You do not have access to this action');
        }

		if (ProviderOriginatedService::confirmProviderClient($this->get('ProviderId'), $this->get('clientId'), $this->get('approve', true))) {
			return $this->get('approve');
		}
	}
}

<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Security\Services\Exceptions\AuthorizationException;

class GetProviderClientStatusCommand extends ProfileBaseCommand
{
	const IDENTIFIER = "client.getProviderClient";

	protected $rules = [
		'clientId' => 'int|required',
		'providerId' => 'int|required',
	];

	public function validHandle()
	{
        if(!$this->securityPolicy->canFetchClientProviders($this->get('clientId'))) {
            throw new AuthorizationException('You do not have access to this information');
        }

		$invite = ProviderOriginatedService::getProviderClient($this->get('providerId'), $this->get('clientId'));
		return (object) [
			'invited' => $invite->exists,
			'confirmed' => !is_null($invite->approved),
			'approved' => $invite->approved === true,
		];
	}
}

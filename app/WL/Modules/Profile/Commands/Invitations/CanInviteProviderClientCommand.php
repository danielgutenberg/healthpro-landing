<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Security\Services\Exceptions\AuthorizationException;

class CanInviteProviderClientCommand extends ProfileBaseCommand
{
	const IDENTIFIER = "provider.canInviteProviderClientCommand";

	protected $rules = [
		'clientId' => 'int|required',
		'providerId' => 'int|required',
	];

	public function validHandle()
	{
		$clientId = $this->get('clientId');
		$providerId = $this->get('providerId');

        if(!$this->securityPolicy->canImportClients($providerId)) {
            throw new AuthorizationException('You do not have access to this action');
        }

        $client = ProfileService::getProfileById($clientId);
        if ($client->type != ModelProfile::CLIENT_PROFILE_TYPE) {
			throw new AuthorizationException('You do not have access to this action');
		}

        $result = [
        	'status' 	 => '',
        	'can_invite' => true,
		];

        $providerClient = ProviderOriginatedService::getProviderClient($providerId, $clientId);

        if ($providerClient->exists) {
        	$result['can_invite'] = false;

        	if(is_null($providerClient->approved)) {
				$result['status'] = 'pending';
			} else {
				$result['status'] = $providerClient->approved ? 'approved' : 'declined';
			}
		}

		return (object) $result;
	}
}

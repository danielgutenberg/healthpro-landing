<?php namespace WL\Modules\Profile\Commands\Invitations;


use WL\Modules\Profile\Commands\ProfileBaseCommand;
use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Modules\Profile\Services\ProfileImporterInterface;
use WL\Security\Services\Exceptions\AuthorizationException;

class InviteProviderClientCommand extends ProfileBaseCommand
{
	const IDENTIFIER = "client.inviteExistingClient";

	protected $rules = [
		'client_id' => 'int|required',
		'provider_id' => 'int|required',
	];

	public function validHandle(ProfileImporterInterface $importService)
	{
        if(!$this->securityPolicy->canImportClients($this->get('provider_id'))) {
            throw new AuthorizationException('You do not have access to this action');
        }

        $invite = ProviderOriginatedService::inviteClient($this->get('provider_id'), $this->get('client_id'));

		$importService->sendInvite($invite->client_id, $invite->provider_id, false);

		return ProviderOriginatedService::collectInviteData($invite);
	}
}

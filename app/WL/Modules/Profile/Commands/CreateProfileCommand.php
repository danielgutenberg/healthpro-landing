<?php namespace WL\Modules\Profile\Commands;

use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\User\Facades\User;
use WL\Security\Facades\AuthorizationUtils;

class CreateProfileCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.createProfileCommand';

	protected $rules = [
		'profile_type' => 'required|in:' . ModelProfile::PROVIDER_PROFILE_TYPE . ',' . ModelProfile::CLIENT_PROFILE_TYPE,
		'first_name' => 'required|string',
		'last_name' => 'required|string',
		'is_api' => 'sometimes|boolean'
	];

	public function validHandle(ModelProfileService $svc)
	{
		$isApi = array_get($this->inputData, 'is_api', false);

		$profile = null;
		$result = new \stdClass();

		$user = Sentinel::check();

		$profile = $svc->createAndAssociateProfile($user->id, $this->inputData['profile_type'], $this->inputData['first_name'], $this->inputData['last_name']);

		if ($profile != null) {
			$svc->setCurrentProfileId($profile->id);

			if ($isApi) {
				$keys = User::getApiKeys();
				if ($keys != null) {
					$result->api_access_key = $keys->api_access_key;
					$result->api_secret_key = $keys->api_secret_key;
				}
			}

			$result->profile_id = $profile->id;
			$result->profile_type = $profile->type;
			$result->isSpecialUser = AuthorizationUtils::inRoles(['administrator']);
		}

		return $result;
	}
}

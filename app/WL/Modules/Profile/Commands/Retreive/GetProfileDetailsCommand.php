<?php namespace WL\Modules\Profile\Commands;

use Carbon\Carbon;
use WL\Modules\Payment\Facades\PaymentService;
use WL\Modules\Payment\Models\PaymentMethod;
use WL\Modules\Profile\Facades\ProfileReminderService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ProviderProfile;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Taxonomy\Facades\TagService;
use WL\Modules\Taxonomy\Facades\TaxonomyService;
use WL\Modules\Taxonomy\Models\Taxonomy;

class GetProfileDetailsCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getProfileDetailsCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
	];


	protected $metaFields = [
		'any' => [
			'first_name',
			'last_name',
			'gender',
			'country_id',
			'title',
			'birthday',
			'occupation',
		],
		'client' => [
			'accepted_terms_conditions',
			'phone'
		],
		'staff' => ['phone'],
		'provider' => [
			'about_self_background',
			'about_what_i_do',
			'about_services_benefit',
			'about_self_specialities',
			'business_name',
			'business_name_display',
			'accepted_terms_conditions',
			'is_blogger',
			'accepted_payment_methods',
			'requires_professional_approval'
		]
	];

	protected $tagFields = [
		'any' => [],
		'provider' => [],
		'client' => [],
		'staff' => []
	];

	protected $images = [
		'any' => [],
		'provider' => [],
		'client' => [],
		'staff' => []
	];

	private function loadProfileAttachments($fileFields, $profile, ModelProfileService $svc)
	{
		$data = [];
		foreach ($fileFields as $field => $size) {
			$data[$field] = $svc->loadUploadedFilesUrl($profile, $field, $size);
		}
		return $data;
	}

	private function convertDatesToCarbon(&$data, $fields)
	{
		foreach ($fields as $field) {
			if (isset($data[$field])) {
				$data[$field] = \Carbon::parse($data[$field]);
			}
		}
	}

	public function validHandle(ModelProfileService $svc)
	{
		$detailedProfile = new \stdClass();
		$success = false;
		$profile = $svc->getProfileById($this->inputData['profile_id']);

		if ($profile) {
			$this->tagFields['provider'] = array_merge($this->tagFields['provider'], ProviderProfile::$PROVIDER_ITEMS_TAGS);

			$data = array_merge(
				$svc->loadProfileMetaFields($profile->id, $this->metaFields['any']),
				$svc->loadProfileMetaFields($profile->id, $this->metaFields[$profile->type]),
				$profile->toArray()
			);

			$data['tags'] = array_merge(
				$svc->loadProfileTags($profile->id, $this->tagFields['any']),
				$svc->loadProfileTags($profile->id, $this->tagFields[$profile->type])
			);

			$maxTags = 4;
			$serviceTags = ProviderService::getServicesTags($profile->id);
			$entityType = \WL\Modules\Provider\Models\ProviderService::class;
			$condensedServiceTags = TagService::condenseTags($serviceTags, $entityType);

			if (count($condensedServiceTags) > $maxTags) {
				$condensedServiceTags = TagService::condenseTags($serviceTags, $entityType, 3, 2);

				if (count($condensedServiceTags) > $maxTags) {
					$condensedServiceTags = array_slice($condensedServiceTags, 0, $maxTags);
				}
			}

			if (!empty($condensedServiceTags)) {
				$data['tags']['services']['tags'] = $condensedServiceTags;
				$data['tags']['services']['tag_field_name'] = 'condensed_service_tags';
				$data['tags']['services']['tax_slug'] = '';
			}

			$data['attachments'] = $this->loadProfileAttachments($this->images[$profile->type], $profile->id, $svc);
			$data['jobs'] = $svc->jobService()->getProfileJobs($profile->id);
			$data['phone'] = $profile->phone;
			$data['educations'] = $svc->educationService()->getProfileEducations($profile->id);
			$data['avatar'] = $svc->loadUploadedFileUrl($profile, 'avatar', '250x250');
			$data['avatar_original'] = $svc->loadUploadedFileUrl($profile, 'avatar');
			$data['public_url'] = $profile->present()->publicUrl();
			$data['assets'] = $svc->assetsService()->getProfileAssets($profile->id);
			$data['professional_body'] = $svc->professionalBodyService()->loadProfileProfessionalBody($profile->id);
			$data['social_pages'] = $svc->loadSocialAccounts($profile->id);
			$data['unavailable_before_minutes'] = $svc->getProviderUnavailableTimeBeforeAppointment($profile->id);
			$data['reminders'] = ProfileReminderService::getProfileReminders($profile->id);
			$data['commission_free_period'] = ProfileService::getCommissionFreePeriodEndDate($profile->id);
			$data['commission_free_period_in_days'] = ProfileService::getCommissionFreePeriod($profile->id);
			$data['available_credit'] = PaymentService::getCredit($profile->id);
			$data['features'] = ProviderMembershipService::getProviderFeatures($profile->id);
			$data['accepted_payment_methods'] = array_get($data, 'accepted_payment_methods') ?? PaymentMethod::ONLY_IN_PERSON;

			$this->convertDatesToCarbon($data, ['birthday', 'created_at', 'updated_at', 'commission_free_period']);

			foreach ($data as $field => $value) {
				$detailedProfile->$field = $value;
			}

			if(isset($detailedProfile->birthday) && $detailedProfile->birthday instanceof Carbon){
				$detailedProfile->age = $detailedProfile->birthday->age;
			}
			$success = true;
		}

		return ['profile' => $detailedProfile, 'success' => $success];
	}
}

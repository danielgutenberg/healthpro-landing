<?php namespace WL\Modules\Profile\Commands\Retreive;


use WL\Modules\Profile\Commands\ValidatableCommand;
use WL\Modules\Profile\Facades\ProfileService;

class SearchProfessionalByNameCommand extends ValidatableCommand
{
	const IDENTIFIER = 'profile.searchProfessionalByNameCommand';
	private $limit = 15;
	private $maxCount = 100;

	protected $rules = [
		'query' => 'required',
		'limit' => 'sometimes|numeric'
	];

	protected $messages = [
		'required' => 'The :attribute field is required.',
		'numeric' => 'The :attribute must be numeric.',
	];

	public function validHandle()
	{
		$query = $this->get('query');
		$limit = $this->get('limit', $this->limit);

		$limit = $limit < $this->maxCount ? $limit : $this->maxCount;

		return ProfileService::searchForProviders($query, false, null, $limit);
	}
}

<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Profile\Models\ProviderProfile;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\Provider\Commands\Review\GetProviderReviewsCommand;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Rating\Models\Rating;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Taxonomy\Facades\TagService;

class GetProfileSummaryDetailsCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getProfileSummaryDetailsCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
	];


	protected $metaFields = [
		'country_id',
		'title',
		'birthday',
		'phone',
		'accepted_terms_conditions',
		'about_self_background',
		'about_what_i_do',
		'about_services_benefit',
		'about_self_specialities',
		'business_name',
		'accepted_terms_conditions',
		'business_name_display'
	];

	protected $tagFields = [

	];

	protected $images = [

	];

	private function loadProfileAttachments($fileFields, $profile, ModelProfileService $svc)
	{
		$data = [];
		foreach ($fileFields as $field => $size) {
			$data[$field] = $svc->loadUploadedFilesUrl($profile, $field, $size);
		}
		return $data;
	}

	private function convertDatesToCarbon(&$object, $fields)
	{
		foreach ($fields as $field) {
			if (isset($object->$field)) {
				$object->$field = \Carbon::parse($object->$field);
			}
		}
	}


	public function validHandle(ModelProfileService $svc)
	{
		$profile = $svc->getProfileById($this->inputData['profile_id'], true);
		$needFields = null;

		if (isset($this->inputData['fields']) && !empty($this->inputData['fields']))
			$needFields = explode(',', $this->inputData['fields']);


		if ($profile) {

			$loadMethods = [
				'services' => function (&$object, $id, $svc) {
					$maxTags = 4;
					$serviceTags = ProviderService::getServicesTags($id);
					$entityType = \WL\Modules\Provider\Models\ProviderService::class;
					$condensedServiceTags = TagService::condenseTags($serviceTags, $entityType);

					if (count($condensedServiceTags) > $maxTags) {
						$condensedServiceTags = TagService::condenseTags($serviceTags, $entityType, 3, 2);

						if (count($condensedServiceTags) > $maxTags) {
							$condensedServiceTags = array_slice($condensedServiceTags, 0, $maxTags);
						}
					}

					$object->services = [];
					if (!empty($condensedServiceTags)) {
						$object->services = implode(', ', array_map(function ($item) {
							return $item->name;
						}, $condensedServiceTags));
					}
				},

				'ratings' => function (&$object, $id, $svc) {
					$object->ratings = new \stdClass();
					$object->ratings->details = ReviewService::getEntityOverallRatings('provider_profile', $id, ProviderProfile::class, Rating::TYPE_STAR);
					$object->ratings->overall = ReviewService::getEntityOverallRating('provider_profile', $id, ProviderProfile::class, Rating::TYPE_STAR);
				},

				'reviews' => function(&$object, $id) {
					$object->reviews = $this->dispatch(new GetProviderReviewsCommand($id));
				},

				'tags' => function (&$object, $id, $svc) {
					$tagFields = array_merge($this->tagFields, ProviderProfile::$PROVIDER_ITEMS_TAGS);
					$object->tags = $svc->loadProfileTags($id, $tagFields);
				},

				'attachments' => function (&$object, $id, $svc) {
					$object->attachments = $this->loadProfileAttachments($this->images, $id, $svc);
				},

				'jobs' => function (&$object, $id, $svc) {
					$object->jobs = $svc->jobService()->getProfileJobs($id);
				},

				'educations' => function (&$object, $id, $svc) {
					$object->educations = $svc->educationService()->getProfileEducations($id);
				},

				'assets' => function (&$object, $id, $svc) {
					$object->assets = $svc->assetsService()->getProfileAssets($id);
				},


				'professional_body' => function (&$object, $id, $svc) {
					$object->professional_body = $svc->professionalBodyService()->loadProfileProfessionalBody($id);
				},

				'social_pages' => function (&$object, $id, $svc) {
					$object->social_pages = $svc->loadSocialAccounts($id);
				},

				'locations' => function (&$object, $id, $svc) {
					$object->locations = $svc->getLocations($id);
				},

			];

			$detailedProfile = $svc->loadProfileBasicDetails($profile->id);

			$data = [];

			if ($needFields != null) {
				if (in_array('all', $needFields)) {

					$data = $svc->loadProfileMetaFields($profile->id, $this->metaFields);

					foreach ($loadMethods as $name => $func) {
						$func($detailedProfile, $profile->id, $svc);
					}

				} else {
					$loadFields = array_intersect($this->metaFields, $needFields);
					$data = $svc->loadProfileMetaFields($profile->id, $loadFields);

					foreach ($loadMethods as $name => $func) {
						if (in_array($name, $needFields)) {
							$func($detailedProfile, $profile->id, $svc);
						}
					}

				}
			} else {
				$loadMethods['services']($detailedProfile, $profile->id, $svc);
				$loadMethods['ratings']($detailedProfile, $profile->id, $svc);
			}

			$data = array_merge($data, $svc->loadProfileMetaFields($profile->id, $this->metaFields));
			foreach ($data as $field => $value) {
				$detailedProfile->$field = $value;
			}

			$this->convertDatesToCarbon($detailedProfile, ['birthday']);

			return $detailedProfile;
		}
	}
}

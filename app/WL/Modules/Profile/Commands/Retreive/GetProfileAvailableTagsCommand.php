<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Profile\Models\ProviderProfile;

use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\Taxonomy\Facades\TaxonomyService;
use WL\Modules\Taxonomy\Services\TagServiceInterface;

class GetProfileAvailableTagsCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getProfileAvailableTagsCommand';

	protected $rules = null;

	protected $taxonomies = [

	];

	public function validHandle(TagServiceInterface $tagSvc)
	{
		$this->taxonomies = array_merge($this->taxonomies, ProviderProfile::$PROVIDER_ITEMS_TAGS);

		$tags = [];
		$tags['services'] = array(
			'tax_slug' => 'services',
			'tags'     => self::getProfileSessionServices(),
		);

		foreach ($this->taxonomies as $field => $taxSlug) {
			if( isset($tags[$taxSlug]) )
				continue;

			$tax = TaxonomyService::getTaxonomyBySlug($taxSlug);
			$tags[$field] = [
				'tax_slug' => $taxSlug,
				'tags' => $tagSvc->getTaxonomyTags($tax->id, true)
			];
		}

		return ['items' => $tags, 'success' => true];
	}

	protected function getProfileSessionServices() {
		return app(ModelProfileService::class)->getProfileSessionTags(session('profile_id'));
	}
}

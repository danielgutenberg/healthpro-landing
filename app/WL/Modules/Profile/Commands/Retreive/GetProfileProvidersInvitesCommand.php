<?php namespace WL\Modules\Profile\Commands;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use WL\Modules\Appointment\Commands\GetProviderAvailabilityCommand;
use WL\Modules\Appointment\Facades\AppointmentService;
use WL\Modules\Client\Facades\ClientService;
use WL\Modules\Location\Facades\LocationService;
use WL\Modules\PricePackage\Facade\PricePackageService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Provider\Models\ProviderServiceSession;
use WL\Security\Services\Exceptions\AuthorizationException;

class GetProfileProvidersInvitesCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getProfileProvidersCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
	];

    protected $clientId;

	/**
	 * @return mixed
	 */
	public function validHandle()
	{
        if(!$this->securityPolicy->canFetchClientInvites($this->inputData['profile_id'])) {
            throw new AuthorizationException('You do not have access to this information');
        }

		$this->clientId = $this->inputData['profile_id'];

	    $profile = ProfileService::getProfileById($this->clientId);

		if (!$profile || $profile->type != ModelProfile::CLIENT_PROFILE_TYPE)
			throw new ModelNotFoundException();

		$invites = ProviderOriginatedService::getClientProviderInvites($this->clientId);

        return $invites->map(function($invite){
            return $this->runSubCommand(new GetProfileSummaryDetailsCommand([
                'profile_id' => $invite->provider_id,
                'fields' => 'ratings,reviews,services'
            ]));
        });
	}

}

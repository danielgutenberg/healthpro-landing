<?php namespace WL\Modules\Profile\Commands;


use WL\Modules\Profile\Services\ModelProfileService;

class GetProfileAssetsCommand extends ProfileBaseWithUpdateCommand
{
	const IDENTIFIER = 'profile.getProfileAssetsCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
		'asset_type' => 'required'
	];

	public function validHandle(ModelProfileService $svc)
	{
		return $svc->assetsService()->getProfileAssetsByType(
			$this->inputData['profile_id'],
			$this->inputData['asset_type']
		);
	}
}

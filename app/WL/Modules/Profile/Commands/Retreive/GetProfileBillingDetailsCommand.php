<?php namespace WL\Modules\Profile\Commands;


use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Services\Exceptions\AuthorizationException;

class GetProfileBillingDetailsCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getProfileBillingDetailsCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
	];

	public function validHandle()
	{
		$profileId = $this->inputData['profile_id'];

		if(!$this->securityPolicy->canAccessProfileData($profileId)) {
		    throw new AuthorizationException('You are not profile Owner');
        }

		$meta = ['bill_to_name', 'bill_to_address',];

		return ProfileService::loadProfileMetaFields($profileId, $meta);
	}
}

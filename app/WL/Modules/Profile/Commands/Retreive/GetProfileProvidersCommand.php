<?php namespace WL\Modules\Profile\Commands;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use WL\Modules\Appointment\Commands\GetProviderAvailabilityCommand;
use WL\Modules\Appointment\Facades\AppointmentService;
use WL\Modules\Client\Facades\ClientService;
use WL\Modules\Location\Facades\LocationService;
use WL\Modules\PricePackage\Facade\PricePackageService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Provider\Models\ProviderServiceSession;
use WL\Security\Services\Exceptions\AuthorizationException;

class GetProfileProvidersCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getProfileProvidersCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
		'return_fields' => 'required|in:all,basic',
	];

	/**
	 * @return mixed
	 */
	public function validHandle()
	{
        if(!$this->securityPolicy->canFetchClientProviders($this->inputData['profile_id'])) {
            throw new AuthorizationException('You do not have access to this information');
        }

	    $profile = ProfileService::getProfileById($this->inputData['profile_id']);
		$returnFields = $this->inputData['return_fields'];

		if (!$profile || $profile->type != ModelProfile::CLIENT_PROFILE_TYPE)
			throw new ModelNotFoundException();

		$invites = ProviderOriginatedService::getClientProviders($profile->id);

		// load additional details for providers
        $map =  $invites->map(function($invite) use ($returnFields) {
			$details = $this->runSubCommand(new GetProfileSummaryDetailsCommand([
				'profile_id' => $invite->provider_id,
				'fields' => 'ratings,reviews,services'
			]));

			if ('all' === $returnFields) {
				$details->next_appointment = AppointmentService::getClientNextAppointment($invite->provider_id, $invite->client_id, Carbon::now());
				$details->last_appointment = AppointmentService::getClientLastAppointment($invite->provider_id, $invite->client_id, Carbon::now());

				$details->can_review = ClientService::providerNeedsReview($invite->client_id, $invite->provider);
				$details->services = ProviderService::getServices($invite->provider_id);

				$details->available_packages = PricePackageService::getClientPackagesForEntityType(
					$invite->provider_id,
					$invite->client_id,
					ProviderServiceSession::PACKAGE_ENTITY_TYPE
				);
			}
			return $details;
		});

		if ('all' === $returnFields) {
			return $map->sortBy(function ($professional) {
				if ($professional->next_appointment) {
					return $professional->next_appointment->from_utc;
				}
				return true;
			})->values();
		}

		return $map;
	}

}

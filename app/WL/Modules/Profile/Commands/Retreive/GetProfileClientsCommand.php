<?php namespace WL\Modules\Profile\Commands;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use WL\Modules\Appointment\Facades\AppointmentService;
use WL\Modules\Appointment\Models\ProviderAppointment;
use WL\Modules\Client\Facades\ClientService;
use WL\Modules\Profile\Commands\ClientMeta\ProviderClientMetaGetCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Models\ProviderOriginated;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Security\Services\Exceptions\AuthorizationException;

class GetProfileClientsCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getProfileClientsCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
		'return_fields' => 'required|in:all,basic',
	];

	protected $fields = [
		'all' => 'phone,title',
		'basic' => 'title',
	];

	/**
	 * @return mixed
	 */
	public function validHandle()
	{
	    if(!$this->securityPolicy->canFetchProviderClients($this->inputData['profile_id'])) {
            throw new AuthorizationException('You do not have access to this information');
        }

		$profile = ProfileService::getProfileById($this->inputData['profile_id']);
		$returnFields = $this->inputData['return_fields'];

		if (!$profile || $profile->type != ModelProfile::PROVIDER_PROFILE_TYPE)
			throw new ModelNotFoundException();

		$invites = ProviderOriginatedService::getProviderClients($profile->id);

		return $invites->map(function($invite) use ($profile, $returnFields) {

			$details = $this->runSubCommand(new GetProfileSummaryDetailsCommand([
				'profile_id' => $invite->client_id,
				'fields' => $this->fields[$returnFields],
			]));
			$details->providerId = $profile->id;

			// load additional data if needed
			if ('all' === $returnFields) {

				$details->meta = $this->runSubCommand(new ProviderClientMetaGetCommand([
					'provider_id' => $profile->id,
					'client_id' => $invite->client_id,
					'fields' => [],
				]));
				$details->last_appointment = AppointmentService::getClientLastAppointment($invite->provider_id, $invite->client_id, Carbon::now());
				$details->next_appointment = AppointmentService::getClientNextAppointment($invite->provider_id, $invite->client_id);

				$details->visits = count(AppointmentService::getAppointmentsByProviderAndClient(
					$invite->provider_id, $invite->client_id, [ProviderAppointment::STATUS_CONFIRMED], ProviderAppointment::TYPE_PAST));
			}

			return $details;
		});
	}

}

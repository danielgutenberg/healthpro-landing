<?php namespace WL\Modules\Profile\Commands;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Modules\Appointment\Facades\AppointmentService;
use WL\Modules\Client\Facades\ClientService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\Provider\Commands\LoadProviderServicesCommand;
use WL\Modules\Provider\Commands\Review\GetProviderReviewsCommand;
use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Modules\Provider\ValueObjects\ProviderFeatures;
use WL\Modules\Rating\Models\Rating;
use WL\Modules\Review\Commands\GetReviewHelpfulRatingCommand;
use WL\Modules\Review\Facade\ReviewService;
use WL\Schedule\Facades\ScheduleService;
use WL\Schedule\ScheduleAvailability;

class GetProviderFullDetailsCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getProviderFullDetailsCommand';

	protected $rules = [
		'profile_id' => 'required',
	];

	public function beforeHandle()
	{
		if (!isset($this->inputData['profile_id'])) {
			throw new ModelNotFoundException();
		}
	}

	public function validHandle(ModelProfileService $svc)
	{
		$profile = ProfileService::getProfileByIdOrSlug($this->inputData['profile_id']);
		if (!$profile) {
			throw new ModelNotFoundException();
		}
		$currentProfile = ProfileService::getCurrentProfile();

		$data = $this->dispatch(new GetProviderDetailsCommand([
			'profile_id' => $profile->id
		]));

		$data['reviews'] = $this->dispatch(new GetProviderReviewsCommand($profile->id));
		$data['services'] = $this->dispatch(new LoadProviderServicesCommand($profile->id));
		$locationIds = [];
		$data['services']->each(function($service) use (&$locationIds) {
			$service->locations->map(function($location) use (&$locationIds) {
				$locationIds[] = $location->id;
			});
		});
		$data['locations'] = $svc->getLocations($profile->id)->filter(function($location) use ($locationIds) {
			return in_array($location->id, array_unique($locationIds));
		});
		$data['currentProfile'] = $currentProfile;
		$data['overallRating'] = ReviewService::getEntityOverallRating('provider_profile', $profile->id, $profile->typeInstance()->getMorphClass(), Rating::TYPE_STAR);
		$data['overallRatings'] = ReviewService::getEntityOverallRatings('provider_profile', $profile->id, $profile->typeInstance()->getMorphClass(), Rating::TYPE_STAR);

        $data['hasGiftCertificates'] = ProviderMembershipService::hasFeature($profile->id, ProviderFeatures::GIFT_CERTIFICATE);
        if($data['hasGiftCertificates']) {
            $data['hasGiftCertificates'] = $data['services']->filter(function ($service) {
                    return $service->gift_certificates;
                })->count() > 0;
        }

		$data['commentsCount'] = 0;
		foreach ($data['reviews'] as &$review) {
			$review->helpfulRating = $this->dispatch(new GetReviewHelpfulRatingCommand($review->id));
			$review->profileName = $review->profile->getMeta('first_name').' '.$review->profile->getMeta('last_name');
			$review->profileAvatar = ProfileService::loadUploadedFileUrl($review->profile, 'avatar', '90x90');
			$review->daysAgo = $review->updated_at->diffInDays();

			$data['commentsCount'] += count($review->comments);
		}

		$data['shouldReview'] = false;
		if (is_object($currentProfile) && $currentProfile->type == ModelProfile::CLIENT_PROFILE_TYPE && ClientService::providerNeedsReview($currentProfile->id, $profile)) {
			$data['shouldReview'] = true;
			$data['clientLastAppointment'] = AppointmentService::getClientLastAppointment($profile->id, $currentProfile->id, Carbon::now());
		}

		$data['locations']->each(function($location) use ($profile) {
			$location->timezone = $location->timezone(true);
			$location->availabilities = ScheduleService::getProviderAvailability($profile->id, $location->id, ScheduleAvailability::TYPE_APPOINTMENT)->sortBy('day');
		});

		return $data;
	}
}

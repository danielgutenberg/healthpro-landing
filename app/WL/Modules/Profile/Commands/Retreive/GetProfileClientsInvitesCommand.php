<?php namespace WL\Modules\Profile\Commands;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Security\Services\Exceptions\AuthorizationException;

class GetProfileClientsInvitesCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getProfileClientsInvitesCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
	];

	/**
	 * @return mixed
	 */
	public function validHandle()
	{
        if(!$this->securityPolicy->canFetchProviderClients($this->inputData['profile_id'])) {
            throw new AuthorizationException('You do not have access to this information');
        }

		$profile = ProfileService::getProfileById($this->inputData['profile_id']);

		if (!$profile || $profile->type != ModelProfile::PROVIDER_PROFILE_TYPE)
			throw new ModelNotFoundException();

		$invites = ProviderOriginatedService::getProviderClientsInvites($profile->id);

		// load clients details
		return $invites->map(function($invite) {
			$data = ProviderOriginatedService::collectInviteData($invite);
			$data->profile = $this->runSubCommand(new GetProfileSummaryDetailsCommand([
				'profile_id' => $invite->client_id,
				'fields' => 'title',
			]));

			return $data;
		});
	}

}

<?php namespace WL\Modules\Profile\Commands;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Modules\Client\Commands\Review\GetClientReviewsCommand;
use WL\Modules\Profile\Facades\ProfileReminderService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\ProfilesMeta\ProviderClientsMeta;
use WL\Modules\Review\Commands\GetReviewHelpfulRatingCommand;

class GetClientFullDetailsCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getClientFullDetailsCommand';

	protected $rules = [
		'profile_id' => 'required',
	];

	public function beforeHandle()
	{
		if (!isset($this->inputData['profile_id'])) {
			throw new ModelNotFoundException();
		}
	}

	public function validHandle()
	{
		$profile = ProfileService::getProfileById($this->inputData['profile_id']);
		if (!$profile) {
			throw new ModelNotFoundException();
		}
		$currentProfile = ProfileService::getCurrentProfile();

		$data = $this->dispatch(new GetClientDetailsCommand([
			'profile_id' => $profile->id
		]));

		$data['currentProfile'] = $currentProfile;

		$data['canMessage'] = false;
		if ($currentProfile && $currentProfile->type == ModelProfile::PROVIDER_PROFILE_TYPE) {
			$data['canMessage'] = ProfileService::isProfessionalsClient($currentProfile->id, $profile->id);

			$meta = ProviderOriginatedService::getMetaData($currentProfile->id, $profile->id, [
				ProviderClientsMeta::KEY_RANGE_EXEMPTION,
				ProviderClientsMeta::INTRO_SESSION_EXEMPTION,
			]);

			$data['rangeExemption'] = $meta->{ProviderClientsMeta::KEY_RANGE_EXEMPTION};
			$data['introSessionExcemption'] = $meta->{ProviderClientsMeta::INTRO_SESSION_EXEMPTION};
		}

		$data['reviews'] = $this->dispatch(new GetClientReviewsCommand($profile->id));
		$data['commentsCount'] = 0;
		foreach ($data['reviews'] as &$review) {
			$review->helpfulRating = $this->dispatch(new GetReviewHelpfulRatingCommand($review->id));
			$review->providerName = $review->elm->getMeta('first_name').' '.$review->elm->getMeta('last_name');
			$review->providerAvatar = ProfileService::loadUploadedFileUrl($review->elm, 'avatar', '90x90');
			$review->daysAgo = $review->updated_at->diffInDays();
			$data['commentsCount'] += count($review->comments);
		}

		$data['reminders'] = ProfileReminderService::getProfileReminders($profile->id);

		return $data;
	}
}

<?php namespace WL\Modules\Profile\Commands;


use WL\Modules\Payment\Facades\PaymentService;
use WL\Modules\Profile\Exceptions\WrongProfileTypeException;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Services\ModelProfileService;

class GetProviderProgressCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getProviderProgressCommand';

	protected $rules = [
		'profile_id' => 'required',
	];

	public function validHandle(ModelProfileService $svc)
	{
		return ProviderService::getProgress($this->inputData['profile_id']);
	}
}

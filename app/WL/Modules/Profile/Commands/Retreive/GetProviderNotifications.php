<?php namespace WL\Modules\Profile\Commands;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Modules\Profile\ProfilesMeta\EmailNotificationsProvider;
use WL\Modules\Profile\ProfilesMeta\SmsNotificationsProvider;

class GetProviderNotifications extends ValidatableCommand {

	const IDENTIFIER = 'profile.getProviderNotifications';

	/**
	 * Get provider notification meta ..
	 *
	 * @return array
	 */
	public function validHandle() {
		if(! isset($this->inputData['profile']))
			throw new ModelNotFoundException(_('Invalid profile'));

		$profile = $this->inputData['profile'];

		return [
			'email' => (new EmailNotificationsProvider($profile))->provide(),
			'sms'   => (new SmsNotificationsProvider($profile))->provide(),
		];
	}
}

<?php namespace WL\Modules\Profile\Commands;


use WL\Modules\Profile\Services\ModelProfileService;

class GetProfileAssetCommand extends ProfileBaseWithUpdateCommand
{
	const IDENTIFIER = 'profile.getProfileAssetCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
		'asset_id' => 'required|integer'
	];

	public function validHandle(ModelProfileService $svc)
	{
		return $svc->assetsService()->getProfileAssetById(
			$this->inputData['profile_id'],
			$this->inputData['asset_id']
		);
	}
}

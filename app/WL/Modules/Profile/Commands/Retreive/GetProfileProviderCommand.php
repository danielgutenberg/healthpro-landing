<?php namespace WL\Modules\Profile\Commands;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use WL\Modules\Appointment\Commands\GetProviderAvailabilityCommand;
use WL\Modules\Appointment\Facades\AppointmentService;
use WL\Modules\Client\Facades\ClientService;
use WL\Modules\PricePackage\Facade\PricePackageService;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Provider\Models\ProviderServiceSession;
use WL\Security\Services\Exceptions\AuthorizationException;

class GetProfileProviderCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getProfileProviderCommand';

	protected $rules = [
		'profile_id' => 'required|integer',
		'provider_id' => 'required|integer',
		'return_fields' => 'required|in:all,basic',
	];

	/**
	 * @return mixed
	 */
	public function validHandle()
	{
        if(!$this->securityPolicy->canFetchClientProviders($this->inputData['profile_id'])) {
            throw new AuthorizationException('You do not have access to this information');
        }

		$profile = ProfileService::getProfileById($this->inputData['profile_id']);
		$providerId = $this->inputData['provider_id'];
		$returnFields = $this->inputData['return_fields'];
		$dateTime = Carbon::now();

		if (!$profile || $profile->type != ModelProfile::CLIENT_PROFILE_TYPE)
			throw new ModelNotFoundException();

		$provider = ProfileService::getProfileById($providerId);

		if (!$provider || $provider->type != ModelProfile::PROVIDER_PROFILE_TYPE)
			throw new ModelNotFoundException();

		$providerDetails = $this->runSubCommand(new GetProfileSummaryDetailsCommand([
			'profile_id' => $provider->id,
			'fields' => 'ratings,reviews,services'
		]));

		if ('all' === $returnFields) {
			$providerDetails->next_appointment = AppointmentService::getClientNextAppointment($provider->id, $profile->id, $dateTime);
			$providerDetails->last_appointment = AppointmentService::getClientLastAppointment($provider->id, $profile->id, $dateTime);

			$providerDetails->can_review = ClientService::providerNeedsReview($profile->id, $provider);
			$providerDetails->services = ProviderService::getServices($provider->id);

			$providerDetails->available_packages = PricePackageService::getClientPackagesForEntityType(
				$provider->id,
				$profile->id,
				ProviderServiceSession::PACKAGE_ENTITY_TYPE
			);
		}

		return $providerDetails;
	}

}

<?php namespace WL\Modules\Profile\Commands;


use WL\Modules\Profile\Exceptions\WrongProfileTypeException;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Services\ModelProfileService;

class GetProviderDetailsCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.getProviderDetailsCommand';

	protected $rules = [
		'profile_id' => 'required',
	];

	public function validHandle(ModelProfileService $svc)
	{
		$profile = $svc->getProfileById($this->inputData['profile_id']);

		if (!$profile || $profile->type != ModelProfile::PROVIDER_PROFILE_TYPE)
			throw new WrongProfileTypeException();

		$this->inputData['profile_id'] = $profile->id;

		return $this->dispatch(new GetProfileDetailsCommand($this->inputData));
	}
}

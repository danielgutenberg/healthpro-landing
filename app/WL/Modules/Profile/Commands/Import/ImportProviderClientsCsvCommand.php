<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Profile\Exceptions\WrongProfileTypeException;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\Profile\Services\ProfileFileImporterInterface;
use WL\Security\Services\Exceptions\AuthorizationException;

class ImportProviderClientsCsvCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.importProviderClientsCsvCommand';

	protected $rules = [
		'providerId' => 'required|integer',
		'file' => 'required',
		'sendEmails' => 'sometimes|boolean',
		'customEmailBody' => 'sometimes|string'
	];

	public function validHandle(ProfileFileImporterInterface $importService, ModelProfileService $profileService)
	{
        if(!$this->securityPolicy->canImportClients($this->get('providerId'))) {
            throw new AuthorizationException('You do not have access to this action');
        }

		$currentProvider = $profileService->getProfileById($this->get('providerId'));

		if ($currentProvider->type != ModelProfile::PROVIDER_PROFILE_TYPE) {
			throw new WrongProfileTypeException('importer can be only provider');
		}

		return $importService->importFile(
			$currentProvider->id,
			$this->get('file'),
			$this->get('sendEmails', true),
			$this->get('customEmailBody', null),
			$this->get('couponDiscount', null)
		);
	}
}

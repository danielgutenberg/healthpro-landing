<?php namespace WL\Modules\Profile\Commands;

use WL\Modules\Client\Models\ClientImportRecord;
use WL\Modules\Profile\Exceptions\WrongProfileTypeException;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\Profile\Services\ProfileImporterInterface;
use WL\Security\Services\Exceptions\AuthorizationException;

class ImportProviderClientCommand extends ProfileBaseCommand
{
	const IDENTIFIER = 'profile.importProviderClientCommand';

	protected $rules = [
		'provider_id' => 'required|integer',
		'client_id' => 'required_without_all:email,first_name,last_name,phone',
		'email' => 'required_without:client_id|email',
		'first_name' => 'required_without:client_id',
		'last_name' => 'required_without:client_id',
		'phone' => 'sometimes',
		'sendEmails' => 'sometimes|boolean',
		'customEmailBody' => 'sometimes|string',
	];

	public function validHandle(ProfileImporterInterface $importService, ModelProfileService $profileService)
	{
        if(!$this->securityPolicy->canImportClients($this->get('provider_id'))) {
            throw new AuthorizationException('You do not have access to this action');
        }

		$currentProvider = $profileService->getProfileById($this->get('provider_id'));

		if ($currentProvider->type != ModelProfile::PROVIDER_PROFILE_TYPE) {
			throw new WrongProfileTypeException('importer can be only provider');
		}


		if ($this->get('client_id')) {

			$client = $profileService->getProfileById($this->get('client_id'));
			if ($client->type != ModelProfile::CLIENT_PROFILE_TYPE) {
				throw new WrongProfileTypeException('you can invite only clients');
			}

			return $importService->inviteClient(
				$client->id,
				$currentProvider->id,
				$this->get('sendEmails', true),
				$this->get('customEmailBody', null),
				$this->get('couponDiscount', null)
			);
		}

		$clientImport = new ClientImportRecord(
			$this->get('email'),
			$this->get('first_name'),
			$this->get('last_name'),
			$this->get('phone')
		);

		return $importService->importClient(
			$currentProvider->id,
			$clientImport,
			$this->get('sendEmails', true),
			$this->get('customEmailBody', null),
			$this->get('couponDiscount', null)
		);
	}
}

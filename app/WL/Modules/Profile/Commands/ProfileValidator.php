<?php namespace WL\Modules\Profile\Commands;

use Carbon\Carbon;
use Illuminate\Validation\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use WL\Modules\Profile\Facades\ProfileService;

class ProfileValidator extends Validator
{

	private function isArrayHasKeys($dataArray, $keys, $attribute = null, array $items = null, $msg = 'Invalid')
	{
		$ret = true;
		foreach ($keys as $key) {
			if (!isset($dataArray[$key]) || empty($dataArray[$key])) {
				if ($attribute != null && $items != null) {
					$tItems = array_merge($items, [$key]);
					$this->addInvalidMessage($attribute, $tItems, $msg);
				}
				$ret = false;
			}
		}

		return $ret;
	}

	private function addInvalidMessage($attribute, array $items, $msg = 'Invalid')
	{
		$this->messages->add($attribute . '-' . implode('-', $items), _($msg));
	}

	public function validateTags($attribute, $value, $parameters)
	{
		$valid = is_array($value);

		foreach ($value as $key => $values) {
			if (!is_array($values)) {
				$this->addInvalidMessage($attribute, [$key]);
				$valid = false;
			}
		}

		return $valid;
	}

	public function validateAsset($attribute, $value, $parameters)
	{

		$regexTypeValidators = [
			'video' => "/https?:\\/\\/(www\\.)?(youtube\\.com\\/watch\\?&?v=|youtu\\.be\\/)[^\\s]*/i",
		];

		$assetTypes = [
			'video',
			'podcast',
			'article',
			'photo',
			'certificate'
		];

		$allValid = true;
		if ($allValid) {
			$fields = $value;
			$valid = isset($fields['type']) && in_array($fields['type'], $assetTypes) && (array_key_exists('file', $fields) || array_key_exists('url', $fields));


			if ($valid) {

				if (array_key_exists('file', $fields)) {

					$valid = $this->isArrayHasKeys($fields, ['file'], $attribute, ['asset']);
					if ($valid) {
						$valid = $fields['file'] instanceof UploadedFile;
					}
				}

				if (array_key_exists('url', $fields)) {
					$valid = $this->isArrayHasKeys($fields, ['title', 'url'], $attribute, ['asset']);


					if ($valid && isset($regexTypeValidators[$fields['type']])) {

						$valid = preg_match($regexTypeValidators[$fields['type']], $fields['url']) == 1;
						if (!$valid)
							$this->addInvalidMessage($attribute, ['asset', 'url']);
					}
				}

				if (!$valid) {
					$this->addInvalidMessage($attribute, ['asset']);
				}
			}
			$allValid = $allValid && $valid;


		}
		return $allValid;
	}


	public function validateUploadedfile($attribute, $value, $parameters)
	{
		return isset($value) && $value != null && $value instanceof UploadedFile;
	}

	public function validateBirthday($attribute, $value, $parameters)
	{
		$allValid = count($value) == 3;

		if ($allValid) {
			try {
				if($value['year'] . $value['month'] . $value['day']){
					$date = Carbon::create($value['year'], $value['month'], $value['day']);
					$allValid = $value['day'] == $date->day && $value['month'] == $date->month && $value['year'] == $date->year;
				}
			} catch (\Exception $e) {
				$allValid = false;
			}
		}

		return $allValid;
	}

	public function validateProfbody($attribute, $value, $parameters)
	{
		$allValid = is_array($value);

		if ($allValid && !empty($value)) {
			foreach ($value as $i => $body) {
				$valid = isset($body['title']);
				if (!$valid)
					$this->addInvalidMessage($attribute, [$i, 'title']);
				try {
					Carbon::createFromDate($body['expiry_date']['year'], $body['expiry_date']['month']);
				} catch (\Exception $e) {
					$valid = false;
					$this->addInvalidMessage($attribute, [$i, 'expiry_date']);
				}

				if (!$valid) {
					$this->addInvalidMessage($attribute, [$i]);
				}
				$allValid = $allValid && $valid;
			}

		}
		return $allValid;
	}

	public function validateSocial($attribute, $value, $parameters)
	{
		$cleanSocial = ProfileService::getSocialNetworks();

		if (is_array($value)) {
			foreach ($value as $key => $data) {
				if (!empty($data) && !preg_match($cleanSocial[$key]['regex'], $data)) {
					$this->addInvalidMessage($attribute, [$key], 'Invalid Url');
				}
			}
		}

		return true;
	}

	public function validateEducation($attribute, $value)
	{
		$isValid = false;
		$index = 0;
		foreach ($value as $education) {
			$isValid = ($this->isArrayHasKeys($education, ['education_id']) || $this->isArrayHasKeys($education, ['education_name']));

			if(!empty($education['education_id'])) $isValid = is_numeric($education['education_id']);

			if (!$isValid)
				$this->addInvalidMessage($attribute, [$index], 'Missing education information');

//			if (!$this->isArrayHasKeys($education, ['from'])) {
//				$isValid = false;
//				$this->addInvalidMessage($attribute, [$index, 'from'], 'Missing start date');
//			}

			if (!$this->isArrayHasKeys($education, ['degree']) || strlen(trim($education['degree'])) == 0) {
				$isValid = false;
				$this->addInvalidMessage($attribute, [$index, 'degree'], 'Missing degree');
			}

			if ($isValid) {
				if(array_key_exists('to', $education) && !!$education['to']['month'] && !!$education['to']['year'] &&
					array_key_exists('from', $education) && !!$education['from']['month'] && !!$education['from']['year']) {
					try {
						$from = Carbon::createFromDate($education['from']['year'], $education['from']['month']);
					} catch (\Exception $ex) {
						$this->addInvalidMessage($attribute, [$index, 'from']);
						return false;
					}

					try {
						$to   = Carbon::createFromDate($education['to']['year'] , $education['to']['month']);
					} catch (\Exception $ex) {
						$this->addInvalidMessage($attribute, [$index, 'from']);
						return false;
					}

					if( $from->getTimestamp() > $to->getTimestamp() ) {
						$isValid = false;
						$this->addInvalidMessage($attribute, [$index], 'Validation dates error');
					}
				}
			}

			++$index;
		}

		return $isValid;
	}

	public function validateWork($attribute, $value) {
		$isValid = false;
		$index = 0;
		foreach ($value as $work) {
			$isValid = $this->isArrayHasKeys($work, ['company_name'], $attribute);
			if (!$isValid)
				$this->addInvalidMessage($attribute, [$index, 'company_name'], 'Missing company information');

			if (!$this->isArrayHasKeys($work, ['from'])) {
				$isValid = false;
				$this->addInvalidMessage($attribute, [$index, 'from'], 'Missing start date');
			}

			if (!$this->isArrayHasKeys($work, ['job_name']) || strlen(trim($work['job_name'])) == 0) {
				$isValid = false;
				$this->addInvalidMessage($attribute, [$index, 'job_name'], 'Missing job title');
			}

			if ($isValid) {
				if(array_key_exists('to', $work) && !!$work['to']['month'] && !!$work['to']['year']) {
					try {
						$from = Carbon::createFromDate($work['from']['year'], $work['from']['month']);
					} catch (\Exception $ex) {
						$this->addInvalidMessage($attribute, [$index, 'from']);
						return false;
					}

					try {
						$to   = Carbon::createFromDate($work['to']['year'] , $work['to']['month']);
					} catch (\Exception $ex) {
						$this->addInvalidMessage($attribute, [$index, 'from']);
						return false;
					}

					if( $from->getTimestamp() > $to->getTimestamp() ) {
						$isValid = false;
						$this->addInvalidMessage($attribute, [$index], 'Validation dates error');
					}
				}
			}

			++$index;
		}

		return $isValid;
	}

	public function validateReminders($attribute, $value, $paramenters)
	{
		$isValid  = true;

		if(!is_array($value))
			return false;

		foreach ($value as $reminder) {
			$isValid &= $this->isArrayHasKeys($reminder, ['id','value']);
			$isValid &= is_numeric($reminder['value']);
		}

		return $isValid;
	}
}

<?php namespace WL\Modules\Profile\Commands;

use Illuminate\Support\Facades\DB;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Security\Commands\AuthorizableCommand;

class UpgradeOldProfessionalCommand extends AuthorizableCommand {

	const IDENTIFIER = 'profile.upgradeOldProfessionals';

	/**
	 * Perform user activation ..
	 * @param ModelProfileService $profileService
	 * @return bool
	 */
	public function handle(ModelProfileService $profileService)
	{
		$profile = ProfileService::getCurrentProfile();

		if (!$profile) {
			return false;
		}

		if (!ProfileService::loadProfileMetaField($profile->id, 'old_returning_professional')) {
			return false;
		}

		DB::beginTransaction();

		$plan = ProviderMembershipService::startProviderPlan($profile->id, 6);
		$planActivated = ProviderMembershipService::activatePlan($plan);
		$statusChanged = ProfileService::storeProfileMetaField($profile->id, 'old_returning_professional', false, false);

		if ($planActivated && $statusChanged) {
			DB::commit();
			return true;
		}

		DB::rollBack();
		return false;
	}
}

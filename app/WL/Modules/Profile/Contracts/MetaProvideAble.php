<?php namespace WL\Modules\Profile\Contracts;

interface MetaProvideAble {

	/**
	 * @param array $merged
	 * @return array
	 */
	public function provide(array $merged = array());
}

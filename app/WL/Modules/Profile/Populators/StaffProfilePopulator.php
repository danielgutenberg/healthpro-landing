<?php namespace WL\Modules\Profile\Populators;

use WL\Modules\Profile\Models\StaffProfile;

interface StaffProfilePopulator {

	/**
	 * @param StaffProfile $profile
	 *
	 * @return $this
	 */
	public function setModel(StaffProfile $profile);
}

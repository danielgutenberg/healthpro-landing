<?php namespace WL\Modules\Profile\Populators;

use WL\Modules\Profile\Models\StaffProfile;
use WL\Populators\FormerPopulator;

class FormerStaffProfilePopulator extends FormerPopulator implements StaffProfilePopulator {

	/**
	 * We accept only the Profile model here
	 *
	 * @param StaffProfile $model
	 * @return $this
	 */
	public function setModel(StaffProfile $model) {
		$this->model = $model;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getFields() {
		$fields         = $this->model->toArray();
		$fields['meta'] = $this->model->getAllMeta();

		return $fields;
	}
}

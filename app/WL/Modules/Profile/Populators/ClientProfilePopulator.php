<?php namespace WL\Modules\Profile\Populators;

use WL\Modules\Profile\Models\ClientProfile;

interface ClientProfilePopulator {

	/**
	 * @param ClientProfile $profile
	 *
	 * @return $this
	 */
	public function setModel(ClientProfile $profile);
}

<?php namespace WL\Modules\Profile\Populators;

use WL\Modules\Profile\Models\ProviderProfile;

interface ProviderProfilePopulator {

	/**
	 * @param ProviderProfile $profile
	 *
	 * @return $this
	 */
	public function setModel(ProviderProfile $profile);
}

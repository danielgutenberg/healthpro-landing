<?php namespace WL\Modules\Profile\Populators;

use WL\Modules\Profile\Models\ProviderProfile;
use WL\Populators\FormerPopulator;

class FormerProviderProfilePopulator extends FormerPopulator implements ProviderProfilePopulator {

	/**
	 * We accept only the Profile model here
	 *
	 * @param ProviderProfile $model
	 * @return $this
	 */
	public function setModel(ProviderProfile $model) {
		$this->model = $model;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getFields() {
		$fields         = $this->model->toArray();
		$fields['meta'] = $this->model->getAllMeta();

		return $fields;
	}
}

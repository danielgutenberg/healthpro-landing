<?php namespace WL\Modules\Profile\Populators;

use WL\Modules\Profile\Models\ClientProfile;
use WL\Populators\FormerPopulator;

class FormerClientProfilePopulator extends FormerPopulator implements ClientProfilePopulator {

	/**
	 * We accept only the Profile model here
	 *
	 * @param ClientProfile $model
	 * @return $this
	 */
	public function setModel(ClientProfile $model) {
		$this->model = $model;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getFields() {
		$fields         = $this->model->toArray();
		$fields['meta'] = $this->model->getAllMeta();

		return $fields;
	}
}

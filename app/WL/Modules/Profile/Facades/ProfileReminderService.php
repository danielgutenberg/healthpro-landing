<?php namespace WL\Modules\Profile\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Profile\Services\ProfileReminderServiceInterface;


class ProfileReminderService extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return ProfileReminderServiceInterface::class;
	}
}

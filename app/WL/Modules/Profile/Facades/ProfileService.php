<?php namespace WL\Modules\Profile\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Profile\Services\ModelProfileService;

class ProfileService extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return ModelProfileService::class;
	}
}

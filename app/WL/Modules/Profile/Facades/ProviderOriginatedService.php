<?php namespace WL\Modules\Profile\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Profile\Services\ProviderOriginatedServiceInterface;

class ProviderOriginatedService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return ProviderOriginatedServiceInterface::class;
	}
}

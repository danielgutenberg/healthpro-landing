<?php namespace WL\Modules\Profile\SecurityPolicies;

use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Facades\AuthorizationUtils;
use WL\Security\SecurityPolicies\BaseAccessPolicy;
use WL\Security\Services\Exceptions\AuthorizationException;

/**
 * Class ProfileAccessPolicy
 * @package WL\Modules\Profile
 */
class ProfileAccessPolicy extends BaseAccessPolicy
{


	/**
	 * Can show verified field?
	 * @param $profileUserId
	 * @return bool
	 */
	public function canShowVerifiedField($profileUserId)
	{
		$userId = null;
		$user = AuthorizationUtils::loggedInUser();

		if ($user)
			$userId = $user->id;

		$isOwner = $this->isOwner($userId, $profileUserId);
		$isSpecialUser = AuthorizationUtils::hasAccess(['profile.load.other']);
		return $isOwner || $isSpecialUser;
	}


	/**
	 * Can user switch profile
	 * @param $profileId number
	 * @return boolean
	 */
	public function canSwitchProfileTo($profileId)
	{
		if (ProfileService::getUserProfiles(AuthorizationUtils::loggedInUser()->id, $profileId))
			return true;
		else
			return false;
	}

	/**
	 * @param int $profileId
	 * @throws AuthorizationException
	 */
	public function canUpdateProfile($profileId)
	{
		if (!$this->isCurrentProfile($profileId) && !$this->canUpdateOther()) {
			throw new AuthorizationException('You are not profile Owner');
		}
	}

	/**
	 * @param $profileId
	 * @throws int AuthorizationException
	 */
	public function canAccessProfileData($profileId)
	{
		return $this->isCurrentProfile($profileId) || $this->canAccessOtherData();
	}

    public function canFetchProviderClients($profileId)
    {
        return $this->canAccessProfileData($profileId);
	}

	public function isCurrentProfile($profileId)
	{
		return $this->isOwner($profileId, ProfileService::getCurrentProfileId());
	}

    public function canImportClients($profileId)
    {
        return $this->isCurrentProfile($profileId);
	}

    public function canFetchClientMeta($profileId)
    {
        return $this->isCurrentProfile($profileId);
	}

    public function canEditClientMeta($profileId)
    {
        return $this->isCurrentProfile($profileId);
	}

    public function canFetchClientProviders($profileId)
    {
        return $this->canAccessProfileData($profileId);
    }

    public function canFetchClientInvites($profileId)
    {
        return $this->canAccessProfileData($profileId);
    }

    public function canRespondClientInvites($profileId)
    {
        return $this->isCurrentProfile($profileId);
    }

	protected function canUpdateOther()
	{
		return AuthorizationUtils::hasAccess(['profile.update.other']);
	}

	protected function canAccessOtherData()
	{
		return AuthorizationUtils::hasAccess(['access.all.profiles']);
	}
}

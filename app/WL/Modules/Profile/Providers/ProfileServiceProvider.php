<?php namespace WL\Modules\Profile\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use WL\Modules\Profile\Models\ClientProfile;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Models\ProviderProfile;
use WL\Modules\Profile\Observers\ClientProfileObserver;
use WL\Modules\Profile\Observers\ProviderProfileObserver;
use WL\Modules\Profile\Populators\ClientProfilePopulator;
use WL\Modules\Profile\Populators\FormerClientProfilePopulator;
use WL\Modules\Profile\Populators\FormerProviderProfilePopulator;
use WL\Modules\Profile\Populators\ProviderProfilePopulator;
use WL\Modules\Profile\Populators\FormerStaffProfilePopulator;
use WL\Modules\Profile\Populators\StaffProfilePopulator;
use WL\Modules\Profile\Repositories\DbProviderOriginatedRepository;
use WL\Modules\Profile\Repositories\DbStaffProfileRepository;
use WL\Modules\Profile\Repositories\ProfileReminderRepository;
use WL\Modules\Profile\Repositories\ProfileReminderSentRepository;
use WL\Modules\Profile\Repositories\StaffProfileRepository;
use WL\Modules\Profile\Services\BasicProfileImporterService;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\Profile\Services\ModelProfileServiceImpl;
use WL\Modules\Profile\Repositories\DbModelProfileRepository;
use WL\Modules\Profile\Repositories\ModelProfileRepository;
use WL\Modules\Profile\Repositories\ClientProfileRepository;
use WL\Modules\Profile\Repositories\DbClientProfileRepository;
use WL\Modules\Profile\Repositories\ProviderProfileRepository;
use WL\Modules\Profile\Repositories\DbProviderProfileRepository;
use WL\Modules\Profile\Repositories\ProfileAssetsInterface;
use WL\Modules\Profile\Repositories\DbProfileAssetsRepository;
use WL\Modules\Profile\Repositories\ProfileEducationInterface;
use WL\Modules\Profile\Repositories\DbProfileEducationRepository;
use WL\Modules\Profile\Repositories\ProfileJobInterface;
use WL\Modules\Profile\Repositories\DbProfileJobRepository;
use WL\Modules\Profile\Repositories\ProfileProfessionalBodyInterface;
use WL\Modules\Profile\Repositories\DbProfileProfessionalBodyRepository;
use Illuminate\Support\Facades\Event;
use WL\Modules\Profile\Services\ProfileCsvImporterService;
use WL\Modules\Profile\Services\ProfileFileImporterInterface;
use WL\Modules\Profile\Services\ProfileImporterInterface;
use WL\Modules\Profile\Services\ProfileListImporterInterface;
use WL\Modules\Profile\Services\ProfileListImporterService;
use WL\Modules\Profile\Services\ProviderOriginatedService;
use WL\Modules\Profile\Services\ProviderOriginatedServiceInterface;
use WL\Modules\Profile\Services\ProfileReminderService;
use WL\Modules\Profile\Services\ProfileReminderServiceInterface;


class ProfileServiceProvider extends ServiceProvider
{

	public function register()
	{
		$this->app->bind(ClientProfileRepository::class,          DbClientProfileRepository::class);
		$this->app->bind(ModelProfileRepository::class,           DbModelProfileRepository::class);
		$this->app->bind(ProfileAssetsInterface::class,           DbProfileAssetsRepository::class);
		$this->app->bind(ProfileEducationInterface::class,        DbProfileEducationRepository::class);
		$this->app->bind(ProfileJobInterface::class,              DbProfileJobRepository::class);
		$this->app->bind(ProfileProfessionalBodyInterface::class, DbProfileProfessionalBodyRepository::class);
		$this->app->bind(ProviderProfileRepository::class,        DbProviderProfileRepository::class);
		$this->app->bind(StaffProfileRepository::class,           DbStaffProfileRepository::class);
		$this->app->bind(ClientProfilePopulator::class,           FormerClientProfilePopulator::class);
		$this->app->bind(ProviderProfilePopulator::class,         FormerProviderProfilePopulator::class);
		$this->app->bind(StaffProfilePopulator::class,            FormerStaffProfilePopulator::class);

		$this->app->bind(ModelProfileService::class, function($app) {
			return new ModelProfileServiceImpl(new DbModelProfileRepository(new ModelProfile()), $app['yaml']);
		});


		$this->app->bind(ProfileFileImporterInterface::class, function ($app) {
			return new ProfileCsvImporterService(new DbProviderOriginatedRepository());
		});

		$this->app->bind(ProfileListImporterInterface::class, function ($app) {
			return new ProfileListImporterService(new DbProviderOriginatedRepository());
		});
		$this->app->bind(ProfileImporterInterface::class, function ($app) {
			return new BasicProfileImporterService(new DbProviderOriginatedRepository());
		});

		$this->app->bind(ProviderOriginatedServiceInterface::class, function($app) {
			return new ProviderOriginatedService(new DbProviderOriginatedRepository());
		});

		$this->app->bind(ProfileReminderServiceInterface::class, function($app) {
			return new ProfileReminderService(new ProfileReminderRepository(), new ProfileReminderSentRepository());
		});


//
//		$this->app->bind(ProviderProfileService::class, function($app) {
//			return new ProviderProfileServiceImpl(new DbProviderProfileRepository(new ModelProfile()), $app['yaml']);
//		});
//
//		$this->app->bind(ModelProfileService::class, function($app) {
//			return new ModelProfileServiceImpl(new DbModelProfileRepository(new ModelProfile()), $app['yaml']);
//		});

        $this->app->booting(function () {
            AliasLoader::getInstance()->alias('ProfileService', \WL\Modules\Profile\Facades\ProfileService::class);
        });


		Event::listen('profile.created',  'WL\Events\Listeners\ProfileListener@profileCreated');
		Event::listen('profile.changed',  'WL\Events\Listeners\ProfileListener@profileChanged');
	}

	public function boot()
	{
		ClientProfile::observe( new ClientProfileObserver() );
		ProviderProfile::observe( new ProviderProfileObserver() );
	}

}

<?php namespace WL\Modules\Profile\Presenters;

use WL\Modules\Profile\Facades\ProfileService;
use WL\Presenters\Presenter;
use Illuminate\Support\Facades\URL;
use WL\Modules\Profile\Models\ModelProfile;

/**
 * Here will be declared all presenter funcs which works by current object ..
 *
 * Class ProfilePresenter
 * @package WL\Modules\Profile
 */
class ProfilePresenter extends Presenter
{
	public function avatar()
	{
		$avatar =  ProfileService::getAvatar($this->id);
		return $avatar ? $avatar->url() : '';
	}

	public function clientProfileUrl()
	{
		return route('client-profile-show', [
			'id' => $this->id,
		]);
	}

	public function publicUrl()
	{
		if ($this->type == ModelProfile::STAFF_PROFILE_TYPE) {
			return false;
		}
		if ($this->type == ModelProfile::CLIENT_PROFILE_TYPE) {
			return URL::to($url = 'c/' . $this->id);
		}
		if ($this->type == ModelProfile::PROVIDER_PROFILE_TYPE) {
			if ($this->slug) {
				return URL::to($url = 'p/' . $this->slug);
			} else {
				return URL::to($url = 'p/' . $this->id);
			}
		}

		return false;
	}

	public function fullName()
	{
		return $this->entity->getMeta('first_name') . ' ' . $this->entity->getMeta('last_name');
	}

	public function email()
	{
		return ProfileService::getOwner($this->id)->email;
	}
}

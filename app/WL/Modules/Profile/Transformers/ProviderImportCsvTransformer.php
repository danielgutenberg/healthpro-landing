<?php namespace WL\Modules\Profile\Transformers;

use WL\Modules\User\Transformers\UserRegistrationTransformer;
use WL\Transformers\Transformer;

class ProviderImportCsvTransformer extends Transformer
{
	public function transform($item)
	{
		$data = [
			'usersAndProfiles' => (new UserRegistrationTransformer())->transformCollection($item['usersAndProfiles']),
			'existedEmails' => $item['existedEmails'],
			'failedRegistrationEmails' => $item['failedRegistrationEmails'],
			'declinedEmails' => $item['declinedEmails'],
		];

		return $data;
	}
}

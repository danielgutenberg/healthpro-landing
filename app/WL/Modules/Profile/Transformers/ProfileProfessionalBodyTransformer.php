<?php namespace WL\Modules\Profile\Transformers;

use WL\Transformers\Transformer;

class ProfileProfessionalBodyTransformer extends Transformer
{

	public function transform($item)
	{
		if (!is_null($item->expiry_date)) {
			$expiryDate = [
				'month' => $item->expiry_date->format('m'),
				'year' => $item->expiry_date->format('Y')
			];
		} else {
			$expiryDate = [
				'month' => null,
				'year' => null
			];
		}
		return [
			'id' => $item->id,
			'profile_id' => $item->profile_id,
			'title' => $item->title,
			'description' => $item->description,
			'expiry_date' => $expiryDate
		];
	}
}

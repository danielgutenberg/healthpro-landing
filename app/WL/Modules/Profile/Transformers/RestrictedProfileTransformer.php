<?php namespace WL\Modules\Profile\Transformers;


use WL\Transformers\Transformer;

class RestrictedProfileTransformer extends Transformer
{
	public function transform($item)
	{
		return [
			'id' => $item->id,
			'first_name' => $item->first_name,
			'last_name' => $item->last_name,
			'full_name' => $item->first_name . ' ' . $item->last_name,
			'avatar' => $item->avatar,
			'type' => $item->type
		];
	}
}

<?php namespace WL\Modules\Profile\Transformers;

use WL\Modules\Taxonomy\TagTransformer;
use WL\Transformers\Transformer;

class ProfileTagTransformer extends Transformer
{

	public function transform($item)
	{

		return [
			'tag_field_name' => $item['tag_field_name'],
			'tax_slug' => $item['tax_slug'],
			'tags' => (new TagTransformer())->transformCollection($item['tags'])
		];
	}

	public function transformHashedCollection($items, $pluralizeKeys = false)
	{
		if (!is_null($items)) {
			if (!is_array($items)) {
				if ($items instanceof Collection) {
					$items = (array)$items;
					$items = array_shift($items);
				} else
					throw new \Exception('Tag transformer expects a collection.');
			}

			$result = [];
			foreach ($items as $key => $keyItems) {
				$result[$key . ($pluralizeKeys ? 's' : '')] = $this->transform($keyItems);
			}

			return $result;
		}

		return [];
	}
}

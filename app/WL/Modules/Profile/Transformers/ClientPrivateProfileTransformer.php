<?php namespace WL\Modules\Profile\Transformers;

use Carbon\Carbon;
use WL\Transformers\Transformer;

class ClientPrivateProfileTransformer extends Transformer
{

	public function transform($item)
	{
		$result = (new PublicProfileTransformer())->transform($item);

		$result = array_merge($result, [
			'accepted_terms_conditions' => $item->accepted_terms_conditions,
			'phone' => $item->phone
		]);

		if (isset($item->tags))
			$result['tags'] = (new ProfileTagTransformer())->transformHashedCollection($item->tags);
		if (isset($item->assets))
			$result['assets'] = (new ProfileAssetTransformer())->transformHashedCollection($item->assets);
		if (isset($item->social_pages))
			$result['social_pages'] = (new ProfileSocialAccountTransformer())->transformCollection($item->social_pages);

		if (!empty($item->birthday)) {
			$birthday = new Carbon($item->birthday);
			$result['birthday'] = [
				'day' => $birthday->day,
				'month' => $birthday->month,
				'year' => $birthday->year,
			];
		}
		
		$result['reminders'] = (new ProfileReminderTransformer())->transformCollection($item->reminders);

		return $result;
	}
}

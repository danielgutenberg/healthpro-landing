<?php namespace WL\Modules\Profile\Transformers;

use WL\Transformers\Transformer;
use Carbon\Carbon;

class ProfileJobTransformer extends Transformer
{
	public function transform($item)
	{
		$from = Carbon::parse($item->from);

		if ($item->to)
			$to = Carbon::parse($item->to);

		return [
			'id' => $item->id,
			'job_title' => $item->job_title,
			'company_name' => $item->company_name,
			'from' => [
				'month' => $from->format('m'),
				'year'  => $from->format('Y')
			],
			'to' => !empty($to) ? [
				'month' => $to->format('m'),
				'year'  => $to->format('Y')
			] : null,
			'profile_id' => $item->profile_id
		];
	}
}

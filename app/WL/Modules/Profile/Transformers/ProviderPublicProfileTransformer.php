<?php namespace WL\Modules\Profile\Transformers;

use WL\Modules\Location\Transformer\LocationTransformer;
use WL\Modules\Profile\Models\ProviderProfile;
use WL\Modules\Profile\Transformers\ProfileAssetTransformer;
use WL\Modules\Profile\Transformers\ProfileEducationTransformer;
use WL\Modules\Profile\Transformers\ProfileJobTransformer;
use WL\Modules\Profile\Transformers\ProfileProfessionalBodyTransformer;
use WL\Modules\Profile\Transformers\PublicProfileTransformer;
use WL\Modules\Profile\Transformers\ProfileSocialAccountTransformer;
use WL\Modules\Profile\Transformers\ProfileTagTransformer;
use WL\Modules\Rating\Models\Rating;
use WL\Modules\Rating\Transformers\OverallRatingTransformer;
use WL\Modules\Rating\Transformers\RatingTransformer;
use WL\Modules\Review\Facade\ReviewService;
use WL\Transformers\Transformer;

class ProviderPublicProfileTransformer extends Transformer
{
	public function transform($item)
	{
		$result = (new PublicProfileTransformer())->transform($item);

		$fields = [
			'slug' => null,
			'services' => null,

			'ratings' => function ($item) {
				return [
					'details' => (new OverallRatingTransformer())->transformCollection($item->ratings->details),
					'overall' => round($item->ratings->overall->rating_value, 1)
				];
			},

			'about_self_background' => null,
			'about_what_i_do' => null,
			'about_services_benefit' => null,
			'about_self_specialities' => null,
			'accepted_terms_conditions' => null,

			'tags' => function ($item) {
				return (new ProfileTagTransformer())->transformHashedCollection($item->tags);
			},
			'assets' => function ($item) {
				return (new ProfileAssetTransformer())->transformHashedCollection($item->assets);
			},
			'social_pages' => function ($item) {
				return (new ProfileSocialAccountTransformer())->transformCollection($item->social_pages);
			},
			'professional_body' => function ($item) {
				return (new ProfileProfessionalBodyTransformer())->transformCollection($item->professional_body);
			},
			'educations' => function ($item) {
				return (new ProfileEducationTransformer())->transformCollection($item->educations);
			},
			'jobs' => function ($item) {
				return (new ProfileJobTransformer())->transformCollection($item->jobs);
			},
			'locations' => function ($item) {
				return (new LocationTransformer())->transformCollection($item->locations);
			},

		];

		foreach ($fields as $key => $func) {
			if (isset($item->$key)) {
				if ($func != null)
					$result[$key] = $func($item);
				else
					$result[$key] = $item->$key;
			}
		}

		if (!empty($item->business_name_display) && !empty($item->business_name) ) {
			$result['full_name'] = $item->business_name;
		}
		elseif (!empty($item->full_name)){
			$result['full_name'] = $item->full_name;
		}
		return $result;

	}
}

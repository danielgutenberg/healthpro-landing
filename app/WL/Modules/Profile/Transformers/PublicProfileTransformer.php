<?php namespace WL\Modules\Profile\Transformers;

use Carbon\Carbon;
use WL\Transformers\Transformer;

class PublicProfileTransformer extends Transformer
{
	public function transform($item)
	{
		$allowed = array_flip(['id', 'title', 'gender', 'first_name', 'last_name', 'avatar', 'type', 'avatar_original', 'public_url', 'occupation', 'age', 'slug', 'is_provider_client']);

		$result = array_intersect_key(json_decode(json_encode($item), True), $allowed);
		$result['full_name'] = $item->first_name . ' ' . $item->last_name;

		return $result;
	}
}

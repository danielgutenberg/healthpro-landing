<?php namespace WL\Modules\Profile\Transformers;

use WL\Transformers\Transformer;
use Carbon\Carbon;

class ProfileEducationTransformer extends Transformer
{

	public function transform($item)
	{
		if ($item->from) {
			$from = Carbon::parse($item->from);
		}

		if ($item->to) {
			$to = Carbon::parse($item->to);
		}

		return [
			'id' => $item->id,
			'education_id' => $item->education_id,
			'education_name' => $item->education_name,
			'from' => !empty($from) ? [
				'month' => $from->format('m'),
				'year'  => $from->format('Y')
			] : null,
			'to' => !empty($to) ? [
				'month' => $to->format('m'),
				'year'  => $to->format('Y')
			] : null,
			'profile_id' => $item->profile_id,
			'degree' => $item->degree
		];
	}
}

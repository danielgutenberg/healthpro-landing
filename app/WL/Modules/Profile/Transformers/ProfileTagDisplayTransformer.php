<?php namespace WL\Modules\Profile\Transformers;

use WL\Modules\Taxonomy\TagDisplayTransformer;
use WL\Transformers\Transformer;

class ProfileTagDisplayTransformer extends Transformer
{

	public function transform($item)
	{

		return (new TagDisplayTransformer())->transformCollection($item['tags']);
	}
}

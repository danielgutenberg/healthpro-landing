<?php namespace WL\Modules\Profile\Transformers;


use Carbon\Carbon;
use WL\Transformers\Transformer;

class ProviderClientInviteTransformer extends Transformer
{
	public function transform($item)
	{
		$result = [
			'id' => $item->id,
			'client_id' => $item->client_id,
            'client_email' => $item->client_email,
			'provider_id' => $item->provider_id,
			'approved' => $item->approved,
			'last_invite' => $item->last_invite->toDateTimeString(),
			'time_since' => $item->last_invite->diffForHumans(),
			'invite_count' => $item->invite_count,
			'can_invite' => $item->can_invite,
		];
		if(isset($item->profile)){
			$result['profile'] = (new PublicProfileTransformer())->transform($item->profile);
		}
		return $result;
	}
}

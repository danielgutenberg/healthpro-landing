<?php namespace WL\Modules\Profile\Transformers;


use WL\Transformers\Transformer;

class ProfileReminderTransformer extends Transformer
{
	public function transform($item)
	{
		$result = [
			'id' => $item->id,
			'value' => $item->value,
			'type' => $item->type,
			'about_entity_type' => $item->about_entity_type
		];

		return $result;
	}
}

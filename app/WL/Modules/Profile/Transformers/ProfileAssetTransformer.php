<?php namespace WL\Modules\Profile\Transformers;

use WL\Transformers\Transformer;

class ProfileAssetTransformer extends Transformer
{

	public function transform($item)
	{
		return [
			'id' => $item->id,
			'attachment_id' => $item->attachment_id,
			'url' => $item->url,
			'attachment' => $item->attachment,
			'profile_id' => $item->profile_id,
			'title' => $item->title,
			'description' => $item->description,
			'storage' => $item->storage,
			'type' => $item->type,

		];
	}
}

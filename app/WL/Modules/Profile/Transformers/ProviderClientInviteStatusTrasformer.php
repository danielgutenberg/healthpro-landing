<?php namespace WL\Modules\Profile\Transformers;


use WL\Transformers\Transformer;

class ProviderClientInviteStatusTrasformer extends Transformer
{
	public function transform($item)
	{
		if(!$item->invited){
			$status = 'not_invited';
		} elseif(!$item->confirmed){
			$status = 'invited';
		} else {
			$status = $item->approved ? 'approved' : 'decline';
		}
		return [
			'status' => $status,
		];
	}
}

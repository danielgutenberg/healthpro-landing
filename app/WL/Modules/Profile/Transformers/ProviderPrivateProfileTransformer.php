<?php namespace WL\Modules\Profile\Transformers;

use Carbon\Carbon;
use WL\Modules\Location\Models\Phone;
use WL\Modules\Phone\Transformers\PhoneTransformer;
use WL\Modules\Profile\Transformers\ProfileJobTransformer;
use WL\Transformers\Transformer;

class ProviderPrivateProfileTransformer extends Transformer
{

	public function transform($item)
	{
		$result = (new PublicProfileTransformer())->transform($item);

		$result = array_merge($result, [
			'about_self_background' => $item->about_self_background,
			'about_what_i_do' => $item->about_what_i_do,
			'about_services_benefit' => $item->about_services_benefit,
			'about_self_specialities' => $item->about_self_specialities,
			'business_name' => $item->business_name,
			'business_name_display' => $item->business_name_display,
			'accepted_terms_conditions' => $item->accepted_terms_conditions,
			'is_blogger' => $item->is_blogger,
			'commission_free_period' => $item->commission_free_period->toDateString(),
			'commission_free_period_in_days' => $item->commission_free_period_in_days,
			'available_credit' => $item->available_credit,
			'features' => $item->features,
			'accepted_payment_methods' => $item->accepted_payment_methods
		]);

		$result['tags'] = (new ProfileTagTransformer())->transformHashedCollection($item->tags);
		$result['assets'] = (new ProfileAssetTransformer())->transformHashedCollection($item->assets);
		$result['social_pages'] = (new ProfileSocialAccountTransformer())->transformCollection($item->social_pages);
		$result['professional_body'] = (new ProfileProfessionalBodyTransformer())->transformCollection($item->professional_body);
		$result['educations'] = (new ProfileEducationTransformer())->transformCollection($item->educations);
		$result['jobs'] = (new ProfileJobTransformer())->transformCollection($item->jobs);
		$result['phone'] = (new PhoneTransformer())->transform($item->phone);

		if (!empty($item->birthday)) {
			$birthday = new Carbon($item->birthday);
			$result['birthday'] = [
				'day' => $birthday->day,
				'month' => $birthday->month,
				'year' => $birthday->year,
			];
		}

		return $result;
	}
}

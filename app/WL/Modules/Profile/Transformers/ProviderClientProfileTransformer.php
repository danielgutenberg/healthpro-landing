<?php namespace WL\Modules\Profile\Transformers;

use WL\Modules\Appointment\Transformers\FullAppointmentTransformer;
use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Modules\Profile\ProfilesMeta\ProviderClientsMeta;
use WL\Transformers\Transformer;

class ProviderClientProfileTransformer extends Transformer
{
	public function transform($item)
	{
		$result = (new ProviderPublicProfileTransformer())->transform($item);

		$result['meta'] = $item->meta;

		if (isset($item->phone)) {
			$result['phone'] = $item->phone;
		}

		if (isset($item->visits)) {
			$result['visits'] = $item->visits;
		}

		if (isset($item->last_appointment) && $item->last_appointment) {
			$result['last_appointment'] = (new FullAppointmentTransformer())->transform($item->last_appointment);
		}

		if (isset($item->next_appointment) && $item->next_appointment) {
			$result['next_appointment'] = (new FullAppointmentTransformer())->transform($item->next_appointment);
		}

		return $result;
	}
}

<?php namespace WL\Modules\Profile\Transformers;

use WL\Modules\PricePackage\Transformers\ChargedPricePackageTransformer;
use WL\Modules\Provider\Transformers\ProviderServiceTransformer;
use WL\Modules\Appointment\Transformers\FullAppointmentTransformer;
use WL\Modules\Rating\Transformers\OverallRatingTransformer;
use WL\Modules\Review\Transformers\ReviewSummaryTransformer;
use WL\Modules\Review\Transformers\ReviewTransformer;
use WL\Transformers\Transformer;

class ClientProviderProfileTransformer extends Transformer
{
	public function transform($item)
	{
		$result = (new ProviderPublicProfileTransformer())->transform($item);

		$result['can_review'] = $item->can_review;
		$result['is_searchable'] = $item->is_searchable;
		if (isset($item->services) && $item->services)
			$result['services'] = (new ProviderServiceTransformer())->transformCollection($item->services);

		if (isset($item->reviews) && $item->reviews)
			$result['reviews'] = (new ReviewSummaryTransformer())->transformCollection($item->reviews);

		$result['ratings'] = [
			'details' => (new OverallRatingTransformer())->transformCollection($item->ratings->details),
			'overall' => round($item->ratings->overall->rating_value, 1)
		];

		if (isset($item->last_appointment) && $item->last_appointment) {
			$result['last_appointment'] = (new FullAppointmentTransformer())->transform($item->last_appointment);
		}

		if (isset($item->next_appointment) && $item->next_appointment) {
			$result['next_appointment'] = (new FullAppointmentTransformer())->transform($item->next_appointment);
		}

		if (isset($item->available_packages) && $item->available_packages) {
			$result['available_packages'] = (new ChargedPricePackageTransformer())->transformCollection($item->available_packages);
		}

		return $result;
	}
}

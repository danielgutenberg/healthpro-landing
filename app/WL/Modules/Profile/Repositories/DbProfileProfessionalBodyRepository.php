<?php namespace WL\Modules\Profile\Repositories;

use Carbon\Carbon;
use WL\Modules\Profile\Models\ProfileProfessionalBody;
use WL\Repositories\DbRepository;

class DbProfileProfessionalBodyRepository extends DbRepository implements ProfileProfessionalBodyInterface
{

	/**
	 * @var Profile
	 */
	protected $model;

	protected $modelClassName = ProfileProfessionalBody::class;

	public function __construct()
	{
		$this->model = new ProfileProfessionalBody();
	}

	function getUnexpiredItems($profileId)
	{
		return ProfileProfessionalBody::where(function($query) {
			$query->whereNull('expiry_date')
				->orWhere('expiry_date', '>', Carbon::now());
		})->where('profile_id', $profileId)->get();
	}

	function removeAllProfileItems($profileId)
	{
		return ProfileProfessionalBody::where('profile_id', $profileId)->delete();
	}

}

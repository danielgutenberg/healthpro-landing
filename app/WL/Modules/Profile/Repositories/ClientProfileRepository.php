<?php namespace WL\Modules\Profile\Repositories;

use WL\Repositories\Repository;

interface ClientProfileRepository  extends Repository
{
	/**
	 * @param $clientId
	 * @return mixed
     */
	public function getProviders($clientId);
}

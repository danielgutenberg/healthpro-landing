<?php namespace WL\Modules\Profile\Repositories;

use Illuminate\Support\Facades\DB;
use WL\Modules\Profile\Models\ProfileAsset;
use WL\Repositories\DbRepository;

class DbProfileAssetsRepository extends DbRepository implements ProfileAssetsInterface
{

	/**
	 * @var Profile
	 */
	protected $model;

	protected $modelClassName = ProfileAsset::class;

	public function __construct()
	{
		$this->model = new ProfileAsset();
	}

	public function getByIdSecure($profileId, $id)
	{
		return DB::table('profile_assets')
			->join('attachments', 'attachments.id', '=', 'attachment_id')
			->select('profile_assets.*', DB::raw('attachments.name as url'))
			->where('profile_assets.id', $id)
			->where('profile_assets.profile_id', $profileId)
			->first();
	}

	public function removeAsset($profileId, $assetId)
	{
		return ProfileAsset::where('id', $assetId)
			->where('profile_id', $profileId)
			->delete();
	}

	public function getAssetsByType($profileId, $assetType)
	{
		$results = DB::table('profile_assets')
			->join('attachments', 'attachments.id', '=', 'attachment_id')
			->where('profile_assets.type', $assetType)
			->where('profile_assets.profile_id', $profileId)
			->select('profile_assets.*', DB::raw('attachments.name as url'))
			->get();

		return $results;
	}


	public function getAllAssets($profileId)
	{
		$results = DB::table('profile_assets')
			->join('attachments', 'attachments.id', '=', 'attachment_id')
			->where('profile_id', $profileId)
			->select('profile_assets.*', DB::raw('attachments.name as url'))
			->get();

		return $results;
	}

}

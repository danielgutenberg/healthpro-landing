<?php namespace WL\Modules\Profile\Repositories;

use Illuminate\Support\Facades\DB;
use WL\Modules\Appointment\Models\ProviderAppointment;
use WL\Modules\Profile\Models\ProviderProfile;
use WL\Repositories\DbRepository;

class DbProviderProfileRepository extends DbRepository implements ProviderProfileRepository
{
	protected $model;

	protected $modelClassName = ProviderProfile::class;

	public function __construct()
	{
		$this->model = new $this->modelClassName();
	}
}

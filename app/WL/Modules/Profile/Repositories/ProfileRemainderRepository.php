<?php namespace WL\Modules\Profile\Repositories;

use WL\Modules\Profile\Models\ProfileReminder;
use WL\Repositories\DbRepository;

class ProfileReminderRepository extends DbRepository implements ProfileReminderRepositoryInterface
{

	public function getProfileReminders($profileId)
	{
		return ProfileReminder::where('profile_id', $profileId)->get();
	}

	public function updateProfileReminderValue($id, $value)
	{
		return ProfileReminder::where('id', $id)->update(['value' => $value]);
	}
}

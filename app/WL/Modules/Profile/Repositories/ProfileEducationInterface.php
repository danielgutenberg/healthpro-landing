<?php namespace WL\Modules\Profile\Repositories;

use WL\Repositories\Repository;
use WL\Modules\Profile\Models\ProfileEducation;

/**
 * Interface ProfileEducationInterface
 * @package WL\Modules\Profile\Repositories
 */
interface ProfileEducationInterface extends Repository
{
	/**
	 * @param $profileId
	 * @return mixed
	 */
	public function removeProfileEducations($profileId);

	/**
	 * @param ProfileEducation $education
	 * @return mixed
	 */
	public function saveEducation(ProfileEducation $education);

	/**
	 * @param $profileId
	 * @return mixed
	 */
	public function getProfileEducation($profileId);

}

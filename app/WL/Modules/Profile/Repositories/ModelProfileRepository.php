<?php namespace WL\Modules\Profile\Repositories;

use WL\Modules\Profile\Models\ModelProfile;
use WL\Repositories\Repository;

/**
 * Interface ModelProfileRepository
 * @package WL\Modules\Profile\Repositories
 */
interface ModelProfileRepository extends Repository
{
	/**
	 * Get profiles by user id
	 * @param $userId
	 * @return mixed
	 */
	public function getProfilesByUserId($userId);

	/**
	 * Add relation between location and profile
	 * @param $locationId
	 * @param $profileId
	 * @param $acceptAppointments
	 * @return mixed
	 */
	public function addLocation($locationId, $profileId, $acceptAppointments);

	/**
	 * Remove relation between location and prpfile
	 * @param $locationId
	 * @param $profileId
	 * @return mixed
	 */
	public function removeLocation($locationId, $profileId);

	/**
	 * Get profile by user and profile
	 * @param $userId number
	 * @param $profileId number
	 * @return mixed
	 */
	public function getProfileByUserAndProfile($userId, $profileId);

	/**
	 * Get or fail profile by slug
	 * @param $slug
	 * @return mixed
	 */
	public function getBySlug($slug);

	/**
	 * Get profile by user and profile
	 * @param $userId number
	 * @param $profileId number
	 * @return mixed
	 */
	public function getProfileByUserAndProfileId($userId, $profileId);


	/**
	 * Get profile by user id and profile type
	 * @param int $userId
	 * @param string $profileType
	 * @return mixed
	 */
	public function getProfileByUserAndProfileType($userId, $profileType);

	/**
	 * Get profile owner by profile id
	 * @param $profileId
	 * @return mixed
	 */
	public function getProfileOwnerByProfileId($profileId);

	/**
	 * @param $profileId
	 * @param $searchable
	 * @return mixed
	 */
	public function setIsSearchable($profileId, $searchable);

    /**
     * @param int $profileId
     * @param bool $bookable
     * @return int
     */
    public function setIsBookable($profileId, $bookable);

    /**
     * @param int $profileId
     * @param string $status
     * @return int
     */
    public function setStatus($profileId, $status);

	/**
	 * Search for profiles by query string. Query string can be email or user (first name - last name)
	 * @param $query
	 * @param bool $isEmail
	 * @param string $profileType
	 * @param $onlyProfileIds
	 * @param $limit
	 * @return mixed
	 */
	public function searchProfiles(
		$query,
		$isEmail = false,
		$profileType = ModelProfile::PROVIDER_PROFILE_TYPE,
		$onlyProfileIds,
		$limit
	);

	/**
	 * Get provider profiles
	 *
	 * @return mixed
	 */
	public function getProviders();

	/**
	 * @param $providerId
	 * @param $unavailableMinutes
	 * @return mixed
	 */
	public function setProviderUnavailableTimeBeforeAppointment($providerId, $unavailableMinutes);

	/**
	 * @param $providerId
	 * @return mixed
	 */
	public function getProviderUnavailableTimeBeforeAppointment($providerId);
}

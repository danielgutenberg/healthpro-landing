<?php namespace WL\Modules\Profile\Repositories;

use WL\Repositories\Repository;

/**
 * Interface ProfileAssetsInterface
 * @package WL\Modules\Profile\Repositories
 */
interface ProfileAssetsInterface extends Repository
{
	/**
	 * Return only one type assets of profile
	 *
	 * @param $profileId
	 * @param $assetType
	 * @return mixed
	 */
	public function getAssetsByType($profileId, $assetType);

	/**
	 * Return all profile assets
	 *
	 * @param $profileId
	 * @return mixed
	 */
	public function getAllAssets($profileId);


	/**
	 * Removes Profile Asset
	 *
	 * @param $profileId
	 * @param $assetId
	 * @return mixed
	 */
	public function removeAsset($profileId, $assetId);

	/**
	 * @param $profileId
	 * @param $id
	 * @return mixed
	 */
	public function getByIdSecure($profileId, $id);
}

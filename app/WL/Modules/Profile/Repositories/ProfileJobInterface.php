<?php namespace WL\Modules\Profile\Repositories;

use WL\Repositories\Repository;
use WL\Modules\Profile\Models\ProfileJob;

/**
 * Interface ProfileEducationInterface
 * @package WL\Modules\Profile\Repositories
 */
interface ProfileJobInterface extends Repository
{
	/**
	 * @param $profileId
	 * @return mixed
	 */
	public function removeProfileJobs($profileId);

	/**
	 * @param ProfileJob $job
	 * @return mixed
	 */
	public function saveJob(ProfileJob $job);

	/**
	 * @param $profileId
	 * @return mixed
	 */
	public function getProfileJobs($profileId);

}

<?php namespace WL\Modules\Profile\Repositories;

use Illuminate\Support\Facades\DB;
use WL\Modules\Profile\Models\ProfileJob;
use WL\Repositories\DbRepository;
use Carbon\Carbon;

class DbProfileJobRepository extends DbRepository implements ProfileJobInterface
{

	protected $model;

	protected $modelClassName = ProfileJob::class;

	public function __construct()//ProfileEducation $model)
	{
		$this->model = new ProfileJob();
	}

	public function removeProfileJobs($profileId)
	{
		ProfileJob::where('profile_id', $profileId)->delete();
	}

	public function saveJob(ProfileJob $job)
	{
		return $job->save();
	}

	public function getProfileJobs($profileId)
	{
		$result =  DB::table('profiles_jobs')
			->where('profile_id', $profileId)
			->select('profiles_jobs.*')
			->get();
		$formattedResults = $this->formatResults($result);
		return ProfileJob::hydrate($formattedResults);
	}

	public function formatResults($jobs)
	{
		if (!is_array($jobs)) {
			return $jobs;
		}
		foreach ($jobs as $my => $job) {
			$from = Carbon::parse($job->from);

			if ($job->to) {
				$to = Carbon::parse($job->to);
			} else {
				$to = null;
			}

			$jobs[$my]->from_month = $from->format('m');
			$jobs[$my]->from_year = $from->format('Y');
			$jobs[$my]->to_month = !empty($to) ? $to->format('m') : null;
			$jobs[$my]->to_year = !empty($to) ? $to->format('Y') : null;
		}

		return $jobs;
	}
}

<?php namespace WL\Modules\Profile\Repositories;

use Illuminate\Support\Facades\DB;
use WL\Modules\Profile\Models\ProfileEducation;
use WL\Repositories\DbRepository;
use Carbon\Carbon;

class DbProfileEducationRepository extends DbRepository implements ProfileEducationInterface
{

	protected $model;

	protected $modelClassName = ProfileEducation::class;

	public function __construct()//ProfileEducation $model)
	{
		$this->model = new ProfileEducation();
	}

	public function removeProfileEducations($profileId)
	{
		ProfileEducation::where('profile_id', $profileId)->delete();
	}

	public function saveEducation(ProfileEducation $education)
	{
		return $education->save();
	}

	public function getProfileEducation($profileId)
	{
		$result =  DB::table('profiles_educations')
			->join('educations', 'educations.id', '=', 'profiles_educations.education_id')
			->where('profile_id', $profileId)
			->select('profiles_educations.*', 'educations.name as education_name')
			->get();

		$formattedResults = $this->formatResults($result);
		return ProfileEducation::hydrate($formattedResults);
	}

	public function formatResults($educations)
	{
		if (!is_array($educations)) {
			return $educations;
		}
		foreach ($educations as $my => $education) {
			$from = Carbon::parse($education->from);

			if ($education->to) {
				$to = Carbon::parse($education->to);
			} else {
				$to = null;
			}

			$educations[$my]->from_month = $from->format('m');
			$educations[$my]->from_year = $from->format('Y');
			$educations[$my]->to_month = !empty($to) ? $to->format('m') : null;
			$educations[$my]->to_year = !empty($to) ? $to->format('Y') : null;
		}

		return $educations;
	}
}

<?php namespace WL\Modules\Profile\Repositories;

use WL\Repositories\Repository;

interface StaffProfileRepository extends Repository
{

	/**
	 * Get a list of profiles with filters
	 *
	 * @param $filters
	 * @return mixed
	 */
	public function getFiltered( $filters = [] );

}

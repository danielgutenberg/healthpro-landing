<?php namespace WL\Modules\Profile\Repositories;

use Illuminate\Support\Facades\DB;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Models\ProviderOriginated;
use WL\Repositories\DbRepository;

class DbProviderOriginatedRepository extends DbRepository implements ProviderOriginatedRepositoryInterface
{

	public function getProviderClients($providerId, $originated = null, $pending = false)
	{
		$query = ProviderOriginated::where('provider_id', $providerId);

		if (!is_null($originated)) {
			$query->where('originated', $originated);
		}

		if ($pending) {
			$query->whereNull('approved');
		} else {
			$query->where('approved', true);
		}

		return $query->get();
	}

	public function getClientProviders($clientId, $pending = false)
	{
		$query = ProviderOriginated::where('client_id', $clientId);

		if ($pending) {
			$query->whereNull('approved');
		} else {
			$query->where('approved', true);
		}

		return $query->get();
	}

	public function getOriginator($clientId)
	{
		return ProviderOriginated::where('client_id', $clientId)->where('originated', 1)->first();
	}

	public function firstOrNew($providerId, $clientId)
	{
		return ProviderOriginated::firstOrNew([
			'client_id' => $clientId,
			'provider_id' => $providerId,
		]);
	}

	public function get($providerId, $clientId)
	{
		return ProviderOriginated::where('provider_id', $providerId)->where('client_id', $clientId)->first();
	}

	public function hasProviderOriginated($clientId)
	{
		return ProviderOriginated::where('originated', true)->where('client_id', $clientId)->count() == 0;
	}
}

<?php namespace WL\Modules\Profile\Repositories;

use Illuminate\Support\Facades\DB;
use WL\Modules\Profile\Models\ClientProfile;
use WL\Modules\Profile\Models\ProviderProfile;
use WL\Repositories\DbRepository;

class DbClientProfileRepository extends DbRepository implements ClientProfileRepository
{
	protected $model;

	protected $modelClassName = ClientProfile::class;

	public function __construct()
	{
		$this->model = new $this->modelClassName();
	}

	public function getProviders($clientId)
	{
		$_query = DB::table(ClientProfile::getTableName())->distinct()
			->where('profiles.type', '=', ProviderProfile::PROVIDER_PROFILE_TYPE)
			->select('profiles.*')
			->join('provider_appointments', 'profiles.id', '=', 'provider_appointments.provider_id')
			->where('provider_appointments.client_id', '=', $clientId);

		$items = ProviderProfile::hydrate($_query->get());

		return $items;
	}
}

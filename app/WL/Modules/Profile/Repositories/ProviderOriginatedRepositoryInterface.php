<?php namespace WL\Modules\Profile\Repositories;

use Illuminate\Support\Collection;
use WL\Modules\Profile\Models\ProviderOriginated;
use WL\Repositories\Repository;

interface ProviderOriginatedRepositoryInterface extends Repository
{
	/**
	 * @param int $providerId
	 * @param boolean|null $originated
	 * @param boolean|false $pending
	 * @return Collection
	 */
	public function getProviderClients($providerId, $originated = null, $pending = false);

	/**
	 * @param int $providerId
     * @param boolean|false $pending
	 * @param boolean|true $deleted
	 * @return Collection
	 */

	public function getClientProviders($clientId, $pending = false);

	/**
	 * @param int $providerId
	 * @param int $clientId
	 * @return ProviderOriginated
	 */
	public function firstOrNew($providerId, $clientId);

	/**
	 * @param int $providerId
	 * @param int $clientId
	 * @return ProviderOriginated
	 */
	public function get($providerId, $clientId);

	/**
	 * @param int $clientId
	 * @return bool
	 */
	public function hasProviderOriginated($clientId);

	/**
	 * @param int $clientId
	 * @return ProviderOriginated
	 */
	public function getOriginator($clientId);
}

<?php namespace WL\Modules\Profile\Repositories;

use WL\Repositories\Repository;

interface ProfileReminderRepositoryInterface extends Repository
{

	/**
	 * Get profile reminders
	 * 
	 * @param $profileId
	 * @return mixed
     */
	public function getProfileReminders($profileId);

	/**
	 * Update ProfileReminder value
	 * 
	 * @param $id
	 * @param $value
	 * @return mixed
     */
	public function updateProfileReminderValue($id, $value);
	
	
}

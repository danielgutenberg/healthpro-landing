<?php namespace WL\Modules\Profile\Repositories;

use WL\Modules\Profile\Models\ProfileReminderSent;
use WL\Repositories\DbRepository;

class ProfileReminderSentRepository extends DbRepository implements ProfileReminderSentRepositoryInterface
{
	public function getProfileReminderSentDateTime($profileReminderId, $aboutEntityId, $aboutEntityType)
	{
		return ProfileReminderSent::where('profile_reminder_id', $profileReminderId)
			->where('about_entity_id', $aboutEntityId)
			->where('about_entity_type', $aboutEntityType)
			->orderBy('created_at','DESC')
			->value('created_at');
	}
}

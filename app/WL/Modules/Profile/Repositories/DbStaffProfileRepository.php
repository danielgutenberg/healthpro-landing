<?php namespace WL\Modules\Profile\Repositories;

use WL\Modules\Profile\Models\StaffProfile;
use WL\Repositories\DbRepository;

class DbStaffProfileRepository extends DbRepository implements StaffProfileRepository
{
	/**
	 * @var Profile
	 */
	protected $model;

	protected $modelClassName = StaffProfile::class;

	public function __construct(StaffProfile $model) {
		$this->model = $model;
	}

	public function getFiltered($filters = []) {

	}
}

<?php namespace WL\Modules\Profile\Repositories;

use WL\Repositories\Repository;

interface ProfileReminderSentRepositoryInterface extends Repository
{
	
	public function getProfileReminderSentDateTime($profileReminderId, $aboutEntityId, $aboutEntityType);
	
}

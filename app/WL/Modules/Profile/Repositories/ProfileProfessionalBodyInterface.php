<?php namespace WL\Modules\Profile\Repositories;

use WL\Repositories\Repository;

/**
 * Interface ProfileProfessionalBodyInterface
 * @package WL\Modules\Profile\Repositories
 */
interface ProfileProfessionalBodyInterface extends Repository
{

	/**
	 * @param $profileId
	 * @return mixed
	 */
	function getUnexpiredItems($profileId);

	/**
	 * @param $profileId
	 * @return mixed
	 */
	function removeAllProfileItems($profileId);
}

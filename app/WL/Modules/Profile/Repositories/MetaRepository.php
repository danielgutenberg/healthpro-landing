<?php namespace WL\Modules\Profile\Repositories;


use WL\Modules\Profile\Models\ProfileMeta;
use WL\Helpers\ArrayHelper;
use Illuminate\Support\Facades\DB;

class MetaRepository
{
	protected $elmType;
	protected $table;

	public function __construct($elmType)
	{
		$this->elmType = $elmType;
		$this->table = ProfileMeta::getTableName();
	}

	public function setElmType($elmType)
	{
		$this->elmType = $elmType;
	}

	private function getMeta($elmId, $key)
	{
		return DB::table(ProfileMeta::getTableName())->where('elm_id', $elmId)->where('elm_type', $this->elmType)->where('meta_key', $key)->first();
	}

	private function updateMeta($elmId, $key, $value)
	{
		return DB::table(ProfileMeta::getTableName())->where('elm_id', $elmId)->where('elm_type', $this->elmType)->where('meta_key', $key)
			->update(['meta_value' => $value]);
	}

	private function insertMeta($elmId, $key, $value)
	{
		return DB::table(ProfileMeta::getTableName())->insert([
			'elm_id' => $elmId,
			'elm_type' => $this->elmType,
			'meta_key' => $key,
			'meta_value' => $value,
		]);
	}

	public function getMetaKey($elmId, $key, $defaultValue = null)
	{
		$result = $this->getMeta($elmId, $key);

		return !empty($result) ? $result->meta_value : $defaultValue;
	}

	public function setMetaKey($elmId, $key, $value)
	{
		if($this->getMeta($elmId, $key)){
			return $this->updateMeta($elmId, $key, $value);
		}
		return $this->insertMeta($elmId, $key, $value);
	}

	public function getMetaKeys($elmId, $keys = [])
	{
		$items = ArrayHelper::array_attr($keys);
		$query = ProfileMeta::where('elm_id', $elmId)->where('elm_type', $this->elmType);
		if (!empty($items)) {
			$query->whereIn('meta_key', array_keys($items));
		}

		foreach ($query->get() as $meta) {
			$items[$meta->meta_key] = $meta->meta_value;
		}

		return $items;
	}
}

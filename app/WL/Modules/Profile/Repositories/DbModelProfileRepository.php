<?php namespace WL\Modules\Profile\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use WL\Modules\Appointment\Models\ProviderAppointment;
use WL\Modules\Location\Facades\LocationService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Models\ProfileMeta;
use WL\Modules\User\Models\User;
use WL\Repositories\DbRepository;

class DbModelProfileRepository extends DbRepository implements ModelProfileRepository
{

	protected $model;

	protected $modelClassName = ModelProfile::class;

	const LOCATION_PROFILE_TABLE = 'locations_profiles';
	const USER_PROFILE_TABLE = "users_profiles";
	const META_PROFILE_TABLE = 'profiles_meta';

	public function __construct(ModelProfile $model)
	{
		$this->model = $model;
	}

	public function getById($id, $withTrashed = false)
	{
		$query = $this->model->where('id', $id);
		if ($withTrashed) {
			$query->withTrashed();
		}
		return $query->first();
	}

	/**
	 * Create profile instance and save ..
	 *
	 * @param bool $save
	 * @param array $attributes
	 * @return mixed
	 */
	public function createProfile($save = true, array $attributes = array())
	{
		$profile = self::createNew(array_merge(
			$attributes, [
				'is_searchable' => 0,
				'verified' => 0,
			]
		));

		if ($save) {
			$profile->save();
		}

		return $profile;
	}

	public function getProfilesByUserId($userId)
	{
		return User::find($userId)->profiles;
	}

	public function getBySlug($slug)
	{
		return ModelProfile::where('slug', $slug)->firstOrFail();
	}

	public function getProfileByUserAndProfileId($userId, $profileId)
	{
		return User::find($userId)->profiles()->where('profile_id', $profileId)->first();
	}

	public function getLocations($profileId)
	{
		return LocationService::getLocations($profileId);
	}

	public function addLocation($locationId, $profileId, $acceptAppointments)
	{
		return DB::table(static::LOCATION_PROFILE_TABLE)
			->insert([
				'location_id' => $locationId,
				'profile_id' => $profileId,
				'accept_appointments' => $acceptAppointments
			]);
	}

	public function removeLocation($locationId, $profileId)
	{
		$affected = DB::table(static::LOCATION_PROFILE_TABLE)
			->where('location_id', '=', $locationId)
			->where('profile_id', '=', $profileId)
			->delete();

		return ($affected > 0);
	}

	public function getProfileByUserAndProfile($userId, $profileId)
	{
		return User::find($userId)->profiles()->where('id', $profileId)->get();
	}

	public function getProfileByUserAndProfileType($userId, $profileType)
	{
		return User::find($userId)->profiles()->where('type', $profileType)->get();
	}

	public function getProfileOwnerByProfileId($profileId)
	{
		return User::whereHas('profiles', function ($q) use ($profileId){
			$q->where('profile_id', '=', $profileId)
				->where('is_owner', true);
		})
			->first();
	}

	public static function array_attr($array)
	{
		$arr = [];
		foreach ($array as $key => $value) {
			if (!is_string($key)) {
				$arr[$value] = null;
			} else {
				$arr[$key] = $value;
			}
		}

		return $arr;
	}

	public function setIsSearchable($profileId, $searchable)
	{
		return $this->updateBooleanField($profileId, 'is_searchable', $searchable);
	}

	public function setIsBookable($profileId, $bookable)
	{
		return $this->updateBooleanField($profileId, 'is_bookable', $bookable);
	}

	public function setStatus($profileId, $status)
	{
		return $this->updateSingleFields($profileId, 'status', $status);
	}

	private function updateBooleanField($id, $field, $value)
	{
		$model = ModelProfile::find($id);
		if((bool) $model->$field === (bool) $value) {
			return false;
		}
		return $model->update([$field => $value]);
	}

    private function updateSingleFields($id, $field, $value)
    {
        return ModelProfile::where('id', $id)->update([$field => $value]);
	}

	public function searchProfiles(
		$queryString,
		$isEmail = false,
		$profileType = ModelProfile::PROVIDER_PROFILE_TYPE,
		$onlyProfileIds = null,
		$limit = null
	){
		$query = null;
		$queryString = trim($queryString, '%\\');

		if (empty($queryString)) {
			return [];
		}

		if ($isEmail) {
			$query = DB::table('profiles')
				->select('profiles.id')
				->join('users_profiles', 'users_profiles.profile_id', '=', 'profiles.id')
				->join('users', 'users.id', '=', 'users_profiles.user_id')
				->where('profiles.type', $profileType)
				->where('users.email', 'like', "%$queryString%");
		} else {
			$queryParts = explode(' ', $queryString, 2);

			$firstName = trim($queryParts[0]);
			$lastName = null;
			if (count($queryParts) > 1) {
				$lastName = trim($queryParts[1]);
			}

			$query = DB::table('profiles')
				->selectRaw('DISTINCT profiles.id')
				->where('profiles.type', $profileType)
				->join('profiles_meta', 'profiles.id', '=', 'profiles_meta.elm_id')
				->where(function ($q) use ($firstName, $lastName, $queryString){
					$q->where(function ($q) use ($firstName){
						$q->where('profiles_meta.elm_type', '=', ModelProfile::class)
							->where('profiles_meta.meta_key', '=', 'first_name')
							->where('profiles_meta.meta_value', 'LIKE', "%$firstName%");
					})
						->orWhere(function ($q) use ($lastName){
							if ($lastName != null) {
								$q->where('profiles_meta.meta_key', '=', 'last_name')
									->where('profiles_meta.meta_value', 'LIKE', "%$lastName%");
							}
						})
						->orWhere(function ($q) use ($queryString){
							$q->where('profiles_meta.meta_key', '=', 'last_name')
								->where('profiles_meta.meta_value', 'LIKE', "%$queryString%");
						})
						->orWhere(function ($q) use ($queryString){
							$q->where('profiles_meta.meta_key', '=', 'first_name')
								->where('profiles_meta.meta_value', 'LIKE', "%$queryString%");
						});
				});
		}

		if ($profileType == ModelProfile::PROVIDER_PROFILE_TYPE) {
			$query->where('profiles.is_searchable', true);
		}

		if (is_array($onlyProfileIds)) {
			$query->whereIn('profiles.id', $onlyProfileIds);
		}

		if ($limit != null) {
			$query->take($limit);
		}

		$result = $query->pluck('profiles.id');

		return $result;
	}

	public function getProviders()
	{
		return ModelProfile::where('type', ModelProfile::PROVIDER_PROFILE_TYPE)->get();
	}

	public function setProviderUnavailableTimeBeforeAppointment($providerId, $unavailableMinutes)
	{
		$table = 'provider_unavailable_time_option';
		$item = DB::table($table)->where('provider_id', $providerId)->first();
		if ($item == null) {
			$result = DB::table($table)->insert([
				'provider_id' => $providerId,
				'unavailable_minutes_before' => $unavailableMinutes
			]);
		} else {
			$result = DB::table($table)
				->where('provider_id', $providerId)
				->update([
						'unavailable_minutes_before' => $unavailableMinutes
					]
				);
		}

		return $result;
	}

	public function getProviderUnavailableTimeBeforeAppointment($providerId)
	{
		$table = 'provider_unavailable_time_option';
		$item = DB::table($table)->where('provider_id', $providerId)->first();

		return $item == null ? null : $item->unavailable_minutes_before;
	}

	public function findProfilesByMeta($metaKey, $exists = true, $metaValue = null)
	{
		$existsCondition = $exists ? 'profiles_meta.id IS NOT NULL' : 'profiles_meta.id IS NULL';
		$query = DB::table('profiles')->leftJoin('profiles_meta', function ($join) use ($metaKey){
			$join->on('profiles.id', '=', 'profiles_meta.elm_id')
				->where('profiles_meta.elm_type', ModelProfile::class)
				->where('profiles_meta.meta_key', $metaKey);
		})->select('profiles.*')->whereRaw($existsCondition);

		if($exists && $metaValue){
			$query->where('profiles_meta.meta_value', $metaValue);
		}

		return ModelProfile::hydrate($query->get());
	}
}

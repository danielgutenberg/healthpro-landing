<?php namespace WL\Modules\Profile\Observers;

use App\Tag;
use WL\Modules\Attachment\Facades\AttachmentService;
use WL\Modules\Profile\Models\ProviderProfile;

class ProviderProfileObserver {

	public function creating(ProviderProfile $model) {
		if ($model->ignoreEvents) {
			return true;
		}
	}

	public function created(ProviderProfile $model) {
		if ($model->ignoreEvents) {
			return true;
		}

	}

	public function save(ProviderProfile $model) {
		if ($model->ignoreEvents) {
			return true;
		}
	}

	public function saved(ProviderProfile $model) {
		if ($model->ignoreEvents) {
			return true;
		}

		$this->saveMeta($model);
	}

	/**
	 * 	We need to delete manually all morphy related tables ...
	 *
	 * @param ProviderProfile $model
	 */
	public function deleted(ProviderProfile $model) {

		/** Remove all attachments by current ProviderProfile .. */
		AttachmentService::removeEntityAttachments($model);

		/** Removal all attached tags by current ProviderProfile .. */
		#@todo use service ..
		#$model->detachTags();

		/** Delete all meta .. */
		$model->deleteAllMeta();
	}

	/**
	 * 	Save Meta ...
	 *
	 * @param ProviderProfile $model
	 * @return bool
	 */
	public function saveMeta(ProviderProfile $model) {
		if (! $model->hasFeeder() ) {
			return true;
		}

		return $model->syncMeta($model->getFeeder()->meta() ?: []);
	}

}

<?php namespace WL\Modules\Profile\Observers;

use WL\Modules\Profile\Models\ClientProfile;

class ClientProfileObserver {

	public function creating(ClientProfile $model) {
		if ($model->ignoreEvents) {
			return true;
		}
	}

	public function created(ClientProfile $model) {
		if ($model->ignoreEvents) {
			return true;
		}

	}

	public function save(ClientProfile $model) {
		if ($model->ignoreEvents) {
			return true;
		}
	}

	public function saved(ClientProfile $model) {
		if ($model->ignoreEvents) {
			return true;
		}

		$this->saveMeta($model);
	}

	/**
	 * 	We need to delete manually all morphy related tables ...
	 *
	 * @param ProviderProfile $model
	 */
	public function deleted(ClientProfile $model) {
		/** Delete all meta .. */
		$model->deleteAllMeta();
	}

	/**
	 * 	Save Meta ...
	 *
	 * @param ProviderProfile $model
	 * @return bool
	 */
	public function saveMeta(ClientProfile $model) {
		if (! $model->hasFeeder() ) {
			return true;
		}

		return $model->syncMeta($model->getFeeder()->meta() ?: []);
	}

}

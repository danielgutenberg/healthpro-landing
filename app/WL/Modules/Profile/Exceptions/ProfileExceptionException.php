<?php namespace WL\Modules\Profile\Exceptions;

use App\Exceptions\GenericException;

class ProfileException extends GenericException {}

class WrongProfileTypeException extends ProfileException {}
class MainProfileNotFoundException extends ProfileException {}
class ProfileSlugAlreadyExistsException extends  ProfileException {}
class ProfileSlugAlreadyChangedException extends  ProfileException {}
class ProfileCsvImportWrongColumnsNumberException extends ProfileException {}
class ProfileListImportInvalidClientDataException extends ProfileException {}
class ProfileClientImportAlreadyApprovedException extends ProfileException {}
class ProfileClientImportAlreadyDeclineException extends ProfileException {}
class ProfileClientImportAlreadyInvitedException extends ProfileException {}
class ProviderClientNotFound extends ProfileException {}

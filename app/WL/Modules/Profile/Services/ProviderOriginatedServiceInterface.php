<?php namespace WL\Modules\Profile\Services;


use Illuminate\Support\Collection;
use WL\Modules\Profile\Models\ProviderOriginated;

interface ProviderOriginatedServiceInterface
{
	/**
	 * @param int $providerId
	 * @return Collection
	 */
	public function getProviderOriginatedClients($providerId);

	/**
	 * @param int $providerId
	 * @return Collection
	 */
	public function getProviderClients($providerId);

	/**
	 * @param int $providerId
	 * @return Collection
	 */
	public function getProviderClientsInvites($providerId);

	/**
	 * @param int $providerId
	 * @param int $clientId
	 * @return boolean
	 */
	public function setProviderOriginatedClient($providerId, $clientId);

	/**
	 * @param int $providerId
	 * @param int $clientId
	 * @return ProviderOriginated
	 */
	public function reinviteClient($providerId, $clientId);

    /**
     * @param int $providerId
     * @param int $clientId
     * @return ProviderOriginated
     */
    public function inviteClient($providerId, $clientId);

	/**
	 * @param int $providerId
	 * @param int $clientId
	 * @return bool
	 */
	public function canInviteClient($providerId, $clientId);

	/**
	 * @param ProviderOriginated $model
	 * @return bool
	 */
	public function checkCanInvite(ProviderOriginated $model);

	/**
	 * @param int $clientId
	 * @return Collection
	 */
	public function getClientProviders($clientId, $deleted = true);
	/**
	 * @param int $clientId
	 * @return Collection
	 */
    public function getClientProviderInvites($clientId);

	/**
	 * @param int $providerId
	 * @param int $clientId
	 * @param boolean|true $approve
	 * @return boolean
	 */
	public function confirmProviderClient($providerId, $clientId, $approve=true);

	/**
	 * @param int $providerId
	 * @param int $clientId
	 * @return boolean
	 */
	public function addProviderClient($providerId, $clientId);

	/**
	 * @param int $providerId
	 * @param int $clientId
	 * @return ProviderOriginated
	 */
	public function getProviderClient($providerId, $clientId);

	/**
	 * @param int $clientId
	 * @return boolean
	 */
	public function approveOriginatorProvider($clientId);

	/**
	 * @param int $providerId
	 * @param int $clientId
	 * @return boolean
	 */
	public function isProviderClient($providerId, $clientId);

	/**
	 * Check if approved ot invited
	 * @param int $providerId
	 * @param int $clientId
	 * @param boolean Approve if invited
	 * @return boolean
	 */
	public function isApprovedOrInvited($providerId, $clientId, $force = false);

	/**
	 * @param int $providerId
	 * @param int $clientId
	 * @param array|string $array
	 * @param mixed|null $value
	 * @return array
	 */
	public function setMetaData($providerId, $clientId, $array, $value = null);

	/**
	 * @param int $providerId
	 * @param int $clientId
	 * @param array $fieldNames
	 * @return \stdClass
	 */
	public function getMetaData($providerId, $clientId, array $fieldNames = []);

	/**
	 * @param int $providerId
	 * @param int $clientId
	 * @param string $key
	 * @param boolean|null $force
	 * @return boolean
	 */
	public function toggleMeta($providerId, $clientId, $key, $force = null);
}

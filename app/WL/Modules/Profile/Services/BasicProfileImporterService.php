<?php namespace WL\Modules\Profile\Services;

use Illuminate\Support\Facades\Crypt;
use WL\Events\Facades\EventHelper;
use WL\Modules\Client\Events\ClientListRequestNotification;
use WL\Modules\Client\Facades\ClientService;
use WL\Modules\Client\Models\ClientImportRecord;
use WL\Modules\Helpers\Facades\EventInfoBuilder;
use WL\Modules\Profile\Events\ProfileHasBeenAlreadyRegisteredEmailEvent;
use WL\Modules\Profile\Events\ProfileHasBeenRegisteredEmailEvent;
use WL\Modules\Profile\Exceptions\ProfileClientImportAlreadyApprovedException;
use WL\Modules\Profile\Exceptions\ProfileClientImportAlreadyDeclineException;
use WL\Modules\Profile\Exceptions\ProfileClientImportAlreadyInvitedException;
use WL\Modules\Profile\Exceptions\ProfileListImportInvalidClientDataException;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Facades\ProviderOriginatedService as ProviderInviteService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Repositories\ProviderOriginatedRepositoryInterface;
use WL\Modules\User\Facades\User;

class BasicProfileImporterService implements ProfileImporterInterface
{
	protected $originatedRepository;

	public function __construct(ProviderOriginatedRepositoryInterface $repository)
	{
		$this->originatedRepository = $repository;
	}

	public function importClient($providerProfileId, ClientImportRecord $clientImport, $sendEmailsToClients = true, $providerCustomEmailBody = null, $couponDiscount = null)
	{
		$response = [
			'user' => User::getUserByEmail($clientImport->email),
			'profile' => null,
		];

		if (!$response['user']) {
			// User does not exist - create user and send registration email to newly created client user and return response data
			$profile = ClientService::createClient($clientImport->email, $clientImport->firstName, $clientImport->lastName, $clientImport->phone);
			if(is_null($profile)){
				throw new ProfileListImportInvalidClientDataException();
			}
			ProviderInviteService::setProviderOriginatedClient($providerProfileId, $profile->id);
			$response = [
				'user' => ProfileService::getOwner($profile->id),
				'profile' => ProfileService::loadProfileBasicDetailsModel($profile),
			];
			if($sendEmailsToClients){
				$this->sendInvite($profile->id, $providerProfileId, true, $providerCustomEmailBody, $couponDiscount);
			}

			return (object) $response;
		}

		$profile = ProfileService::getUserProfiles($response['user']->id, ModelProfile::CLIENT_PROFILE_TYPE)->first();
		if (is_null($profile)) {
			// user does not have a client profile - create
			$profile = ClientService::createClientProfile($response['user']->id, $clientImport->firstName, $clientImport->lastName, $clientImport->phone);
			if(is_null($profile)){
				throw new ProfileListImportInvalidClientDataException();
			}
		}

		$response['profile'] = ProfileService::loadProfileBasicDetailsModel($profile);
		$invite = ProviderInviteService::getProviderClient($providerProfileId, $profile->id);
		if($invite->exists){
			if(is_null($invite->approved)){
				if(!ProviderInviteService::canInviteClient($providerProfileId, $profile->id)){
					throw new ProfileClientImportAlreadyInvitedException('Client already invited');
				}
			} elseif($invite->approved){
				throw new ProfileClientImportAlreadyApprovedException('Client already in your client list');
			} else {
				throw new ProfileClientImportAlreadyDeclineException('Client declined your invitation already');
			}
		}
		// Client was never invited by this professional - send invitation email and return response data
		ProviderInviteService::setProviderOriginatedClient($providerProfileId, $profile->id);
		if($sendEmailsToClients) {
			$this->sendInvite($profile->id, $providerProfileId, false, $providerCustomEmailBody, $couponDiscount);
		}
		return (object) $response;
	}

	public function inviteClient($clientProfileId, $providerProfileId, $sendEmailsToClients = true, $providerCustomEmailBody = null, $couponDiscount = null)
	{
		$client = ProfileService::getProfileById($clientProfileId);
		$provider = ProfileService::getProfileById($providerProfileId);


		$response = [
			'user' => ProfileService::getOwner($client->id),
			'profile' => ProfileService::loadProfileBasicDetailsModel($client),
		];

		$invite = ProviderInviteService::getProviderClient($provider->id, $client->id);
		if($invite->exists){
			if(is_null($invite->approved)){
				if(!ProviderInviteService::canInviteClient($provider->id, $client->id)){
					throw new ProfileClientImportAlreadyInvitedException('Client already invited');
				}
			} elseif($invite->approved){
				throw new ProfileClientImportAlreadyApprovedException('Client already in your client list');
			} else {
				throw new ProfileClientImportAlreadyDeclineException('Client declined your invitation already');
			}
		}

		// Client was never invited by this professional - send invitation email and return response data
		ProviderInviteService::setProviderOriginatedClient($provider->id, $client->id);
		if($sendEmailsToClients) {
			$this->sendInvite($client->id, $provider->id, false, $providerCustomEmailBody, $couponDiscount);
		}
		return (object) $response;
	}

	public function sendInvite($clientProfileId, $providerProfileId, $newClient = true, $providerCustomEmailBody = null, $couponDiscount = null)
	{
		$emailData = EventInfoBuilder::collectClientInvite($clientProfileId, $providerProfileId, $providerCustomEmailBody, $couponDiscount, $newClient);

		if($newClient){
			EventHelper::fire(new ProfileHasBeenRegisteredEmailEvent($providerProfileId, $clientProfileId, $emailData));
		} else {
			$profile = ProfileService::getProfileById($clientProfileId);
			EventHelper::fire(new ProfileHasBeenAlreadyRegisteredEmailEvent($providerProfileId, $clientProfileId, $emailData));
			EventHelper::fire(new ClientListRequestNotification($profile, EventInfoBuilder::collectProfileInfo($providerProfileId, 'provider')));
		}
	}
}


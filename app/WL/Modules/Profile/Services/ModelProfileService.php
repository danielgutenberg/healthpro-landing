<?php namespace WL\Modules\Profile\Services;

use Carbon\Carbon;
use WL\Models\EmailAddress;
use WL\Modules\Profile\Models\ModelProfile;

interface ModelProfileService
{
	/**
	 * Add location to profile
	 * @param $locationId number
	 * @param $profileId number
	 * @return boolean
	 */
	public function addLocation($locationId, $profileId);

	/**
	 * Remove location from profile
	 * @param $locationId number
	 * @param $profileId number
	 * @return boolean
	 */
	public function removeLocation($locationId, $profileId);

	/**
	 * @return ProfileAssetService
	 */
	public function assetsService();

	/**
	 * @return ProfileProfessionalBodyService
	 */
	public function professionalBodyService();

	/**
	 * @return ProfileEducationService
	 */
	public function educationService();

	/**
	 * @return ProfileJobService
	 */
	public function jobService();

	/**
	 * Get owner for profile
	 * @param $profileId
	 * @return mixed|null
	 */
	public function getOwner($profileId);

	/**
	 * Get profile owner email address
	 * @param $profileId
	 * @return string|null
	 */
	public function getProfileEmail($profileId);

    /**
     * @param int $profileId
     * @return EmailAddress
     */
    public function getNameAndEmail($profileId);

    /**
     * @param int $profileId
     * @return EmailAddress
     */
    public function getProfessionalEmail($profileId);

	/**
	 * Create profile
	 * @param $firstName
	 * @param $lastName
	 * @return ModelProfile|null
	 */
	public function createClient($firstName, $lastName);


	/**
	 * Create and Associate profile with User
	 *
	 * @param $userId
	 * @param $profileType
	 * @param $firstName
	 * @param $lastName
	 * @return mixed
     */
	public function createAndAssociateProfile($userId, $profileType, $firstName, $lastName);

	/**
	 * Create provider profile .
	 *
	 * @param $firstName
	 * @param $lastName
	 * @return mixed
	 */
	public function createProvider($firstName, $lastName);

	/**
	 * Create staff profile .
	 *
	 * @param $firstName
	 * @param $lastName
	 * @return mixed
	 */
	public function createStaff($firstName, $lastName);

	/**
	 * Get profile by id
	 * @param $profileId
	 * @return mixed
	 */
	public function getProfileById($profileId);

	/**
	 * @param $slug
	 * @return mixed
	 */
	public function getProfileBySlug($slug);

	/**
	 * Get Profile by Id or Slug, both are unique identifier of profile
	 * @param type $key
	 * @return type
	 */
	public function getProfileByIdOrSlug($key);

	/**
	 * Get user profiles
	 * @param $userId
	 * @return mixed
	 */
	public function getUserProfiles($userId);

	/**
	 * Get current profile id
	 * @return mixed
	 */
	public function getCurrentProfileId();

	/**
	 * Set current profile id
	 * @param $profileId
	 * @return mixed
	 */
	public function setCurrentProfileId($profileId);

	/**
	 * Invalidate current profile id
	 * @return mixed
	 */
	public function invalidateCurrentProfileId();


	/**
	 * Get current profile
	 * @return ModelProfile|null
	 */
	public function getCurrentProfile();


	/**
	 * Associate uploaded File to entity
	 *
	 * @param $file
	 * @param $type
	 * @param $profileId
	 * @param bool $deleteExists
	 * @return mixed
	 */
	public function saveUploadedFile($file, $type, $profileId, $deleteExists = false);

	/**
	 * Searches file
	 *
	 * @param $entity
	 * @param $type
	 * @param $size
	 * @return mixed
	 */
	public function loadUploadedFileUrl($entity, $type, $size = null);


	/**
	 * Load urls and ids of attachments
	 *
	 * @param $entity
	 * @param $type
	 * @param null $size
	 * @return mixed
	 */
	public function loadUploadedFilesUrl($entity, $type, $size = null);

	/**
	 * Save field as Meta
	 *
	 * @param $profileId
	 * @param $fieldsData
	 * @param $fieldNames
	 * @param bool $convertNullToEmpty
	 * @return mixed
	 */
	public function storeProfileMetaFields($profileId, $fieldsData, $fieldNames, $convertNullToEmpty = false);

	/**
	 * @param int $profileId
	 * @param string $fieldName
	 * @param string $fieldValue
     * @param bool|true $trigger
	 * @return boolean
	 */
	public function storeProfileMetaField($profileId, $fieldName, $fieldValue, $trigger = true);

	/**
	 * @param int $profileId
	 * @param string $fieldName
	 * @param string $fieldValue
	 * @return boolean
	 */
	public function storeProfileMetaFieldHashed($profileId, $fieldName, $fieldValue);

	/**
	 * Saving ModelProfile and generates Slug
	 * @param $profileId
	 * @param $updateSlug
	 * @return mixed
	 */
	public function updateProfile($profileId, $updateSlug);


	/**
	 * Check when slug is not duplicated and not manually changed
	 *
	 * @param $profileId
	 * @param $newSlug
	 * @return mixed
	 */
	public function checkProfileSlug($profileId, $newSlug);

	/**
	 * Update profile slug and check if it unique and not changed before
	 *
	 * @param $profileId
	 * @param $newSlug
	 * @return mixed
	 */
	public function updateProfileSlug($profileId, $newSlug, $sluggify = false);

	/**
	 * Associate multiple tag fields with entity
	 *
	 * @param $profileId
	 * @param $data
	 * @param $tagFields
	 * @return mixed
	 */
	public function associateTagHelper($profileId, $data, $tagFields);

	/**
	 * Load profile associated tags
	 * @param $profileId
	 * @param $tagFields
	 * @return mixed
	 */
	public function loadProfileTags($profileId, $tagFields);

	/**
	 * Load meta fields and profile fields in stdClass
	 *
	 * @param $profileId
	 * @return mixed
	 */
	public function loadProfileBasicDetailsModel($profileId);

	/**
	 * @param int $profileId
	 * @param string $fieldName
	 * @param string|null $default
	 * @return mixed
	 */
	public function loadProfileMetaField($profileId, $fieldName, $default = null);

	/**
	 * @param int $profileId
	 * @param string $fieldNames
	 * @param mixed $default
	 * @return mixed
	 */
	public function loadProfileMetaFieldHashed($profileId, $fieldNames, $default = null);

	/**
	 * Load meta fields associated with profile in stdClass
	 *
	 * @param $profileId
	 * @param $fieldNames
	 * @param array $defaults
	 * @return mixed
	 */
	public function loadProfileMetaFields($profileId, $fieldNames, $defaults = []);

	/**
	 * Return social accounts associated with profile
	 *
	 * @param $profileId
	 * @return mixed
	 */
	public function loadSocialAccounts($profileId);

	/**
	 * Load basic profile information like first, last names, avatar
	 * @param $profileId
	 * @return mixed
	 */
	public function loadProfileBasicDetails($profileId);

	/**
	 * Load basic profile information like first, last names, avatar for all user profiles
	 * @param $userId
	 * @return array
	 */
	public function loadProfilesBasicDetails($userId);

	/**
	 * Load profiles with details for the given profile ids
	 *
	 * @param array $profileIds
	 * @return mixed
	 */
	public function loadProfiles(array $profileIds = []);

	/**
	 * Storing social accounts
	 *
	 * @param $profileId
	 * @param $inpData
	 * @return mixed
	 */
	public function storeSocialAccounts($profileId, $inpData);


	/**
	 * @param $profileId
	 * @param $fieldsData
	 * @param $fieldNames
	 * @return mixed
	 */
	public function storeProfileMetaFieldsWithRegex($profileId, $fieldsData, $fieldNames);

	/**
	 * @param int $profileId
	 * @return bool
	 */
	public function isSearchable($profileId);

    /**
     * @param int $profileId
     * @return bool
     */
    public function isBookable($profileId);

	/**
	 * Set is_searchable field of ModelProfile
	 *
	 * @param $profileId
	 * @param $searchable
	 * @return mixed
     */
	public function setIsSearchable($profileId, $searchable);

    /**
     * @param int $profileId
     * @param bool $bookable
     * @return int
     */
    public function setIsBookable($profileId, $bookable);

    /**
     * @param int $profileId
     * @param string $status
     * @return int
     */
    public function setProfileStstus($profileId, $status);

	/**
	 * Check if the user has given profile type
	 *
	 * @param $userId
	 * @param $profileType
	 * @return bool
	 */
	public function hasProfile($userId, $profileType);

	/**
	 * @param int $profileId
	 * @return mixed
	 */
	public function acceptedTerms($profileId);

	/**
	 * Search for provider by name or email
	 * @param $query
	 * @param bool $isEmail
	 * @param null $onlyProviderIds
	 * @param $limit
	 * @return mixed
	 */
	public function searchForProviders($query, $isEmail = true, $onlyProviderIds, $limit);

	/**
	 * Search for provider by name or email
	 * @param $query
	 * @param bool $isEmail
	 * @param null $onlyProviderIds
	 * @param $limit
	 * @return mixed
	 */
	public function searchForClients($query, $isEmail = true, $onlyProviderIds, $limit);

	/**
	 * @param $profileId
	 * @return mixed
     */
	public function getCommissionFreePeriod($profileId);

	/**
	 * @param $profileId
	 * @return mixed
     */
	public function getCommissionFreePeriodEndDate($profileId);

	/**
	 * returns providers profiles
	 *
	 * @return mixed
	 */
	public function getAllProviders();

	/**
	 * send notifications to providers ready to enhance
	 *
	 * @return mixed
	 */
	public function sendProvidersReadyForEnhanceNotification();

	/**
	 * send notifications to providers which has an pending appointment today
	 * @param Carbon $date
	 * @return mixed
	 */
	public function sendProviderHasTodayPendingAppointmentNotifications(Carbon $date);

	/**
	 * @param $providerId
	 * @param $unavailableTimeMinutes
	 * @return mixed
     */
	public function setProviderUnavailableTimeBeforeAppointment($providerId, $unavailableTimeMinutes);

	/**
	 * @param $providerId
	 * @return mixed
     */
	public function getProviderUnavailableTimeBeforeAppointment($providerId);

	/**
	 * @param $providerId
	 * @return mixed
	 */
	public function setProfileBloggerStatus($providerId, $status);

	/**
	 * @param $providerId
	 * @return mixed
	 */
	public function setProfilePhoneVerified($providerId);

    /**
     * @param int $viewerId
     * @param int $viewedId
     * @return bool
     */
    public function profileIsVisible($viewerId, $viewedId);

    /**
     * @param $profileId
     * @param $status
     * @return mixed
     */
    public function setProfileStatus($profileId, $status);

    /**
     * @param int $profileId
     * @return string
     */
    public function getProfileDisplayName($profileId);
}

<?php namespace WL\Modules\Profile\Services;

use WL\Modules\Profile\Exceptions\WrongProfileTypeException;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Repositories\DbProviderOriginatedRepository;
use WL\Modules\Profile\Repositories\ProviderProfileRepository;

class ProviderProfileServiceImpl implements ProviderProfileService
{
	private $providerProfileRepository;
	private $originatedService;

	function __construct(ProviderProfileRepository $providerProfileRepository)
	{
		$this->providerProfileRepository = $providerProfileRepository;
		$this->originatedService = new ProviderOriginatedService(new DbProviderOriginatedRepository());
	}

	public function isProfessionalsClient(ModelProfile $professionalProfile, ModelProfile $clientProfile)
	{
		if (!$professionalProfile || $professionalProfile->type != ModelProfile::PROVIDER_PROFILE_TYPE)
			throw new WrongProfileTypeException;

		if (!$clientProfile || $clientProfile->type != ModelProfile::CLIENT_PROFILE_TYPE)
			throw new WrongProfileTypeException;

		return $this->originatedService->isProviderClient($professionalProfile->id, $clientProfile->id);
	}
}

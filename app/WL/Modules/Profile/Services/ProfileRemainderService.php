<?php namespace WL\Modules\Profile\Services;

use WL\Modules\Profile\Models\ProfileReminder;
use WL\Modules\Profile\Models\ProfileReminderSent;
use WL\Modules\Profile\Repositories\ProfileReminderRepositoryInterface;
use WL\Modules\Profile\Repositories\ProfileReminderSentRepositoryInterface;
use WL\Yaml\Facades\Yaml;

class ProfileReminderService implements ProfileReminderServiceInterface
{
	protected $repository;
	protected $sentRepository;

	public function __construct(ProfileReminderRepositoryInterface $repository, ProfileReminderSentRepositoryInterface $sentRepository)
	{
		$this->repository = $repository;
		$this->sentRepository = $sentRepository;
	}

	public function create($profileId, $value, $type, $aboutEntityType)
	{
		$item = new ProfileReminder(
			[
				'profile_id' => $profileId,
				'type' => $type,
				'about_entity_type' => $aboutEntityType,
				'value' => $value
			]
		);

		return $this->repository->save($item) ? $item : null;
	}

	public function createDefaultProfileReminders($profileId)
	{
		$configItems = Yaml::get('defaults', 'wl.profile.reminders');
		$result = [];
		foreach ($configItems as $configItem) {
			$result[] = $this->create($profileId, $configItem['value'], $configItem['type'], $configItem['about_entity_type']);
		}

		return $result;
	}

	public function update($profileReminderId, $value)
	{
		// $value = null disables reminder
		if(!is_numeric($value) || $value <= 0)
			$value = null;
		return $this->repository->updateProfileReminderValue($profileReminderId, $value);
	}

	public function getProfileReminders($profileId)
	{
		$items = $this->repository->getProfileReminders($profileId);

		return $items;
	}

	public function setProfileReminderSent($profileReminderId, $aboutEntityId, $aboutEntityType)
	{
		$item = new ProfileReminderSent([
			'profile_reminder_id' => $profileReminderId,
			'about_entity_id' => $aboutEntityId,
			'about_entity_type' => $aboutEntityType
		]);

		return $this->sentRepository->save($item);
	}

	public function getProfileReminderSentDateTime($profileReminderId, $aboutEntityId, $aboutEntityType)
	{
		return $this->sentRepository->getProfileReminderSentDateTime($profileReminderId, $aboutEntityId, $aboutEntityType);
	}

}

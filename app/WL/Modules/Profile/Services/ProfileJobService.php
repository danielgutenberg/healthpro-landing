<?php namespace WL\Modules\Profile\Services;

/**
 * Interface ProfileJobService
 * @package WL\Modules\Profile
 */
interface ProfileJobService
{
	/**
	 * @param $profileId
	 * @param array $jobs
	 * @return mixed
	 */
	public function explicitSetProfileJobs($profileId, array $jobs);

	/**
	 * @param $profileId
	 * @return mixed
	 */
	public function removeAllProfileJobs($profileId);

	/**
	 * @param $profileId
	 * @return mixed
	 */
	public function getProfileJobs($profileId);

	/**
	 * @param $profileId
	 * @param $job
	 * @return mixed
	 */
	public function addProfileJob($profileId, $job);

	/**
	 * @param $jobId
	 * @return mixed
	 */
	public function removeJob($jobId);

}

<?php namespace WL\Modules\Profile\Services;

use WL\Modules\Profile\Exceptions\WrongProfileTypeException;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Repositories\ClientProfileRepository;

class ClientProfileServiceImpl implements ClientProfileService
{
	private $clientProfileRepository;

	function __construct(ClientProfileRepository $clientProfileRepository)
	{
		$this->clientProfileRepository = $clientProfileRepository;
	}

	public function getProviders(ModelProfile $profile)
	{
		if (!$profile || $profile->type != ModelProfile::CLIENT_PROFILE_TYPE)
			throw new WrongProfileTypeException;

		return $this->clientProfileRepository->getProviders($profile->id);
	}
}

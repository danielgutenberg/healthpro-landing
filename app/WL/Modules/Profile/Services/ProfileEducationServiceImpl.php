<?php namespace WL\Modules\Profile\Services;

use WL\Modules\Education\Models\Education;
use Carbon\Carbon;
use WL\Modules\Profile\Models\ProfileEducation;
use WL\Modules\Profile\Repositories\ProfileEducationInterface;

class ProfileEductionServiceImpl implements ProfileEducationService
{
	public function __construct(ProfileEducationInterface $repository)
	{
		$this->repo = $repository;
	}

	public function explicitSetProfileEducations($profileId, array $profileEducations)
	{
		$this->removeAllProfileEducations($profileId);
		foreach ($profileEducations as $edu) {
			$this->addProfileEducation($profileId, $edu);
		}

	}

	public function removeAllProfileEducations($profileId)
	{
		return $this->repo->removeProfileEducations($profileId);
	}

	public function getProfileEducations($profileId)
	{
		return $this->repo->getProfileEducation($profileId);
	}

	public function addProfileEducation($profileId, $profileEducation)
	{
		$education = new ProfileEducation();
		$validFrom = false;
		if (array_key_exists('from', $profileEducation) && !!$profileEducation['from']['month'] && !!$profileEducation['from']['year']) {
			$validFrom = true;
			$from = $profileEducation['from']['month'] . '/1/' . $profileEducation['from']['year'];
		}
		$validTo = false;
		if (array_key_exists('to', $profileEducation) && !!$profileEducation['to']['month'] && !!$profileEducation['to']['year']) {
			$validTo = true;
			$to = $profileEducation['to']['month'] . '/1/' . $profileEducation['to']['year'];
		}
		if (empty($profileEducation['education_id'])) {
			if (!empty($profileEducation['education_name'])) {
				$newEducation = new Education();
				$newEducation->fill([
					'name' => $profileEducation['education_name'],
					'rank' => (isset($profileEducation['education_rank']) ? $profileEducation['education_rank'] : 0),
					'is_custom' => (isset($profileEducation['education_custom']) ? ($profileEducation['education_custom'] == false ? 0 : 1) : 1)
				]);
				if ($newEducation->saveOrReturn()) {
					$profileEducation['education_id'] = $newEducation->id;
				} else {
					return false;
				}

			} else {
				return false;
			}
		}

		$education->education_id = $profileEducation['education_id'];
		$education->from = $validFrom ? Carbon::parse($from) : null;
		$education->to = $validTo ? Carbon::parse($to) : null;
		$education->profile_id = $profileId;
		$education->degree = isset($profileEducation['degree']) ? $profileEducation['degree'] : '';

		return $this->repo->saveEducation($education);
	}

	public function removeEducation($educationId)
	{
		return $this->repo->destroy($educationId);
	}
}

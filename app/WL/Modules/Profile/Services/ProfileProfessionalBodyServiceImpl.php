<?php namespace WL\Modules\Profile\Services;

use Carbon\Carbon;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Models\ProfileProfessionalBody;
use WL\Modules\Profile\Repositories\ProfileProfessionalBodyInterface;

class ProfileProfessionalBodyServiceImpl implements ProfileProfessionalBodyService
{

	function __construct(ProfileProfessionalBodyInterface $professionalBody)
	{
		$this->professionalBody = $professionalBody;
	}

	public function removeProfileProfessions($profileId)
	{
		$this->professionalBody->removeAllProfileItems($profileId);
	}

	public function storeProfileProfessionalBody($profileId, $professionalBodyItems)
	{

		$this->removeProfileProfessions($profileId);

		if(!empty($professionalBodyItems)){
			foreach ($professionalBodyItems as $data) {

				$item = new ProfileProfessionalBody();
				$item->title = $data['title'];

				if(isset($data['description']))
					$item->description = $data['description'];

				$item->profile_id = $profileId;
				if (isset($data['expiry_date']) && isset($data['expiry_date']['year']) && isset($data['expiry_date']['month'])) {
					$item->expiry_date = Carbon::createFromDate($data['expiry_date']['year'], $data['expiry_date']['month'])->endOfMonth();
				}

				$this->professionalBody->save($item);

			}
		}
	}

	public function loadProfileProfessionalBody($profileId)
	{
		return $this->professionalBody->getUnexpiredItems($profileId);
	}


}

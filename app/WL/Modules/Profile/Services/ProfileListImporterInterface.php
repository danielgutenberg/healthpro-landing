<?php namespace WL\Modules\Profile\Services;

use WL\Modules\Client\Models\ClientImportRecord;

interface ProfileListImporterInterface
{
    /**
     * @param int $providerProfileId
     * @param ClientImportRecord[] $clientImports
     * @param bool|true $sendEmailsToClients
     * @param string|null $customEmailBody
	 * @param string|null $couponDiscount
     * @return array
     */
    public function importClients($providerProfileId, $clientImports, $sendEmailsToClients = true, $customEmailBody = null, $couponDiscount = null);

	/**
	 * @param int $providerProfileId
	 * @param array $data
	 * @param bool|true $sendEmailsToClients
	 * @param string|null $customEmailBody
	 * @param string|null $couponDiscount
	 * @return array
	 */
	public function importClientsRaw($providerProfileId, $data, $sendEmailsToClients = true, $customEmailBody = null, $couponDiscount = null);
}

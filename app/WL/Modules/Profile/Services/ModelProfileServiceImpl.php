<?php namespace WL\Modules\Profile\Services;

use Carbon\Carbon;
use Creativeorange\Gravatar\Facades\Gravatar;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Event;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use WL\Events\Facades\EventHelper;
use WL\Models\EmailAddress;
use WL\Modules\Appointment\Facades\AppointmentService;
use WL\Modules\Attachment\Exceptions\AttachmentUploadException;
use WL\Modules\Attachment\Facades\AttachmentService;
use WL\Modules\HasOffers\Events\IsSearchableEvent;
use WL\Modules\Helpers\Facades\EventInfoBuilder;
use WL\Modules\Payment\Facades\PaymentConfig;
use WL\Modules\Payment\Facades\PaymentService;
use WL\Modules\Payment\Models\PaymentMethod;
use WL\Modules\Profile\Events\AvatarAddedEvent;
use WL\Modules\Profile\Events\ProfileDetailsChangeEvent;
use WL\Modules\Profile\Events\ProfileMetaChangeEvent;
use WL\Modules\Profile\Events\ProviderProfileCompletedNotification;
use WL\Modules\Profile\Events\TermsAccepted;
use WL\Modules\Profile\Exceptions\ProfileException;
use WL\Modules\Profile\Exceptions\ProfileSlugAlreadyChangedException;
use WL\Modules\Profile\Exceptions\ProfileSlugAlreadyExistsException;
use WL\Modules\Profile\Exceptions\WrongProfileTypeException;
use WL\Modules\Profile\Facades\ProfileReminderService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Models\ProviderProfile;
use WL\Modules\Profile\Repositories\DbProfileAssetsRepository;
use WL\Modules\Profile\Repositories\DbProfileEducationRepository;
use WL\Modules\Profile\Repositories\DbProfileJobRepository;
use WL\Modules\Profile\Repositories\DbProfileProfessionalBodyRepository;
use WL\Modules\Profile\Repositories\DbProviderProfileRepository;
use WL\Modules\Profile\Repositories\MetaRepository;
use WL\Modules\Profile\Repositories\ModelProfileRepository;
use WL\Modules\Provider\Events\IsSearchableChangeEvent;
use WL\Modules\Provider\Events\LocationAdded;
use WL\Modules\Provider\Events\LocationRemoved;
use WL\Modules\Provider\Events\ProgressUpdate;
use WL\Modules\Provider\Events\ProviderCreateEvent;
use WL\Modules\Provider\Events\ProviderPendingAppointmentNotification;
use WL\Modules\Provider\Events\ProviderProfileEnhanceNotification;
use WL\Modules\Provider\Events\SocialAccountChangeEvent;
use WL\Modules\Provider\Facades\ProviderMembershipService;
use WL\Modules\Provider\Facades\ProviderService;
use WL\Modules\Provider\Models\ProviderService as ProviderServiceEntity;
use WL\Modules\Provider\ValueObjects\ProviderFeatures;
use WL\Modules\Rating\Models\Rating;
use WL\Modules\Review\Facade\ReviewService;
use WL\Modules\Slug\Facade\SlugService;
use WL\Modules\Taxonomy\Facades\TagService;
use WL\Modules\Taxonomy\Facades\TaxonomyService;
use WL\Modules\User\Events\BecameSearchableEvent;
use WL\Modules\User\Facades\User;
use WL\Yaml\Contracts\Parsable;
use WL\Yaml\Facades\Yaml;

class ModelProfileServiceImpl implements ModelProfileService
{
	private $modelProfileRepository;
	private $assetsService;
	private $professionalBodyService;
	private $educationService;
	private $providerService;
	protected $metaRepository;

	/**
	 * @var Parsable
	 */
	private $parser;

	function __construct(ModelProfileRepository $modelProfileRepository, Parsable $parser)
	{
		$this->modelProfileRepository = $modelProfileRepository;
		$this->parser = $parser;

		$this->professionalBodyService = new ProfileProfessionalBodyServiceImpl(new DbProfileProfessionalBodyRepository());
		$this->assetsService = new ProfileAssetServiceImpl(new DbProfileAssetsRepository());
		$this->educationService = new ProfileEductionServiceImpl(new DbProfileEducationRepository());
		$this->jobService = new ProfileJobServiceImpl(new DbProfileJobRepository());

		$this->providerService = new ProviderProfileServiceImpl(new DbProviderProfileRepository());
		$this->metaRepository = new MetaRepository(ModelProfile::class);
	}

	//TODO must be refactored to just methods or separate service facades
	public function providerService()
	{
		return $this->providerService;
	}

	public function assetsService()
	{
		return $this->assetsService;
	}

	public function professionalBodyService()
	{
		return $this->professionalBodyService;
	}

	public function jobService()
	{
		return $this->jobService;
	}

	public function educationService()
	{
		return $this->educationService;
	}

	//=====END TODO

	public function getOwner($profileId)
	{
		return $this->modelProfileRepository->getProfileOwnerByProfileId($profileId);
	}

	public function getProfileEmail($profileId)
	{
		$user = $this->getOwner($profileId);
		if ($user) {
			return $user->email;
		}
	}

    public function getNameAndEmail($profileId)
    {
        $email = $this->getProfileEmail($profileId);
        $name = $this->getProfileDisplayName($profileId);;
        return new EmailAddress($email, $name);
	}

    public function getProfessionalEmail($profileId)
    {
        $domain = Yaml::get('expert_domain', 'wl.settings.app');
        $profile = $this->getProfileById($profileId);
        if($profile->type !== ModelProfile::PROVIDER_PROFILE_TYPE || !$profile->slug) {
            return null;
        }
        $email = $profile->slug . '@' . $domain;
        $name = $this->getProfileDisplayName($profileId);
        return new EmailAddress($email, $name);
    }

	private function createProfile($attributes, $firstName, $lastName, $save = true)
	{
		$profile = $this->modelProfileRepository->createProfile($save, $attributes);
		$profile->saveMeta('first_name', $firstName);
		$profile->saveMeta('last_name', $lastName);
		ProfileReminderService::createDefaultProfileReminders($profile->id);

		return $profile;
	}

	/**
	 * Create staff profile .
	 *
	 * @param $firstName
	 * @param $lastName
	 * @return mixed|void
	 */
	public function createStaff($firstName, $lastName)
	{
		$profile = $this->createProfile(['type' => ModelProfile::STAFF_PROFILE_TYPE], $firstName, $lastName);

		Event::fire('profile.created', $profile);

		return $profile;
	}

	/**
	 * Create provider profile .
	 *
	 * @param $firstName
	 * @param $lastName
	 * @return mixed|void
	 */
	public function createProvider($firstName, $lastName)
	{
		$profile = $this->createProfile(['type' => ModelProfile::PROVIDER_PROFILE_TYPE], $firstName, $lastName);

		$this->storeDefaultMetaFields($profile->id, ModelProfile::PROVIDER_PROFILE_TYPE);

		if (session()->has('is_blogger')) {
			$this->setProfileBloggerStatus($profile->id, true);
		}

		$provider = $profile
			->typeInstance();

		$provider
			->saveSlug(
				implode(' ', [$firstName, $lastName])
			);


		Event::fire('profile.created', $profile);

		return $provider;
	}

	private function storeDefaultMetaFields($profileId, $profileType)
	{
		$metaFields = [];

		if($profileType === ModelProfile::PROVIDER_PROFILE_TYPE) {
			$metaFields = [
				'accepted_terms_conditions' => Carbon::now()->format('Y-m-d'),
				'accepted_payment_methods' => PaymentMethod::ONLY_IN_PERSON,
			];
		}

		if(!empty($metaFields)) {
			foreach ($metaFields as $key => $value) {
				$this->setProfileMetaField($profileId, $key, $value);
			}
		}
	}

	/**
	 * Create client profile ..
	 *
	 * @param $firstName
	 * @param $lastName
	 * @return mixed
	 */
	public function createClient($firstName, $lastName)
	{
		$profile = $this->createProfile(['type' => ModelProfile::CLIENT_PROFILE_TYPE], $firstName, $lastName);

		Event::fire('profile.created', $profile);

		return $profile;
	}

	public function createAndAssociateProfile($userId, $profileType, $firstName, $lastName)
	{
		$profile = null;

		if ($this->hasProfile($userId, $profileType)) {
			return null;
		}

		$user = User::getUserById($userId);

		switch ($profileType) {

			case ModelProfile::CLIENT_PROFILE_TYPE:
				$profile = $this->createClient($firstName, $lastName);
				break;

			case ModelProfile::PROVIDER_PROFILE_TYPE:
				$profile = $this->createProvider($firstName, $lastName);
				break;
		}

		if ($profile) {
			$user->profiles()
				->attach($profile, ['is_owner' => true]);

			PaymentService::createPaymentAccounts($profile->id);
			$this->generateProfileAvatar($profile);
			User::generateApiKeys($user->id, $profile->id);

			if($profile->type == ModelProfile::PROVIDER_PROFILE_TYPE) {
                event(new ProviderCreateEvent($profile->id));
            }
		}

		return $profile;
	}

	public function generateProfileAvatar(ModelProfile $profile, $force = false)
	{
		$profile = ModelProfile::typeCast($profile);
		$oldAvatar = AttachmentService::findFile('avatar', $profile, false);

		if ($oldAvatar && !$force) {
			return null;
		}

		$user = $this->getOwner($profile->id);

		$avatar = null;
		$imgUrl = Gravatar::exists($user->email) ? Gravatar::get($user->email) : '';

		if ($imgUrl) {
			$avatar = $this->setProfileAvatarFromUrl($profile, $imgUrl);
		}

		if ($avatar && $oldAvatar) {
			$oldAvatar->delete();
		}

		return $avatar;
	}

	public function setProfileAvatarFromUrl(ModelProfile $profile, $imgUrl)
	{
		$avatar = null;

		$tmpFile = tempnam(sys_get_temp_dir(), 'urlavatar-');

		$handle = @fopen($imgUrl, 'rb');

		if ($handle) {
			$readed = file_put_contents($tmpFile, $handle);

			if ($readed > 0) {
				$fileMime = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $tmpFile);


				$file = new UploadedFile($tmpFile, 'avatar.jpg', $fileMime);

				try {
					$avatar = AttachmentService::uploadImage('avatar', $profile, $file);
				} catch (AttachmentUploadException $ex) {

				}

				unlink($tmpFile);
			}

			fclose($handle);
		}

		return $avatar;
	}

	public function setCurrentProfileId($profileId)
	{
		// This will throw ModelNotFoundException if profile with this id doesn't exists.
		$profile = $this->getProfileById($profileId);
		$result = session(['profile_id' => $profileId]);

		Event::fire('profile.changed', $profile);

		return $result;
	}

	public function getIsProvider($profileId)
	{
		$profile = $this->getProfileById($profileId);

		return $profile ? $profile->type == ModelProfile::PROVIDER_PROFILE_TYPE : false;
	}

	public function invalidateCurrentProfileId()
	{
		session(['profile_id' => null]);
	}

	public function getProfileById($profileId, $withTrashed = false)
	{
		return $this->modelProfileRepository->getById($profileId, $withTrashed);
	}

	public function getProfileBySlug($slug)
	{
		return (!empty($slug))
			? $this->modelProfileRepository->getBySlug($slug)
			: null;
	}

	public function getProfileByIdOrSlug($key)
	{
		return (is_numeric($key))
			? $this->getProfileById($key)
			: $this->getProfileBySlug($key);
	}

	public function getCurrentProfile()
	{
		$result = null;

		try {
			if ($currentProfileID = $this->getCurrentProfileId()) {
				$result = $this->getProfileById($this->getCurrentProfileId());
			}

		} catch (ModelNotFoundException $ex) {
		}

		return $result;
	}

	public function getCurrentProfileId()
	{
		return session('profile_id');
	}

	public function saveUploadedFile($file, $type, $profileId, $deleteExists = false)
	{
		if ($file) {
			$profile = new ModelProfile();
			$profile->id = $profileId;
			$old = $deleteExists ? $old = AttachmentService::findFile('avatar', $profile, false) : null;

			$attachment = AttachmentService::uploadImage($type, $profile, $file);
			if ($attachment && $old) {
				$old->delete();
			}
			event(new AvatarAddedEvent($profileId));
			return $attachment;
		}

		return null;
	}

	public function associateTagHelper($profileId, $data, $tagFields)
	{
		$profile = new ModelProfile();
		$profile->id = $profileId;
		foreach ($tagFields as $field => $taxonomySlug) {

			$tax = TaxonomyService::getTaxonomyBySlug($taxonomySlug);

			if (array_key_exists($field, $data)) {
				$tags = $data[$field];

				$tags = array_filter(array_map('trim', $tags));

				if (isset($tags['__custom__'])) {
					$newTags = array_filter(array_map('trim', explode(',', $tags['__custom__'])));
					unset($tags['__custom__']);

					$newTagIds = array_map(function ($item) {
						return $item->id;
					}, TagService::addTagNames($newTags, $tax->id));
					$tags = array_merge($tags, $newTagIds);

				}


				TagService::explicitSetTagIdsWithEntity($profile, array_map('intval', $tags), $tax->id);
			}
		}
	}

	public function loadProfileTags($profileId, $tagFields)
	{
		$data = [];
		$profile = new ModelProfile();
		$profile->id = $profileId;
		foreach ($tagFields as $tagFieldName => $taxSlug) {
			$data[$tagFieldName] = [
				'tag_field_name' => $tagFieldName,
				'tax_slug' => $taxSlug,
				'tags' => TagService::getEntityTagsForTaxonomySlug($profile, $taxSlug)
			];
		}

		return $data;
	}

	public function loadOverallProviderRatings($providerId)
	{

		$ratings = new \stdClass();
		$ratings->details = ReviewService::getEntityOverallRatings('provider_profile', $providerId,
			ProviderProfile::class, Rating::TYPE_STAR);
		$ratings->overall = ReviewService::getEntityOverallRating('provider_profile', $providerId,
			ProviderProfile::class, Rating::TYPE_STAR);

		return $ratings;
	}

	/**
	 * Get profile session tags ..
	 *
	 * @param $profile_id
	 * @param bool $findParents
	 * @return array
	 */
	public function getProfileSessionTags($profile_id, $findParents = true)
	{
		$providerServicesIds = app('provider_service')->getServices($profile_id)->pluck('id')->all();
		$entity_type = get_class(new ProviderServiceEntity());

		$tags = TagService::getEntitiesTags($providerServicesIds, $entity_type);

		if ($findParents) {
			$childrens = $tags->pluck('id')->all();
			$tagIds = TagService::getParents($childrens);

			$tags = TagService::getTagsByIds(array_merge($childrens, $tagIds));
		}

		return $tags;
	}

	public function loadProfileBasicDetailsModel($profile)
	{

		if ($profile) {
			$basicInfo = ModelProfile::$BASIC_INFO_PROFILE_META_DETAILS;
			$data = $profile->toArray();

			foreach ($basicInfo['image'] as $key => $size) {
				$data[$key] = $this->loadUploadedFileUrl($profile, $key, $size);
			}

			$data['public_url'] = $profile->present()->publicUrl();

			$data = array_merge($data, $this->loadProfileMetaFields($profile->id, $basicInfo['value']));


			foreach ($basicInfo['date_value'] as $key) {
				if (isset($data[$key])) {
					$data[$key] = Carbon::parse($data[$key]);
				}
			}

			$detailedProfile = new \stdClass();

			foreach ($data as $field => $value) {
				$detailedProfile->$field = $value;
			}

			$detailedProfile->type = $profile->type;
			$detailedProfile->full_name = $detailedProfile->first_name . ' ' . $detailedProfile->last_name;

			return $detailedProfile;
		}

		return null;
	}

	public function loadProfileBasicDetails($profileId)
	{
		$data = null;
		$profile = $this->getProfileById($profileId, true);

		$r = $this->loadProfileBasicDetailsModel($profile);

		return $r;
	}

	public function loadProfilesBasicDetails($userId)
	{
		$result = [];
		$profiles = $this->getUserProfiles($userId);

		if (!$profiles) {
			return $result;
		}

		foreach ($profiles as $profile) {
			$result[] = $this->loadProfileBasicDetailsModel($profile);
		}

		return $result;
	}

	public function loadProfiles(array $profileIds = [], $withTrashed = false)
	{
		$result = [];
		if (!$profileIds) {
			return $result;
		}

		foreach ($profileIds as $profileId) {
			$profile = $this->getProfileById($profileId, $withTrashed);

			if (!$profile) {
				continue;
			}

			$result[] = $this->loadProfileBasicDetailsModel($profile);
		}

		return $result;

	}

	public function loadUploadedFileUrl($entity, $type, $size = null)
	{
		$attach = AttachmentService::findFile($type, $entity, false);

		if ($attach) {
			return $attach->url($size);
		}

		return null;
	}

	public function loadUploadedFilesUrl($entity, $type, $size = null)
	{
		$data = [];
		$attachments = AttachmentService::attachmentCollection($type, $entity);
		foreach ($attachments as $attach) {
			$data[] = [
				$size => $attach->url($size),
				'id' => $attach->id,
				'type' => $type,
				'original' => $attach->url()
			];
		}

		return $data;
	}

	public function searchForProviders($query, $isEmail = true, $onlyProviderIds = null, $limit = null)
	{
		return $this->searchForProfiles($query, $isEmail, ModelProfile::PROVIDER_PROFILE_TYPE, $onlyProviderIds, $limit);
	}

	public function searchForClients($query, $isEmail = true, $onlyProviderIds = null, $limit = null)
	{
		return $this->searchForProfiles($query, $isEmail, ModelProfile::CLIENT_PROFILE_TYPE, $onlyProviderIds, $limit);
	}

	private function searchForProfiles(
		$query,
		$isEmail = true,
		$profileType = ModelProfile::PROVIDER_PROFILE_TYPE,
		$onlyProviderIds,
		$limit
	) {
		$result = [];

		if (empty($query)) {
			return [];
		}

		$providerIds = $this->modelProfileRepository->searchProfiles($query, $isEmail, $profileType, $onlyProviderIds, $limit);
		if (!empty($providerIds)) {
			$result = $this->loadProfiles($providerIds);
		}

		return $result;
	}

	private function setProfileMetaField($profileId, $fieldName, $value)
	{
		return $this->metaRepository->setMetaKey($profileId, $fieldName, $value);
	}

	private function getProfileMetaField($profileId, $fieldName, $default = null)
	{
		return $this->metaRepository->getMetaKey($profileId, $fieldName, $default);
	}

	public function loadProfileMetaField($profileId, $fieldName, $default = null)
	{
		return $this->getProfileMetaField($profileId, $fieldName, $default);
	}

	public function loadProfileMetaFieldHashed($profileId, $fieldNames, $default = null)
	{
		$hashedVallue = $this->getProfileMetaField($profileId, $fieldNames);
		if (is_null($hashedVallue)) {
			return $default;
		}

		return Crypt::decrypt(base64_decode($hashedVallue));
	}

	public function loadProfileMetaFields($profileId, $fieldNames, $defaults = [])
	{
		$data = [];
		foreach ($fieldNames as $field) {
			if (!array_key_exists($field, $defaults)) {
				$data[$field] = $this->getProfileMetaField($profileId, $field);
			} else {
				$data[$field] = $this->getProfileMetaField($profileId, $field, $defaults[$field]);
			}
		}

		return $data;
	}

	public function getUserProfiles($userId, $profileIdOrType = null)
	{
		if ($profileIdOrType === null) {
			return $this->modelProfileRepository->getProfilesByUserId($userId);
		} elseif (is_numeric($profileIdOrType)) {
			return $this->modelProfileRepository->getProfileByUserAndProfileId($userId, $profileIdOrType);
		} else {
			return $this->modelProfileRepository->getProfileByUserAndProfileType($userId, $profileIdOrType);
		}
	}

	public function loadSocialAccounts($profileId)
	{
		$socialNetworks = self::getSocialNetworks();

		$data = [];

		if (!$socialNetworks) {
			return $data;
		}

		foreach (array_keys($socialNetworks) as $key) {
			$data[$key] = $this->getProfileMetaField($profileId, $key);
		}

		return $data;
	}

	public function storeSocialAccounts($profileId, $inpData)
	{
		$this->storeProfileMetaFieldsWithRegex($profileId, $inpData, self::getSocialNetworks());
		event(new SocialAccountChangeEvent($profileId));
	}

	public function storeProfileMetaFieldsWithRegex($profileId, $fieldsData, $fieldNames)
	{
		foreach ($fieldNames as $field => $data) {
			if (is_array($data)) {
				$fieldValue = array_get($fieldsData, $field);
				if (isset($fieldValue)) {

					if ($field !== 'link') {
						// strip the query part
						$fieldValue = strtok($fieldValue, '?');
					}
					if (preg_match($data['regex'], $fieldValue)) {
						$this->setProfileMetaField($profileId, $field, $fieldValue);
					} elseif (empty($fieldValue)) {
						$this->setProfileMetaField($profileId, $field, (string)$fieldValue);
					}
				}
			}
		}
	}


	public function storeProfileMetaFields($profileId, $fieldsData, $fieldNames, $convertNullToEmpty = false)
	{
        $updatedFields = [];
		foreach ($fieldNames as $field) {
			if (array_key_exists($field, $fieldsData)) {
				if ($convertNullToEmpty && $fieldsData[$field] == null) {
					$fieldsData[$field] = '';
				}
                $updatedFields[] = $field;
				$this->setProfileMetaField($profileId, $field, $fieldsData[$field]);

			}
		}
		event(new ProfileMetaChangeEvent($profileId, $updatedFields));
	}

	public function storeProfileMetaField($profileId, $fieldName, $fieldValue, $trigger = true)
	{
		$success = $this->setProfileMetaField($profileId, $fieldName, $fieldValue) > 0;
		if ($success && $trigger) {
			event(new ProfileMetaChangeEvent($profileId, [$fieldName]));
		}

		return $success;
	}

	public function storeProfileMetaFieldHashed($profileId, $fieldName, $fieldValue)
	{
		$hashedValue = base64_encode(Crypt::encrypt($fieldValue));

		return $this->storeProfileMetaField($profileId, $fieldName, $hashedValue);
	}

	public function updateProfile($profileId, $updateSlug = false)
	{
		$saved = false;
		if ($updateSlug) {
			$profile = $this->modelProfileRepository->getById($profileId);
			$details = $this->loadProfileBasicDetailsModel($profile);

			$profile->slug = SlugService::generateUniqueSlug($details->full_name, 'profiles', $profile->id);

			$saved = $this->modelProfileRepository->save($profile);
		}

		if ($saved) {
			event(new ProfileDetailsChangeEvent($profileId));
		}

		return $saved;

	}

	public function checkProfileSlug($profileId, $newSlug)
	{
		$changed = $this->getProfileMetaField($profileId, 'lastSlugUpdate') != null;

		$allowed = !$changed && !SlugService::slugExists($newSlug, 'profiles', 'slug', $profileId);

		return ['allowed' => $allowed, 'wasChanged' => $changed];
	}

	public function updateProfileSlug($profileId, $newSlug, $sluggify = false, $exclude = true)
	{
		if ($sluggify) {
			$newSlug = SlugService::generateUniqueSlug($newSlug, 'profiles', $profileId);
		}

		$excludeId = null;
		if ($exclude) {
			$excludeId = $profileId;
		}
		if (!SlugService::slugExists($newSlug, 'profiles', 'slug', $excludeId)) {

			$profile = $this->modelProfileRepository->getById($profileId);
			$profile->slug = $newSlug;
			$this->setProfileMetaField($profileId, 'lastSlugUpdate', Carbon::now()->toDateTimeString());

			return $this->modelProfileRepository->save($profile);

		} else {
			throw new ProfileSlugAlreadyExistsException(_('This slug already taken'));
		}

	}

	/**
	 * Get profile avatar ..
	 *
	 * @param $profile_id
	 * @return mixed
	 */
	public function getAvatar($profile_id)
	{
		$entity = $this->modelProfileRepository->getById($profile_id);

		return AttachmentService::findFile('avatar', $entity, false);
	}


	/**
	 * Get parsed social networks ..
	 *
	 * @return mixed
	 */
	public function getSocialNetworks()
	{
		return $this->parser->parse('wl.social_pages.social_pages');
	}

	public function addLocation($locationId, $profileId, $acceptAppointments = true)
	{
		event(new LocationAdded($profileId, $locationId));

		return $this->modelProfileRepository->addLocation($locationId, $profileId, $acceptAppointments);
	}

	public function getLocations($profileId)
	{
		return $this->modelProfileRepository->getLocations($profileId);
	}

	public function removeLocation($locationId, $profileId)
	{
		event(new LocationRemoved($profileId, $locationId));

		return $this->modelProfileRepository->removeLocation($locationId, $profileId);
	}

    public function isSearchable($profileId)
    {
        return ProviderMembershipService::hasFeature($profileId, ProviderFeatures::SEARCHABLE);
	}

    public function isBookable($profileId)
    {
        return ProviderMembershipService::hasFeature($profileId, ProviderFeatures::BOOKABLE);
	}

	public function setIsSearchable($profileId, $searchable, $trigger = true)
	{
		$res = $this->modelProfileRepository->setIsSearchable($profileId, $searchable);
		if($res) {
			event(new IsSearchableChangeEvent($profileId));
			if($searchable) {
				$profile = $this->getProfileById($profileId);
				event(new IsSearchableEvent($profile));
				if(!$this->getProfileMetaField($profileId, 'became_searchable')) {
					// Became searchable for the first time
					$this->setProfileMetaField($profileId, 'became_searchable', Carbon::now()->format('Y-m-d'));
					if ($trigger) {
						event(new BecameSearchableEvent($profile));
						EventHelper::fire(new ProviderProfileCompletedNotification($profile));
					}
				}
			}
		}
		return $res;
	}

	public function setIsBookable($profileId, $bookable)
	{
		$res = $this->modelProfileRepository->setIsBookable($profileId, $bookable);
        if($res) {
            //TODO - trigger event
        }
		return $res;
	}

	public function setProfileStstus($profileId, $status)
	{
		$res = $this->modelProfileRepository->setStatus($profileId, $status);
        if($res) {
            //TODO - trigger event
        }
        return $res;
	}

	public function hasProfile($userId, $profileType)
	{
		return count($this->getUserProfiles($userId, $profileType)) ? true : false;
	}


	public function isProfessionalsClient($professionalProfileId, $clientProfileId)
	{
		$professionalProfile = $this->modelProfileRepository->getById($professionalProfileId);
		$clientProfile = $this->modelProfileRepository->getById($clientProfileId);

		return $this->providerService->isProfessionalsClient($professionalProfile, $clientProfile);
	}

	/**
	 * @param int $profileId
	 * @return mixed
	 */
	public function acceptedTerms($profileId)
	{
		event(new TermsAccepted($profileId));
	}

	public function getCommissionFreePeriod($profileId)
	{
		return $this->getProfileMetaField($profileId, 'commission_free_period_in_days', PaymentConfig::getDaysWithNoFeeForNewClient());
	}

	public function setCommissionFreePeriod($profileId, $commissionFreePeriodInDays)
	{
		$this->setProfileMetaField($profileId, 'commission_free_period_in_days', $commissionFreePeriodInDays);
	}

	public function getCommissionFreePeriodEndDate($profileId)
	{
		$commissionFreePeriod = $this->getCommissionFreePeriod($profileId);

		$user = $this->getOwner($profileId);

		return $user->created_at->addDays($commissionFreePeriod);
	}

	public function isProfileTypeIn($profileId, $types = [])
	{
		$profile = $this->getProfileById($profileId);
		if (!$profile) {
			throw new ProfileException(__("Profile not found"));
		}

		if (!in_array($profile->type, $types)) {
			throw new WrongProfileTypeException();
		}

		return array_search($profile->type, $types);
	}

	public function getAllProviders()
	{
		return $this->modelProfileRepository->getProviders();
	}

	private function isProviderNeedEnhance(ModelProfile $provider)
	{
		$providerId = $provider->id;
		$educations = $this->educationService()->getProfileEducations($providerId);
		$media = $this->assetsService()->getProfileAssets($providerId);

		return count($educations) == 0 || count($media) == 0;
	}

	public function sendProvidersReadyForEnhanceNotification()
	{
		$result = [];
		$providers = $this->getAllProviders();

		foreach ($providers as $provider) {
			if ($this->isProviderNeedEnhance($provider)) {
				$result[] = $provider;
				$data = EventInfoBuilder::collectProfileInfo($provider->id, 'provider');
				EventHelper::fire(new ProviderProfileEnhanceNotification($provider, $data));
			}
		}

		return $result;
	}

	public function sendProviderHasTodayPendingAppointmentNotifications(Carbon $date)
	{
		$take = 100;
		$page = 0;

		do {
			$appointments = AppointmentService::getTodayPendingAppointments($date, $page++, $take);

			foreach ($appointments as $appointment) {
				$data = EventInfoBuilder::collectProviderAppointmentInfo($appointment);
				$provider = $this->getProfileById($appointment->provider_id);
				EventHelper::fire(new ProviderPendingAppointmentNotification($provider, $data));
			}

		} while (count($appointments) == $take);
	}

	public function setProviderUnavailableTimeBeforeAppointment($providerId, $unavailableTimeMinutes)
	{
		return $this->modelProfileRepository->setProviderUnavailableTimeBeforeAppointment($providerId, $unavailableTimeMinutes);
	}

	public function getProviderUnavailableTimeBeforeAppointment($providerId)
	{
		return $this->modelProfileRepository->getProviderUnavailableTimeBeforeAppointment($providerId);
	}

	public function setProfileBloggerStatus($providerId, $status)
	{
		return $this->setProfileMetaField($providerId, 'is_blogger', $status);
	}

	public function setProfilePhoneVerified($providerId)
	{
		return $this->setProfileMetaField($providerId, 'phone_sms_activated', true);
	}

	public function profileIsVisible($viewerId, $viewedId)
	{
		$viewer = $this->getProfileById($viewerId);
		$viewed = $this->getProfileById($viewedId);
		if($viewed->type == ModelProfile::CLIENT_PROFILE_TYPE){
			if($viewer->type == ModelProfile::CLIENT_PROFILE_TYPE){
				return false;
			}
			if($viewer->type == ModelProfile::PROVIDER_PROFILE_TYPE){
				return $this->providerService->isProfessionalsClient($viewer, $viewed);
			}
		}
		return true;
	}

    public function setProfileStatus($profileId, $status)
    {
        $profile = $this->getProfileById($profileId);
        if($profile) {
            return $profile->update(['status' => $status]) ? $status : null;
        }
	}

    public function getProfileDisplayName($profileId)
    {
        $meta = $this->loadProfileMetaFields($profileId, ['first_name', 'last_name', 'business_name', 'business_name_display']);
        if(array_get($meta, 'business_name_display') && array_get($meta, 'business_name')) {
            return $meta['business_name'];
        }
        return array_get($meta, 'first_name') . ' ' . array_get($meta, 'last_name');
	}
}

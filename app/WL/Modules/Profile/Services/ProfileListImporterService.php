<?php namespace WL\Modules\Profile\Services;

use WL\Modules\Client\Models\ClientImportRecord;
use WL\Modules\Profile\Exceptions\ProfileClientImportAlreadyApprovedException;
use WL\Modules\Profile\Exceptions\ProfileClientImportAlreadyDeclineException;
use WL\Modules\Profile\Exceptions\ProfileClientImportAlreadyInvitedException;
use WL\Yaml\Facades\Yaml;

class ProfileListImporterService extends BasicProfileImporterService implements ProfileListImporterInterface
{
	public function importClients($providerProfileId, $clientImports, $sendEmailsToClients = true, $customEmailBody = null, $couponDiscount = null)
	{
        $response = [
            'usersAndProfiles' => [],
            'existedEmails' => [],
            'declinedEmails' => [],
            'failedRegistrationEmails' => [],
        ];

		$limit = Yaml::get('max', 'wl.import.limits');
        foreach ($clientImports as $index => $clientImport) {
        	if($index >= $limit) {
				$response['exceededLimit'][] = $clientImport->email;
				continue;
			}
            try{
				$response['usersAndProfiles'][] = $this->importClient($providerProfileId, $clientImport, $sendEmailsToClients, $customEmailBody, $couponDiscount);
            } catch (ProfileClientImportAlreadyApprovedException $e){
                $response['existedEmails'][] = $clientImport->email;
            } catch (ProfileClientImportAlreadyInvitedException $e){
                $response['existedEmails'][] = $clientImport->email;
            } catch (ProfileClientImportAlreadyDeclineException $e){
                $response['declinedEmails'][] = $clientImport->email;
            } catch (\Exception $e){
                $response['failedRegistrationEmails'][] = $clientImport->email;
            }
        }

        return $response;
	}

	public function importClientsRaw($providerProfileId, $data, $sendEmailsToClients = true, $customEmailBody = null, $couponDiscount = null)
	{
		$clientImports = array_map(function($record){
			return new ClientImportRecord(array_get($record, 'email'), array_get($record, 'first_name'), array_get($record, 'last_name'), array_get($record, 'phone'));
		}, $data);

		return $this->importClients($providerProfileId, $clientImports, $sendEmailsToClients, $customEmailBody, $couponDiscount);
	}

}

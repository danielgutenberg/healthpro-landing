<?php namespace WL\Modules\Profile\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use WL\Modules\Client\Models\ClientImportRecord;
use WL\Modules\Profile\Exceptions\ProfileCsvImportWrongColumnsNumberException;

class ProfileCsvImporterService extends ProfileListImporterService implements ProfileFileImporterInterface
{
	public function importFile($providerProfileId, UploadedFile $clientsFile, $sendEmailsToClients = true, $customEmailBody = null, $couponDiscount = null)
	{
		$clientImports = [];

		if (($handle = fopen($clientsFile->getPathname(), "r")) !== false) {
			while (($data = fgetcsv($handle)) !== false) {
				$num = count($data);
				if ($num < 3 /* [last name, first name, email, phone]  from WA-169 */) {
					throw new ProfileCsvImportWrongColumnsNumberException('Invalid number of columns');
				}
				if ($data[2] == 'Email' || $data[2] == '') {
					continue;
				}
				$clientImports[] = $this->csvRowToClientImport($data);
			}
			fclose($handle);
		}

		return $this->importClients($providerProfileId, $clientImports, $sendEmailsToClients, $customEmailBody);
	}

	/**
	 * @param int $providerId
	 * @param array $row
	 * @return ClientImportRecord
	 */
	private function csvRowToClientImport(array $row)
	{
		return new ClientImportRecord($row[2], $row[1], $row[0], array_get($row, 3));
	}


}

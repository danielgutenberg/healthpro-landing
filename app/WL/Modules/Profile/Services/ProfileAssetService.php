<?php namespace WL\Modules\Profile\Services;

use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Models\ProfileAsset;

/**
 * Interface ProfileAssetService
 * @package WL\Modules\Profile
 */
interface ProfileAssetService
{
	/**
	 * return assets for profile id, filtered by asset type
	 *
	 * @param $profileId
	 * @param $assetType
	 * @return mixed
	 */
	public function getProfileAssetsByType( $profileId, $assetType);

	/**
	 * return all profile assets
	 *
	 * @param $profileId
	 * @return mixed
	 */
	public function getProfileAssets($profileId);

	/**
	 * @param $profileId
	 * @param $assetId
	 * @return mixed
	 */
	public function removeProfileAsset($profileId, $assetId);

	/**
	 * @param $profileId
	 * @param $assetId
	 * @return mixed
	 */
	public function getProfileAssetById($profileId, $assetId);

	/* *
	 * @param ProfileAsset $asset
	 * @return mixed
	 */
	//public function saveProfileAsset(ProfileAsset $asset);

	/**
	 * Store profile assets
	 *
	 * @param $profileId
	 * @param $data
	 * @param $assetsFields
	 * @return mixed
	 */
	public function storeProfileAssets($profileId, $data, $assetsFields);

	/**
	 * @param $profileId
	 * @param $assetData
	 * @return mixed
	 */
	public function storeProfileAsset($profileId, $assetData);

	/**
	 * @param $assetId
	 * @param $assetData
	 * @return mixed
	 */
	public function updateProfileAsset($assetId, $assetData);
}

<?php namespace WL\Modules\Profile\Services;


interface ProfileProfessionalBodyService
{
	/**
	 *
	 * store array of ProfileProfessionalBody array can contain ['title'=>'','description'=>'','expiry_date'=>datetime]
	 * @param $profileId
	 * @param $professionalBodyItems
	 * @return mixed
	 */
	public function storeProfileProfessionalBody($profileId, $professionalBodyItems);

	/**
	 * @param $profileId
	 * @return mixed
	 */
	public function loadProfileProfessionalBody($profileId);
}

<?php namespace WL\Modules\Profile\Services;


interface ProfileReminderServiceInterface
{
	/**
	 * Create ProfileRemainder
	 *
	 * @param $profileId
	 * @param $value
	 * @param $type
	 * @param $aboutEntityType
	 * @return mixed
     */
	public function create($profileId, $value, $type, $aboutEntityType);

	/**
	 * Create default profile reminders from config/wl/profile/reminder/
	 *
	 * @param $profileId
	 * @return mixed
     */
	public function createDefaultProfileReminders($profileId);

	/**
	 * update value
	 *
	 * @param $profileReminderId
	 * @param $value
	 * @return mixed
     */
	public function update($profileReminderId, $value);

	/**
	 * Returns provider reminders
	 *
	 * @param $profileId
	 * @return mixed
     */
	public function getProfileReminders($profileId);

	/**
	 * mark ProfileReminder as Sent
	 * @param $profileReminderId
	 * @param $aboutEntityId
	 * @param $aboutEntityType
	 * @return mixed
	 */
	public function setProfileReminderSent($profileReminderId, $aboutEntityId, $aboutEntityType);

	/**
	 * check if ProfileRemainder was sent
	 *
	 * @param $profileReminderId
	 * @param $aboutEntityId
	 * @param $aboutEntityType
	 * @return mixed
	 */
	public function getProfileReminderSentDateTime($profileReminderId, $aboutEntityId, $aboutEntityType);

}

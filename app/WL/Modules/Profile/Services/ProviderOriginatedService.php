<?php namespace WL\Modules\Profile\Services;


use App\Exceptions\GenericException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use WL\Modules\Profile\Exceptions\ProfileClientImportAlreadyInvitedException;
use WL\Modules\Profile\Exceptions\ProviderClientNotFound;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\ProfilesMeta\ProviderClientsMeta;
use WL\Modules\Profile\Models\ProviderOriginated;
use WL\Modules\Profile\Repositories\MetaRepository;
use WL\Modules\Profile\Repositories\ProviderOriginatedRepositoryInterface;
use WL\Validation\Exceptions\InvalidParametersException;

class ProviderOriginatedService implements ProviderOriginatedServiceInterface
{
	protected $providerOriginatedRepository;
	protected $metaRepository;

	public function __construct(ProviderOriginatedRepositoryInterface $providerOriginatedRepository)
	{
		$this->providerOriginatedRepository = $providerOriginatedRepository;
		$this->metaRepository = new MetaRepository(ProviderOriginated::class);
	}

	public function getProviderOriginatedClients($providerId)
	{
		return $this->providerOriginatedRepository->getProviderClients($providerId, true);
	}

	public function getProviderClients($providerId)
	{
		return $this->providerOriginatedRepository->getProviderClients($providerId);
	}

	public function getProviderClientsInvites($providerId)
	{
		return $this->providerOriginatedRepository->getProviderClients($providerId, null, true);
	}

	public function getClientProviders($clientId, $deleted = true)
	{
		return $this->providerOriginatedRepository->getClientProviders($clientId, false, $deleted);
	}

	public function getClientProviderInvites($clientId)
	{
		return $this->providerOriginatedRepository->getClientProviders($clientId, true);
	}

	public function setProviderOriginatedClient($providerId, $clientId)
	{
		$model = $this->getProviderClient($providerId, $clientId);
		if(!$this->checkCanInvite($model)){
			return true;
		}
		return $model->exists ? $this->increametProviderClient($model) : $this->createProviderClient($model);

	}

	public function reinviteClient($providerId, $clientId)
	{
		if(!$model = $this->providerOriginatedRepository->get($providerId, $clientId)){
			throw new ProviderClientNotFound();
		}

		if(!$this->checkCanInvite($model)){
			throw new ProfileClientImportAlreadyInvitedException();
		}
		if($this->increametProviderClient($model)){
			return $model->fresh();
		}
	}

	public function inviteClient($providerId, $clientId)
	{
        $model = $this->getProviderClient($providerId, $clientId);

		if(!$this->checkCanInvite($model)){
			throw new ProfileClientImportAlreadyInvitedException();
		}

		if($this->createProviderClient($model)){
			return $model->fresh();
		}
	}

	public function canInviteClient($providerId, $clientId)
	{
		$model = $this->getProviderClient($providerId, $clientId);
		return $this->checkCanInvite($model);
	}

	public function checkCanInvite(ProviderOriginated $model)
	{
		if(!$model->exists){
			return true;
		}
		if($model->invite_count >= ProviderOriginated::MAX_INVITES){
			return false;
		}
		if($model->last_invite->diffInHours(Carbon::now()) < ProviderOriginated::INVITE_INTERVAL_HOURS){
			return false;
		}
		return true;
	}

	public function confirmProviderClient($providerId, $clientId, $approved = true)
	{
		$model = $this->getProviderClient($providerId, $clientId);
		if(!$model->exists) {
			return false;
		}
		if(!is_null($model->approved) && $model->approved == $approved) {
			return $approved;
		}
		$model->approved = $approved;
		return $this->providerOriginatedRepository->save($model);
	}

	public function addProviderClient($providerId, $clientId)
	{
		$model = $this->getProviderClient($providerId, $clientId);
		if($model->exists && $model->approved){
			return false;
		}
		$model->approved = true;
		return $this->createProviderClient($model);
	}

	public function getProviderClient($providerId, $clientId)
	{
		return $this->providerOriginatedRepository->firstOrNew($providerId, $clientId);
	}

	public function isProviderClient($providerId, $clientId)
	{
		$model = $this->getProviderClient($providerId, $clientId);

		return $model->exists && $model->approved;
	}

	public function isApprovedOrInvited($providerId, $clientId, $force = false)
	{
		$model = $this->getProviderClient($providerId, $clientId);
		if(!$model->exists) {
			return false;
		}
		if($model->approved) {
			return true;
		} elseif(is_null($model->approved)) {
			if($force) {
				$model->approved = true;
				$this->providerOriginatedRepository->save($model);
			}
			return true;
		}
	}

	public function setMetaData($providerId, $clientId, $array, $value = null)
	{
		if(!$model = $this->providerOriginatedRepository->get($providerId, $clientId)){
			throw new ProviderClientNotFound();
		}

		if(!is_array($array)){
			$array = [$array => $value];
		}

		$response = [];

		foreach ($array as $key => $value){
			$response[$key] = $this->metaRepository->setMetaKey($model->id, $key, $value);
		}

		return $response;
	}

	public function getMetaData($providerId, $clientId, array $fieldNames = [])
	{
		if(!$model = $this->providerOriginatedRepository->get($providerId, $clientId)){
			throw new ProviderClientNotFound();
		}

		return ProviderClientsMeta::hydrate($this->metaRepository->getMetaKeys($model->id, $fieldNames));
	}

	public function toggleMeta($providerId, $clientId, $key, $force = null)
	{
		if(!$model = $this->providerOriginatedRepository->get($providerId, $clientId)){
			throw new ProviderClientNotFound();
		}

		$dataType = (new ProviderClientsMeta())->getCast($key);
		if(!in_array($dataType, ['bool', 'boolean'])){
			throw new InvalidParametersException($key . ' not a boolean');
		}
		if(!is_null($force)){
			$setValue = (boolean) $force;
		} else {
			$setValue = !$this->metaRepository->getMetakey($model->id, $key);
		}
		if(!$this->metaRepository->setMetaKey($model->id, $key, $setValue)){
			throw new GenericException('Failed to set meta value');
		}
		return $setValue;
	}

	public function approveOriginatorProvider($clientId)
	{
		// Check if table exists - prevent seeding issue
		if(!Schema::hasTable(ProviderOriginated::getTableName())){
			return false;
		}
		$model = $this->providerOriginatedRepository->getOriginator($clientId);
		if($model && is_null($model->approved)){
			$model->approved = true;
			return $this->providerOriginatedRepository->save($model);
		}
		return false;
	}

	public function collectInviteData(ProviderOriginated $invite)
	{
		$data = (object) [
			"id" => $invite->id,
			"client_id" => $invite->client_id,
			"provider_id" => $invite->provider_id,
			"approved" => $invite->approved,
			"originated" => $invite->originated,
			"created_at" => $invite->created_at,
			"updated_at" => $invite->updated_at,
			"invite_count" => $invite->invite_count,
			"last_invite" => $invite->last_invite,
		];
        $data->client_email = ProfileService::getProfileEmail($invite->client_id);
		$data->can_invite = $this->checkCanInvite($invite);
		return $data;
	}

	private function createProviderClient(ProviderOriginated $model)
	{
		$model->originated = !$this->providerOriginatedRepository->hasProviderOriginated($model->client_id);
		$model->invite_count = 1;
		return $this->saveProviderClient($model);
	}

	private function increametProviderClient(ProviderOriginated $model)
	{
		$model->invite_count++;
		return $this->saveProviderClient($model);
	}

	private function saveProviderClient(ProviderOriginated $model)
	{
		$model->last_invite = Carbon::now();
		return $this->providerOriginatedRepository->save($model);
	}
}

<?php namespace WL\Modules\Profile\Services;


use WL\Modules\Client\Models\ClientImportRecord;
use WL\Modules\Profile\Exceptions\ProfileClientImportAlreadyApprovedException;
use WL\Modules\Profile\Exceptions\ProfileClientImportAlreadyDeclineException;
use WL\Modules\Profile\Exceptions\ProfileClientImportAlreadyInvitedException;
use WL\Modules\Profile\Exceptions\ProfileListImportInvalidClientDataException;

interface ProfileImporterInterface
{
	/**
	* @param int $providerProfileId
	* @param ClientImportRecord $clientImport
	* @param boolean|true $sendEmailsToClients
	* @param string|null $providerCustomEmailBody
	* @param string|null $couponDiscount
	* @throws ProfileListImportInvalidClientDataException - if failed to create client from given data
	* @throws ProfileClientImportAlreadyApprovedException
	* @throws ProfileClientImportAlreadyDeclineException
	* @throws ProfileClientImportAlreadyInvitedException
	* @return \stdClass
	*/
	public function importClient($providerProfileId, ClientImportRecord $clientImport, $sendEmailsToClients = true, $providerCustomEmailBody = null, $couponDiscount = null);

	/**
	 * @param int $clientProfileId
	 * @param int $providerProfileId
	 * @param bool $newClient
	 * @param string|null $providerCustomEmailBody
	 * @param null $couponDiscount
	 * @return void
	 */
	public function sendInvite($clientProfileId, $providerProfileId, $newClient = true, $providerCustomEmailBody = null, $couponDiscount = null);
}

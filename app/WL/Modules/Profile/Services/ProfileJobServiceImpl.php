<?php namespace WL\Modules\Profile\Services;

use Carbon\Carbon;
use WL\Modules\Profile\Models\ProfileJob;
use WL\Modules\Profile\Repositories\ProfileJobInterface;

class ProfileJobServiceImpl implements ProfileJobService
{
	public function __construct(ProfileJobInterface $repository)
	{
		$this->repo = $repository;
	}

	public function explicitSetProfileJobs($profileId, array $profileJobs)
	{
		$this->removeAllProfileJobs($profileId);
		foreach ($profileJobs as $job) {
			$this->addProfileJob($profileId, $job);
		}

	}

	public function removeAllProfileJobs($profileId)
	{
		return $this->repo->removeProfileJobs($profileId);
	}

	public function getProfileJobs($profileId)
	{
		return $this->repo->getProfileJobs($profileId);
	}

	public function addProfileJob($profileId, $profileJob)
	{
		$job = new ProfileJob();
		$from = $profileJob['from']['month'] . '/1/' . $profileJob['from']['year'];

		$validTo = false;
		if (array_key_exists('to', $profileJob) && !!$profileJob['to']['month'] && !!$profileJob['to']['year']) {
			$validTo = true;
			$to = $profileJob['to']['month'] . '/1/' . $profileJob['to']['year'];
		}
		$job->company_name = $profileJob['company_name'];
		$job->from = Carbon::parse($from);
		$job->to = $validTo ? Carbon::parse($to):null;
		$job->profile_id = $profileId;
		$job->job_title = $profileJob['job_name'];

		return $this->repo->saveJob($job);
	}

	public function removeJob($jobId)
	{
		return $this->repo->destroy($jobId);
	}
}

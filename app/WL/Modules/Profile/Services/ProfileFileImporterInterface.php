<?php namespace WL\Modules\Profile\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface ProfileFileImporterInterface
{
	/**
	 * @param int $providerProfileId
	 * @param UploadedFile $clientsFile
     * @param boolean|true $sendEmailsToClients
	 * @param string|null $customEmailBody
	 * @param string|null $couponDiscount
	 * @return mixed
	 */
    public function importFile($providerProfileId, UploadedFile $clientsFile, $sendEmailsToClients = true, $customEmailBody = null, $couponDiscount = null);
}

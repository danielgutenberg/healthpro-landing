<?php namespace WL\Modules\Profile\Services;

use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Attachment\Facades\AttachmentService;
use WL\Modules\Attachment\Models\Attachment;
use WL\Modules\Profile\Models\ProfileAsset;
use WL\Modules\Profile\Repositories\ProfileAssetsInterface;

class ProfileAssetServiceImpl implements ProfileAssetService
{

	public function __construct(ProfileAssetsInterface $profileAssetsRepository)
	{
		$this->profileAssetsRepository = $profileAssetsRepository;
	}

	private function integrateAdditionalData(array $assets)
	{
		foreach ($assets as &$asset) {
			$asset->attachment = AttachmentService::loadAttachmentUrlsById($asset->attachment_id);
		}
	}


	public function getProfileAssetsByType($profileId, $assetType)
	{
		$assets = $this->profileAssetsRepository->getAssetsByType($profileId, $assetType);

		$this->integrateAdditionalData($assets);

		return $assets;
	}

	public function getProfileAssets($profileId)
	{
		$assetsResult = [];
		$assets = $this->profileAssetsRepository->getAllAssets($profileId);
		$this->integrateAdditionalData($assets);

		foreach ($assets as $asset) {
			$assetsResult[$asset->type][] = $asset;
		}

		return $assetsResult;
	}

	public function removeProfileAsset($profileId, $assetId)
	{
		return $this->profileAssetsRepository->removeAsset($profileId, $assetId);
	}


	public function getProfileAssetById($profileId, $assetId)
	{
		$asset = $this->profileAssetsRepository->getByIdSecure($profileId, $assetId);
		$this->integrateAdditionalData([$asset]);
		return $asset;
	}

	private function saveProfileAsset(ProfileAsset $asset)
	{
		return $this->profileAssetsRepository->save($asset);
	}

	private function takeKeys($arr, $keys)
	{
		$ret = [];
		foreach ($keys as $key) {
			if (isset($arr[$key])) {
				$ret[$key] = $arr[$key];
			}
		}

		return $ret;
	}

	public function storeProfileAsset($profileId, $assetData)
	{
		if (is_array($assetData) && (isset($assetData['file']) || isset($assetData['url']))) {
			$profile = new ModelProfile();
			$profile->id = $profileId;

			$asset = new ProfileAsset();
			$asset->fill($this->takeKeys($assetData, ['title', 'description', 'type']));

			if (array_key_exists('file', $assetData)) {
				$attachment = AttachmentService::uploadFile('asset_' . $asset->type, $profile, $assetData['file']);
			} else {
				$attachment = new Attachment();
				$attachment->fill([
					'name' => $assetData['url'],
					'elm_type' => $profile->getMorphClass(),
					'elm_id' => $profileId,
					'file' => 'profile_asset_' . $asset->type,
					'extension' => '',
					'content_type' => 'url',
					'original_name' => $assetData['url'],
					'size' => 0
				]);

				AttachmentService::save($attachment);
			}

			if ($attachment->id != null) {
				$asset->profile_id = $profileId;
				$asset->attachment_id = $attachment->id;

				$this->profileAssetsRepository->save($asset);

				$ret = AttachmentService::loadAttachmentUrlsById($asset->attachment_id);
				$ret['id'] = $asset->id;
				return $ret;
			}
		}

		return null;
	}

	public function storeProfileAssets($profileId, $data, $assetsFields)
	{
		$ret = [];
		foreach ($assetsFields as $field) {
			if (array_key_exists($field, $data)) {
				$ret[$field] = $this->storeProfileAsset($profileId, $data[$field]);
			}
		}
		return $ret;
	}

	public function updateProfileAsset($assetId, $assetData)
	{
		$setItems = ['title', 'description'];
		$asset = $this->profileAssetsRepository->getById($assetId);
		foreach ($setItems as $field) {
			if (isset($assetData[$field])) {
				$asset->$field = $assetData[$field];
			}
		}
		return $this->saveProfileAsset($asset);
	}
}

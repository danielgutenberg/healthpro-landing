<?php namespace WL\Modules\Profile\Services;

use WL\Modules\Profile\Models\ModelProfile;

interface ClientProfileService
{
	/**
	 * @param $profile
	 * @return mixed
     */
	public function getProviders(ModelProfile $profile);
}

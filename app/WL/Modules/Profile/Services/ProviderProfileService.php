<?php namespace WL\Modules\Profile\Services;

use WL\Modules\Profile\Models\ModelProfile;

interface ProviderProfileService
{
	/**
	 * @param ModelProfile $professionalProfile
	 * @param ModelProfile $clientProfile
	 * @return bool
	 */
	public function isProfessionalsClient(ModelProfile $professionalProfile, ModelProfile $clientProfile);
}

<?php namespace WL\Modules\Profile\Services;

/**
 * Interface ProfileEducationService
 * @package WL\Modules\Profile
 */
interface ProfileEducationService
{
	/**
	 * @param $profileId
	 * @param array $educations
	 * @return mixed
	 */
	public function explicitSetProfileEducations($profileId, array $educations);

	/**
	 * @param $profileId
	 * @return mixed
	 */
	public function removeAllProfileEducations($profileId);

	/**
	 * @param $profileId
	 * @return mixed
	 */
	public function getProfileEducations($profileId);

	/**
	 * @param $profileId
	 * @param $education
	 * @return mixed
	 */
	public function addProfileEducation($profileId, $education);

	/**
	 * @param $educationId
	 * @return mixed
	 */
	public function removeEducation($educationId);

}

<?php
namespace WL\Modules\User\Models;

use Cartalyst\Sentinel\Reminders\EloquentReminder;
use WL\Models\ModelTrait;

class Reminder extends EloquentReminder
{
	use ModelTrait;
}

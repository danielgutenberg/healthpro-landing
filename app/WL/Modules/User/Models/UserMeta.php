<?php
namespace WL\Modules\User\Models;

use WL\Models\ModelMeta;

class UserMeta extends ModelMeta
{
	protected $table = 'users_meta';
}

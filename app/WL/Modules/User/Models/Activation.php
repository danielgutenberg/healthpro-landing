<?php
namespace WL\Modules\User\Models;

use Cartalyst\Sentinel\Activations\EloquentActivation;
use WL\Models\ModelTrait;

class Activation extends EloquentActivation
{
	use ModelTrait;
}

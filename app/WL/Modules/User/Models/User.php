<?php namespace WL\Modules\User\Models;

use Carbon\Carbon;
use Cartalyst\Sentinel\Users\EloquentUser;
use Watson\Validating\ValidatingInterface;
use Watson\Validating\ValidatingTrait;
use WL\Contracts\Presentable;
use WL\Models\MetaTrait;
use WL\Models\ModelTrait;
use WL\Models\PresentableTrait;
use WL\Modules\Notification\Models\NotifiableInterface;
use WL\Modules\Notification\Models\NotifiableTrait;
use WL\Modules\Phone\PhoneTrait;
use WL\Modules\Profile\Models\ManageProfileTrait;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\ProfileGroup\Models\SocialLink;
use WL\Modules\Referral\Contracts\ReferrableContract;
use WL\Modules\User\Presenters\UserPresenter;

class User extends EloquentUser implements Presentable, ReferrableContract, ValidatingInterface, NotifiableInterface
{
	use ValidatingTrait;

	use ModelTrait;

	use PhoneTrait;

	use PresentableTrait;

	use ManageProfileTrait;

	use MetaTrait;

	use NotifiableTrait;

	protected $rules = [];

	protected $metaClassName = UserMeta::class;

	protected $morphClass = self::class;

	protected $presenter = UserPresenter::class;

	protected $fillable = [
		'email',
		'password',
		'last_name',
		'first_name',
		'permissions',
		'pending_email',
		'force_login'
	];

	/**
	 * Get list of profiles by type ...
	 *
	 * @param string $type
	 * @return $this
	 */
	public function profiles($type = '')
	{
		return $this->belongsToMany($type ?: ModelProfile::class, 'users_profiles', 'user_id', 'profile_id')
			->withPivot('is_owner', 'accepted', 'permissions');
	}

	public function getRules()
	{
		return ($this->exists)
			? [
				'email' => "required|email|unique:users,email,{$this->id}",
			] : [
				'email' => "required|email|unique:users,email",
				'password' => 'required',
			];
	}

	public function __get($key)
	{
		if ($key == 'full_name') {
			return trim($this->first_name . ' ' . $this->last_name);
		} else {
			return parent::__get($key);
		}
	}

	public static function findByReminder($code)
	{
		$remindersTable = Reminder::getTableName();

		$selfTable = self::getTableName();

		return self::join($remindersTable, $selfTable . '.id', '=', $remindersTable . '.user_id')
			->where($remindersTable . '.code', '=', $code)
			->select($selfTable . '.*')// we need only User fields
			->first();
	}

	public static function findByActivation($code)
	{
		$activationsTable = Activation::getTableName();

		$selfTable = self::getTableName();

		return self::join($activationsTable, $selfTable . '.id', '=', $activationsTable . '.user_id')
			->where($activationsTable . '.code', '=', $code)
			->select($selfTable . '.*')// we need only User fields
			->first();
	}

	public function connection($provider)
	{
		return SocialLink::where('user_id', '=', $this->id)
			->where('provider', '=', $provider)
			->first();
	}

	public function disconnect($provider)
	{
		$connection = $this->connection($provider);

		return null === $connection ? false : $connection->delete();
	}

	/**
	 * Find incomplete user activation
	 * @return \WL\Modules\User\Models\Activation
	 */
	public function activation()
	{
		return $this->activations()
			->where('completed', '=', 0)
			->orderBy('updated_at', 'DESC')
			->first();
	}

	public function isActivated()
	{
		$activation = $this->activations()
			->where('completed', '=', 1)
			->orderBy('updated_at', 'DESC')
			->first();

		if (!$activation) {
			return false;
		}

		return $activation->completed == 1;
	}

	public function socials()
	{
		return $this->hasMany(\Cartalyst\Sentinel\Addons\Social\Models\Link::class);
	}

	public function getActiveSocials()
	{
		return $this->socials()->where('oauth2_expires', '>', Carbon::now('UTC'))->get();
	}

	/**
	 * Get an generated referral code ...
	 *
	 * @return bool
	 */
	public function getReferralCode()
	{
		return $this->referral_code;
	}

	/**
	 * Generate an referral code ...
	 *
	 * @return bool
	 */
	public function setReferralCode($code)
	{
		$this->referral_code = $code;
		$this->save();
	}
}

<?php namespace WL\Modules\User\Drivers;

use Mail;
use Setting;
use WL\Modules\User\Contracts\ActivableContract;
use WL\Modules\User\Facades\User;

class ActivationMailDriver extends AbstractTemplateDriver implements ActivableContract {

	/**
	 * Send activation code for specific entity to address.
	 *
	 * @param $entity
	 * @param $address
	 * @param array $params
	 * @return mixed
	 */
	public function sendActivation($entity, $address, array $params = array()) {
		$activation = self::createActivation($entity, $address, $params);

		$fromEmail = trim(Setting::get('default_from_email'));
		$fromName  = Setting::get('default_from_name');
		Mail::alwaysFrom($fromEmail, $fromName);

		$emailTo = $params['email'] ?? $address;

		$data = $activation + [
				'name' => $params['name'],
				'newUser' => $params['newUser'],
				'type' => $params['type'],
				'firstName' => $params['firstName']
			];
		if (isset($params['email'])) {
			$data['email'] = $params['email'];
		}

		if (!$params['newUser']) {
			User::setPendingEmail($params['userId'], $emailTo);
		}

		Mail::send('emails.user.activation', $data, function ($message) use($address, $entity, $params, $emailTo) {
			$message->to($emailTo, 'name');
			if ($params['newUser']) {
				$message->subject(_('Confirm Your Email to Get Started with HealthPRO'));
			} else {
				$message->subject(_('Verify Your Updated Email Address'));
			}
		});
	}


}

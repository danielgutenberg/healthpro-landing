<?php namespace WL\Modules\User\Drivers;

use Activation;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Crypt;

/**
 * That component has Sentinel Activation Dependency ..
 *
 * Class AbstractTemplateDriver
 * @package WL\Modules\User\Drivers
 */
abstract class AbstractTemplateDriver {

	/**
	 * Create an activation for specific entity ..
	 *
	 * @param $entity
	 * @param $address
	 * @return mixed|string
	 */
	public function createActivation($entity, $address, $params) {
		$activation = Activation::create($entity);

		$addressToHash = $params['email'] ?? $address;

		return array_merge(array(
			'address'  => $address,
			'hash'     => self::hashCode($addressToHash),
		), $activation->toArray());
	}

	/**
	 * Check for activation exists ...
	 *
	 * @param $entity
	 * @param null $code
	 * @return bool
	 */
	public function existsActivation($entity, $code = null) {
		return Activation::exists($entity, $code);
	}

	/**
	 * Complete entity activation .
	 *
	 * @param $entity
	 * @param $code
	 * @return bool
	 */
	public function complete($entity, $code) {
		return Activation::complete($entity, $code);
	}

	/**
	 * Complete activation forced .
	 *
	 * @param $entity
	 * @return bool
	 */
	public function forceComplete($entity) {
		if( $activation = self::existsActivation($entity)) {
			$activation->fill([
				'completed'    => true,
				'completed_at' => Carbon::now(),
			]);

			$activation->save();

			return true;
		}
	}

	/**
	 * Check if that entity has completed activation .
	 *
	 * @param $entity
	 * @param callable $callback
	 * @return bool
	 */
	public function completed($entity, Closure $callback = null) {
		$result = Activation::completed($entity);

		if( $callback )
			return $callback($entity, $result);

		return $result;
	}

	/**
	 * Hash code ..
	 *
	 * @param $code
	 * @return string
	 */
	protected function hashCode($code) {
		return base64_encode(Crypt::encrypt($code));
	}
}

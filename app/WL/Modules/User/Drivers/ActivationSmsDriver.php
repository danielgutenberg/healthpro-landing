<?php namespace WL\Modules\User\Drivers;

use WL\Modules\User\Contracts\ActivableContract;

class ActivationSmsDriver extends AbstractTemplateDriver implements ActivableContract {

	/**
	 * Send activation code for specific entity to address.
	 *
	 * @param $entity
	 * @param $address
	 * @return mixed
	 */
	public function sendActivation($entity, $address) {
		// TODO: Implement sendActivation() method.
	}
}

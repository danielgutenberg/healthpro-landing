<?php namespace WL\Modules\User\Services;

use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Event;
use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use WL\Modules\HasOffers\Events\RegisteredEvent;
use stdClass;
use WL\Modules\Payment\Facades\PaymentService;
use WL\Modules\Profile\Events\ProfileEmailChangeEvent;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Models\ProviderProfile;
use WL\Modules\Provider\Events\ProviderCreateEvent;
use WL\Modules\User\Contracts\ActivableContract as Activable;
use WL\Modules\User\Events\ActivationCompletedEvent;
use WL\Modules\User\Events\ActivationSentEvent;
use WL\Modules\User\Exceptions\ActivationException;
use WL\Modules\User\Exceptions\UserManualPasswordRequiredException;
use WL\Modules\User\Exceptions\UserNotFoundException;
use WL\Modules\User\Models\User;
use WL\Security\Facades\ApiKeyService;
use WL\Security\Facades\AuthorizationUtils;

class UserServiceImpl implements UserServiceInterface
{

	const MEMBER_ROLE = 'member';
	const ADMIN_ROLE = 'administrator';
	const STAFF_ROLE = 'staff';

	/**
	 * @var
	 */
	private $activable;

	public function __construct(Activable $activable)
	{
		$this->activable = $activable;
	}

	public function registerUser($email, $password, $firstName, $lastName, $profileType = ModelProfile::CLIENT_PROFILE_TYPE)
	{
		return $this->registerUserInternal($email, $password, $firstName, $lastName, $profileType);
	}

	public function registerUserSilent($email, $password, $firstName, $lastName, $profileType = ModelProfile::CLIENT_PROFILE_TYPE)
	{
		return $this->registerUserInternal($email, $password, $firstName, $lastName, $profileType, true, true);
	}

	private function registerUserInternal(
		$email,
		$password,
		$firstName,
		$lastName,
		$profileType = ModelProfile::CLIENT_PROFILE_TYPE,
		$setPasswordRequired = false,
		$suppressEvents = false
	) {
		$validator = Validator::make(
			[
				'email' => $email,
				'password' => $password,
			],
			[
				'email' => 'required|email',
				'password' => 'required',
			]
		);

		if ($validator->fails()) {
			throw new ValidationException($validator);
		}

		/*
         * if user email already exists in the system we check to see if it was added by a professional inviting them or
		 * creating an appoitnment for them, and if it was then we just update the account with the new password
         */
		if ($this->isUserExists($email)) {
			if (!$this->isManualPasswordRequired($email)) {
				$validator->errors()->add('email', __('This email address is already in use on our system.', 'registration.email.already.exists'));
				throw new ValidationException($validator);
			}

			list($user, $profile) = $this->addPasswordToExistingEmail($email, $password, $firstName, $lastName, $profileType);

		} else {

			$user = $this->registerAccount($email, $password, false);

			$profile = $this->registerUserProfile($user, $firstName, $lastName, $profileType);

			if ($profile) {
				ProfileService::generateProfileAvatar($profile);
				$this->generateApiKeys($user->id, $profile->id);
			}

			$isRoleAttached = $this->attachUserRole($user);

			if ($setPasswordRequired) {
				// ask to set manual password
				$this->setManualPasswordRequired($email, true);
				$this->setRegisteredByProvider($email, true);
			}

			$isUserCreated = !empty($user);
			$isProfileCreated = !empty($profile);

			$allIsOk = ($isUserCreated && $isProfileCreated && $isRoleAttached);

			if(!$allIsOk) {
				return false;
			}
		}

		if (!$suppressEvents) {
			/**
			 * Fire an event for referral module ..
			 */
			event('user.referral.registered', array(
				'user' => $user
			));

			event(new RegisteredEvent($profile));

			$profileDetails = ProfileService::loadProfileBasicDetails($profile->id);

			/**
			 * Send user activation
			 */
			if ($profile->type == ModelProfile::CLIENT_PROFILE_TYPE) {
                $this->sendActivation($user, [
                    'name' => $profileDetails->full_name,
					'firstName' => $profileDetails->first_name,
                    'type' => $profileDetails->type,
                ]);
            }
		}

		$createdEntities = new stdClass();
		$createdEntities->user = $user;
		$createdEntities->profile = $profile;
		return $createdEntities;
	}

	private function addPasswordToExistingEmail($email, $password, $firstName, $lastName, $profileType)
	{
		$this->updatePassword($email, $password);
		$user = $this->getUserByEmail($email);
		$profile = $user->profiles()->first();
		/*
         * override old name set by professional with name used to register
         */
		$fieldNames = ['first_name', 'last_name'];
		$data = ['first_name' => $firstName, 'last_name' => $lastName];
		ProfileService::storeProfileMetaFields($profile->id, $data, $fieldNames);

		if ($profileType != $profile->type) {
			$this->registerUserProfile($user, $firstName, $lastName, $profileType);
		}

		return [$user, $profile];
	}

	public function updateEmail($oldEmail, $newEmail)
	{
		$validator = Validator::make(
			[
				'email' => $newEmail,
			],
			[
				'email' => 'required|email',
			]
		);

		if ($validator->fails()) {
			throw new ValidationException($validator);
		}

		$user = $this->getUserByEmail($oldEmail);

		if (empty($user)) {
			throw new UserNotFoundException();
		}

		$profiles = ProfileService::getUserProfiles($user->id);
		$profiles->each(function($profile) use ($user, $newEmail) {
		    if($profile->type == ModelProfile::PROVIDER_PROFILE_TYPE) {
                $profileDetails = ProfileService::loadProfileBasicDetails($profile->id);
                /**
                 * Send user activation
                 */
                $this->sendActivation($user, [
                    'name' 	=> $profileDetails->full_name,
                    'firstName' => $profileDetails->first_name,
                    'type'	=> $profileDetails->type,
                    'email' => $newEmail
                ], 'mail', false);
            }
            event(new ProfileEmailChangeEvent($profile->id, $newEmail));
        });
	}

	public function updatePassword($email, $newPassword)
	{
		$this->updateCredentials($email, ['password' => $newPassword]);
		$this->setManualPasswordRequired($email, false);
	}

	/**
	 * Update user's credentials
	 * @param $userEmail string
	 * @param $credentials array
	 * @throws UserNotFoundException
	 */
	private function updateCredentials($userEmail, $credentials)
	{
		$user = $this->getUserByEmail($userEmail);

		if (empty($user)) {
			throw new UserNotFoundException();
		}

		$this->setManualPasswordRequired($userEmail, false);

		Sentinel::update($user, $credentials);
	}

	public function registerAccount($email, $password, $activate = false)
	{
		return Sentinel::register([
			'email' => $email,
			'password' => $password
		], $activate);
	}

	/**
	 * Register profile .
	 *
	 * @param \WL\Modules\User\Models\User $user
	 * @param $firstName
	 * @param $lastName
	 * @param null $profile_type
	 * @return null
	 */
	public function registerUserProfile($user, $firstName, $lastName, $profile_type = null, $newUser = false)
	{
		$profile = null;

		if (null == $profile_type) {
			$profile_type = ModelProfile::CLIENT_PROFILE_TYPE;
		}

		if ($profile_type == ModelProfile::CLIENT_PROFILE_TYPE) {
			$profile = ProfileService::createClient($firstName, $lastName);
		} elseif ($profile_type == ModelProfile::PROVIDER_PROFILE_TYPE) {
			$profile = ProfileService::createProvider($firstName, $lastName);
		}

		if ($profile) {
			$user->profiles()
				->attach($profile, ['is_owner' => true]);
			PaymentService::createPaymentAccounts($profile->id);
			$this->generateApiKeys($user->id, $profile->id);
            if($profile_type == ModelProfile::PROVIDER_PROFILE_TYPE) {
                event(new ProviderCreateEvent($profile->id));
            }
		}

		if ($newUser) {
			event(new RegisteredEvent($profile));
		}

		return $profile;
	}

	public function attachUserRole($user, $role = null)
	{
		if ($role == null) {
			$role = self::MEMBER_ROLE;
		}

		if (!empty($user)) {
			$member = Sentinel::findRoleByName($role);

			if ($member) {
				$member->users()->attach($user);

				return true;
			}
		}

		return false;
	}

	public function generateApiKeys($userId, $profileId = null)
	{
		$profileId = $profileId ?: ProfileService::getCurrentProfileId();
		$keys = ApiKeyService::generateKeysFor($userId, $profileId);

		return $keys;
	}

	public function getApiKeys($userId = null, $profileId = null)
	{
		$userId = $userId ?: AuthorizationUtils::loggedInUser()->id;
		$profileId = $profileId ?: ProfileService::getCurrentProfileId();

		return ApiKeyService::getUserProfileKey($userId, $profileId);
	}

	public function isUserExists($email, $profile = null)
	{
		$user = $this->getUserByEmail($email);

		if ($user && $profile) {
			return (bool) $user->profiles->where('type', $profile)->first();
		}

		$result = empty($user) ? false : true;

		return $result;
	}

	public function getUserByEmail($email)
	{
		return Sentinel::findByCredentials([
			'email' => $email
		]);
	}

	public function getUserById($id)
	{
		return Sentinel::findById($id);
	}

	/**
	 * Login user by entity ..
	 *
	 * @param $user
	 * @param bool $remember
	 * @return mixed
	 */
	public function loginEntity($user, $remember = true)
	{
		$result = Sentinel::login($user, $remember);

		if ($result) {
			// Listener require User not EloquentUser
			$user = $this->getUserById($user->id);
			// simulate sentinel behavior
			event('sentinel.authenticated', $user);
		}

		return $result;
	}

	public function loginUser($email, $password, $remember = false)
	{

		if ($this->isManualPasswordRequired($email)) {
			throw new UserManualPasswordRequiredException(_('User Must set password manually'));
		}

		$credentials = [
			'email' => $email,
			'password' => $password,
		];

		$result = !empty(Sentinel::forceAuthenticate($credentials, $remember));

		return $result;
	}

	public function logoutUser()
	{
		Event::fire('user.logout');
		Sentinel::logout();
	}

	public function areValidCredentials($email, $password)
	{
		$user = $this->getUserByEmail($email);

		if (empty($user)) {
			return false;
		}

		$credentials = [
			'email' => $email,
			'password' => $password,
		];

		return Sentinel::validateCredentials($user, $credentials);
	}

	public function createReminder($email)
	{
		$user = $this->getUserByEmail($email);

		if (empty($user)) {
			return false;
		}

		Reminder::create($user);

		return true;
	}

	/**
	 * Get user by referral code ..
	 *
	 * @param $code
	 * @return mixed
	 */
	public function getUserByReferral($code)
	{
		return Sentinel::getUserRepository()
			->createModel()
			->newQuery()
			->where('referral_code', $code)
			->first();
	}

	/**
	 * Send activation to specific user .
	 *
	 * @param $user
	 * @param array $params
	 * @param string $driverSlug
	 * @param bool $newUser
	 * @return mixed
	 * @throws ActivationException
	 */
	public function sendActivation($user, array $params = [], $driverSlug = 'mail', $newUser = true)
	{
		$driver = app('activation')
			->getDriver($driverSlug);

		if (is_null($driver)) {
			throw new ActivationException(_('Invalid driver'));
		}

		$params['newUser'] = $newUser;
		$params['userId'] = $user->id;

		$activation = $driver->sendActivation($user, $user->email, $params);

		foreach ($user->profiles(ProviderProfile::class)->get() as $profile) {
			event(new ActivationSentEvent($profile->id));
		}

		return $activation;
	}

	/**
	 * Check if user by email has been activated ..
	 *
	 * @param $email
	 * @return bool
	 * @throws ActivationException
	 */
	public function isActivated($email)
	{
		$user = $this->getUserByEmail($email);

		if (!isset($user->id)) {
			throw new ActivationException(_('Invalid user id'));
		}

		return $this->isUserActivated($user);
	}

	/**
	 * Check if user has been activated ..
	 * @param User $user
	 * @return bool
	 */
	public function isUserActivated($user)
	{
		return (bool) $this->activable->completed($user);
	}

	/**
	 * Complete user activation ...
	 *
	 * @param $email
	 * @param $code
	 * @param $hash
	 * @throws ActivationException
	 * @internal param $user
	 */
	public function completeActivation($email, $code, $hash)
	{
		$user = $this->getUserByEmail($email);

		if (!isset($user->id)) {
			throw new ActivationException(_('Invalid user id'));
		}

		if (!$this->activable->existsActivation($user, $code)) {
			throw new ActivationException(_('Invalid activation'));
		}

		/**
		 * If user logged in and users not are equal than logout from current user ..
		 */
		if ($logged = Sentinel::check()) {
			if ($logged->id != $user->id) {
				self::logoutUser();
				ProfileService::invalidateCurrentProfileId();
			}
		}

		$email = Crypt::decrypt(base64_decode($hash));
		$newUser = !$user->isActivated();

		$this->updateCredentials($user->email, ['email' => $email , 'pending_email' => null]);

		if (!$this->activable->completed($user, function ($user) use ($code) {

			if ($this->activable->complete($user, $code)) {
				self::loginEntity($user, true);

				Event::fire('sentinel.authenticated', array($user));

				$user->subscribeForNotificationDispatchers($user->roles);

				foreach ($user->profiles(ProviderProfile::class)->get() as $profile) {
					event(new ActivationCompletedEvent($profile->id));
				}
			}

			return true;

		})
		) {
			throw new ActivationException(_('Activation confirmed'));
		}

		return $newUser;
	}

	/**
	 * Force activate user account ..
	 *
	 * @param $email
	 * @param bool $needAuthentication
	 * @return bool
	 * @throws ActivationException
	 * @internal param bool $needAuthentification
	 */
	public function forceActivate($email, $needAuthentication = false)
	{
		$user = $this->getUserByEmail($email);

		if (!isset($user->id)) {
			throw
			new ActivationException(_('Invalid user id'));
		}

		if (!self::isActivated($email)) {
			$this->activable->forceComplete($user);
		}

		if ($needAuthentication) {
			if (!Sentinel::check()) {
				if ($this->activable->completed($user, function () use ($user) {
					self::loginEntity($user, true);

					Event::fire('sentinel.authenticated', array($user));
				})
				) {
					;
				}
			}
		}

		return true;
	}

	public function setManualPasswordRequired($email, $required = true)
	{
		$user = $this->getUserByEmail($email);

		return $user->saveMeta('user_password_required', $required);
	}

	public function isManualPasswordRequired($email)
	{
		$user = $this->getUserByEmail($email);
		$meta = false;
		if ($user != null) {
			$meta = $user->getMeta('user_password_required');
		}

		return $meta === '1';
	}

	public function isRegisteredByProvider($email)
	{
		$user = $this->getUserByEmail($email);
		$meta = false;
		if ($user != null) {
			$meta = $user->getMeta('user_registered_by_provider');
		}

		return $meta === '1';
	}

	public function setRegisteredByProvider($email, $registeredByProvider = true)
	{
		$user = $this->getUserByEmail($email);

		return $user->saveMeta('user_registered_by_provider', $registeredByProvider);
	}

	public function setPendingEmail($id, $email)
	{
		$user = $this->getUserById($id);

		Sentinel::update($user, ['pending_email' => $email]);
	}
}

<?php namespace WL\Modules\User\Services;

use WL\Modules\User\Models\User;

interface UserServiceInterface
{
	/**
	 * Register user
	 * @param $email string
	 * @param $password string
	 * @param $firstName string|null
	 * @param $lastName string|null
	 * @param $isProvider boolean|false
	 * @return boolean
	 */
	public function registerUser($email, $password, $firstName, $lastName, $isProvider = false);

	/**
	 * Register user without events
	 * @param $email string
	 * @param $password string
	 * @param $firstName string|null
	 * @param $lastName string|null
	 * @param $isProvider boolean|false
	 * @return boolean
	 */
	public function registerUserSilent($email, $password, $firstName, $lastName, $isProvider = false);


	/**
	 * Update user's email
	 * @param $oldEmail string
	 * @param $newEmail string
	 * @return mixed
	 */
	public function updateEmail($oldEmail, $newEmail);

	/**
	 * Update user's password
	 * @param $email string
	 * @param $newPassword string
	 * @return mixed
	 */
	public function updatePassword($email, $newPassword);

	/**
	 * Get user by email
	 * @param $email string
	 * @return \WL\Modules\User\Models\User|false
	 */
	public function getUserByEmail($email);

	/**
	 * Register the actual account if needed.
	 *
	 * @param      $email
	 * @param      $password
	 * @param bool $activate
	 *
	 * @return mixed
	 */
	public function registerAccount($email, $password, $activate = true);

	/**
	 * Create the user's first profile.
	 * Can be called multiple times.
	 * @param User $user
	 * @param $firstName
	 * @param $lastName
	 * @param $profile_type
	 *
	 * @return bool
	 */
	public function registerUserProfile($user, $firstName, $lastName, $profile_type = null);

	/**
	 * Attach a role to the user.
	 *
	 * @param      $user
	 * @param null $role
	 *
	 * @return bool
	 */
	public function attachUserRole($user, $role = null);

	/**
	 * Generate API keys for an user-profile combination
	 * @param $userId
	 * @param null $profileId
	 * @return mixed
	 */
	public function generateApiKeys($userId, $profileId = null);

	/**
	 * Get ApiKey by user and profile
	 * @param null $userId
	 * @param null $profileId
	 * @return mixed
	 */
	public function getApiKeys($userId = null, $profileId = null);

	/**
	 * Is user registered
	 * @param $email string
	 * @return boolean
	 */
	public function isUserExists($email, $profile = null);

	/**
	 * Login user
	 * @param $email string
	 * @param $password string
	 * @param $remember boolean
	 * @return boolean
	 */
	public function loginUser($email, $password, $remember = false);

	/**
	 * Logout current logged in user
	 * @return mixed
	 */
	public function logoutUser();

	/**
	 * Validate user credentials
	 * @param $email string
	 * @param $password string
	 * @return boolean
	 */
	public function areValidCredentials($email, $password);

	/**
	 * Create reminder
	 * @param $email string
	 * @return boolean
	 */
	public function createReminder($email);

	/**
	 * Set the value of user_password_required - see \WL\Modules\User\Services\UserServiceInterface::setManualPasswordRequired for more about this
	 *
	 * @param $email
	 * @param bool|true $required
	 * @return mixed
	 */
	public function setManualPasswordRequired($email, $required = true);

	/**
	 * Used when we don't want to display the "current password" section of the change password form (e.g. for social login users that need to set their password after signing up).
	 * If the manual password is required this then means that the user needs to set their password (presumably for the first time) and so we do not show them the "current password" section.
	 *
	 * @param $email
	 * @return mixed
	 */
	public function isManualPasswordRequired($email);

	/**
	 * @param $email
	 * @param bool $registeredByProvider
	 * @return mixed
     */
	public function setRegisteredByProvider($email, $registeredByProvider = true);

	/**
	 * @param $email
	 * @return mixed
     */
	public function isRegisteredByProvider($email);

	/**
	 * @param $email
	 * @return mixed
	 */
	public function setPendingEmail($userId, $email);


}

<?php namespace WL\Modules\User\Presenters;

use WL\Modules\Attachment\Facades\AttachmentService;
use WL\Presenters\Presenter;

class UserPresenter extends Presenter
{
	public function name()
	{
		return $this->first_name . ' ' . $this->last_name;
	}

	public function username()
	{
		$exploded = explode('@', $this->email);

		return $exploded[0];
	}

	public function attachment($attachmentName)
	{
		// we will need an empty model object to make sure all the methods exist.
		$attachment = AttachmentService::findFile($attachmentName, $this->entity, false);
		return ($attachment) ? $attachment->present() : null;
	}

	public function avatar()
	{
		return AttachmentService::findFile('avatar', $this->entity, false);
	}
}

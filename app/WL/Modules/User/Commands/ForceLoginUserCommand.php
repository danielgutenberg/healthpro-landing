<?php namespace WL\Modules\User\Commands;


use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\User\Exceptions\ActivationException;
use WL\Modules\User\Facades\User;
use WL\Security\Commands\AuthorizableCommand;

class ForceLoginUserCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'user.forceLoginUserCommand';

	protected $userId = null;
	protected $profileType = null;
	protected $profileId = null;

	function __construct($userId, $profileType = null, $profileId = null)
	{
		$this->userId = $userId;
		$this->profileType = $profileType;
		$this->profileId = $profileId;
	}

	public function handle()
	{
		$user = null;

		if (is_string($this->userId)) {
			$user = User::getUserByEmail($this->userId);
		} else {
			$user = User::getUserById($this->userId);
		}

		if (!$user) {
			throw new ActivationException("User '{$this->userId}' not found");
		}

		$profiles = ProfileService::getUserProfiles($user->id);

		if (null != $this->profileId) {
			$profileOwner = ProfileService::getOwner($this->profileId);

			if ($profileOwner->id != $user->id) {
				throw new ActivationException("Cannot login user '{$this->userId}' to this profile");
			}

			$profile = ProfileService::getProfileById($this->profileId);
		} else if (null === $this->profileType) {
			$profile = $profiles->first();
		} else {
			$profile = $profiles->first(function ($k, $p) {
				return $this->profileType === $p->type;
			});
		}

		if (null === $profile) {
			throw new ActivationException("Cannot login user '{$this->userId}'");
		}

		// login
		User::loginEntity($user, false);

		// set profile
		ProfileService::setCurrentProfileId($profile->id);

		return ProfileService::getCurrentProfileId() === $profile->id;
	}
}

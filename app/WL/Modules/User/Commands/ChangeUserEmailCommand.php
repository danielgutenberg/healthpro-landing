<?php namespace WL\Modules\User\Commands;


use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Facades\AuthorizationUtils;
use WL\Modules\User\Facades\User;

class ChangeUserEmailCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'user.changeUserEmailCommand';

	private $newEmail;
	private $oldEmail;
	private $currentPassword;
	private $validator;

	function __construct($newEmail, $currentPassword, $oldEmail)
	{
		$this->newEmail = $newEmail;
		$this->currentPassword = $currentPassword;
		$this->oldEmail = $oldEmail;

		$messages = [
			'email.required' => __('E-mail address is required.'),
			'email.email' => __("Sorry, that doesn't look like an email."),
			'currentPassword.required' => __('Enter your current password.'),
		];

		$this->validator = Validator::make(
			[
				'email' => $this->newEmail,
				'currentPassword' => $this->currentPassword,
				'oldEmail' => $this->oldEmail
			],
			[
				'email' => 'required|email',
				'oldEmail' => 'sometimes|email',
				'currentPassword' => 'required',
			],
			$messages
		);

		$this->validator->after(function ($validator) {
			if ($this->oldEmail == '' && !User::areValidCredentials(AuthorizationUtils::loggedInUser()->email, $this->currentPassword)) {
				$validator->errors()->add('currentPassword', __('Current password is incorrect.', 'change.password.current.incorrect'));
			} elseif ($this->oldEmail != '' && AuthorizationUtils::loggedInUser()->email != $this->oldEmail) {
				$validator->errors()->add('oldEmail', __('This is not your current email. Please reenter your current email address.', 'registration.email.old.incorrect'));
			} elseif (AuthorizationUtils::loggedInUser()->email == $this->newEmail) {
				$validator->errors()->add('email', __('This is your current email address. Please use another email address.', 'registration.email.already.exists'));
			} elseif (User::isUserExists($this->newEmail)) {
				$validator->errors()->add('email', __('This email address is already in use on our system.', 'registration.email.already.exists'));
			}
		});
	}

	public function handle()
	{
		if ($this->validator->fails())
			throw new ValidationException($this->validator);

		User::updateEmail(AuthorizationUtils::loggedInUser()->email, $this->newEmail);
	}
}

<?php namespace WL\Modules\User\Commands;


use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Facades\AuthorizationUtils;
use WL\Modules\User\Exceptions\ActivationException;
use WL\Modules\User\Facades\User;

class SwitchCurrentUserCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'user.switchCurrentUserCommand';

	function __construct($toUserId)
	{
		$this->toUserId = $toUserId;
	}

	public function handle()
	{
		$currentUser = AuthorizationUtils::loggedInUser();

		if (!env('ENABLE_SWITCH_CURRENT_USER'))
			throw new ActivationException('SwitchUser feature is disabled');

		if (!$currentUser)
			throw new ActivationException('Access deny');

		if (!($toUser = User::getUserById($this->toUserId)))
			throw new ActivationException("User '{$this->toUserId}' not found");

		User::loginEntity($toUser, false);
		$profiles = ProfileService::getUserProfiles($toUser->id);
		if ($profile = $profiles->first()) {
			ProfileService::setCurrentProfileId($profile->id);
		}
	}
}

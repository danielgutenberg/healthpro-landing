<?php namespace WL\Modules\User\Commands;


use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\User\Facades\User;

class ResetPasswordCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'user.resetPassword';

	private $email;

	private $validator;

	function __construct($email)
	{
		$this->email = $email;

		$this->validator = Validator::make(
			[
				'email' => $this->email,
			],
			[
				'email' => 'required|email',
			]
		);

	}

	public function handle()
	{
		$this->validator->after(function ($validator) {
			$user = User::getUserByEmail($this->email);
			if (!$user) {
				$validator->errors()->add('email', __("Email address does not exist in system", 'registration.email.does.not.exists'));
			} else {
				if ($user->profiles->isEmpty()) {
					$validator->errors()->add(
						'email',
						__(
							"This account has been deactivated. Please speak to customer support if you would like to reactivate the account",
							'registration.email.does.not.exists'
						)
					);
				}
			}
		});

		if ($this->validator->passes())
			User::createReminder($this->email);
		else
			throw new ValidationException($this->validator);
	}
}

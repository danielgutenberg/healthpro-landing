<?php namespace WL\Modules\User\Commands;


use App\Http\Requests\ApiRequest;
use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Modules\Order\Commands\SendOrderInvoiceEmailCommand;
use WL\Modules\Order\Events\SignupOrderInvoiceMailEvent;
use WL\Modules\Payment\Commands\AddCardCommand;
use WL\Modules\Payment\Commands\GetCardCommand;
use WL\Modules\Payment\Commands\RemoveAllPaymentMethodsCommand;
use WL\Modules\Payment\Facades\PaymentPaypalService;
use WL\Modules\Payment\Models\PaymentPaypalSubscription;
use WL\Modules\Payment\Transformers\CreditCardFormTransformer;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\Models\ProviderProfile;
use WL\Modules\Provider\Commands\StartProviderPlanCommand;
use WL\Modules\Search\Facades\OrderService;
use WL\Modules\User\Facades\User;
use WL\Security\Commands\AuthorizableCommand;
use Illuminate\Support\Facades\DB;
use stdClass;
use WL\Security\Facades\AuthorizationUtils;

/**
 * Class RegisterUserCommand
 * @package WL\Modules\User\Commands
 */
class RegisterUserCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'user.register';

	private $email;
	private $password;
	private $firstName;
	private $lastName;
	private $profileType;
	private $code;
	private $request;
	private $profileId;
	private $productId;
	private $paymentMethodId;

	private $validator;
	private $messages;
	private $isAjaxCall;
	private $isApi;


	function __construct(ApiRequest $request, $email, $password, $firstName, $lastName, $profileType, $code, $isApi = false, $isAjaxCall = false)
	{
		$this->request = $request;
		$this->email = $email;
		$this->password = $password;
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->profileType = $profileType;
		$this->code = $code;
		$this->paymentType = $request->input('payment_type', null);
		$this->profileId = $request->input('profile_id', null);
		$this->paymentMethodId = $request->get('payment_method_id');
		$this->productId = $request->input('product_id');
		$this->isApi = $isApi;
		$this->isAjaxCall = $isAjaxCall;

		$this->messages = [
			'email.required' => __('E-mail address is required.'),
			'email.email' => __('Please enter a valid email address.'),
			'password.required' => __('Password is required.'),
			'password.between' => __('Password length must be at least :min characters.'),
			'profileType.in' => __('Profile type not allowed'),
			'provider.required' => __('Provider is required'),
		];

		$this->validator = $this->makeBaseValidator();

	}

	public function handle()
	{
		$results = new stdClass();

		if ($this->validator->passes()) {
			DB::transaction(function() use (&$results) {
				if ($this->profileId && $this->profileId == ProfileService::getCurrentProfileId()) {
					$profile = ProfileService::getCurrentProfile();
				} else {
					$user = User::getUserByEmail($this->email);
					$login = false;
					if (!empty($user) && $this->profileType == ModelProfile::PROVIDER_PROFILE_TYPE) {
						$professional = $user->profiles->where('type', ModelProfile::PROVIDER_PROFILE_TYPE)->first();
						if (!$professional) {
							$login = User::areValidCredentials($this->email, $this->password);
						}
					}

					if ($login) {
						User::loginEntity($user);
						$profile = ProfileService::createAndAssociateProfile($user->id, ModelProfile::PROVIDER_PROFILE_TYPE, $this->firstName, $this->lastName);
						ProfileService::setCurrentProfileId($profile->id);
					} else {
						$result = User::registerUser($this->email, $this->password, $this->firstName, $this->lastName, $this->profileType);

						ProfileService::setCurrentProfileId($result->profile->id);
						$profile = $result->profile;
						User::loginEntity($result->user);
					}
				}
				if ($this->profileType == ModelProfile::PROVIDER_PROFILE_TYPE) {
					if ($this->request->input('payment_type') == 'cc') {
					    if($this->paymentMethodId) {
					        $data = [
                                'profile_id' => $profile->id,
                                'credit_card_id' => $this->paymentMethodId
                            ];
					        $cardResult = $this->dispatch(new GetCardCommand($data));
                        }
                        if(!isset($cardResult) || !$cardResult) {
                            $this->dispatch(new RemoveAllPaymentMethodsCommand(['profile_id' => $profile->id]));

                            $data = (new CreditCardFormTransformer())->transform($this->request);
                            $data['profile_id'] = $profile->id;
                            $cardResult = $this->dispatch(new AddCardCommand($data));
                        }

						$paymentMethodId = $cardResult->getPaymentMethod()->id;
						$data = [
							'profileId' => $profile->id,
							'paymentMethodId' => $paymentMethodId,
							'productId' => $this->productId,
							'couponId' => $this->request->get('couponId'),
						];
						$order = $this->dispatch(new StartProviderPlanCommand($data));

						session()->forget('productId');
						if(OrderService::bookOrder($order->id, false, true, false)) {
						    $this->dispatch(new SendOrderInvoiceEmailCommand($order->id, SignupOrderInvoiceMailEvent::class));
                        }
					}
					if ($this->request->input('payment_type') == 'paypal') {
						session(['productId' => $this->productId]);
						$data = [
							'profileId' => $profile->id,
							'paymentMethodId' => null,
							'productId' => $this->productId,
                            'couponId' => $this->request->get('couponId'),
						];
						$order = $this->dispatch(new StartProviderPlanCommand($data));

						$data = PaymentPaypalService::getCartData($order, PaymentPaypalSubscription::PAYPAL_DESCRIPTION);
						$response = PaymentPaypalService::setExpressCheckout($data, true);

						$results->paypal_link = $response['paypal_link'];
					}
				} else {
					$results->redirect_to = route('home');
				}
			});


			if ($this->isApi) {
				$keys = User::getApiKeys();
				if ($keys != null) {
					$results->api_access_key = $keys->api_access_key;
					$results->api_secret_key = $keys->api_secret_key;
				}
			}

			if ($this->isApi || $this->isAjaxCall) {

				$currentProfile = ProfileService::getCurrentProfile();
				if (!$currentProfile) {
					$this->validator->errors()->add('email', __("The profile set up with this email has been deleted, please speak to Customer Service"));
					throw new ValidationException($this->validator);
				}
				$results->profile_type = $currentProfile->type;

				$results->profile_id = $currentProfile->id;
			}

			$results->verifiedEmail = AuthorizationUtils::loggedInUser()->isActivated();

			$results->isSpecialUser = AuthorizationUtils::inRoles(['administrator']);

			return $results;

		} else {
			throw new ValidationException($this->validator);
		}

	}

	private function makeBaseValidator()
	{
		$data = [
			'email' => $this->email,
			'password' => $this->password,
			'profileType' => $this->profileType,
			'code' => $this->code,
			'profileId' => $this->profileId
		];
		$validation = [
			'email' => 'required_without:profileId|email',
			'password' => 'required_without:profileId|between:7,255',
			'code' => 'sometimes|string',
			'profileType' => 'sometimes|in:' . ModelProfile::CLIENT_PROFILE_TYPE . ',' . ModelProfile::PROVIDER_PROFILE_TYPE
		];
		if ($this->profileType == ModelProfile::PROVIDER_PROFILE_TYPE) {
			$data['productId'] = $this->productId;
			$validation['productId'] = 'required|integer';
		}

		$validator = Validator::make($data, $validation,$this->messages);

		return $validator;
	}
}

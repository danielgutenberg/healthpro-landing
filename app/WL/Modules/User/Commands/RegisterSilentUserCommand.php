<?php namespace WL\Modules\User\Commands;


use WL\Modules\Profile\Commands\ValidatableCommand;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\User\Facades\User;

/**
 * Class RegisterUserCommand
 * @package WL\Modules\User\Commands
 */
class RegisterSilentUserCommand extends ValidatableCommand
{
	const IDENTIFIER = 'user.registerSilentCommand';

	protected $rules = [
		'email' => 'required|email',
	];

	protected $messages = [
		'email.required' => 'Please provide a valid email',
	];

	public function validHandle()
	{
		$email = $this->inputData['email'];
		$firstName = array_get($this->inputData, 'first_name', '');
		$lastName = array_get($this->inputData, 'last_name', '');
		$password = bin2hex(openssl_random_pseudo_bytes(32));

		User::registerUserSilent($email, $password, $firstName, $lastName, ModelProfile::CLIENT_PROFILE_TYPE);
		$user = User::getUserByEmail($email);

		return $user;
	}
}

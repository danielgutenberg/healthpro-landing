<?php namespace WL\Modules\Profile\Commands;


use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\Profile\SecurityPolicies\ProfileAccessPolicy;
use WL\Modules\User\Facades\User;

class CheckEmailExistsCommand extends ValidatableCommand
{
	const IDENTIFIER = 'profile.checkUserExists';

	protected $rules = [
		'email' => 'required|email',
		'profileType' => 'required|in:' . ModelProfile::CLIENT_PROFILE_TYPE . ',' . ModelProfile::PROVIDER_PROFILE_TYPE
	];

	protected $messages = [
		'slug.email' => 'Please enter a valid email',
		'slug.required' => 'Please enter an email'
	];

	public function validHandle()
	{
		$email = $this->inputData['email'];

		return User::isUserExists($email, $this->inputData['profileType']);
	}
}

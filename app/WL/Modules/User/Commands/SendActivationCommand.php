<?php namespace WL\Modules\User\Commands;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\User\Exceptions\ActivationException;
use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\User\Facades\User;

class SendActivationCommand extends AuthorizableCommand {

	const IDENTIFIER = 'user.sendActivation';
	/**
	 * @var
	 */
	private $user;


	function __construct($user = null) {
		$this->user = $user;
	}

	/**
	 * Perform user activation ..
	 * @param ModelProfileService $profileService
	 * @return bool
	 */
	public function handle(ModelProfileService $profileService) {

		if (null === $this->user) {
			$this->user = Sentinel::check();
		}

		if (!$this->user) {
			return false;
		}

		if ($this->user->isActivated() && !$this->user->pending_email) {
			throw new ActivationException(_('This email has already been verified'));
		}

		$profileDetails = $profileService->loadProfileBasicDetails($profileService->getCurrentProfileId());

		$data = [
			'name' 	=> $profileDetails->full_name,
			'firstName' => $profileDetails->first_name,
			'type'	=> $profileDetails->type
		];

		$newUser = true;
		if ($this->user->pending_email) {
			$data['email'] = $this->user->pending_email;
			$newUser = false;
		}

		return User::sendActivation($this->user, $data, 'mail', $newUser);
	}
}

<?php namespace WL\Modules\User\Commands;

use Cartalyst\Sentinel\Addons\Social\Tests\LinkRepositoryTest;

use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use stdClass;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Facades\ProviderOriginatedService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\SocialId\Services\SocialIdServiceInterface;
use Illuminate\Database\QueryException;
use Redirect;
use WL\Modules\User\Facades\User;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Facades\AuthorizationUtils;
use WL\Wizard\Services\Facade\WizardService;
use Cartalyst\Sentinel\Addons\Social\Repositories\LinkRepository;

class LoginUserCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'user.login';

	private $email;
	private $password;
	private $remember;
	private $profileType;
	private $isAjaxCall;
	private $socialData;
	private $redirectTo;

	private $socialValidator;
	private $validator;
	/**
	 * @var
	 */
	private $isApi;

	function __construct($email, $password, $remember, $profileType, $isApi, $isAjaxCall, $socialData = null, $redirectTo = '')
	{
		$this->email = $email;
		$this->password = $password;
		$this->remember = $remember;
		$this->profileType = session('profile_type') ?: $profileType;
		$this->isApi = $isApi;
		$this->isAjaxCall = $isAjaxCall;
		$this->socialData = $socialData;
		$this->redirectTo = $this->getRedirectUrl($redirectTo);

		$profileTypes = [ModelProfile::CLIENT_PROFILE_TYPE, ModelProfile::PROVIDER_PROFILE_TYPE];

		$messages = [
			'email.required' => __('E-mail address is required.'),
			'email.email' => __('Please enter a valid email address.'),
			'password.required' => __('Password is required.'),
			'provider.required' => __('Provider is required'),
		];

		if ($this->socialData != null) {
			$this->password = bin2hex(openssl_random_pseudo_bytes(32));
			$this->socialValidator = Validator::make(
				[
					'provider' => isset($this->socialData['provider']) ? $this->socialData['provider'] : null,
				],
				[
					'provider' => 'required|string',

				],
				$messages
			);
		}

		$this->validator = Validator::make(
			[
				'email' => $this->email,
				'password' => $this->password,
				'profile_type' => $this->profileType
			],
			[
				'email' => 'required|email',
				'password' => 'required',
				'profile_type' => "sometimes|in:" . join(',', $profileTypes)
			],
			$messages
		);

	}

	public function handle(SocialIdServiceInterface $socialIdService)
	{
		$this->validator->after(function ($validator) {
			if (!User::areValidCredentials($this->email, $this->password)) {
				$validator->errors()->add('email', __("Email or password is not valid", 'registration.email.or.password.not.valid'));
			}
		});

		$result = new stdClass();

		if ($this->socialData != null) {
			if ($this->socialValidator->passes()) {

				//get uid from accessToken
				$socialInfo = $socialIdService->getSocialInfo($this->socialData['provider'], $this->socialData['accessToken']);

				$user = User::getUserByEmail($socialInfo->email);

				/*
				 * if user is not yet registered, register them
				 */
				try {
					if (!$user) {
						$firstName = $this->stripIllegalChars($socialInfo->firstName);
						$lastName = $this->stripIllegalChars($socialInfo->lastName);

						User::registerUser($socialInfo->email, $this->password, $firstName, $lastName, $this->profileType);
						$user = User::getUserByEmail($socialInfo->email);
						if ($user != null) {
							User::setManualPasswordRequired($socialInfo->email);

							$socialIdService->registerMe($this->socialData['provider'], $socialInfo->uid, $user->id, $this->socialData['accessToken']);
						}
					} else {
						/*
						 * if user signed up through the site not through the social network and then logs in
						 * using the social network we need to save the social credentials
						 */
						if (!$link = $socialIdService->getUserBySocialId($this->socialData['provider'], $socialInfo->uid)) {
							$repository = new LinkRepository();
							$link = $repository->findLink($this->socialData['provider'], $socialInfo->uid);
							$link->user_id = $user->id;
							$link->save();
						}
					}
				} catch (QueryException $e) {
					if (strpos($e->getMessage(), 'social_provider_user_id_unique') > 0) {
						$successRoute = $this->processDuplicate($socialIdService, $e->getBindings()[2]);

						if (!$this->isApi && !$this->isAjaxCall) {
							return Redirect::route($successRoute)
								->with('success', 'Successfully authenticated');
						}
					} else {
						throw $e;
					}
				}

				$user = User::getUserByEmail($socialInfo->email);
				User::loginEntity($user, $this->remember);
				$this->setProfile();


			} else {
				throw new ValidationException($this->socialValidator);
			}
		} else {
			if ($this->validator->passes()) {
				User::loginUser($this->email, $this->password, $this->remember);
				$this->setProfile();
			} else {
				throw new ValidationException($this->validator);
			}
		}

		if ($this->isApi) {
			$keys = User::getApiKeys();
			if ($keys != null) {
				$result->api_access_key = $keys->api_access_key;
				$result->api_secret_key = $keys->api_secret_key;
			}
		}

		if ($this->isApi || $this->isAjaxCall) {

			$currentProfile = ProfileService::getCurrentProfile();
			if (!$currentProfile) {
				$this->validator->errors()->add('email', __("The profile set up with this email has been deleted, please speak to Customer Service"));
				throw new ValidationException($this->validator);
			}
			$result->profile_type = $currentProfile->type;

			// add redirectTo parameter
			// we want to add custom redirects for professional and client.
			// client is always redirected to the client homepage
			$result->redirect_to = $this->getRedirectTo($currentProfile);

			$result->profile_id = $currentProfile->id;
		}

		$result->verifiedEmail = AuthorizationUtils::loggedInUser()->isActivated();

		$result->isSpecialUser = AuthorizationUtils::inRoles(['administrator']);

		return $result;
	}

	private function setProfile()
	{
		$loggedInUser = AuthorizationUtils::loggedInUser();
		if ($loggedInUser->force_login) {
			$loggedInUser->force_login = 0;
			$loggedInUser->save();
		}
 		$loginProfileId = session('login_profile');
		// if profile id is set and it belongs to the logged in user then log user in to specific profile
		if ($loginProfileId) {
			$loginProfileUser = ProfileService::getOwner($loginProfileId);
			if ($loginProfileUser->id == $loggedInUser->id) {
				return ProfileService::setCurrentProfileId(session('login_profile'));
			}
		}

		// login to first profile with appropriate profile type
		if ($this->profileType) {
			$profiles = ProfileService::getUserProfiles($loggedInUser->id, $this->profileType);
			if (!$profiles->isEmpty()) {
				ProfileService::setCurrentProfileId($profiles[0]->id);
			}
		}
	}

	private function getRedirectTo($profile)
	{
		// handle the redirectTo param and use it if exists
		if ($this->redirectTo) {
			return $this->redirectTo;
		}

		// by default professional gets redirected to his calendar
		if ($profile->type == ModelProfile::PROVIDER_PROFILE_TYPE) {
			if (ProfileService::loadProfileMetaField($profile->id, 'old_returning_professional')) {
				return route('dashboard-schedule');
			}
			if (!WizardService::isWizardCompleted('profile_setup') && !$profile->is_searchable) {
				return route('wizard_continue', ['wizard' => 'profile_setup']);
			}

			return route('dashboard-schedule');
		}

		$pros = ProviderOriginatedService::getClientProviders($profile->id);
		if (!$pros->isEmpty()) {
			return route('dashboard-my-providers');
		}

		return route('home');
	}

	private function getRedirectUrl($url)
	{
		if (session('redirect_url')) {
			$redirect = $this->filterRedirectUrl(session('redirect_url'));
			if ($redirect) {
				return $redirect;
			}
		}

		return $this->filterRedirectUrl($url);
	}

	// make sure we receive a valid url
	private function filterRedirectUrl($url)
	{
		$url = trim($url);

		if ('' === $url)
			return $url;

		# make sure we have a valid url
		$url = url($url);

		$currentUrlHost = parse_url($url, PHP_URL_HOST);
		if ($currentUrlPort = parse_url($url, PHP_URL_PORT))
			$currentUrlHost .= ':' . $currentUrlPort;

		# make sure we do not redirect to external url
		if ($currentUrlHost === request()->server->get('HTTP_HOST'))
			return $url;

		return null;
	}

	private function processDuplicate($socialIdService, $id)
	{
		$link = $socialIdService->findLink($id);
		$token = $link->oauth2_access_token;
		$socialInfo = $socialIdService->getSocialInfo($this->provider, $token);

		$socialIdService->deleteLink($id);

		$user = User::getUserByEmail($socialInfo->email);

		User::loginEntity($user);
		ProfileService::setCurrentProfileId($user->profiles->first()->id);
	}

	private function stripIllegalChars($string)
	{
		return preg_replace("/[^\p{Latin}0-9 ']/", '', $string);
	}
}

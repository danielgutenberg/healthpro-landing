<?php namespace WL\Modules\User\Commands;


use Illuminate\Support\Facades\Session;
use Sentinel;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Facades\AuthorizationUtils;
use WL\Modules\User\Facades\User as UserService;
use WL\Modules\Profile\Facades\ProfileService;

class LogoutUserCommand extends AuthorizableCommand {

	const IDENTIFIER = 'user.logout';

	public function handle() {
		if( AuthorizationUtils::isLoggedIn() ) {
			UserService::logoutUser();
			ProfileService::invalidateCurrentProfileId();
			Session::flush();
		}
	}
}

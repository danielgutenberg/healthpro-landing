<?php namespace WL\Modules\User\Commands;


use WL\Modules\User\Facades\User;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Facades\AuthorizationUtils;

class LoadCurrentUserCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'user.loadCurrentUser';

	public function handle()
	{
		$user = AuthorizationUtils::loggedInUser();
		$manualPasswordRequired = User::isManualPasswordRequired($user->email);

		return [$user, $manualPasswordRequired];
	}
}

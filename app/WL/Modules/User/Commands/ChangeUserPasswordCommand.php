<?php namespace WL\Modules\User\Commands;


use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Facades\AuthorizationUtils;
use WL\Modules\User\Facades\User;

class ChangeUserPasswordCommand extends AuthorizableCommand
{
	const IDENTIFIER = 'user.changeUserPasswordCommand';

	private $currentPassword;
	private $newPassword;
	private $newConfirmedPassword;
	private $validator;
	private $forgotPassword;

	function __construct($currentPassword, $newPassword, $newConfirmedPassword, $forgotPassword = false)
	{
		$this->currentPassword = $currentPassword;
		$this->newPassword = $newPassword;
		$this->newConfirmedPassword = $newConfirmedPassword;
		$this->forgotPassword = $forgotPassword;

		$messages = [
			'currentPassword.required' => __('Enter your current password.'),
			'newPassword.required' => __("Enter new password."),
			'newPassword.different' => __("New password must be different than the old one"),
			'newPassword.between' => __('Password length must be at least :min characters.'),
			'newPassword_confirmation.required' => __("Confirm new password."),
			'newPassword_confirmation.same' => __("Please make sure your two passwords match"),
		];

		$this->validator = Validator::make(
			[
				'currentPassword' => $this->currentPassword,
				'newPassword' => $this->newPassword,
				'newPassword_confirmation' => $this->newConfirmedPassword,
			],
			[
				'currentPassword' => 'required',
				'newPassword' => 'required|different:currentPassword|between:7,255|confirmed',
				'newPassword_confirmation' => 'required|same:newPassword',
			],
			$messages
		);

		$this->validatorNoCurrentPassword = Validator::make(
			[
				'newPassword' => $this->newPassword,
				'newPassword_confirmation' => $this->newConfirmedPassword,
			],
			[
				'newPassword' => 'required|between:7,255|confirmed',
				'newPassword_confirmation' => 'required|same:newPassword',
			],
			$messages
		);

		$this->validator->after(function ($validator) {
			if (!User::areValidCredentials(AuthorizationUtils::loggedInUser()->email, $this->currentPassword)) {
				$validator->errors()->add('currentPassword', __('Current password is incorrect.', 'change.password.current.incorrect'));
			}
		});
	}

	public function handle()
	{
		$validator = $this->validator;

		if(User::isManualPasswordRequired(AuthorizationUtils::loggedInUser()->email) || $this->forgotPassword){
			$validator = $this->validatorNoCurrentPassword;
		}
		if ($validator->fails())
			throw new ValidationException($validator);

		User::updatePassword(AuthorizationUtils::loggedInUser()->email, $this->newPassword);
	}
}

<?php namespace WL\Modules\User\Commands;


use WL\Security\Commands\AuthorizableCommand;
use WL\Modules\User\Exceptions\ActivationException;
use WL\Modules\User\Facades\User;

class ActivateUserCommand extends AuthorizableCommand {

	const IDENTIFIER = 'user.activate';
	/**
	 * @var
	 */
	private $code;
	/**
	 * @var
	 */
	private $email;
	/**
	 * @var
	 */
	private $hash;

	function __construct($code, $email, $hash) {
		$this->code = $code;
		$this->email = $email;
		$this->hash = $hash;
	}

	/**
	 * Perform user activation ..
	 */
	public function handle() {
		try {
			if( $activation = app('activation')->getDefaultDriver() ) {
				return User::completeActivation($this->email, $this->code, $this->hash);
			}
		} catch( ActivationException $e ) {
			throw new ActivationException($e);
		}
	}
}

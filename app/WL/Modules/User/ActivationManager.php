<?php namespace WL\Modules\User;

use WL\Modules\User\Drivers\ActivationMailDriver;
use WL\Modules\User\Drivers\ActivationSmsDriver;
use WL\Yaml\Contracts\Parsable;

class ActivationManager {

	const CONFIGURATION_DIR = 'user/activation';

	/**
	 * @var array
	 */
	protected $drivers = array(
		'mail' => ActivationMailDriver::class,
		'sms'  => ActivationSmsDriver::class,
	);

	/**
	 * Loaded instances ..
	 *
	 * @var array
	 */
	protected $loaded  = array();

	/**
	 * @var
	 */
	private $parser;

	protected $configuration;

	public function __construct(Parsable $parser) {

		$this->parser = $parser;
	}

	/**
	 * @param $name
	 * @param $arguments
	 * @return bool
	 */
	public static function __callStatic($name, $arguments) {
		if( $name == 'mail' ) {
			return app('activation')->getDriver('mail')->sendActivation(implode(', ', $arguments));
		} elseif( $name == 'sms' ) {
			return app('activation')->getDriver('sms')->sendActivation(implode(', ', $arguments));
		}

		return false;
	}

	/**
	 * Create drive by slug ...
	 *
	 * @param $slug
	 * @return bool
	 */
	public function createDriver($slug) {
		if( ! $driver = $this->drivers[$slug] )
			return false;

		if( ! isset($this->loaded[$slug])) {
			$instance =  new $driver;

			$this->loaded[$slug] = $instance;
		}

		return $this->loaded[$slug];
	}

	/**
	 * Get driver by slug ...
	 *
	 * @param $slug
	 * @return bool
	 */
	public function getDriver($slug = null) {
		if( is_null($slug) )
			self::getDefaultDriver();

		return self::createDriver($slug);
	}

	/**
	 * Get default driver ..
	 *
	 * @return bool
	 */
	public function getDefaultDriver() {
		if(! $slug = self::getDefaultDriverSlug())
			return false;

		return self::createDriver($slug);
	}

	/**
	 * Get default driver as slug ..
	 *
	 * @return bool
	 */
	public function getDefaultDriverSlug() {
		$configuration = self::getConfiguration();

		if(! $slug = $configuration['default'])
			return false;

		return $slug;
	}



	/**
	 * Get activation default driver
	 *
	 * @return bool|mixed
	 */
	protected function setConfiguration() {
		if(! $this->configuration) {
			if(! is_dir(base_path('config/wl/' . self::CONFIGURATION_DIR)))
				return false;

			$this->configuration = $this->parser->parse(
				('/wl/' . self::CONFIGURATION_DIR) . DIRECTORY_SEPARATOR . 'configuration'
			);
		}
	}

	/**
	 * Get configuration ..
	 *
	 * @return mixed
	 */
	protected function getConfiguration() {
		if(! $configuration = $this->configuration)
			self::setConfiguration();

		return $this->configuration;
	}
}

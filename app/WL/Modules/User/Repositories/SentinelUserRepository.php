<?php namespace WL\Modules\User\Repositories;

use WL\Modules\User\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use WL\Modules\User\Exceptions\InvalidPhoneException;
use WL\Modules\User\Exceptions\UserNotLoggedInException;

class SentinelUserRepository implements UserRepository
{
	private $updateTypes = ['user', 'meta', 'phones'];

	/**
	 * @param int $id
	 * @return \WL\Modules\User\Models\User
	 */
	public function getById($id)
	{
		return Sentinel::findById($id);
	}
	/**
	 * @param \WL\Modules\User\Models\User $user
	 * @param array $fields
	 * @return mixed
	 */
	public function updateUser($user, array $fields)
	{
		Sentinel::update($user, $fields);

		return $this;
	}

	/**
	 * @param \WL\Modules\User\Models\User $user
	 * @param array $meta
	 * @return mixed
	 */
	public function updateMeta($user, array $meta)
	{
		$user->updateMeta($meta);

		return $this;
	}

	/**
	 * @return \WL\Modules\User\Models\User
	 * @throws UserNotLoggedInException
	 */
	public function getCurrentUser()
	{
		if (Sentinel::guest()) {
			throw new UserNotLoggedInException();
		}

		return Sentinel::check();
	}

	/**
	 * @return \WL\Modules\User\Models\User
	 * @throws UserNotLoggedInException
	 */
	public function getUsers()
	{
		return User::all();
	}


	/**
	 * @param \WL\Modules\User\Models\User $user
	 * @param array $phones
	 * @return mixed
	 * @deprecated
	 */
	public function updatePhones($user, array $phones) {
	}

	/**
	 * @param \WL\Modules\User\Models\User $user
	 * @param array $fields
	 * @throws InvalidPhoneException
	 * @return boolean
	 * @deprecated
	 */
	public function addPhone($user, array $fields)
	{
		$phone = new Phone($fields);

		if (!$phone->isValid()) {
			throw new InvalidPhoneException($phone->getErrors()->first());
		}

		return $user->addPhone($phone);
	}


	/**
	 * @param \WL\Modules\User\Models\User $user
	 * @param array $fields
	 * @return $this
	 */
	public function update($user, array $fields)
	{
		foreach ($this->getUpdateTypes() as $type) {
			if (isset($fields[$type])) {
				$this->{'update' . ucfirst($type)}($user, $fields[$type]);
			}
		}

		return $this;
	}

	/**
	 * @return array
	 */
	public function getUpdateTypes()
	{
		return $this->updateTypes;
	}

	/**
	 * Get Phone model By ID
	 * @param \WL\Modules\User\Models\User $user
	 * @param int $phoneId
	 * @return \App\Phone
	 */
	public function getPhoneById($user, $phoneId)
	{
		return $user->getPhoneById($phoneId);
	}

	/**
	 * Return an list of user profile managers ...
	 *
	 * @param \WL\Modules\User\Models\User $user
	 * @return bool|static
	 * @throws \WL\Modules\User\Exceptions\UserExceptions
	 */
	public function managers(User $user, $accepted = false)
	{
		return $user->managers($accepted);
	}
}

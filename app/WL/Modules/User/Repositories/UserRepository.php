<?php namespace WL\Modules\User\Repositories;

interface UserRepository
{
	/**
	 * @param int $id
	 * @return \WL\Modules\User\Models\User
	 */
	public function getById($id);

	/**
	 * @param \WL\Modules\User\Models\User $user
	 * @param array $fields
	 * @return mixed
	 */
	public function updateUser($user, array $fields);

	/**
	 * @param \WL\Modules\User\Models\User $user
	 * @param array $meta
	 * @return mixed
	 */
	public function updateMeta($user, array $meta);

	/**
	 * @param \WL\Modules\User\Models\User $user
	 * @param array $phones
	 * @return mixed
	 */
	public function updatePhones($user, array $phones);

	/**
	 * @param \WL\Modules\User\Models\User $user
	 * @param array $fields
	 * @return mixed
	 */
	public function update($user, array $fields);

	/**
	 * @return \WL\Modules\User\Models\User
	 */
	public function getCurrentUser();

	/**
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function getUsers();

	/**
	 * @param \WL\Modules\User\Models\User $user
	 * @param array $fields
	 * @return $this
	 */
	public function addPhone($user, array $fields);

	/**
	 * Get Phone model By ID
	 * @param \WL\Modules\User\Models\User $user
	 * @param int $phoneId
	 * @return \App\Phone
	 */
	public function getPhoneById($user, $phoneId);

}

<?php namespace WL\Modules\User\Contracts;

interface ActivableContract {

	/**
	 * Send activation code for specific entity to address.
	 *
	 * @param $entity
	 * @param $address
	 * @param array $params
	 * @return mixed
	 */
	public function sendActivation($entity, $address, array $params = array());
}

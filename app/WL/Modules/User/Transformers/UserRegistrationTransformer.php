<?php namespace WL\Modules\User\Transformers;

use WL\Modules\Profile\Transformers\PublicProfileTransformer;
use WL\Transformers\Transformer;

class UserRegistrationTransformer extends Transformer
{

	public function transform($item)
	{
		$data = (new UserTransformer())->transform($item->user);
		$data['profile'] = (new PublicProfileTransformer())->transform($item->profile);
			
		return $data;
	}

}

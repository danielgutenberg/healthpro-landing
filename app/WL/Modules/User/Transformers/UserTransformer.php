<?php namespace WL\Modules\User\Transformers;

use WL\Transformers\Transformer;

class UserTransformer extends Transformer
{
	public function transform($item)
	{
		$data = [
			'user' => [
				'email' => $item->email
			],
			'email_verified' => $item->emailVerified,
			'user_password_required' => $item->manualPasswordRequired
		];

		if (isset($item->pending_email) && $item->pending_email != $item->email) {
			$data['user']['pending_email'] = $item->pending_email;
		}

		return $data;
	}
}

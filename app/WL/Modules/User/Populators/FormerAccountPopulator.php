<?php namespace WL\Modules\User\Populators;

use WL\Modules\User\Models\User;
use WL\Populators\FormerPopulator;
use WL\Modules\Page\Populators\Exceptions\PageNotLoadedException;

class FormerAccountPopulator extends FormerPopulator implements AccountPopulator
{
	/**
	 * We accept only the Page model here
	 * @param \WL\Modules\User\Models\User $model
	 * @return $this
	 */
	public function setModel(User $model)
	{
		$this->model = $model;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getFields()
	{
		$fields = array_merge($this->model->toArray(), $this->model->getAllMeta(['title']));

		return $fields;
	}
}

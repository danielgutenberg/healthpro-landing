<?php namespace WL\Modules\User\Populators;

use WL\Modules\User\Models\User;

interface UserProfilePopulator
{
	/**
	 * @param \WL\Modules\User\Models\User $Model
	 *
	 * @return $this
	 */
	public function setModel(User $Model);
}

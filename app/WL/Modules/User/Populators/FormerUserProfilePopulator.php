<?php namespace WL\Modules\User\Populators;

use WL\Modules\User\Models\User;
use WL\Populators\FormerPopulator;
use WL\Modules\Page\Populators\Exceptions\PageNotLoadedException;

class FormerUserProfilePopulator extends FormerPopulator implements UserProfilePopulator
{
	/**
	 * We accept only the Page model here
	 * @param \WL\Modules\User\Models\User $model
	 * @return $this
	 */
	public function setModel(User $model)
	{
		$this->model = $model;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getFields()
	{
		$bdate = explode('-', $this->model->getMeta('birthdate'));
		if (count($bdate) < 3) {
			$bdate = ['', '', ''];
		}

		$fields = array_merge($this->model->getAllMeta(['gender', 'country']), [
			'birth_year'	=> (int) $bdate[0]?: '',
			'birth_month'	=> (int) $bdate[1]?: '',
			'birth_day'		=> (int) $bdate[2]?: '',
		]);

		return $fields;
	}
}

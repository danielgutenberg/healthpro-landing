<?php namespace WL\Modules\User\Composers;

use Illuminate\Contracts\View\View;
use Sentinel;
use WL\Contracts\Composable;

class UserComposer implements Composable {

	/**
	 * @param View $view
	 * @return mixed|void
	 */
	public function compose(View $view) {
		if( $sessionUser = Sentinel::check() ) {
			$activationManager = app('activation')->getDefaultDriver();

			$isActivatedUser = false;
			$activation 	 = $activationManager->completed($sessionUser);

			if( isset($activation->id) )
				$isActivatedUser = $activation->completed;

			$view->isActivatedUser = $isActivatedUser;

			/*
			 * if user has signed up with social media account we do not give him a message to set his password
			 */
			if ($sessionUser->socials->count() > 0) {
				$view->userSetPasswordRequired = false;
			} else {
				$view->userSetPasswordRequired = (int)$sessionUser->getMeta('user_password_required');
			}
		}
	}
}

<?php namespace WL\Modules\User\Events;


class BecameSearchableEvent
{
	private $profile;

	/**
	 * Event constructor.
	 * @param $profile
	 * @param string $registered
	 */
	public function __construct($profile)
	{
		$this->profile = $profile;
	}

	/**
	 * @return ModelProfile
	 */
	public function getProfile()
	{
		return $this->profile;
	}
}

<?php namespace WL\Modules\User\Providers;

use Illuminate\Support\ServiceProvider;
use WL\Modules\User\ActivationManager;
use WL\Modules\User\Populators\AccountPopulator;
use WL\Modules\User\Populators\FormerAccountPopulator;
use WL\Modules\User\Populators\FormerUserProfilePopulator;
use WL\Modules\User\Populators\UserProfilePopulator;
use WL\Modules\User\Repositories\SentinelUserRepository;
use WL\Modules\User\Repositories\UserRepository;
use WL\Modules\User\Services\UserServiceImpl;


class UserServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

		$this->app->bind(UserRepository::class, SentinelUserRepository::class);
		$this->app->bind(AccountPopulator::class, FormerAccountPopulator::class);
		$this->app->bind(UserProfilePopulator::class, FormerUserProfilePopulator::class);

		$this->app->bind('activation', function($app) {
			return new ActivationManager($app['yaml']);
		});


		$this->app->singleton('user', function ($app) {
			return new UserServiceImpl(
				$app['activation']->getDefaultDriver()
			);
		});
	}
}


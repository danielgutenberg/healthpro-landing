<?php namespace WL\Modules\User\Exceptions;

use App\Exceptions\GenericException;

class SocialUserIdNotEqualsException extends GenericException {}

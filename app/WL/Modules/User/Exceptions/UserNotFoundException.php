<?php namespace WL\Modules\User\Exceptions;

use App\Exceptions\GenericException;

class UserNotFoundException extends GenericException {}

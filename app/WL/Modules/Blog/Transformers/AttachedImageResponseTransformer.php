<?php namespace WL\Modules\Blog;

use WL\Transformers\Transformer;

class AttachedImageResponseTransformer extends Transformer
{

	public function transform($item)
	{
		$items = [];
		foreach($item['urls'] as $url){
			$items[] = ['url'=>$url];
		}
		return ['files'=>$items];

	}
}

<?php namespace WL\Modules\Blog;

use WL\Transformers\Transformer;

class BlogPostTransformer extends Transformer
{

	public function transform($item)
	{
		return [
			'id' => $item->id,
			'author_id' => $item->author_id,
			'title' => $item->name,
			'content' => $item->description,
			'cut_content' => $item->getBeforeCutContent(),
			'slug' => $item->slug,
			'cover_image' => $item->cover_image,
			'appointment_id' => $item->appointment_id,
		];
	}
}

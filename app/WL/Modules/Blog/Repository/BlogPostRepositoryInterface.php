<?php namespace WL\Modules\Blog;

/**
 * Interface BlogPostRepositoryInterface
 * @package WL\Modules\Blog
 */
interface BlogPostRepositoryInterface
{
	/**
	 * @param $id
	 * @return mixed
	 */
	function getById($id);

	/**
	 * @param $id
	 * @return mixed
	 */
	function removeById($id);

	/**
	 * @param $id
	 * @param $authorId
	 * @return mixed
	 */
	public function getByIdAuthorId($id, $authorId);

	/**
	 * @param BlogPost $post
	 * @return mixed
	 */
	function save(BlogPost $post);

	/**
	 * @param BlogPost $post
	 * @return mixed
	 */
	function remove(BlogPost $post);

	/**
	 * Receive post by owner or not, sorted by date
	 *
	 * @param $page
	 * @param $perPage
	 * @param null $owner
	 * @return mixed
	 */
	function getPosts($page, $perPage, $owner = null, $year = null, $month = null);


    /**
     * Returns only DRAFT profile posts
     *
     * @param $owner_id
     * @return mixed
     */
    public function getDraftPosts($owner_id, $admin = false);

    /**
     * Returns only PENDING profile posts
     *
     * @param $owner_id
     * @return mixed
     */
    public function getPendingPosts($owner_id, $admin = false);

	/**
	 * Returns only Future posts
	 *
	 * @param $owner_id
	 * @return mixed
	 */
	public function getFuturePosts($owner_id, $admin = false);

	/**
	 * Search posts by owner or not
	 *
	 * @param $term
	 * @param $page
	 * @param $perPage
	 * @param null $owner
	 * @return mixed
	 */
	function searchPosts($term, $page, $perPage, $owner = null);

	/**
	 * Receive post by slug
	 *
	 * @param $slug
	 * @return mixed
	 */
	function getBlogPostBySlug($slug);

	/**
	 * @param $id
	 * @param $state
	 * @return mixed
	 */
	function setBlogPostState($id, $state);


	/**
	 * @param $id
	 * @param $fieldName
	 * @param $fieldValue
	 * @return mixed
	 */
	function updateBlogPostField($id, $fieldName, $fieldValue);


	/**
	 * Returns true when exists
	 *
	 * @param $slug
	 * @return bool
	 */
	function blogPostSlugExists($slug);

	function getMonthlyArchive($category = null);

}

<?php namespace WL\Modules\Blog;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class BlogPostRepository implements BlogPostRepositoryInterface
{

	public static function tableName()
	{
		return 'blog_post';
	}

	/**
	 * return additional filter for getEntityByTags
	 * @return mixed
	 */
	public static function generateRulesForPublishedPosts()
	{
		return [['blog_post.state', '=', BlogPost::$STATE_PUBLISHED], ['blog_post.publish_at', '<=', \Carbon::now()]];
	}

	public function getById($id)
	{
		return BlogPost::find($id);
	}

	function removeById($id)
	{
		$post = BlogPost::find($id);
		return $post->delete();
	}

	public function getByIdAuthorId($id, $authorId)
	{
		return BlogPost::where('id', $id)->where('author_id', $authorId)->first();
	}

	private function skip($query, $page, $perPage)
	{
		return $query->skip($page * $perPage)->take($perPage);
	}

	private function filterOnlyPublished($query)
	{
		foreach ($this->generateRulesForPublishedPosts() as $rule) {
			$query->where($rule[0], $rule[1], $rule[2]);
		}

	}

	public function getPosts($page, $perPage, $owner = null, $year = null, $month = null)
	{
		$query = null;
		$query = DB::table('blog_post');

		$this->filterOnlyPublished($query);

		if ($owner != null) {
			$query->union(DB::table('blog_post')->where('author_id', $owner));
			$query->distinct();
		}

		if ($year && $month and $startDate = Carbon::parse('first day of ' . $month . ' ' . $year)) {
			$query->where('publish_at', '>', $startDate);
			$query->where('publish_at', '<', $startDate->copy()->endOfMonth());
		}

		$count = DB::table(DB::raw("({$query->toSql()}) as sub"))
			->mergeBindings($query)
			->count();

		$items = $this->skip($query, $page, $perPage)->orderBy('publish_at', 'DESC')->get();

		return [$count, BlogPost::hydrate($items)];
	}

	public function getDraftPosts($owner_id, $admin = false)
	{
		$posts = BlogPost::where('state', BlogPost::$STATE_DRAFT);

		if (!$admin) {
			$posts->where('author_id', $owner_id);
		}

		return $posts->get();
	}

	public function getPendingPosts($owner_id, $admin = false)
	{
		$posts = BlogPost::where('state', BlogPost::$STATE_PENDING);

		if (!$admin) {
			$posts->where('author_id', $owner_id);
		}

		return $posts->get();
	}

	public function getFuturePosts($owner_id, $admin = false)
	{
		$posts = BlogPost::where('publish_at', '>', \Carbon::now());

		if (!$admin) {
			$posts->where('author_id', $owner_id);
		}

		return $posts->get();
	}

	public function searchPosts($term, $page, $perPage, $owner = null)
	{
		$query = null;
		if ($owner == null) {
			$query = BlogPost::query();
			$this->filterOnlyPublished($query);
		} else {
			$query = BlogPost::where('author_id', $owner);
		}

		$query = $query->where('title', 'LIKE', "%$term%");

		return [$query->count(), $this->skip($query->orderBy('publish_at', 'DESC'), $page, $perPage)->get()];

	}


	public function getBlogPostBySlug($slug)
	{
		return BlogPost::where('slug', $slug)->first();
	}

	public function setBlogPostState($id, $state)
	{
		return BlogPost::where('id', $id)->update(['state' => $state]);
	}

	public function save(BlogPost $post)
	{
		return $post->save();
	}

	public function remove(BlogPost $post)
	{
		return $post->delete();
	}


	public function updateBlogPostField($id, $fieldName, $fieldValue)
	{
		return BlogPost::where('id', $id)->update([$fieldName => $fieldValue]);
	}

	function blogPostSlugExists($slug)
	{
		return BlogPost::where('slug', $slug)->count() > 0;
	}

	public function getMonthlyArchive($category = null)
	{
		$posts = BlogPost::where('state', 'published')->get()->groupBy(function($post) {
			return $post->publish_at->format('F Y');
		})->toArray();

		uksort($posts, function($a, $b) {
			return Carbon::parse('first day of ' . $a) < Carbon::parse('first day of ' . $b);
		});

		return $posts;
	}

}

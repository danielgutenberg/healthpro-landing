<?php namespace WL\Modules\Blog;

use WL\Modules\Role\Models\Role;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Sitemap;
use WL\Models\MetaTrait;
use WL\Models\ModelBase;
use WL\Modules\Notification\Facades\NotificationService;
use WL\Modules\Notification\Models\NotifiableInterface;
use WL\Modules\Notification\Models\NotifierInterface;
use WL\Modules\Notification\Models\NotifierTrait;
use WL\Modules\Taxonomy\Facades\TagService;
use WL\Modules\Taxonomy\Facades\TaxonomyService;
use WL\Translator\Models\TranslatableTrait;

class BlogPost extends ModelBase implements NotifierInterface
{
	public static $STATE_DRAFT = 'draft';
	public static $STATE_PENDING = 'pending';
	public static $STATE_PUBLISHED = 'published';

	public static $TAXONOMY_POST_TAGS = 'Post Tags';
	public static $TAXONOMY_CATEGORIES = 'Post Categories';

	use TranslatableTrait;
	use SluggableTrait;
	use MetaTrait;
	use NotifierTrait;

	protected $metaClassName = BlogPostMeta::class;

	public $ratingTypes = ['like' => 'thumb'];
	public $ratingValuesAllowed = ['thumb' => [1]];

	protected $sluggable = array(
		'build_from' => 'title',
		'save_to' => 'slug',
		'on_update' => true,
		'unique' => true,
	);

	protected $table = 'blog_post';


	public function excerpt()
	{
		$re = "/(?<ucut>.*?)<span[^>]*?class.*?=.*?[\"'].*?post-cut.*?['\"].*?>/i";

		if (preg_match($re, $this->content, $matches) > 0) {
			return $matches['ucut'];
		}

		return $this->content;
	}

	/**
	 * Returns an excerpt
	 *
	 * @param int     $length Length in words
	 * @param string $trailing
	 *
	 * @return string
	 */
	public function excerptPlain($length = null, $trailing = null)
	{
		$excerpt = $this->trim_words($this->content, $length, $trailing);

		return $excerpt;
	}

	private function truncate($str, $length = 150, $trailing)
	{
		$length -= mb_strlen($trailing);
		if (mb_strlen($str) > $length) {
			return mb_substr($str, 0, $length) . $trailing;
		} else {
			$res = $str;
		}
		return $res;

	}

	/**
	 * Trims to the specified number of words.
	 *
	 * @param      $text
	 * @param int  $num_words
	 * @param string $trailing
	 *
	 * @return string
	 */

	function trim_words( $text, $num_words = 65, $trailing = null ) {

		if ( null === $num_words )
			$num_words = 65;

		if ( null === $trailing )
			$trailing = __( '&hellip;' );

		$text = $this->strip_tags( $text );

		$words_array = preg_split( "/[\n\r\t ]+/", $text, $num_words + 1, PREG_SPLIT_NO_EMPTY );

		if ( count( $words_array ) > $num_words ) {
			array_pop( $words_array );
			$text = implode( ' ', $words_array );
			$text = $text . $trailing;
		} else {
			$text = implode( ' ', $words_array );
		}

		return $text;
	}
    /**
     * trims text to a space then adds ellipses if desired
     * @param string $input text to trim
     * @param int $length in characters to trim to
     * @param bool $ellipses if ellipses (...) are to be added
     * @param bool $strip_html if html tags are to be stripped
     * @return string
     */
    function trim_text($input, $length, $ellipses = true, $strip_html = true) {
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }

        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= '...';
        }

        return $trimmed_text;
    }
	/**
	 * Removes all html and ensures there are spaces between words.
	 *
	 * @param $string
	 *
	 * @return string
	 */
	function strip_tags($string)
	{
		$string = str_replace( '&nbsp;', ' ', $string);
		$string = preg_replace( '@\<figure[\s\S]+?figure\>@', '', $string);
		$string = preg_replace( '@<(script|style)[^>]*?>.*?</\\1>@si', '', $string );

		//Pad the string before common elements that separate words with spaces
		//todo fix by creating new lines in the content

		$tags = ['p', 'blockquote', 'li', 'h1', 'td', 'h2', 'h3'];

		foreach ( $tags as $tag ) {
			$string = str_replace( '</' . $tag . '>', '</' . $tag . '> ', $string);
		}

		$string = strip_tags($string);

		return trim($string);
	}

	public function getDates()
	{
		$res = parent::getDates();
		array_push($res, "publish_at");
		return $res;
	}

	/**
	 * Include self to sitemap index
	 * @return none
	 */
	public function handleSitemapIndex()
	{
		Sitemap::addSitemap(route('sitemap', ['blogs']), self::max('updated_at'));
	}

	/**
	 * Generate sitemap page
	 * @return none
	 */
	public function handleSitemapPage()
	{
		$categoriesSlug = TaxonomyService::getTaxonomyByName(static::$TAXONOMY_CATEGORIES)->slug;
		foreach (self::where('state','=','published')->orderBy('updated_at','desc')->get() as $post) {
			$categories = TagService::getEntityTagsForTaxonomySlug($post,$categoriesSlug);
			foreach($categories as $category){
				Sitemap::addTag(route('blog-get', [$category->slug, $post->slug]), $post->updated_at);
			}
		}
	}

	public function notifierConfig($action)
	{
		if($action == 'created'){
			return [
				'type'        => 'blogpost',
				'title'       =>$this->title,
				'description' =>$this->trim_words($this->content,10),
				'expire_at'   => Carbon::now()->addDays(7)->toDateTimeString()
			];
		}elseif($action == 'updated'){
			return [
				'type'        => 'blogpost',
				'title'       =>$this->title,
				'description' =>$this->trim_words($this->content,10),
				'expire_at'   => Carbon::now()->addDays(7)->toDateTimeString()
			];
		}else{
			return [
				'type'        => 'blogpost',
				'title'       =>$this->title,
				'description' =>$this->trim_words($this->content,10),
				'expire_at'   => Carbon::now()->addDays(7)->toDateTimeString()
			];
		}

	}

	public function notifierDispatchers()
	{
		return Role::all();
	}
}

<?php namespace WL\Modules\Blog;

use URL;
use WL\Modules\Attachment\Facades\AttachmentService;
use WL\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Collection;
use WL\Modules\Comment\Facades\CommentService;
use WL\Modules\Profile\Models\ModelProfile;

use WL\Modules\Taxonomy\Services\TagServiceInterface;
use WL\Modules\Taxonomy\Services\TaxonomyServiceInterface;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Yaml\Contracts\Parsable;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class BlogService implements BlogServiceInterface
{

	const CONFIGURATION_DIR = 'wl/blog/configuration.yaml';

	/**
	 * @var Parsable
	 */
	private $parser;

	/**
	 * @var
	 */
	private $configuration;

	public function __construct(
		BlogPostRepositoryInterface $repository,
		TagServiceInterface $tagService,
		TaxonomyServiceInterface $taxonomyService,
		Parsable $parser
	) {
		$this->repo = $repository;
		$this->tagService = $tagService;
		$this->taxService = $taxonomyService;
		$this->parser = $parser;
	}

	public function taxTag()
	{
		if (!isset($this->taxtag))
			$this->taxtag = $this->taxService->getTaxonomyByName(BlogPost::$TAXONOMY_POST_TAGS);
		return $this->taxtag;
	}

	public function taxCat()
	{
		if (!isset($this->taxcat))
			$this->taxcat = $this->taxService->getTaxonomyByName(BlogPost::$TAXONOMY_CATEGORIES);
		return $this->taxcat;
	}

	public function getCategoriesWithPosts()
	{
		$taxCatId = $this->taxCat()->id;
		$tags = $this->tagService->getTagsUsageGreaterThanNumber($taxCatId, BlogPost::class, 0);

		#todo clean this up. We only want tags with posts that are published
		$tagsWithPosts = [];
		$blogData = [
			'taxonomyId' => $taxCatId,
			'page' => 0,
			'perPage' => 1,
			'filters' => BlogPostRepository::generateRulesForPublishedPosts()
		];
		foreach($tags as &$tag) {
			if (0 !== $this->tagService->getEntitiesByTags([$tag->name], BlogPostRepository::tableName(), $blogData)[0]) {
				$tagsWithPosts[] = $tag;
			}
		}
		return $tagsWithPosts;
	}

	private function getPostData(&$post)
	{
		$post->tags = $this->tagService->getEntityTagsForTaxonomyId($post, $this->taxTag()->id);
		$post->category = $this->tagService->getEntityTagsForTaxonomyId($post, $this->taxCat()->id)->first();

		$image = AttachmentService::findFile('cover_image', $post, false);

		$post->link = URL::route('blog-get',['category' => ($post->category ? $post->category->slug : 'not-set'), 'slug' => $post->slug]);

		$post->editLink = URL::route('blog-edit',['category' => ($post->category ? $post->category->slug : 'not-set'), 'slug' => $post->slug]);

		// Fix relative urls when displaying images.
		// We don't need the domain in the DB, but when displaying, it's better.
		$post->content = str_replace('src="/uploads/blog','src="' . URL::to('/uploads/blog'), $post->content);

		if ($image) {
			$post->cover_image_url = $image->url();
		}

		if($postMeta = $post->getAllMeta()){
			$post->meta = $postMeta;
		}

		$post->author = ProfileService::loadProfileBasicDetails($post->author_id);

		$post->comments = CommentService::getCommentsForEntity($post->id, get_class($post));
	}

	public function getPostsData(&$posts)
	{
		if (is_array($posts) || $posts instanceof Collection) {
			foreach ($posts as $post) {
				$this->getPostData($post);
			}
		} else if ($posts instanceof BlogPost) {
			$this->getPostData($posts);
		}
	}

	public function getPosts($page = 0, $perPage = 10, $year = null, $month = null)
	{
		$posts = $this->repo->getPosts($page, $perPage, null, $year, $month);
		$this->getPostsData($posts[1]);
		return $posts;
	}

	public function getMonthlyArchive($category = null)
	{
		return $this->repo->getMonthlyArchive($category);
	}

	public function removePost($id)
	{
		return $this->repo->removeById($id);
	}

	public function searchPosts($term, $page, $perPage)
	{
		$posts = $this->repo->searchPosts($term, $page, $perPage);
		$this->getPostsData($posts[1]);
		return $posts;
	}

	public function commentOnBlogPost($blogPostSlug, $author, $commentData)
	{
		$post = $this->getBlogPostBySlug($blogPostSlug);
		return $post->addComment($commentData, null, $author);
	}

	public function getAuthor($blogPost)
	{
		return ModelProfile::find($blogPost->author_id);
	}

	public function likeBlogPost($blogPostSlug)
	{
		$post = $this->repo->getBlogPostBySlug($blogPostSlug);
		return $post->rate('thumb', 1);
	}

	public function getBlogPostBySlug($blogPostSlug)
	{
		$post = $this->repo->getBlogPostBySlug($blogPostSlug);
		$this->getPostsData($post);
		return $post;
	}

	public function getBlogPostById($id)
	{
		$post = $this->repo->getById($id);
		$this->getPostsData($post);
		return $post;
	}

	public function getPrivatePosts($author, $page, $perPage)
	{
		$posts = $this->repo->getPosts($page, $perPage, $author);
		$this->getPostsData($posts[1]);
		return $posts;
	}

	public function getDraftPosts($profile_id)
	{
		$admin = $this->hasAllBlogPermissions();
		$posts = $this->repo->getDraftPosts($profile_id, $admin);
		$this->getPostsData($posts);
		return $posts;
	}

	public function getPendingPosts($profile_id)
	{
		$admin = $this->hasAllBlogPermissions();
		$posts = $this->repo->getPendingPosts($profile_id, $admin);
		$this->getPostsData($posts);
		return $posts;
	}

	public function getFuturePosts($profile_id)
	{
		$admin = $this->hasAllBlogPermissions();
		$posts = $this->repo->getFuturePosts($profile_id, $admin);
		$this->getPostsData($posts);
		return $posts;
	}

	private function hasAllBlogPermissions()
	{
		$profile = ProfileService::getCurrentProfile();
		$isStaff = $profile->type == ModelProfile::STAFF_PROFILE_TYPE;

		return $isStaff || Sentinel::inRole('administrator');

	}

	public function searchPrivatePosts($author, $term, $page, $perPage)
	{
		$posts = $this->repo->searchPosts($term, $page, $perPage, $author);
		$this->getPostsData($posts[1]);
		return $posts;
	}

	public function updateBlogPostField($id, $fieldName, $fieldValue)
	{
		return $this->repo->updateBlogPostField($id, $fieldName, $fieldValue);
	}

	public function updateBlogPost($authorId, $blogPostId, $blogPostData, $coverImageFile)
	{
		$post = null;

		if ($blogPostId != null) {
			$post = $this->repo->getById($blogPostId); //getByIdAuthorId($blogPostId, $authorId);
		} else {
			$post = new BlogPost();
		}

		if ($post == null) {
			return null;
		}

		$needSluggify = ($blogPostData['title'] != $post->title || $blogPostData['state'] == BlogPost::$STATE_DRAFT)
			&& ($blogPostData['state'] != BlogPost::$STATE_PUBLISHED);

		$post->fill([
			'content' => $blogPostData['content'],
			'title' => $blogPostData['title'],
			'state' => $blogPostData['state'],
		]);

		if ($post->profile_id == null) {
			$post->author_id = $authorId;
		}

		if (!empty($blogPostData['professional_id'])) {
			$post->author_id = $blogPostData['professional_id'];
		}

		if (array_key_exists('publish_at', $blogPostData))
			$post->publish_at = $blogPostData['publish_at'];
		else
			$post->publish_at = \Carbon::now();

		if (array_key_exists('new_slug', $blogPostData)
			&& strlen($blogPostData['new_slug']) > 0
			&& $post->slug != $blogPostData['new_slug']
		) {
			if (!$this->repo->blogPostSlugExists($blogPostData['new_slug'])) {
				$post->slug = $blogPostData['new_slug'];
			}
		}

		if ($needSluggify || $post->slug == null) {
			$post->sluggify();
		}

		if ($this->repo->save($post)) {
			if ($coverImageFile != null) {
				//shit
				$att = AttachmentService::uploadImage('cover_image', $post, $coverImageFile);
				$att->fill([
					'info' => '',
					'category' => '',
					'rank' => 0,
				]);
				$att->save();
				//shit end
			}

			if (isset($blogPostData['tags']) /*&& count($blogPostData['tags']) > 0*/) {

				if (!is_array($blogPostData['tags'])) {
					$blogPostData['tags'] = [$blogPostData['tags']];
				}

				$this->tagService->explicitSetTagsWithEntity($post, $blogPostData['tags'], $this->taxTag()->id);
			} else {
				$this->tagService->explicitSetTagsWithEntity($post, [], $this->taxTag()->id);
			}

			if (isset($blogPostData['category'])) {

				if (!is_array($blogPostData['category'])) {
					$blogPostData['category'] = [$blogPostData['category']];
				}

				$this->tagService->explicitSetTagsWithEntity($post, $blogPostData['category'], $this->taxCat()->id);
			}else{
				$this->tagService->explicitSetTagsWithEntity($post, [], $this->taxCat()->id);
			}

			if (isset($blogPostData['meta'])) {
				foreach ($blogPostData['meta'] as $meta_key => $meta_value) {
					$post->saveMeta($meta_key,$meta_value);
				}
			}

			$this->getPostsData($post);

			return $post;
		}


		return null;
	}


	/**
	 * Get blog configuration ..
	 *
	 * @param null $key
	 * @return mixed
	 */
	public function getConfiguration($key = null) {
		if ( ! isset($this->configuration) ) {
			$configuration = $this->parser->parse(self::CONFIGURATION_DIR);

			$this->configuration = $configuration;
		}

		if ($key) {
			return $this->configuration[$key];
		}

		return $this->configuration;
	}

}

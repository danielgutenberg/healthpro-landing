<?php namespace WL\Modules\Blog;

/**
 * Interface BlogServiceInterface
 * @package WL\Modules\Blog
 */
interface BlogServiceInterface
{
	/**
	 * @param int $page
	 * @param int $perPage
	 * @return mixed
	 */
	function getPosts($page = 0, $perPage = 10, $year = null, $month = null);

	/**
	 * Get categories with posts
	 *
	 * @return mixed
	 */
	function getCategoriesWithPosts();

	public function getMonthlyArchive($category = null);

	/**
	 * Completes post entity with associated data
	 *
	 * @param $posts
	 * @return mixed
	 */
	function getPostsData(&$posts);

	/**
	 * @param int $id
	 * @return mixed
	 */
	function removePost($id);

	/**
	 * @param $term
	 * @param $page
	 * @param $perPage
	 * @return mixed
	 */
	function searchPosts($term, $page, $perPage);

	/**
	 * @param $blogPostSlug
	 * @param $author
	 * @param $commentData
	 * @return mixed
	 */
	function commentOnBlogPost($blogPostSlug, $author, $commentData);

	/**
	 * @param $blogPost
	 * @return mixed
	 */
	function getAuthor($blogPost);

	/**
	 * @param $blogPostSlug
	 * @return mixed
	 */
	function likeBlogPost($blogPostSlug);

	/**
	 * @param $blogPostSlug
	 * @return mixed
	 */
	function getBlogPostBySlug($blogPostSlug);

	/**
	 * @param $id
	 * @return mixed
	 */
	function getBlogPostById($id);

	/**
	 * @param $author
	 * @param $page
	 * @param $perPage
	 * @return mixed
	 */
	function getPrivatePosts($author, $page, $perPage);


    /**
     * Returns only DRAFT posts
     * @param $profile_id
     * @return mixed
     */
    public function getDraftPosts($profile_id);

    /**
     * Returns only PENDING posts
     * @param $profile_id
     * @return mixed
     */
    public function getPendingPosts($profile_id);

	/**
	 * Returns only Future Posts
	 * @param $profile_id
	 * @return mixed
	 */
	public function getFuturePosts($profile_id);


	/**
	 * @param $author
	 * @param $term
	 * @param $page
	 * @param $perPage
	 * @return mixed
	 */
	function searchPrivatePosts($author, $term, $page, $perPage);

	/**
	 * @param $authorId
	 * @param $blogPostId
	 * @param $blogPostData
	 * @param $coverImageFile
	 * @return mixed
	 */
	function updateBlogPost($authorId, $blogPostId, $blogPostData, $coverImageFile);


	/**
	 * @param $id
	 * @param $fieldName
	 * @param $fieldValue
	 * @return mixed
	 */
	public function updateBlogPostField($id, $fieldName, $fieldValue);
}


<?php namespace WL\Modules\Blog\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Modules\Blog\BlogServiceInterface;

class BlogService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return BlogServiceInterface::class;
	}
}

<?php namespace WL\Modules\Blog;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class BlogServiceProvider extends ServiceProvider
{
	public function register()
	{
		$this->app->booting(function () {
			AliasLoader::getInstance()->alias('BlogService', 'WL\Modules\Blog\Facades\BlogService');
		});

		$this->app->bind(BlogServiceInterface::class, BlogService::class);
		$this->app->bind(BlogPostRepositoryInterface::class, BlogPostRepository::class);
	}
}


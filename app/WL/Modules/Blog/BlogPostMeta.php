<?php namespace WL\Modules\Blog;

use WL\Models\ModelMeta;

class BlogPostMeta extends ModelMeta {

	protected $table = 'blog_post_meta';

}

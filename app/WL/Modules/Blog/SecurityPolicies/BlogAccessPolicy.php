<?php namespace WL\Modules\Blog;

use WL\Security\Facades\AuthorizationUtils;
use WL\Security\SecurityPolicies\BaseAccessPolicy;

/**
 * Class BlogAccessPolicy
 * @package WL\Modules\Blog
 */
class BlogAccessPolicy extends BaseAccessPolicy
{
	/**
	 * Can load blog post?
	 * @param $post
	 * @return bool
	 */
	public function canLoadBlogPost($post)
	{
		$result = true;
		if ($post && (($post->state == BlogPost::$STATE_DRAFT) || ($post->state == BlogPost::$STATE_PENDING))) {
			$isEditor = AuthorizationUtils::hasAccess(['blogpost.update']);
			$result = $isEditor;
		}
		return $result;
	}


	public function isBlogPostEditor()
	{
		return AuthorizationUtils::hasAccess(['blogpost.update']);
	}

}

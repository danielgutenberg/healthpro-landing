<?php namespace WL\Modules\Blog;


use WL\Modules\Attachment\Facades\AttachmentService;
use WL\Modules\Profile\Commands\ValidatableCommand;

class AttachImageToBlogPostCommand extends ValidatableCommand
{

	const IDENTIFIER = 'blog.attachImageToBlogPostCommand';

	protected $rules = [
		'images' => 'required',
		'post_id' => 'required'
	];

	public function validHandle()
	{
		$post = new BlogPost();
		$post->id = $this->inputData['post_id'];

		$urls = [];
		foreach($this->inputData['images'] as $image ) {
			$att = AttachmentService::uploadImage('post_images', $post, $image);
			$att->fill([
				'info' => '',
				'category' => '',
				'rank' => 0,
			]);
			$att->save();
			$urls[] = $att->url();
		}

		return ['urls' => $urls];
	}
}

<?php namespace WL\Modules\Blog;


use WL\Modules\Taxonomy\Services\TagServiceInterface;
use WL\Modules\Taxonomy\Services\TaxonomyServiceInterface;
use WL\Security\Commands\AuthorizableCommand;

class LoadBlogPostAdditionalInfoCommand extends AuthorizableCommand
{

	const IDENTIFIER = 'blog.loadBlogPostAdditionalInfoCommand';

	function convertToHashMap($arr, $propertyName)
	{
		$map = [];
		if ($arr) {
			foreach ($arr as $item) {
				$map[$item->$propertyName] = $item;
			}
		}
		return $map;
	}

	public function handle(TagServiceInterface $tagSvc, TaxonomyServiceInterface $taxSvc)
	{
		$data = [];
		$taxTagId = $taxSvc->getTaxonomyByName(BlogPost::$TAXONOMY_POST_TAGS)->id;
		$taxCatId = $taxSvc->getTaxonomyByName(BlogPost::$TAXONOMY_CATEGORIES)->id;

		$data['availableTags'] = $tagSvc->getTaxonomyTags($taxTagId);
		$data['availableCategories'] = $tagSvc->getTaxonomyTags($taxCatId);
		$data['availableStates'] = [BlogPost::$STATE_DRAFT, BlogPost::$STATE_PENDING, BlogPost::$STATE_PUBLISHED];
		$data['categoriesWithPosts'] = $tagSvc->getTagsUsageGreaterThanNumber($taxCatId, BlogPost::class, 0);


		$data['hashedAvailableTags'] = $this->convertToHashMap($data['availableTags'], 'name');
		$data['hashedAvailableCategories'] = $this->convertToHashMap($data['availableCategories'], 'name');
		$data['hashedCategoriesWithPosts'] = $this->convertToHashMap($data['categoriesWithPosts'], 'name');

		return $data;
	}
}

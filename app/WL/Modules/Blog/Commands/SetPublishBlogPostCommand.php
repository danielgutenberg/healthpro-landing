<?php namespace WL\Modules\Blog;


use WL\Security\Commands\AuthorizableCommand;

class SetPublishBlogPostCommand extends AuthorizableCommand
{

	const IDENTIFIER = 'blog.setPublishBlogPostCommand';

	function __construct($id)
	{
		$this->id = $id;
	}

	public function handle()
	{
		return BlogPost::$STATE_PUBLISHED;
	}
}

<?php namespace WL\Modules\Blog;


use WL\Modules\Profile\Commands\ValidatableCommand;
use WL\Modules\Taxonomy\Services\TaxonomyServiceInterface;
use WL\Modules\Taxonomy\Services\TagServiceInterface;


class LoadPostsByTagCommand extends ValidatableCommand
{
	const IDENTIFIER = 'blog.loadPostsByTagCommand';

	protected $rules = [
		'slug' => 'required',
		'taxonomyName' => 'required',
		'page' => 'required',
		'perPage' => 'required',
	];

	public function validHandle(BlogServiceInterface $svc, TagServiceInterface $tagSvc, TaxonomyServiceInterface $taxSvc)
	{
		$tax = $taxSvc->getTaxonomyByName($this->inputData['taxonomyName']);
		$taxId = $tax != null ? $tax->id : null;

		$tag = $tagSvc->getByName($this->inputData['slug'], $taxId, true);

		$tagName = $tag != null ? $tag->name : null;

		$data = [
			'success' => false,
			'totalPosts' => 0,
		];

		if ($tax && $tag) {
			list($count, $items) = $tagSvc->getEntitiesByTags(
				[$tagName],
				BlogPostRepository::tableName(),
				[
					'taxonomyId' => $taxId,
					'page'       => $this->inputData['page'],
					'perPage'    => $this->inputData['perPage'],
					'filters'    => BlogPostRepository::generateRulesForPublishedPosts() //i know its dirty ):
				]
			);

			$items = BlogPost::hydrate($items);
			$svc->getPostsData($items);


			$editorData = ['editor' => false];
			if ((new BlogAccessPolicy())->isBlogPostEditor())
				$editorData = $this->runSubCommand(new LoadEditorBlogPostsCommand($this->inputData['profileId']), $editorData);

			$data = [
				'totalPosts' => $count,
				'posts' => $items,
                'tagName' => $tagName,
				'success' => true,
				'archive' => $svc->getMonthlyArchive()
			];
			$data = array_merge($data, $this->runSubCommand(new LoadBlogPostAdditionalInfoCommand(), []), $editorData);

		}

		return $data;
	}
}

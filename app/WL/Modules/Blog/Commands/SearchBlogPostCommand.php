<?php namespace WL\Modules\Blog;


use WL\Modules\Profile\Commands\ValidatableCommand;

class SearchBlogPostCommand extends ValidatableCommand
{

	const IDENTIFIER = 'blog.searchBlogPostCommand';

	protected $rules = [
		'page' => 'required',
		'perPage' => 'required',
	];

	public function validHandle(BlogServiceInterface $svc)
	{
		list($total, $posts) = $svc->searchPosts(
			$this->inputData['term'],
			$this->inputData['page'],
			$this->inputData['perPage']
		);

		$editorData = ['editor' => false];
		if ((new BlogAccessPolicy())->isBlogPostEditor())
			$editorData = $this->runSubCommand(new LoadEditorBlogPostsCommand($this->inputData['profileId']), $editorData);

		$data = [
			'posts' => $posts,
			'totalPosts' => $total,
		];

		return array_merge($data, $this->runSubCommand(new LoadBlogPostAdditionalInfoCommand(), []), $editorData);

	}
}

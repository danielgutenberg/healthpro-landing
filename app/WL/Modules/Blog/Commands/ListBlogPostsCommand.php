<?php namespace WL\Modules\Blog;


use WL\Modules\Profile\Commands\ValidatableCommand;

class ListBlogPostCommand extends ValidatableCommand
{

	const IDENTIFIER = 'blog.listBlogPostCommand';

	protected $rules = [
		'page' => 'required',
		'perPage' => 'required',
	];

	public function validHandle(BlogServiceInterface $svc)
	{

		list($total, $posts) = $svc->getPosts(
			$this->inputData['page'],
			$this->inputData['perPage'],
			$this->inputData['year'] ?? null,
			$this->inputData['month'] ?? null
		);

		$editorData = ['editor' => false];
		if ((new BlogAccessPolicy())->isBlogPostEditor())
			$editorData = $this->runSubCommand(new LoadEditorBlogPostsCommand($this->inputData['profileId']), $editorData);

		$data = [
			'posts' => $posts,
			'totalPosts' => $total,
			'archive' => $svc->getMonthlyArchive()
		];

		return array_merge($data, $this->runSubCommand(new LoadBlogPostAdditionalInfoCommand(), []), $editorData);
	}
}

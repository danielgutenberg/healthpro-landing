<?php namespace WL\Modules\Blog;


use WL\Security\Commands\AuthorizableCommand;

class SetOtherStatesBlogPostCommand extends AuthorizableCommand
{

	const IDENTIFIER = 'blog.setOtherStatesBlogPostCommand';

	function __construct($id, $state)
	{
		$this->id = $id;
		$this->state = $state;
	}

	public function handle()
	{
		return $this->state;
	}
}

<?php namespace WL\Modules\Blog;


use WL\Security\Commands\AuthorizableCommand;

class LoadEditorBlogPostsCommand extends AuthorizableCommand
{

	const IDENTIFIER = 'blog.loadEditorBlogPostsCommand';


	function __construct($profileId)
	{
		$this->profileId = $profileId;
	}

	public function handle(BlogServiceInterface $svc)
	{
		$data = ['editor' => false, 'draftPosts' => [], 'futurePosts' => [], 'pendingPosts' => []];

		if ($this->profileId != null) {
			$data = [
				'editor' => true,
				'draftPosts' => $svc->getDraftPosts($this->profileId),
				'pendingPosts' => $svc->getPendingPosts($this->profileId),
				'futurePosts' => $svc->getFuturePosts($this->profileId)
			];
		}

		return $data;

	}
}

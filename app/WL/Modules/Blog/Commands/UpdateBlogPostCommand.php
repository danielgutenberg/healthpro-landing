<?php namespace WL\Modules\Blog;

use Carbon\Carbon;

use WL\Modules\Profile\Commands\ValidatableCommand;
use WL\Modules\Slug\Facade\SlugService;


class UpdateBlogPostCommand extends ValidatableCommand
{
	const IDENTIFIER = 'blog.updateBlogPostCommand';

	protected $rules = [
		'title' => 'required|',
		'content' => 'required|',
		'state' => 'required|',
		'author_id' => 'required|',
		//'tags' => 'required|',
//		'category' => 'required|',
		'publish_at' => 'required|date'
	];


	public function validHandle(BlogServiceInterface $svc)
	{

		$coverImage = null;
		$data = $this->inputData;
		$slug = $data['slug'];

		$stateSetter = [
			BlogPost::$STATE_DRAFT => (new SetOtherStatesBlogPostCommand($slug, BlogPost::$STATE_DRAFT)),
			BlogPost::$STATE_PENDING => (new SetOtherStatesBlogPostCommand($slug, BlogPost::$STATE_PENDING)),
			BlogPost::$STATE_PUBLISHED => (new SetPublishBlogPostCommand($slug))
		];



		$post = $svc->getBlogPostBySlug($slug);

		$id = $post ? $post->id : null;

		if (array_key_exists('tags', $this->inputData))
			$data['tags'] = array_map('trim', $this->inputData['tags']);

		$data['state'] = null;

		if (array_key_exists('state', $this->inputData) && array_key_exists($this->inputData['state'], $stateSetter)) {
			$data['state'] = $this->runSubCommand($stateSetter[$this->inputData['state']], BlogPost::$STATE_DRAFT);
		}

		$data['content'] = str_replace( '&nbsp;', ' ', $data['content']);

		$data['publish_at'] = Carbon::parse($data['publish_at']);

		if ($data['state'] == BlogPost::$STATE_PUBLISHED && $post->state != BlogPost::$STATE_PUBLISHED && $data['publish_at'] < Carbon::now()) {
			$data['publish_at'] = Carbon::now();
		}

		if (isset($data['cover_image'])) {
			$coverImage = $data['cover_image'];
		}

		$data['new_slug'] = SlugService::generateUniqueSlug($data['new_slug'], 'blog_post', $id);

		$post = $svc->updateBlogPost($data['author_id'], $id, $data, $coverImage);

		return ['post' => $post];
	}
}

<?php namespace WL\Modules\Blog;



use WL\Modules\Profile\Commands\ValidatableCommand;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Facades\AuthorizationUtils;


class GetBlogPostCommand extends ValidatableCommand
{

	const IDENTIFIER = 'blog.getBlogPostCommand';

	protected $rules = ['slug' => 'required'];
	private $securityPolicies;

	function __construct($inputData)
	{
		parent::__construct($inputData);
		$this->securityPolicies = new BlogAccessPolicy();
	}


	function convertToHashMap($arr, $propertyName)
	{
		$map = [];
		if ($arr) {
			foreach ($arr as $item) {
				$map[$item->$propertyName] = $item;
			}
		}
		return $map;
	}

	public function validHandle(BlogServiceInterface $svc)
	{
		$profileId = $this->inputData['profileId'];

		$editorData = ['editor' => false];
		if ($this->securityPolicies->isBlogPostEditor())
			$editorData = $this->runSubCommand(new LoadEditorBlogPostsCommand($profileId), $editorData);

		$post = $svc->getBlogPostBySlug($this->inputData['slug']);

		if (!$this->securityPolicies->canLoadBlogPost($post))
			$post = null;

		$data = [
			'post' => $post,
			'currentProfile' => null
		];

		if ($post != null) {
			if ($profileId)
				$data['currentProfile'] = ProfileService::loadProfileBasicDetails($profileId);

			if ($post->category && $post->category->slug != $this->inputData['categorySlug']) {
				#todo handle this cleaner
				$data['rightCategory'] = $post->category->slug;
				return $data;
			}

			if ($editorData['editor']) {
				$data['postTags'] = $this->convertToHashMap($post->tags, 'name');
				$data = array_merge($data, $this->runSubCommand(new LoadBlogPostAdditionalInfoCommand(), []));
			}
		}

		$data['canAddComment'] = AuthorizationUtils::hasAccess(['comment.add']);
		$data['isGuest'] = AuthorizationUtils::isUserGuest();
        $data['archive'] = $svc->getMonthlyArchive();
		return array_merge($data, $editorData);
	}
}

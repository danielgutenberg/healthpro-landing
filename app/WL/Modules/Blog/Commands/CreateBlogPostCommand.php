<?php namespace WL\Modules\Blog;

use Carbon\Carbon;

use WL\Modules\Profile\Commands\ValidatableCommand;


class CreateBlogPostCommand extends ValidatableCommand
{

	const IDENTIFIER = 'blog.createBlogPostCommand';

	protected $rules = [
		'profileId' => 'required'
	];

	public function validHandle(BlogServiceInterface $svc)
	{
		$data = [
			'title' => ('Post created at ' . Carbon::now()->toDateTimeString()),
			'content' => '',
			'state' => 'draft',
			'late_publishing_time' => Carbon::now()
		];

		$post = $svc->updateBlogPost($this->inputData['profileId'], null, $data, null);
		return ['post' => $post];
	}
}

<?php namespace WL\Modules\Blog;


use WL\Modules\Profile\Commands\ValidatableCommand;

class DeleteBlogPostCommand extends ValidatableCommand
{
	const IDENTIFIER = 'blog.deleteBlogPostCommand';

	protected $rules = ['id'=>'required'];

	public function validHandle(BlogServiceInterface $svc)
	{
		return $svc->removePost($this->inputData['id']);
	}
}

<?php namespace WL\Modules\Ticket\Observers;

use WL\Modules\Ticket\Models\Ticket;
use Setting;
use Mail;
use Log;
use Config;

class TicketObserver
{

	public function saving(Ticket $model)
	{
		if (!$model->isValid()) return false;
	}

	public function created(Ticket $ticket)
	{
		$emails = preg_split("/\s*,\s*/", trim(Setting::get('ticket_admin_emails','')) );
		foreach ($emails as $toEmail) {
			if (!$toEmail) continue;
			try {
				if (!app()->environment('testing')) {
					Mail::send('emails.ticket.admin-notity', compact('ticket'), function ($message) use ($toEmail, $ticket) {
						$_from = Config::get('mail.from');

						$from_email = trim(Setting::get('ticket_from_email') ?: $_from['address']);
						$from_name = trim(Setting::get('ticket_from_name') ?: $_from['name']);

						$message->from($from_email, $from_name);
						$message->to($toEmail, 'Ticket Support');
						$message->replyTo($ticket->user_email, $ticket->user_name);
						$message->subject('[HealthPro] (Ticket #' . $ticket->id . ') New Message from Guest');
					});
				}
			} catch (\Exception $e) {
				// Hide errors from user. Ticket is successfully created
				Log::error($e);
			}
		}
	}

}

<?php
namespace WL\Modules\Ticket\Observers;

use WL\Modules\Ticket\Models\TicketConversation;
use Setting;
use Mail;
use Log;
use Config;

class TicketConversationObserver
{
	public function saved(TicketConversation $reply)
	{
		$ticket = $reply->ticket;

		$ticket->update([
			'status' => 'pending',
			'updated_at' => time()
		]);

		// Send email to user email if message is created by admin
		if ( ($user = $reply->user) && $user->inRole('administrator') ) {
			try {
				Mail::send('admin.emails.tickets.reply', compact('reply','ticket'), function($message) use ($ticket) {
					$_from = Config::get('mail.from');

					$from_email = trim(Setting::get('ticket_from_email') ?: $_from['address']);
					$from_name = trim(Setting::get('ticket_from_name') ?: $_from['name']);

					$replyTo_email = trim(Setting::get('ticket_replyto_email') ?: $from_email);
					$replyTo_name = trim(Setting::get('ticket_replyto_name') ?: $from_name);

					$message->from($from_email, $from_name);
					$message->to($ticket->user_email, $ticket->user_name);
					$message->replyTo($replyTo_email, $replyTo_name);
					$message->subject('[HealthPro] (Ticket #'.$ticket->id.') Reply');
				});
			} catch (\Exception $e) {
				Log::error($e);
			}
		}
	}
}

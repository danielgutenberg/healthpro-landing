<?php namespace WL\Modules\Ticket\Models;

use WL\Modules\Ticket\Models\Ticket;
use WL\Models\ModelBase;
use WL\Modules\User\Models\User;

class TicketConversation extends ModelBase
{

	protected $rules = [
		'ticket_id' => 'required|integer',
		'message' => 'required',
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function setUser(User $user)
	{
		$this->user_id = $user->id;
		$this->user_name = $user->full_name;
	}

	public function ticket()
	{
		return $this->belongsTo(Ticket::class);
	}

}

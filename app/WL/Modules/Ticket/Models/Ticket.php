<?php namespace WL\Modules\Ticket\Models;

use WL\Models\ModelBase;
use WL\Modules\Meta\Models\Meta;
use WL\Modules\User\Models\User;
use WL\Models\MetaTrait;

class Ticket extends ModelBase
{
	use MetaTrait;
	protected $morphClass = self::class;
	protected $metaClassName = Meta::class;

	protected $rules = [
			'user_name' => 'required',
			'user_email' => 'required:email',
			'message' => 'required',
		];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function conversations() {
		return $this->hasMany(\WL\Modules\Ticket\Models\TicketConversation::class);
	}

	public function __get($name) {
		if (preg_match("/^message_(\d+)$/", $name, $m)) {
			return substr($this->message, 0, $m[1]);
		} else return parent::__get($name);
	}

	public function newConversation(User $user) {
		$conv = new \WL\Modules\Ticket\Models\TicketConversation(['ticket_id' => $this->id]);
		$conv->setUser($user);
		return $conv;
	}
}

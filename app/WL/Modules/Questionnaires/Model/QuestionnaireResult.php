<?php namespace WL\Modules\Questionnaires;

use WL\Modules\User\Models\User;
use WL\Models\ModelBase;

/**
 * Class QuestionnaireResult
 * @package WL\Modules\Questionnaires
 */
class QuestionnaireResult extends ModelBase
{
    /**
     * @var string
     */
    public $table = 'questionnaire_result';

    /**
     * @var array
     */
    protected $rules = array(
        'questionnaire_id' => 'required|integer',
        'question_id' => 'required|integer',
        'owner_id' => 'required|integer',
        'field_value' => 'required'
    );

    /** only for repository
     * @param $requiredValues
     */
    public function setPredefinedRule($requiredValues)
    {
        $this->rules['field_value'] = 'required|in:' . $requiredValues;
    }

    /** get owner of current result field
     * @return \WL\Modules\User\Models\User;
     */
    public function getOwner()
    {
        if (isset($this->anonymous) && !$this->anonymous) {
            return User::find($this->owner_id);
        }
        return null;
    }
}

<?php namespace WL\Modules\Questionnaires;

use WL\Models\ModelBase;

/**
 * Class QuestionnaireQuestion
 * @package WL\Modules\Questionnaires
 */
class QuestionnaireQuestion extends ModelBase
{
    /**
     * @var string
     */
    public $table = 'questionnaire_question';

    /**
     * @var array
     */
    protected $rules = array(
        'questionnaire_id' => 'required|integer',
        'order' => 'required|integer',
        'field_name' => 'required',
        'field_type' => 'required',
    );


}

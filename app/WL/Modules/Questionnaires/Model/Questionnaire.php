<?php namespace WL\Modules\Questionnaires;

use WL\Modules\Profile\Services\ModelProfileService;
use WL\Modules\Questionnaires\Facade\QuestionnaireService;
use WL\Models\ModelBase;

/**
 * Class Questionnaire
 * @package WL\Modules\Questionnaires
 */
class Questionnaire extends ModelBase
{
    /**
     * @var string
     */
    public $table = 'questionnaire';

    /**
     * @var array
     */
    protected $rules = array(
        'name' => 'required',
        'is_public' => 'required|boolean',
        'type' => 'required|in:survey,questionnaire',
        'profile_id' => 'integer',
        'anonymous' => 'required|boolean',
        'appointment_id' => 'integer'

    );

    /**
     * @return Profile
     */
    public function getProfile()
    {
        return app(ModelProfileService::class)->getProfileById($this->profile_id);
    }


    /** return associated template with that questionnaire
     * @return mixed
     */
    public function getTemplate()
    {
        return QuestionnaireService::getTemplate($this);
    }

    /** set Template Items
     * @param array $templateItems
     * @return bool
     */
    public function setTemplate($templateItems)
    {
        return QuestionnaireService::setTemplate($this, $templateItems);
    }

    /** set Result Items
     * @param $owner
     * @param array $resultItems
     * @return bool
     */
    public function setResult($owner, $resultItems)
    {
        return QuestionnaireService::setResult($this, $owner, $resultItems);
    }

    /** array of QuestionnaireResult
     * @return mixed
     */
    public function getResults()
    {
        return QuestionnaireService::getResults($this);
    }

    /** paginated QuestionnaireResults by owner
     * @param int $page
     * @param int $perPage
     * @return mixed
     */
    public function getPaginatedResults($page = 0, $perPage = 0)
    {
        return QuestionnaireService::getPaginatedResults($this->id, $page, $perPage);
    }

    /** ! NOT TESTED !
     * @return mixed
     */
    public function getStatistics()
    {
        return QuestionnaireService::getResultStatistics($this);
    }
}

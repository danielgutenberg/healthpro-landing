<?php namespace WL\Modules\Questionnaires;

use Illuminate\Support\Facades\DB;
use WL\Repositories\DbRepository;

/**
 * Class QuestionnaireResultRepository
 * @package WL\Modules\Questionnaires
 */
class QuestionnaireResultRepository extends DbRepository
{

    /**
     *
     */
    public function __construct()
    {
        $this->model = new QuestionnaireResult();
    }

    /**
     * @param array $options
     * @return Questionnaire
     */
    public function createQuestionnaireResult($options = [])
    {
        return new Questionnaire($options);
    }

    /**
     * @param $questionnaireId
     * @param int $page
     * @param int $perPage
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getPagedResultsGroupedByOwner($questionnaireId, $page = 0, $perPage = 0)
    {
        $query = DB::table('questionnaire_result')
            ->join('questionnaire', 'questionnaire.id', '=', 'questionnaire_result.questionnaire_id')
            ->select('questionnaire_result.owner_id', 'questionnaire_result.updated_at', 'questionnaire_result.questionnaire_id', 'questionnaire.anonymous')
            ->where('questionnaire_result.questionnaire_id', $questionnaireId)
            ->groupBy('owner_id')
            ->orderBy('questionnaire_result.updated_at');

        if ($perPage > 0) {
            $query = $query->skip($page * $perPage)->take($perPage);
        }

        return QuestionnaireResult::hydrate($query->get());
    }

    /**
     * @param $questionnaireId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getResults($questionnaireId)
    {
        $query = DB::table('questionnaire_result')
            ->join('questionnaire_question', 'questionnaire_result.question_id', '=', 'questionnaire_question.id')
            ->join('questionnaire', 'questionnaire.id', '=', 'questionnaire_result.questionnaire_id')
            ->select('questionnaire_result.*', 'questionnaire_question.field_name', 'questionnaire_question.field_type', 'questionnaire_question.order', 'questionnaire.anonymous')
            ->where('questionnaire_result.questionnaire_id', $questionnaireId)
            ->orderBy('owner_id');

        return QuestionnaireResult::hydrate($query->get());
    }

    /**
     * @param $questionnaireId
     * @param $ownerId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getResult($questionnaireId, $ownerId)
    {
        $query = DB::table('questionnaire_result')
            ->join('questionnaire_question', 'questionnaire_result.question_id', '=', 'questionnaire_question.id')
            ->join('questionnaire', 'questionnaire.id', '=', 'questionnaire_result.questionnaire_id')
            ->select('questionnaire_result.*', 'questionnaire_question.field_name', 'questionnaire_question.field_type', 'questionnaire_question.order', 'questionnaire.anonymous')
            ->where('questionnaire_result.questionnaire_id', $questionnaireId)
            ->where('questionnaire_result.owner_id', $ownerId)
            ->orderBy('owner_id');

        return QuestionnaireResult::hydrate($query->get());
    }

    /**
     * @param $item
     * @return mixed
     */
    private function saveItem($item)
    {
        $item->isValidOrFail();
        return $item->save();
    }

    /**
     * @param $questionnaireId
     * @param $owner
     * @return mixed
     * @throws QuestionnaireCannotSave
     */
    public function deleteResult($questionnaireId, $owner)
    {
        if (!(isset($owner->id))) {
            throw new QuestionnaireCannotSave('owner id property not set');
        }

        return QuestionnaireResult::where('questionnaire_id', $questionnaireId)
            ->where('owner_id', $owner->id)
            ->delete();
    }


    /**
     * @param $questionnaireId
     * @param $owner
     * @param $items
     * @return bool
     * @throws QuestionnaireCannotSave
     */
    public function setResult($questionnaireId, $owner, $items)
    {
        if (!(isset($owner->id))) {
            throw new QuestionnaireCannotSave('owner id property not set');
        }

        $templateRepo = new QuestionnaireQuestionRepository();

        $objects = [];
        $result = count($items) > 0;
        if ($result) {
            foreach ($items as $item) {

                $object = new QuestionnaireResult();
                $object->fill(array_merge($item,
                    [
                        'questionnaire_id' => $questionnaireId,
                        'owner_id' => $owner->id
                    ]));

                if (isset($object->question_id)) {

                    $answers = $templateRepo->getPredefinedAnswers($object->question_id);
                    if (strlen($answers) > 0) {
                        $object->setPredefinedRule($answers);
                    }

                }

                array_push($objects, $object);

                $x = $object->isValid();
                $result = $result && ($x != 0);
            }

            if ($result) {
                $this->deleteResult($questionnaireId, $owner);

                foreach ($objects as $item) {
                    $this->saveItem($item);
                }
            }
        }

        return $result;
    }

    /**
     * @param $questionnaireId
     * @return mixed
     */
    public function getStatistics($questionnaireId)
    {

        $results = DB::table('questionnaire_result')
            ->select('id,question_id,field_value', DB::raw('count(*) as total'))
            ->where('questionnaire_id', $questionnaireId)
            ->groupBy('question_id', 'field_value')
            ->get();

        return $results;
    }


}



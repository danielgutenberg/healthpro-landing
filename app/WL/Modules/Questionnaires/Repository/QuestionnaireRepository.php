<?php namespace WL\Modules\Questionnaires;

use WL\Repositories\DbRepository;

/**
 * Class QuestionnaireRepository
 * @package WL\Modules\Questionnaires
 */
class QuestionnaireRepository extends DbRepository
{

    /**
     *
     */
    public function __construct()
    {
        $this->model = new Questionnaire();
    }

    /**
     * @param array $options
     * @return Questionnaire
     */
    public function createQuestionnaire($options = [])
    {
        return new Questionnaire($options);
    }

    /**
     * @param $appointmentId
     * @return mixed
     */
    public function getByAppointmentId($appointmentId)
    {
        return Questionnaire::where('appointment_id', $appointmentId)->get();
    }


    /**
     * @param $questionnaireId
     * @return mixed
     */
    public function removeQuestionnaire($questionnaireId)
    {
        return Questionnaire::where('id', $questionnaireId)->delete()>0;
    }

    /**
     * @param $questionnaire
     * @return mixed
     */
    public function saveQuestionnaire($questionnaire)
    {
        $questionnaire->isValidOrFail();
        return $questionnaire->save();
    }


    /**
     * @param $query
     * @param $types
     * @return mixed
     */
    function types($query, $types)
    {
        foreach ($types as $item) {
            $query = $query->where('type', $item);
        }

        return $query;
    }


    /**
     * @param array $types
     * @param int $page
     * @param int $perPage
     * @return mixed
     */
    public function listPublicQuestionnaires($types = [], $page = 0, $perPage = 0)
    {
        $query = Questionnaire::where('is_public', 1);
        $query = $this->types($query, $types);

        return $this->scopeSkip($query, $page, $perPage)->get();
    }

    /**
     * @param $owner
     * @param array $types
     * @param int $page
     * @param int $perPage
     * @return mixed
     */
    public function listMyQuestionnaires($owner, $types = [], $page = 0, $perPage = 0)
    {
        $query = Questionnaire::where('owner_id', $owner->id)
            ->where('owner_type', get_class($owner));

        $query = $this->types($query, $types);

        return $this->scopeSkip($query, $page, $perPage)->get();
    }

    /**
     * @param $query
     * @param $field
     * @param $value
     * @return mixed
     */
    public function scopeLike($query, $field, $value)
    {
        return $query->where($field, 'LIKE', "%$value%");
    }

    /**
     * @param $query
     * @param $page
     * @param $perPage
     * @return mixed
     */
    public function scopeSkip($query, $page, $perPage)
    {
        if ($perPage > 0) {
            return $query->skip($page * $perPage)->take($perPage);
        }
        return $query;
    }

    /**
     * @param $profileId
     * @param $term
     * @param int $page
     * @param int $perPage
     * @return mixed
     */
    public function searchQuestionnaire($profileId, $term, $page = 0, $perPage = 0)
    {
        $query = Questionnaire::where('profile_id', $profileId);
        $query = $this->scopeLike($query, 'name', $term);
        $query = $this->scopeSkip($query, $page, $perPage);
        return $query->get();
    }

    /**
     * @param $term
     * @param int $page
     * @param int $perPage
     * @return mixed
     */
    public function searchPublicQuestionnaire($term, $page = 0, $perPage = 0)
    {
        $query = Questionnaire::where('is_public', 1);
        $query = $this->scopeLike($query, 'name', $term);
        $query = $this->scopeSkip($query, $page, $perPage);
        return $query->get();
    }
}



<?php namespace WL\Modules\Questionnaires;

use Carbon\Carbon;
use WL\Repositories\DbRepository;

/**
 * Class QuestionnaireQuestionRepository
 * @package WL\Modules\Questionnaires
 */
class QuestionnaireQuestionRepository extends DbRepository
{

    /**
     *
     */
    public function __construct()
    {
        $this->model = new QuestionnaireQuestion();
    }

    /**
     * @param array $options
     * @return QuestionnaireQuestion
     */
    public function createQuestionnaireTemplate($options = [])
    {
        return new QuestionnaireQuestion($options);
    }

    /**
     * @param $questionnaireId
     * @return mixed
     */
    public function getTemplate($questionnaireId)
    {
        $items = QuestionnaireQuestion::where('questionnaire_id', $questionnaireId)
            ->orderBy('order', 'asc')->get();
        return $items;
    }

    /**
     * @param $templateRow
     * @return mixed
     */
    private function saveItem($templateRow)
    {
        $templateRow->isValidOrFail();
        return $templateRow->save();
    }

    /**
     * @param $questionnaireId
     * @return mixed
     */
    public function deleteTemplate($questionnaireId)
    {
        return QuestionnaireQuestion::where('questionnaire_id', $questionnaireId)->delete();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getPredefinedAnswers($id)
    {
        return QuestionnaireQuestion::where('id', $id)->pluck('field_predefined_answers')->first();
    }

    /**
     * @param $questionnaireId
     * @param $items
     * @return bool
     */
    public function saveTemplate($questionnaireId, $items)
    {
        $result = count($items) > 0;
        $objects = [];

        if ($result) {

            foreach ($items as $item) {

                $object = new QuestionnaireQuestion();
                $object->fill(array_merge($item,
                    [
                        'questionnaire_id' => $questionnaireId
                    ]));

                array_push($objects, $object);
                $result = $result && ($object->isValid() != 0);
            }

            if ($result) {
                $this->deleteTemplate($questionnaireId);
                foreach ($objects as $item) {
                    $item->questionnaire_id = $questionnaireId;
                    $this->saveItem($item);
                }
            }
        }

        return $result;
    }

    /**
     * @param $questionnaireIdFrom
     * @param $questionnaireIdTo
     * @return mixed
     */
    public function copyTemplate($questionnaireIdFrom, $questionnaireIdTo)
    {
        $items = $this->getTemplate($questionnaireIdFrom);
        $insert = [];

        $now = Carbon::now();
        foreach ($items as $item) {
            array_push($insert, [
                'questionnaire_id' => $questionnaireIdTo,
                'order' => $item->order,
                'field_type' => $item->field_type,
                'field_name' => $item->field_name,
                'field_predefined_answers' => $item->field_predefined_answers,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }

        return QuestionnaireQuestion::insert($insert);
    }

}



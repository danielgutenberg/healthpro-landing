<?php namespace WL\Modules\Questionnaires\Service;

use WL\Modules\Questionnaires\Questionnaire;
use WL\Modules\Questionnaires\QuestionnaireRepository;
use WL\Modules\Questionnaires\QuestionnaireResultRepository;
use WL\Modules\Questionnaires\QuestionnaireQuestionRepository;

/**
 * Class QuestionnaireService
 * @package WL\Modules\Questionnaires\Service
 */
class QuestionnaireService
{
    /**
     * @var QuestionnaireRepository
     */
    public $questionnaireRepo;
    /**
     * @var QuestionnaireQuestionRepository
     */
    public $templateRepo;
    /**
     * @var QuestionnaireResultRepository
     */
    public $resultRepo;

    /**
     *
     */
    public function __construct()
    {
        $this->questionnaireRepo = new QuestionnaireRepository();
        $this->templateRepo = new QuestionnaireQuestionRepository();
        $this->resultRepo = new QuestionnaireResultRepository();
    }

    /**
     * @param $id
     * @return Questionnaire
     */
    public function getQuestionnaireById($id)
    {
        return $this->questionnaireRepo->getById($id);
    }

    /**
     * @param $id
     * @return bool
     */
    public function removeQuestionnaireById($id)
    {
        return $this->questionnaireRepo->removeQuestionnaire($id);
    }

    /** update or create class
     * @param array $questionnaireData
     * @param null $questionnaireId if create leave null
     * @return Questionnaire
     */
    public function setQuestionnaire($questionnaireData, $questionnaireId = null)
    {

        if ($questionnaireId != null) {
            $questionnaire = $this->questionnaireRepo->getById($questionnaireId);
        } else {
            $questionnaire = new Questionnaire();
        }

        if (isset($questionnaire)) {
            $questionnaire->fill($questionnaireData);
        }

        return $questionnaire;
    }

    /**
     * @param Questionnaire $questionnaire
     * @return bool
     */
    public function saveQuestionnaire($questionnaire)
    {
        return $this->questionnaireRepo->saveQuestionnaire($questionnaire);
    }


    /** return associated QuestionnaireTemplate fields
     * @param $questionnaire
     * @return array(QuestionnaireTemplate)
     */
    public function getTemplate($questionnaire)
    {
        return $this->templateRepo->getTemplate($questionnaire->id);
    }

    /**
     * @param $owner
     * @param $page
     * @param $perPage
     * @param array $types
     * @return mixed
     */
    public function paginateQuestionnaire($owner, $page, $perPage, $types = [])
    {
        return $this->questionnaireRepo->listMyQuestionnaires($owner, $types, $page, $perPage);
    }

    /**
     * @param $page
     * @param $perPage
     * @param $types
     * @return mixed
     */
    public function paginatePublicQuestionnaire($page, $perPage, $types)
    {
        return $this->questionnaireRepo->listPublicQuestionnaires($types, $page, $perPage);
    }

    /**
     * @param $profileId
     * @param $term
     * @param int $page
     * @param int $perPage
     * @return mixed
     */
    public function searchQuestionnaire($profileId, $term, $page = 0, $perPage = 0)
    {
        return $this->questionnaireRepo->searchQuestionnaire($profileId, $term, $page, $perPage);
    }

    /**
     * @param $term
     * @param int $page
     * @param int $perPage
     * @return mixed
     */
    public function searchPublicQuestionnaire($term, $page = 0, $perPage = 0)
    {
        return $this->questionnaireRepo->searchPublicQuestionnaire($term, $page, $perPage);
    }


    /** copy Questionnaire to Profile
     * @param $questionnaireId
     * @param $ownerProfileId
     * @return Questionnaire
     */
    public function copyQuestionnaire($questionnaireId, $ownerProfileId)
    {
        $questionnaire = new Questionnaire();
        $targetQuestionnaire = $this->questionnaireRepo->getById($questionnaireId);

        $questionnaire->fill(array_merge($targetQuestionnaire->toArray(), [
            'profile_id' => $ownerProfileId,
            'is_public' => false,
            'appointment_id' => null,
            'id'=>null
        ]));

        $this->questionnaireRepo->saveQuestionnaire($questionnaire);

        return $questionnaire;
    }


    /**
     * @param $questionnaire
     * @return array
     */
    private function getHashedTemplate($questionnaire)
    {
        $rTemplate = $this->templateRepo->getTemplate($questionnaire->id);
        $hTemplate = [];
        foreach ($rTemplate as $item) {
            $hTemplate[$item->id] = $item;
        }

        return $hTemplate;
    }


    /** associate QuestionnaireTemplate with Questionnaire
     * @param Questionnaire $questionnaire
     * @param array $templateItems
     * @return bool
     */
    public function setTemplate($questionnaire, $templateItems)
    {
        return $this->templateRepo->saveTemplate($questionnaire->id, $templateItems);
    }

    /**
     * @param $questionnaire
     * @return bool
     */
    public function deleteTemplate($questionnaire)
    {
        return $this->templateRepo->deleteTemplate($questionnaire->id);
    }


    /** set result to Questionnaire
     * @param Questionnaire $questionnaire
     * @param \WL\Modules\User\Models\User $owner
     * @param array $resultItems
     * @return bool
     * @throws \WL\Modules\Questionnaires\QuestionnaireCannotSave
     */
    public function setResult($questionnaire, $owner, $resultItems)
    {
        return $this->resultRepo->setResult($questionnaire->id, $owner, $resultItems);
    }

    /** untested
     * @param $questionnaire
     * @return mixed
     */
    public function getResultsStatistics($questionnaire)
    {
        $template = $this->templateRepo->getTemplate($questionnaire->id);
        $statistics = $this->resultRepo->getStatistics($questionnaire->id);

        return $statistics;
    }

    /** get result of concrete User
     * @param $questionnaire
     * @param $ownerId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getResult($questionnaire, $ownerId)
    {
        return $this->resultRepo->getResult($questionnaire->id, $ownerId);
    }

    /** get paginated Results of Questionnaire
     * @param Questionnaire $questionnaire
     * @param int $page
     * @param int $perPage
     * @return array
     */
    public function getResults($questionnaire, $page = 0, $perPage = 0)
    {
        $results = $this->resultRepo->getResults($questionnaire->id, $page, $perPage);

        $compiledResults = [];
        foreach ($results as $item) {

            if (!isset($compiledResults[$item->owner_id]))
                $compiledResults[$item->owner_id] = [];

            array_push($compiledResults[$item->owner_id], $item);
        }

        return $compiledResults;
    }

    /** get paginated Results with grouping by owner
     * @param $questionnaireId
     * @param int $page
     * @param int $perPage
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getPaginatedResults($questionnaireId, $page = 0, $perPage = 0)
    {
        return $this->resultRepo->getPagedResultsGroupedByOwner($questionnaireId, $page, $perPage);
    }


}


<?php namespace WL\Modules\Questionnaires;

use WL\Modules\Questionnaires\Service\QuestionnaireService;
use Cartalyst\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class QuestionnaireServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->instance('QuestionnaireService', new QuestionnaireService());
		$this->app->booting(function () {
			AliasLoader::getInstance()->alias('QuestionnaireService', 'WL\Modules\Questionnaires\Facade\QuestionnaireService');
		});
	}
}

<?php namespace WL\Modules\Questionnaires\Facade;

use Illuminate\Support\Facades\Facade;

class QuestionnaireService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'QuestionnaireService';
	}
}

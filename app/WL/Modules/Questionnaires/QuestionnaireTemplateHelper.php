<?php namespace WL\Modules\Questionnaires;

class QuestionnaireTemplateHelper extends Iterator
{
	private $position = '';
	private $data = [];
	private $first = '';
	private $last = '';

	public function __construct($first,$data) {

		$this->position = $first;
		$this->first = $first;
		$this->data = $data;
	}

	function rewind() {
		$this->position = $this->first;
	}

	function current() {
		return $this->data[$this->position];
	}

	function key() {
		return $this->position;
	}

	function next() {
		$this->position = $this->current()->order_next;
	}

	function valid() {
		return isset($this->data[$this->position]);
	}
}

<?php namespace WL\Modules\Questionnaires;

class QuestionnaireWithResultTransformer extends QuestionnaireBasicTransformer
{
	public function __construct($result)
	{
		$this->result = $result;
	}

	public function transform($item)
	{
		return array_merge(parent::transform($item), [
			'template' => (new QuestionnaireResultTransformer())->transformCollection($this->result)
		]);
	}
}

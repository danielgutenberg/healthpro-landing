<?php namespace WL\Modules\Questionnaires;

class QuestionnaireWithTemplateTransformer extends QuestionnaireBasicTransformer
{

	public function transform($item)
	{
		$template = $item->getTemplate()->all();
		return array_merge(parent::transform($item),
			[
				'question' => is_array($template)==true?(new QuestionnaireQuestionTransformer())->transformCollection($template):null
			]);
	}
}

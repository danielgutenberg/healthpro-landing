<?php namespace WL\Modules\Questionnaires;

use WL\Transformers\Transformer;

class QuestionnaireResultTransformer extends Transformer
{

	public function transform($item)
	{
		return [
			'id' => $item->id,
			'questionnaire_id' => $item->questionnaire_id,
			'owner_id' => $item->owner_id,
			'order' => $item->order,
			'field_name' => $item->field_name,
			'field_type' => $item->field_type,
			'field_value' => $item->field_value,
			'field_predefined_answers' => $item->field_predefined_answers
		];
	}
}

<?php namespace WL\Modules\Questionnaires;

use WL\Transformers\Transformer;

class QuestionnaireBasicTransformer extends Transformer
{

    public function transform($item)
    {
        return [
            'id' => $item->id,
            'name' => $item->name,
            'description' => $item->description,
            'is_public' => $item->is_public,
            'type' => $item->type,
            'anonymous' => $item->name,
            'profile_id' => $item->profile_id,
            'appointment_id' => $item->appointment_id,
        ];
    }
}

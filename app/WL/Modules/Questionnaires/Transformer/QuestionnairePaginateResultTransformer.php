<?php namespace WL\Modules\Questionnaires;

use WL\Transformers\Transformer;

class QuestionnairePaginateResultTransformer extends Transformer
{

    public function transform($item)
    {
        return [
            'id' => $item->id,
            'questionnaire_id' => $item->questionnaire_id,
            'owner' => $item->getOwner(),
            'anonymous' => $item->anonymous
        ];
    }
}

<?php namespace WL\Modules\Geocode;

use Illuminate\Config\Repository;
use Geocoder\Geocoder;
use Geocoder\HttpAdapter\CurlHttpAdapter;
use Geocoder\Provider\ProviderInterface;

class Geocode
{
	protected $geocoder = null;

	protected $adapter = null;

	protected $config = null;

	protected $apiKey = null;

	public function __construct(Repository $config, Geocoder $geocoder, CurlHttpAdapter $adapter) {
		$this->geocoder = $geocoder;
		$this->adapter  = $adapter;
		$this->config   = $config;

		if( $defaultProviders = $this->getDefaultProviders() ) {
			$this->addProviders($defaultProviders);
		}
	}

	public function getDefaultProviders() {
		return $providers = $this->getConfig('geocode')['providers'];
	}

	/**
	 * 	Add provider to stack ..
	 *
	 * @param ProviderInterface $provider
	 * @return \Geocoder\GeocoderInterface
	 */
	public function addProvider(ProviderInterface $provider) {
		return $this->getGeocoder()->registerProvider($provider);
	}

	/**
	 * 	Add providers to stack ..
	 *
	 * @param array $providers
	 */
	public function addProviders(array $providers) {
		foreach($providers as $provider) {
			$providerObject = new $provider($this->getAdapter(), null, null, true, $this->getConfig('geocode')['apiKey']);
			$this->addProvider($providerObject);
		}
	}

	/**
	 * Geocode doesn't has implement remove provider function, so we will reset geocoder object with null providers ..
	 *
	 * @return $this
	 */
	public function removeAllProviders() {
		unset($this->geocoder);

		$this->geocoder = new Geocoder;

		return $this;
	}


	/**
	 * 	Geocode Street Address, Zip Codes, etc ...
	 *
	 * @param null $adress
	 * @return \Geocoder\Result\ResultInterface|\Geocoder\ResultInterface
	 */
	public function geocode($adress = null) {
		return $this->getGeocoder()->geocode($adress);
	}

	/**
	 * 	Reverse Geocoding ...
	 *
	 * @param $lat
	 * @param $lon
	 * @return \Geocoder\Result\ResultInterface|\Geocoder\ResultInterface
	 */
	public function reverse($lat, $lon) {
		return $this->getGeocoder()->reverse($lat, $lon);
	}

	/**
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Helpers			                                                  *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */

	public function setLocale($locale) {
		return $this->getGeocoder()->setLocale($locale);
	}

	public function getLocale() {
		return $this->getGeocoder()->getLocale();
	}

	public function getConfig($key) {
		return $this->config->get($key);
	}

	public function getGeocoder() {
		return $this->geocoder;
	}

	public function getAdapter() {
		return $this->adapter;
	}
}

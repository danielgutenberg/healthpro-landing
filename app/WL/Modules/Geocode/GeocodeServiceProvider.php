<?php namespace WL\Modules\Geocode;

use Illuminate\Support\ServiceProvider;

use Geocoder\Geocoder;
use Geocoder\HttpAdapter\CurlHttpAdapter;


class GeocodeServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	public function boot()
	{
		////$this->package('wl/geocode', null, dirname(__FILE__));
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['geocode'] = $this->app->share(function($app) {
			return new Geocode($app['config'], new Geocoder, new CurlHttpAdapter);
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('geocode');
	}
}

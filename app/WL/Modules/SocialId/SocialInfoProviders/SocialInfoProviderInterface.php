<?php namespace WL\Modules\SocialId\SocialInfoProviders;

interface SocialInfoProviderInterface
{

	public function getProviderName();
	public function getDefaultUserInformation($token);

}

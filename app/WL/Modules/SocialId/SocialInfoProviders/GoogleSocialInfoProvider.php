<?php namespace WL\Modules\SocialId\SocialInfoProviders;

use League\OAuth2\Client\Exception\IDPException;
use League\OAuth2\Client\Provider\Google;
use League\OAuth2\Client\Token\AccessToken;
use WL\Modules\SocialId\Exceptions\SocialIdAccessTokenWrongOrExpired;

class GoogleSocialInfoProvider implements SocialInfoProviderInterface
{
	public function __construct()
	{
		$this->provider = new Google([]);
	}

	public function getProviderName()
	{
		return 'google';
	}

	public function getDefaultUserInformation($token)
	{
		$accessToken = new AccessToken(['access_token' => $token]);

		try {
			$user = $this->provider->getUserDetails($accessToken);

		} catch (IDPException $ex) {
			throw new SocialIdAccessTokenWrongOrExpired(_('AccessToken is wrong or expired'));
		}

		return $user;
	}

}

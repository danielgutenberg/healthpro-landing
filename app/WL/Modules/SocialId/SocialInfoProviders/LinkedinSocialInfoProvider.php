<?php namespace WL\Modules\SocialId\SocialInfoProviders;

use League\OAuth2\Client\Exception\IDPException;
use League\OAuth2\Client\Provider\LinkedIn;
use League\OAuth2\Client\Token\AccessToken;
use WL\Modules\SocialId\Exceptions\SocialIdAccessTokenWrongOrExpired;

class LinkedinSocialInfoProvider implements SocialInfoProviderInterface
{
	public function __construct()
	{
		$this->provider = new LinkedIn([]);
	}

	public function getProviderName()
	{
		return 'linkedin';
	}

	public function getDefaultUserInformation($token)
	{
		$accessToken = new AccessToken(['access_token' => $token]);

		try {
			$user = $this->provider->getUserDetails($accessToken);

		} catch (IDPException $ex) {
			throw new SocialIdAccessTokenWrongOrExpired(_('AccessToken is wrong or expired'));
		}

		return $user;
	}

}

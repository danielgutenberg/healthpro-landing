<?php namespace WL\Modules\SocialId\Services;

use Cartalyst\Sentinel\Addons\Social\Models\Link;
use Cartalyst\Sentinel\Addons\Social\Repositories\LinkRepository;

class SocialIdService implements SocialIdServiceInterface
{

	public function __construct(LinkRepository $repository, array $socialInfoProviders)
	{
		$this->repo = $repository;
		foreach ($socialInfoProviders as $provider) {
			$this->providers[$provider->getProviderName()] = $provider;
		}
	}

	public function getSocialInfo($socialNetwork, $accessToken)
	{
		if (isset($this->providers[$socialNetwork])) {
			return $this->providers[$socialNetwork]->getDefaultUserInformation($accessToken);
		}

		return null;

	}

	public function registerMe($socialNetwork, $socialId, $userId, $access_token = null)
	{
		$link = new Link(); //$this->repo->createModel();
		$link->uid = $socialId;
		$link->provider = $socialNetwork;
		$link->user_id = $userId;
		if ($access_token != null)
			$link->oauth2_access_token = $access_token;

		return $link->save();
	}

	public function getUserBySocialId($socialNetwork, $socialId)
	{
		$link = $this->repo->findLink($socialNetwork, $socialId);

		if ($link != null && $link->user != null)
			return $link->user;

		return null;
	}

	public function findLink($id)
	{
		return Link::find($id);
	}

	public function deleteLink($id)
	{
		return Link::destroy($id);
	}
}

<?php namespace WL\Modules\SocialId\Services;

use Cartalyst\Sentinel\Addons\Social\Repositories\LinkRepository;
use Illuminate\Support\ServiceProvider;
use WL\Modules\SocialId\SocialInfoProviders\FacebookSocialInfoProvider;
use WL\Modules\SocialId\SocialInfoProviders\GoogleSocialInfoProvider;
use WL\Modules\SocialId\SocialInfoProviders\LinkedinSocialInfoProvider;

class SocialIdServiceProvider extends ServiceProvider
{
	public function register()
	{

		$this->app->bind(SocialIdServiceInterface::class, function ($app) {

			return new SocialIdService(new LinkRepository(), [
					new FacebookSocialInfoProvider(),
					new LinkedinSocialInfoProvider(),
					new GoogleSocialInfoProvider()
				]
			);
		});
//
//		$this->app->booting(function () {
//			AliasLoader::getInstance()->alias('SocialIdService', Facade/SocialService::class);
//		});
	}

}

<?php namespace WL\Modules\SocialId\Services;


/**
 * Interface SocialIdServiceInterface
 * @package WL\Modules\SocialId\Services
 */
interface SocialIdServiceInterface
{
	/**
	 * Register user by Social network
	 *
	 * @param $socialNetwork
	 * @param $socialId
	 * @param $userId
	 * @param $access_token
	 * @return mixed
	 */
	public function registerMe($socialNetwork, $socialId, $userId, $access_token = null);

	/**
	 * Get User By Social network
	 *
	 * @param $socialNetwork
	 * @param $socialId
	 * @return mixed
     */
	public function getUserBySocialId($socialNetwork, $socialId);


	/**
	 * Get SocialNetwork Information about AccessToken owner
	 *
	 * @param $socialNetwork
	 * @param $accessToken
	 * @return mixed
     */
	public function getSocialInfo($socialNetwork, $accessToken);
}

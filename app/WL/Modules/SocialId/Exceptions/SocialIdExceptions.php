<?php namespace WL\Modules\SocialId\Exceptions;

use App\Exceptions\GenericException;

class SocialIdExceptions extends GenericException {}

class SocialIdAccessTokenWrongOrExpired extends SocialIdExceptions {}

class SocialIdEmailNotEqualsUserInput extends SocialIdExceptions {}

class SocialIdUIDlNotEqualsUserInput extends SocialIdExceptions {}



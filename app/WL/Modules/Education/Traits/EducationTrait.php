<?php
namespace WL\Modules\Education\Traits;

use App\Http\Requests\Admin\ProviderProfile\EducationRequest;
use Watson\Validating\ValidationException;
use WL\Modules\Education\Models\Education;
use Validator;

trait EducationTrait {

	protected $_pivoteducations = [];

	public function educations() {
		return $this->belongsToMany('\WL\Modules\Education\Models\Education', 'profiles_educations', 'profile_id', 'education_id')->withPivot('id', 'degree', 'from', 'to')->withTimestamps();
	}


	/**
	 *    Attach new Education ..
	 *
	 * @param Education $education
	 * @param array $params
	 * @internal param Education $Education
	 * @return bool
	 */
	public function attachEducation(Education $education, array $params) {
		$validator = Validator::make($params + ['education_id' => $education->id], EducationRequest::rules());

		if( $validator->fails() ) {
			$e = new ValidationException();
			$e->setErrors($validator->getMessageBag());
			throw $e;
		}

		if( $this->isAttachedEducation($education) ) {
			return $this->updateAttachedEducation($education, $params);
		}

		return $this->educations()->attach([$education->id => $params]);
	}

	/**
	 * 	Attach multiple educations
	 *
	 * @param array $educations
	 */
	public function attachEducations($educations = []) {
		if( count($educations) ) {
			foreach ($educations as $Education => $params) {

				$this->attachEducation(Education::find($Education)->first(), $params);
			}

		}
	}


	/**
	 *    Detach Education
	 *
	 * @param Education $education
	 * @internal param Education $Education
	 * @return mixed
	 */
	public function detachEducation(Education $education) {
		return $this->educations()->detach($education->id);
	}

	/**
	 * 	Detach multiple educations ..
	 *
	 * @param array $educations
	 */
	public function detachEducations($educations = []) {
		if( count($educations) ) {
			foreach ($educations as $education) {
				$this->detachEducation(Education::find($education)->first());
			}
		}
	}


	/**
	 *    Check if Education is attached ...
	 *
	 * @param Education $education
	 * @internal param Education $Education
	 * @return bool
	 */
	public function isAttachedEducation(Education $education) {
		$pivot = $this->getPivotEducation($education);

		if( isset($pivot->id) )
			return true;

		return false;
	}

	/**
	 *    Get pivot table by Education ..
	 *
	 * @param Education $education
	 * @internal param Education $Education
	 * @return mixed
	 */
	public function getPivotEducation(Education $education) {
		if( ! isset($this->_pivoteducations[$education->id]) ) {

			$pivot = $this->educations()->wherePivot('education_id', $education->id )->first();

			$this->_pivoteducations[$education->id] = $pivot;
		}

		return $this->_pivoteducations[$education->id];
	}

	/**
	 *    Update Attached Education ...
	 *
	 * @param Education $education
	 * @param array $params
	 * @internal param Education $Education
	 * @return bool
	 */
	public function updateAttachedEducation(Education $education, array $params) {
		if(  $this->isAttachedEducation($education) ) {
			$this->getPivotEducation($education)->pivot->update($params);

			return $this->getPivotEducation($education)->pivot->save();
		}

		return false;
	}

	/**
	 * 	Update attached Education by pivot ID ...
	 *
	 * @param $id
	 * @param array $params
	 * @return bool
	 */
	public function updateAttachedEducationById($id, array $params) {
		if( $pivot = $this->educations()->wherePivot('id', $id )->first() ) {
			$pivot->pivot->update($params);
			return $pivot->pivot->save();
		}

		return false;
	}

}

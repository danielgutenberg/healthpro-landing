<?php namespace WL\Modules\Education\Facade;

use Illuminate\Support\Facades\Facade;

class EducationService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'EducationService';
	}
}

<?php namespace WL\Modules\Education\Transformers;

use WL\Transformers\Transformer;

class EducationTransformer extends Transformer
{

	public function transform($item)
	{
		return [
			'id' => $item->id,
			'name' => $item->name,
		];
	}
}

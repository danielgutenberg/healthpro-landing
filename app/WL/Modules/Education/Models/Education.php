<?php namespace WL\Modules\Education\Models;

use WL\Models\ModelBase;

use WL\Contracts\Presentable;
use WL\Models\PresentableTrait;

class Education extends ModelBase implements Presentable {

	use PresentableTrait;

	protected $table = 'educations';

	protected $presenter = 'WL\Modules\Education\Presenters\EducationPresenter';

	protected $fillable = ['name', 'rank', 'is_custom'];

	protected $rules = [
		'name' => 'required|min:3|max:100',
	];

	public function profiles() {
		return $this->morphTo();
	}
}

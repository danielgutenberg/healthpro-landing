<?php

namespace WL\Modules\Education\Populators;

use WL\Modules\Education\Models\Education;

interface EducationPopulator {
	/**
	 * @param Education $page
	 *
	 * @return $this
	 */
	public function setModel(Education $page);
}

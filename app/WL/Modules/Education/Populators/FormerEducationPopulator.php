<?php namespace WL\Modules\Education\Populators;

use WL\Modules\Education\Models\Education;
use WL\Modules\Education\Populators\EducationPopulator;
use WL\Populators\FormerPopulator;

class FormerEducationPopulator extends FormerPopulator implements EducationPopulator {

	/**
	 * We accept only the Education model here
	 *
	 * @param Education $model
	 * @return $this
	 */
	public function setModel(Education $model) {
		$this->model = $model;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getFields() {
		$fields            = $this->model->toArray();

		return $fields;
	}
}

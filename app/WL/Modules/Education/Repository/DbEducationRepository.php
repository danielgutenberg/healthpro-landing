<?php namespace WL\Modules\Education\Repository;

use WL\Modules\Education\Models\Education;
use WL\Repositories\DbRepository;
use WL\Modules\Education\Repository\EducationRepository;
use WL\Repositories\Education\Profile;

class DbEducationRepository extends DbRepository implements EducationRepository {

	/**
	 * @var Profile
	 */
	protected $model;

	protected $modelClassName = '\WL\Modules\Education\Models\Education';

	public function __construct(Education $model) {
		$this->model = $model;
	}

	public function getList($filter = []) {
		$qBuilder = $this->model;

		foreach ($filter as $key => $value) {
			if ($key == 'limit')
				$qBuilder = $qBuilder->limit($value); elseif (preg_match("/^(\w+)(:like)$/", $key, $m))
				$qBuilder = $qBuilder->where($m[1], 'LIKE', $value);
			else
				$qBuilder = $qBuilder->where($key, '=', $value);
		}


		return $qBuilder->get();
	}

	public function getFiltered($filters = []) {

	}
}

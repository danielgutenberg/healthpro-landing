<?php namespace WL\Modules\Education\Repository;

interface EducationRepository {

	/**
	 * Get a list of pages with filters
	 *
	 * @param $filters
	 * @return mixed
	 */
	public function getFiltered( $filters = [] );

	/**
	 * Get by field value
	 * @param type $field
	 * @param type $value
	 * @return type
	 */
	public function getBy($field, $value);

}

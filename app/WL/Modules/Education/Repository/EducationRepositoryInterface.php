<?php namespace WL\Modules\Education\Repository;

/**
 * Interface EducationRepositoryInterface
 * @package WL\Modules\Education
 */
interface EducationRepositoryInterface
{
	/**
	 * @param $name
	 * @return mixed
	 */
	public function searchEducation($name);
}

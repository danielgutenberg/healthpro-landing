<?php namespace WL\Modules\Education\Repository;


use Illuminate\Support\Facades\DB;

class DbEducationRepositoryImpl implements EducationRepositoryInterface
{
	public function searchEducation($name)
	{
		return DB::table('educations')
			->where('name','LIKE',"%$name%")
			->get();
	}
}

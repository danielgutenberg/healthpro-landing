<?php namespace WL\Modules\Education\Commands;


use WL\Modules\Education\Facade\EducationService;
use WL\Modules\Profile\Commands\ValidatableCommand;

class SearchEducationCommand extends ValidatableCommand
{

	const IDENTIFIER = 'education.searchEducationCommand';

	protected $rules = [
		'q' => 'required',
	];

	public function validHandle()
	{
		$educations = EducationService::searchEducation($this->inputData['q']);
		return $educations;

	}
}

<?php namespace WL\Modules\Education\Service;

/**
 * Interface EducationService
 * @package WL\Modules\Education\Service
 */
interface EducationService
{
	/**
	 * @param $name
	 * @return mixed
	 */
	public function searchEducation($name);
}

<?php namespace WL\Modules\Education\Service;

use WL\Modules\Education\Repository\EducationRepositoryInterface;

class EducationServiceImpl implements EducationService
{

	public function __construct(EducationRepositoryInterface $repo)
	{
		$this->repo = $repo;
	}

	public function searchEducation($name)
	{
		return $this->repo->searchEducation($name);
	}
}

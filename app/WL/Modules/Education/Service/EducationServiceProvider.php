<?php namespace WL\Modules\Education\Service;

use Illuminate\Support\ServiceProvider;
use WL\Modules\Education\Repository\DbEducationRepositoryImpl;

class EducationServiceProvider extends ServiceProvider
{
	public function register()
	{
		$this->app->instance('EducationService', new EducationServiceImpl(
				new DbEducationRepositoryImpl()
			)
		);

		//$this->app->bind(EducationService::class, EducationServiceImpl::class);

	}
}


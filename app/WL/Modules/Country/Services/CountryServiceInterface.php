<?php namespace WL\Modules\Country\Services;

interface CountryServiceInterface
{
	/**
	 * Get All Countries
	 * @return Collection
	 */
	public function loadCountries();

	/**
	 * Get Country by country code
	 * @return Country
	 */
	public function getCountryByCode($code);

}

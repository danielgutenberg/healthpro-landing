<?php namespace WL\Modules\Country\Services;

use Illuminate\Support\Facades\Facade;

class CountryService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return CountryServiceImpl::class;
	}
}

<?php
namespace WL\Settings;

use Illuminate\Support\Facades\Schema;
use Yaml;

class Settings
{
	const TYPE_TEXT = 'text';

	const TYPE_TEXTAREA = 'textarea';

	const TYPE_CHECKBOX = 'checkbox';

	const TYPE_RADIO = 'radio';

	const TYPE_SELECT = 'select';

	const TYPE_NUMBER = 'number';

	const NAME_FIELD = 'name';

	const VALUE_FIELD = 'value';

	const PASSWORD_FIELD = 'password';

	/**
	 * @var Option
	 */
	private static $model;

	private static $settings;

	private static $dbSettings;

	private static $appSettings;

	public function __construct($model)
	{
		self::$model = $model;

		self::fetch();
	}

	/**
	 * Get the setting value
	 * If default value exists
	 * @param $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function get($key, $default = null)
	{
		$value = null;

		// check if the value exists in settings array
		if (array_key_exists($key, self::$settings)) {
			$value = self::$settings [$key];
			// then check if we have default value
		} elseif (null !== $default) {
			$value = $default;
			// and only then grab default value from config if exists
		} elseif (array_key_exists($key, self::$appSettings)) {
			$value = self::$appSettings[$key];
		}

		return $value;
	}

	/**
	 * Fetch an array of settings that begin with the specified prefix
	 * @param string $prefix
	 * @return array
	 */
	public function getPrefix($prefix)
	{
		$settings = [];
		foreach (self::$settings as $key => $value) {
			if(starts_with($key, $prefix) && $key !== $prefix){
				$settings[substr($key, strlen($prefix))] = $value;
			}
		}
		return $settings;
	}

	/**
	 * Fetch settings from the config and database
	 * and then merge them
	 *
	 * @param bool $force
	 */
	public static function fetch($force = false)
	{
		if (null === self::$settings || $force) {
			self::$dbSettings = Schema::hasTable('options') ? self::$model->all()->pluck(self::VALUE_FIELD, self::NAME_FIELD)->all() : [];
			self::$appSettings = Yaml::parse('wl.settings.app');
			self::$settings = array_merge(self::$appSettings, self::$dbSettings);
		}
	}

	/**
	 * Settings force reload
	 */
	public static function reload()
	{
		self::fetch(true);
	}

	/**
	 * Set setting
	 *
	 * @param $key
	 * @param string $value
	 */
	public function set($key, $value = '')
	{
		self::$model->updateOrCreate(array(
			self::NAME_FIELD => $key
		), array(
			self::NAME_FIELD => $key,
			self::VALUE_FIELD => $value
		));

		self::reload();
	}

	/**
	 * Check if the setting exists
	 *
	 * @param $key
	 * @return bool
	 */
	public function has($key)
	{
		return Yaml::get($key, 'wl.settings.app') ? true : false;
	}

	/**
	 * Get settings group with config
	 * @param $group
	 * @return mixed
	 */
	public function group($group)
	{
		$settings = Yaml::parse('wl.settings.' . $group);

		return $settings;
	}

	/**
	 * Get only group options
	 * @param $group
	 */
	public function groupOptions($group)
	{
		$groupData = self::group($group);
		if (array_key_exists('options', $groupData)) {
			$groupOptions = array();
			foreach ($groupData['options'] as $option) {
				$groupOptions[$option['field']] = self::get($option['field']);
			}
			return $groupOptions;
		} else {
			return false;
		}
	}

	/**
	 * Get settings array
	 *
	 * @return array
	 */
	public function all()
	{
		return self::$settings;
	}

	/**
	 * Reset settings
	 *
	 * @param null|string $group
	 */
	public function reset($group = null)
	{
		if ($group) {
			return self::resetGroup($group);
		}

		return self::$model->truncate();
	}

	/**
	 * Remove group options
	 * @param $group
	 * @return bool
	 */
	public function resetGroup($group)
	{
		$settings = self::groupOptions($group);

		if (is_array($settings) && count($settings)) {
			return self::$model->whereIn('name', array_keys($settings))->delete();
		}

		return true;
	}
}

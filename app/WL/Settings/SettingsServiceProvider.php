<?php
namespace WL\Settings;

use Illuminate\Support\ServiceProvider;
use WL\Settings\Models\Option;


class SettingsServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	public function boot()
	{
		////$this->package('wl/settings');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['settings'] = $this->app->share(function($app) {
			return new Settings( new Option );
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('settings');
	}
}

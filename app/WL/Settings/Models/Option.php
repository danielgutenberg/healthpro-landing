<?php
namespace WL\Settings\Models;

use WL\Models\ModelBase;

class Option extends ModelBase
{
	protected $table    = 'options';

	public $timestamps  = false;

	protected $fillable = array('name', 'value');

	protected $hidden   = array('id');
}

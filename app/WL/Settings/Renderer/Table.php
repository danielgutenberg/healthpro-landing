<?php
namespace WL\Settings\Renderer;

use WL\Settings\Exception as SettingsException;
use WL\Settings\Settings;
use Former;
use HtmlObject;
use URL;
use Lang;

class Table
{
	const CELL_HEAD = 'th';

	const CELL_BODY = 'td';

	const DEFAULT_WRAP = 'p';

	protected $config;

	protected $attributes;

	public function __construct( $config, $attributes )
	{
		$this->setConfig( $config )
			 ->setAttributes( $attributes );
	}

	public function setConfig($config)
	{
		$this->config = $config;

		return $this;
	}

	public function getConfig()
	{
		return $this->config;
	}

	public function setAttributes($attributes)
	{
		$this->attributes = $attributes;

		return $this;
	}

	public function getAttributes()
	{
		return $this->attributes;
	}

	/**
	 * @param $id
	 * @param $action
	 * @param string $method
	 * @return mixed
	 */
	public function openForm($id, $action, $method = 'POST')
	{
		return Former::open()
			->id( $id )
			->action( $action )
			->secure()
			->method( $method );
	}

	/**
	 * @return mixed
	 */
	public function closeForm()
	{
		return Former::close();
	}

	/**
	 * @return HtmlObject\Element
	 */
	public function hiddenFields()
	{
		$div = new HtmlObject\Element('div', null, array(
			'class' => 'hidden',
		));

		if (isset($this->attributes['_method'])) {
			$div->appendChild(
				Former::hidden('_method')->value($this->attributes['_method'])->label(false)
			);
		}

		return $div;
	}

	public function getOptions()
	{
		return isset($this->config['options']) ? $this->config['options'] : [];
	}

	public function render()
	{
		$html = '';

		$html .= $this->openForm(
			'form-entries-index',
			URL::to( $this->attributes['action'] )
		);

		$html .= $this->hiddenFields();

		$html .= $this->renderTable();

		$html .= $this->tableActions();

		$html .= $this->closeForm();

		echo $html;
	}


	public function renderTable()
	{
		$table = new HtmlObject\Element('table', '', array(
			'class' => 'table table-striped table-hover',
		));

		$head = new HtmlObject\Element('thead');

		$head->appendChild( $this->renderHeadRow() );

		$body = new HtmlObject\Element('tbody');

		foreach ($this->getOptions() as $option) {
			$body->appendChild(
				$this->renderRow($option)
			);
		}

		$table->appendChild($head)
			->appendChild($body);

		return $table;
	}

	public function tableActions()
	{
		$wrap = new HtmlObject\Element('div', null, array(
			'form-actions',
			'clearfix',
		));

		$save = Former::button( _('Submit') )->type('submit');

		$reset = new HtmlObject\Link(
			$this->attributes['reset'],
			_('Reset'),
			array(
				'class' => 'right',
			)
		);

		$wrap->appendChild(
			Former::actions( $save, $reset )
		);

		return $wrap;
	}


	public function renderHeadRow()
	{
		$tr = new HtmlObject\Element('tr');

		foreach (array(Settings::NAME_FIELD, Settings::VALUE_FIELD) as $cell) {
			$tr->appendChild(
				$this->renderCell(ucfirst($cell), self::CELL_HEAD)
			);
		}

		return $tr;
	}

	public function renderCell($data, $tag = self::CELL_BODY)
	{
		$cell = new HtmlObject\Element($tag, $data);

		return $cell;
	}

	public function renderRow( $row )
	{
		$tr = new HtmlObject\Element('tr');

		$field = $this->renderField( $row );

		$label = new HtmlObject\Element('label');
		$label->setValue( isset($row['label']) ? $row['label'] : $row['field'] )
			  ->setAttribute('for', $this->getInputName( $row ) );

		$tr->appendChild(
			$this->renderCell( $label )
		)->appendChild(
			$this->renderCell( $field )
		);

		return $tr;
	}


	/**
	 * @param array $field
	 * @return HtmlObject\Element
	 */
	public function renderField($field)
	{
		if (!isset($field['type'])) {
			$field['type'] = Settings::TYPE_TEXT;
		}

		// create the proper input
		//$input = Former::$field['type']( $this->getInputName( $field ) );
		$input = call_user_func_array(['Former', $field['type']], [$this->getInputName( $field )]);
		$input->label(false);

		if (isset($field['placeholder'])) {
			$input->placeholder( $field['placeholder'] );
		}

		if (isset($field['help'])) {
			$input->help( $field['help'] );
		}

		if (Settings::TYPE_SELECT === $field['type'] && isset($field['options'])) {
			$input->options( $field['options'] );
		}

		if (Settings::TYPE_RADIO === $field['type'] && isset($field['options'])) {
			$input->radios( $this->formattedRadioInput( $field ) );
		}

		// force value for checkbox
		if (Settings::TYPE_CHECKBOX === $field['type']) {
			$input->value( 1 );
		}

		return $input->wrapAndRender();
	}

	protected function formattedRadioInput( $field )
	{
		$radioValues = array();

		foreach( $field['options'] as $value => $label ) {
			$radioValues[ $label ] = array(
				'name'  => $this->getInputName( $field ),
				'value' => $value,
			);
		}

		return $radioValues;
	}

	protected function getInputName( $field )
	{
		return 'options[' . $field['field'] .  ']';
	}
}

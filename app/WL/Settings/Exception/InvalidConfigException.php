<?php

namespace WL\Settings\Exception;

use ErrorException as BaseErrorException;

class InvalidConfigException extends BaseErrorException
{

}

<?php
namespace WL\Gettext;

use Config;
use Session;
use File;

class Gettext
{
	/**
	 * variable holds the current locale
	 *
	 * @var string
	 */
	protected $locale = null;

	/**
	 * variable holds the current encoding
	 *
	 * @var string
	 */
	protected $encoding = null;

	/**
	 * constructor method
	 * accepts an optional $locale, otherwise the default will be used
	 *
	 * @internal param string $locale
	 */
	public function __construct ()
	{
		// check if a locale is present in the session
		// otherwise use default
		$session_locale = Session::get('gettext_locale', null);
		$locale = (is_null($session_locale)) ? Config::get("gettext.config.default_locale") : $session_locale;
		Session::forget('gettext_locale');

		// check if an encoding is present in the session
		$session_encoding = Session::get('gettext_encoding', null);
		$encoding = (is_null($session_encoding)) ? Config::get("gettext.config.default_encoding") : $session_encoding;
		Session::forget('gettext_encoding');

		// set the encoding and locale
		$this->setEncoding($encoding);

		// determine and set textdomain
		$textdomain = Config::get("gettext.config.textdomain");
		$path = Config::get("gettext.config.path_to_mo");

		$this->setTextDomain($textdomain, $path);
	}

	/**
	 * method to set the encoding
	 *
	 * @param string $encoding
	 * @return \WL\Gettext\Ggettext
	 * @throws InvalidEncodingException
	 */
	public function setEncoding ($encoding)
	{
		// fetch encodings list
		$encodings = Config::get('gettext.encodings.list');

		// sanity check
		if (!in_array($encoding, $encodings)) {
	        throw new InvalidEncodingException("The provided encoding [$encoding] does not exist in the list of valid encodings [gettextencodings.php]");
		}

		// set encoding, transform to uniform syntax
		$this->encoding = $encoding;

		// save locale to session
		Session::put('gettext_encoding', $this->encoding);

		// return - allow object chaining
		return $this;
	}

	/**
	 * method to set the locale
	 *
	 * @param string $locale
	 * @throws EnvironmentNotSetException
	 * @throws InvalidLocaleException
	 * @throws LocaleNotFoundException
	 * @return \WL\Gettext\Gettext
	 */
	public function setLocale ($locale)
	{
		// fetch locales list
		$locales = $this->getLocales();

		// sanity check
		if (!in_array($locale, $locales)) {
			throw new InvalidLocaleException("The provided locale [$locale] does not exist in the list of valid locales [config/locales.php]");
		}

		// set locale in class
		$this->locale = $locale;

		// get localecodeset
		$localecodeset = $this->getLocaleAndEncoding();

		// set environment variable
		if (!putenv('LC_ALL=' . $localecodeset)) {
			throw new EnvironmentNotSetException("The given locale [$localecodeset] could not be set as environment [LC_ALL] variable; it seems it does not exist on this system");
		}

		if (!putenv('LANG=' . $localecodeset)) {
			throw new EnvironmentNotSetException("The given locale [$localecodeset] could not be set as environment [LANG] variable; it seems it does not exist on this system");
		}

		// set locale - the exception is only thrown in case the app is NOT run from the command line
		// ignoring the cli creates a chicken and egg problem when attempting to fetch the installed locales/encodings,
		// since the ServiceProvider will always attempt to load the locale/encoding before the config files can even be
		// published; thus not allowing the user to change the default settings which should prevent the exception in the first place
		if (!setlocale(LC_ALL, $localecodeset) && !\App::runningInConsole()) {
			throw new LocaleNotFoundException("The given locale [$localecodeset] could not be set; it seems it does not exist on this system");
		}

		// save locale to session
		Session::put('gettext_locale', $this->locale);

		// return - allow object chaining
		return $this;
	}

	/**
	 * method to merge the locale and encoding into a single string
	 *
	 * @return string
	 */
	public function getLocaleAndEncoding ()
	{
		// windows compatibility - use only the locale, not the encoding
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
			return $this->getLocale();
		else
			return $this->getLocale() . "." . $this->getEncoding();

	}

	/**
	 * method to fetch the set encoding
	 *
	 * @return string
	 * @throws EncodingNotSetException
	 */
	public function getEncoding ()
	{
		// sanity check
		if (!$this->hasEncoding()) {
			throw new EncodingNotSetException("The encoding needs to be set before calling Gettext::getEncoding()");
		}

		// return encoding
		return $this->encoding;
	}

	/**
	 * method to fetch the set locale
	 *
	 * @return string
	 * @throws LocaleNotSetException
	 */
	public function getLocale ()
	{
		// sanity check
		if (!$this->hasLocale()) {
			throw new LocaleNotSetException("The locale needs to be set before calling Gettext::getLocale()");
		}

		// return locale
		return $this->locale;
	}

	/**
	 * method to check if an encoding has been set
	 *
	 * @return boolean
	 */
	public function hasEncoding ()
	{
		// check if encoding has been set
		return isset($this->encoding) && !is_null($this->encoding);
	}

	/**
	 * method to check if a locale has been set
	 *
	 * @return boolean
	 */
	public function hasLocale ()
	{
		// check if locale has been set
		return isset($this->locale) && !is_null($this->locale);
	}

	/**
	 * method to set the text domain
	 *
	 * @param string $textdomain
	 * @param string $path
	 * @return \WL\Gettext\Gettext
	 */
	public function setTextDomain ($textdomain, $path)
	{
	    $prefix = 'resources';

		$full_path = $this->getLocaleFolder($path);

		// sanity check - path must exist relative to app/ folder
		if (!File::isDirectory($full_path)) {
	        $this->createFolder( $prefix . DIRECTORY_SEPARATOR . $path );
		}

		// bind text domain
		bindtextdomain($textdomain, $full_path);

		// set text domain
		textdomain($textdomain);

		// allow object chaining
		return $this;
	}

	/**
	 * get locale folder path
	 *
	 * @param $path
	 * @param string $prefix
	 * @return string
	 */
	public function getLocaleFolder($path, $prefix = 'resources')
	{
		// full path to localization messages
		return base_path( $prefix . DIRECTORY_SEPARATOR . $path );
	}

	/**
	 * method which auto creates the LC_MESSAGES folder for each set locale, if they do not exist yet
	 *
	 * @param string $path
	 * @param null $locale
	 * @throws LocaleFolderCreationException
	 * @return \WL\Gettext\Gettext
	 */
	public function createFolder ($path, $locale = null)
	{
	    if (null === $locale) {
		    $locale = $this->getLocale();
	    }

		// set full path
		$full_path = base_path( $path . DIRECTORY_SEPARATOR . $locale . DIRECTORY_SEPARATOR . 'LC_MESSAGES' );

		// check if the folder exists
		if (!File::isDirectory($full_path)) {
			// folder does not exist, attempt to create it
			// throws an ErrorException when failed
			if (!File::makeDirectory($full_path, 0755, true)) {
	            throw new LocaleFolderCreationException("The locale folder [$full_path] does not exist and could not be created automatically; please create the folder manually");
			}
		}

		// allow object chaining
		return $this;
	}

	/**
	 * method to dump the current locale/encoding settings to string
	 *
	 * @return string
	 */
	public function __toString ()
	{
		return (string) $this->getLocaleAndEncoding();
	}

	/**
	 * get available locales list
	 *
	 * @return array
	 */
	public function getLocales()
	{
		return Config::get('gettext.locales.list');
	}

	/**
	 * Find .po and .mo files for current locale
	 *
	 * @param string|null $locale
	 * @return array|bool
	 */
	public function getLocaleFiles( $locale = null )
	{
		if (null === $locale) {
			$locale = $this->getLocale();
		}

		$path = Config::get("gettext.config.path_to_mo");
		$textdomain = Config::get("gettext.config.textdomain");

		// full path to localization messages
		$full_path = $this->getLocaleFolder( $path . DIRECTORY_SEPARATOR . $locale . DIRECTORY_SEPARATOR . 'LC_MESSAGES' );

		$mo = $full_path . DIRECTORY_SEPARATOR . $textdomain . '.mo';
		$po = $full_path . DIRECTORY_SEPARATOR . $textdomain . '.po';

		return File::exists( $mo ) && File::exists( $po ) ? ['mo' => $mo, 'po' => $po] : false;
	}

	/**
	 * Remove files for a given locale
	 * @param null $locale
	 * @return bool
	 */
	public function deleteLocaleFiles( $locale = null )
	{
		if (null === $locale) {
			$locale = $this->getLocale();
		}

		$files = $this->getLocaleFiles( $locale );

		if ($files) {
			File::delete( $files );
		}

		return $this;
	}

	public function setLocaleFiles( $files, $locale = null )
	{
		if (null === $locale) {
			$locale = $this->getLocale();
		}

		if (!is_array( $files ) || !isset( $files['po'] ) || !isset( $files['mo'] ) ) {
			return false;
		}

		$prefix = 'resources';
		$path = Config::get("gettext.config.path_to_mo");
		$textdomain = Config::get("gettext.config.textdomain");

		// full path to localization messages
		$full_path = $this->getLocaleFolder( $path . DIRECTORY_SEPARATOR . $locale . DIRECTORY_SEPARATOR . 'LC_MESSAGES' );

		// create dir if not exists
		if (!File::isDirectory($full_path)) {
			$this->createFolder( $prefix . DIRECTORY_SEPARATOR . $path, $locale );
		}

		foreach ($files as $type => $file) {
			$filename = $full_path . DIRECTORY_SEPARATOR . $textdomain . '.' . $type;
			if ( !File::move( $file, $filename ) ) {
				throw new FileMoveFailsException("The file [$file] cant't be moved to a new location.");
			}
		}

		return $this;
	}

	public function getPotPath()
	{
		$textdomain = Config::get("gettext.config.textdomain");
		$output = Config::get("gettext.config.compiler.output_folder");

		$path = storage_path() . DIRECTORY_SEPARATOR . $output . DIRECTORY_SEPARATOR . $textdomain . ".pot";

		return File::exists( $path ) ? $path : false;
	}
}

<?php
function getGlobPattern($levels) {
	$pattern = "*";
	for ($i = 1; $i <= $levels; $i++)
	{
		if ($pattern != "")
			$pattern .= ",";
		$pattern .= str_repeat("*/", $i);
	}
	return $pattern;
}
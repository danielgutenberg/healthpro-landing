<?php
namespace WL\Gettext;

use Illuminate\Support\ServiceProvider;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Process\ProcessBuilder;

class GettextServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
	//	//$this->package('wl/gettext');

		include_once __DIR__ . '/Exceptions.php';

		include_once __DIR__ . '/functions.php';

		$gt = new Gettext();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// register gettext and alias
		$this->registerGettext();

		// register blade compiler
		$this->registerGettextBladeCompiler();

		// register commands
		$this->registerCompileCommand();
		$this->registerExtractCommand();
		$this->registerListCommand();
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array("gettext");
	}

	public function registerGettext()
	{
		// register Gettext
		$this->app['gettext'] = $this->app->share(function($app) {
			return new Gettext();
		});
	}

	public function registerGettextBladeCompiler()
	{
		// register bladecompiler
		$this->app['bladecompiler'] = $this->app->share(function($app) {
			return new Compilers\BladeCompiler(new Filesystem, "");
		});
	}

	public function registerCompileCommand()
	{
		// add compile command to artisan
		$this->app['gettext.compile'] = $this->app->share(function($app) {
			return new Commands\CompileCommand();
		});
		$this->commands('gettext.compile');
	}

	public function registerExtractCommand()
	{
		// add extract command to artisan
		$this->app['gettext.extract'] = $this->app->share(function($app) {
			return new Commands\ExtractCommand(new ProcessBuilder);
		});
		$this->commands('gettext.extract');
	}

	public function registerListCommand()
	{
		// add list command to artisan
		$this->app['gettext.list'] = $this->app->share(function($app) {
			return new Commands\ListCommand();
		});
		$this->commands('gettext.list');
	}
}

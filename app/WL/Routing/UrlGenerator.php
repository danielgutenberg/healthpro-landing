<?php namespace WL\Routing;
use Illuminate\Routing\UrlGenerator as IlluminateUrlGenerator;
use WL\Manifest\Facades\Manifest;

class UrlGenerator extends IlluminateUrlGenerator
{
	public function asset($path, $secure = null, $appendVersion = true)
	{
		# get the generated path
		$url = parent::asset($path, $secure);

		# append version if needed
		return $appendVersion
			? $this->appendAssetsVersion($url)
			: $url;
	}

	protected function appendAssetsVersion($url)
	{
		$assetsVersion = Manifest::assetsVersion();

		if ($assetsVersion) {
			$parsedUrl = parse_url($url);
			$url .= (isset($parsedUrl['query']) ? '&' : '?') . 'ver=' . $assetsVersion;
		}

		return $url;
	}
}

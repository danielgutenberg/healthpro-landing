<?php namespace WL\MetaFields\Facades;

use Illuminate\Support\Facades\Facade;

class MetaFields extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'metafields';
	}
}

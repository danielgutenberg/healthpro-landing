<?php namespace WL\MetaFields;

use WL\Yaml\Facades\Yaml;

class MetaFields
{
	const FULL = 'full';

	const SHORT = 'short';

	const SLUG = 'slug';

	protected $metas;

	protected $prefix = 'wl.meta';

	public function __construct()
	{}

	protected function parse($model, $meta = false)
	{
		if (!isset($this->metas[$model])) {
			$this->metas[$model] = $this->parseConfig($model);
		}

		return $meta ? $this->metas[$model][$meta] : $this->metas[$model];
	}

	protected function parseConfig($model)
	{
		return Yaml::parse($this->prefix . '.' . $model);
	}

	public function listing($model, $meta)
	{
		return $this->parse($model, $meta);
	}

	public function options($model, $meta, $params = [])
	{
		return $this->buildOptions(
			$this->parse($model, $meta, $params)
		);
	}

	protected function buildOptions(array $values, $params = [])
	{
		$params = array_merge([
			'value'	=> self::SLUG,
			'name'	=> self::FULL,
		], $params);

		$options = [];
		foreach ($values as $val) {
			$options[ $val[$params['value']] ] = $val[$params['name']];
		}

		return $options;
	}

	public function check($value, $metaName)
	{

	}

	public function keys($model, $meta)
	{
		return array_keys($this->options($model, $meta));
	}

}

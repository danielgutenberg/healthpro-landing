<?php
namespace WL\MetaFields;

use Illuminate\Support\ServiceProvider;

class MetaFieldsServiceProvider extends ServiceProvider
{
	public function boot()
	{
		////$this->package('wl/metafields');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['metafields'] = $this->app->share(function($app) {
			return new MetaFields;
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('metafields');
	}
}

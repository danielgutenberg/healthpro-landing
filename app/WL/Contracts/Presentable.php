<?php namespace WL\Contracts;

/**
 * Interface Presentable
 * @package WL\Contracts
 */
interface Presentable
{
	/**
	 * @return mixed
	 */
	public function present();
}

<?php namespace WL\Contracts;

use Illuminate\Contracts\View\View;

interface Composable {

	/**
	 * View composer .
	 *
	 * @param View $view
	 * @return mixed
	 */
	public function compose(View $view);

}

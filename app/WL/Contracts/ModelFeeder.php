<?php namespace WL\Contracts;

interface ModelFeeder
{
	/**
	 * @param $request
	 *
	 * @return void
	 */
	public function setFeeder($request);

	/**
	 * @return mixed
	 */
	public function getFeeder();

	/**
	 * @return mixed
	 */
	public function hasFeeder();
}

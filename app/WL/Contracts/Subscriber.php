<?php namespace WL\Contracts;

interface Subscriber
{
	public function subscribe( $params );

	public function message();

	public function success();
}

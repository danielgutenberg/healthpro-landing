<?php
namespace WL\Subscriber;

use WL\Contracts\Subscriber;
use WL\Settings\Facades\Setting;
use Mailchimp_ValidationError;
use Mailchimp_List_DoesNotExist;
use WL\Integration\Mailchimp\Facades\Mailchimper;

class MailchimpSubscriber extends SubscriberAbstract implements Subscriber
{
	public function subscribe( $params = [] )
	{
		try {
			MailChimper::lists()->subscribe(Setting::get('landing_mailchimp_list_id'), $params);

			$this->success = true;

		} catch ( Mailchimp_ValidationError $e ) {
			$this->message = $e->getMessage();
		} catch ( Mailchimp_List_DoesNotExist $e ) {
			$this->message = $e->getMessage();
		}

		return $this->success;
	}
}

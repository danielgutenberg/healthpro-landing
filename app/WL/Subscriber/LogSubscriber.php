<?php
namespace WL\Subscriber;

use WL\Contracts\Subscriber;
use Illuminate\Contracts\Logging\Log;

class LogSubscriber extends SubscriberAbstract implements Subscriber
{
	public function subscribe( $params = [] )
	{
		if ( ! isset( $params['email'] ) || !$params['email']  ) {
			$this->message = 'Please set your email';
			$this->success = false;
		} else {
//            Log::info( '[EMAIL SUBSCRIPTION] ' . $params['email'] . ' subscribed for the newsletter.' );
			$this->message = '';
			$this->success = true;
		}

		return $this->success;
	}
}

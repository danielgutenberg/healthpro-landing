<?php
namespace WL\Subscriber;

abstract class SubscriberAbstract
{
	protected $message = '';

	protected $success = false;

	public function success()
	{
		return $this->success;
	}

	public function message()
	{
		return $this->message;
	}
}

<?php namespace WL\Controllers;

use \WL\Repositories\Repository as RepositoryInterface;

trait Repository {

	/**
	 * @var Repository Instance ..
	 */
	protected $_repository;

	/**
	 *    Set Repository ...
	 *
	 * @param RepositoryInterface $repository
	 */
	public function setRepository(RepositoryInterface $repository) {
		if(! $this->_repository)
			$this->_repository = $repository;
	}

	/**
	 * 	Get registered model repository
	 *
	 * @return bool
	 */
	public function getRepository($model = null) {
		if (! $this->_repository)
			return false;

		if( isset($model->id) )
			return $this->_repository->getById($model->id);

		return $this->_repository->createNew();
	}
}
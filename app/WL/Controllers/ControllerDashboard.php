<?php
namespace WL\Controllers;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use WL\AssetManager\Facades\AssetManager;

class ControllerDashboard extends ControllerSite
{
	protected $user;

	public function __construct()
	{

		AssetManager::setPrefix('assets/dashboard')->add([
			'styles/main.css',
		]);

		parent::__construct();
	}
}

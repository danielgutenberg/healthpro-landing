<?php
namespace WL\Controllers;

use \Illuminate\Support\ViewErrorBag;
use WL\AssetManager\Facades\AssetManager;

class ControllerAdmin extends ControllerBase
{
	protected $layout = 'admin.layouts.default';

	public function __construct() {
		parent::__construct();

		AssetManager::setPrefix('assets/admin')
			->add([
				'libs/modernizr/modernizr.js',
				'libs/jquery/dist/jquery.min.js',
				'libs/jquery-ui/jquery-ui.js',
				'libs/fastclick/lib/fastclick.js',
				'libs/foundation/js/foundation.min.js',
				'libs/jquery-form/jquery.form.js',
				'libs/jquery-ui/themes/ui-lightness/jquery-ui.min.css',
				'libs/foundation-datepicker/js/foundation-datepicker.js',
				'libs/foundation-datepicker/stylesheets/foundation-datepicker.css',

				'libs/datetimepicker/jquery.datetimepicker.js',
				'libs/datetimepicker/jquery.datetimepicker.css',

				'libs/fontawesome/css/font-awesome.min.css',
				'libs/FroalaWysiwygEditor/js/froala_editor.min.js',
				'libs/FroalaWysiwygEditor/css/froala_editor.min.css',
				'libs/FroalaWysiwygEditor/css/froala_style.min.css',

				'libs/select2/select2.min.js',
				'libs/select2/select2.css',
				'libs/tinymce/tinymce.min.js',


				'libs/jquery-tree/src/js/jquery.tree.js',
				'libs/jquery-tree/src/css/jquery.tree.css',
				'scripts/app.js',
				'styles/app.css',
				[
					'asset' => '//html5shim.googlecode.com/svn/trunk/html5.js',
					'params' => ['if' => 'lt IE 9']
				]
			]);

		#@todo Temporary . Laravel 5 bug form errors ..
		! \Session::has('errors') ? \View::share('errors',  new ViewErrorBag) : \View::share('errors', \Session::get('errors'));
	}
}

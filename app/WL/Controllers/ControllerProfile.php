<?php namespace WL\Controllers;

use App\Http\Requests\Admin\ProviderProfile\DeleteRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use WL\Modules\Appointment\Facades\AppointmentService;
use WL\Modules\PricePackage\Facade\PricePackageService;
use WL\Modules\Profile\Models\ModelProfile;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

use Session;
use Redirect;
use Illuminate\Support\Facades\Response;


class ControllerProfile extends ControllerAdmin {

	use Repository;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index() {
		$profiles = ModelProfile::withTrashed()->get();

		return view('admin.profiles.index', compact('profiles'));
	}

	public function restore($profileId)
	{
		try {
			$profile = ModelProfile::withTrashed()->find($profileId);
			/*
			 * make sure profile is not searchable when getting deleted
			 */
			$profile->restore();

			Session::flash('success', 'Profile successfully restored');
		} catch(ModelNotFoundException $e) {
			Session::flash('error', 'The profile with id ' . $profileId . ' cannot be found');
		}

		return Redirect::route('admin.profiles.index');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin/profiles/{id}
	 *
	 * @param ModelProfile $profile
	 * @internal param int $id
	 * @return Response
	 */
	public function destroy($profileId) {
		try {
			$profile = ModelProfile::withTrashed()->find($profileId);

			$return = false;
			$appointments = AppointmentService::getProviderAppointments($profile->id, ['confirmed', 'pending'], 'future');
			$appointments->each(function($appointment) use (&$return) {
				if (Carbon::now()->addHours(24)->gt(Carbon::parse($appointment->from_utc))) {
					Session::flash('error', 'Can\'t delete professional with upcoming appointments');
					$return = true;
					return false;
				};
			});

			if ($return) {
				return Redirect::route('admin.profiles.index');
			}

			$packages = PricePackageService::getProfilePricePackages($profile->id);
			$activePackages = $packages->filter(function($package) {
				$activePackages = $package->charges->filter(function($charge) {
					return $charge->entities_left != 0 && Carbon::now()->lt(Carbon::parse($charge->expires_on));
				});
				return !($activePackages->isEmpty());
			});

			if (!($activePackages->isEmpty())) {
				Session::flash('error', 'Can\'t delete professional with active packages');
				return Redirect::route('admin.profiles.index');
			}
			/*
			 * make sure profile is not searchable when getting deleted
			 */
			$profile->is_searchable = 0;
			$profile->save();
			$this->getRepository()->destroy($profileId);

			Session::flash('success', 'Entry successfully deleted');
		} catch(ModelNotFoundException $e) {
			Session::flash('error', 'The profile with id ' . $profileId . ' cannot be found');
		}

		return Redirect::route('admin.profiles.index');
	}

	/**
	 *    Remove profile by ids ...
	 *
	 * @param ModelProfile $profile
	 * @param DeleteRequest|FormRequest|Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete(ModelProfile $profile, DeleteRequest $request) {
		try {
			$profile->destroy( $request->fields() );

			$successMessage = 'Entries successfully deleted';

			Session::flash('success', $successMessage);

			if ($request->ajax()) {
				return Response::json([
					'success' => 1,
					'message' => $successMessage,
				]);
			}
		} catch (ModelNotFoundException $e) {
			Session::flash('error', 'The profile cannot be found');
		}
	}
}

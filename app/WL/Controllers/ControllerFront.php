<?php
namespace WL\Controllers;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use WL\AssetManager\Facades\AssetManager;

class ControllerFront extends ControllerSite
{
	protected $user;

	public function __construct()
	{
		AssetManager::setPrefix('assets/front')->add([
			'styles/main.css',
		]);

		parent::__construct();
	}
}

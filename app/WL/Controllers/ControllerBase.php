<?php
namespace WL\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;

class ControllerBase extends Controller
{
	public function __construct()
	{
	}

	/**
	 * @var array
	 */
	protected $renderVars = [];

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if (!is_null($this->layout)) {
			$this->layout = view($this->layout);
		}
	}

	/**
	 * Render page
	 *
	 * @param string $template
	 * @param array $vars
	 * @return Response
	 */
	protected function render($template, $vars = [])
	{
		return View::make($template, array_merge($this->renderVars, $vars));
	}

	/**
	 * Add variable to the renderer
	 *
	 * @param string $name
	 * @param mixed $value
	 */
	protected function renderVar($name, $value)
	{
		$this->renderVars[$name] = $value;
	}
}

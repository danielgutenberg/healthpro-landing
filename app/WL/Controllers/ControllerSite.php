<?php
namespace WL\Controllers;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Input;
use WL\Modules\Profile\Facades\ProfileService;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Contracts\Validation\ValidationException;
use WL\Modules\Profile\Models\ModelProfile;

class ControllerSite extends ControllerBase {

	use DispatchesJobs;

	use ValidatesRequests;

	protected $user;
	protected $renderVars = [];

	public function __construct() {
		$this->user = $user = Sentinel::check();

		$currentProfileId = ProfileService::getCurrentProfileId();
		try {
			$this->currentProfile = ProfileService::loadProfileBasicDetailsModel(ProfileService::getProfileById($currentProfileId));
			$this->renderVars['blockSignup'] = ($this->currentProfile->type == ModelProfile::CLIENT_PROFILE_TYPE || $this->currentProfile->is_searchable);
		} catch (\Exception $e) {
			$this->renderVars['blockSignup'] = false;
			$this->currentProfile = null;
		}

		parent::__construct();
	}

	/**
	 * Dispatch command with Validation exception handling
	 * @param $command
	 * @param $request
	 * @return mixed|null
	 */
	protected function runCommandWithErrorHandle($command, $request) {
		try {
			return $this->dispatch($command);
		} catch (ValidationException $e) {
			$this->throwValidationException($request, $e->getMessageProvider());
		}
	}

	/**
	 * Get parameters with default value from INPUT
	 *
	 * @param $parametersNameArray
	 * @return array
	 */
	protected function getParams($parametersNameArray) {
		$result = [];
		foreach ($parametersNameArray as $name => $def) {
			$result[] = Input::get($name, $def);
		}
		return $result;
	}

}

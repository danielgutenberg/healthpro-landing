<?php
namespace WL\Packages\Sentinel;

use Cartalyst\Sentinel\Throttling\EloquentThrottle;
use WL\Models\ModelTrait;

class Throttle extends EloquentThrottle
{
	use ModelTrait;
}

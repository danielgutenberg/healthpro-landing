<?php
namespace WL\Packages\Sentinel;

use Cartalyst\Sentinel\Persistences\EloquentPersistence;
use WL\Models\ModelTrait;

class Persistence extends EloquentPersistence
{
	use ModelTrait;
}

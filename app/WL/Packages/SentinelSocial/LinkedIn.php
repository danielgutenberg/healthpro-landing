<?php namespace WL\Packages\SentinelSocial;

class LinkedIn extends \League\OAuth2\Client\Provider\LinkedIn implements SocialExportableInterface
{

	public function __construct($options = [])
	{
		array_push($this->fields, 'picture-urls::(original)');

		parent::__construct($options);
	}

	public function requestFields($token, array $fields)
	{
		$url = 'https://api.linkedin.com/v1/people/~:(' . implode(",", $fields) . ')?format=json';
		$headers = $this->getHeaders($token);
		$response = $this->fetchProviderData($url, $headers);
		return json_decode($response);
	}

	public function requestFormatedFields($token, array $fields)
	{
		$raw = $this->requestFields($token, $fields);

		$result = new SocialFields();
		foreach ($raw as $name => $data) {
			if ($name == 'certifications') {
				foreach ($data->values as $item) {
					$result->certifications[] = $item->name;
				}

			} elseif ($name == 'dateOfBirth') {
				$result->birthday = $data;

			} elseif ($name == 'educations') {
				foreach ($data->values as $item) {
					$result->educations[] = (object)[
						'degree' => $item->degree,
						'name' => $item->schoolName,
						'from' => $item->startDate->year,
						'to' => $item->endDate->year,
					];
				}

			} elseif ($name == 'interests') {
				$result->interests = (string)$data;

			} elseif ($name == 'languages') {
				foreach ($data->values as $item) {
					$result->languages[] = $item->language->name;
				}

			} elseif ($name == 'location') {
				$result->location = (object)[
					'name' => $data->name,
					'code' => $data->country->code,
				];

			} elseif ($name == 'skills') {
				foreach ($data->values as $item) {
					$result->skills[] = $item->skill->name;
				}

			} elseif ($name == 'pictureUrls') {
				$result->pictureUrls = $data->values;

			}
		}
		$result->__raw = $raw;
		return $result;
	}

}

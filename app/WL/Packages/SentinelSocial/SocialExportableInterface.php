<?php namespace WL\Packages\SentinelSocial;

interface SocialExportableInterface {

	/**
	 * Make provider request for data by fields
	 * @param type $token
	 * @param type array $fields
	 * @return stdClass
	 */
	public function requestFields($token, array $fields);

	/**
	 * Make provider request for data by fields and return as SocialFields
	 * @param type $token
	 * @param type array $fields
	 * @return SocialFields
	 */
	public function requestFormatedFields($token, array $fields);
}

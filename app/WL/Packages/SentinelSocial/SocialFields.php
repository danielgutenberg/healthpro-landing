<?php namespace WL\Packages\SentinelSocial;

class SocialFields
{
	public $certifications = [];
	public $birthday = null;
	public $educations = [];
	public $interests = '';
	public $languages = [];
	public $location = null;
	public $skills = [];
	public $pictureUrls = [];
	public $__raw = null;
}

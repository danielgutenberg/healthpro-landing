<?php namespace WL\Packages\SentinelSocial;


class Facebook extends \League\OAuth2\Client\Provider\Facebook implements SocialExportableInterface {

    const DEFAULT_GRAPH_VERSION = 'v2.7';
    public $responseType = 'json';

    public function requestFields($token, array $fields)
	{
        $url = 'https://graph.facebook.com/'.$this->graphApiVersion.'/me?fields='.implode(",", $fields).'&access_token='.$token;

		$response = $this->fetchProviderData($url);
		return json_decode($response);
	}

	public function requestFormatedFields($token, array $fields)
	{
		$raw = $this->requestFields($token, $fields);

		$result = new SocialFields();
		foreach ($raw as $name => $data) {
			if ($name=='picture' && isset($data->data->url)) {
				$result->pictureUrls[] = $data->data->url;
			}
		}
		$result->__raw = $raw;

		return $result;
	}

}

<?php namespace WL\Packages\SentinelSocial;

class Google extends \League\OAuth2\Client\Provider\Google implements SocialExportableInterface {

	public function requestFields($token, array $fields)
	{

		$url = 'https://www.googleapis.com/plus/v1/people/me?fields='.urlencode(implode(",", $fields)).'&alt=json&access_token='.$token;
		// dd($url);
		$response = $this->fetchProviderData($url);
		return json_decode($response);
	}

	public function requestFormatedFields($token, array $fields)
	{
		$raw = $this->requestFields($token, $fields);

		$result = new SocialFields();
		foreach ($raw as $name => $data) {
			if ($name=='image' && isset($data->url)) {
				$result->pictureUrls[] = $data->url;
			}
		}
		$result->__raw = $raw;

		return $result;
	}

}

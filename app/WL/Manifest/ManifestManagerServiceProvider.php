<?php namespace WL\Manifest;

use Illuminate\Support\ServiceProvider;
use WL\Manifest\Manifest;

class ManifestServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['manifest'] = $this->app->share(function($app) {
			return new Manifest;
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['manifest'];
	}
}

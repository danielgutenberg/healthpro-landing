<?php namespace WL\Manifest\Facades;
use Illuminate\Support\Facades\Facade;

class Manifest extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'manifest';
	}
}

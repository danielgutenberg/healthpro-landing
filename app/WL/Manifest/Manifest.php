<?php namespace WL\Manifest;

use Illuminate\Support\Facades\File;
use WL\Manifest\Exceptions\SyntaxErrorException;

class Manifest
{
	protected $manifest = null;

	const MANIFEST_FILENAME = 'manifest.json';

	public function manifest()
	{
		if (null === $this->manifest)
			$this->readManifestFile();

		return $this->manifest;
	}

	public function value($key)
	{
		$manifest = $this->manifest();

		return is_object($manifest) && $manifest->$key
			? $manifest->$key
			: null;
	}

	public function assetsVersion()
	{
		return $this->value('assetsVersion');
	}

	protected function readManifestFile()
	{
		$path = public_path('assets/frontend/' . self::MANIFEST_FILENAME);

		if (!File::exists($path)) {
			return false;
		}

		$file = File::get($path);
		$json = json_decode($file);

		# handle json errors
		if (null === $json) {
			throw new SyntaxErrorException('Manifest file error: ' . json_last_error_msg());
		}

		# no manifest file at the moment
		return $this->setManifest($json);
	}

	protected function setManifest($json)
	{
		$this->manifest = $json;

		return $this->manifest;
	}
}

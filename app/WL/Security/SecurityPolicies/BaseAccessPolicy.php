<?php namespace WL\Security\SecurityPolicies;

use WL\Modules\Profile\Models\ModelProfile;

/**
 * Class ProfileAccessPolicy
 * @package WL\Security\SecurityPolicies
 */
class BaseAccessPolicy
{
	/**
	 * Is staff profile?
	 * @param $profile ModelProfile
	 * @return bool
	 */
	protected function isStaff($profile)
	{
		return $this->checkProfileType($profile, ModelProfile::STAFF_PROFILE_TYPE);
	}

	/**
	 * Is client profile?
	 * @param $profile ModelProfile
	 * @return bool
	 */
	protected function isClient($profile)
	{
		return $this->checkProfileType($profile, ModelProfile::CLIENT_PROFILE_TYPE);
	}

	/**
	 * Is provider profile?
	 * @param $profile ModelProfile
	 * @return bool
	 */
	protected function isProvider($profile)
	{
		return $this->checkProfileType($profile, ModelProfile::PROVIDER_PROFILE_TYPE);
	}

	/**
	 * Get profile type
	 * @param $profile ModelProfile
	 * @param $type string
	 * @return bool
	 */
	protected function checkProfileType($profile, $type)
	{
		$result = false;

		if ($type && $profile && $profile->type == $type)
			$result = true;

		return $result;
	}

	/**
	 * Is owner
	 *
	 * @param $userId
	 * @param $objectOwnerId
	 * @return bool
	 */
	protected function isOwner($userId, $objectOwnerId)
	{
		return (!is_null($userId) && ($userId == $objectOwnerId));
	}
}

<?php namespace WL\Security\Repositories;

use WL\Security\Repositories\Exceptions\ObjectAuthoritiesRepositoryException;
use WL\Yaml\Contracts\Parsable;

class YamlObjectAuthoritiesRepository implements ObjectAuthoritiesRepository {

	/**
	 * @var Parsable
	 */
	private $parser;

	/**
	 * Permissions source ..
	 *
	 * @var string
	 */
	private $source = 'wl.security.object-permissions';

	function __construct(Parsable $parser) {
		$this->parser = $parser;
	}

	/**
	 * Load permissions by indentifier ...
	 *
	 * @param string $identifier
	 * @return array()
	 * @throws ObjectAuthoritiesRepositoryException
	 */
	public function loadByIdentifier($identifier) {
		$permissions = self::getParser()->parse($this->source);

		return !empty($permissions[$identifier]) ? $permissions[$identifier] : array();
	}

	/**
	 * Get parser instace ...
	 *
	 * @return Parsable
	 */
	public function getParser() {
		return $this->parser;
	}
}

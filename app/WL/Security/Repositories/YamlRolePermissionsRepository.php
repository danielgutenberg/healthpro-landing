<?php namespace WL\Security\Repositories;

use WL\Yaml\Contracts\Parsable;

class YamlRolePermissionsRepository implements RolePermissionsRepository {

	/**
	 * @var Parsable
	 */
	private $parser;

	function __construct(Parsable $parser) {
		$this->parser = $parser;
	}

	public function getPermissionsForRole($role) {
		$rolePermissions = self::getParser()->parse('wl.security.role-permissions');
		return array_get($rolePermissions, $role);
	}

	public function getPermissionsForRoles(array $roles) {
		$rolePermissions = self::getParser()->parse('wl.security.role-permissions');
		return array_flatten(array_only($rolePermissions, $roles));
	}

	public function getRoles() {
		$rolePermissions = self::getParser()->parse('wl.security.role-permissions');
		foreach ($rolePermissions as &$perms) {
			$perms = array_flatten($perms);
			sort($perms);
		}
		return $rolePermissions;
	}

	/**
	 * Get parser instance ..
	 *
	 * @return Parsable
	 */
	public function getParser() {
		return $this->parser;
	}
}

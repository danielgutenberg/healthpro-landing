<?php namespace WL\Security\Repositories;


/**
 * Interface ProfilePermissionsRepository
 * @package WL\Security
 */
interface ProfilePermissionsRepository
{
	/**
	 * Get permissions by role
	 * @param $profileType
	 * @return array|null
	 */
	public function getPermissionsForProfileType($profileType);
}

<?php namespace WL\Security\Repositories\Exceptions;

use Exception;

class ObjectAuthoritiesRepositoryException extends Exception {}

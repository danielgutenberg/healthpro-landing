<?php namespace WL\Security\Repositories;

use WL\Security\Models\ApiKey;
use WL\Security\Repositories\Exceptions\StatusNotValidException;

/**
 * Interface UserApiKeyRepository
 * @package WL\SecurityRepositories\UserApiKey
 */
interface ApiKeyRepository
{

	/**
	 * Find ApiKey by accessKey
	 * @param string $accessKey An access key
	 * @return ApiKey
	 */
	public function findByApiAccessKey($accessKey);

	/**
	 * Update API keychain active status for user
	 * @param $userId integer
	 * @param $profileId integer
	 * @param null $status string Status
	 * @return bool Update status
	 * @throws StatusNotValidException
	 */
	public function updateApiKeyStatusFor($userId, $profileId, $status);

	/**
	 * Find keychain by user id
	 * @param $userId integer User id
	 * @param $profileId integer Profile id
	 * @return mixed
	 */
	public function findByUserProfile($userId, $profileId);

	/**
	 * Delete user API keys by user id
	 * @param $userId
	 * @param $profileId
	 * @return mixed
	 */
	public function deleteApiKey($userId, $profileId);
}

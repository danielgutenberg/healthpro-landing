<?php namespace WL\Security\Repositories;

use WL\Yaml\Contracts\Parsable;

class YamlProfilePermissionsRepository implements ProfilePermissionsRepository {

	/**
	 * @var Parsable
	 */
	private $parser;

	function __construct(Parsable $parser) {
		$this->parser = $parser;
	}

	public function getPermissionsForProfileType($profileType) {
		$profilePermissions = self::getParser()->parse('wl.security.profile-permissions');
		return array_get($profilePermissions, $profileType);
	}

	/**
	 * Get parser instance
	 *
	 * @return Parsable
	 */
	public function getParser() {
		return $this->parser;
	}
}

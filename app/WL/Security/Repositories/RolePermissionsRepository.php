<?php namespace WL\Security\Repositories;


/**
 * Interface RolePermissionsRepository
 * @package WL\Security
 */
interface RolePermissionsRepository
{
	/**
	 * Get permissions by role
	 * @param $role
	 * @return array|null
	 */
	public function getPermissionsForRole($role);

	/**
	 * Get permissions by roles
	 * @param array $roles
	 * @return array|null
	 */
	public function getPermissionsForRoles(array $roles);

	/**
	 * List all project roles
	 * @return type
	 */
	public function getRoles();

}

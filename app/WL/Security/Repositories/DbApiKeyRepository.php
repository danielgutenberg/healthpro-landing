<?php namespace WL\Security\Repositories;

use Illuminate\Database\QueryException;
use WL\Security\Models\ApiKey;
use WL\Security\Repositories\Exceptions\StatusNotValidException;

/**
 * DB implementation for storing API auth key's
 * Class DbUserApiKeyRepository
 * @package WL\Security\Repositories\UserApiKey
 */
class DbApiKeyRepository implements ApiKeyRepository
{
	/** Valid statuses
	 * @var array
	 */
	private $validStatuses = [ApiKey::STATUS_ACTIVE, ApiKey::STATUS_INACTIVE, ApiKey::STATUS_SUSPENDED];

	public function findByApiAccessKey($accessKey)
	{
		return ApiKey::where('api_access_key', $accessKey)->first();
	}

	public function updateApiKeyStatusFor($userId, $profileId, $status = null)
	{
		try {
			if ($this->isValidStatus($status)) {
				$apiKey = ApiKey::where('user_id', $userId)
					->where('profile_id', $profileId)
					->first();
				$result = $apiKey->update(['status' => $status]) != 0;
			} else {
				$joinedStatuses = implode(', ', $this->validStatuses);
				throw new StatusNotValidException(__("status is not valid. Use one of $joinedStatuses", 'user.api.key.status.not.valid'));
			}
		} catch (QueryException $qe) {
			$result = false;
		}

		return $result;
	}

	public function findByUserProfile($userId, $profileId)
	{
		return ApiKey::where('user_id', $userId)
			->where('profile_id', $profileId)
			->first();
	}

	public function deleteApiKey($userId, $profileId)
	{
		return ApiKey::where('user_id', $userId)
			->where('profile_id', $profileId)
			->delete();
	}

	private function isValidStatus($status)
	{
		return in_array($status, $this->validStatuses);
	}
}

<?php namespace WL\Security\Repositories;

/**
 * Interface ObjectAuthoritiesRepository
 *
 * Extend it if you need load ObjectAuthorities from DB, network...
 * @package WL\Security
 */
interface ObjectAuthoritiesRepository
{
	/**
	 * Loads ObjectAuthority by it's identifier
	 *
	 * @param $identifier string
	 * @return array|null
	 */
	public function loadByIdentifier($identifier);
}

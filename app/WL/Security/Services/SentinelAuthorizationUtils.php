<?php namespace WL\Security\Services;

use WL\Modules\User\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Security\Facades\Authorizer;
use WL\Modules\User\Services\UserServiceImpl;

class SentinelAuthorizationUtils
{
	/**
	 * Check if current user in roles
	 * @param array $roles
	 * @return mixed
	 */
	public function inRoles($roles = [])
	{
		$result = false;
		$user = Sentinel::check();

		if (!empty($user)) {
			$userRoles = array_merge($user->roles->pluck('slug')->all(), ['guest']);
			if (array_intersect($userRoles, $roles)) {
				$result = true;
			}
		}

		return $result;
	}

	/**
	 * Get logged in user
	 * @return \WL\Modules\User\Models\User|null
	 */
	public function loggedInUser()
	{
		return Sentinel::check();
	}

	/**
	 * Is user logged in?
	 * @return bool
	 */
	public function isLoggedIn()
	{
		if (Sentinel::check())
			return true;
		else
			return false;
	}

	/**
	 * Is user guest?
	 * @return boolean
	 */
	public function isUserGuest()
	{
		return Sentinel::guest();
	}

	/**
	 * Checks permissions for currently login in user and profile
	 * @param array $permissions
	 * @return boolean
	 */
	public function hasAccess(array $permissions)
	{
		return Authorizer::hasAccess(Sentinel::check(), ProfileService::getCurrentProfile(), $permissions);
	}

	/**
	 *
	 * Check If current profile is of specified type
	 *
	 * @param string $profileType ModelProfile::STAFF_PROFILE_TYPE||ModelProfile::PROVIDER_PROFILE_TYPE||ModelProfile::CLIENT_PROFILE_TYPE etc..
	 * @return bool
	 */
	public function hasProfileType($profileType)
	{
		$profileOk = false;

		if ($profileType) {
			$profile = ProfileService::getCurrentProfile();

			if ($profile && $profile->type == $profileType) {
				$profileOk = true;
			}

		}
		return $profileOk;
	}

	/**
	 *
	 * Check if user has specified roles OR profile is of specified type
	 *
	 * @param array $userRoles default:[UserServiceImpl::ADMIN_ROLE,UserServiceImpl::STAFF_ROLE]
	 * @param string $profileType default: ModelProfile::STAFF_PROFILE_TYPE
	 * @return bool
	 */
	public function hasRoleOrProfileType($userRoles = [UserServiceImpl::ADMIN_ROLE, UserServiceImpl::STAFF_ROLE], $profileType = ModelProfile::STAFF_PROFILE_TYPE)
	{
		$rolesOk = false;

		if ($userRoles) {
			$rolesOk = $this->inRoles($userRoles);
		}

		$profileOk = false;

		if ($profileType) {
			$profileOk = $this->hasProfileType($profileType);
		}

		return ($rolesOk || $profileOk);
	}

}

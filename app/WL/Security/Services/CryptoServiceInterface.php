<?php namespace WL\Security\Services;


/**
 * Interface UserApiKeyBasicOperations
 * Simplify public\private key generation
 * @package WL\UserApiKey
 */
interface CryptoServiceInterface
{
	/**
	 * Generates public key.
	 * @param mixed $seeds used for generate hash
	 * @return string
	 */
	public function generatePublicKey(...$seeds);

	/**
	 * Generates private key. sha256 is used
	 * @param mixed $seeds used for generate hash
	 * @return string 64 bytes hex
	 */
	public function generatePrivateKey(...$seeds);

	/**
	 * Creates 64 bytes signature
	 * @param string $publicKey public key
	 * @param integer $expire unix time
	 * @param integer $secretKey secret key
	 * @param array $options additional options (not used)
	 * @return string 64 bytes hex
	 */
	public function sign($publicKey, $expire, $secretKey, array $options = []);

	/**
	 * Validates signature
	 * @param string $signature 64 bytes hex
	 * @param string $publicKey 40 bytes hex
	 * @param integer $expire unix time
	 * @param string $secretKey 64 bytes hex
	 * @param array $options additional options (not used)
	 * @return bool
	 */
	public function isValid($signature, $publicKey, $expire, $secretKey, array $options = []);
}

<?php namespace WL\Security\Services\Exceptions;

use App\Exceptions\GenericExceptionAuthorization;

class AuthorizationException extends GenericExceptionAuthorization
{
}

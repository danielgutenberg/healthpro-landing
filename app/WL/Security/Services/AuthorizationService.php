<?php namespace WL\Security\Services;

use WL\Modules\Profile\Models\ModelProfile;
use WL\Security\Models\ObjectIdentity;


/**
 * Interface AuthorizationService
 *
 * Implement it for authorize you objects
 * @package WL\Security
 */
interface AuthorizationService
{
	/**
	 * Authorize user for using object
	 * @param $user
	 * @param $profile ModelProfile
	 * @param ObjectIdentity $object
	 * @return mixed
	 */
	public function authorize($user, $profile, ObjectIdentity $object);

	/**
	 * User has appropriate permissions?
	 * @param $user
	 * @param $profile ModelProfile
	 * @param array $requiredPermissions
	 * @return mixed
	 */
	public function hasAccess($user, $profile, array $requiredPermissions);


	/**
	 * Is user authorized for object?
	 *
	 * @param $user
	 * @param $profile
	 * @param ObjectIdentity $object
	 * @return mixed
	 */
	public function isAuthorized($user, $profile, ObjectIdentity $object);
}

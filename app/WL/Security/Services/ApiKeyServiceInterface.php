<?php namespace WL\Security\Services;


/**
 * Interface UserApiKeyCrud
 * @package WL\UserApiKey
 */
interface ApiKeyServiceInterface
{
	/**
	 * Generate keychain for user id
	 * @param $userId integer
	 * @param $profileId integer
	 * @return mixed
	 */
	public function generateKeysFor($userId, $profileId);

	/**
	 * Get Api Key by access key
	 * @param $accessKey
	 * @return mixed
	 */
	public function getApiKey($accessKey);

	/**
	 * Get api Key by user and profile
	 * @param $userId
	 * @param $profileId
	 * @return mixed
	 */
	public function getUserProfileKey($userId, $profileId);

	/**
	 * Update status of API keychain
	 * @param $userId integer
	 * @param $profileId integer
	 * @param $status  string
	 * @return mixed
	 */
	public function updateApiStatusFor($userId, $profileId, $status);

	/**
	 * Delete keychain by user id
	 * @param $userId integer
	 * @param $profileId integer
	 * @return mixed
	 */
	public function deleteKeysFor($userId, $profileId);

	/**
	 * Validates signature
	 * @param string $signature 64 bytes hex
	 * @param string $publicKey 40 bytes hex
	 * @param integer $expire unix time
	 * @param string $secretKey 64 bytes hex
	 * @return bool
	 */
	public function isValid($signature, $publicKey, $expire, $secretKey);
}

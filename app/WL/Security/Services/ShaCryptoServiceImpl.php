<?php namespace WL\Security\Services;


use Carbon\Carbon;
use WL\Security\Services\Exceptions\SeedNotProvidedException;

/**
 * Class ShaUserApiKeyBasicOperationsService
 * Simplify public\private key generation
 * @package WL\UserApiKey
 */
class ShaCryptoServiceImpl implements CryptoServiceInterface
{
	/**
	 * Time frame which a signature is valid
	 */
	const TIME_FRAME = 5 * 60; //5 min

	/**
	 * Generates public key. tiger160 is used
	 * @param mixed $seeds used for generate hash
	 * @return string 40 bytes hex
	 * @throws SeedNotProvidedException
	 */
	public function generatePublicKey(...$seeds)
	{
		return static::generateKey('tiger160,3', $seeds);
	}

	/**
	 * Generates private key. sha256 is used
	 * @param mixed $seeds used for generate hash
	 * @return string 64 bytes hex
	 * @throws SeedNotProvidedException
	 */
	public function generatePrivateKey(...$seeds)
	{
		return static::generateKey('sha256', $seeds);
	}

	/**
	 * Creates 64 bytes signature
	 * @param string $publicKey public key
	 * @param integer $expire unix time
	 * @param integer $secretKey secret key
	 * @param array $options additional options (not used)
	 * @return string 64 bytes hex
	 */
	public function sign($publicKey, $expire, $secretKey, array $options = [])
	{
		$strToSign = $publicKey . ':' . $expire;
		return base64_encode(hash_hmac('sha256', $strToSign, $secretKey, true));
	}

	/**
	 * Validates signature
	 * @param string $signature 64 bytes hex
	 * @param string $publicKey 40 bytes hex
	 * @param integer $expire unix time
	 * @param string $secretKey 64 bytes hex
	 * @param array $options additional options (not used)
	 * @return bool
	 */
	public function isValid($signature, $publicKey, $expire, $secretKey, array $options = [])
	{
		$now = time();
		$expire = intval($expire);
		$timeDiff = $expire - $now;

		// Expire time should be greater then the current time and
		// fit to predefined time frame
	
		if (($expire < $now) || ($timeDiff > static::TIME_FRAME)) {
			return false;
		}

		$validSignature = $this->sign($publicKey, $expire, $secretKey);
		
		return $signature === $validSignature;
	}

	/** Generates key. Helper method
	 * @param $algo string hash algorithm
	 * @param array $seeds
	 * @return string hash value
	 * @throws SeedNotProvidedException
	 */
	private static function generateKey($algo, $seeds)
	{
		if (!is_array($seeds)) {
			throw new SeedNotProvidedException(__("Seeds isn't provided", 'api.key.seeds.not.provided'));
		}

		//Add delimiter to prevent builtin||securely = built||insecurely
		$joinedSeeds = implode(':', $seeds);
		return base64_encode(hash($algo, $joinedSeeds, true));
	}
}

<?php namespace WL\Security\Services;

use WL\Security\Models\ApiKey;
use WL\Security\Repositories\ApiKeyRepository;


/**
 * Class UserApiKeyCrudService
 * @package WL\UserApiKey
 */
class ApiKeyServiceServiceImpl implements ApiKeyServiceInterface
{

	/**
	 * @var ApiKeyRepository
	 */
	private $apiKeyRepository;
	/**
	 * @var CryptoServiceInterface
	 */
	private $cryptoService;

	/**
	 * Time frame which a signature is valid
	 */
	const TIME_FRAME = 5 * 60; //5 min

	/**
	 * @param ApiKeyRepository $userApiKeyRepository
	 * @param CryptoServiceInterface $cryptoService
	 */
	function __construct(ApiKeyRepository $userApiKeyRepository, CryptoServiceInterface $cryptoService)
	{
		$this->apiKeyRepository = $userApiKeyRepository;
		$this->cryptoService = $cryptoService;
	}

	public function generateKeysFor($userId, $profileId)
	{
		$keyChain = $this->apiKeyRepository->findByUserProfile($userId, $profileId);
		if (!$keyChain) {
			$keyChain = new ApiKey();
		}

		$keyChain->user_id = $userId;
		$keyChain->profile_id = $profileId;
		$keyChain->api_access_key = $this->cryptoService->generatePublicKey($userId, $profileId, time(), mt_rand());
		$keyChain->api_secret_key = $this->cryptoService->generatePrivateKey($userId, $profileId, $keyChain->api_access_key, time(), mt_rand());
		$keyChain->status = ApiKey::STATUS_ACTIVE;
		$keyChain->save();
		return $keyChain;
	}

	public function getApiKey($accessKey)
	{
		return $this->apiKeyRepository->findByApiAccessKey($accessKey);
	}

	public function getUserProfileKey($userId, $profileId)
	{
		return $this->apiKeyRepository->findByUserProfile($userId, $profileId);
	}

	public function deleteKeysFor($userId, $profileId)
	{
		return $this->apiKeyRepository->deleteApiKey($userId, $profileId);
	}

	public function updateApiStatusFor($userId, $profileId, $status)
	{
		return $this->apiKeyRepository->updateApiKeyStatusFor($userId, $profileId, $status);
	}

	public function isValid($signature, $publicKey, $expire, $secretKey)
	{
		return $this->cryptoService->isValid($signature, $publicKey, $expire, $secretKey);
	}
}

<?php namespace WL\Security\Services;

use WL\Security\Models\ObjectIdentity;
use WL\Security\Repositories\ObjectAuthoritiesRepository;
use WL\Security\Repositories\ProfilePermissionsRepository;
use WL\Security\Repositories\RolePermissionsRepository;
use WL\Security\Services\Exceptions\AuthorizationException;
use WL\Yaml\Parser;

class AuthorizationServiceImpl implements AuthorizationService
{
	private $objectAuthoritiesRepository;
	private $rolePermissionsRepository;
	private $profilePermissionsRepository;

	function __construct(ObjectAuthoritiesRepository $objectAuthoritiesRepository, RolePermissionsRepository $rolePermissionsRepository, ProfilePermissionsRepository $profilePermissionsRepository)
	{
		$this->objectAuthoritiesRepository = $objectAuthoritiesRepository;
		$this->rolePermissionsRepository = $rolePermissionsRepository;
		$this->profilePermissionsRepository = $profilePermissionsRepository;
	}

	public function authorize($user, $profile, ObjectIdentity $object)
	{
		if (empty($object))
			throw new AuthorizationException(__('Object identity is not provided', 'security.authorization.object.is.not.provided'));

		$identifier = $object->getIdentifier();

		$objectPermissions = $this->objectAuthoritiesRepository->loadByIdentifier($identifier);

		if (!$objectPermissions)
			throw new AuthorizationException(__('No permissions found for '.get_class($object).' object'));

		if (!$this->hasAccess($user, $profile, $objectPermissions))
			throw new AuthorizationException(__("The User and profile is not authorized for '".implode(",", $objectPermissions)."' to '".get_class($object)."' object", 'security.authorization.user.is.not.authorized.for.object'));
	}

	public function hasAccess($user, $profile, array $requiredPermissions)
	{
		$result = false;

		// Initially I'm a guest!
		$userRoles = ['guest'];
		$userPermissions = [];
		$profilePermissions = [];

		if (!empty($user)) {
			//user roles
			$userRoles = $user->roles->pluck('slug')->all();
			$userPermissions = $user->permissions;
		}

		$rolesPermissions = $this->rolePermissionsRepository->getPermissionsForRoles($userRoles);

		if (!empty($profile)) {
			//profile permissions
			$profileType = $profile->type;
			$loadedProfilePermissions = $this->profilePermissionsRepository->getPermissionsForProfileType($profileType);
			if ($loadedProfilePermissions)
				$profilePermissions = $loadedProfilePermissions;
		}

		//Merge permissions from user roles and user permissions
		$mergedUserPermissions = array_merge($rolesPermissions, $userPermissions, $profilePermissions);

		if (array_intersect($mergedUserPermissions, $requiredPermissions)) {
			$result = true;
		} elseif(!app()->environment('production') || ( ($user || $profile) && array_intersect( $requiredPermissions, (new Parser())->get('hasAccess','wl.security.log_errors',[]) ) )) {
			\Log::info("User:".($user?$user->id:'null')." or Profile:".($profile?$profile->id:'null')." not hasAccess to '".implode(",", $requiredPermissions)."'");
		}

		return $result;
	}

	public function isAuthorized($user, $profile, ObjectIdentity $object)
	{
		$result = true;

		try {
			$this->authorize($user, $profile, $object);
		} catch (AuthorizationException $ex) {
			$result = false;
		}

		return $result;
	}

	public function getRoles() {
		return $this->rolePermissionsRepository->getRoles();
	}
}

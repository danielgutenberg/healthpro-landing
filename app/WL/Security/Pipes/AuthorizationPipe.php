<?php namespace WL\Security\Pipes;

use WL\Modules\Profile\Facades\ProfileService;
use WL\Security\Commands\AuthorizableCommand;
use WL\Security\Facades\AuthorizationUtils;
use WL\Security\Facades\Authorizer;

/**
 * Class AuthorizationPipe
 *
 * Pipe for laravel Command Bus
 * @package WL\Security
 */
class AuthorizationPipe
{
	public function handle($command, $next)
	{
		//bypass not authorizable commands
		if (!($command instanceof AuthorizableCommand))
			return $next($command);

		$user = AuthorizationUtils::loggedInUser();
		$profile = ProfileService::getCurrentProfile();
		Authorizer::authorize($user, $profile, $command);
		return $next($command);
	}
}

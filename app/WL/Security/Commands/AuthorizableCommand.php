<?php namespace WL\Security\Commands;

use App\Commands\Command;
use App\Commands\Exceptions\CommandExecutionException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use WL\Security\Models\ObjectIdentity;

/**
 * Class AuthorizableCommand
 *
 * Use it as base class for your commands that needs to be authorizable
 * @package WL\Security
 */
class AuthorizableCommand extends Command implements ObjectIdentity
{

	use DispatchesJobs;

	/**
	 * Get object identifier. It must be unique.
	 * @return mixed
	 * @throws CommandExecutionException
	 */
	public function getIdentifier()
	{
		if (!defined(static::class . '::IDENTIFIER')) {
			throw new CommandExecutionException("IDENTIFIER constant must be defined in your command. Example: const IDENTIFIER = \"client.inviteClient\";");
		}

		return static::IDENTIFIER;
	}
}

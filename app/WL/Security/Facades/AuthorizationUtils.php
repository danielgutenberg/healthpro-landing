<?php namespace WL\Security\Facades;

use Illuminate\Support\Facades\Facade;

class AuthorizationUtils extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'authorization-utils';
	}
}


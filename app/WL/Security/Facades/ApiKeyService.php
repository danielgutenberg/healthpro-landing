<?php namespace WL\Security\Facades;

use Illuminate\Support\Facades\Facade;
use WL\Security\Services\ApiKeyServiceInterface;

class ApiKeyService extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return ApiKeyServiceInterface::class;
	}
}

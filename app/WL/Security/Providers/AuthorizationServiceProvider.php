<?php namespace WL\Security\Providers;

use Illuminate\Support\ServiceProvider;
use WL\Security\Pipes\AuthorizationPipe;
use WL\Security\Repositories\YamlObjectAuthoritiesRepository;
use WL\Security\Repositories\YamlProfilePermissionsRepository;
use WL\Security\Repositories\YamlRolePermissionsRepository;
use WL\Security\Services\AuthorizationServiceImpl;
use WL\Security\Services\SentinelAuthorizationUtils;
use WL\Yaml\Parser;

class AuthorizationServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('authorizer', function () {
			return new AuthorizationServiceImpl(
				new YamlObjectAuthoritiesRepository(new Parser()),
				new YamlRolePermissionsRepository(new Parser()),
				new YamlProfilePermissionsRepository(new Parser()));
		});

		$this->app->singleton('authorization-utils', function () {
			return new SentinelAuthorizationUtils();
		});

		$this->app->bind('AuthorizationPipe', AuthorizationPipe::class);
	}
}

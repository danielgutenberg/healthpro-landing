<?php namespace WL\Security\Providers;

use Illuminate\Support\ServiceProvider;
use WL\Security\Repositories\DbApiKeyRepository;
use WL\Security\Services\ApiKeyServiceInterface;
use WL\Security\Services\ApiKeyServiceServiceImpl;
use WL\Security\Services\ShaCryptoServiceImpl;

class ApiKeyServiceProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(ApiKeyServiceInterface::class, function () {
			return new ApiKeyServiceServiceImpl(new DbApiKeyRepository(), new ShaCryptoServiceImpl());
		});
	}
}

<?php namespace WL\Security\Middlewares;


use Closure;
use WL\Helpers\Menu;
use WL\Modules\Menu\Facades\MenuService;
use WL\Modules\Menu\Security\SecurityTrait;
use WL\Security\Facades\AuthorizationUtils;
use WL\Security\Services\Exceptions\AuthorizationException;

/**
 * Send user to 401 when the user doesn't have permission and action IS NOT a member privileged
 * Send user to login page when the user doesn't have permission because they aren't logged in,  and the action is allowed to Members
 *
 * We can't use middleware params simple way here for configuration.
 * So we use single middleware.
 * Also we can't use Command pipe or AuthorizationFailureHandler, because
 * we don't know about routes and permissions. The only options exists is middleware.
 *
 * Class UnauthorizedMemberRedirectorMiddleware
 * @package WL\Security\Middlewares
 */
class UnauthorizedMemberRedirectorMiddleware
{
	use SecurityTrait;

	public function handle($request, Closure $next)
	{
		$route = null;
		if ($request->route()) {
			$route = $request->route()->getName();
		}

		$routes = [];
		$data = MenuService::getRawData('frontend-profile-sidebar');
		$profileType = $this->getCurrentProfileType();
		foreach ($data['items'] as $item) {
			if (!isset($item['hide']) && isset($item['link']) && $item['link'] != '#') {
				$routes[$item['link']] = $this->getLinkPermissions($item);
			}
			if (isset($item['submenu'])) {
				foreach ($item['submenu']['items'] as $subItem) {
					if (isset($subItem['link']) && $subItem['link'] != '#') {
						$routes[$subItem['link']] = $this->getLinkPermissions($subItem);
					}
				}
			}
		}

		$response = null;
		try {
			if ( !isset($routes[$route])) {
				$response = $next($request);
			} else {
				if (!empty($routes[$route]) && !in_array($profileType, $routes[$route])) {
					return redirect(route('dashboard-myprofile-personalinfo'));
				}
			}
			$response = $next($request);
		} catch (AuthorizationException $ex) {
			if (AuthorizationUtils::inRoles(['guest', 'member'])) {
				return redirect(route('auth-login'));
			}
			abort(401);
		}

		return $response;
	}

	private function getLinkPermissions($item)
	{
		return $item['profile_types'] ?? [];
	}

}

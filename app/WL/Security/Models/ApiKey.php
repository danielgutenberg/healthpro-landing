<?php namespace WL\Security\Models;

use WL\Modules\User\Models\User;
use WL\Models\ModelBase;
use WL\Modules\Profile\Models\ModelProfile;

class ApiKey extends ModelBase
{
	protected $table = 'api_keys';

	const STATUS_ACTIVE = 'active';
	const STATUS_INACTIVE = 'inactive';
	const STATUS_SUSPENDED = 'suspended';

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function profile()
	{
		return $this->belongsTo(ModelProfile::class, 'profile_id');
	}
}

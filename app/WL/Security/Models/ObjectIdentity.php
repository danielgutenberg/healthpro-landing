<?php namespace WL\Security\Models;

/**
 * Interface ObjectIdentity
 * @package WL\Security
 */
interface ObjectIdentity
{
	/**
	 * Get object identifier. It must be unique.
	 * @return mixed
	 */
	public function getIdentifier();
}

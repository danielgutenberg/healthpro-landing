<?php namespace WL\Security\Composers;

use Illuminate\Contracts\View\View;
use WL\Contracts\Composable;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Security\Facades\AuthorizationUtils;
use WL\Modules\User\Services\UserServiceImpl;

class AuthUtilComposer implements Composable
{

	/**
	 *
	 * Bind data to all view templates
	 *
	 * @param View $view
	 * @return void
	 */
	public function compose(View $view)
	{

		$isAdminStaffRole = AuthorizationUtils::inRoles([UserServiceImpl::ADMIN_ROLE, UserServiceImpl::STAFF_ROLE]);
		$isStaffProfile = AuthorizationUtils::hasProfileType(ModelProfile::STAFF_PROFILE_TYPE);

		$userProfileGlobals = [];
		if (AuthorizationUtils::isLoggedIn()) {
			$profile = ProfileService::getCurrentProfile();
			if ($profile != null) {
				$userProfileGlobals['uid'] = AuthorizationUtils::loggedInUser()->id;
				$userProfileGlobals['urole'] = AuthorizationUtils::loggedInUser()->roles->first()->slug;
				$userProfileGlobals['pid'] = $profile->id;
				$userProfileGlobals['ptype'] = $profile->type;
			}
		}

		$view->with('isAdminStaffRole', $isAdminStaffRole);
		$view->with('isStaffProfile', $isStaffProfile);

		$view->with('_UID', array_get($userProfileGlobals, 'uid'));
		$view->with('_UROLE', array_get($userProfileGlobals, 'urole'));
		$view->with('_PID', array_get($userProfileGlobals, 'pid'));
		$view->with('_PTYPE', array_get($userProfileGlobals, 'ptype'));

	}
}

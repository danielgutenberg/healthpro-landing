<?php namespace WL\Helpers;


use Carbon\Carbon;

class DateHelper
{
	public static function joinDateTime(Carbon $date, Carbon $time, $tz = null)
	{
		if(is_null($tz))
			$tz = $date->getTimezone()->getName();

		return Carbon::create($date->year, $date->month, $date->day, $time->hour, $time->minute, $time->second, $tz);
	}

    public static function roundMinutes(Carbon $date, $minutes = 1, $up = null)
    {
        $seconds = 60 * $minutes;
        if($delta = $date->timestamp % $seconds) {
            if(is_null($up)) {
                $up = abs($seconds - $delta) <= $delta;
            }
            $delta = ($up ? $seconds : 0) - $delta;
            $date->timestamp($date->timestamp + $delta);
        }
	}

    public static function toUtc(Carbon $date, $timezone)
    {
        $carbon = new Carbon($date->toDateTimeString(), $timezone);
        $carbon->timezone('UTC');
        return $carbon;
	}
}

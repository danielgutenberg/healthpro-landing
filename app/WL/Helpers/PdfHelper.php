<?php namespace WL\Helpers;


use Dompdf\Dompdf;
use Illuminate\View\View;

class PdfHelper
{
    public static function renderView(View $view, $orientation=null)
    {
        $domPdf = new Dompdf();
        $html = $view->render();
        $domPdf->getOptions()->set('isHtml5ParserEnabled', true);
        $domPdf->getOptions()->set('isRemoteEnabled', true);
        $domPdf->getOptions()->setChroot(app_path('public'));

        if(!in_array($orientation, ['portrait', 'landscape'])) {
            $orientation = 'portrait';
        }
        $domPdf->setPaper('A4', $orientation);

        $domPdf->loadHtml($html);
        $domPdf->render();

        return $domPdf->output();
    }
}
<?php namespace WL\Helpers;

use WL\Modules\Menu\Facades\MenuService;
use WL\Modules\Menu\Providers\BasicMenuProvider;
use WL\Modules\Payment\Facades\PaymentService;
use WL\Modules\Profile\Services\ModelProfileService;

class Menu
{

	public static function render($menuName, $options = [])
	{
		$menuData = MenuService::getRawData($menuName);

		$html = static::provideMenuFor($menuData, $options);
		if ($html == null) {
			return '';
		}

		return $html;
	}

	private static function provideMenuFor($data, $options = array())
	{
		$menu = null;

		if (isset($data['provider_class'])) {
			//represents MenuProvider as class name
			$class = array_pull($data, 'provider_class');
			$menu = (new $class($data))->provide($options);
		} else {
			// just array of items
			$menu = (new BasicMenuProvider($data))->provide($options);
		}
		return $menu;
	}

	public static function addReceivePaymentButton()
	{
		$profile = app(ModelProfileService::class)->getCurrentProfile();
		$accepts = PaymentService::professionalAccepts($profile->id);

		if (!empty($accepts)) {
			return '';
		}

		return '<a class="header--trigger_btn m-receive_payments" href="'  . route('dashboard-settings-payment') . '">Receive Payments</a>';
	}

}

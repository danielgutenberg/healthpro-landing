<?php namespace WL\Helpers;

use WL\Yaml\Parser;

class MetaParser
{

	/**
	 * 	Parse an file meta by field and return an array with options ...
	 *
	 * @param $file
	 * @param $field
	 * @param string $type
	 * @return array
	 * @throws \Exception
	 */
	public static function parseOptions($file, $field, $type = 'short')
	{
		$results = self::parseFile(trim($file));

		if( !in_array($field, array_keys($results)) )
			return false;

		$return = [];
		foreach ($results[$field] as $field) {
			$return[$field['slug']] = $field[$type];
		}

		return $return;
	}

	/**
	 * @param $file
	 * @param $field
	 * @param $slug
	 * @return bool|mixed
	 * @throws \Exception
	 */
	public static function parseAssoc($file, $field = null, $slug = null)
	{
		$results = self::parseFile(trim($file));

		if (!$field) {
			return $results;
		}

		// invalid field provided
		if(!in_array($field, array_keys($results))) {
			return false;
		}

		// return all results if slug was not provided
		if (!$slug) {
			return $results[$field];
		}

		// return result by slug if exists
		if (isset($results[$field][$slug])) {
			return $results[$field][$slug];
		}

		// invalid slug provided
		return false;
	}

	/**
	 * @param $file
	 * @return mixed
	 * @throws \Exception
	 */
	public static function parseFile($file)
	{
		$path =  DIRECTORY_SEPARATOR . 'config/wl/meta/';

		if(!file_exists(base_path() . $path . trim($file) . '.yaml'))
			throw new \Exception(__('File not found'));

		$parser = new Parser($path);

		return $parser->parse($file);
	}
}

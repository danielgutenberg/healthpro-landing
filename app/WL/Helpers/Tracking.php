<?php namespace WL\Helpers;

use Illuminate\Support\Facades\View;
use WL\Modules\Profile\Models\ModelProfile;
use WL\Modules\User\Services\UserServiceImpl;
use WL\Security\Facades\AuthorizationUtils;

class Tracking {

	/**
	 * @return bool
	 */
	public static function canDisplayTracking()
	{
		$isAdminStaffRole = AuthorizationUtils::inRoles([UserServiceImpl::ADMIN_ROLE, UserServiceImpl::STAFF_ROLE]);
		$isStaffProfile = AuthorizationUtils::hasProfileType(ModelProfile::STAFF_PROFILE_TYPE);

		return env('LIVE') == true && !($isAdminStaffRole || $isStaffProfile);
	}

	/**
	 * @param string|array $views
	 */
	public static function code($views)
	{
		if (!static::canDisplayTracking()) {
			return;
		}

		if (is_string($views)) {
			$views = [$views];
		}

		foreach ($views as $view) {
			echo View::make($view)->render();
		}

		return;
	}
}

<?php namespace WL\Helpers;

class Html {

	public static function selectMultiple($name, $source, $target = [], $intersectItems = []) {
		if(! $name || !$source)
			return false;

		return \View::make('helpers.select-multiple', compact('name', 'source', 'target', 'intersectItems'));
	}

}

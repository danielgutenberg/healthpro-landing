<?php namespace WL\Helpers;


class ArrayHelper
{
	public static function array_attr($array)
	{
		$arr = [];
		foreach ($array as $key => $value) {
			if (!is_string($key)) {
				$arr[$value] = null;
			} else {
				$arr[$key] = $value;
			}
		}

		return $arr;
	}

	public static function intersect_keys($array, $keys)
	{
		return array_intersect_key($array, array_flip($keys));
	}
}

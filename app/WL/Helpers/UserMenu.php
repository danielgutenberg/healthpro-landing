<?php namespace WL\Helpers;

use stdClass;
use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;

class UserMenu
{

	public static function renderUser($type = 'header')
	{
		$currentProfile = ProfileService::loadProfileBasicDetailsModel(ProfileService::getCurrentProfile());
		$currentUser    = ProfileService::getOwner($currentProfile->id);
		$typeName = self::mapToProfileViewModel($currentProfile)->type;//temp fix until we will replace all Provider by Professional profile type in database.
		return view('helpers.layout.user-menu.user', compact('type', 'currentProfile', 'currentUser', 'typeName'));
	}


	public static function renderUserProfiles($type = 'header')
	{
		$userProfiles = [];

		$currentProfile = ProfileService::loadProfileBasicDetailsModel(ProfileService::getCurrentProfile());
		$currentUser    = ProfileService::getOwner($currentProfile->id);
		$currentUserProfiles = ProfileService::loadProfilesBasicDetails($currentUser->id);

		if(!count($currentUserProfiles))
			return '';

		foreach ($currentUserProfiles as $profile) {
			// Filter current profile
			if ($profile->id == ProfileService::getCurrentProfileId())
				continue;

			$userProfiles[] = self::mapToProfileViewModel($profile);
		}

		$hasProviderProfile = ProfileService::hasProfile($currentUser->id, ModelProfile::PROVIDER_PROFILE_TYPE);
		$hasClientProfile = ProfileService::hasProfile($currentUser->id, ModelProfile::CLIENT_PROFILE_TYPE);

		return view('helpers.layout.user-menu.profiles', compact('userProfiles', 'currentUser',  'hasProviderProfile', 'hasClientProfile', 'type'));
	}


	private static function mapToProfileViewModel($profile)
	{
		$profileViewModel = new stdClass();
		$profileViewModel->type = 'None';
		$profileViewModel->id = 'none';
		$profileViewModel->fullName = 'None';
		$profileViewModel->avatar = null;

		if (empty($profile))
			return $profileViewModel;

		$profileViewModel->id = $profile->id;

		//looks evil
		if ($profile->type == ModelProfile::PROVIDER_PROFILE_TYPE) {
			$profile->type = 'professional';
		}

		$profileViewModel->type = ucfirst($profile->type);
		$profileViewModel->fullName = $profile->full_name;
		$profileViewModel->fullName = trim($profileViewModel->fullName);
		$profileViewModel->avatar = $profile->avatar;

		return $profileViewModel;
	}
}

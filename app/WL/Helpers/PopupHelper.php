<?php namespace WL\Helpers;


use WL\Modules\Profile\Facades\ProfileService;
use WL\Modules\Profile\Models\ModelProfile;

class PopupHelper
{
	protected static $popups = [
		'professional_welcome' => ProfessionalWelcomePopup::class,
		'old_returning_professional' => WelcomeOldReturningProfessionalPopup::class,
	];

	public static function show($popup)
	{
		if(!array_key_exists($popup, self::$popups)) {
			return false;
		}

		$popup = new self::$popups[$popup];
		return $popup->show(func_get_args());
	}
}

class ProfessionalWelcomePopup
{
	public function show()
	{
		$profile = ProfileService::getCurrentProfile();
		if(!$profile || $profile->type !== ModelProfile::PROVIDER_PROFILE_TYPE) {
			return false;
		}
		$saw = ProfileService::loadProfileMetaField($profile->id, 'saw_popup_welcome_professional');
		if(!$saw) {
			ProfileService::storeProfileMetaField($profile->id, 'saw_popup_welcome_professional', 1);
		}
		return !$saw;
	}
}

class WelcomeOldReturningProfessionalPopup
{
	public function show()
	{
		$profile = ProfileService::getCurrentProfile();
		if(!$profile || $profile->type !== ModelProfile::PROVIDER_PROFILE_TYPE) {
			return false;
		}
		return ProfileService::loadProfileMetaField($profile->id, 'old_returning_professional');
	}
}

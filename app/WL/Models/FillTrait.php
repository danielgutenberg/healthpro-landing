<?php namespace WL\Models;


trait FillTrait
{
	public function __construct($attributes=[])
	{
		$this->fill($attributes);
	}

	public function fill($attributes)
	{
		foreach ($attributes as $attribute => $value) {
			if(property_exists($this, $attribute)){
				$this->$attribute = $value;
			}
		}
	}
}

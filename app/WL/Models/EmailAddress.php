<?php namespace WL\Models;


class EmailAddress
{
    public $name;
    public $email;

    public function __construct($email, $name)
    {
        $this->email = $email;
        $this->name = $name;
    }
}
<?php
namespace WL\Models;

use WL\Models\ModelBase;

abstract class ModelSocialPage extends ModelBase {

	public $timestamps = false;

	protected $table = 'social_pages';

	protected $fillable = array('elm_id', 'elm_type', 'type', 'url');

	protected $rules = [
		'type'		  	=> 'required',
		'url' 		    => 'required'
	];


	public function elm() {
		return $this->morphTo();
	}

	/**
	 * Static method to get a table name
	 * @return string
	 */
	public static function getTableName() {
		return with(new static)->getTable();
	}

	public static function saveSocialPage($type, $url, $elmId, $elmTable) {
		$page = self::where('elm_id', '=', $elmId)->where('elm_type', '=', $elmTable)->where('type', '=', $type)->first();

		if (! $page) {
			$page = new static(array(
				'elm_type' => $elmTable,
				'elm_id'   => $elmId,
				'type'     => $type,
			));
		}

		$page->url = $url;

		$page->saveOrFail();
	}
}

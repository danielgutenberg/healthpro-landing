<?php
namespace WL\Models;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;
use WL\Contracts\ModelFeeder;
use URL;

class ModelBase extends Model implements ModelFeeder
{
	use ValidatingTrait;

	use ModelTrait;

	const LEVEL          = '—';

	const STATE_ACTIVE   = 1;
	const STATE_INACTIVE = 0;

	protected $injectUniqueIdentifier = true;

	protected $rules = [];

	protected $guarded   = ['id'];

	public  $ignoreEvents = false;

	protected $feeder;

	public function setFeeder( $request )
	{
		$this->feeder = $request;
	}

	public function getFeeder()
	{
		return $this->feeder;
	}

	public function hasFeeder()
	{
		return null !== $this->feeder;
	}

	public function getStates()
	{
		return array(
			self::STATE_ACTIVE   => __('Active'),
			self::STATE_INACTIVE => __('Inactive'),
		);
	}

	/**
	 * Get the name of the current state
	 * @return [type] [description]
	 */
	public function state()
	{
		$states = $this->getStates();
		return isset($this->state) ? $states[$this->state] : null;
	}

	/**
	 * @return bool|null
	 */
	public function isActive()
	{
		return isset($this->state) ? (int) $this->state === self::STATE_ACTIVE : null;
	}

	/**
	 * Add URL filter for all links. Adds http:// to the start if not exists.
	 *
	 * @param string $value
	 * @param boolean $secure
	 * @return string
	 */
	public function filterUrl($value, $secure = false)
	{
		if (!$value) {
			return $value;
		}

		if (!preg_match('~^https?\:\/\/~', $value)) {
			$value = 'http' . ($secure ? 's' : '') . '://' . $value;
		}
		return $value;
	}

	/**
	 * Get the URL to the post.
	 *
	 * @return string|null
	 */
	public function url()
	{
		return isset($this->slug) ? URL::to($this->slug) : null;
	}

	public static function generateUniqueId()
	{
		return base_convert(md5(time() + rand()), 10, 32);
	}

	/**
	 * Static method to get a table name
	 * @return string
	 */
	public static function getTableName()
	{
		return self::getStatic()->getTable();
	}

	/**
	 * Return new static object
	 *
	 * @return static
	 */
	public static function getStatic()
	{
		return new static;
	}

	public function translatable()
	{
		return false;
	}

	public function saveOrFail(array $options = [])
	{
		return $this->save();
	}
}

<?php
namespace WL\Models;

trait TreeTrait
{
	/**
	 * The field to add the level to on queries
	 *
	 * @var string
	 */
	public $levelField = 'title';

	/**
	 * Find all the child nodes for the page
	 *
	 * @param null $parent_id
	 * @return mixed
	 */
	public static function getChildren($parent_id = null)
	{
		$self = self::getStatic();

		$query = $self->getChildrenQuery( $parent_id );

		return $query->get();
	}

	public function getChildrenQuery( $parent_id = null )
	{
		if (null === $parent_id) {
			$parent_id = 0;
		}

		$table = self::getTableName();

		$builder = $this
			->select( $table . '.*' )
			->where('parent_id', $parent_id)
			->orderBy('rank', 'asc');

		// join translation table
		if ( $this->translatable() ) {

			$joinTable = $this->translation()->getModel()->getTableName();

			// get the sources only
			$builder->join($joinTable, $table . '.id', '=', $joinTable . '.elm_id')
				->whereRaw($table . '.id = ' . $joinTable . '.source_id')
				->where($joinTable . '.elm_type',  '=', $this->morphClass);

			if ( null === $this->getModelLanguage() ) {

			} else {
				$builder->where( $joinTable . '.lang', '=', $this->getModelLanguage() );
			}
		}

		return $builder;
	}

	/**
	 * Get a page parent
	 * @return mixed
	 */
	public function getParent()
	{
		return self::getStatic()->findOrFail($this->parent_id);
	}

	/**
	 * Check if the loaded page has a parent
	 * @return bool
	 */
	public function hasParent()
	{
		return $this->parent_id ? true : false;
	}

	/**
	 * Build a simple tree
	 *
	 * @param int $parent
	 * @return mixed
	 */
	public static function getTree($parent = 0)
	{
		$entries = self::getStatic()->getChildren($parent);

		foreach ($entries as &$entry) {
			$entry->items = self::getStatic()->getTree($entry->id);
		}

		return $entries;
	}

	/**
	 * Get a tree for a simple list (for example: admin panel)
	 *
	 * @param int $parent
	 * @param array $list
	 * @param string $level
	 * @return array
	 */
	public static function getOneLevelTree($parent = 0, $list = array(), $level = '')
	{
		$self = self::getStatic();

		$entries = $self->getChildren($parent);

		foreach ($entries as $entry) {

			// update the level before displaying the field
			if ( $level ) {
				$entry->setAttribute( $self->levelField, $level . ' ' . $entry->{$self->levelField} );
			}

			$list[] = $entry;
			$currentLevel = $level;
			$level .= self::LEVEL;
			$list = $self->getOneLevelTree($entry->id, $list, $level);
			$level = $currentLevel;
		}

		return $list;
	}

	/**
	 * Get a tree for a dropdown
	 *
	 * @param bool $exclude
	 * @param int $parent
	 * @param array $list
	 * @param string $level
	 * @return array
	 */
	public static function getListTree($exclude = false, $parent = 0, $list = array(0 => 'Top Level'), $level = '')
	{
		$entries = self::getStatic()->getChildren($parent);

		foreach ($entries as $node) {
			if ($exclude != $node->id) {
				$list[$node->id] = $level . ' ' .  $node->title;
				$currentLevel = $level;
				$level .= self::LEVEL;
				$list = self::getStatic()->getListTree($exclude, $node->id, $list, $level);
				$level = $currentLevel;
			}
		}
		return $list;
	}

	public function getAncestors($list = [])
	{
		if (!$this->hasParent()) {
			return $list;
		}

		$list[] = $this->getParent();

		return $list[count($list) - 1]->getAncestors( $list );
	}

	/**
	 * When deleting the page we need to find another parent for the children.
	 * @return bool
	 */
	public function reassignChildren()
	{
		self::getStatic()->where('parent_id', $this->id)->update(array(
			'parent_id' => $this->hasParent() ? $this->_parent_id : 0,
		));

		return true;
	}
}

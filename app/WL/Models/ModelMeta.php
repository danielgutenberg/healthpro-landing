<?php
namespace WL\Models;

use Eloquent;

abstract class ModelMeta extends Eloquent
{
	public $timestamps = false;

	protected $fillable = array('elm_id', 'elm_type', 'meta_key', 'meta_value');

	protected $mcasts = [];

	public function elm()
	{
		return $this->morphTo();
	}

	/**
	 * Static method to get a table name
	 * @return string
	 */
	public static function getTableName()
	{
		return with(new static)->getTable();
	}

	/**
	 *	return meta value
	 */
	public function getValue()
	{
		return $this->meta_value;
	}

	/**
	 * @param $key
	 * @param $value
	 * @param $elmId
	 * @param $elmTable
	 * @param bool $single
	 */
	public static function saveMeta($key, $value, $elmId, $elmTable, $single = true)
	{
		$meta = self::where('elm_id', '=', $elmId)
			->where('elm_type', '=', $elmTable)
			->where('meta_key', '=', $key)
			->first();

		// Clean empty meta from DB
		if (is_null($value)) {
			if ($meta) $meta->delete();
			return true;
		}

		if (!$meta) {
			$meta = new static( array(
				'elm_type'  => $elmTable,
				'elm_id'    => $elmId,
				'meta_key'  => $key,
			) );
		}

		$meta->meta_value = $value;

		return $meta->save();
	}

	public static function clearMeta($key, $elmId, $elmTable)
	{
		$meta = self::where('elm_id', '=', $elmId)
			->where('elm_type', '=', $elmTable)
			->where('meta_key', '=', $key)
			->first();

		if( $meta ) {
			return $meta->delete();
		}
	}
}

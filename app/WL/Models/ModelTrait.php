<?php
namespace WL\Models;

use WL\Helpers\StringHelper;

trait ModelTrait
{

	/**
	 * @param integer $id
	 * @return self static
	 */
	public static function findOrCreate($id)
	{
		$obj = static::find($id);
		return $obj ?: new static;
	}

	/**
	 * Returns the date of the resource creation,
	 * on a good and more readable format :)
	 *
	 * @return string|null
	 */
	public function createdAt()
	{
		return isset($this->created_at) ? $this->date($this->created_at) : null;
	}

	/**
	 * Returns the date of the resource last update,
	 * on a good and more readable format :)
	 *
	 * @return string|null
	 */
	public function updatedAt()
	{
		return isset($this->updated_at) ?  $this->date($this->updated_at) : null;
	}

	public function syncManyToMany($values, $relation) {
		if(! $relation)
			return false;

		if(! $values) {
			$this->$relation()->detach();
		} else {
			return $this->$relation()->sync($values);
		}
	}


	/**
	 * Get the date the resource was created.
	 *
	 * @param \Carbon|null $date
	 * @return string
	 */
	public function date($date=null)
	{
		if(is_null($date)) {
			$date = $this->created_at;
		}

		return StringHelper::date($date);
	}

	/**
	 * Static method to get a table name
	 * @return string
	 */
	public static function getTableName()
	{
		return with(new static)->getTable();
	}

}

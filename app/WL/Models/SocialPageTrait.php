<?php namespace WL\Models;

use WL\Yaml\Parser;
use Watson\Validating\ValidationException;

trait SocialPageTrait {

	protected $configSocialPages = [];

	public function socialPages() {
		if (!isset($this->socialPageClassName))
			throw new \Exception('ModelSocialPage require $metaClassName field');

		if (!is_subclass_of($this->socialPageClassName, \WL\Models\ModelSocialPage::class))
			throw new \Exception('$model->metaClassName field must be instance of \WL\Models\ModelSocialPage');

		return $this->morphMany($this->socialPageClassName, 'elm');
	}


	/**
	 *    Add an social page ...
	 *
	 * @param $type
	 * @param $url
	 * @return int
	 */
	public function saveSocialPage($type, $url) {
		if( !in_array( $type, $this->getConfigPages() ) )
			throw new ValidationException(__('Invalid address type'));

		$re = "/(((ftp|http|https):\\/\\/)|(\\/)|(..\\/))(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-\\/]))?/";

		if(! preg_match($re, $url) )
			throw new ValidationException(__('Invalid social url'));

		$socialClass = $this->socialPageClassName;

		return $socialClass::saveSocialPage($type, $url, $this->id, $this->getMorphClass());
	}

	/**
	 * 	Add social pages ...
	 *
	 * @param array $pages
	 * @return $this
	 */
	public function saveSocialPages( array $pages ) {
		foreach ($pages as $type => $url) {
			if( $url ) {
				$this->saveSocialPage($type, $url);
			} else {
				$this->deleteSocialPage($type);
			}
		}

		return $this;
	}



	/**
	 * Delete social page ...
	 *
	 * @param $type
	 * @return mixed
	 */
	public function deleteSocialPage($type) {
		if( $UrlObj = $this->getSocialPageByType($type) )
			return $UrlObj->delete();
	}

	/**
	 * 	Multiple social pages ..
	 *
	 * @param array $types
	 * @return $this
	 */
	public function deleteSocialPages( array $types ) {
		foreach ($types as $type) {
			$this->deleteSocialPage($type);
		}

		return $this;
	}



	/**
	 * 	Get social page by type ..
	 *
	 * @param $type
	 * @return mixed
	 * @throws \Exception
	 */
	public function getSocialPageByType($type) {
		return $this->socialPages()->whereType($type)->first();
	}

	/**
	 * 	Get social pages from config file ..
	 *
	 * @return array
	 */
	public function getConfigPages() {
		if( ! $this->configSocialPages ) {
			$parser = new Parser();
			$file   = $parser->parse('/wl/social_pages/social_pages.yaml');
			$pages  = array_map(function($page) {
				return key($page);
			}, $file);

			if( count($pages) ) {
				$this->configSocialPages = $pages;
			} else {
				$this->configSocialPages = [];
			}
		}

		return $this->configSocialPages;
	}

	/**
	 * 	Get an array of registered social pages to fill former populator ...
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function getSocialPagesPopulator() {
		$pages = [];
		$registeredUrls = $this->socialPages()->pluck('url', 'type')->all();

		foreach ($this->getConfigPages() as $page) {
			$pages[$page] = isset($registeredUrls[$page]) ? $registeredUrls[$page] : null;
		}

		return $pages;
	}

}

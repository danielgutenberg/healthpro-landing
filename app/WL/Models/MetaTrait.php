<?php namespace WL\Models;

use Illuminate\Support\Debug\Dumper;

trait MetaTrait {

	// Allow Model to use meta even if model is not created yet, and will be saved on event created
	private $unsavedMeta = [];

	public function meta() {
		if (!isset($this->metaClassName))
			throw new \Exception('MetaTrait require $metaClassName field');

		if (!is_subclass_of($this->metaClassName, \WL\Models\ModelMeta::class))
			throw new \Exception('$model->metaClassName field must be instance of \WL\Models\ModelMeta');

		return $this->morphMany($this->metaClassName, 'elm');
	}

	/**
	 * 	Save one meta ...
	 *
	 * @param $metaKey
	 * @param $metaValue
	 * @return mixed
	 */
	public function saveMeta($metaKey, $metaValue) {
		static $subscribedToCreated = false;

		if (!$this->exists) {
			$this->unsavedMeta[$metaKey] = $metaValue;

			if (!$subscribedToCreated) {
				$metas = &$this->unsavedMeta;
				$this->created(function() use (&$metas) {
					$metaClass = $this->metaClassName;
					foreach ($metas as $metaKey=>$metaValue) {
						$metaClass::saveMeta($metaKey, $metaValue, $this->id, $this->getMorphClass());
					}
					$metas = [];
				}, 1);
				$subscribedToCreated = true;
			}
			return true;
		}
		$metaClass = $this->metaClassName;

		return $metaClass::saveMeta($metaKey, $metaValue, $this->id, $this->getMorphClass());
	}

	/**
	 * Save many meta ..
	 *
	 * @param array $metaArgs
	 * @return $this
	 */
	public function saveMany(array $metaArgs) {
		foreach($metaArgs as $metaKey => $metaValue) {
			$this->saveMeta($metaKey, $metaValue);
		}

		return $this;
	}

	/**
	 * 	Sync meta by meta key ...
	 *
	 * @param array $meta
	 * @return MetaTrait
	 */
	public function syncMeta(array $meta) {
		$currentMeta = $this->getAllMeta();

		$diffMeta = array_diff(array_keys($currentMeta), array_keys($meta));

		array_map(function($meta) {
			$this->deleteMeta($meta);
		}, $diffMeta);

		return $this->saveMany($meta);
	}

	public function deleteMeta($name) {
		if (!$this->exists && array_key_exists($name, $this->unsavedMeta)) {
			unset($this->unsavedMeta[$name]);
			return true;
		}
		return $this->meta()->where('meta_key', '=', $name)->delete();
	}

	public function updateMeta( array $meta ){
		$success = true;
		foreach ($meta as $name => $value)  {
			if (! $this->saveMeta($name, $value)) {
				$success = false;
			}
		}
		return $success;
	}

	public function getMeta($name, $default = '') {
		if (!$this->exists && array_key_exists($name, $this->unsavedMeta)) {
			return $this->unsavedMeta[$name];
		}

		$meta = $this->meta()->where('meta_key', '=', $name)->first();

		return ($meta === null) ? $default : $meta->meta_value;
	}

	public function getAllMeta(array $fieldNames = []) {
		if (!$this->exists) {
			return $this->unsavedMeta;
		}

		if(! $fieldNames) {
			$return = [];
			$meta = $this->meta;

			foreach($meta as $m) {
				$return[$m->meta_key] = $m->meta_value;
			}

			return $return;
		}

		$values = [];
		foreach ($fieldNames as $field) {
			if( $meta = $this->getMeta($field) ) {
				$values[$field] = $meta;
			}

		}

		return $values;
	}

	public function getMetaObject(array $fieldNames = [])
	{
		if (!isset($this->metaClassName))
			throw new \Exception('MetaTrait require $metaClassName field');

		if (!is_subclass_of($this->metaClassName, \WL\Models\ModelMeta::class))
			throw new \Exception('$model->metaClassName field must be instance of \WL\Models\ModelMeta');

		$metas = $this->getAllMeta($fieldNames);

		array_walk($metas, function(&$value, $key) {
			$value = $this->metaCast($key, $value);
		});

		return (object) $metas;
	}

	public function metaCast($key, $value)
	{
		$model = new $this->metaClassName;
		return $model->cast($key, $value);
	}

	public function deleteAllMeta() {
		foreach ($this->getAllMeta() as $metakey => $metaValue) {
			$this->deleteMeta($metakey);
		}
	}
}

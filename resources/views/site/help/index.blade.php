@extends('site.layouts.default')
@section('title')Frequently Asked Questions - FAQ - Support | HealthPRO @stop
@section('meta-description')Our list of frequently asked questions (FAQs) will help you learn more about HealthPRO. Read our FAQs to get support and answers to common questions people have about HealthPRO.@stop
@section('meta-keywords'){{'FAQ,FAQ’s,FAQs,frequently,asked,questions,common,commonly,support,answers,HealthPRO.com,HealthPRO,pro,health,learn,more'}}@stop
@section('content')
    <div class="layout m-front">

        <div class="layout--body">

            <div class="page">
                <header class="page_header m-help">
                    <div class="page_header--title">We are here to <b>help you</b></div>

                    <p class="page_header--text">Everything you need to know about using HealthPRO for your business</p>
                </header>
                <nav class="page_nav">
                    <ul>
                        @foreach ($sections as $section)
                            <li @if ($section->slug == $sectionSlug) class="m-current" @endif>
                                <a href="{{ URL::route('help', ['sectionSlug' => $section->slug]) }}">
                                    {{$section->name}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </nav>
                <div class="help wrap">
                    <nav class="help--nav sidebar_nav m-bordered">
                      <span href="#" class="sidebar_nav--item m-{{$categorySlug}} m-current help--nav_toggler">
                        <a href="#"><span>{{$categoryName}}</span></a>
                      </span>
                      <ul>
                          @if ($sectionSlug)
                              @foreach ($categories as $category)
                                  <li class="sidebar_nav--item m-{{$category->slug}} @if ($category->slug == $categorySlug) m-current @endif">
                                      <a href="{{ URL::route('help', ['sectionSlug' => $sectionSlug, 'categorySlug' => $category->slug]) }}"><span>{{$category->name}}</span></a>
                                  </li>
                              @endforeach
                          @endif
                      </ul>
                    </nav>
                    <div class="help--body text m-medium">
                        <div class="help-category">{{ $categoryName }}</div>
                        @foreach ($posts as $post)
                            <h4 class="content-subject">{{$post->subject}}</h4>
                            <p>{!! nl2br($post->content) !!}</p>
                            <div class="content-spacer"></div>
                        @endforeach
                        <div class="help--ask">
                          <div class="help--ask--title">Didn't find what you are looking for - please send us your question and we will answer within 48 hours.</div>
                          <div class="help--form" data-cs-form="submit_question"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@extends('site.layouts.default')

@section('wrapper-class') m-search_results @endsection

@section('title')
	{{Request::get('service')}} Services | in {{Request::get('location')}} – Review & Book Online via HealthPRO
@stop

@section('meta-description')Browse all available professionals offering {{Request::get('service')}} services in and around {{Request::get('location')}} | HealthPRO helps you easily find and book with independent professionals in your area. @stop
@section('meta-keywords'){{Request::get('service')}}, {{Request::get('location')}}, healthPRO, find, review, reviews, find, best, top, close, area, clinic, health, professionals, clinics, classes, class @stop
@section('content')
<div class="search" data-search>
	<aside class="search--filters" data-search-node="filters">
		<form action="#" class="search_filters js-search_filters m-opened" data-search-filters>
			<nav class="search_filters--nav" data-search-filters-node="nav">
				<div class="search_filters--nav--label">Search by</div>
				<div class="search_filters--nav--tabs">
					{{-- <label class="search_filters--nav--item m-active" data-type="single">
						Search for a Professional
						<input type="radio" name="type" value="single">
					</label>
					<label class="search_filters--nav--item" data-type="class">
						Search Classes
						<input type="radio" name="type" value="class">
					</label> --}}

					<label class="search_filters--nav--item" data-search-by="service">
						Session
						<input type="radio" name="search_by_radio" value="service">
					</label>
					<label class="search_filters--nav--item" data-search-by="name">
						Name
						<input type="radio" name="search_by_radio" value="name">
					</label>
				</div>
			</nav>
			<div class="search_filters--content" data-search-filters-node="content">
				<div class="search_filters--form form">
					<div class="search_filters--form--inner" data-search-filters-node="fieldset_container">
						<div class="search_filters--form--fieldset">
							<div class="search_filters--form--fieldset--inner m-service" data-search-filters-node="fieldset">
								<div class="search_filters--form--fieldset--row">
									<dl class="form--group search_filters--location">
										<dt><label for="location">Where are you?</label></dt>
										<dd>
											<span class="field m-wide">
												<input type="text" placeholder="eg. New York, Los Angeles, etc." name="location" data-selector="q[location]" id="location">
												<span class="field--error"></span>
												<span class="field--autocomplete"></span>
											</span>
										</dd>
									</dl>
									<dl class="form--group m-hide search_filters--clients_address">
										<dt><label for="clients_address">Your address</label></dt>
										<dd>
											<span class="field m-wide">
												<input type="text" name="clients_address" id="clients_address" data-selector="q[clients_address]" >
												<span class="field--error"></span>
											</span>
										</dd>
									</dl>
									<dl class="form--group m-query">
										<dt><label for="service">What session are you looking for?</label></dt>
										<dd>
											<span class="form--group--multifield" data-query-container>
												<span class="field m-wide" data-query-type="service">
													<input type="text" placeholder="" name="service" id="service" data-selector="q[service]">
													<span class="field--error"></span>
													<span class="field--autocomplete"></span>
												</span>
											</span>
										</dd>
									</dl>
									<dl class="form--group search_filters--date">
										<dt><label for="date">Date</label></dt>
										<dd class="search_filters--date--row">
											<div class="search_filters--date--item">
												<span class="search_filters--date--item--label">From</span>
												<label class="field m-wide m-date">
													<input type="text" name="date_from" id="date_from" data-selector="date[from]" readonly="true">
													<span class="field--error"></span>
												</label>
											</div>
											<div class="search_filters--date--item">
												<span class="search_filters--date--item--label">To</span>
												<label class="field m-wide m-date">
													<input type="text" name="date_until" id="date_until" data-selector="date[until]" readonly="true">
													<span class="field--error"></span>
												</label>
											</div>
										</dd>
									</dl>
									<dl class="form--group search_filters--location_type">
										<dt><label>Location Type</label></dt>
										<dd>
											{{-- <span class="select m-wide">
												<select data-select id="location_type_id" name="location_type_id">
													<option value="0">All</option>
													@foreach($locationTypes as $locationType)
														<option value="{{$locationType->id}}">{{$locationType->name}}</option>
													@endforeach
												</select>
											</span> --}}
											@foreach($locationTypes as $locationType)
												<div class="search_filters--location_type--item">
													<label class="checkbox">
														<input type="checkbox" name="location_type_id" value="{{$locationType->id}}">
														<span class="checkbox--i"></span>
														@php
															$className = snake_case(strtok($locationType->name, ' '))
														@endphp
														<span class="checkbox--label search_filters--location_type--label m-{{$className}}">{{$locationType->name}}</span>
													</label>
												</div>
											@endforeach
										</dd>
									</dl>
									{{-- <dl class="form--group m-time">
										<dt><label for="time">Time</label></dt>
										<dd>
											<span class="select m-wide">
												<select data-select id="time" name="time"></select>
											</span>
										</dd>
									</dl> --}}
								</div>
								<button class="search_filters--form--opener search_filters--form--advanced_opener btn m-more m-wide" type="button" data-search-filters-node="advanced_btn">
									<span>
										Advanced Filters
										<b></b>
									</span>
								</button>
								<div class="search_filters--form_advanced">
									<dl class="form--group m-radius">
										<dt><label for="radius">Locations within (miles)</label></dt>
										<dd>
											<span class="field m-wide">
												<input type="number" name="radius" min="0" max="100" step="5" id="radius">
											</span>
										</dd>
									</dl>
									<div class="search_filters--form_advanced_list">
										<div class="form--group">
											<label class="checkbox">
												<input type="checkbox" name="online_booking_only">
												<span class="checkbox--i"></span>
												<span class="checkbox--label">{{__('Online Booking Only')}}</span>
											</label>
										</div>
										<div class="form--group search_filters--gift">
											<label class="checkbox">
												<input type="checkbox" name="gift_cert_available">
												<span class="checkbox--i"></span>
												<span class="checkbox--label search_filters--gift--label"> {{__('Gift Certificates Available')}}</span>
											</label>
										</div>
									</div>
									{{--<dl class="form--group m-conditions">--}}
										{{--<dt><label for="conditions">Conditions and Concerns</label></dt>--}}
										{{--<dd>--}}
											{{--<span class="field m-wide">--}}
												{{--<input type="text" id="conditions" placeholder="eg. Back Pain, Stress, Coaching" name="[tag][conditions][optional]">--}}
												{{--<span class="field--autocomplete"></span>--}}
											{{--</span>--}}
										{{--</dd>--}}
									{{--</dl>--}}
								</div>
							</div>

							<div class="search_filters--form--fieldset--inner m-name">
								<div class="search_filters--form--fieldset--row">
									<dl class="form--group m-query">
										<dt><label for="name">Professional Name</label></dt>
										<dd>
											<span class="form--group--multifield" data-query-container>
												<span class="field m-wide m-show" data-query-type="name">
													<input type="text" placeholder="e.g. Steve Martin, Angela Smith" name="name" id="name" data-selector="q[name]">
													<span class="field--error"></span>
													<span class="field--autocomplete"></span>
												</span>
											</span>
										</dd>
									</dl>
								</div>
							</div>
						</div>
						<div class="search_filters--form--submit" data-search-filters-node="submit">
							<button class="btn m-blue m-wide m-large" type="submit">Update Results</button>
						</div>
					</div>
					<div class="search_filters--form--buttons" data-search-filters-node="buttons">
						<button class="search_filters--form--opener search_filters--form--filters_opener btn m-more m-wide" type="button" data-search-filters-node="filters_opener">
							<span>
								Filters
								<b></b>
							</span>
						</button>
					</div>
				</div>
			</div>

		</form>
	</aside>
	<div class="search--main" data-search-node="main">
		<div class="search--results"data-search-node="results">
			<div class="search_results" data-search-results>

				{{-- <header class="search_results--header"></header> --}}
				<div class="search_results--toolbar"></div>

				<div class="search_results--content">
					<div class="search_results--empty m-hide">
						<p>We couldn't find anyone for you. =(</p>
						<p>Maybe try searching for a different day? How about searching within the following 3 days?</p>
						<p><button class="btn m-blue m-small search_results--empty--show_next">Show me</button></p>

						<div class="search_results--know_professional" id="search_know_professional">
							<div class="search_results--know_professional_wrap">
								<button type="button" class="btn m-green search_results--know_professional_toggler m-open">Know a professional?</button>
								<button type="button" class="btn m-link search_results--know_professional_toggler m-close">Cancel</button>
								<div id="search_know_professional_form" class="search_results--know_professional_form"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="search_results--loading loading_overlay m-light m-hide"><div class="spin m-logo"></div></div>

				<div class="search_results--loading_more m-hide">
					<div class="heartbeat m-medium"></div>
					Loading new results
				</div>

			</div>
		</div>
		<div class="search--map" data-search-node="map">
			<div class="search_map" data-search-map></div>
			<button class="search_map--opener search--map--opener" type="button" data-search-map-opener>Open map</button>
		</div>
	</div>

</div>
@stop

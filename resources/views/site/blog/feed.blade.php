{!! '<'.'?'.'xml version="1.0" encoding="UTF-8" ?>'."\n" !!}
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/" xmlns:media="http://search.yahoo.com/mrss/">
	<channel>
		<title><![CDATA[{{ MetaService::pageTitle()  }}]]></title>
		<link>{{ URL::to('/') }}</link>
		<description>{{ MetaService::pageDescription() }}</description>
		<atom:link href="{{ Request::url() }}" rel="self"></atom:link>
		<language>en-US</language>
		<lastBuildDate>{{ $posts->first()->publish_at->format('r') }}</lastBuildDate>

		@foreach ($posts as $post)

			<item>
				<title>{{ $post->title }}</title>
				<link>{{ $post->link }}</link>
				<comments>{{ $post->link }}#comments</comments>
				<guid isPermaLink="true">{{ $post->link }}</guid>
				<description><![CDATA[{!! $post->excerptPlain(null, '...') !!}]]></description>
				@if (!empty($post->content))
					<content:encoded><![CDATA[{!! $post->content !!}]]></content:encoded>
				@endif

				@if ($post->category)

					<category><![CDATA[{!! $post->category->name !!}]]></category>
				@endif

				@if(!empty($post->tags))

					@foreach ($post->tags as $tag)
						<category><![CDATA[{!! $tag->name !!}]]></category>
					@endforeach
				@endif
				<dc:creator xmlns:dc="http://purl.org/dc/elements/1.1/">{{ $post->author->full_name }}</dc:creator>
				<pubDate>{{ $post->publish_at->format('r') }}</pubDate>
				<slash:comments>{{ count($post->comments)}}</slash:comments>
			</item>

		@endforeach
	</channel>
</rss>

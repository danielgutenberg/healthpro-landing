@extends('site.layouts.default')

@if (!empty($chosenMonth) && !empty($chosenYear))
	@section('title')HealthPRO Blog Articles from {{$chosenMonth}} {{$chosenYear}} @stop
	@section('meta-description')Read all the articles posted on the HealthPRO blog in {{$chosenMonth}} {{$chosenYear}}. View the posts from {{$chosenMonth}} {{$chosenYear}} and stay up to date on all our health, wellness and fitness blog posts.@stop
@elseif (!empty($tagName))
	@section('title')HealthPRO Blog Posts about {{$tagName}} @stop
	@section('meta-description')Read and explore all of our latest articles relating to {{$tagName}}. Blog posts that have to do with {{$tagName}} are featured on the HealthPRO blog for your reading pleasure.@stop
@else
	@section('meta-description'){{MetaService::pageDescription()}}@stop
	@section('title'){{MetaService::pageTitle()}}@stop
@endif
@section('meta-tags-additional')
	<link rel="alternate" type="application/rss+xml" title="HealthPRO Blog » Feed" href="{{route('blog-feed')}}">
@stop

@section('content')

	<div class="layout--body">
		<div class="page">
            @include('site.blog.post.intro',['selected'=>['blog'=>'m-current','about'=>'','careers'=>'','press'=>'']])
			@if($editor)
				@include('site.blog.post.create')
			@endif
			<?php
			$classInner = 'blog--article--inner-multicolumn';
            $classHeaderDimension = 'blog--article--header-multicolumn';
			if(empty($posts)||count($posts)<2){
                $classInner = 'blog--article--inner-1column';
                $classHeaderDimension = 'blog--article--header-1column';
			}
			?>
 			<div class="blog--wrapper">
				<div class="blog m-row">
                    @if (!empty($tagName))
                    	<div class="blog--main_title">{{$tagName}}</div>
                    @endif
						<div class="blog--articles">
				@if(count($posts)>0)
                    <?php $i = 0; ?>
					@foreach ($posts as $post)
						<?php
							$i++;
                            $colNumber = 1;
                            if(0 == $i % 2){
								$colNumber = 2;
							}?>
						<article class="blog--article m-col{{$colNumber}}">
							@if($post->state!='published')
								<div class="blog-post-draft">DRAFT</div>
							@endif
							<div class="blog--article--inner {{$classInner}}">
								<a href="{{$post->link}}" class="blog--article--header_link">
									<div class="blog--article--header {{$classHeaderDimension}}"
											style="background-image: url({{$post->cover_image_url}});">
										<div title="{{$post->title}}" class="blog--article--title">
											{{ $post->trim_text($post->title, 60) }}
										</div>
										{{--<ul class="blog--article--meta">--}}
											{{--<li class="m-comments">{{count($post->comments)}}</li>--}}
										{{--</ul>--}}
									</div>
								</a>
								<div class="blog--article--content">
									{!! $post->trim_text($post->content, 220, true, false) !!}
								</div>

								<a href="{{$post->link}}" class="btn m-green">Continue Reading</a>
							</div>
						</article>
					@endforeach
						</div>
				@else
					<div class="blog-no-posts">No posts found ):</div>
				@endif

				<?php
				if ($controllerMethod == 'blog-index-page') {
					$sendData = [
						'next' => [($page - 1)],
						'prev' => [($page + 1)]
					];
				} else if ($controllerMethod == 'blog-index-archive') {
					$sendData = [
						'next' => [
							'month' => Request::segment(6),
							'year' => Request::segment(4),
							'page_num' => ($page - 1)
						],
						'prev' => [
							'month' => Request::segment(6),
							'year' => Request::segment(4),
							'page_num' => ($page + 1)
						]
					];
				} else {
					$sendData = [
						'next' => [$tagSlug, ($page - 1)],
						'prev' => [$tagSlug, ($page + 1)]
					];
				}

				if (isset($term)) {
					$sendData['next']['term'] = $term;
					$sendData['prev']['term'] = $term;
				}
				$renderPrev =  ($totalPosts - ($page+1)*$perPage) > $perPage;
				$renderNext = $page > 0;
				?>

				@if($renderPrev || $renderNext)
					<nav class="blog--nav">

						@if($renderPrev)
							<a href="{{ URL::route($controllerMethod,$sendData['prev']) }}"
							   class="blog--nav_item m-prev btn m-light_gray">&larr; Older Posts</a>
						@endif

						@if($renderNext)
							<a href="{{ URL::route($controllerMethod,$sendData['next'])}}"
							   class="blog--nav_item m-next btn m-light_gray">Newer Posts &rarr;</a>
						@endif
					</nav>
				@endif

			</div>
				@include('site.blog.post.sidebar')
			</div>
		</div>
	</div>
@stop

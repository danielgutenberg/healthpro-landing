<div class="blog-sidebar">
	@if(!empty($profile_in_sidebar) && !empty($post) && !empty($post->author) && !empty($post->author->public_url))
		<div class="article--author" data-sticky-block>
			<button class="article--author--opener" type="button" data-sticky-opener>About the author</button>
			<figure class="article--author--userpic userpic m-circle m-medium">
				<span class="userpic--inner">@if($post->author->avatar)<img src="{{$post->author->avatar}}">@endif</span>
			</figure>
			@if(!empty($post->author))
			<div class="article--author--info">
				@if(!empty($post->author->full_name))
				<div class="article--author--name">
					{{$post->author->full_name}}
				</div>
				@endif
                @if(!empty($post->author->services))
				<ul class="article--author--services">
					@foreach (array_slice($post->author->services, 0, 4) as $service)
						<li>{{$service}}</li>
					@endforeach
				</ul>
                @endif
			</div>
            @endif
			<a href="{{$post->author->public_url}}" class="article--author--btn btn m-green">Visit My Profile</a>
		</div>
	@endif

	<div class="blog-sidebar--block m-categories">
		<div class="blog-sidebar--block--header">
			<div class="blog-sidebar--block--header--title">Categories</div>
		</div>
		<div class="blog-sidebar--block--content">
			{!! MenuHelper::render('frontend-blog-categories') !!}
		</div>
	</div>


	@if (!empty($tags))

		<div class="blog-sidebar--block m-tags">
			<div class="blog-sidebar--block--header">
				<div class="blog-sidebar--block--header--title">Topic Tags</div>
			</div>
			<div class="blog-sidebar--block--content">
				<ul>
					@foreach($tags as $tag)
						<li class="blog-sidebar--block--tag {{!empty($tagSlug) && !empty($tag['attributes']['slug']) && $tag['attributes']['slug'] == $tagSlug ? ' m-active' : ''}}">
							<a href="/providers/blog/tag/{{$tag['attributes']['slug']}}">{{$tag['attributes']['name']}}</a>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	@endif

	@if(!empty($archive))
		<div class="blog-sidebar--block m-archive">
			<div class="blog-sidebar--block--header">
				<div class="blog-sidebar--block--header--title">Archive</div>
			</div>
			<div class="blog-sidebar--block--content">
				<ul>
					@foreach ($archive as $archiveItemName => $blogs)
						<li class="blog-sidebar--block--item {{!empty($archiveName) && $archiveName == $archiveItemName ? 'm-active' : ''}}">
							<a href="{{route('blog-index-archive', ['year' => explode(' ', $archiveItemName)[1], 'month' =>explode(' ', $archiveItemName)[0]])}}">{{$archiveItemName}}</a>
						</li>
					@endforeach
				</ul>
			</div>
		</div>

	@endif

</div>

<div class="blog-posts-draft form js-blog_drafts" style="margin: 20px 0 0;">
	<dl class="form--line form--group m-horizontal">
		<dt>Draft Posts:</dt>
		<dd>
			<div class="select">
				<select id="draftPosts" data-select>
					@foreach($draftPosts as $post)
						<option value="{{ $post->editLink }}">{{$post->title}}</option>
					@endforeach
				</select>
			</div>
			<a href="{{URL::route('blog-get',['category'=> '', 'slug'=>''])}}" class="btn m-blue" data-id="draftPosts">Edit</a>
		</dd>
	</dl>
	<dl class="form--line form--group m-horizontal">
		<dt>Pending Posts:</dt>
		<dd>
			<div class="select">
				<select id="pendingPosts" data-select>
					@foreach($pendingPosts as $post)
						<option value="{{ $post->editLink }}">{{$post->title}}</option>
					@endforeach
				</select>
			</div>
			<a href="{{URL::route('blog-get',['category'=> '', 'slug'=>''])}}" class="btn m-blue" data-id="pendingPosts">Edit</a>
		</dd>
	</dl>
	<dl class="form--group form--line m-horizontal">
		<dt>Future Posts:</dt>
		<dd>
			<div class="select">
				<select id="futurePosts" data-select>
					@foreach($futurePosts as $post)
						<option value="{{ $post->editLink }}">{{$post->title}}</option>
					@endforeach
				</select>
			</div>
			<a href="{{URL::route('blog-get',['category'=> '', 'slug'=>''])}}" class="btn m-blue" data-id="futurePosts">Edit</a>
		</dd>
	</dl>
</div>

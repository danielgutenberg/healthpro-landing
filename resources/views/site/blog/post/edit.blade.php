<div class="article_edit" data-image-url="{{URL::route('blog-image',[])}}?&post_id={{$post->id}}&_token={{Session::token()}}">
	<div class="wrap">

		{!! Former::framework('Nude') !!}
		{!! Former::open_for_files()->action( route('blog-update', [$post->slug]) )->class('form') !!}

			<div class="article_edit--body">
				<ul class="accordion_panes--list m-post_settings">
				    <li class="accordion_panes--item">
				        <label for="accordion_1" class="accordion_panes--item--opener">Post Settings</label>
				        <input type="checkbox" id="accordion_1" checked>
				        <div class="accordion_panes--item--content">
							<dl class="form--group form--line">
								<dt>
									<label for="cover_image">{{__('Cover Image')}}:</label>
								</dt>
								<dd>
									<input type='file' name="cover_image" />
									<div class="image-preview">
										<img id="cover_image_preview" src="#" alt="" style="visibility: hidden"/>
									</div>
								</dd>
							</dl>

							<dl class="form--group form--line">
								<dt>Status</dt>
								<dd>
									<div class="radio_group m-horizontal">
										@foreach($availableStates as $state)
											<label class="radio">
												{!! Former::radio('state')->value($state)->check($post->state==$state) !!}
												<span class="radio--i"></span>
												<span class="radio--label">{{$state}}</span>
											</label>

										@endforeach
									</div>
							    </dd>
						    </dl>

							<dl class="form--group form--line">
								<dt>
									<label for="tags">{{__('Post Tags')}}:</label>
								</dt>
								<dd>
									{!! TaxonomyHtml::input('tags', BlogService::taxTag()->slug, $postTags, ['view' => 'helpers.taxonomy.select2-input']) !!}
								</dd>
							</dl>

							<dl class="form--group form--line">
								<dt>
									<label for="category">{{__('Post Categories')}}:</label>
								</dt>
								<dd>
									{!! TaxonomyHtml::input('category', BlogService::taxCat()->slug, $post->category, ['view' => 'helpers.taxonomy.select2-input']) !!}
								</dd>
							</dl>


							<dl class="form--group form--line">
								<dt>
									<label for="slug">Slug:</label>
								</dt>
								<dd>
									<label class="field m-wide">
										<input type="text" name="new_slug" value="{{$post->slug}}" style="{{ ($errors->has('slug')?'color:red':'') }}">
									</label>
								</dd>
							</dl>

							<dl class="form--group form--line">
								<dt>
									<label for="name">Professional:</label>
								</dt>
								<dd>
									<label class="field m-wide">
										<input type="text" placeholder="e.g. Steve Martin, Angela Smith" name="name" id="name" data-selector="q[name]" value="{{$professional_name}}">
										<span class="field--autocomplete"></span>
									</label>
								</dd>
							</dl>
							<input type="hidden" name="professional_id" id="professional_id" value="{{$post->author_id}}">

							<dl class="form--line form--group">
								<dt>
									<label for="publish_at">{{__('Publishing time')}}:</label>
								</dt>
								<dd>
									<label class="field">
										<input type='text' name="publish_at" value="{{$post->publish_at}}" style="{{ ($errors->has('publish_at')?'color:red':'') }}"/>
									</label>
								</dd>
							</dl>
				        </div>
				        <span class="accordion_panes--item--arrow"></span>
				    </li>
				    <li class="accordion_panes--item">
				        <label for="accordion_2" class="accordion_panes--item--opener">SEO TOOLS</label>
				        <input type="checkbox" id="accordion_2">
				        <div class="accordion_panes--item--content">
				            <div>
								<dl class="form--group form--line">
									<dt>
										<label for="meta_title">{{__('Meta Title')}}</label>
									</dt>
									<dd>
										<div class="field m-wide">
											<input type="text" name="meta[title]" value="{{$post->meta['title'] or ''}}" placeholder="Default Title">
											<br>
										</div>
									</dd>
									<dt>
										<label for="meta_keywords">{{__('Meta Keywords')}}</label>
									</dt>
									<dd>
										<div class="field m-wide">
											<input type="text" name="meta[keywords]" value="{{$post->meta['keywords'] or ''}}" placeholder="Default Keywords">
											<br>
										</div>
									</dd>
									<dt>
										<label for="meta_description">{{__('Meta Description')}}</label>
									</dt>
									<dd>
										<div class="field m-wide">
											<textarea name="meta[description]" placeholder="Default Description">{{$post->meta['description'] or ''}}</textarea>
										</div>
									</dd>
								</dl>
							</div>
				        </div>
				        <span class="accordion_panes--item--arrow"></span>
				    </li>
				</ul>
				<input type="text" name="title" class="title-hidden" value="" hidden>
				<input type="text" name="content" class="content-hidden" value="" hidden>
			</div>

			<footer class="article_edit--footer">
				<button type="submit" class="article_edit--submit btn m-blue">Save</button>
			</footer>

		{!! Former::close() !!}

		{!! Former::open()->action( route('blog-delete',['id'=>$post->id]) )->method('DELETE')->class('form article_edit--delete') !!}
			{!! Former::button(__('Delete Post'))->type('submit')->class('btn m-red') !!}
		{!! Former::close() !!}
	</div>
</div>

<header class="page_header m-blog">
	<div class="page_header--title">HealthPRO Professionals <b>Blog</b></div>
    {{-- #todo make these configurable for all pages/routes like meta --}}
	<p class="page_header--text">A Resource For Health and Wellness Professionals <br> Read the latest insights on running your health, wellness or fitness business.</p>
</header>

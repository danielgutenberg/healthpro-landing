<div class="article_edit" style="background-color: #f9fafc; padding: 20px;">
	<div class="wrap">
		{!! Former::open()->action( route('blog-create') ) !!}
		{!! Former::button(__('Create new post'))->type('submit')->class('btn m-blue') !!}
		{!! Former::close() !!}

		@include('site.blog.post.draft')
	</div>
</div>

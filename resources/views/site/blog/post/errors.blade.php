<div class="article--errors">
	<dl>
		<dt>There were errors:</dt>
		<dd>
			<ul>
				<?php $prettyNames=['title'=>'Title must not be empty','publish_at'=>'Wrong `Publishing Time` or empty','content'=>'Contents of post must be filled','category'=>'Category Not set']; ?>
				@foreach($errors->getBag('default')->toArray() as $key=>$value)
					<li>{{array_key_exists($key,$prettyNames)?$prettyNames[$key]:$key}}</li>
				@endforeach
			</ul>
		</dd>
	</dl>
</div>

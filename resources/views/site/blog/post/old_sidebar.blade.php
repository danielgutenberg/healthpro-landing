<div class="blog-post-categories">
	<span>Categories: </span>
	@foreach($categoriesWithPosts as $item)
	<a href="{{URL::route('blog-category-non-page',$item->slug)}}" class="category-item">{{$item->name}}</a>
	@endforeach
</div>

<div class="blog-post-tags">
	<span>Tags: </span>
	@foreach($availableTags as $item)
	<a href="{{URL::route('blog-tag-non-page',$item->slug)}}" class="tag-item">{{$item->name}}</a>
	@endforeach
</div>

@extends('site.layouts.default')

@section('title'){{$metaTitlePrefix.$metaTitle.$metaTitleSuffix}}@stop
@section('meta-description'){{$metaDescription}}@stop
@section('meta-keywords'){{$metaKeywords}}@stop

@section('content')

	<div class="page">
		<div class="article_cover" style="background-image: url({{$post->cover_image_url}});"></div>
		<div class="article-columns" >
			<article class="article @if($isEditMode)m-editable js-article_edit @endif" data-article>
			@if($errors->has())
				@include('site.blog.post.errors')
			@endif
			<div class="article--container" data-sticky-container>

				<div class="article--text">
					<div class="container">
						@if ($isEditMode)
							<h1 contenteditable="true" role="text" aria-multiline="false" class="article--title">{{$post->title}}</h1>
							<div class="editable" id="a">
								{!! $post->content !!}
							</div>
						@else
							<h1>{{$post->title}}</h1>
							{!! $post->content !!}
						@endif
					</div>
				</div>

				<div class="article--info">
					<figure class="article--info--userpic userpic m-circle">
						<span class="userpic--inner"><img src="{{$post->author->avatar ?: asset('assets/frontend/images/favicon/favicon-128x128.png')}}"></span>
					</figure>
					<strong class="article--info--name">{{$post->author->full_name}}</strong>
					<time class="article--info--date">{{$post->publish_at->toFormattedDateString()}}</time>

					{{--<ul class="article--info--meta">--}}
						{{--<li class="m-comments">{{ count($post->comments)}}</li>--}}
					{{--</ul>--}}

					<span class="article--info--label">Share</span>
					<ul class="article--info--share social-likes" data-article-el="share">
						<li class="facebook" title="Share link on Facebook">Facebook</li>
						<li class="twitter" title="Share link on Twitter">Twitter</li>
						<li class="plusone" title="Share link on Google+">Google+</li>
					</ul>
				</div>
			</div>

		</article>
			@include('site.blog.post.sidebar',array('profile_in_sidebar'=>true))
		</div>
		@if($isEditMode)
			@include('site.blog.post.edit',compact('post','postTags','hashedAvailableTags','hashedAvailableCategories'))
		@elseif($editor)
			<div class="article_edit">
				<div class="wrap" style="text-align:right;">
					<a href="{{$post->editLink}}" class="btn m-blue">{{__("Edit")}}</a>
				</div>
			</div>
		@endif
	</div>

	{{--@include('site._partials.comment.comments',['comments'=>$post->comments, 'entityId'=>$post->id,'entityType'=>get_class($post),'canPostComment'=>$canAddComment])--}}
@stop

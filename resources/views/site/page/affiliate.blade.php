@extends('site.layouts.professional')
@section('title')Affiliate Program | HealthPRO @stop
@section('meta-description')Earn extra income with HealthPRO’s affiliate program while helping independent health, wellness and fitness professionals to better manage their business. Discover high earning potential and the many other benefits of joining the HealthPRO affiliate program.@stop
@section('meta-keywords'){{'healthpro.com,healthpro,pro,health,earn,income,affiliate,program,join,us,our,team,commission,upfront,high,earnings'}}@stop
@section('content')
	<section class="affiliate">
		<header class="affiliate--header">
			<div class="affiliate--header-wrapper">
				<h1 class="affiliate--header-title">
					THE HEALTHPRO AFFILIATE PROGRAM
				</h1>
				<h4 class="affiliate--header-title2">
					Earn income while helping Health, Wellness and Fitness Professionals
					better manage their business
				</h4>
				<div class="affiliate--header-button">
					<a href="#" class="btn_round m-xlarge m-arrow m-green m-upper affiliate--btn-become
					affiliate--btn-become1  affiliate--btn-notmobile" data-toggle="popup" data-target="affiliate_program">BECOME AN AFFILIATE
					</a>
					<a href="#" class="btn_round m-large  m-arrow m-green m-upper affiliate--btn-become
					affiliate--btn-become1  affiliate--btn-mobile" data-toggle="popup" data-target="affiliate_program">BECOME AN AFFILIATE
					</a>
				</div>
			</div>
		</header>
		<div class="affiliate--body-line"></div>
		<section class="affiliate--body">
			<div class="affiliate--line1-mobile"></div>
			<div class="affiliate--body-item affiliate--body-item-equation">
				<div class="affiliate--body-item-up">
					<div class="affiliate--body-item-up-icon affiliate--icon-join-us"></div>
					<h3 class="affiliate--body-item-up-title">COME JOIN OUR TEAM</h3>
					<div class="affiliate--body-item-up-line"></div>
					<div class="affiliate--body-item-up-text">It only takes minutes to get on board and become part of the leading health and wellness platform.</div>
				</div>
				<div class="affiliate--body-item-down">
					<div class="affiliate--equation">
						<div class="affiliate--equation-item affiliate--equation-partnership">
							<img class="affiliate--equation-img affiliate--equation-partnership-img" src="/assets/frontend/images/content/affiliate/partnership.svg" alt="Partner with a leading brand" title="partnership"/>
							<div class="affiliate--equation-text affiliate--equation-partnership-text">Partner with a leading brand</div>
						</div>
						<div class="affiliate--equation-item affiliate--equation-plus affiliate--equation-item-sign">
							<img class="affiliate--equation-img affiliate--equation-plus-img" src="/assets/frontend/images/content/affiliate/plus.svg" alt="+" title="plus"/>
						</div>
						<div class="affiliate--equation-item affiliate--equation-product">
							<div class="affiliate--equation-text affiliate--equation-product-text">Offer a quality product to your network</div>
							<img class="affiliate--equation-img affiliate--equation-product-img" src="/assets/frontend/images/content/affiliate/quality-product.svg" alt="Offer a quality product to your network" title="product"/>
						</div>
						<div class="affiliate--equation-item affiliate--equation-equal affiliate--equation-item-sign">
							<img class="affiliate--equation-img affiliate--equation-equal-img" src="/assets/frontend/images/content/affiliate/equal.svg" alt="=" title="equal"/>
						</div>
						<div class="affiliate--equation-item affiliate--equation-profit">
							<img class="affiliate--equation-img affiliate--equation-profit-img" src="/assets/frontend/images/content/affiliate/profit.svg" alt="Enjoy high earning potential" title="profit"/>
							<div class="affiliate--equation-text affiliate--equation-profit-text">Enjoy high earning potential</div>
						</div>
					</div>
				</div>
			</div>
			<div class="affiliate--line2-mobile"></div>
			<div class="affiliate--body-item">
				<div class="affiliate--body-item-up">
					<div class="affiliate--body-item-up-icon affiliate--icon-what-we-give-you"></div>
					<h3 class="affiliate--body-item-up-title">WHAT WE GIVE YOU</h3>
					<div class="affiliate--body-item-up-line"></div>
					<div class="affiliate--body-item-up-text">Your success is our success. We give you the tools and reporting to succeed.</div>
				</div>
				<div class="affiliate--body-item-down">
					<ul class="affiliate--body-item-down-list">
						<li>Ongoing Support</li>
						<li>Banner Ads</li>
						<li>Social Media & Email Messaging</li>
						<li>Tracking Codes</li>
					</ul>
				</div>
			</div>
			<div class="affiliate--line3-mobile"></div>
			<div class="affiliate--body-item">
				<div class="affiliate--body-item-up">
					<div class="affiliate--body-item-up-icon affiliate--icon-what-you-earn"></div>
					<h3 class="affiliate--body-item-up-title">WHAT YOU EARN</h3>
					<div class="affiliate--body-item-up-line"></div>
					<div class="affiliate--body-item-up-text">High upfront commissions that grow exponentially over time.</div>
				</div>
				<div class="affiliate--body-item-down affiliate--graph">
					<div class="affiliate--graph-wrapper">
						<div class="affiliate--graph-data">
							<h4 class="affiliate--graph-title">IT'S EASY TO EARN OVER</h4>
							<div class="affiliate--graph-after-item">
								<div class="affiliate--graph-after-item-profit">$18,630</div>
								<div class="affiliate--graph-after-item-term">After 1 year</div>
							</div>
							<div class="affiliate--graph-after-item">
								<div class="affiliate--graph-after-item-profit">$56,700</div>
								<div class="affiliate--graph-after-item-term">After 2 years</div>
							</div>
							<div class="affiliate--graph-after-item">
								<div class="affiliate--graph-after-item-profit">$114,210</div>
								<div class="affiliate--graph-after-item-term">After 3 years</div>
							</div>
						</div>
						<img class="affiliate--graph-img" src="/assets/frontend/images/content/affiliate/bg-down-3.svg" alt="1. month 12 - $2,295  2. month24 - $3,915  3. month 36 - $5,535 " title="profits terms"/>
					</div>
					<div class="affiliate--graph-note">*Based on less than one sale per day</div>
				</div>
			</div>

		</section>
		<section class="affiliate--footer">
			<div class="affiliate--footer-button">
				<a href="#" class="cta_btn m-xlarge m-arrow m-blue m-upper affiliate--btn-become
				  affiliate--btn-become2 affiliate--btn-notmobile"  data-toggle="popup" data-target="affiliate_program">BECOME AN AFFILIATE
				</a>
				<a href="#" class="cta_btn m-large m-arrow m-blue m-upper affiliate--btn-become
				  affiliate--btn-become2 affiliate--btn-mobile" data-toggle="popup" data-target="affiliate_program">BECOME AN AFFILIATE
				</a>
			</div>
		</section>
	</section>
@stop
@section('popups')
	<div class="popup affiliate_program" data-popup="affiliate_program">
		<div class="popup--container" data-popup-container>
			<button class="popup--close" data-popup-close>close</button>
			<div class="popup--content partner_contact--content">
				<div class="partner_contact--form" data-cs-form="affiliate_program"></div>
				<div class="loading_overlay m-light m-hide"><span class="spin m-logo"></span></div>
			</div>
		</div>
		<div class="popup--bg"></div>
	</div>
@append

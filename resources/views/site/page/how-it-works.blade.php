@extends('site.layouts.professional')

@section('title')Learn How HealthPRO Works @stop
@section('meta-description')Learn how managing schedules, payments and bookings is simple and fast with HealthPRO’s online platform. Find out how HealthPRO’s software makes it easy to manage your independent health, wellness or fitness business.@stop
@section('meta-keywords'){{'Learn,how,HealthPRO,works,find,out,easy,simple,platform,software,online,management,business,scheduling,payments,calendar,HealthPRO.com,HealthPRO,pro,health'}}@stop
@section('content')
	<section class="how_it_works">
		<div class="promo_simplifies" data-scrolly-section>
			<div class="promo_leader m-simplifies">
				<div class="promo_leader--inner wrap">
					<h1 class="promo_leader--heading">HealthPRO’s <span class="nowrap">All-in-One</span> Software Simplifies Your Business</h1>
					<div class="promo_leader--text">
						<p>Tailored Business Solutions Save You Time and Money</p>
					</div>
					<div class="promo_leader--actions">
						<a class="promo_leader--actions_btn btn_round m-blue_light m-upper m-arrow" href="#compare_plans" data-scroll>Compare plans</a>
						<a class="promo_leader--actions_btn btn_round m-green m-upper m-arrow" href="#demo_request" data-toggle="popup" data-target="demo_request">Request a demo</a>
					</div>
					<a class="promo_leader--scroll" href="#booking_options" data-scroll-more>Scroll Down to Learn More</a>
				</div>
			</div>
			<div class="promo_simplifies--footer">
				<div class="promo_simplifies--footer_inner wrap">
					<div class="promo_simplifies--footer_text">
						<a href="#compare_plans" data-scroll>Find the right plan for your business</a>
					</div>
				</div>
			</div>
		</div>

		<div class="presentation m-booking_options" id="booking_options" data-scrolly-section>
			<div class="presentation--inner wrap">
				<div class="presentation--phone m-booking_options"><figure class="presentation--phone_content"></figure></div>
				<div class="presentation--content">
					<div class="presentation--content_heading">24/7 Booking Options</div>
					<div class="presentation--content_text">
						<p>Online booking gives clients the freedom to self-book at their convenience. You are alerted instantly whenever a client books, reschedules or cancels. Say goodbye to no-shows with automated appointment reminders and notifications sent out to clients via email or SMS.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="presentation m-right m-has_bg m-calendar_integration" id="calendar_integration" data-scrolly-section>
			<div class="presentation--inner wrap">
				<div class="presentation--phone m-calendar_integration"><figure class="presentation--phone_content"></figure></div>
				<div class="presentation--content">
					<div class="presentation--content_heading">Seamless Calendar Integration</div>
					<div class="presentation--content_text">
						<p>You are in full control of your schedule - easily block off time slots, allot padding time between sessions and set appointment durations. For the ultimate convenience, your HealthPRO calendar automatically syncs with your Google calendar.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="presentation m-transactions_simple" id="transactions_simple" data-scrolly-section>
			<div class="presentation--inner wrap">
				<div class="presentation--phone m-transactions_simple"><figure class="presentation--phone_content"></figure></div>
				<div class="presentation--content">
					<div class="presentation--content_heading">Transactions Made Simple</div>
					<div class="presentation--content_text">
						<p>Clients appreciate the flexible payment options including paying online with any major credit or debit card. You’ll enjoy the ease of processing payments via Paypal or Stripe, and the freedom to accept cash or checks, view transaction history and issue invoices or receipts 24/7.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="presentation m-right m-has_bg m-client_engagement" id="client_engagement" data-scrolly-section>
			<div class="presentation--inner wrap">
				<div class="presentation--tablet m-client_engagement"><figure class="presentation--tablet_content"></figure></div>
				<div class="presentation--content">
					<div class="presentation--content_heading">Advanced Client Engagement</div>
					<div class="presentation--content_text">
						<p>HealthPRO’s software makes it a breeze to stay connected in real-time with your clients. It’s simple to send messages and notifications, access client notes and communicate with your clients all from the HealthPRO platform.</p>
					</div>
				</div>
			</div>
		</div>
		{{-- data here --}}


		<div class="pricing_cta" id="compare_plans" data-scrolly-section>
			<div class="pricing_cta--inner wrap">
				<div class="pricing_cta--heading">A Plan For Everyone</div>
				<div class="pricing_cta--text">HealthPRO’s plans meet the needs of both new and established businesses</div>
				<div class="pricing_cta--list">
					@foreach ($products as $product)
						@php
							$membership = $product['membership'];
							$price = explode('.', $product['products'][0]['price']);
							$options = array();
							$product_action = '';
							$is_selected = false;
							$product_id = null;

							switch ($membership['slug']) {
								case 'part_timer':
									$classname = 'm-green';
									break;
								case 'healthpro':
									$classname = 'm-red';
									break;
								case 'healthpro_plus':
									$classname = 'm-blue m-unlimited';
									break;
							}

							foreach ($product['products'] as $product_options) {
								$obj = new stdClass;
								$obj->id = $product_options['id'];
								$obj->price = $product_options['price'];
								$obj->billing_cycles = $product_options['billing_cycles'];
								$obj->name = $membership['name'];
								$product_id = $product_options['id'];

								if ($obj->billing_cycles == 1) {
									$monthly_price = $product_options['price'];
									$monthly_price = explode('.', $monthly_price);
								}

								array_push($options, $obj);
								$is_subscribed = $product['action'] != 'BUY NOW';
							}
						@endphp

						@if ($is_subscribed)
							<a
								class="pricing_cta--item pricing_cta--btn {{ $classname }}"
								href="{{route('dashboard-billing-info')}}#change_plan:{{$product_id}}">
						@else
							<button
								class="pricing_cta--item pricing_cta--btn {{ $classname }}"
								data-pricing-btn
								data-toggle="popup"
								data-target="popup_membership"
								data-product-options="{{ json_encode($options) }}">
						@endif
								<span class="pricing_cta--btn_price">
									<sup class="m-currency">$</sup>
									<b>{{ $monthly_price[0] }} .</b>
									<sup>
										@if (count($monthly_price) > 1)
											{{ $monthly_price[1] }}
										@else
											00
										@endif
										<i>/ month</i>
									</sup>
								</span>
								<span class="pricing_cta--btn_bookings">
									@if ($product['membership']['bookings_limit'])
										{{ $product['membership']['bookings_limit'] }}
									@else
										Unlimited
									@endif
									Bookings
								</span>
								@unless ($membership['slug'] == 'part_timer')
									<span class="pricing_cta--btn_features">
										<span class="pricing_cta--btn_features_item">
											<i class="inline_icon m-comments"></i>
											SMS Notifications
										</span>
										<span class="pricing_cta--btn_features_item">
											<i class="inline_icon m-gift"></i>
											Gift Certificates
										</span>
									</span>
								@endunless
						@if ($is_subscribed)
							</a>
						@else
							</button>
						@endif

					@endforeach

				</div>
			</div>
		</div>

		<div class="business_features">
			<div class="business_features--inner">
				<div class="business_features--content">
					<div class="business_features--heading">Run Your Business with Powerful Features that are Easy-to-Use </div>

					<?php
						$features = [
							[
								'slug' => 'profile',
								'title' => 'Public <br> Profile',
								'panel' => [
									'list' => [
										[
											'title' => 'Your Services',
											'text' => 'List all the types of services you provide.'
										],
										[
											'title' => 'Your Rates',
											'text' => 'Set different rates for each service you offer.'
										],
										[
											'title' => 'Your Durations',
											'text' => 'Set various durations for each session.'
										],
										[
											'title' => 'Your Locations',
											'text' => 'List all the locations from which you work.'
										],
										[
											'title' => 'Social Links ',
											'text' => 'Let clients connect with you from their favorite social networks.'
										],
										[
											'title' => 'Photos & Videos',
											'text' => 'Upload photos and videos. '
										],
										[
											'title' => 'Suitability & Conditions ',
											'text' => 'List any specialities or niche populations with which you work.'
										],
									]
								]
							],
							[
								'slug' => 'payments',
								'title' => 'Payments & <br> Transactions',
								'panel' => [
									'list' => [
										[
											'title' => 'Multiple Payment Types',
											'text' => 'Offer clients the flexibility to pay you online, or with check or cash.'
										],
										[
											'title' => 'Major Credit Cards Accepted',
											'text' => 'Visa, MasterCard, American Express, Discover Cards, Diners Club and JCB.'
										],
										[
											'title' => 'PayPAL and Stripe Integration',
											'text' => 'Process online payments quickly, easily and securely.'
										],
										[
											'title' => 'Transaction History',
											'text' => 'Effortlessly keep track of all transactions.'
										],
										[
											'title' => 'Receipts and Invoices',
											'text' => 'Issue client receipts and invoices with a click.'
										],
										[
											'title' => 'PCI Compliant',
											'text' => ''
										],
										[
											'title' => 'SSL Certificate',
											'text' => ''
										],
										[
											'title' => '3rd Party Security Certificate',
											'text' => ''
										],
										[
											'title' => '3rd Party Identification & Domain Control Verification',
											'text' => ''
										],
									]
								]
							],
							[
								'slug' => 'scheduling',
								'title' => 'Automation & <br> Scheduling',
								'panel' => [
									'list' => [
										[
											'title' => 'Notifications & Appointment Reminders',
											'text' => 'By email or via SMS.*'
										],
										[
											'title' => 'Two-Way Calendar Sync',
											'text' => 'Calendar seamlessly syncs with Google.'
										],
										[
											'title' => 'Print Your Schedule Feature ',
											'text' => 'Print a copy of your schedule for your ultimate convenience.'
										],
										[
											'title' => 'Block Off Time',
											'text' => 'Block off time slots when you’re unavailable.'
										],
										[
											'title' => 'Padding Time',
											'text' => 'Allot yourself extra time between appointments.'
										],
										[
											'title' => 'Calendar Updates',
											'text' => 'Real time calendar updates and you’re notified of each schedule change.'
										],
										[
											'title' => 'Mobile Friendly',
											'text' => 'Manage and grow your business on the go from any device.'
										],
										[
											'title' => 'Multiple Client Upload',
											'text' => 'Upload clients through a CSV.'
										],
									],
									'content' => '<p>List all the locations from which you work. Set various durations for each session.</p>',
									'disclaimer' => '*Available to HealthPRO and HealthPRO Plus plan users.'
								]
							],
							[
								'slug' => 'booking',
								'title' => 'Online <br> Booking',
								'panel' => [
									'list' => [
										[
											'title' => '24/7 Booking',
											'text' => 'Clients have total freedom to book, reschedule and cancel appointments.'
										],
										[
											'title' => 'Single & Recurring Appointments',
											'text' => 'Clients can book single appointments or sessions that take place weekly.'
										],
										[
											'title' => 'Monthly Packages & Auto-Renewal',
											'text' => 'Offer clients a discount when purchasing a group of sessions.'
										],
										[
											'title' => 'Custom Sessions',
											'text' => 'Offer customized appointments for specific customers (duration & price).'
										],
									]
								]
							],
							[
								'slug' => 'suite',
								'title' => 'Marketing <br> Suite',
								'panel' => [
									'list' => [
										[
											'title' => 'Personalized URL',
											'text' => 'Banner and buttons to your profile page.'
										],
										[
											'title' => 'Gift Certificates*',
											'text' => 'Offer clients the option to purchase gift certificates for friends and family.'
										],
										[
											'title' => 'Social Networks',
											'text' => 'Link your social media account to your HealthPRO profile.'
										],
										[
											'title' => 'Blog',
											'text' => 'Become a HealthPRO guest blogger. <a href="/providers/blog">Read our blog here.</a>'
										],
									],
									'disclaimer' => '*Available to HealthPRO and HealthPRO Plus plan users.'
								]
							],
							[
								'slug' => 'support',
								'title' => 'Communication & <br> Support',
								'panel' => [
									'list' => [
										[
											'title' => 'Client Notes & History',
											'text' => 'Save and access notes/history on each client.'
										],
										[
											'title' => 'Attendance Tracker',
											'text' => 'View a clear record of clients’ attendance.'
										],
										[
											'title' => 'Direct Messaging',
											'text' => 'Easily communicate with all your clients on one platform.'
										],
										[
											'title' => 'Phone, Email and Live Chat Support',
											'text' => 'Our dedicated support team is always available.'
										],
									]
								]
							],
						];
					?>

					<div class="business_features--collapsible" data-collapsible>
						@foreach($features as $feature)
							@if (isset($feature['panel']))
								<div class="business_features--collapsible_item" data-collapsible-item>
									<div class="business_features--collapsible_title" data-collapsible-opener><i class='business_features--collapsible_title_opener'></i>{!!$feature['title']!!}</div>
									<div class="business_features--collapsible_content" data-collapsible-content>
										@if (isset($feature['panel']['content']))
											<div class="business_features--text">
												{!! $feature['panel']['content'] !!}
											</div>
										@endif
										@if (isset($feature['panel']['list']))
											<ul class="business_features--list">
												@foreach ($feature['panel']['list'] as $item)
													<li>
														<b>{{ $item['title'] }}</b>
														{!! $item['text'] !!}
													</li>
												@endforeach
											</ul>
										@endif
										@if (isset($feature['panel']['disclaimer']))
											<div class="business_features--text">
												{!! $feature['panel']['disclaimer'] !!}
											</div>
										@endif
									</div>
								</div>
							@endif
						@endforeach
					</div>
				</div>
			</div>
		</div>

		<div class="promo_section m-light">
			<div class="promo_section--wrap wrap">
				<div class="promo_section--cta">
					<a href="" target="" class="cta_btn m-green m-larger m-arrow" data-toggle="popup" data-target="demo_request">Request a Demo</a>
				</div>
			</div>
		</div>
	</section>
@stop

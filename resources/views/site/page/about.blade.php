@extends('site.layouts.professional_menu')
@section('title')Meet The HealthPRO Team @stop
@section('meta-description')Learn more about HealthPRO and our dedicated team. Meet the people behind HealthPRO; find out who we are and what we are doing.@stop
@section('meta-keywords'){{'About,team,founder,investor,employees,staff,business,development,meet,HealthPRO.com,HealthPRO,pro,health'}}@stop
@section('content')
	<div class="page-about promo">
		<div class="about_hero">
			<div class="about_hero--inner wrap">
				<div class="about_hero--heading">OUR TEAM</div>
			</div>
		</div>

		<div class="about_section">
			<div class="about_section--wrap">
				<div class="about_section--founders">
					<div class="about_section--title">Meet Our <strong>Founders</strong></div>
					<ul class="about_section--list">
						<li class="about_section--list_item m-large m-wide">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/reuven.jpg')}}" alt="Reuven Jacob">
							</figure>
							<div class="about_section--list_item_name">Reuven Jacob</div>
							<div class="about_section--list_item_position">Co-Founder / CEO</div>
							<div class="about_section--story">
								<p>Reuven is a serial entrepreneur with a flair for the creative and inventive. Previously, he founded Baruch Haba, a tourism and hospitality business. He also directed sales for Rumplers Foods where he successfully pursued and developed distribution channels for nutritious and affordable foods for local public consumption.</p>
							</div>
						</li>
						<li class="about_section--list_item m-large m-wide">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/suzi.jpg')}}" alt="Suzi Zettel, MSW">
							</figure>
							<div class="about_section--list_item_name">Suzi Zettel, MSW</div>
							<div class="about_section--list_item_position">Co-Founder and VP of Therapeutic Services</div>
							<div class="about_section--story">
								<p>Suzi is a practicing Yoga Instructor and a certified yoga and family therapist. She is dedicated to helping her colleagues identify and overcome the challenges of being a health, wellness and fitness professional. Passionate about health, children, human rights, poverty, and the environment, Suzi is busy making the world a better place.</p>
							</div>
						</li>
					</ul>
					<ul class="about_section--list">
						<li class="about_section--list_item m-large">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/harry.jpg')}}" alt="Harry Zettel">
							</figure>
							<div class="about_section--list_item_name">Harry Zettel</div>
							<div class="about_section--list_item_position">VP of Business Development</div>
							<div class="about_section--story">
								<p>Harry brings decades of startup experience together with an expertise in accounting. Harry was previously a Managing Partner and VP of Business Development at Global Asset Tracing Inc. and a Partner at Perry Krieger and Associates Inc., a boutique insolvency firm in Toronto, Canada. Harry is the financial guru who turns inspirational ideas into fiscally responsible and successful realities.</p>
							</div>
						</li>
						<li class="about_section--list_item m-large">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/sam.jpg')}}" alt="Sam Capuano">
							</figure>
							<div class="about_section--list_item_name">Sam Capuano</div>
							<div class="about_section--list_item_position">Chairman</div>
							<div class="about_section--story">
								<p>Sammy is a successful entrepreneur who is passionate about helping people. Sammy has started and invested in nearly a dozen startups and social-responsibility ventures. Most recently, Sammy was Founder and Chairman of GreenSmoke, an industry-leading electronic cigarette company with over 150 employees. In 2014, GreenSmoke was sold to Altria Group Inc., the parent company of Philip Morris USA.</p>
							</div>
						</li>
						<li class="about_section--list_item m-large">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/daniel_treisman.jpg')}}" alt="Daniel Treisman">
							</figure>
							<div class="about_section--list_item_name">Daniel Treisman</div>
							<div class="about_section--list_item_position">Investor/Advisor</div>
							<div class="about_section--story">
								<p>Daniel Treisman is a serial entrepreneur, media and internet advertising expert. He is the Editor and Managing Director of the popular news and entertainment website, Inquisitr, which garners over 30 million unique views each month. Daniel is also the founder and CEO of Guru Media, a traffic acquisition portal for media and retail websites. </p>
							</div>
						</li>
					</ul>
				</div>

				<div class="about_section--team">
					<div class="about_section--title">Meet The <strong>Team</strong></div>
					<ul class="about_section--list">
						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/adina.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Adina Geffen</div>
							<div class="about_section--list_item_position">Development Manager</div>
							<div class="about_section--story">
								<p>Adina is an experienced team leader in both startups and global companies, having most recently led a software development team for Qualcomm. She developed expert skills and valuable experience while taking many projects from initial stages to successful delivery, both to customers and the open source community. Adina excels at helping her team stay focused on delivery, while paying close attention to details and the overall quality of the product.</p>
							</div>
						</li>
						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/aaron.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Aaron Harun</div>
							<div class="about_section--list_item_position">CTO</div>
							<div class="about_section--story">
								<p>Aaron has served as CTO for numerous early and mid-stage startups. Aaron, who began as a full-stack developer at the tender age of 17, has managed and built multiple websites for Fortune 500 companies and for companies with over 35k concurrent online users. He has developed plugins and themes that are installed on over a million websites. His specialties include ‘Lean Startup’ methodology and responsive design.</p>
							</div>
						</li>
						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/shalom.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Shalom Issenberg</div>
							<div class="about_section--list_item_position">Director of Marketing</div>
							<div class="about_section--story">
								<p>Shalom has a proven track record in designing and executing B2B and B2C strategies across some of the internet’s most competitive verticals including health, finance, investing and more. Shalom is an expert in creating and optimizing software for social media marketing, search technologies, web analytics and customer relationship management solutions. He has successfully managed multi-million dollar advertising campaigns across virtually all digital platforms and social networks.</p>
							</div>
						</li>
						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/alex.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Alex Traiman</div>
							<div class="about_section--list_item_position">Marketing &amp; Strategy</div>
							<div class="about_section--story">
								<p>Alex Traiman is a media, marketing and public relations specialist who has worked extensively in both the business and nonprofit sectors. Alex served as a project manager for the launch of over a dozen content-driven websites, and has consulted on marketing and investor relations for numerous private and public startups. Alex is a trained professional journalist, a sought after public speaker, and an award-winning documentary filmmaker.</p>
							</div>
						</li>

						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/judy.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Judy Goodman</div>
							<div class="about_section--list_item_position">Marketing &amp; Branding Specialist</div>
							<div class="about_section--story">
								<p>Judy is experienced in researching, creating and editing print and online content for both businesses and nonprofit organizations in a range of sectors. Her creativity, attention to detail and technical writing skills enable her to produce quality content under tight deadlines to advance the success of the team. Judy is a published author, a freelance writer for leading publications, and a practicing certified ACE fitness professional with a specialty in sports nutrition.</p>
							</div>
						</li>
						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/jennifer.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Jennifer Dentes</div>
							<div class="about_section--list_item_position">Graphic Designer</div>
							<div class="about_section--story">
								<p>Jennifer is a traditional graphic artist and web designer who takes great pride in designing clean and functional websites. Extremely self-motivated and detail-oriented, Jennifer loves a challenge and enjoys simplifying complex projects. She excels at remaining focused on the needs of the user to provide a top quality compelling visual experience.</p>
							</div>
						</li>
						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/stella.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Stella Krasnitski</div>
							<div class="about_section--list_item_position">UX/UI Designer</div>
							<div class="about_section--story">
								<p>Stella is a passionate UI/UX professional who specializes in using and applying the latest trends and techniques to create winning responsive and mobile website designs. Her creativity, excellent interpersonal skills, strong work ethic and team focused attitude have served her well in her previous posts designing websites for startups. Stella thrives under the pressure of juggling multiple priorities with tight deadlines.</p>
							</div>
						</li>
						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/daniel.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Daniel Gutenberg</div>
							<div class="about_section--list_item_position">Backend Developer</div>
							<div class="about_section--story">
								<p>Daniel is a self-taught web developer who learned to implement and adapt technically sophisticated online web applications while initially working in QA. He is a passionate, responsible and committed engineer with a get-it-done-on-time spirit. Daniel is a natural problem solver and loves the challenge of analyzing each and every situation to come up with the best possible programming solution.</p>
							</div>
						</li>

						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/eli.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Eli De Mayo</div>
							<div class="about_section--list_item_position">Backend Developer</div>
							<div class="about_section--story">
								<p>Eli is a backend developer with years of experience programming across multiple platforms, languages and technologies while working with hi-tech startups. Eli is well known for his remarkable ability stay calm and focused, even in the most stressful situations, to consistently deliver a high quality product. In his spare time, Eli volunteers as an animal rescuer.</p>
							</div>
						</li>
						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/dovid.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Dovid Krongold</div>
							<div class="about_section--list_item_position">Quality Assurance</div>
							<div class="about_section--story">
								<p>For the past two decades, Dovid has helped top companies like Apple and Henry Schein, along with numerous startups, to succeed while deploying complex online products. Dovid is enthusiastic and dedicated to ensuring that users enjoy a friendly and smooth online experience. Dovid's comprehensive testing of HealthPRO.com assures that both professionals and their clients can focus on what they want most - wellness and good health.</p>
							</div>
						</li>
						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/eugen.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Eugen Figursky</div>
							<div class="about_section--list_item_position">Full-stack Developer</div>
							<div class="about_section--story">
								<p>Eugen is a full-stack web developer with over a decade of experience in the industry. His attention to detail combined with his keen eye for simple and elegant design drives his success in achieving results that work for every type of user. Eugene stays up-to-date on new releases and loves finding interesting solutions to complex problems.</p>
							</div>
						</li>
						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/vladislav.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Vladislav Cartovenco</div>
							<div class="about_section--list_item_position">Frontend Developer</div>
							<div class="about_section--story">
								<p>Vladislav is a front-end developer who loves combining clean and efficient code with hiqh quality UI/UX design. He has developed the front interlayer for hundreds of websites ranging from small personal sites to large and intricate projects. Vladislav enthusiastically incorporates the most cutting-edge technologies to improve user interaction with a product.</p>
							</div>
						</li>

						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/jonathan.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Jonathan Richey</div>
							<div class="about_section--list_item_position">Customer Services Department Manager</div>
							<div class="about_section--story">
								<p>Jonathan is a 20-year veteran in the field of customer service. He possesses an acute understanding of the complexities involved in providing customers with a positive user experience, and is dedicated to incorporating excellent customer service into the company’s wider strategic vision. Both personable and professional, Jonathan strives to listen compassionately and respond proactively to each and every customer.</p>
							</div>
						</li>
						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/esther.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Esther Richey</div>
							<div class="about_section--list_item_position">Customer Service Sales Agent</div>
							<div class="about_section--story">
								<p>Esther brings over 15 years experience in sales across various products and target demographics. Her razor-focused drive has helped her achieve success in previous positions, including in her most recent post as a partner responsible for sales growth in a leading real estate firm. Esther uses her skills and experience to understand and meet the unique needs of each customer.</p>
							</div>
						</li>
						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/steven.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Steven Goldreich</div>
							<div class="about_section--list_item_position">Customer Service Sales Agent</div>
							<div class="about_section--story">
								<p>Steven combines his flair for creative problem solving with his strong technical analysis skills to achieve exceptional results. Steven’s diverse experience, ranging from working as a certified professional engineer to leading sales teams, provides him with a solid foundation to ensure HealthPRO’s community of health and wellness professionals enjoy and benefit from a seamless user experience.</p>
							</div>
						</li>
						<li class="about_section--list_item m-overlay">
							<figure class="about_section--list_item_fig">
								<img src="{{asset('/assets/frontend/images/content/about/team/avi.jpg')}}" alt="">
							</figure>
							<div class="about_section--list_item_name">Avi Gargir</div>
							<div class="about_section--list_item_position">Customer Service Sales Agent</div>
							<div class="about_section--story">
								<p>Avi has honed his natural interpersonal skills and developed his ability to rapidly assess and respond to situations through various positions he has held. Avi also brings over five years experience working in the fields of customer service and sales.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="promo_section m-low m-dark">
			<div class="promo_section--wrap">
				<div class="promo_heading m-about">
					<div class="promo_heading--title">Are you a Health Professional?</div>
					<div class="promo_heading--description">
						<p>We help you successfully manage your business so you can get back to your passion</p>
					</div>
				</div>
				<div class="promo_section--cta">
					@if(Sentinel::check())
						<a href="{{route('dashboard-schedule')}}" class="cta_btn m-green m-larger m-arrow">Get Started Now</a>
					@else
						<a href="{{route('pricing')}}" class="cta_btn m-green m-larger m-arrow">Sign Up Now</a>

					@endif
				</div>
			</div>
		</div>
	</div>
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"> </script>
<script>
    $(document).ready(function () {
        $('.about_section--list_item').on('touchend', function (e) {
            e.preventDefault();
            $(this).trigger('hover');
            return false;
        });
    });
</script>

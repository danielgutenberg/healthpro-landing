@extends('site.layouts.professional')
@section('title')How to Contact Us for Service or Support | HealthPRO @stop
@section('meta-description')Please contact us today with any questions you have about our product. HealthPRO’s service and support team is happy to assist you with any concerns; contact us today for help or to learn more.@stop
@section('meta-keywords'){{'Contact,us,support,service,help,question,phone,number,email,address,learn,more,assist,assistance,HealthPRO.com,HealthPRO,pro,health,product'}}@stop
@section('content')
	<section class="contactus">
		<header class="contactus-header">
			<div class="contactus-header-body">
				<h1>CONTACT US</h1>
				<div class="contactus-buttons">
					<a href="/pricing" class="btn_round m-arrow m-blue  m-upper contactus-btn contactus-btn-compare-plans">COMPARE PLANS</a>
					<a class="btn_round m-arrow m-green m-upper contactus-btn contactus-btn-request-demo" data-toggle="popup" data-target="demo_request">REQUEST A DEMO</a>
				</div>
			</div>
		</header>
		<section class="contactus-details-wrapper">
			<div class="contactus-details">
				<div class="contactus-details-item contactus-details-call-us">
					<div class="contactus-details-icon contactus-details-call-us-icon"></div>
					<h3 class="contactus-details-title">
						CALL US
					</h3>
					<div class="contactus-details-text">
						<div>Sun-Thurs: 7am-3pm EST</div>
						<div class="">
							<div><b>SALES:</b> 1866- HEALTHPRO (432-5847)</div>
							<div><b>SUPPORT:</b> (424) 307 2000</div>
						</div>
					</div>
				</div>

				<div class="contactus-details-item contactus-details-email-us">
					<div class="contactus-details-icon contactus-details-email-us-icon"></div>
					<h3 class="contactus-details-title">EMAIL US</h3>
					<div class="contactus-details-text">support@healthpro.com</div>
				</div>
				<div class="contactus-details-item contactus-details-mail-address">
					<div class="contactus-details-icon contactus-details-mail-address-icon"></div>
					<h3 class="contactus-details-title">MAILING ADDRESS</h3>
					<div class="contactus-details-text">
						<div>HealthPRO Inc. 1712 Pioneer Ave.</div>
						<div>Suite 6030A</div>
						<div>Cheyenne, WY 82001 USA</div>
					</div>
				</div>
			</div>
		</section>
	<section class="contactus-form">
		<header class="contactus-form-header">
			<h2 class="contactus-form-header-title">CONTACT HEALTHPRO TODAY</h2>
			<div class="contactus-underline"></div>
		</header>
		<div class="contactus-content" data-cs-form="contact_us"></div>
	</section>
	</section>
@stop

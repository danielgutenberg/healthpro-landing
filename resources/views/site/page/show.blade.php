@extends('site.layouts.default')
@if ($page->slug == 'terms')
	@section('title')Terms and Conditions | HealthPRO @stop
	@section('meta-description')Please read HealthPRO’s terms and conditions carefully. HealthPRO’s terms and conditions are an agreement that defines the relationship between you and HealthPRO.@stop
	@section('meta-keywords'){{'terms,conditions,agreement,HealthPRO.com,HealthPRO,pro,health,policy,relationship,contract'}}@stop
@else
	@section('title')
		{{{ StringHelper::title($page->title) }}} ::
		@parent
	@stop
@endif

@section('content')
	<div class="page--text">
	<h1>{{ $page->title }}</h1>
	{!! $page->filteredContent() !!}
	</div>
@stop

<section class="home-signup partner-signup">
	<div class="home-signup--wrap">
		<div class="home-signup--col  home-signup--features partner-signup--features">
			<div class="home-signup--col partner-mobile-app">
				<img class="partner-mobile-app-img" alt="partner mobile app" src='{{ asset("assets/frontend/images/design/partner/{$partner_name}-phone.png") }}'/>
			</div>
			<div class="home-signup--col partner-text">
				<div class="headline m-l partner-title">{!! $text_title !!}</div>
				<p class="paragraph m-l m-more_space">{!! $text_paragraph_1 !!}</p>
				<p class="paragraph m-l m-more_space">{!! $text_paragraph_2 !!}</p>
				<p class="paragraph m-l m-more_space">{!! $text_paragraph_3 !!}</p>
				<div class="paragraph m-more_space">
				  <img alt="partner logo" src='{{asset("assets/frontend/images/design/partner/{$partner_name}-logo.png")}}' />
				</div>
			</div>
		</div>
		<!-- <div class="partner-signup-form-left-bg"></div> -->
		<div class="home-signup--col partner-signup--col home-signup--form partner-signup--form">
			@include('site.auth.partials.sign')
			<div class="home-signup--form_footer">
				Already have an account?
				<a href="{{route('auth-login')}}" data-toggle="popup" data-target="popup_auth" data-authpopup-toggle="login">Log In</a>
			</div>
		</div>
	</div>
</section>

<div class="home_platform" id="home-all_in_one">
	<i class="home_platform--shape"></i>
	<div class="home-professional--wrap">
		<div class="home_platform--content">
			<div class="home_platform--heading"><b>HealthPRO</b> is an all-in-one platform for your business</div>
			<ul class="home_platform--list">
				<li class="home_platform--list_item m-booking">Online Booking</li>
				<li class="home_platform--list_item m-payment">Payment Collection</li>
				<li class="home_platform--list_item m-calendar">Calendar Integration</li>
				<li class="home_platform--list_item m-messages">2-Way Messaging</li>
				<li class="home_platform--list_item m-reminders">Appointment Reminders</li>
				<li class="home_platform--list_item m-marketing_tools">Marketing Tools</li>
			</ul>
		</div>
	</div>
</div>

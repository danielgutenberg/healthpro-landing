<div class="promo_section m-light m-center m-onthego">
	<div class="promo_section--wrap">
		<div class="promo_heading">
			<div class="promo_heading--title">HealthPRO On The Go</div>
			<div class="promo_heading--description">
				<p>
					Managing Your Business Has <strong>Never Been Simpler</strong>
				</p>
			</div>
		</div>
		<div class="promo_section--screen">
			<div class="promo_screen m-onthego"></div>
		</div>
	</div>
</div>

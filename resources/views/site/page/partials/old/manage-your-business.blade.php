<div class="home_manage" id="home-manage-your-business">
	<div class="home-professional--wrap">
		<div class="home_manage--content">
			<div class="home_manage--heading">Manage Your Business with One Simple-to-Use Platform</div>
			<ul class="home_manage--list">
				<li class="home_manage--list_item m-how">
					<div class="home_manage--list_item--title">How it Works</div>
					<div class="home_manage--list_item--text">Create your account &amp; <span>Get started in 5 minutes</span></div>
					<a class="btn_frame m-green m-arrow m-upper" href="{{route('how-it-works')}}">Learn more</a>
				</li>
				<li class="home_manage--list_item m-features">
					<div class="home_manage--list_item--title">Features</div>
					<div class="home_manage--list_item--text">Seamless 2-Way calendar integration &amp; Trusted payment platform</div>
					<a class="btn_frame m-green m-arrow m-upper" href="{{route('features')}}">Learn more</a>
				</li>
				<li class="home_manage--list_item m-pricing">
					<div class="home_manage--list_item--title">Pricing</div>
					<div class="home_manage--list_item--text">No setup costs, No monthly fees. <span>We get paid, When you get paid.</span></div>
					<a class="btn_frame m-green m-arrow m-upper" href="{{route('pricing')}}">Learn more</a>
				</li>
			</ul>
		</div>
	</div>
</div>

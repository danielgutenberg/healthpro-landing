<div class="home-about" id="home-about">
	<div class="home-professional--wrap">
		<div class="home-about--content">
			<div class="home-professional--heading m-about">Learn More About HealthPRO</div>
			<div class="home-about--video">
				<a href="" class="home-about--video_link" data-toggle="popup" data-target="popup_home_video">
					<span>watch the video</span>
				</a>
			</div>
		</div>
	</div>
</div>

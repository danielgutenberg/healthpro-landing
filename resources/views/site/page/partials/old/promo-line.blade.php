<div class="promo_line">
	<ul class="promo_line--list">
		<li class="promo_line--list_item">No Monthly Fees</li>
		<li class="promo_line--list_item">No Commitment</li>
		<li class="promo_line--list_item">Payouts Within 72 Hours</li>
		<li class="promo_line--list_item">No Transfer Fee</li>
		<li class="promo_line--list_item">Max $150 Booking Fees per Month</li>
	</ul>
</div>

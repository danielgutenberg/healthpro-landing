<div class="promo_section m-alt m-trainer">
	<div class="promo_section--wrap">
		<div class="promo_section--content">
			<div class="promo_content m-payments">
				<div class="promo_content--title">Simple to get paid</div>
				<div class="promo_content--description">
					<p>Secure client payments direct to your account within 72 hours of the appointment.</p>
				</div>
				@if (! empty($showCta) and $showCta)
					<div class="promo_section--content--btn">
						@if (! empty($hubspotButton) and $hubspotButton)
							<?php
							// showing hubspot button depending on page
							// button #5
							switch ($hubspotButton) {
							case 'eblast-june-6':
								echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-cdd090ec-8420-4c9b-b329-cd53be483f0e">
								<span class="hs-cta-node hs-cta-cdd090ec-8420-4c9b-b329-cd53be483f0e" id="hs-cta-cdd090ec-8420-4c9b-b329-cd53be483f0e">
								<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
								<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/cdd090ec-8420-4c9b-b329-cd53be483f0e" ><img class="hs-cta-img" id="hs-cta-img-cdd090ec-8420-4c9b-b329-cd53be483f0e" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/cdd090ec-8420-4c9b-b329-cd53be483f0e.png" alt="Sign Up Now >"/></a>
								</span>
								<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
								<script type="text/javascript">
								hbspt.cta.load(496304, "cdd090ec-8420-4c9b-b329-cd53be483f0e", {});
								</script>
								</span>';
								break;
							case 'idea-perks':
								echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-ec9dfd5a-1184-4da0-a522-dadb4acd1533">
								<span class="hs-cta-node hs-cta-ec9dfd5a-1184-4da0-a522-dadb4acd1533" id="hs-cta-ec9dfd5a-1184-4da0-a522-dadb4acd1533">
								<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
								<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/ec9dfd5a-1184-4da0-a522-dadb4acd1533" ><img class="hs-cta-img" id="hs-cta-img-ec9dfd5a-1184-4da0-a522-dadb4acd1533" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/ec9dfd5a-1184-4da0-a522-dadb4acd1533.png" alt="Sign Up Now >"/></a>
								</span>
								<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
								<script type="text/javascript">
								hbspt.cta.load(496304, "ec9dfd5a-1184-4da0-a522-dadb4acd1533", {});
								</script>
								</span>';
								break;
							case 'idea-display-banner-june-2016':
								echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-3a4a0bff-92bb-443f-92a2-0def2584b20d">
								<span class="hs-cta-node hs-cta-3a4a0bff-92bb-443f-92a2-0def2584b20d" id="hs-cta-3a4a0bff-92bb-443f-92a2-0def2584b20d">
								<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
								<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/3a4a0bff-92bb-443f-92a2-0def2584b20d" ><img class="hs-cta-img" id="hs-cta-img-3a4a0bff-92bb-443f-92a2-0def2584b20d" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/3a4a0bff-92bb-443f-92a2-0def2584b20d.png" alt="Sign Up Now >"/></a>
								</span>
								<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
								<script type="text/javascript">
								hbspt.cta.load(496304, "3a4a0bff-92bb-443f-92a2-0def2584b20d", {});
								</script>
								</span>';
								break;
								break;
							case 'idea-mbw-banner-june-9-2016':
								echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-4a5c3f03-d3c1-4e78-991b-0a35f0480c79">
								<span class="hs-cta-node hs-cta-4a5c3f03-d3c1-4e78-991b-0a35f0480c79" id="hs-cta-4a5c3f03-d3c1-4e78-991b-0a35f0480c79">
								<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
								<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/4a5c3f03-d3c1-4e78-991b-0a35f0480c79" ><img class="hs-cta-img" id="hs-cta-img-4a5c3f03-d3c1-4e78-991b-0a35f0480c79" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/4a5c3f03-d3c1-4e78-991b-0a35f0480c79.png" alt="Sign Up Now >"/></a>
								</span>
								<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
								<script type="text/javascript">
								hbspt.cta.load(496304, "4a5c3f03-d3c1-4e78-991b-0a35f0480c79", {});
								</script>
								</span>';
								break;
							case 'idea-mbw-text-june-9-2016':
								echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-28107826-4c1b-4ae1-a76e-b7146f52043a">
								<span class="hs-cta-node hs-cta-28107826-4c1b-4ae1-a76e-b7146f52043a" id="hs-cta-28107826-4c1b-4ae1-a76e-b7146f52043a">
								<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
								<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/28107826-4c1b-4ae1-a76e-b7146f52043a" ><img class="hs-cta-img" id="hs-cta-img-28107826-4c1b-4ae1-a76e-b7146f52043a" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/28107826-4c1b-4ae1-a76e-b7146f52043a.png" alt="Sign Up Now >"/></a>
								</span>
								<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
								<script type="text/javascript">
								hbspt.cta.load(496304, "28107826-4c1b-4ae1-a76e-b7146f52043a", {});
								</script>
								</span>';
								break;
							case 'idea-fitness-tracker':
								echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-e194d13d-8b4f-449d-8f4a-b0d9c62b8edf">
								<span class="hs-cta-node hs-cta-e194d13d-8b4f-449d-8f4a-b0d9c62b8edf" id="hs-cta-e194d13d-8b4f-449d-8f4a-b0d9c62b8edf">
								<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
								<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/e194d13d-8b4f-449d-8f4a-b0d9c62b8edf" ><img class="hs-cta-img" id="hs-cta-img-e194d13d-8b4f-449d-8f4a-b0d9c62b8edf" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/e194d13d-8b4f-449d-8f4a-b0d9c62b8edf.png" alt="Sign Up Now >"/></a>
								</span>
								<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
								<script type="text/javascript">
								hbspt.cta.load(496304, "e194d13d-8b4f-449d-8f4a-b0d9c62b8edf", {});
								</script>
								</span>';
								break;
							}
							?>
						@else
							@if(Sentinel::check())
								<a href="{{route('dashboard-schedule')}}" class="cta_btn m-green">Get Started Now</a>
							@else
								<a href="{{route('auth-register')}}" target="" data-toggle="popup" data-target="popup_auth" data-authpopup-toggle="sign_up"  class="cta_btn m-green">Sign Up Now</a>
							@endif
						@endif
					</div>
				@endif
			</div>
		</div>
		<div class="promo_section--screen m-payments">
			<div class="promo_screen m-payments"></div>
		</div>
	</div>
</div>

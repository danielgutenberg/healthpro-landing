<div class="home-grow">
	<div class="home-grow--wrap">
		<h2 class="home-professional--heading m-grow">Grow Your Business Today</h2>
		<div class="home-grow--scene">
			<?php
				$linkAttr = 'href="#" data-toggle="popup" data-target="popup_home_tour"';
				$features = [
					[
						'slug' => 'get-discovered',
						'title' => 'Get Discovered',
						'text' => 'Clients can find you by searching for your service in your area.',
					],
					[
						'slug' => 'get-booked',
						'title' => 'Get Booked',
						'text' => 'Your clients can instantly book you online 24/7',
					],
					[
						'slug' => 'get-paid',
						'title' => 'Get Paid',
						'text' => 'Hassle-free billing. Seamless payout process. Accept payments online.',
					],
					[
						'slug' => 'get-reviewed',
						'title' => 'Get Reviewed',
						'text' => 'Verified reviews can only be left by real customers.',
					],
				];
			?>
			<ul class="home-grow--features">
				@foreach($features as $k => $feature)
					<li class="home-grow--feature">
						@if (! empty($growLinksStay) and $growLinksStay)
							<a class="home-grow--feature-link" data-feature="{{$feature['slug']}}" data-scroll="#how_it_works_profile">
						@else
							<a {!! $linkAttr !!} class="home-grow--feature-link" data-feature="{{$feature['slug']}}">
						@endif
							<span class="home-grow--feature-image m-{{$feature['slug']}}">
								<span class="home-grow--feature-count">{{$k+1}}</span>
							</span>
								<h4 class="home-grow--feature-title">{{$feature['title']}}</h4>
								<p class="home-grow--feature-description">{{$feature['text']}}</p>
							<span class="home-grow--feature-btn">
								<span class="cta_btn m-arrow">How It Works</span>
							</span>
						</a>
					</li>
				@endforeach
			</ul>
			<div class="home-grow--phone">
				@foreach($features as $feature)
					<i class="home-grow--phone-inside m-{{$feature['slug']}}"></i>
				@endforeach
			</div>
		</div>
		@if (! empty($showCta) and $showCta)
			<div class="home-grow--btn">
				@if(Sentinel::check())
					<a href="{{route('dashboard-schedule')}}" class="cta_btn m-green m-larger m-arrow">Get Started Now</a>
				@else
					<a href="{{route('auth-register')}}" target="" data-toggle="popup" data-target="popup_auth" data-authpopup-toggle="sign_up"  class="cta_btn m-green m-larger m-arrow">Sign Up Now</a>
				@endif
			</div>
		@endif
		{{--<div class="home-grow--btn">--}}
			{{--<a href="#" class="cta_btn m-blue">See our feature list</a>--}}
		{{--</div>--}}
	</div>
</div>

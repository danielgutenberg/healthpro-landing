<div class="promo_hero m-pricing {{$sectionClass or ''}}">
	<div class="promo_hero--inner wrap">
		<div class="promo_hero--heading {{$headingClass or ''}}">{!! $heading or 'HealthPRO Only Charges a Flat <span>2% Booking Fee</span>' !!}</div>
		<div class="promo_hero--description {{$descriptionClass or ''}}">
			<p>{!! $description or 'You get paid on time, every time, stress free!' !!}</p>
		</div>
		<div class="promo_hero--cta">
			@if (! empty($hubspotButton) and $hubspotButton)
				<?php
				// showing hubspot button depending on page
				// button #7
				switch ($hubspotButton) {
				case 'eblast-june-6':
					echo '<span class="hs-cta-wrapper m-larger m-arrow" id="hs-cta-wrapper-58a8eb8f-801f-4ea4-9941-7d4cb96b8afd">
					<span class="hs-cta-node hs-cta-58a8eb8f-801f-4ea4-9941-7d4cb96b8afd" id="hs-cta-58a8eb8f-801f-4ea4-9941-7d4cb96b8afd">
					<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
					<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/58a8eb8f-801f-4ea4-9941-7d4cb96b8afd" ><img class="hs-cta-img" id="hs-cta-img-58a8eb8f-801f-4ea4-9941-7d4cb96b8afd" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/58a8eb8f-801f-4ea4-9941-7d4cb96b8afd.png" alt="Sign Up Now >"/></a>
					</span>
					<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					<script type="text/javascript">
					hbspt.cta.load(496304, "58a8eb8f-801f-4ea4-9941-7d4cb96b8afd", {});
					</script>
					</span>';
					break;
				case 'idea-perks':
					echo '<span class="hs-cta-wrapper m-larger m-arrow" id="hs-cta-wrapper-0535a934-6a1c-47a5-b24d-2c8d38e7605e">
					<span class="hs-cta-node hs-cta-0535a934-6a1c-47a5-b24d-2c8d38e7605e" id="hs-cta-0535a934-6a1c-47a5-b24d-2c8d38e7605e">
					<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
					<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/0535a934-6a1c-47a5-b24d-2c8d38e7605e" ><img class="hs-cta-img" id="hs-cta-img-0535a934-6a1c-47a5-b24d-2c8d38e7605e" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/0535a934-6a1c-47a5-b24d-2c8d38e7605e.png" alt="Sign Up Now >"/></a>
					</span>
					<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					<script type="text/javascript">
					hbspt.cta.load(496304, "0535a934-6a1c-47a5-b24d-2c8d38e7605e", {});
					</script>
					</span>';
					break;
				case 'idea-display-banner-june-2016':
					echo '<span class="hs-cta-wrapper m-larger m-arrow" id="hs-cta-wrapper-b1af40e4-a4b0-496c-9453-8eef623c7a78">
					<span class="hs-cta-node hs-cta-b1af40e4-a4b0-496c-9453-8eef623c7a78" id="hs-cta-b1af40e4-a4b0-496c-9453-8eef623c7a78">
					<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
					<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/b1af40e4-a4b0-496c-9453-8eef623c7a78" ><img class="hs-cta-img" id="hs-cta-img-b1af40e4-a4b0-496c-9453-8eef623c7a78" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/b1af40e4-a4b0-496c-9453-8eef623c7a78.png" alt="Sign Up Now >"/></a>
					</span>
					<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					<script type="text/javascript">
					hbspt.cta.load(496304, "b1af40e4-a4b0-496c-9453-8eef623c7a78", {});
					</script>
					</span>';
					break;
				case 'idea-mbw-banner-june-9-2016':
					echo '<span class="hs-cta-wrapper m-larger m-arrow" id="hs-cta-wrapper-1ef662c7-2fb9-4426-808f-59936cac7ac0">
					<span class="hs-cta-node hs-cta-1ef662c7-2fb9-4426-808f-59936cac7ac0" id="hs-cta-1ef662c7-2fb9-4426-808f-59936cac7ac0">
					<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
					<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/1ef662c7-2fb9-4426-808f-59936cac7ac0" ><img class="hs-cta-img" id="hs-cta-img-1ef662c7-2fb9-4426-808f-59936cac7ac0" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/1ef662c7-2fb9-4426-808f-59936cac7ac0.png" alt="Sign Up Now >"/></a>
					</span>
					<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					<script type="text/javascript">
					hbspt.cta.load(496304, "1ef662c7-2fb9-4426-808f-59936cac7ac0", {});
					</script>
					</span>';
					break;
				case 'idea-mbw-text-june-9-2016':
					echo '<span class="hs-cta-wrapper m-larger m-arrow" id="hs-cta-wrapper-3ccc85cd-0db6-4722-9693-386682186b60">
					<span class="hs-cta-node hs-cta-3ccc85cd-0db6-4722-9693-386682186b60" id="hs-cta-3ccc85cd-0db6-4722-9693-386682186b60">
					<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
					<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/3ccc85cd-0db6-4722-9693-386682186b60" ><img class="hs-cta-img" id="hs-cta-img-3ccc85cd-0db6-4722-9693-386682186b60" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/3ccc85cd-0db6-4722-9693-386682186b60.png" alt="Sign Up Now >"/></a>
					</span>
					<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					<script type="text/javascript">
					hbspt.cta.load(496304, "3ccc85cd-0db6-4722-9693-386682186b60", {});
					</script>
					</span>';
					break;
				case 'idea-fitness-tracker':
					echo '<span class="hs-cta-wrapper m-larger m-arrow" id="hs-cta-wrapper-aa633cfd-97bc-4573-a3ab-bc0f28ec555b">
					<span class="hs-cta-node hs-cta-aa633cfd-97bc-4573-a3ab-bc0f28ec555b" id="hs-cta-aa633cfd-97bc-4573-a3ab-bc0f28ec555b">
					<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
					<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/aa633cfd-97bc-4573-a3ab-bc0f28ec555b" ><img class="hs-cta-img" id="hs-cta-img-aa633cfd-97bc-4573-a3ab-bc0f28ec555b" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/aa633cfd-97bc-4573-a3ab-bc0f28ec555b.png" alt="Sign Up Now >"/></a>
					</span>
					<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					<script type="text/javascript">
					hbspt.cta.load(496304, "aa633cfd-97bc-4573-a3ab-bc0f28ec555b", {});
					</script>
					</span>';
					break;
				}
				?>
			@else
				@if(Sentinel::check())
					<a href="{{route('dashboard-schedule')}}" class="cta_btn m-green m-larger m-arrow">Get Started Now</a>
				@else
					<a href="{{route('auth-register')}}" target="" data-toggle="popup" data-target="popup_auth" data-authpopup-toggle="sign_up"  class="cta_btn m-green m-larger m-arrow">Sign Up Now</a>
				@endif
			@endif
		</div>
	</div>
</div>

<div class="home_advantages" id="home-simplifies">
	<div class="home-professional--wrap">
		<div class="home_advantages--content">
			<div class="home_advantages--heading"><b>HealthPRO</b> simplifies your business</div>
			<ul class="home_advantages--list">
				<li class="home_advantages--list_item">Clients book their own appointments</li>
				<li class="home_advantages--list_item">No more constant callbacks</li>
				<li class="home_advantages--list_item">Automated reminders</li>
				<li class="home_advantages--list_item">Get paid on time, every time</li>
				<li class="home_advantages--list_item">Never lose money on last minute cancellations</li>
			</ul>
			@if(Sentinel::check())
				<a href="{{route('dashboard-schedule')}}" class="btn_multiline m-arrow">
					Get Started
					<small>No Setup Costs, No Monthly Fees</small>
				</a>
			@else
				<a href="{{route('auth-register')}}" target="" data-toggle="popup" data-target="popup_auth" data-authpopup-toggle="sign_up"  class="btn_multiline m-arrow">
					Get Started
					<small>No Setup Costs, No Monthly Fees</small>
				</a>
			@endif
		</div>
	</div>
</div>

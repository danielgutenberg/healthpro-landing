<div class="promo_section m-light">
	<div class="promo_section--wrap">
		<div class="promo_section--content">
			<div class="promo_content m-schedule">
				<div class="promo_content--title">Simple to set up your schedule</div>
				<div class="promo_content--description">
					<p>Set up your availability in minutes. Reschedule, cancel or message your clients with any last minute updates.</p>
				</div>
			</div>
			@if (! empty($showCta) and $showCta)
				<div class="promo_section--content--btn">
					@if (! empty($hubspotButton) and $hubspotButton)
						<?php
						// showing hubspot button depending on page
						// button #4
						switch ($hubspotButton) {
						case 'eblast-june-6':
							echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-ff7e0fb7-8f52-4b60-bc51-bcce2d6b6333">
							<span class="hs-cta-node hs-cta-ff7e0fb7-8f52-4b60-bc51-bcce2d6b6333" id="hs-cta-ff7e0fb7-8f52-4b60-bc51-bcce2d6b6333">
							<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
							<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/ff7e0fb7-8f52-4b60-bc51-bcce2d6b6333" ><img class="hs-cta-img" id="hs-cta-img-ff7e0fb7-8f52-4b60-bc51-bcce2d6b6333" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/ff7e0fb7-8f52-4b60-bc51-bcce2d6b6333.png" alt="Sign Up Now >"/></a>
							</span>
							<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
							<script type="text/javascript">
							hbspt.cta.load(496304, "ff7e0fb7-8f52-4b60-bc51-bcce2d6b6333", {});
							</script>
							</span>';
							break;
						case 'idea-perks':
							echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-0329b77b-899e-450b-9b12-d77e7be0c431">
							<span class="hs-cta-node hs-cta-0329b77b-899e-450b-9b12-d77e7be0c431" id="hs-cta-0329b77b-899e-450b-9b12-d77e7be0c431">
							<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
							<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/0329b77b-899e-450b-9b12-d77e7be0c431" ><img class="hs-cta-img" id="hs-cta-img-0329b77b-899e-450b-9b12-d77e7be0c431" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/0329b77b-899e-450b-9b12-d77e7be0c431.png" alt="Sign Up Now >"/></a>
							</span>
							<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
							<script type="text/javascript">
							hbspt.cta.load(496304, "0329b77b-899e-450b-9b12-d77e7be0c431", {});
							</script>
							</span>';
							break;
						case 'idea-display-banner-june-2016':
							echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-4a8e5437-1255-4c00-97cd-7f80daf663f8">
							<span class="hs-cta-node hs-cta-4a8e5437-1255-4c00-97cd-7f80daf663f8" id="hs-cta-4a8e5437-1255-4c00-97cd-7f80daf663f8">
							<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
							<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/4a8e5437-1255-4c00-97cd-7f80daf663f8" ><img class="hs-cta-img" id="hs-cta-img-4a8e5437-1255-4c00-97cd-7f80daf663f8" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/4a8e5437-1255-4c00-97cd-7f80daf663f8.png" alt="Sign Up Now >"/></a>
							</span>
							<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
							<script type="text/javascript">
							hbspt.cta.load(496304, "4a8e5437-1255-4c00-97cd-7f80daf663f8", {});
							</script>
							</span>';
							break;
						case 'idea-mbw-banner-june-9-2016':
							echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-a83f2554-408f-4c6a-99a5-878ac1d7d365">
							<span class="hs-cta-node hs-cta-a83f2554-408f-4c6a-99a5-878ac1d7d365" id="hs-cta-a83f2554-408f-4c6a-99a5-878ac1d7d365">
							<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
							<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/a83f2554-408f-4c6a-99a5-878ac1d7d365" ><img class="hs-cta-img" id="hs-cta-img-a83f2554-408f-4c6a-99a5-878ac1d7d365" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/a83f2554-408f-4c6a-99a5-878ac1d7d365.png" alt="Sign Up Now >"/></a>
							</span>
							<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
							<script type="text/javascript">
							hbspt.cta.load(496304, "a83f2554-408f-4c6a-99a5-878ac1d7d365", {});
							</script>
							</span>';
							break;
						case 'idea-mbw-text-june-9-2016':
							echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-4c4bbb83-a885-46dd-b90f-e1714f2104bf">
							<span class="hs-cta-node hs-cta-4c4bbb83-a885-46dd-b90f-e1714f2104bf" id="hs-cta-4c4bbb83-a885-46dd-b90f-e1714f2104bf">
							<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
							<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/4c4bbb83-a885-46dd-b90f-e1714f2104bf" ><img class="hs-cta-img" id="hs-cta-img-4c4bbb83-a885-46dd-b90f-e1714f2104bf" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/4c4bbb83-a885-46dd-b90f-e1714f2104bf.png" alt="Sign Up Now >"/></a>
							</span>
							<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
							<script type="text/javascript">
							hbspt.cta.load(496304, "4c4bbb83-a885-46dd-b90f-e1714f2104bf", {});
							</script>
							</span>';
							break;
						case 'idea-fitness-tracker':
							echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-c158f939-4798-4988-853d-c8b19f4254c3">
							<span class="hs-cta-node hs-cta-c158f939-4798-4988-853d-c8b19f4254c3" id="hs-cta-c158f939-4798-4988-853d-c8b19f4254c3">
							<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
							<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/c158f939-4798-4988-853d-c8b19f4254c3" ><img class="hs-cta-img" id="hs-cta-img-c158f939-4798-4988-853d-c8b19f4254c3" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/c158f939-4798-4988-853d-c8b19f4254c3.png" alt="Sign Up Now >"/></a>
							</span>
							<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
							<script type="text/javascript">
							hbspt.cta.load(496304, "c158f939-4798-4988-853d-c8b19f4254c3", {});
							</script>
							</span>';
							break;
						}
						?>
					@else
						@if(Sentinel::check())
							<a href="{{route('dashboard-schedule')}}" class="cta_btn m-green">Get Started Now</a>
						@else
							<a href="{{route('auth-register')}}" target="" data-toggle="popup" data-target="popup_auth" data-authpopup-toggle="sign_up"  class="cta_btn m-green">Sign Up Now</a>
						@endif
					@endif
				</div>
			@endif
		</div>
		<div class="promo_section--screen">
			<div class="promo_screen m-schedule"></div>
		</div>
	</div>
</div>

<div class="promo_section m-light">
	<div class="promo_section--wrap">
		<div class="promo_heading m-distance_small">
			<div class="promo_heading--title">Payout Scenarios</div>
		</div>
		<div class="promo_payout">
			<?php
				$payouts = [
					[
						'$2,500',
						'$50',
						'$2,450',
					],
					[
						'$5,000',
						'$100',
						'$4,900',
					],
					[
						'$7,500',
						'$150',
						'$7,350',
					],
					[
						'$10,000',
						'$150',
						'$9,850',
					],
				];
			?>

			<table>
				<thead>
					<tr>
						<th>Monthly Revenue</th>
						<th>HealthPRO 2% Booking Fee</th>
						<th>You Earn</th>
					</tr>
				</thead>
				<tbody>
					@foreach($payouts as $k => $payout)
						<tr>
							<td><span>{{$payout[0]}}</span></td>
							<td><span class="{{ count($payouts) == $k+1 ? 'm-max' : '' }}">{{$payout[1]}}</span></td>
							<th><span>{{$payout[2]}}</span></th>
						</tr>
					@endforeach
				</tbody>
			</table>
			<div class="promo_payout--powered">
				<i></i>
				<i></i>
			</div>
		</div>
	</div>
</div>

<div class="promo_section m-alt" id="how_it_works_profile">
	<div class="promo_section--wrap">

		<div class="promo_heading m-distance_small">
			<h2 class="promo_heading--title">How It Works</h2>
		</div>
		<div class="promo_section--content">
			<div class="promo_content m-build-profile">
				<h3 class="promo_content--title">Simple to build a profile</h3>
				<div class="promo_content--description">
					<p>Create your online presence in just five minutes so clients can discover and book you. We will showcase your services, rates and business profile to potential clients.</p>
				</div>
			</div>
			@if (! empty($showCta) and $showCta)
				<div class="promo_section--content--btn">
					@if(Sentinel::check())
						<a href="{{route('dashboard-schedule')}}" class="cta_btn m-green">Get Started Now</a>
					@else
						<a href="{{route('auth-register')}}" target="" data-toggle="popup" data-target="popup_auth" data-authpopup-toggle="sign_up"  class="cta_btn m-green">Sign Up Now</a>
					@endif
				</div>
			@endif
		</div>
		<div class="promo_section--screen">
			<div class="promo_screen m-build-profile"></div>
		</div>
	</div>
</div>

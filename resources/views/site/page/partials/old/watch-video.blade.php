<div class="home_watch" id="home-watch">
	<div class="home-professional--wrap">
		<div class="home_watch--content">
			<div class="home_watch--heading"><b>HealthPRO</b> takes the weight of scheduling, billing and promoting off your shoulders</div>
			<button class="home_watch--cta_link" data-toggle="popup" data-target="popup_home_video">Watch to learn more</button>
			<div class="home_watch--video">
				<button class="home_watch--video_link" data-toggle="popup" data-target="popup_home_video">
					<span>watch the video</span>
				</button>
			</div>
		</div>
		<a href="#home-testimonials" class="home_watch--scroll" data-scroll-offset="25" data-scroll="#home-testimonials"></a>
	</div>
</div>

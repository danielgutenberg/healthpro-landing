<div class="promo_section m-get_started {{$sectionClass or ''}}">
	<div class="promo_section--wrap">
		<div class="promo_heading m-has_tooltips">
			<div class="promo_heading--title">Ready To Get Started?</div>
			<div class="promo_heading--description">
				@if( ! empty($textLines))
					@foreach($textLines as $textLine)
						<div class="promo_heading--description--line">
							@if( ! empty($textLine['content']))
								<p>{{$textLine['content']}}</p>
							@endif
							@if( ! empty($textLine['tooltip']))
								<em>{{$textLine['tooltip']}}</em>
							@endif
						</div>
					@endforeach
				@endif
			</div>
		</div>
		<div class="promo_section--cta">
			@if (! empty($hubspotButton) and $hubspotButton)
				<?php
				// showing hubspot button depending on page
				// button #8
				switch ($hubspotButton) {
				case 'eblast-june-6':
					echo '<span class="hs-cta-wrapper m-larger m-arrow" id="hs-cta-wrapper-8ca87a9d-c127-43db-8aaa-8844ab6997bd">
					<span class="hs-cta-node hs-cta-8ca87a9d-c127-43db-8aaa-8844ab6997bd" id="hs-cta-8ca87a9d-c127-43db-8aaa-8844ab6997bd">
					<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
					<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/8ca87a9d-c127-43db-8aaa-8844ab6997bd" ><img class="hs-cta-img" id="hs-cta-img-8ca87a9d-c127-43db-8aaa-8844ab6997bd" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/8ca87a9d-c127-43db-8aaa-8844ab6997bd.png" alt="Sign Up Now >"/></a>
					</span>
					<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					<script type="text/javascript">
					hbspt.cta.load(496304, "8ca87a9d-c127-43db-8aaa-8844ab6997bd", {});
					</script>
					</span>';
					break;
				case 'idea-perks':
					echo '<span class="hs-cta-wrapper m-larger m-arrow" id="hs-cta-wrapper-7b5a85d2-9faa-4873-82a5-69c6dc52f227">
					<span class="hs-cta-node hs-cta-7b5a85d2-9faa-4873-82a5-69c6dc52f227" id="hs-cta-7b5a85d2-9faa-4873-82a5-69c6dc52f227">
					<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
					<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/7b5a85d2-9faa-4873-82a5-69c6dc52f227" ><img class="hs-cta-img" id="hs-cta-img-7b5a85d2-9faa-4873-82a5-69c6dc52f227" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/7b5a85d2-9faa-4873-82a5-69c6dc52f227.png" alt="Sign Up Now >"/></a>
					</span>
					<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					<script type="text/javascript">
					hbspt.cta.load(496304, "7b5a85d2-9faa-4873-82a5-69c6dc52f227", {});
					</script>
					</span>';
					break;
				case 'idea-display-banner-june-2016':
					echo '<span class="hs-cta-wrapper m-larger m-arrow" id="hs-cta-wrapper-7d1fa032-c393-4e1e-84fe-7f8144690c6a">
					<span class="hs-cta-node hs-cta-7d1fa032-c393-4e1e-84fe-7f8144690c6a" id="hs-cta-7d1fa032-c393-4e1e-84fe-7f8144690c6a">
					<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
					<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/7d1fa032-c393-4e1e-84fe-7f8144690c6a" ><img class="hs-cta-img" id="hs-cta-img-7d1fa032-c393-4e1e-84fe-7f8144690c6a" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/7d1fa032-c393-4e1e-84fe-7f8144690c6a.png" alt="Sign Up Now >"/></a>
					</span>
					<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					<script type="text/javascript">
					hbspt.cta.load(496304, "7d1fa032-c393-4e1e-84fe-7f8144690c6a", {});
					</script>
					</span>';
					break;
				case 'idea-mbw-banner-june-9-2016':
					echo '<span class="hs-cta-wrapper m-larger m-arrow" id="hs-cta-wrapper-0f32c8f1-b6d6-449a-b407-1173fcb3a9d0">
					<span class="hs-cta-node hs-cta-0f32c8f1-b6d6-449a-b407-1173fcb3a9d0" id="hs-cta-0f32c8f1-b6d6-449a-b407-1173fcb3a9d0">
					<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
					<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/0f32c8f1-b6d6-449a-b407-1173fcb3a9d0" ><img class="hs-cta-img" id="hs-cta-img-0f32c8f1-b6d6-449a-b407-1173fcb3a9d0" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/0f32c8f1-b6d6-449a-b407-1173fcb3a9d0.png" alt="Sign Up Now >"/></a>
					</span>
					<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					<script type="text/javascript">
					hbspt.cta.load(496304, "0f32c8f1-b6d6-449a-b407-1173fcb3a9d0", {});
					</script>
					</span>';
					break;
				case 'idea-mbw-text-june-9-2016':
					echo '<span class="hs-cta-wrapper m-larger m-arrow" id="hs-cta-wrapper-2e43df2f-d30c-4118-8856-b747af9ba090">
					<span class="hs-cta-node hs-cta-2e43df2f-d30c-4118-8856-b747af9ba090" id="hs-cta-2e43df2f-d30c-4118-8856-b747af9ba090">
					<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
					<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/2e43df2f-d30c-4118-8856-b747af9ba090" ><img class="hs-cta-img" id="hs-cta-img-2e43df2f-d30c-4118-8856-b747af9ba090" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/2e43df2f-d30c-4118-8856-b747af9ba090.png" alt="Sign Up Now >"/></a>
					</span>
					<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					<script type="text/javascript">
					hbspt.cta.load(496304, "2e43df2f-d30c-4118-8856-b747af9ba090", {});
					</script>
					</span>';
					break;
				case 'idea-fitness-tracker':
					echo '<span class="hs-cta-wrapper m-larger m-arrow" id="hs-cta-wrapper-212acd0a-1458-431e-bcf3-0ad898f50e97">
					<span class="hs-cta-node hs-cta-212acd0a-1458-431e-bcf3-0ad898f50e97" id="hs-cta-212acd0a-1458-431e-bcf3-0ad898f50e97">
					<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
					<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/212acd0a-1458-431e-bcf3-0ad898f50e97" ><img class="hs-cta-img" id="hs-cta-img-212acd0a-1458-431e-bcf3-0ad898f50e97" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/212acd0a-1458-431e-bcf3-0ad898f50e97.png" alt="Sign Up Now >"/></a>
					</span>
					<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					<script type="text/javascript">
					hbspt.cta.load(496304, "212acd0a-1458-431e-bcf3-0ad898f50e97", {});
					</script>
					</span>';
					break;
				}
				?>
			@else
				@if(Sentinel::check())
					<a href="{{route('dashboard-schedule')}}" class="cta_btn m-green m-larger m-arrow">Get Started Now</a>
				@else
					<a href="{{route('auth-register')}}" target="" data-toggle="popup" data-target="popup_auth" data-authpopup-toggle="sign_up"  class="cta_btn m-green m-larger m-arrow">Sign Up Now</a>
				@endif
			@endif
		</div>
	</div>
</div>

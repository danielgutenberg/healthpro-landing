<div class="promo_section">
	<div class="promo_section--wrap">
		<div class="promo_heading m-narrow m-distance_medium">
			<div class="promo_heading--title">How You Get Paid</div>
			<div class="promo_heading--description">
				<p>HealthPRO has integrated with StripeTM to provide a seamless and secure payments solution.</p>
			</div>
		</div>
		<div class="promo_get_paid">
			<div class="promo_get_paid--stripe">
				<a href="#" data-toggle="popup" data-target="popup_pricing_stripe">What is <span>Stripe</span> ?</a>
			</div>
			<div class="promo_get_paid--list_wrap">
				<ul class="promo_get_paid--list">
					<li class="promo_get_paid--list_item">
						<span class="promo_get_paid--list_item_image m-booking"></span>
						<h4 class="promo_get_paid--list_item_title">
							Client books an appointment - Card is authorized
						</h4>
					</li>
					<li class="promo_get_paid--list_item">
						<span class="promo_get_paid--list_item_image m-calendar"></span>
						<h4 class="promo_get_paid--list_item_title">
							24 hours prior to an appointment - Client card is charged
						</h4>
					</li>
					<li class="promo_get_paid--list_item">
						<span class="promo_get_paid--list_item_image m-payment"></span>
						<h4 class="promo_get_paid--list_item_title">
							within 72 hours of a payment - Money is deposited in your account
						</h4>
					</li>
				</ul>
				<i class="promo_get_paid--list_lock"></i>
				<i class="promo_get_paid--list_lock m-last"></i>
			</div>
		</div>
	</div>
</div>

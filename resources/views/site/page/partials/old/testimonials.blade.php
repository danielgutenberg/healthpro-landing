<?php
	$linkAttr = 'href="#" data-toggle="popup" data-target="popup_home_tour"';
	$testimonials = [
		[
			'quote' => 'This is very intuitive and easy to use',
			'text' => 'It was really fun to do a session and then see the $ showed up in my bank account like magic! This is very intuitive and easy to use. The tribe at HealthPro are serious about developing software that works for everyone and have taken our feedback to heart. They are developing solutions as we come up against limitations in the software and we look forward to seeing the unveiling of each new feature!',
			'person' =>
				[
					'pic' => 'assets/frontend/images/design/testimonials/michael.png',
					'who' => 'Michael Lee',
					'company' => 'Phoenix Rising Therapeutic Life Mentoring',
				]
		],
		[
			'quote' => 'Everything I need appears to be included in HealthPRO',
			'text' => 'I\'m new to the industry so I\'ve been hunting for the right software to manage/automate my appointments. Everything I need appears to be included in HealthPRO. If I think of something that would make things run smoother they jump into action to try to put it in the works. I can\'t wait until I start bringing in clients!',
			'person' =>
				[
					'pic' => 'assets/frontend/images/design/testimonials/cassandra.png',
					'who' => 'Cassandra Schmigotzki',
					'company' => 'The Long and Winding Road to Wellness',
				]
		],
		[
			'quote' => 'A seamless and easy to use platform',
			'text' => 'My experience with HealthPro has been fantastic. A seamless and easy to use platform that is of great value in providing health and wellness services widely! Join and take your practice to the next level!',
			'person' =>
				[
					'pic' => 'assets/frontend/images/design/testimonials/debbie.png',
					'who' => 'Debbie Novick',
					'company' => 'Novick Integrated Medicine',
				]
		],
		[
			'quote' => 'The platform is user-friendly and has much-needed tools',
			'text' => 'HealthPRO is a great way for health and wellness professionals to manage their business. The platform is user-friendly and has much-needed tools to help professionals from start to finish - everything from attracting and booking clients to accepting payment online.',
			'person' =>
				[
					'pic' => 'assets/frontend/images/design/testimonials/jason.png',
					'who' => 'Jason Davis, head of product development',
					'company' => 'IDEA Health &amp; Fitness Association',
				]
		],
		[
			'quote' => 'It’s the perfect simplified intuitive solution for wellness professionals',
			'text' => 'HealthPRO is a simple to use and essential business solution we recommend to all our health practitioner and fitness professional clients. The reason we recommend our clients to HealthPRO is that it’s a way for our clients to locally market themselves, easily get bookings and manage many aspects of their business all in one online platform. It’s the perfect simplified intuitive solution for wellness professionals.',
			'person' =>
				[
					'pic' => 'assets/frontend/images/design/testimonials/jennifer.png',
					'who' => 'Jennifer Dickens',
					'company' => 'Health &amp; Wellness Marketing',
				]
		],
		[
			'quote' => 'We love the suite of promotional tools offered to small business owners.',
			'text' => 'One of the "go-to" solutions we strongly encourage our clients to use is HealthPRO. We especially love the suite of promotional tools offered to small business owners. It\'s like having your own mini website with a place to showcase your work history, specialties, certifications and social networks. Even videos, photos and other media can be uploaded to your profile. If a potential client thinks your profile is a good fit, they can book an appointment with you directly from your profile. Very handy indeed.',
			'person' =>
				[
					'pic' => 'assets/frontend/images/design/testimonials/kim.png',
					'who' => 'Kim Peterson, Founder &amp; CEO',
					'company' => 'Common Sense Health',
				]
		],
	];
?>

<div class="testimonials_container" id="home-testimonials">
	<div class="testimonials--heading">Testimonials</div>
	<div class="testimonials @if (! empty($showCta) and $showCta) m-has_cta @endif">
		@foreach($testimonials as $testimonial)
			<div class="testimonials--item">
				<div class="testimonials--quote">“{{$testimonial['quote']}}”</div>
				<div class="testimonials--text">
					<p>“{{$testimonial['text']}}”</p>
				</div>
				<div class="testimonials--person">
					<figure class="testimonials--person--pic"><img src="{{ asset( $testimonial['person']['pic'] ) }}" alt="{{ $testimonial['person']['who'] }}" width="97"></figure>
					<div class="testimonials--person--content">
						<div class="testimonials--person--who">{{ $testimonial['person']['who'] }}</div>
						<div class="testimonials--person--company">{{ $testimonial['person']['company'] }}</div>
					</div>
				</div>
			</div>
		@endforeach
	</div>
	@if (! empty($showCta) and $showCta)
		<div class="testimonials--footer">
			@if(Sentinel::check())
				<a href="{{route('dashboard-schedule')}}" class="testimonials--btn cta_btn m-red m-arrow">Get Started</a>
			@else
				<a href="{{route('pricing')}}" class="testimonials--btn cta_btn m-red m-arrow">Get Started</a>
			@endif
		</div>
	@endif
</div>

<div class="promo_section m-light">
	<div class="promo_section--wrap">
		<div class="promo_section--content">
			<div class="promo_content m-messaging">
				<div class="promo_content--title">Simple to keep in touch</div>
				<div class="promo_content--description">
					<p>HealthPROs instant two-way-messaging makes it super easy to communicate with your clients any time, any where, any place.</p>
				</div>
			</div>
			@if (! empty($showCta) and $showCta)
				<div class="promo_section--content--btn">
					@if (! empty($hubspotButton) and $hubspotButton)
						<?php
						// showing hubspot button depending on page
						// button #6
						switch ($hubspotButton) {
						case 'eblast-june-6':
							echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-9d6fd1be-d43e-442b-b80f-1591cafe362a">
							<span class="hs-cta-node hs-cta-9d6fd1be-d43e-442b-b80f-1591cafe362a" id="hs-cta-9d6fd1be-d43e-442b-b80f-1591cafe362a">
							<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
							<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/9d6fd1be-d43e-442b-b80f-1591cafe362a" ><img class="hs-cta-img" id="hs-cta-img-9d6fd1be-d43e-442b-b80f-1591cafe362a" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/9d6fd1be-d43e-442b-b80f-1591cafe362a.png" alt="Sign Up Now >"/></a>
							</span>
							<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
							<script type="text/javascript">
							hbspt.cta.load(496304, "9d6fd1be-d43e-442b-b80f-1591cafe362a", {});
							</script>
							</span>';
							break;
						case 'idea-perks':
							echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-836b8623-1265-4af6-9d18-a3ad0d380a01">
							<span class="hs-cta-node hs-cta-836b8623-1265-4af6-9d18-a3ad0d380a01" id="hs-cta-836b8623-1265-4af6-9d18-a3ad0d380a01">
							<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
							<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/836b8623-1265-4af6-9d18-a3ad0d380a01" ><img class="hs-cta-img" id="hs-cta-img-836b8623-1265-4af6-9d18-a3ad0d380a01" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/836b8623-1265-4af6-9d18-a3ad0d380a01.png" alt="Sign Up Now >"/></a>
							</span>
							<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
							<script type="text/javascript">
							hbspt.cta.load(496304, "836b8623-1265-4af6-9d18-a3ad0d380a01", {});
							</script>
							</span>';
							break;
						case 'idea-display-banner-june-2016':
							echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-a1705594-ddc9-4b46-97fb-0d3631baa6a3">
							<span class="hs-cta-node hs-cta-a1705594-ddc9-4b46-97fb-0d3631baa6a3" id="hs-cta-a1705594-ddc9-4b46-97fb-0d3631baa6a3">
							<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
							<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/a1705594-ddc9-4b46-97fb-0d3631baa6a3" ><img class="hs-cta-img" id="hs-cta-img-a1705594-ddc9-4b46-97fb-0d3631baa6a3" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/a1705594-ddc9-4b46-97fb-0d3631baa6a3.png" alt="Sign Up Now >"/></a>
							</span>
							<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
							<script type="text/javascript">
							hbspt.cta.load(496304, "a1705594-ddc9-4b46-97fb-0d3631baa6a3", {});
							</script>
							</span>';
							break;
							break;
						case 'idea-mbw-banner-june-9-2016':
							echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-1d096dc0-92c7-4f86-bb54-ca71dc78c504">
							<span class="hs-cta-node hs-cta-1d096dc0-92c7-4f86-bb54-ca71dc78c504" id="hs-cta-1d096dc0-92c7-4f86-bb54-ca71dc78c504">
							<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
							<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/1d096dc0-92c7-4f86-bb54-ca71dc78c504" ><img class="hs-cta-img" id="hs-cta-img-1d096dc0-92c7-4f86-bb54-ca71dc78c504" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/1d096dc0-92c7-4f86-bb54-ca71dc78c504.png" alt="Sign Up Now >"/></a>
							</span>
							<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
							<script type="text/javascript">
							hbspt.cta.load(496304, "1d096dc0-92c7-4f86-bb54-ca71dc78c504", {});
							</script>
							</span>';
							break;
						case 'idea-mbw-text-june-9-2016':
							echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-68fe7084-53aa-488e-b434-6df2451e45e8">
							<span class="hs-cta-node hs-cta-68fe7084-53aa-488e-b434-6df2451e45e8" id="hs-cta-68fe7084-53aa-488e-b434-6df2451e45e8">
							<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
							<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/68fe7084-53aa-488e-b434-6df2451e45e8" ><img class="hs-cta-img" id="hs-cta-img-68fe7084-53aa-488e-b434-6df2451e45e8" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/68fe7084-53aa-488e-b434-6df2451e45e8.png" alt="Sign Up Now >"/></a>
							</span>
							<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
							<script type="text/javascript">
							hbspt.cta.load(496304, "68fe7084-53aa-488e-b434-6df2451e45e8", {});
							</script>
							</span>';
							break;
						case 'idea-fitness-tracker':
							echo '<span class="hs-cta-wrapper" id="hs-cta-wrapper-e48b46aa-1624-4d17-9e33-2251c4a6ca57">
							<span class="hs-cta-node hs-cta-e48b46aa-1624-4d17-9e33-2251c4a6ca57" id="hs-cta-e48b46aa-1624-4d17-9e33-2251c4a6ca57">
							<Unable to render embedded object: File (--[if lte IE 8]><div id="hs-cta-ie-element"></div><) not found.[endif]-->
							<a href="http://cta-redirect.hubspot.com/cta/redirect/496304/e48b46aa-1624-4d17-9e33-2251c4a6ca57" ><img class="hs-cta-img" id="hs-cta-img-e48b46aa-1624-4d17-9e33-2251c4a6ca57" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/496304/e48b46aa-1624-4d17-9e33-2251c4a6ca57.png" alt="Sign Up Now >"/></a>
							</span>
							<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
							<script type="text/javascript">
							hbspt.cta.load(496304, "e48b46aa-1624-4d17-9e33-2251c4a6ca57", {});
							</script>
							</span>';
							break;
						}
						?>
					@else
						@if(Sentinel::check())
							<a href="{{route('dashboard-schedule')}}" class="cta_btn m-green">Get Started Now</a>
						@else
							<a href="{{route('auth-register')}}" target="" data-toggle="popup" data-target="popup_auth" data-authpopup-toggle="sign_up"  class="cta_btn m-green">Sign Up Now</a>
						@endif
					@endif
				</div>
			@endif
		</div>
		<div class="promo_section--screen">
			<div class="promo_screen m-messaging"></div>
		</div>
	</div>
</div>

<section class="home-signup">
	<div class="home-signup--wrap">
		<div class="home-signup--col home-signup--features">
			<div class="home-signup--features_in">
				<h2 class="home-signup--features_title">HEALTHPRO, MORE THAN A SOFTWARE</h2>
				<ul class="home-signup--features_list">
					<li><div class="home-signup--features_list_icon"></div><div class="home-signup--features_list_text">Simplify Business, Increase Revenue</div></li>
					<li><div class="home-signup--features_list_icon"></div><div class="home-signup--features_list_text">All-in-One Management Platform</div></li>
					<li><div class="home-signup--features_list_icon"></div><div class="home-signup--features_list_text">Boost Your Online Presence</div></li>
					<li><div class="home-signup--features_list_icon"></div><div class="home-signup--features_list_text">Dedicated Support 24/7</div></li>
				</ul>
			</div>
		</div>
		<div class="home-signup--col home-signup--form home-signup--area1">
			<div class="home-signup--get-started">
				<h2 class="home-signup--get-started-title">READY TO GET STARTED?</h2>
				<p class="home-signup--get-started-body">Select a flexible plan that works best for you.
					                                     Sign up and instantly discover all the ways HealthPRO will help you to easily manage your business.
					                                     Simple-to-Use. Sign Up in Minutes. Penalty Free. Cancel Anytime!
				</p>
			</div>
			<div class="home-signup--area1-2">
				<div class="home-signup--area1-2-inner">
					<div class="home-signup--area1-2-inner2">
						<a href="{{route('pricing')}}" class="home-signup--btn-choose">CHOOSE YOUR PLAN</a>
					</div>
					<div class="home-signup--risk-free">
						<b>Risk FREE</b> - Money Back Guarantee
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="pricing_plans {{$sectionClass or ''}}" data-pricing>
	<div class="wrap">
		<div class="pricing_plans--wrap">
			<div class="pricing_plans--list">
				@foreach ($products as $product)
					@php
						$membership = $product['membership'];
						$price = explode('.', $product['products'][0]['price']);
						$options = array();
						$product_action = '';
						$is_selected = false;
						$product_id = null;

						switch ($membership['slug']) {
							case 'part_timer':
								$classname = 'm-green';
								$btn_classname = 'm-green_mute';
								break;
							case 'healthpro':
								$classname = 'm-red';
								$btn_classname = 'm-red_mute';
								break;
							case 'healthpro_plus':
								$classname = 'm-blue m-best_deal';
								$btn_classname = 'm-blue_mute';
								break;
						}

						foreach ($product['products'] as $product_options) {
							$obj = new stdClass;
							$obj->id = $product_options['id'];
							$obj->price = $product_options['price'];
							$obj->billing_cycles = $product_options['billing_cycles'];
							$obj->name = $membership['name'];
							$product_id = $product_options['id'];

							array_push($options, $obj);

							if (!$is_selected) {
								$is_selected = $product_options['id'] == $productId;
							}

							if ($product['action'] == 'SWITCH') {
								$btn_classname = 'm-light_gray_border';
								if ($product_options['allowed']) {
									switch ($product_options['billing_cycles']) {
										case 1:
											$product_action = 'Change to Monthly';
											break;
										case 12:
											$product_action = 'Change to Annual';
											break;
									}
								}
							} else {
								$product_action = $product['action'];
							}
						}

						$is_subscribed = $product_action != 'BUY NOW'

					@endphp
					<div class="pricing_box {{ $classname }}">
						<div class="pricing_box--header">{{ $membership['name'] }}</div>
						<div class="pricing_box--content">
							<div class="pricing_box--price">
								<span class="pricing_box--price_val">
									<sup class="m-currency">$</sup>{{ $price[0] }}.<sup>{{ array_key_exists('1', $price) ? $price[1] : '00' }}</sup></span>
								<span class="pricing_box--price_label">Per Month</span>
							</div>
							<div class="pricing_box--bookings">
								@if ($membership['bookings_limit'])
									{{ $membership['bookings_limit'] }} Bookings
									<span class="pricing_box--tip_holder" data-tooltip data-tooltip-position="bottom" data-tooltip-delay="0">
										<span class="pricing_box--tip" data-tooltip-content>
											{!! nl2br($membership['description']) !!}
										</span>
									</span>
								@else
									Unlimited Bookings
								@endif
							</div>
							<div class="pricing_box--options">
								@if ($membership['slug'] != 'part_timer')
									<ul class="pricing_box--options_list">
										<li><i class="inline_icon m-comments"></i> SMS Notifications</li>
										<li><i class="inline_icon m-gift"></i> Gift Certificates</li>
									</ul>
								@endif
							</div>
							<div class="pricing_box--actions">
								@if ($is_subscribed)
									<a href="{{route('dashboard-billing-info')}}#change_plan:{{$product_id}}" class="btn_round {{ $btn_classname }}">{{ $product_action }}</a>
								@else
									<button
										class="btn_round {{ $btn_classname }}"
										data-pricing-btn
										@if ($is_selected) data-selected-pricing @endif
										data-toggle="popup"
										data-target="popup_membership"
										data-product-options="{{ json_encode($options) }}">
										{{ $product_action }}
									</button>
								@endif
							</div>
							<div class="pricing_box--short">
								<div class="pricing_box--short_header">{{ $membership['name'] }}</div>
								<span class="pricing_box--short_price"><i>$</i> <b>{{ $price[0] }}.{{ array_key_exists('1', $price) ? $price[1] : '00' }}</b> /mo</span>
								<div class="pricing_box--short_bookings">
									@if ($membership['bookings_limit'])
										{{ $membership['bookings_limit'] }} Bookings
										<span class="pricing_box--tip_holder" data-tooltip data-tooltip-position="bottom" data-tooltip-delay="0">
											<span class="pricing_box--tip" data-tooltip-content>
												{!! nl2br($membership['description']) !!}
											</span>
										</span>
									@else
										Unlimited Bookings
									@endif
								</div>
								<div class="pricing_box--short_actions">
									@if ($is_subscribed)
										<a href="{{route('dashboard-billing-info')}}#change_plan:{{$product_id}}" class="btn_round {{ $btn_classname }}">{{ $product_action }}</a>
									@else
										<button
											class="btn_round {{ $btn_classname }}"
											data-pricing-btn
											@if ($is_selected) data-selected-pricing @endif
											data-toggle="popup"
											data-target="popup_membership"
											data-product-options="{{ json_encode($options) }}">
											{{ $product_action }}
										</button>
									@endif
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
		<div class="pricing_plans--guarantee">
			<p>45-Day Money Back Guarantee. <b>RISK-FREE!</b></p>
		</div>
	</div>
</div>

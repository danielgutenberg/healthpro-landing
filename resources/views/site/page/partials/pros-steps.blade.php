<section class="home-steps">
	<div class="wrap">
		<ul class="home-steps--list">
			<li class="home-steps--item m-join">
				<div class="home-steps--item_title">Easy To Use</div>
				<div class="home-steps--item_text">Manage and automate your business using one user-friendly platform.</div>
			</li>
			<li class="home-steps--item m-get_booked">
				<div class="home-steps--item_title">Powerful Features</div>
				<div class="home-steps--item_text">Intuitive features make scheduling, payments and client engagement a breeze.</div>
			</li>
			<li class="home-steps--item m-enjoy">
				<div class="home-steps--item_title">Saves You Time</div>
				<div class="home-steps--item_text">Save time, earn more money and grow your business - hassle-free.</div>
			</li>
		</ul>
	</div>
</section>

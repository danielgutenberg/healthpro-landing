<div class="home_hero">
	<div class="home_hero--slides js-slider">
		<?php foreach($slides as $key => $slide):?>
		<div class="home_hero--slides_item c" id="intro"
			 style="background-image: url('/assets/frontend/images/design/home/professionals/2016/hero/slide_{!! $key !!}.jpg');">
			<div class="home_hero--in">
				<?php if('intro'==$key):?>
				<figure class="home_hero--device">
					<img src="{{asset('/assets/frontend/images/design/home/professionals/2016/hero/smartphone.png')}}" alt="Healthpro App">
				</figure>
					<?php endif; ?>
				<i class="home_hero--icon m-{!! $key !!}"></i>
				<div class="home_hero--content">
					<div class="home_hero--heading">{!! $slide['heading'] !!}</div>
					<div class="home_hero--text">
						{!! $slide['text'] !!}
					</div>
					<div class="home_hero--cta">
						<div>
							<a href="{{route('pricing')}}" class="home_hero--btn-choose-your-plan">CHOOSE YOUR PLAN</a>
						</div>
					</div>
					<div class="home_hero--risk-free-up-text">45 Day Money Back Guarantee!</div>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
	<div class="home_hero--nav js-slider_nav">
		<ul class="home_hero--nav_list">
			<?php $i = 0; foreach($slides as $key => $slide):?>
			<li class="home_hero--nav_item">
				<button class="home_hero--nav_link m-{!! $key !!}" data-slide-target="{!! $key !!}" data-slide-id="{!! $i !!}">
					<span>{!! $slide['button_text'] !!}</span>
				</button>
			</li>
			<?php $i++; endforeach; ?>
		</ul>
	</div>
</div>

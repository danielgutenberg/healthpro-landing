@extends('site.layouts.professional')
@section('title')
    {{$title}}
@stop
@section('meta-description'){{$description}}@stop
@section('meta-keywords'){{$keywords}} @stop
@section('content')
    <div class="fc-bookme">
        <img  class="fc-bookme-logo" width="260" height="90" src="assets/frontend/images/design/svg/logo.svg" alt="healthpro logo">
        <div class="fc-bookme-top">
            <div class="fc-bookme-h1style1">Why should I add a <b>“Book Me”</b> button to my Facebook page?</div>
            <p  class="fc-bookme-p1">
                Adding a “Book Me” button to your Facebook Business Page will help drive people from Facebook to your HealthPRO profile where they can easily book appointments with you. Adding a button is fast, easy and free - just follow the steps below:
            </p>
        </div>
        <img class="fc-bookme-tutorial" alt='tutorial how add facebook "Book Me" button' src="/assets/frontend/images/design/facebook-book-me/tutorial.png"/>
        <div class="fc-bookme-body">
            <div class="fc-bookme-body-item">
                <div class="fc-bookme-body-item-header">1</div>
                <div class="fc-bookme-body-item-text">
                    <p>Go to your Facebook page cover photo and click + <b>Facebook Business Page</b>.</p>
                <br/><p>If you do not yet have a Facebook Business Page, go to your Facebook profile page and click on the drop down menu. Select Create Page to get started with your business page.</p>
                </div>
                <img class="fc-bookme-body-item-icon" alt="step 1 tutorial" src="/assets/frontend/images/design/facebook-book-me/fc-tutorial1.png"/>
            </div>
            <div class="fc-bookme-body-item">
                <div class="fc-bookme-body-item-header">2</div>
                <div class="fc-bookme-body-item-text">
                    <p>From the dropdown menu choose <b>Book Services</b> and then <b>Book Now</b>.</p>
                </div>
                <img class="fc-bookme-body-item-icon" alt="step 2 tutorial" src="/assets/frontend/images/design/facebook-book-me/fc-tutorial2.png"/>
            </div>
            <div class="fc-bookme-body-item">
                <div class="fc-bookme-body-item-header">3</div>
                <div class="fc-bookme-body-item-text">
                    <p>Copy the <b>URL</b> from your profile page on HealthPRO.</p>
                </div>
                <img class="fc-bookme-body-item-icon" alt="step 3 tutorial" src="/assets/frontend/images/design/facebook-book-me/fc-tutorial3.png"/>
            </div>
            <div class="fc-bookme-body-item">
                <div class="fc-bookme-body-item-header">4</div>
                <div class="fc-bookme-body-item-text">
                    <p>Enter the <b>URL from your HealthPRO</b> profile page.</p>
                </div>
                <img class="fc-bookme-body-item-icon" alt="step 4 tutorial" src="/assets/frontend/images/design/facebook-book-me/fc-tutorial4.png"/>
            </div>
            <div class="fc-bookme-body-item">
                <div class="fc-bookme-body-item-header">5</div>
                <div class="fc-bookme-body-item-text">
                    <p><b>Create and you’re done!</b></p>
                </div>
                <img class="fc-bookme-body-item-icon" alt="step 5 tutorial" src="/assets/frontend/images/design/facebook-book-me/fc-tutorial5.png"/>
            </div>
        </div>
    </div>
@stop

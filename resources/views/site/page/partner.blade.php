@extends('site.layouts.professional')
@section('title')Partners Page | HealthPRO @stop
@section('meta-description')Learn about the growing number of associations, organizations and companies which have partnered with HealthPRO. Partnering with HealthPRO brings added value to your members and customers; learn about the benefits today.@stop
@section('meta-keywords'){{'partnership,partner,management,scheduling,software,solution,support,associations,organizations,companies,Healthpro.com,health,pro,healthpro'}}@stop
@section('content')
	<div class="partner promo">

		<div class="partner_hero">
			<div class="partner_hero--wrap wrap">
				<div class="partner_hero--title">Benefit from Partnering with HealthPRO</div>
				<div class="partner_hero--text">
					<p>Join the growing community of associations and companies who have partnered with HeathPRO to bring added value and benefits to their members and customers.</p>
				</div>
				<ul class="partner_hero--list">
					<li class="partner_hero--list_item m-tools"><span>Extended Benefits for your Customers</span></li>
					<li class="partner_hero--list_item m-revenue"><span>Revenue Sharing &amp;&nbsp;Affiliate Programs</span></li>
					<li class="partner_hero--list_item m-api"><span>Custom Features &amp;&nbsp;Integration (API)</span></li>
					<li class="partner_hero--list_item m-coop"><span>Cooperative Branding Opportunities</span></li>
				</ul>
				<a href="#" class="partner_hero--btn cta_btn m-red m-arrow m-xlarge m-rounded" data-toggle="popup" data-target="partner_contact">Contact us today</a>
			</div>
		</div>

		<div class="partner_features">
			<div class="partner_features--wrap wrap">
				<ul class="partner_features--list">
					<li class="partner_features--list_item m-manage_business">
						<p>HealthPRO’s simple online platform enables your Health, Wellness and Fitness professionals to easily manage their businesses.</p>
					</li>
					<li class="partner_features--list_item m-match_clients">
						<p>Offer your independent professionals the opportunity to simplify and grow their business with powerful features that are easy-to-use.</p>
					</li>
				</ul>
			</div>
		</div>

		<div class="partner_current">
			<div class="partner_current--wrap wrap">
				<div class="partner_current--title">Current Association and Corporate Partnerships</div>
				<div class="partner_current--list js-carousel">
					<div class="partner_current--list_item">
						<div class="partner_current--list_pic" style="width: 122px">
							<a href="/partner/aaaom">
								<img src="{{asset('/assets/frontend/images/content/partner/logos/aaaom.png')}}" alt="American Academy of Acupuncture
							 and Oriental Medicine" width="122">
							</a>
						</div>
					 </div>
					<div class="partner_current--list_item">
						<div class="partner_current--list_pic" style="width: 123px">
							<a href="/partner/hcc">
								<img src="{{asset('/assets/frontend/images/content/partner/logos/hcc.png')}}" alt="The Holistic Chamber of Commerce" width="122">
							</a>
						</div>
					</div>
					<div class="partner_current--list_item">
						<div class="partner_current--list_pic" style="width: 126px">
							<a href="/partner/pryt">
								<img src="{{asset('/assets/frontend/images/content/partner/logos/phoenix.png')}}" alt="Phoenix Rising Yoga Therapy" width="125">
							</a>
						</div>
					</div>
					<div class="partner_current--list_item">
						<div class="partner_current--list_pic" style="width: 300px">
							<a href="/partner/hwncc">
								<img src="{{asset('/assets/frontend/images/content/partner/logos/hwncc.png')}}" alt="Health and Welness Network of Commerce" width="300">
							</a>
						</div>
					</div>
					<div class="partner_current--list_item">
						<div class="partner_current--list_pic" style="width: 122px">
							<a href="/partner/iaplc">
								<img src="{{asset('/assets/frontend/images/content/partner/logos/iaplc.png')}}" alt="International Association of Professional Life Coaches" width="122">
							</a>
						</div>
					</div>
					<div class="partner_current--list_item">
						<div class="partner_current--list_pic" style="width: 242px">
							<a href="/partner/waterwood">
								<img src="{{asset('/assets/frontend/images/content/partner/logos/waterwood.png')}}" alt="Waterwood Healing Center" width="242">
							</a>
						</div>
					</div>
					<div class="partner_current--list_item">
						<div class="partner_current--list_pic" style="width: 242px">
							<a href="/partner/susan-shatzer">
								<img src="{{asset('/assets/frontend/images/design/partner/susan-shatzer-logo.png')}}" alt="Waterwood Healing Center" width="242">
							</a>
						</div>
					</div>
					<div class="partner_current--list_item">
						<div class="partner_current--list_pic" style="width: 242px">
							<a href="/partner/whole-life-marketing">
								<img src="{{asset('/assets/frontend/images/design/partner/whole-life-marketing-logo.png')}}" alt="Waterwood Healing Center" width="242">
							</a>
							</div>
					</div>
					<div class="partner_current--list_item">
						<div class="partner_current--list_pic" style="width: 242px">
							<a href="/partner/gymlynx">
								<img src="{{asset('/assets/frontend/images/design/partner/gymlynx-logo.png')}}" alt="Waterwood Healing Center" width="242">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="partner_cta">
			<div class="wrap">
				<a href="#" class="cta_btn m-red m-arrow m-xlarge partner_cta--btn" data-toggle="popup" data-target="partner_contact">Contact Us Today to Become a Partner</a>
			</div>
		</div>

	</div>
@stop

@section('popups')
	<div class="popup partner_contact" data-popup="partner_contact">
		<div class="popup--container" data-popup-container>
			<button class="popup--close" data-popup-close>close</button>
			<div class="popup--content partner_contact--content">
				<div class="partner_contact--form" data-cs-form="partner_contact"></div>
				<div class="loading_overlay m-light m-hide"><span class="spin m-logo"></span></div>
			</div>
		</div>
		<div class="popup--bg"></div>
	</div>
@append

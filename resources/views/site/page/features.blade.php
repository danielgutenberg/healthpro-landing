@extends('site.layouts.professional')

@section('title')HealthPRO Features - Simple And Easy To Use @stop
@section('meta-description')View a full list of HealthPRO’s advanced features designed to simplify day to day management of your health and wellness business. Check out the powerful tools that will save you time and money running your independent business.@stop
@section('meta-keywords'){{'Features,simple,powerful,advanced,tools,business,management,running,HealthPRO.com,HealthPRO,pro,health'}}@stop
@section('content')
	<div class="page-features promo">
		<div class="promo_hero m-features_header">
			<div class="promo_hero--inner wrap">
				<h1 class="promo_hero--heading">Advanced Features Made Simple</h1>

				<div class="promo_hero--actions">
					<div class="promo_hero--actions_item">
						<a class="btn_round m-blue_light m-upper m-arrow m-large" href="{{route('pricing')}}">Compare plans</a>
					</div>
					<div class="promo_hero--actions_item">
						<a class="btn_round m-green m-upper m-arrow m-large" data-toggle="popup" data-target="demo_request">Request a demo</a>
					</div>
				</div>

			</div>
		</div>

		<div class="promo_hero promo_features m-features">
			<div class="promo_features--wrap wrap">

				<?php
					$features = [
						[
							'slug' => 'profile',
							'title' => 'Public <br> Profile',
							'panel' => [
								'list' => [
									[
										'title' => 'Your Services',
										'text' => 'List all the types of services you provide.'
									],
									[
										'title' => 'Your Rates',
										'text' => 'Set different rates for each service you offer.'
									],
									[
										'title' => 'Your Durations',
										'text' => 'Set various durations for each session.'
									],
									[
										'title' => 'Your Locations',
										'text' => 'List all the locations from which you work.'
									],
									[
										'title' => 'Social Links ',
										'text' => 'Let clients connect with you from their favorite social networks.'
									],
									[
										'title' => 'Photos & Videos',
										'text' => 'Upload photos and videos. '
									],
									[
										'title' => 'Suitability & Conditions ',
										'text' => 'List any specialities or niche populations with which you work.'
									],
								]
							]
						],
						[
							'slug' => 'payments',
							'title' => 'Payments & <br> Transactions',
							'panel' => [
								'list' => [
									[
										'title' => 'Your Durations',
										'text' => 'Set various durations for each session.'
									],
									[
										'title' => 'Your Locations',
										'text' => 'List all the locations from which you work.'
									],
									[
										'title' => 'Social Links ',
										'text' => 'Let clients connect with you from their favorite social networks.'
									],
									[
										'title' => 'Photos & Videos',
										'text' => 'Upload photos and videos. '
									],
									[
										'title' => 'Suitability & Conditions ',
										'text' => 'List any specialities or niche populations with which you work.'
									],
								]
							]
						],
						[
							'slug' => 'scheduling',
							'title' => 'Automation & <br> Scheduling',
							'panel' => [
								'list' => [
									[
										'title' => 'Your Durations',
										'text' => 'Set various durations for each session.'
									],
									[
										'title' => 'Social Links ',
										'text' => 'Let clients connect with you from their favorite social networks.'
									],
									[
										'title' => 'Photos & Videos',
										'text' => 'Upload photos and videos. '
									],
									[
										'title' => 'Suitability & Conditions ',
										'text' => 'List any specialities or niche populations with which you work.'
									],
								],
								'content' => '<p>List all the locations from which you work. Set various durations for each session.</p>',
							]
						],
						[
							'slug' => 'booking',
							'title' => 'Online <br> Booking',
							'panel' => [
								'list' => [
									[
										'title' => 'Social Links ',
										'text' => 'Let clients connect with you from their favorite social networks.'
									],
									[
										'title' => 'Photos & Videos',
										'text' => 'Upload photos and videos. '
									],
									[
										'title' => 'Suitability & Conditions ',
										'text' => 'List any specialities or niche populations with which you work.'
									],
								],
								'content' => '<p>List all the locations from which you work. Set various durations for each session.</p>',
							]
						],
						[
							'slug' => 'suite',
							'title' => 'Marketing <br> Suite',
							'panel' => [
								'content' => '<p>List all the locations from which you work. Set various durations for each session.</p>',
							]
						],
						[
							'slug' => 'support',
							'title' => 'Communication & <br> Support',
							'panel' => [
								'list' => [
									[
										'title' => 'Your Durations',
										'text' => 'Set various durations for each session.'
									],
									[
										'title' => 'Social Links ',
										'text' => 'Let clients connect with you from their favorite social networks.'
									],
								],
								'content' => '<p>List all the locations from which you work. Set various durations for each session.</p>'
							]
						],
					];
				?>

				<h2 class="promo_features--heading">Run Your Business with Powerful Features that are <span class="nowrap">Easy-to-Use</span></h2>

				<div class="promo_features--tabs" data-tabs>
					<ul class="promo_features--tabs_list">
						@php ($i = 1)
						@foreach($features as $feature)
							<li class="promo_features--tabs_item m-{{$feature['slug']}} @if ($i == '1') m-active @endif" data-tabs-target="{{$feature['slug']}}">
								<i class="promo_features--tabs_item_icon"></i>
								<h4 class="promo_features--tabs_item_title">{!!$feature['title']!!}</h4>
							</li>
							@php ($i++)
						@endforeach
					</ul>
					@php ($i = 1)
					@foreach($features as $feature)
						@if (isset($feature['panel']))
							<div class="promo_features--panel @if ($i == '1') m-active @endif m-{{$feature['slug']}}" id="{{$feature['slug']}}" data-tabs-panel>
								<div class="promo_features--tabs_item promo_features--panel_header @if ($i == '1') m-active @endif m-{{$feature['slug']}}" data-tabs-target="{{$feature['slug']}}">
									<i class="promo_features--tabs_item_icon"></i>
									<h4 class="promo_features--tabs_item_title">{!!$feature['title']!!}</h4>
								</div>
								<div class="promo_features--panel_content">
									@if (isset($feature['panel']['content']))
										{!! $feature['panel']['content'] !!}
									@endif
									@if (isset($feature['panel']['list']))
										<ul>
											@foreach ($feature['panel']['list'] as $item)
												<li>
													<b>{{ $item['title'] }}</b>
													{!! $item['text'] !!}
												</li>
											@endforeach
										</ul>
									@endif
								</div>
							</div>
						@endif
						@php ($i++)
					@endforeach
				</div>
			</div>
		</div>

		@include('site.page.partials.pricing-plans', ['sectionClass' => 'm-standalone'])

	</div>
@stop

@section('popups')
	@include('site.home.professionals.popup-video')
@append

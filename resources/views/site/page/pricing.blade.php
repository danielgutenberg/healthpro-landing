@extends('site.layouts.professional')
@section('title')HealthPRO Pricing @stop
@section('meta-description')Find out how much HealthPRO’s online software costs; for a low monthly cost our pricing plans are risk-free and include all the best essential business management features. With HealthPRO you choose the pricing option that works best for your business and receive a 45-day money back guarantee, all for a flat monthly cost.@stop
@section('meta-keywords'){{'Pricing,plan,flexible,affordable,best,features,tools,cost,online,software,pricing,cost,of,how,much,options,powerful,advanced,tools,scheduling,payments,communication,support,business,management,running,HealthPRO.com,HealthPRO,pro,health'}}@stop
@section('content')
	{{-- <a href="#" data-toggle="popup" data-target="popup_membership">Membership</a> --}}
	<section class="pricing">
		<div class="pricing--header">
			<div class="wrap">
				<h1 class="pricing--header_title">
					<p>Get The All-In-One Booking Software</p>
					<p>At A Price That Suits Your Workload</p>
				</h1>
			</div>
		</div>

		@include('site.page.partials.pricing-plans')

		<div class="pricing--features">
			<div class="pricing--features_wrap">
				<div class="pricing--features_header">All The Best Features Included In Every Plan</div>
				<div class="pricing--features_list">
					<div class="pricing_feature">
						<div class="pricing_feature--header m-schedule">Scheduling</div>
						<div class="pricing_feature--title">Seamless schedule setup</div>
						<div class="pricing_feature--content">
							<ul>
								<li>24/7 online self-booking</li>
								<li>Automatic real-time updates</li>
								<li>Client notifications</li>
							</ul>
						</div>
					</div>
					<div class="pricing_feature">
						<div class="pricing_feature--header m-communication">Communication</div>
						<div class="pricing_feature--title">One platform for engaging with clients</div>
						<div class="pricing_feature--content">
							<ul>
								<li>Client notes and appointment history</li>
								<li>Automated reminders</li>
								<li>Instant messages</li>
							</ul>
						</div>
					</div>
					<div class="pricing_feature">
						<div class="pricing_feature--header m-payments">Payments</div>
						<div class="pricing_feature--title">Flexible payment options for your clients</div>
						<div class="pricing_feature--content">
							<p>Accept cash, checks, online payments, and securely process credit and debit cards with:</p>
							<div class="pricing_feature--payments">
								<div class="pricing_feature--payments_item m-stripe">Stripe</div>
								<i class="pricing_feature--payments_or"><i>or</i></i>
								<div class="pricing_feature--payments_item m-paypal">PayPal</div>
							</div>
						</div>
					</div>
					<div class="pricing_feature">
						<div class="pricing_feature--header m-support">Support</div>
						<div class="pricing_feature--title">One platform for engaging with clients</div>
						<div class="pricing_feature--content">
							<ul>
								<li>Free demos</li>
								<li>Setup assistance</li>
								<li>Live chat</li>
								<li>Easy migration from other software</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@stop

@section('js-globals')
	PAYMENTSUCCESSPOPUP: '{{$popup}}',
	SOCIALSIGNUP: '{{$socialSignup}}',
	PRODUCTID: '{{$productId}}',
@stop

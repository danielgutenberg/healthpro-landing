<div class="auth--container auth_login @if (! empty($classes)) {{$classes}} @endif" data-auth-container="login">

	<div class="auth--header">
		@if (! empty($formTitle))
			<div class="auth--title">{{$formTitle}}</div>
		@else
			<div class="auth--title">Log Into Your HealthPRO Account</div>
		@endif
		@unless (! empty($hideDescription) and $hideDescription)
			{{-- <div class="auth--text">
				<p>If you have not yet registered for HealthPRO, please click on “Sign Up” below.</p>
			</div> --}}
		@endunless
	</div>

	{!!
		Former::open()
			->method('POST')
			->action(route('auth-login'))
			->class("auth_form form m-login")
	!!}

		<div class="auth--cols">
			<div class="auth--col m-or m-login_social">
				<div class="auth_form--fast_sign m-login @if (! empty($fastSignClasses)) {{$fastSignClasses}} @endif">
					<h4 class="auth_form--title">Log In with Social Networks</h4>
					<div class="form--line">
						<a href="{{ route('facebook-authorize') }}" class="btn_social -facebook">
							<span class="btn_social--icon m-transparent"></span>
							<span class="btn_social--text">Log In with Facebook</span>
						</a>
					</div>
					<div class="form--line">
						<a href="{{ route('linkedin-authorize') }}" class="btn_social -linkedin">
							<span class="btn_social--icon m-transparent"></span>
							<span class="btn_social--text">Log In with LinkedIn</span>
						</a>
					</div>
					<div class="form--line">
						<a href="{{ route('google-authorize') }}" class="btn_social -google">
							<span class="btn_social--icon m-transparent"></span>
							<span class="btn_social--text">Log In with Google</span>
						</a>
					</div>
				</div>
			</div>
			<div class="auth--col m-login_form">
				<div class="auth_form--login">
					<h4 class="auth_form--title">Log In with Email</h4>
					<div class="form--line">
						<label class="field m-icon m-message m-wide">
							<input type="email" id="login_email_popup" name="email" placeholder="Email">
						</label>
					</div>
					<div class="form--line">
						<label class="field m-icon m-lock m-wide">
							<input type="password" id="login_pass_popup" name="password" placeholder="Password">
						</label>
					</div>
					<div class="form--line auth_form--login_extra">
						<label class="checkbox">
							<input type="checkbox" name="remember">
							<span class="checkbox--i"></span>
							<span class="checkbox--label">Remember Me</span>
						</label>
						<button class="auth_form--toggler" data-authpopup-toggle="reset" type="button">Forgot Password?</button>
					</div>
					<div class="auth_form--actions">
						<button class="btn m-green" type="submit" data-form-el="submit">Log In</button>
						<input type="hidden" name="redirect_to" value="@if (! empty($redirect_to)) {{$redirect_to}} @endif">
						@if (! empty($profile_type))
							<input type="hidden" name="profile_type" value="{{$profile_type}}">
						@endif
					</div>
				</div>
			</div>
		</div>
		@if (Session::has('registerFirst'))
			<div class="auth--alert m-error">
				<p>The account you are signing into does not exists. Would you like to sign up?</p>
				<a href="{{ route('pricing') }}" class="btn m-red m-small">Sign Up as a Health Professional</a>
			</div>
		@endif
	{!! Former::close() !!}
</div>

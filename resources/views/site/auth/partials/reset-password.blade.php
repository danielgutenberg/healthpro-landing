<div class="auth--container auth_reset @if (! empty($classes)) {{$classes}} @endif" data-auth-container="reset">

	<div class="auth--header">
		<div class="auth--title">Reset Your Password</div>
		<div class="auth--text">
			<p>Enter your email address associated with your account, and we'll email you a link to reset your password.</p>
		</div>
	</div>
	@if (Session::has('info'))
		<div class="alert m-full m-show m-register_first">
			<span>{{Session::get('info')}}</span>
		</div>
	@endif
	{!!
		Former::open()
			->method('POST')
			->action(route('auth-reset-password-post'))
			->class("auth_form form m-reset")
			->dataForm('{"ajax": true, "url":"'. route("ajax-auth-reset-password") .'", "name": "password_request"}')
			->data_noinit(1)
	!!}
	<div class="auth_form--restore">
		<h4 class="auth_form--title">Email Address</h4>
		<div class="form--line">
			<label class="field m-icon m-message m-wide">
				<input name="email" type="email" id="reset_email_popup" placeholder="Email Address">
			</label>
		</div>
		<div class="auth_form--actions">
			<button class="btn m-green" type="submit" data-form-el="submit">Reset Password</button>
		</div>
	</div>
	{!! Former::close() !!}
</div>

<div class="auth--container auth_sign" data-auth-container="sign">
	<div class="auth--cols">
		<div class="auth--col m-full">
			<div class="auth_aside m-professional">
				<div class="auth_aside--inner">
					<div class="auth_aside--content" data-auth-sidebar></div>
				</div>
			</div>
		</div>
		<div class="auth--col m-full">
			{!!
				Former::open()
					->method('POST')
					->action(route('auth-register'))
					->class("auth_form form m-sign inline_auth_form")
			!!}
				<div class="auth_form--fast_sign m-sign">
					<div class="auth_form--fast_sign_title">Sign Up with Social Networks</div>
					<div class="auth_form--fast_sign_buttons m-sign">
						<a href="{{ route('facebook-authorize') }}" class="btn_social -facebook">
							<span class="btn_social--icon m-transparent"></span>
						</a>
						<a href="{{ route('linkedin-authorize') }}" class="btn_social -linkedin">
							<span class="btn_social--icon m-transparent"></span>
						</a>
						<a href="{{ route('google-authorize') }}" class="btn_social -google">
							<span class="btn_social--icon m-transparent"></span>
						</a>
					</div>
					<i class="auth_form--fast_sign_or">OR</i>
				</div>

				<div class="auth_form--signup">
					<div class="form--line">
						<label class="field m-icon m-user m-wide">
							<input type="text"
								   id="sign_name_popup"
								   name="first_name"
								   value=""
								   placeholder="Your Name"
								   max="26"
								   data-letters-only>
						</label>
					</div>
					<div class="form--line">
						<label class="field m-icon m-checked_user m-wide">
							<input type="text"
								   id="sign_last_popup"
								   name="last_name"
								   value=""
								   placeholder="Last Name"
								   max="26"
								   data-letters-only>
						</label>
					</div>
					<div class="form--line">
						<label class="field m-icon m-message m-wide">
							<input type="email"
								   id="sign_email_popup"
								   name="email"
								   value=""
								   placeholder="Email Address">
						</label>
					</div>
					<div class="form--line">
						<label class="field m-icon m-lock m-wide">
							<input type="password" id="sign_pass_popup" name="password" placeholder="Create a Password">
						</label>
					</div>
					<div class="form--line">
						<label class="field checkbox">
							<input type="checkbox" class="auth_form--terms">
							<span class="checkbox--i"></span>
							<span class="checkbox--label">
								I agree to HealthPRO's <a href="{{URL::route('page.show', ['slug' => 'terms'])}}" target="_blank">Terms of Service</a>
							</span>
						</label>
					</div>
					<div class="auth_form--actions">
						<button class="btn m-green" type="submit" data-form-el="submit">Create a New Account</button>
						<input type="hidden" name="redirect_to" value="">
						<input type="hidden" name="profile_type" value="{{\WL\Modules\Profile\Models\ModelProfile::PROVIDER_PROFILE_TYPE}}">
					</div>
				</div>
			{!! Former::close() !!}
		</div>
	</div>
</div>

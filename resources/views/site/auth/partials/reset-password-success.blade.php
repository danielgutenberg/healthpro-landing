<div class="auth--container auth_reset" data-auth-container="reset_success">
	<div class="auth--header m-success">
		<div class="auth--title">Success!</div>
		<div class="auth--text">
			<p>An email with further instructions has been sent.</p>
		</div>
	</div>
	{!! Former::close() !!}
</div>

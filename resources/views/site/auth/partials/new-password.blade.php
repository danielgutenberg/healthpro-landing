<div class="auth--container auth_new_password" data-auth-container="new_password">
	<div class="auth--header">
		<div class="auth--title">Set a New Password</div>
		<div class="auth--text">
			<p>Please enter your new password.</p>
		</div>
	</div>
	{!!
        Former::open()
            ->method('POST')
            ->action(route('ajax-auth-set-new-password'))
            ->class("auth_form form")
            ->dataForm('{"ajax": true, "url":"'. route("ajax-auth-set-new-password") .'"}')
    !!}
		<div class="auth_form--restore">
			<h4 class="auth_form--title">Set New Password</h4>
			<div class="form--line">
				<label class="field m-icon m-lock m-wide">
					<input type="password" name="newPassword" placeholder="Password">
				</label>
			</div>
			<div class="form--line">
				<label class="field m-icon m-lock m-wide">
					<input type="password" name="newPassword_confirmation" placeholder="Confirm Password">
				</label>
			</div>
			<div class="auth_form--actions">
				<button class="btn m-green" type="submit" data-form-el="submit">Reset Password</button>
			</div>
		</div>
	{!! Former::close() !!}
</div>

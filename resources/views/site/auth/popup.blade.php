

	<div class="popup m-auth" data-popup="popup_auth" id="popup_auth">
		<div class="popup--container" data-popup-container>
			<div class="popup--content">
				<div class="popup_auth" data-authpopup>
					<button class="popup_auth--close" data-popup-close>close</button>
					<div class="auth m-popup">
						<div class="auth--main">
							@unless(Sentinel::check())
								@include('site.auth.partials.login')
								@include('site.auth.partials.sign')
							@endunless
							@include('site.auth.partials.reset-password')
							@include('site.auth.partials.reset-password-success')
							@include('site.auth.partials.new-password')
						</div>
						<footer class="auth--footer">
							<span class="auth--logo"></span>
							<span class="auth--switch m-login" data-switch="login">Already have an account? <a href="#" data-authpopup-toggle="login">Log In</a></span>
							@unless(Route::current()->getName() == 'home')
								<span class="auth--switch m-signup" data-switch="sign">Don't have an account? <a href="#" data-authpopup-toggle="sign:client">Sign Up</a></span>
							@endunless
						</footer>
					</div>
					<div class="loading_overlay m-light m-hide" data-authpopup-loading><span class="spin m-logo"></span></div>
				</div>
			</div>
		</div>
		<div class="popup--bg" data-popup-close></div>
	</div>

	<div class="popup popup_simple" data-popup="client_welcome" id="client_welcome">
		<div class="popup--container" data-popup-container>
			<div class="popup--wrap popup_simple--wrap">
				<div class="popup_simple--title">Welcome to HealthPRO!</div>
				<div class="popup_simple--text">
					<p>You can now start searching for professionals and booking them online. We wish you much health!</p>
					<p>The HealthPRO team.</p>
				</div>
				<footer class="popup_simple--actions">
					<a class="btn m-green" href="{{route('home')}}">Search for a Professional</a>
					<a class="btn m-blue" href="{{route('dashboard-myprofile-personalinfo')}}">View My Account</a>
				</footer>
			</div>
		</div>
		<div class="popup--bg" data-popup-close></div>
	</div>



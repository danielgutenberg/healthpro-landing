@extends('site.layouts.root')

@section('root-content')
	<div class="layout m-front @yield('wrapper-class')">
		<div class="layout--header">
			<header class="header @yield('header-class')">
				<div class="header--logo">
					<a
						@if(Sentinel::check())
							href="{{route('dashboard-schedule')}}"
						@else
							href="{{route('home')}}"
						@endif
					>
						<span>{{ __( Setting::get('site_title') ) }}</span>
					</a>
				</div>
				<div class="header--content">
					@if (strpos(Request::path(), 'booking') == -1 )
						<div class="header--right">
							{!! MenuHelper::render('frontend-landing-professional-top-nav', ['attr' => 'class="header--nav"']) !!}
							<nav class="header--user">
								{!! MenuHelper::render('frontend-header-user-menu') !!}
							</nav>
						</div>
					@endif
				</div>
				<i class="header--lines"><i></i></i>
				<a href="#" aria-label="Homepage" class="header--toggler" data-slideout data-options='{"slideout": ".layout--slideout", "mask":".layout--mask"}'></a>
			</header>
		</div>
		<div class="layout--mask"></div>
		<div class="layout--slideout">
			<div class="slideout--user">
				{!! MenuHelper::render('frontend-header-user-menu', ['user_menu_template' => 'menu.user-menu-slideout']) !!}
			</div>
			{!! MenuHelper::render('frontend-landing-professional-top-mobile-nav', ['attr' => 'class="slideout--nav m-front"']) !!}
		</div>
		<div class="layout--body m-wizard">
			@yield('content')
		</div>

		@include('site.auth.popup')
	</div>
@stop

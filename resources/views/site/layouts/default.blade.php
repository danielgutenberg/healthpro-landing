<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>@section('title'){{ MetaService::siteTitle()}}@show</title>

	<meta name="description" content="@section('meta-description'){{ __( Setting::get('site_description') ) }}@show">
	<meta name="keywords" content="@section('meta-keywords'){{ __( Setting::get('site_keywords') ) }}@show">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	@yield('meta-tags-additional')

	<script src="//use.typekit.net/bbt2uyw.js"></script>
	<script>try {Typekit.load();} catch (e) {}</script>

	<link rel="stylesheet" href="{{ asset('assets/frontend/styles/common.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/frontend/styles/front.css') }}">

	<link rel="icon" type="image/png" href="{{ asset('assets/frontend/images/favicon/favicon-32x32.png') }}" sizes="32x32"/>

	@yield('styles')
	{!!Setting::get('site_header_scripts')!!}

	{{TrackingHelper::code(['hotjar', 'fbpixel'])}}

</head>
<body class="m-mobile_ready @yield('body-class')">

{{TrackingHelper::code('tag-manager')}}
<div class="layout m-front @yield('wrapper-class')">
	<div class="layout--header">
		<header class="header @yield('header-class')">
			<div class="header--logo">
				<a
					@if(Sentinel::check())
						href="{{route('dashboard-schedule')}}"
					@else
						href="{{route('home')}}"
					@endif
				>
					<span>{{ __( Setting::get('site_title') ) }}</span>
				</a>
			</div>
			<div class="header--content">
				<div class="header--left">
					{!! MenuHelper::render('frontend-landing-top-nav', ['attr' => 'class="header--nav"']) !!}
				</div>
				<div class="header--right">
					@section('header-right-links')
						{!! MenuHelper::render('frontend-landing-top-right', ['attr' => 'class="header--nav"']) !!}
					@show
					<nav class="header--user">
						{!! MenuHelper::render('frontend-header-user-menu') !!}
					</nav>
				</div>
			</div>
			<i class="header--lines"><i></i></i>
			<a href="#" aria-label="Homepage" class="header--toggler" data-slideout data-options='{"slideout": ".layout--slideout", "mask":".layout--mask"}'></a>
		</header>
	</div>
	<div class="layout--mask"></div>
	<div class="layout--slideout">
		<div class="slideout--user">
			{!! MenuHelper::render('frontend-header-user-menu', ['user_menu_template' => 'menu.user-menu-slideout']) !!}
		</div>
		{!! MenuHelper::render('frontend-landing-top-mobile-nav', ['attr' => 'class="slideout--nav m-front"']) !!}
	</div>

	<div class="layout--body">
		@yield('content')
	</div>
	@include('site.layouts.footer')
</div>

@yield('popups')

<script>
	window.GLOBALS = {
		@yield('js-globals')
		_ASSETS_VERSION: '{{Manifest::assetsVersion()}}',
		_TOKEN: '{{Session::token()}}',
		_UID: ('{{$_UID}}' === '')? null : '{{$_UID}}',
		_UROLE: ('{{$_UROLE}}' === '')? null : '{{$_UROLE}}',
		_PID: ('{{$_PID}}' === '')? null : '{{$_PID}}',
		_PTYPE: ('{{$_PTYPE}}' === '')? null : '{{$_PTYPE}}',
	}
	@yield('js-scripts')
</script>

<script src="{{ asset('assets/frontend/scripts/common.js') }}"></script>
<script src="{{ asset('assets/frontend/scripts/front.js') }}"></script>
@yield('scripts')

{!!Setting::get('site_footer_scripts')!!}
</body>
</html>

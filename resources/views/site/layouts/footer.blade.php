<div class="layout--footer">
	<footer class="footer">
		<div class="footer--top">
			<div class="footer--top--left">
				<div class="footer--links">
					<div class="footer--links--col">
						<div class="footer--links--col--title">Company</div>
						<ul>
							<li><a href="{{route('about')}}">About</a></li>
							<li><a href="{{route('blog-index')}}">Blog</a></li>
							<li><a href="http://learn.healthpro.com" target="_blank">Learn</a></li>
							<li><a href="{{route('partner')}}">Partners</a></li>
							<li><a href="{{route('affiliate')}}">Affiliates</a></li>
						</ul>
					</div>
					<div class="footer--links--col">
						<div class="footer--links--col--title">Connect</div>
						<ul>
							<li><a href="{{ Setting::get('social_facebook') }}" target="_blank" class="m-icon m-facebook">Facebook</a></li>
							<li><a href="{{ Setting::get('social_twitter') }}" target="_blank" class="m-icon m-twitter">Twitter</a></li>
							<li><a href="{{ Setting::get('social_googleplus') }}" target="_blank" class="m-icon m-gplus">Google+</a></li>
							<li><a href="{{ Setting::get('social_linkedin') }}" target="_blank" class="m-icon m-linkedin">LinkedIn</a></li>
							<li><a href="{{ Setting::get('social_youtube') }}" target="_blank" class="m-icon m-youtube">YouTube</a></li>
						</ul>
					</div>
					<div class="footer--links--col">
						<div class="footer--links--col--title">Help</div>
						<ul>
							<li><a href="{{URL::route('help', ['sectionSlug' => 'professional', 'categorySlug' => 'getting-started'])}}">FAQ</a></li>
							{{--<li><a href="https://healthprohelp.zendesk.com/" rel="external">FAQ</a></li>--}}
							<li><a href="{{URL::route('page.show', ['slug' => 'contact-us'])}}">Contact Us</a></li>
							<li><a href="{{URL::route('page.show', ['slug' => 'privacy'])}}">Privacy Policy</a></li>
							<li><a href="{{URL::route('page.show', ['slug' => 'terms'])}}">Terms</a></li>
						</ul>
					</div>
				</div>
				<div class="footer--powered">
					<span id="siteseal" class="godaddy">
						<img src="{{ asset('assets/frontend/images/content/powered/godaddy@2x.png') }}" width="180" class="godaddy--improved_img" alt="SSL site seal - click to verify">
						<script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=vjkHfgpmL0SeAGIZmWb8Yrtz1JUbnlgnAmSu4243dubD4mDp2b8dHiKps6gJ"></script>
					</span>
					<span class="stripe"><a href="https://stripe.com" target="_blank"><img src="{{ asset('assets/frontend/images/content/powered/stripe@2x.png') }}" width="160" alt="Powered by Stripe"></a></span>
				</div>
			</div>
			<div class="footer--top--right">
				<div class="footer--newsletter">
					<div class="footer--top--title">Newsletter Sign Up</div>
					<p>Sign up to receive weekly professional tips for running your business, features and articles</p>
					<div class="footer--newsletter--form" data-cs-form="newsletter"></div>
				</div>
			</div>
		</div>
		<div class="footer--bottom">
			<div class="wrap">
				<p class="footer--copy">
					<span>©</span> HealthPRO, Inc 2014 - {{ date('Y') }}. All Rights Reserved.
					<a href="{{URL::route('page.show', ['slug' => 'terms'])}}">Terms</a> &amp;
					<a href="{{URL::route('page.show', ['slug' => 'privacy'])}}">Privacy Policy</a>
				</p>
				<a href="{{route('home')}}" class="footer--logo"></a>
			</div>
		</div>
	</footer>
</div>
@unless (isset($blockSignup) && $blockSignup)
	@include('site.auth.popup')
@endunless
@include('site.membership.popup')

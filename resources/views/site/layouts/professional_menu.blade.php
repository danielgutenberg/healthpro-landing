@extends('site.layouts.root')

@section('root-content')
	<div class="layout m-front m-pros @yield('wrapper-class')">
		<div class="layout--header">
			<header class="header m-pros @yield('header-class')">
				<div class="header--logo">
					<a href="{{route('home')}}">
						<span>{{ __( Setting::get('site_title') ) }}</span>
					</a>
				</div>
				<div class="header--content">
					<div class="header--left">
						{!! MenuHelper::render('frontend-landing-professional-top-left-nav', ['attr' => 'class="header--nav"']) !!}
					</div>
					<div class="header--right">
						@unless(Sentinel::check())<a class="header--call" href="tel:+18664325847">1866-HEALTHPRO</a>@endif
						@if($_PTYPE != "client")
							{!! MenuHelper::render('frontend-landing-professional-top-right-nav', ['attr' => 'class="header--nav"']) !!}
						@endif
						@if(Sentinel::check())
							<nav class="header--user">
								{!! MenuHelper::render('frontend-header-user-menu') !!}
							</nav>
						@else
							{!! MenuHelper::render('frontend-landing-professional-top-right-nav-auth', ['attr' => 'class="header--nav"']) !!}
						@endif
					</div>
				</div>
				<i class="header--lines"><i></i></i>
				<a href="#" aria-label="Homepage" class="header--toggler" data-slideout data-options='{"slideout": ".layout--slideout", "mask":".layout--mask"}'></a>
			</header>
		</div>
		<div class="layout--mask"></div>
		<div class="layout--slideout">
			<div class="slideout--user">
				{!! MenuHelper::render('frontend-header-user-menu', ['user_menu_template' => 'menu.user-menu-slideout']) !!}
			</div>
			{!! MenuHelper::render('frontend-landing-professional-top-mobile-nav', ['attr' => 'class="slideout--nav m-front"']) !!}
		</div>
		<div class="layout--body">
			@yield('content')
		</div>
		@include('site.layouts.footer', ['blockSignup' => $blockSignup ?? false])
	</div>
@stop

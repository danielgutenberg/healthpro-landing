<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>@section('title'){{ MetaService::siteTitle()}}@show</title>

	<meta name="description" content="@section('meta-description'){{ __( Setting::get('site_description') ) }}@show">
	<meta name="keywords" content="@yield('meta-keywords')">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<script src="//use.typekit.net/bbt2uyw.js"></script>
	<script>try {Typekit.load();} catch (e) {}</script>

	<link rel="stylesheet" href="{{ asset('assets/frontend/styles/common.css') }}">

	<link rel="icon" type="image/png" href="{{ asset('assets/frontend/images/favicon/favicon-32x32.png') }}" sizes="32x32"/>

	@yield('styles')
	{!!Setting::get('site_header_scripts')!!}
</head>
<body class="@yield('body-class')">

<div class="layout m-front @yield('wrapper-class')">
	<div class="layout--header">
		<header class="header @yield('header-class')">
			<div class="header--left">
				<div class="header--logo">
					<span>{{ __( Setting::get('site_title') ) }}</span>
				</div>
			</div>
			<i class="header--lines"><i></i></i>
		</header>
	</div>

	<div class="layout--body">
		@yield('content')
	</div>

	<!-- <div class="layout--footer">
		<footer class="footer">

			<div class="footer--bottom">
				<div class="wrap">
					<p class="footer--copy">
						<span>©</span> {{ Setting::get('landing_copyright') }}
					</p>
					<ul class="footer--social">
						<li>
							<a href="{{ Setting::get('social_facebook') }}" class="btn m-social m-facebook" role="external" target="_blank">Facebook</a>
						</li>
						<li>
							<a href="{{ Setting::get('social_twitter') }}" class="btn m-social m-twitter" role="external" target="_blank">Twitter</a>
						</li>
						<li>
							<a href="{{ Setting::get('social_linkedin') }}" class="btn m-social m-linkedin" role="external" target="_blank">LinkedIn</a>
						</li>
						<li>
							<a href="{{ Setting::get('social_googleplus') }}" class="btn m-social m-googleplus" role="external" target="_blank">Google+</a>
						</li>
						<li>
							<a href="{{ Setting::get('social_youtube') }}" class="btn m-social m-youtube" role="external" target="_blank">YouTube</a>
						</li>
					</ul>
				</div>
			</div>
		</footer>
	</div> -->
</div>
</body>
</html>

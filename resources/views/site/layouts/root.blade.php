<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>@section('title'){{ MetaService::siteTitle()}}@show</title>

	<meta name="description" content="@section('meta-description'){{ __( Setting::get('site_description') ) }}@show">
	<meta name="keywords" content="@section('meta-keywords'){{ __( Setting::get('site_keywords') ) }}@show">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<script src="https://use.typekit.net/iql2urw.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

	<link rel="stylesheet" href="{{ asset('assets/frontend/styles/common.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/frontend/styles/front.css') }}">

	<link rel="icon" type="image/png" href="{{ asset('assets/frontend/images/favicon/favicon-32x32.png') }}" sizes="32x32"/>

	@yield('styles')
	{!!Setting::get('site_header_scripts')!!}

	{{TrackingHelper::code(['hotjar', 'fbpixel'])}}
</head>
<body class="m-mobile_ready @yield('body-class')">

{{TrackingHelper::code('tag-manager')}}

@yield('root-content')

@yield('popups')

<script>
	window.GLOBALS = {
		@yield('js-globals')
		_ASSETS_VERSION: '{{Manifest::assetsVersion()}}',
		_TOKEN: '{{Session::token()}}',
		_UID: ('{{$_UID}}' === '')? null : '{{$_UID}}',
		_UROLE: ('{{$_UROLE}}' === '')? null : '{{$_UROLE}}',
		_PID: ('{{$_PID}}' === '')? null : '{{$_PID}}',
		_PTYPE: ('{{$_PTYPE}}' === '')? null : '{{$_PTYPE}}',
	}
	@yield('js-scripts')
</script>

<script src="{{ asset('assets/frontend/scripts/common.js') }}"></script>
<script src="@yield('script_main', asset('assets/frontend/scripts/front.js'))"></script>
@yield('scripts')

{!!Setting::get('site_footer_scripts')!!}
</body>
</html>

@extends('site.layouts.root')

@section('root-content')
	<div class="layout m-front m-pros @yield('wrapper-class')">
		<div class="layout--header">
			<header class="header m-pros @yield('header-class')">
				<div class="header--logo">
					<a
						@if(Sentinel::check())
							href="{{route('dashboard-schedule')}}"
						@else
							href="{{route('home')}}"
						@endif
					>
						<span>{{ __( Setting::get('site_title') ) }}</span>
					</a>
				</div>
				<div class="header--content">
					<div class="header--left">
						{!! MenuHelper::render('frontend-landing-professional-top-left-nav', ['attr' => 'class="header--nav"']) !!}
					</div>
					<div class="header--right">
						@unless(Sentinel::check())<a class="header--call" href="tel:+18664325847">1866-HEALTHPRO</a>@endunless
						{!! MenuHelper::render('frontend-landing-professional-top-right-nav', ['attr' => 'class="header--nav"']) !!}
						@if(\WL\Modules\Profile\Facades\ProfileService::getCurrentProfile())
							<nav class="header--user">
								{!! MenuHelper::render('frontend-header-user-menu') !!}
							</nav>
						@else
							{!! MenuHelper::render('frontend-landing-professional-top-right-nav-auth', ['attr' => 'class="header--nav"']) !!}
						@endif
					</div>
				</div>
				<i class="header--lines"><i></i></i>
				<a href="#" aria-label="Homepage" class="header--toggler" data-slideout data-options='{"slideout": ".layout--slideout", "mask":".layout--mask"}'></a>
			</header>
		</div>
		<div class="layout--mask"></div>
		<div class="layout--slideout">
			<div class="slideout--user">
				{!! MenuHelper::render('frontend-header-user-menu', ['user_menu_template' => 'menu.user-menu-slideout']) !!}
			</div>
			{!! MenuHelper::render('frontend-landing-professional-top-mobile-nav', ['attr' => 'class="slideout--nav m-front"']) !!}
		</div>
		<div class="layout--body">
			@yield('content')
		</div>
		@include('site.layouts.footer')
	</div>
@stop

@section('js-globals')
	@if(App::environment() === 'local')_ENV: '{{App::environment()}}', @endif
@append

@section('popups')
	<div class="popup demo_request" data-popup="demo_request">
		<div class="popup--container" data-popup-container>
			<button class="popup--close" data-popup-close>close</button>
			<div class="demo_request--wrapper">
				<div class="demo_request--header">
					<header class="demo_request--header-inner">
						<h2 class="demo_request--header-inner-p1">Get a personal demo of HealthPRO’s</h2>
						<h2 class="demo_request--header-inner-p2">all-in-one business  management platform.</h2>
					</header>
				</div>
				<div class="popup--content demo_request--content">
					<div class="demo_request--content-inner">
						<header class="demo_request--content-header">
							<h1 class="demo_request--title">Sign Up for a Live Demo</h1>
							<div class="demo_request--text">Discover HealthPRO for Yourself with a Fast, Informative and
								Free Demo.
							</div>
						</header>
						<div data-cs-form="demo_request" class="partner_contact--form"></div>
						<div class="loading_overlay m-light m-hide"><span class="spin m-logo"></span></div>
					</div>
				</div>
			</div>
		</div>
		<div class="popup--bg"></div>
	</div>
@append

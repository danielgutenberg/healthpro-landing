@extends('site.layouts.root')

@section('root-content')
	<div class="layout m-front @yield('wrapper-class')">
		<div class="layout--header">
			<header class="header @yield('header-class')">
				<div class="header--in">
					<div class="header--logo">
						<a href="{{route('home')}}">
							<span>{{ __( Setting::get('site_title') ) }}</span>
						</a>
					</div>
					<i class="header--lines"><i></i></i>
					<a href="#" aria-label="Homepage" class="header--toggler" data-slideout data-options='{"slideout": ".layout--slideout", "mask":".layout--mask"}'></a>
				</div>
			</header>
		</div>
		<div class="layout--mask"></div>
		<div class="layout--slideout">
			<div class="slideout--user">
				{!! MenuHelper::render('frontend-header-user-menu', ['user_menu_template' => 'menu.user-menu-slideout']) !!}
			</div>
			{!! MenuHelper::render('frontend-landing-professional-top-mobile-nav', ['attr' => 'class="slideout--nav m-front"']) !!}
		</div>
		<div class="layout--body">
			@yield('content')
			<a href="#" class="scroll_top">Go top</a>
		</div>
		@include('site.layouts.footer')
	</div>
@stop

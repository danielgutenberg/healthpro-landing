@extends('site.layouts.default')
@section('html-class')
m-landing
@stop

@section('content')
	<div class="promo m-landing" id="subscribe-form">
		<div class="promo--inner">
			<div>{!! __( 'Get More Clients for <strong>Your Health</strong> &amp; <strong>Wellness Business!</strong>' ) !!}</div>
			<p>{!! __( 'Let HealthPRO help you successfully promote your services so you can get back to your passion - helping people live healthier, happier lives.' ) !!}</p>

			<div class="promo--subscribe">
				<form class="promo--subscribe_form js-subscribe_form">
					<label class="field">
						<input type="text" name="email" placeholder="{{ __('Enter your email') }}"></input>
					</label>
					<button type="submit" class="btn m-green js-subscribe_form--submit">GET NOTIFIED WHEN WE LAUNCH!</button>
					<div class="promo--subscribe_form--message"></div>
				</form>
				<div class="promo--subscribe_hidden">
					<iframe name="hubspot_subscribe" id="hubspot_subscribe"></iframe>
				</div>
			</div>
			{{-- {!! Former::open()->id('subscribe-form')->class('promo--subscribe form')->action( route('landing-subscribe') ) !!}
				<fieldset>
					<button class="btn m-green" type="submit">
						<span class="spinner"></span>
						<span>{{ __('Get Notified When We Launch!') }}</span>
					</button>
					<label class="field"><input type="email" name="email" placeholder="{{ __('Enter your email') }}"></label>
				</fieldset>
			{!! Former::close() !!} --}}

			@if ($message = Session::get('success'))
				<div class="section-main__message m-success">
					<p>{{ $message }}</p>
				</div>
			@endif
			@if ($message = Session::get('error'))
				<div class="section-main__message m-error">
					<p>{{ $message }}</p>
				</div>
			@endif
		</div>
	</div>

	<div class="benefits">
		<div class="benefits--inner">
			<div>{!! __("Sign Up With <strong>HealthPRO</strong> and Get <strong>Super Powers</strong><br/>  to Manage Your Health Business.") !!}</div>
			<ul class="benefits--list">
				<li class="benefits--list--item m-marketing">
					<dl>
						<dt>{!! __('All-In-One Marketing Suite') !!}</dt>
						<dd>{!! __('HealthPRO gives you a suite of marketing tools such as website widgets, social sharing features and more.') !!}</dd>
					</dl>
				</li>
				<li class="benefits--list--item m-billing">
					<dl>
						<dt>{!! __('Hassle-Free Billing') !!}</dt>
						<dd>{!! __("HealthPRO handles all your billing stress free, so you don't have to. You'll get paid - on time, every time.") !!}</dd>
					</dl>
				</li>
				<li class="benefits--list--item m-booking">
					<dl>
						<dt>{!! __('Instant Booking') !!}</dt>
						<dd>{!! __('Our state-of-the-art scheduling system allows you to accept and manage appointments online in real time.') !!}</dd>
					</dl>
				</li>
			</ul>
			<!-- <div class="benefits--sign_up">
				<a href="#subscribe-form" data-goto="subscribe-form" data-offset="50" data-focus="[name=email]" class="btn m-red m-border">{!! __('Sign up Now') !!}</a>
			</div> -->
			<div class="benefits--hippa">
				<p>
					{!! sprintf(__('HealthPRO is %s HIPAA Compliant'), '<img src="/assets/front/images/design/landing/hippa-blue.png" width="46" height="52" alt="">') !!}
				</p>
			</div>
		</div>
	</div>
	<div class="promo m-video_section">
		<div class="promo--inner">
			<div>{!! __('Learn More About HealthPRO') !!}</div>
			<p>{!! __('Click Play to Watch The Short Video') !!}</p>
			<button class="promo--play_button" type="button" title="play video" data-toggle="popup" data-target="popup_video">&nbsp;</button>
		</div>
	</div>
	<!-- <div class="join_us">
		<div>{!! __('Our website will be fully functional shortly.') !!}</div>
		<p>{!! __('Sign up to be among the first healthcare providers <br>to gain access to our program!') !!}</p>
		<a href="#subscribe-form" data-goto="subscribe-form" data-offset="50" data-focus="[name=email]" class="btn m-orange">{!! __('Sign up to our email list!') !!}</a>
	</div> -->
@stop
@section('contact-form--title')Want more information? Introduce yourself. @stop

@section('popups')
<div class="popup popup--video" data-popup="popup_video" data-options='{
	"type": "video",
	"videoUrl": "https://www.youtube.com/embed/YPZ8bpGndPo",
	"videoSizes": {
		"width": 630,
		"height": 355
		}
	}'>
	<div class="popup--container m-video_container" data-popup-container>
		<div data-authpopup>
			<button class="popup--close m-video_close" data-popup-close>close</button>
			<div class="popup_video--description">
				<!-- <iframe id="videoLearnMore" width="560" height="315" src="https://www.youtube.com/embed/YPZ8bpGndPo" frameborder="0" allowfullscreen></iframe> -->
			</div>
		</div>
	</div>
	<div class="popup--bg" data-popup-close></div>
</div>
@append

@if (Session::has('gift_certificate_declined'))
	@section('js-scripts')
		window.DISPLAY_ALERT = "{{_('This certificate cannot be used. Please contact the professional.')}}"
	@endsection
@endif
@if (Session::has('gift_certificate_redeemed'))
	@section('js-scripts')
		window.DISPLAY_ALERT = "{{_('The gift certificate has already been redeemed.')}}"
	@endsection
@endif

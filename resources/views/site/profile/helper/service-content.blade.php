@if (count($packages) || count($reccuringPackages) || count($sessions) || !empty($service['description']))
	<div class="book_services--item-content" data-booking-item-content>
		@unless(empty($service['description']))
			<div class="book_services--item-description">
				{{ $service['description'] }}
			</div>
		@endunless

		@if (count($sessions) || count($packages))

			<div class="book_services--item-cols">
				<div class="book_services--item-col m-session">
					@if(count($sessions))
						<div class="book_services--item-packages m-session">
							<h4>Sessions</h4>
							<ul>
								@foreach($sessions as $session)
									<li>
										{{$session['duration']}} for <strong>{{ $session['price'] }}</strong>
									</li>
								@endforeach
							</ul>
						</div>
					@endif
					@if (count($packages))
						<div class="book_services--item-packages m-standard">
							<h4>Packages</h4>
							<ul>
								@foreach($packages as $package)
									<li>
										{{$package['sessions']}} x {{$package['duration']}} for <strong>{{ $package['price'] }}</strong> <strong class="m-sale">Save {{$package['sale']}}</strong>
									</li>
								@endforeach
							</ul>
						</div>
					@endif
				</div>
				<div class="book_services--item-col m-book">
					<button class="book_services--item-book btn m-small m-wide m-red" data-booking-opener>Book Now</button>
				</div>
			</div>

		@endif

		@if(count($reccuringPackages))
			<div class="book_services--item-cols">
				<div class="book_services--item-col m-session">
					<div class="book_services--item-packages m-recurring">
						<h4>Monthly Packages</h4>
						<ul>
							@foreach($reccuringPackages as $package)
								<li>
									{{$package['duration']}} session {{$package['frequency']}} for <strong>{{ $package['price'] }}</strong> ({{$package['months']}})
								</li>
							@endforeach
						</ul>
					</div>
				</div>
				<div class="book_services--item-col m-book">
					<button class="book_services--item-book btn m-small m-wide m-recurring" data-recurring-opener>Book Monthly Package</button>
				</div>
			</div>
		@endif
	</div>
@endif

@if($fromPrice)
	<div class="book_services--item-price" data-session_id="{{$session->id}}">
		from
		@if (intval($fromPrice) == $fromPrice)
			<strong>{{ money_format('%.0n', $fromPrice) }}</strong>
		@else
			<strong>{{ money_format('%n', $fromPrice) }}</strong>
		@endif
		@if ($displayTime)
			for
			@if ($displayTime['h']) <span>{{ $displayTime['h'] }} {{$displayTime['h'] > 1 ? 'hrs' : 'hr'}}</span> @endif
			@if ($displayTime['m']) <span>{{ $displayTime['m'] }} min</span> @endif
		@endif
	</div>
@endif

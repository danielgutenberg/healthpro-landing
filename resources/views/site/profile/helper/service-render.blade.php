<div class="book_services--item {{$hasPackages ? 'm-has_packages' : ''}} {{$editMode ? 'm-edit' : ''}}" data-booking-item data-service_id="{{$service->id}}">
	<div class="book_services--item-header" data-booking-item-header>
		<div class="book_services--item-details">
			<div class="book_services--item-name">
				{{ $primaryServiceType }}
				{!! ProviderServiceHtml::showPackagesLabel($service) !!}
			</div>
			@if ($secondaryServiceTypes)
				<h4 class="book_services--item-sub-name">{{ $secondaryServiceTypes }}</h4>
			@endif
			{!! ProviderServiceHtml::showServiceLocations($service) !!}
		</div>

		<div class="book_services--item-prices">
			{!! ProviderServiceHtml::showFromPrice($service, true) !!}
			{!! ProviderServiceHtml::showFromPriceMonthly($service) !!}
		</div>

		<div class="book_service--item-cta">
			<button class="book_services--item-cta btn m-small">Book Now</button>
		</div>
	</div>
	{!! ProviderServiceHtml::showServiceContent($service) !!}
</div>

@if (Session::has('cancelled_appointment'))
	<div class="alerts m-sticky">
		<div class="alert m-warning">
			<div>{{_('We are sorry but your appointment has been cancelled.')}}</div>
			@if ($manualPasswordRequired)
				<div>{{_('To finish setting up your account please set your password.')}}</div>
				<a target="_blank" href="{{ route('dashboard-settings-account') }}" class="btn m-small m-blue">{{_('Set Password')}}</a>
			@endif
		</div>
	</div>
@endif
@if (Session::has('confirmed_appointment'))
	<div class="alerts m-sticky">
		<div class="alert m-warning">
			<div>{{_('You have already confirmed this appointment.')}}</div>
		</div>
	</div>
@endif
@if (Session::has('past_appointment'))
	<div class="alerts m-sticky">
		<div class="alert m-warning">
			<div>{{_('Sorry, but the date for this appointment has already passed.')}}</div>
		</div>
	</div>
@endif

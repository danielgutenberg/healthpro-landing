<ul class="book_services--item-locations">
	@foreach($locations as $location)
		@php
			switch ($location->location_type_id) {
			case 1:
				$classname = 'office';
				break;
			case 2:
				$classname = 'home-visit';
				break;
			case 3:
				$classname = 'virtual';
			}
		@endphp
		<li class="m-{{ $classname }}">{{ $location->name }}</li>
	@endforeach
</ul>

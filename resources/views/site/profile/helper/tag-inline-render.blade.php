<span class="{{$cssClass}}">
	@foreach($tags as $k => $tag)
		{{$tag->name}}{{$k < (count($tags)-1) ? ',' : ''}}
	@endforeach
</span>

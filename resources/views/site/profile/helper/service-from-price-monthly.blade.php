@if($fromPrice && $session)
	<div class="book_services--item-price" data-session_id="{{$session->id}}">
		monthly packages from
		@if (intval($fromPrice) == $fromPrice)
			<strong>{{ money_format('%.0n', $fromPrice) }}</strong>
		@else
			<strong>{{ money_format('%n', $fromPrice) }}</strong>
		@endif
	</div>
@endif

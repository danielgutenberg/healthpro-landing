@extends('site.layouts.default')

@section('content')
	<div class="profile m-client">
		<div class="profile--main" data-clients-profile>
			<div class="profile--back">
				<a href="/dashboard/my-clients" class="profile--back--link">Back to my clients</a>
			</div>
			<div class="content_block profile_content_block">
				<div class="profile--block profile_card">
					<div class="profile_card--photo">
						<div class="profile_card--photo--userpic">
							<figure class="userpic m-xxlarge m-rounded">
									<span class="userpic--inner">
										@if($profile->avatar)
											<img src="{{ $profile->avatar }}" alt="">
										@endif
									</span>
							</figure>
							@if ($profile->verified)
								<span class="profile_card--verified">verified</span>
							@endif
						</div>
					</div>
					<div class="profile_card--main">
						<header class="profile_card--header">
							<div class="profile_card--header_name">
								<div>{{$profile->first_name . ' ' . $profile->last_name}}</div>
							</div>
						</header>

						@if(isset($profile->age) and $profile->age)
							<dl class="profile_card--info--line">
								<dt>Age:</dt>
								<dd>{{$profile->age}}</dd>
							</dl>
						@endif

						@if (isset($profile->gender) and $profile->gender)
							<dl class="profile_card--info--line">
								<dt>Gender:</dt>
								<dd>{{ ucfirst($profile->gender) }}</dd>
							</dl>
						@endif

						@if(isset($profile->occupation) and $profile->occupation)
							<dl class="profile_card--info--line">
								<dt>Occupation:</dt>
								<dd>{{ ucfirst($profile->occupation) }}</dd>
							</dl>
						@endif

						@php
							$profilePhone = '';
							if ($profile->phone) {
								$profilePhone = $profile->phone;
								if (strlen($profilePhone) == 10) {
									$profilePhone = '1'.$profile->phone;
								}

								$profilePhone = substr($profilePhone,0,1)." (".substr($profilePhone,1,3).") ".substr($profilePhone,4,3)."-".substr($profilePhone,7);
							}
						@endphp

						@if ($profilePhone)
							<dl class="profile_card--info--line">
								<dt>Phone:</dt>
								<dd>+{{ $profilePhone }}</dd>
							</dl>
						@endif

						<div class="profile_card--info">
							@if (count(array_filter($profile->social_pages)))
								<dl class="profile_card--info--line m-contacts">
									<dt>Social:</dt>
									<dd>
										<ul class="profile_card--info--contacts">
											@foreach ($profile->social_pages as $link => $value)
												@if(!empty($value))
													<li>
														<a href="{{$value}}" target="_blank">
															<button class="btn m-social m-{{$link}}" type="button">{{$link}}</button>
														</a>
													</li>
												@endif
											@endforeach
										</ul>
									</dd>
								</dl>
							@endif

							@if($currentProfile && $currentProfile->type == 'provider')

								<dl class="profile_card--info--line m-contacts">
									<dt>Actions:</dt>
									<dd>
										<div data-profile-actions>
											@if ($canMessage)
												<div class="profile_card--info--line m-actions">
													<a href="{{route('dashboard-conversations')}}#to:{{$profile->id}}" data-message class="btn m-green m-contact m-email m-medium">Send Message</a>
												</div>
											@endif
											<div class="profile_card--info--line m-actions">
												@if (!$currentProfile->locations->where('location_type_id', 2)->isEmpty())
													<button class="btn profile_card--info--home_visits {{ $rangeExemption ? 'm-active' : '' }}" type="button" name="button" data-toggle-meta data-meta="range_exemption" data-tooltip='dark m-much_text' title="Allow client to book a home visit appointment"></button>
												@endif
													<button class="btn profile_card--info--introductory {{ $introSessionExcemption ? 'm-active' : '' }}" type="button" name="button" data-toggle-meta data-meta="intro_session_exemption" data-tooltip='dark m-much_text' title="Allow client to book an appointment without scheduling an introductory appointment."></button>
											</div>
										</div>
									</dd>
								</dl>
							@endif


						</div>

					</div>



					{{-- <div class="profile_card--side">
						<header class="profile_card--side--header m-client">
							<div class="profile_card--side--title">Client Measurements</div>
 							<small>Last updated on mm/dd/yy</small>
						</header>
						<div class="profile_card--measurements">
							<ul class="profile_card--indexes">
								<li class="profile_card--indexes--item">
									<div class="profile_card--indexes--item--label">Weight</div>
									<div class="profile_card--indexes--item--value">
										86.4 KG
										<button class="profile_card--indexes--item--edit" name="button">Edit</button>
									</div>
								</li>
								<li class="profile_card--indexes--item">
									<div class="profile_card--indexes--item--label">Height</div>
									<div class="profile_card--indexes--item--value">
										6 FT 8 IN
										<button class="profile_card--indexes--item--edit" name="button">Edit</button>
									</div>
								</li>
								<li class="profile_card--indexes--item">
									<div class="profile_card--indexes--item--label">Waist</div>
									<div class="profile_card--indexes--item--value">
										36 IN
										<button class="profile_card--indexes--item--edit" name="button">Edit</button>
									</div>
								</li>
								<li class="profile_card--indexes--item">
									<div class="profile_card--indexes--item--label">Chest</div>
									<div class="profile_card--indexes--item--value">
										40 IN
										<button class="profile_card--indexes--item--edit" name="button">Edit</button>
									</div>
								</li>
								<li class="profile_card--indexes--item">
									<div class="profile_card--indexes--item--label">BMI</div>
									<div class="profile_card--indexes--item--value">
										36.9
										<button class="profile_card--indexes--item--edit" name="button">Edit</button>
									</div>
								</li>
							</ul>
							<ul class="profile_card--units">
								<li class="profile_card--units--switcher">
									<span class="profile_card--units--switcher--item">
										<input type="radio" name="unit_weight" id="unit_weight_kg" value="kg" checked>
										<label for="unit_weight_kg">KG</label>
									</span>
									<span class="profile_card--units--switcher--item">
										<input type="radio" name="unit_weight" id="unit_weight_lb" value="lb">
										<label for="unit_weight_lb">LB</label>
									</span>
								</li>
								<li class="profile_card--units--switcher">
									<span class="profile_card--units--switcher--item">
										<input type="radio" name="unit_length" id="unit_length_cm" value="cm" checked>
										<label for="unit_length_cm">CM</label>
									</span>
									<span class="profile_card--units--switcher--item">
										<input type="radio" name="unit_length" id="unit_length_in" value="in">
										<label for="unit_length_in">IN</label>
									</span>
								</li>
							</ul>
						</div>
						<div class="profile_card--side--actions">
							<a class="profile_card--side--actions--link" href="#">View last 6 month</a>
							<button class="btn_frame m-red m-small profile_card--side--actions--btn" name="button">Update</button>
						</div>
					</div>--}}
				</div>
				<span class="loading_overlay m-light m-hide" data-meta-loading><span class="spin m-logo"></span></span>
			</div>
			<div class="content_block profile_content_block profile_history"></div>
			<div class="content_block profile_content_block profile_stats"></div>
			<div class="content_block profile_content_block profile_packages m-hide"></div>

			<div class="content_block profile_content_block">

				<div class="reviews m-client">
					<div class="reviews--main">
						<div class="reviews--title">Reviews by {{$profile->first_name . ' ' . $profile->last_name}} <span>({{$commentsCount}})</span></div>
						@if ($reviews)
							<ul class="reviews--list">
								@foreach ($reviews as $review)
									<li class="reviews--list--item">
										<header class="reviews--list--item--header">
											<figure class="userpic m-circle m-s">
										<span class="userpic--inner">
												@if($review->providerAvatar)
												<img src="{{ $review->providerAvatar }}" alt="">
											@endif
										</span>
											</figure>

											<p class="reviews--list--item--header--meta">
												on <a href="{{ route('profile-show', ['id' => $review->elm->slug])  }}"><strong>{{$review->providerName}}</strong></a> {{ ($review->daysAgo==0) ? __('today') : _n("yesterday", "{$review->daysAgo} days ago", $review->daysAgo) }}
											</p>

											@if ($rating = $review->overallRating)
												<div class="reviews--list--item--header--rating">
													<span>Overall Score</span>
													<span class="star_rating m-{{ $rating->present()->valueToWord() }} m-s">
												<span class="star_rating--star"></span>
												<span class="star_rating--star"></span>
												<span class="star_rating--star"></span>
												<span class="star_rating--star"></span>
												<span class="star_rating--star"></span>
											</span>
													<span>({{$rating->countOfRatings}})</span>
												</div>
											@endif
										</header>

										@foreach ($review->comments as $comment)
											<div class="reviews--list--item--content">
												<p class="paragraph">{{$comment->content}}</p>
											</div>
										@endforeach
										@unless ($_PTYPE == 'provider')
											<div class="reviews--list--item--helpful">
												<div class="reviews--list--item--helpful--inner">
												</div>
												<div class="reviews--list--item--helpful--loading loading_overlay m-light m-hide"><span class="spin m-logo"></span></div>
											</div>
										@endunless
									</li>
								@endforeach
							</ul>
						@else
							<div class="reviews--rating m-empty">
								No reviews yet
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('js-globals')
OWN_USER: '{{isset($ownUser) ? $ownUser : false}}',
PROFILE_ID: '{{isset($profile->id) ? $profile->id : false}}',
CAN_MESSAGE: '{{isset($canMessage) ? $canMessage : false}}',
@stop
<script type="text/javascript">
	window.reviews = [
			@foreach($reviews as $review) {
				@if ($review->helpfulRating)
					"helpful": "{{$review->helpfulRating->rating_value}}",
					"rated": "{{$review->helpfulRating->currentProfileRatedValue}}",
				@else
					"helpful": 0,
					"rated": "",
				@endif
				"provider_id": "{{$review->elm->id}}",
				"id": "{{$review->id}}"
		},
		@endforeach
	]
	@if (!empty($currentProfile))
		window.currentProfile = {{$currentProfile->id}}
	@else
		window.currentProfile = 0
	@endif
</script>

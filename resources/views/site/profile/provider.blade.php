@extends('site.layouts.default')

@section('title')
	{{$profile->first_name . ' ' . $profile->last_name}} | View this HealthPRO’s Profile and Book with {{$profile->first_name}} Online.
@stop
@section('meta-description')Get detailed information and schedule an appointment with {{$profile->first_name . ' ' . $profile->last_name}}{{$serviceDescription}} | HealthPRO helps you easily find and book with independent health and wellness professionals in your area.@stop
@section('meta-keywords'){{$profile->last_name}}, {{$profile->first_name}}@foreach($serviceNames as $service), {{$service}}@endforeach @foreach($locationNames as $location), {{$location}}@endforeach @stop
@section('meta-tags-additional')
	<meta property="og:type"            content="product" />
	<meta property="og:url"             content="{{ $profile->public_url }}" />
	<meta property="og:title"           content="Check out my profile on HealthPRO" />
	<meta property="og:description"     content="The platform for health & fitness professionals and their clients" />
	<meta property="og:image"           content="{{ url('assets/frontend/images/design/sharing/facebook-share-cover.jpg') }}" />
	<meta property="og:image:width"     content="1200" />
	<meta property="og:image:height"     content="630" />
@append

@if ($isOwnProfile)
	@section('header-right-links')
		<a class="btn m-small m-mid_gray_border m-bars" href="{{ route('dashboard-home' ) }}">Dashboard</a>
	@stop
@endif

@section('content')

	@if ($shouldReview)
		@include('site.profile.partials.review-prompt', [
			'profile' => $profile,
			'appointment' => $clientLastAppointment
		])
	@endif
	@include('site.profile.helper.cancelled-appointment')
	@include('site.profile.helper.gift-certificate-declined')
	<div class="profile wrap" data-profile {{ $editMode ? 'data-profile-edit' : '' }}>
		<div class="profile--main">
			@include('site.profile.partials.card', [
				'profile' => $profile,
				'editMode' => $editMode,
				'overallRating' => $overallRating,
				'commentsCount' => $commentsCount,
				'isOwnProfile' => $isOwnProfile,
				'currentProfile' => $currentProfile,
				'hasGiftCertificates' => $hasGiftCertificates,
				'services' => $services,

			])

			@include('site.profile.partials.about', [
				'profile' => $profile,
				'editMode' => $editMode,
			])

			@include('site.profile.partials.media', [
				'profile' => $profile,
				'editMode' => $editMode,
			])

			@if ($editMode)
				@include('site.profile.partials.cc', [
					'profile' => $profile,
				])
			@endif

			@include('site.profile.partials.services', [
				'profile' => $profile,
				'services' => $services,
				'editMode' => $editMode,
			])

			@include('site.profile.partials.conditions-concerns', [
				'profile' => $profile,
				'editMode' => $editMode,
			])

			@include('site.profile.partials.working-hours', [
				'profile' => $profile,
				'locations' => $locations,
				'editMode' => $editMode,
			])

			@include('site.profile.partials.qualifications', [
				'profile' => $profile,
				'editMode' => $editMode,
			])

		</div>

		@if(!$editMode)
			<div class="profile--sidebar" data-profile-sidebar>
				<div class="content_block profile--sidebar--book" data-profile-book>
					@if ($bookable)
						<div class="book_appointment" data-options='{"type": "widget"}'></div>
					@else
						<div class="not_searchable">
							<div class="booking_widget--header">
								<div class="booking_widget--header--title">Book an Appointment Online</div>
								<span class="booking_widget--header--close" data-widget-close></span>
							</div>
							<div class="booking_widget--not_searchable">
								<p>This professional is not currently available for online booking.</p>
							</div>

						</div>
					@endif
				</div>
				<div class="content_block profile--sidebar--map map_widget m-hide">
					<div class="map_widget--inner"></div>
					<div class="map_widget--loading loading_overlay m-light"><span class="spin m-logo"></span></div>
				</div>
			</div>
		@endif

	</div>

	@include('site.profile.partials.reviews', [
		'commentsCount' => $commentsCount,
		'reviews' => $reviews,
		'overallRatings' => $overallRatings,
		'isOwnProfile' => $isOwnProfile,
		'isOwnUser' => isset($ownUser) ? $ownUser : false,
	])

	@if (!$editMode)
		<div class="profile_footer" data-profile-footer>
			@if ($profile->is_searchable)
			<button type="button" class="profile_footer--button btn m-red m-border m-wide" name="button" data-booking-opener>Book a Session Now</button>
			@endif
		</div>
	@endif

@stop

@section('popups')
	<div class="popup require_password require_password_popup" data-popup="require_password" id="popup_require_password"></div>
@append

@section('js-globals')
PROVIDER_ID: {{$profile->id}},
APPOINTMENT_ID: '{{$appointmentId ?: null }}',
SHOW_BOOKING_WIZARD: '{{$showBookingWizard ?: false}}',
OWN_USER: '{{isset($ownUser) ? $ownUser : false}}',
PAYPAL_MODE: '{{isset($paypalMode) ? $paypalMode : 'sandbox'}}',
@stop

<script type="text/javascript">
	window.reviews = [
		@foreach($reviews as $review) {
			@if ($review->helpfulRating)
				"helpful": "{{$review->helpfulRating->rating_value}}",
				"rated": "{{$review->helpfulRating->currentProfileRatedValue}}",
			@else
				"helpful": 0,
				"rated": "",
			@endif
				"id": "{{$review->id}}",
				"reviewedBy": "{{$review->profile_id}}"
		},
		@endforeach
	]
	@if (!empty($currentProfile))
		window.currentProfile = {{$currentProfile->id}}
	@else
		window.currentProfile = 0
	@endif
</script>

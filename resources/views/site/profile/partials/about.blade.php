@if ($profile->about_self_background || $profile->about_what_i_do || $profile->about_services_benefit || $profile->about_self_specialities || $editMode)
	<div class="profile--section" {!! $editMode ? 'data-edit="about"' : '' !!}>
		<div class="profile--main--title">About</div>
		<div class="content_block {{$editMode ? 'm-edit' : ''}}">
			<div class="about">
				<div class="about--main profile--block">
					<div class="about--text">
						@if ($profile->about_self_background)
							<div class="headline m-sm"><strong>About Me</strong></div>
							<p class="paragraph">
								{{$profile->about_self_background}}
							</p>
						@endif
						@if ($profile->about_what_i_do)
							<div class="headline m-sm"><strong>What I Do</strong></div>
							<p class="paragraph">
								{{$profile->about_what_i_do}}
							</p>
						@endif
						@if ($profile->about_services_benefit)
							<div class="headline m-sm"><strong>How My Services Will Benefit You</strong></div>
							<p class="paragraph">
								{{$profile->about_services_benefit}}
							</p>
						@endif
						@if ($profile->about_self_specialities)
							<div class="headline m-sm"><strong>What are My Specialities</strong></div>
							<p class="paragraph">
								{{$profile->about_self_specialities}}
							</p>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@endif

@if((isset($profile->tags['concerns_conditions']) && !($profile->tags['concerns_conditions']['tags']->isEmpty())) || $editMode)
	<div class="profile--section" {!! $editMode ? 'data-edit="conditions"' : '' !!}>
		<div class="profile--main--title">Conditions &amp; Concerns</div>
		<div class="content_block {{$editMode ? 'm-edit' : ''}}">
			<div class="profile--block">
				<div class="conditions" data-profile-conditions>
					<div class="profile--block--content m-pull">
						<div class="conditions--list" data-expand-list>
							{!! TaxonomyHtml::showTags('Concerns Conditions:', 'conditions--list--item', $profile->tags, 'concerns_conditions', 'site.profile.helper.tag-list-render') !!}
							<span class="conditions--list--opener btn m-more"><span>Show more</span><b></b></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endif

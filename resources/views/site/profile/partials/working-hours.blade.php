@if(count($locations) || $editMode)
	<div class="profile--section" {!! $editMode ? 'data-edit="locations"' : '' !!}>
		<div class="profile--main--title">Working Hours</div>
		<div class="content_block {{$editMode ? 'm-edit' : ''}}">
			<div class="profile--block">
				<div class="working_hours" data-working-hours>
					<ul class="working_hours--list">
						@foreach($locations as $location)
							<li class="working_hours--location m-{{$location->locationType->type}}">
								<div class="working_hours--location_name">{{$location->name}}</div>
								<address class="working_hours--location_address">
									@if ($location->virtualAddress)

									@elseif ($location->locationType->type == \WL\Modules\Location\Models\LocationType::HOME_VISIT && $location->address->address_label)
										<p>{{$location->address->address_label}}</p>
									@else
										<p>
											{{$location->address->address}}@if ($location->address->address_line_two), {{$location->address->address_line_two}} @endif
										</p>
										<p>
											{{$location->address->postalCode->city}}
											{{$location->address->postalCode->province}}
											{{$location->address->postalCode->code}}
										</p>
									@endif
								</address>
								<ul class="working_hours--location_availabilities">
									@foreach($location->availabilities as $availability)
										<li class="working_hours--location_availability">
											<span>{{(new Carbon())->next($availability->day - 1)->format('l')}}</span>
											<strong>{{$availability->from->format('H:i')}} - {{$availability->until->format('H:i')}}</strong>
										</li>
									@endforeach
								</ul>
								<span class="working_hours--timezones timezone_note">Times shown in <b>{{$location->timezone}}</b></span>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
@endif

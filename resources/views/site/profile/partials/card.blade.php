<div class="content_block {{$editMode ? 'm-edit' : ''}}">
	<div class="profile--block profile_card" data-profile-card>

		<div class="profile_card--photo">
			<div class="profile_card--photo--userpic" {!! $editMode ? 'data-edit="userpic"' : '' !!}>
				<figure class="userpic m-xxlarge m-rounded">
					<span class="userpic--inner">
						@if($profile->avatar)
							<img src="{{ $profile->avatar }}" data-original="{{ $profile->avatar_original }}" alt="">
						@endif
					</span>
				</figure>
				@if ($profile->verified)
					<span class="profile_card--verified">verified</span>
				@endif
			</div>
			<ul class="profile_card--photo--actions">
				@if ($currentProfile)
					<li class="profile_card--photo--actions--item">
						<div class="profile_card--invitations" data-profile-invites-container>
							<div class="profile_card--invitations--line" data-profile-invites></div>
							<div class="profile_card--invitations--loading loading_overlay m-light m-hide"><span class="spin m-logo"></span></div>
						</div>
					</li>
				@endif
				<li class="profile_card--photo--actions--item">
					<a href="{{$isOwnProfile ? '#' : route('dashboard-conversations') . '#to:' . $profile->id}}" data-message class="btn m-blue m-action_msg">Message on HealthPRO</a>
				</li>
			</ul>
		</div>

		<div class="profile_card--main">

			<div class="profile_card--basic_info {{$editMode ? 'm-edit' : ''}}">
				<header class="profile_card--header" {!! $editMode ? 'data-edit="basic_info"' : '' !!}>
					<div class="profile_card--header_name">
						@if ($profile->business_name_display)
							<div>{{$profile->business_name}}</div>
						@else
							<div>{{$profile->first_name . ' ' . $profile->last_name}}</div>
						@endif
						@if ($editMode)
							<a href="{{route('profile-me-show')}}" class="btn m-green m-s">View Profile As Client</a>
						@endif
					</div>

					@if(count($services))
						<ul class="profile_card--header--categories">
							<?php
								$serviceNames = array();
								foreach($services as $service) {
									$serviceNames[] = $service->serviceTypes->first() ? $service->serviceTypes->first()->name : $service->name;
									if ($service->serviceTypes->count() > 1) {
									    foreach ($service->serviceTypes->slice(1) as $subServiceType) {
											$serviceNames[] = '&nbsp;&nbsp;&nbsp;' . $subServiceType->name;
										}
									}
								}
							?>
							@foreach(array_unique($serviceNames) as $serviceName)
								<li>{{ $serviceName }}</li>
							@endforeach
						</ul>
					@endif

				</header>

				<div class="profile_card--info">
					{{--@if ($overallRating && !$editMode)--}}
						{{--<dl class="profile_card--info--line m-rating">--}}
							{{--<dt>Rating:</dt>--}}
							{{--<dd class="profile_card--ratings">--}}
							{{--<span class="star_rating m-{{ $overallRating->present()->valueToWord() }} m-s">--}}
								{{--<span class="star_rating--star"></span>--}}
								{{--<span class="star_rating--star"></span>--}}
								{{--<span class="star_rating--star"></span>--}}
								{{--<span class="star_rating--star"></span>--}}
								{{--<span class="star_rating--star"></span>--}}
							{{--</span>--}}
							{{--<span class="profile_card--info--reviews">--}}
								{{--@if($commentsCount)--}}
									{{--({{$commentsCount}} review{{$commentsCount == 1 ? '' : 's'}})--}}
								{{--@else--}}
									{{--(No reviews yet)--}}
								{{--@endif--}}
							{{--</span>--}}
							{{--</dd>--}}
						{{--</dl>--}}
					{{--@endif--}}

					@if (($profile->business_name) && (!$profile->business_name_display))
						<dl class="profile_card--info--line">
							<dt>Business Name:</dt>
							<dd>{{$profile->business_name}}</dd>
						</dl>
					@endif

					{!! TaxonomyHtml::showTags('Languages:','profile_card--info--line',$profile->tags,'languages') !!}

					@if (isset($profile->tags['experience']) && count($profile->tags['experience']['tags']))
						<dl class="profile_card--info--line">
							<dt>Experience:</dt>
							<dd>{{$profile->tags['experience']['tags'][0]->name}}</dd>
						</dl>
					@endif
				</div>
			</div>

			<div class="profile_card--contacts {{$editMode ? 'm-edit' : ''}}">
				<dl class="profile_card--info--line m-contacts" data-contact  {!! $editMode ? 'data-edit="contacts"' : '' !!}>
					@if(count(array_filter($profile->social_pages)))
					<dt>Contact {{$profile->first_name}}:</dt>
					@endif
					<dd>
						@if(count(array_filter($profile->social_pages)))
							<ul class="profile_card--info--contacts">
								@foreach ($profile->social_pages as $link => $value)
									@if(!empty($value))
										<li>
											<a href="{{$value}}" target="_blank">
												<button class="btn m-social m-{{$link}}" type="button">{{$link}}</button>
											</a>
										</li>
									@endif
								@endforeach
							</ul>
						@endif
						@if ($hasGiftCertificates && !$isOwnProfile)
							<div class="profile_card--info--message m-certificate">
								<button class="profile_card--info--message_link m-link" title="Buy Gift Certificate for appointments with {{$profile->first_name . ' ' . $profile->last_name}}" data-profile-purchase-gift-certificate><i></i>Buy a Gift Certificate</button>
							</div>
						@endif
					</dd>
				</dl>
			</div>
			@if ($isOwnProfile and !$editMode)
				<div class="profile_card--action_btns m-edit">
					<div class="share_button btn m-green m-s m-share">
						Share profile
						<ul class="share_button--list">
							<li><a
								href="https://www.facebook.com/sharer/sharer.php?u={{$profile->public_url}}?share=success&media=fb"
								class="share_button--link m-facebook"
								title="Share on Facebook"
								target="_blank"></a></li>
							<li><a
								href="https://twitter.com/home?status=Check%20out%20my%20profile%20on%20HealthPRO%20{{$profile->public_url}}"
								class="share_button--link m-twitter"
								title="Share on Twitter"
								target="_blank"></a></li>
							<li><a
								href="https://plus.google.com/share?url={{$profile->public_url}}"
								class="share_button--link m-googleplus"
								title="Share on Google Plus"
								target="_blank"></a></li>
							<li><a
								href="https://www.linkedin.com/shareArticle?mini=true&url={{$profile->public_url}}&title=Check%20out%20my%20profile%20on%20HealthPRO&summary=The%20platform%20for%20health%20%26%20fitness%20professionals%20and%20their%20clients&source="
								class="share_button--link m-linkedin"
								title="Share on LinkedIn"
								target="_blank"></a></li>
							<li><a
								href="http://www.stumbleupon.com/submit?url={{$profile->public_url}}"
								class="share_button--link m-stumbleupon"
								title="StumbleUpon"
								target="_blank"></a></li>
						</ul>
					</div>
					<div class="profile_card--action_btns--edit">
						<a href="{{route('profile-me-show', ['editMode' => 'edit'])}}" class="btn m-edit m-mid_gray_border m-s">Edit Profile</a>
					</div>
				</div>
			@endif

		</div>
	</div>
	@if ($profile->tags['gender_identities']['tags']->first() || $profile->tags['age_group']['tags']->first() || $profile->tags['special_needs']['tags']->first() || $editMode)
		<div class="suitability {{$editMode ? 'm-edit' : ''}}" data-profile-suitabilit {!! $editMode ? 'data-edit="suitability"' : '' !!}y>
			<header class="profile--block--header m-suitability">
				<div class="profile--block--header--title">Suitability</div>
			</header>
			<div class="profile--block--content m-pull">
				<div class="suitability--list">
					{!! TaxonomyHtml::showTags('Genders Served:', 'suitability--list--item', $profile->tags, 'gender_identities') !!}
					{!! TaxonomyHtml::showTags('Age Group:', 'suitability--list--item', $profile->tags, 'age_group') !!}
					{!! TaxonomyHtml::showTags('Special Needs:', 'suitability--list--item', $profile->tags, 'special_needs') !!}
				</div>
			</div>
		</div>
	@endif
</div>

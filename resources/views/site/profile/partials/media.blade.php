@if (isset($profile->assets['photo']) || isset($profile->assets['video']) || $editMode)
	<div class="profile--section" {!! $editMode ? 'data-edit="media"' : '' !!}>
		<div class="profile--main--title">Media</div>
		<div class="content_block {{$editMode ? 'm-edit' : ''}}">
			<div class="about" data-about>
				<div class="about--gallery m-loading profile--block">
					<div class="carousel owl-carousel" data-about-gallery>
						@if(isset($profile->assets['video']))
							@foreach ($profile->assets['video'] as $item)
								<a href="{{str_replace('youtu.be/', 'youtube.com/watch?v=', $item->url)}}" class="mfp-iframe carousel--thumb m-video" data-about-gallery-thumb data-about-video-thumb>
									<span><img src=""></span>
								</a>
							@endforeach
						@endif
						@if(isset($profile->assets['photo']))
							@foreach ($profile->assets['photo'] as $item)
								@if(isset($item->attachment['215x140']) && isset($item->attachment['original']))
									<a href="{{$item->attachment['original']}}" title="@if ($item->title){{$item->title}}@endif" class="mfp-image carousel--thumb" data-about-gallery-thumb>
										<img src="{{$item->attachment['215x140']}}" alt="">
									</a>
								@endif
							@endforeach
						@endif
					</div>
					<div class="about--gallery_loading loading_overlay m-light"><span class="spin m-logo"></span></div>
				</div>
			</div>
		</div>
	</div>
@endif

@if ( count($profile->jobs) ||  count($profile->educations) || isset($profile->assets['certificate']) || count($profile->professional_body) || $editMode )
	<div class="profile--section" {!! $editMode ? 'data-edit="qualifications"' : '' !!}>
		<div class="profile--main--title">Qualifications</div>
		<div class="content_block {{$editMode ? 'm-edit' : ''}}">
			@if ( count($profile->educations) )
				<div class="qualif profile--block">
					<div class="profile--block--header m-education">
						<div class="profile--block--header--title">Education</div>
					</div>
					<div class="profile--block--content m-pull">
						<ul class="qualif--list">
							@foreach ($profile->educations as $edu)
								<li class="qualif--list--item">
									@if ($edu->from)
										<span>{{$edu->from->format('M Y')}} - <?php if($edu->to) echo $edu->to->format('M Y'); else echo 'present' ?></span>
									@elseif ($edu->to)
										<span>graduated in  {{$edu->to->format('Y')}}</span>
									@endif
									<p>
										<strong>{{$edu->education_name}}</strong>
										{{$edu->degree}}
									</p>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			@endif

			@if(isset($profile->assets['certificate']))
				<div class="certificates profile--block" data-certificates>
					<div class="profile--block--header m-certificates">
						<div class="profile--block--header--title">Certifications</div>
					</div>

					<div class="profile--block--content">
						<div class="certificates--gallery carousel owl-carousel" data-certificates-gallery>
							@foreach ($profile->assets['certificate'] as $certificate)
								<div class="carousel--thumb_wrap">

									@if(isset($certificate->attachment['215x140']) && isset($certificate->attachment['original']))
										<a href="{{$certificate->attachment['original']}}" class="carousel--thumb" data-certificates-gallery-thumb>
											<img src="{{$certificate->attachment['215x140']}}" alt="">
										</a>
									@endif
									@if(isset($certificate->title))
										<p class="certificates--item_text">{{$certificate->title}}</p>
									@endif
								</div>
							@endforeach
						</div>
					</div>
				</div>
			@endif

			@if ( count($profile->jobs) )
				<div class="qualif profile--block">
					<div class="profile--block--header m-work">
						<div class="profile--block--header--title">Work Experience</div>
					</div>
					<div class="profile--block--content m-pull">
						<ul class="qualif--list">
							@foreach ($profile->jobs as $job)
								<li class="qualif--list--item">
									<p>
										<strong>{{$job->company_name}}</strong>
										{{$job->job_title}}
									</p>
									<span>{{ date_format(new DateTime($job->from), 'M Y') }} - <?php if($job->to_year) echo date_format(new DateTime($job->to), 'M Y'); else echo 'present' ?></span>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			@endif

			@if ( count($profile->professional_body) )
				<div class="certificates profile--block" data-profbody>
					<div class="profile--block--header m-associations">
						<div class="profile--block--header--title">Professional Body Memberships</div>
					</div>
					<div class="profile--block--content m-pull">
						<ul class="qualif--list">
							@foreach ($profile->professional_body as $body)
								<li class="qualif--list--item">
									@if (!is_null($body->expiry_date))
										<span>{{$body->expiry_date->format('m/d/Y')}}</span>
									@endif
									<p>
										<strong>{{$body->title}}</strong>
									</p>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			@endif
		</div>
	</div>
@endif

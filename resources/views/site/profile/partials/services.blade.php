@if (count($services) || $editMode)
	<div class="profile--section" {!! $editMode ? 'data-edit="services"' : '' !!}>
		<div class="profile--main--title">Services</div>
		<div class="content_block m-transparent">
			<div class="book_services">
				<div class="book_services--main profile--block">
					<div class="book_services--list m-loading" data-book_services>
						@foreach($services as $service)
							{!! ProviderServiceHtml::showService($service, $editMode) !!}
						@endforeach
					</div>
					<div class="loading_overlay m-light"><span class="spin m-logo"></span></div>
				</div>
			</div>
		</div>
	</div>
@endif

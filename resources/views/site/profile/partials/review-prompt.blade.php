@if ($appointment and !$ownUser)
<div class="prompt">
	<div class="prompt--inner">
		<p>You still haven't reviewed <strong>{{$profile->first_name . ' ' . $profile->last_name}}</strong> for the appointment on {{ $appointment->date->toFormattedDateString() }}. <a href="{{ route('dashboard-my-providers' ) }}#review:{{$profile->slug or $profile->id}}" class="btn m-blue m-small prompt--btn" data-review-professional>Review Now</a></p>
	</div>
</div>
@endif

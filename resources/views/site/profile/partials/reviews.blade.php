<div class="reviews">
	<div class="profile--section wrap">
		<div class="reviews--header">
			<button
				class="btn m-red reviews--header--btn @unless ($shouldReview and $_PTYPE == 'client' and !$ownUser) m-hide @endunless"
				data-review-professional>
				Write a review
			</button>
			<div class="reviews--title profile--main--title">
				<span>
					@if($commentsCount)
						({{$commentsCount}} reviews)
					@else
						(No reviews yet)
					@endif
				</span>
			</div>
		</div>
		<div class="reviews--block">
			@if ($reviews)
				<div class="reviews--rating">
					@foreach ($overallRatings as $rating)
						<dl class="reviews--rating--item m-column_1">
							<dt>{{__($rating->name)}}</dt>
							<dd>
									<span class="star_rating m-{{ $rating->present()->valueToWord() }} m-s">
										<span class="star_rating--star"></span>
										<span class="star_rating--star"></span>
										<span class="star_rating--star"></span>
										<span class="star_rating--star"></span>
										<span class="star_rating--star"></span>
									</span>
								<span>({{round($rating->rating_value, 2)}})</span>
							</dd>
						</dl>
					@endforeach
				</div>
			@else
				<div class="reviews--rating m-empty">
					No reviews yet
				</div>
			@endif
			<ul class="reviews--list">
				@foreach ($reviews as $review)
					@if (count($review->comments))
						<li class="reviews--list--item">
							<header class="reviews--list--item--header">
								<figure class="userpic m-circle m-s">
										<span class="userpic--inner">
												@if($review->profileAvatar)
												<img src="{{ $review->profileAvatar }}" alt="">
											@endif
										</span>
								</figure>

								<p class="reviews--list--item--header--meta">
									<a name="review_{{$review->id}}" href="{{route('client-profile-show', $review->profile_id)}}"><strong>{{$review->profileName}}</strong></a> {{ ($review->daysAgo==0) ? __('today') : _n("yesterday", "{$review->daysAgo} days ago", $review->daysAgo) }}
								</p>

								@if ($rating = $review->overallRating)
									<div class="reviews--list--item--header--rating">
										<span>Overall Score</span>
											<span class="star_rating m-{{ $rating->present()->valueToWord() }} m-s">
												<span class="star_rating--star"></span>
												<span class="star_rating--star"></span>
												<span class="star_rating--star"></span>
												<span class="star_rating--star"></span>
												<span class="star_rating--star"></span>
											</span>
										<span>({{round($rating->rating_value, 2)}})</span>
									</div>
								@endif
							</header>

							@foreach ($review->comments as $comment)
								<div class="reviews--list--item--content">
									<p class="paragraph">{{$comment->content}}</p>
								</div>
							@endforeach
							@if (!$isOwnUser)
								<div class="reviews--list--item--helpful">
									<div class="reviews--list--item--helpful--inner">
									</div>
									<div class="reviews--list--item--helpful--loading loading_overlay m-light m-hide"><span class="spin m-logo"></span></div>
								</div>
							@endif
						</li>
					@endif
				@endforeach
			</ul>
		</div>
	</div>
</div>

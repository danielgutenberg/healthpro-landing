<div class="profile--section">
	<div class="profile--main--title">Payment Information</div>
	<div class="content_block m-edit">
		<div class="payment_info">
			<div class="payment_info--message">
				<p>Please proceed to the dashboard to edit your debit cards.</p>
			</div>
			<div class="payment_info--cta">
				<a href="{{ route('dashboard-settings-payment') }}" target="_blank" class="btn m-blue m-small">Edit Debit Cards</a>
			</div>
		</div>
	</div>
</div>

@extends('site.layouts.professional')

@section('content')

<div class="not-available-professional">
	<div class="not-available-professional--inner">
		<h1 class="not-available-professional--title">We are sorry but that professional is not available on HealthPRO at the moment.</h1>
		<img src="{{ asset('assets/frontend/images/content/not-available-professional/door.png') }}" alt=""/>
		<h6  class="not-available-professional--title-contact-professional">Contact Professional</h6>
		<a href="{{route('dashboard-conversations') . '#to:' . $profileId}}" data-message class="btn m-blue m-action_msg not-available-professional--button-send-message"><i class="not-available-professional--button-send-message-icon"></i><span class="not-available-professional--button-send-message-text">Send a Messsage</span></a>
		<a class="not-available-professional--title-pro" href="/">ARE YOU A PRO?</a>
		<div class="not-available-professional--text-pro">If you think you got this page in error please <a href="/contact-us">contact support</a>.</div>
	</div>
</div>

@stop

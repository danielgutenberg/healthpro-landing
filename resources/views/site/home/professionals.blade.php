@extends('site.layouts.professional')
@section('title')HealthPRO Online Appointment Scheduling & Management Software For Health & Wellness Professionals @stop
@section('meta-description')HealthPRO’s risk free online scheduling software simplifies business management, bookings and appointments, payment processing, and more. Our All-in-One service appointment scheduling software is an easy to use platform for managing health, wellness and fitness businesses.@stop
@section('meta-keywords'){{'online,software,platform,time,money,independent,professional,wellness,fitness,business,risk,free,calendar,scheduling,payments,booking,calendar,scheduling,payments,transactions,all,in,one,solution,system,HealthPRO.com,HealthPRO,pro,health'}}@stop

@section('content')
	<div class="home-professional">
		@include('site.page.partials.pros-hero')
		@include('site.page.partials.pros-steps')
		@include('site.page.partials.sign-up')
	</div>
	@if (Sentinel::check() && !isset($popup))
		{!! Wizard::render('profile_setup') !!}
	@endif
@stop

@section('js-globals')
	AUTHPOPUP: '{{$popup}}',
@stop

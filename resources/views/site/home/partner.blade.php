@extends('site.layouts.professional')
@section('title')
	{{$title}}
@stop
@section('meta-description'){{$description}}@stop
@section('meta-keywords'){{$keywords}} @stop
@section('content')
	<div class="home-professional  partner-professional">

		@include("site.page.partials.pros-hero")

		@include("site.page.partials.sign-up-partner")

	</div>
	@if (Sentinel::check())
		{!! Wizard::render('profile_setup') !!}
	@endif
@stop


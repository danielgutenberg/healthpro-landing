@extends('site.layouts.default')

@section('content')
	<div class="home-client">
		<div class="home-search">
			<div class="home-search--inner wrap">
				<div class="home-search--title"><span>Better Health.</span> <span>Better Choices.</span></div>
				<div class="home-search--description"><b>Find</b> and <b>book</b> services with the best wellness professionals in your area</div>

				<form action="#" class="home-search--form form js-search_filters">
					<div class="home-search--form--inner">
						<!-- Search by service -->
						<div class="home-search--form--fieldset m-service">
							<div class="home-search--form--row">
								<div class="form--group">
									<div class="field m-borderless m-wide m-location">
										<input type="text" placeholder="e.g. Phoenix, Amsterdam, Neighbourhood" name="location" id="location" data-selector="q[location]">
										<span class="field--error"></span>
										<span class="field--autocomplete"></span>
									</div>
								</div>
								<div class="form--group m-type">
									<div class="field m-borderless m-wide m-search">
										<input type="text" placeholder="e.g. Massage, Chiropractor, Reflexologist" name="service" id="service" data-selector="q[service]">
										<span class="field--error"></span>
										<span class="field--autocomplete"></span>
									</div>
								</div>
								<div class="form--group m-date">
									<div class="field m-borderless m-wide m-date">
										<input type="text" readonly="true" name="date" id="date" data-selector="date">
										<span class="field--error"></span>
									</div>
								</div>
								<div class="form--group m-thin">
									<button class="btn m-green m-wide m-arrow home-search--form--btn" type="submit">FIND &amp; BOOK</button>
								</div>
							</div>
						</div>

						<!-- Search by name -->
						<div class="home-search--form--fieldset m-name">
							<div class="form--group m-wide">
								<span class="field m-borderless m-wide">
									<input type="text" placeholder="e.g. Steve Martin, Angela Smith" name="name" id="name" data-selector="q[name]">
									<span class="field--error"></span>
									<span class="field--autocomplete"></span>
								</span>
							</div>
						</div>
					</div>
					<label class="home-search--form--switch btn m-white m-shade_bg" for="search_by">
						<input type="checkbox" name="search_by" id="search_by" value="name">
						<span class="m-name">Search By Name</span>
						<span class="m-service">Search By Service</span>
					</label>
				</form>
			</div>
		</div>

		<div class="home-features">
			<div class="home-features--inner wrap">
				<ul class="home-features--list">
					<li class="home-features--item">
						<div class="home-features--item_title m-bookings">Instant Bookings</div>
						<div class="home-features--item_text">Realtime availability, 24/7 backed by simple scheduling and helpful reminders.</div>
					</li>
					<li class="home-features--item">
						<div class="home-features--item_title m-services">Multiple Services</div>
						<div class="home-features--item_text">From massage to yoga, book any and all your health and wellness services here.</div>
					</li>
					<li class="home-features--item">
						<div class="home-features--item_title m-reviews">Real Reviews</div>
						<div class="home-features--item_text">Get comprehensive details about professionals and their services!</div>
					</li>
					<li class="home-features--item">
						<div class="home-features--item_title m-offers">Special Offers</div>
						<div class="home-features--item_text">Access discounts and coupons unique to services offered via HealthPRO.</div>
					</li>
				</ul>

			</div>
		</div>

		<div class="home-pros">
			<div class="home-pros--inner wrap">
				@if(count($prosToDisplay))
					<div class="newest_pros">
						<div class="newest_pros--title">Are you a Health Professional?</div>
						<div class="newest_pros--description">Use <b>HealthPRO</b> to Manage and Improve your Business</div>
						<div class="newest_pros--list">
							@if(Sentinel::check())
								@if($_PTYPE == "provider" && !$hasAvatar)
									<div class="newest_pros--item newest_pros--item_cta">
										<a href="{{route('dashboard-myprofile-personalinfo')}}" class="newest_pros--item_link newest_pros--item_cta_link">
											<span class="newest_pros--item_photo"></span>
											<span class="newest_pros--item_cta_text">This can be your profile</span>
											<span class="btn m-red m-small m-upload_photo">Upload your photo</span>
										</a>
									</div>
								@endif
							@else
								<div class="newest_pros--item newest_pros--item_cta">
									<a href="{{$registerAsProfessionalRoute}}" data-toggle="popup" data-target="popup_auth" data-authpopup-toggle="sign:provider"  class="newest_pros--item_link newest_pros--item_cta_link">
										<span class="newest_pros--item_photo"></span>
										<span class="newest_pros--item_cta_text">This can be your profile</span>
										<span class="newest_pros--item_cta_btn btn m-red m-small">Sign up</span>
									</a>
								</div>
							@endif
							@php $i = 0; @endphp
							@foreach ($prosToDisplay as $pro)
								@if (count($pro->services) > 0 and $i < 5)
									<div class="newest_pros--item">
										<a class="newest_pros--item_link" href="{{$pro->profile['public_url']}}">
											<span class="newest_pros--item_photo"><img src="{{$pro->profile['avatar']}}" alt="{{$pro->profile['full_name']}}"></span>
											<span class="newest_pros--item_name">{{$pro->profile['full_name']}}</span>
											<span class="newest_pros--item_service">
												@if ($pro->services[0]['name'] == 'Introductory-Session')
													{{$pro->services[1]['name']}}
												@else
													{{ $pro->services[0]['name'] }}
												@endif
											</span>
										</a>
									</div>
									@php $i++; @endphp
								@endif
							@endforeach
						</div>
						<!-- <div class="newest_pros--text">
							<p>Want to book a professional you know through HealthPRO? <span><a href="#">Tell your professional about HealthPRO</a></span></p>
						</div> -->
					</div>
				@endif

				<div class="home-cta">
					<div class="home-cta--title">Simplify and Expand your Business</div>
					<a href="{{route('pricing')}}" class="home-cta--btn cta_btn m-red m-arrow m-large">Click here to get started</a>
					<div class="home-cta--text">No Monthly Fees or Upfront Payments Required. <b>FREE</b> to Join</div>
				</div>
			</div>
		</div>


		{{--<div class="home-get-app js-get_app">--}}
			{{--<a class="home-get-app--close js-get_app_close" href="#"></a>--}}
			{{--<a class="home-get-app--link" href="itms-apps://itunes.apple.com/app/id1049632887">--}}
				{{--<span class="home-get-app--cta">--}}
					{{--<i class="home-get-app--cta--icon"></i>--}}
					{{--<span class="home-get-app--cta--title">Get our mobile app!</span>--}}
					{{--<span class="home-get-app--cta--text">Start booking today</span>--}}
					{{--<span class="home-get-app--cta--btn btn m-green">Get Now</span>--}}
				{{--</span>--}}
			{{--</a>--}}
		{{--</div>--}}
	</div>
@stop

@section('popups')
	@include('site.home.clients.popup-video')
@append

@section('js-globals')
	@if (! empty($location)) DEFAULT_SEARCH_LOCATION: {{$location}}, @endif
	@if (! empty($city)) DEFAULT_SEARCH_CITY: '{{$city}}', @endif
	AUTHPOPUP: '{{$popup}}',
@stop

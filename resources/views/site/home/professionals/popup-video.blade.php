<div class="popup popup--video" data-popup="popup_home_video" data-options='{
	"type": "video",
	"videoUrl": "https://www.youtube.com/embed/Tq3A_4Iw2Wg?rel=0",
	"videoSizes": {"width": 840,"height": 472}
	}'>
	<div class="popup--container m-video_container" data-popup-container>
		<button class="popup--close m-video_close" data-popup-close>close</button>
	</div>
	<div class="popup--bg" data-popup-close></div>
</div>

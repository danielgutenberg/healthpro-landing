<div class="popup home-tour" data-popup="popup_home_tour">
	<div class="popup--container" data-popup-container>
		<div class="popup--header">
			<div class="popup--header_title">Choose Your Discipline to Take a Tour</div>
			<button class="popup--close" data-popup-close>close</button>
		</div>
		<div class="popup--content">
			<ul class="home-tour--list">
				<?php
					$homeServices = MetaParser::parseAssoc('content/services', 'services');
					$homeServices['more'] = [
						'slug' => 'more',
						'title' => 'And&nbsp;More&hellip;',
					];
				?>
				@foreach($homeServices as $slug => $service)
					<li class="home-tour--item m-{{$service['slug']}}">
						<a href="{{ route('how-it-works', ['service' => $service['slug'] == 'more' ? '' : $service['slug']]) }}" class="home-tour--link m-{{$service['slug']}}">
							<i class="home-tour--icon m-{{$slug}}">
								<img src="/assets/frontend/images/design/home/professionals/2016/svg/tour/service-{{$service['slug']}}.svg" alt="">
							</i>
							<span class="home-tour--label"><span>{!! str_replace(' ', '<br>', $service['title']) !!}</span></span>
						</a>
					</li>
				@endforeach
			</ul>
		</div>
	</div>
	<div class="popup--bg"></div>
</div>

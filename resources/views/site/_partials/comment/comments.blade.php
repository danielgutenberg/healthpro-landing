<div class="comments">
	<div class="wrap">

		<div class="comments--inner">
			<div class="comments--title">Comments
				<small>{{count($comments)}}</small>
			</div>

			@if($isGuest)
				<div class="comments--guest">
					<a href="{{route('auth-login')}}"
						data-toggle="popup"
						data-target="popup_auth"
						data-auth-redirect="{{Request::url()}}"
						data-authpopup-toggle="login">Login</a>
					or
					<a href="{{route('auth-register')}}"
						data-toggle="popup" data-target="popup_auth"
						data-auth-redirect="{{Request::url()}}"
						data-authpopup-toggle="sign_up">register</a>
					to leave a comment
				</div>
			@endif

			@if($canAddComment)
				<div class="comments--form">
					<div class="comment">
						<div class="comment--userpic">
							<figure class="userpic m-48 m-circle">
								<span class="userpic--inner">
									@if($currentProfile->avatar)
										<img src="{{$currentProfile->avatar}}"/>
									@endif
								</span>
							</figure>
						</div>
						<div class="comment--form">
							{!! Former::open()->action( route('comment-add') )->method('POST') !!}
							<div class="field m-wide"><textarea name="content"
																placeholder="Enter your comment here.."></textarea>
							</div>
							<footer class="comment--form--footer">
								<button class="btn m-red" type="submit">Comment</button>
							</footer>
							<input type="hidden" name="entity_id" value="{!! $entityId !!}">
							<input type="hidden" name="entity_type" value="{!! $entityType !!}">
							{!! Former::close() !!}
						</div>
					</div>
				</div>
			@endif
			<div class="comments--list">
				@foreach($comments as $comment)
					<div class="comment">
						<div class="comment--userpic">
							<figure class="userpic m-48 m-circle">
								<span class="userpic--inner">
									@if($comment->profile->avatar)
										<img src="{{$comment->profile->avatar}}"/>
									@endif
								</span>
							</figure>
						</div>
						<div class="comment--container">
							<div class="comment--meta">
								<strong>{{$comment->profile->full_name}}</strong>
								<time>{{$comment->created_at->diffForHumans()}}</time>
							</div>
							<div class="comment--body">
								<p>{{$comment->content}}</p>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</div>

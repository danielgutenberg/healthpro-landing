{{-- @unless(Sentinel::check()) --}}

	<div class="popup m-membership" data-popup="popup_membership" id="popup_membership">
		<div class="popup--container" data-popup-container>
			<div class="popup--content popup_membership" data-membership-popup>
				<div class="membership m-popup">
					{!!
						Former::open()
							->method('POST')
							->action(route('auth-register'))
							->class("membership--form m-membership")
					!!}

					{!! Former::close() !!}
				</div>
				<div class="loading_overlay m-light m-hide m-glow" data-membership-loading><span class="spin m-logo"></span></div>
			</div>
		</div>
		<div class="popup--bg" data-popup-close></div>
	</div>

	<div class="popup popup_response" data-popup="popup_payment_success" id="popup_payment_success" data-options='{"closeOnEsc": false}'>
		<div class="popup--container" data-popup-container>
			<div class="popup_response--wrap m-success m-large">
				<div class="popup_response--title">Payment Successful</div>
				<div class="popup_response--text">
					<p>Thanks for joining HealthPRO! Your payment has successfully been processed.</p>
					<p>Now you will proceed to the wizard to complete the sign up process.</p>
				</div>
				<footer class="popup_response--actions">
					<a class="btn m-green" href="{{route('home')}}">NEXT</a>
				</footer>
			</div>
		</div>
		<div class="popup--bg"></div>
	</div>

	<div class="popup popup_response" data-popup="popup_payment_failed" id="popup_payment_failed" data-options='{"closeOnEsc": false}'>
		<div class="popup--container" data-popup-container>
			<div class="popup_response--wrap m-warning m-large">
				<div class="popup_response--title">Something Went Wrong</div>
				<div class="popup_response--text">
					<p>Sorry, we were unable to process your payment.</p>
					<p>Your payment method has not been changed.</p>
				</div>
				<div class="popup_response--details"></div>
				<footer class="popup_response--actions">
					<a class="btn m-green popup_response--actions_retry" href="{{route('pricing')}}">Try Again</a>
				</footer>
			</div>
		</div>
		<div class="popup--bg"></div>
	</div>

{{-- @endif --}}

<fieldset>
	<legend><?=__('Phones')?> &nbsp;  <span class="js-new-phone" onclick="addPhone()" style="cursor: pointer">[+]</span>&nbsp; </legend>
	<div class="js-phones"></div>
</fieldset>

<script type="text/javascript">
	var phoneIndex = 0;

	function removePhone(object) {
		if( object.closest('p').find('input.js-code').val().length ) {
			if( confirm('Are you sure?') ) {
				object.closest('p').remove();
			}
		} else {
			object.closest('p').remove();
		}

		return false;
	}

	function addPhone(code, phone) {
	 	phoneIndex++;

		var template = '<p style="clear:both">' +
		 '<input type="text" class="js-code" placeholder="+XXX" name="phones['+phoneIndex+'][code]" class="phone" style="width:70px;display:inline;">' +
		 '<input type="text" class="js-phone" placeholder="XXXXXXXX" name="phones['+phoneIndex+'][locNumber]" class="phone" style="width:200px;display:inline;">' +
		  '&nbsp;' +
		   '<span class="delete" style="cursor: pointer" onclick="removePhone($(this))">X</span>' +
		'</p>';

		var divPhones = $("div.js-phones");

		divPhones.append(template);

		divPhones.find('p:last input.js-code').val(code);
		divPhones.find('p:last input.js-phone').val(phone);
	}

	@if(isset($phones) && count($phones))
		@foreach($phones as $phone)
			addPhone('<?=$phone->code?>', '<?=$phone->locNumber?>');
		@endforeach
	@endif
</script>


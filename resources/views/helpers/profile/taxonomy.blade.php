@if($selectDefault = 0) @endif

@if( isset($attachTags[$taxonomy->id]) )
	@if($selectDefault = key($attachTags[$taxonomy->id])) @endif
@endif

@if($taxonomy->act_like == 'selectOne')
	{!! Former::select("tags[select]")->options([0 => "--Select--"] + $tags)->select($selectDefault)->label('') !!}
@else
	@if($nameTag = "tags[".$tagKey."]") @endif
	<input type="checkbox" id="{{$tagKey}}" name="{{$nameTag}}" @if( isset($profile->id) && isset($attachTags[$taxonomy->id][$tagKey]) ) {{'checked'}} @endif value="1" >
	<span>{{$tagValue}}</span>
@endif

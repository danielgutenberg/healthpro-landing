@if (isset($label))
	<label for="{!! $inputName !!}">{{$label}}</label>
@endif
{{-- todo cleanup --}}
<div class="select m-wide">
	<select id="taxonomy-{{$taxonomy->slug}}-input" name="{!! $inputName !!}[]" class="{{isset($class) ? $class : ''}}" {{$taxonomy->act_like == 'selectMany'?'multiple=""':''}} {{ $taxonomy->allow_custom_tags ? 'data-tags=true multiple' : ''}} data-select>

		@if (empty($require))
			<option value=""></option>
		@endif

		@foreach($allTags as $tag)
			<option value="{{ !$taxonomy->allow_custom_tags ? $tag->id : $tag->name }}" @if(isset($hSelectedTags[$tag->id])) selected @endif> {{ $tag->name }}</option>
		@endforeach

	</select>
</div>

@if (isset($label))
	<label for="{!! $inputName !!}">{{$label}}</label>
@endif

@if($taxonomy->act_like=='selectMany')
	<ul id="taxonomy-{{$taxonomy->slug}}-input" {!!isset($class) ? "class='{$class}'" : ''!!} >
		@foreach($allTags as $tag)
		<li>
			<label class="checkbox">
				<input type="checkbox" name="{{$inputName}}[]" value="{{$tag->id}}" @if(isset($hSelectedTags[$tag->id]))checked @endif >
				<span class="checkbox--i"></span>
				<span class="checkbox--label">{{$tag->name}}</span>
			</label>
		</li>
		@endforeach
	</ul>
	@if ($taxonomy->allow_custom_tags)
        <div class="edit_qualif--other">

            <label class="field">
               {{-- <input type="text" name="{{$inputName}}[__custom__]" value="{{implode(', ',array_map(function($tag){return $tag->name;},$selectedCustomTags))}}"> --}}
				<input type="text" name="{{$inputName}}[__custom__]" value="">
                <span class="edit_qualif--other--tip">* use coma (,) as separator</span>
                 <span class="text--label">Custom</span>
            </label>
        </div>
	@endif
		<input type="hidden" name="{{$inputName}}[]" value="" >
@else
	<ul id="taxonomy-{{$taxonomy->slug}}-input" {!!isset($class) ? "class='{$class}'" : ''!!} >
		@foreach($allTags as $tag)
		<li>
			<label class="radio">
				<input type="radio" name="{{$inputName}}" value="{{$tag->id}}" @if(isset($hSelectedTags[$tag->id])) checked @endif >
				<span class="radio--i"></span>
				<span class="radio--label">{{$tag->name}}</span>
			</label>
		</li>
		@endforeach
	@if ($taxonomy->allow_custom_tags)
        <div class="edit_qualif--other">
            <label class="radio">
                <input type="radio" name="{{$inputName}}">
                <span class="radio--i"></span>
                <span class="radio--label">Other</span>
            </label>
            <label class="field">
                <input type="text" name="{{$inputName}}[__custom__]" value="">
            </label>
        </div>
	@endif
	</ul>
@endif

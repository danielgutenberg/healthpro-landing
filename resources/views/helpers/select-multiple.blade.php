<div>
	<select multiple="multiple" id="<?=$name?>-available" style="width: 100; height: 200px">
		@foreach($source as $item)
			@if( !in_array($item->id, array_values($intersectItems)) )
				<option value="<?=$item->id?>"><?=$item->name?></option>
			@endif
		@endforeach
	</select>

	<a href="#" id="add-<?=$name?>"><?=__('add')?> &gt;&gt;</a>
</div>

<div>
	<select name="<?=$name?>[]" multiple="multiple" id="<?=$name?>-selected" style="width: 100; height: 200px">
	@if($target)
		@foreach($target as $item)
			<option value="<?=$item->id?>" selected><?=$item->name?></option>
		@endforeach
	@endif
	</select>
	<a href="#" id="remove-<?=$name?>">&lt;&lt; <?=__('remove')?></a>
</div>

<script type="text/javascript">
	$(function() {
		$('#add-<?=$name?>').on("click", function() {
			$('#<?=$name?>-available option:selected').remove().appendTo('#<?=$name?>-selected');
		});
		$('#remove-<?=$name?>').on("click", function() {
			$('#<?=$name?>-selected option:selected').remove().appendTo('#<?=$name?>-available');
		});

		$("form").on("submit", function() {
			$('#<?=$name?>-selected option').each(function() {
				$(this).prop("selected", true);
			});
		});
	});
</script>
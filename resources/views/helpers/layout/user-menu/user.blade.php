@if ($type == 'slideout' || $type == 'dropdown')
	<a href="{{route('dashboard-myprofile')}}" class="user_menu--user m-current m-{{$type}}"  {!! $type == 'slideout' ? ' data-usermenu-profiles="opener"' : '' !!} >
		<figure class="user_menu--userpic userpic m-small m-circle">
			<span class="userpic--inner">
				@if($currentProfile->avatar)
					<img src="{{$currentProfile->avatar}}" alt="">
				@endif
			</span>
		</figure>
		<div class="user_menu--user_data">
			<dl class="user_menu--user_name">
				<dt>
					@if (trim($currentProfile->full_name))
						<strong>{{$currentProfile->full_name}}</strong>
					@endif
					<span>{{$typeName}}</span>
				</dt>
				<dd>{{$currentUser->email}}</dd>
			</dl>
		</div>
	</a>
@else
	<a href="{{route('dashboard-myprofile')}}" class="user_menu--opener" data-usermenu-el="opener" title="{{$currentProfile->full_name}}">
		<figure class="user_menu--userpic userpic m-circle">
			<span class="userpic--inner">
				@if($currentProfile->avatar)
					<img src="{{$currentProfile->avatar}}" alt="">
				@endif
			</span>
		</figure>
		<span class="user_menu--opener_text">
			@if(trim($currentProfile->full_name))
				<span class="user_menu--opener_label">{{$currentProfile->full_name}}</span>
			@endif
			<span class="user_menu--opener_type">{{ucfirst($currentProfile->type)}} Account</span>
		</span>
	</a>
@endif

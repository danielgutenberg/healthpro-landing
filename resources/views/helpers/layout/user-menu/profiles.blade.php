<div class="user_menu--profiles {{$type == 'slideout' ? 'm-slideout' : ''}}"  {!! $type == 'slideout' ? ' data-usermenu-profiles="dropdown"' : '' !!}>
	@foreach($userProfiles as $profile)
		<a href="{{route('dashboard-myprofile-switch-profile', [$profile->id])}}" class="user_menu--user m-profile">
			<figure class="user_menu--userpic userpic m-small m-circle">
				<span class="userpic--inner">
					@if($profile->avatar)
						<img src="{{$profile->avatar}}" alt="">
					@endif
				</span>
			</figure>
			<div class="user_menu--user_data">
				<dl class="user_menu--user_name">
					<dt>
						@if(trim($profile->fullName))<strong>{{$profile->fullName}}</strong>@endif
						<span>{{$profile->type}}</span>
					</dt>
					<dd>{{$currentUser->email}}</dd>
				</dl>
			</div>
		</a>
	@endforeach
	@if(!$hasClientProfile)
		<div class="user_menu--register">
			<a href="{{route('dashboard-get-register-as', ['type' => 'client'])}}" class="user_menu--register_link">{{_('Sign Up as a Client')}}</a>
		</div>
	@endif
	@if(!$hasProviderProfile)
		<div class="user_menu--register">
			<a href="{{route('dashboard-get-register-as', ['type' => 'provider'])}}" class="user_menu--register_link">{{_('Are You a Health Professional?')}}</a>
		</div>
	@endif
</div>

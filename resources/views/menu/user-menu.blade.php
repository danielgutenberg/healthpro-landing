<ul>
	<li class="header--user--item m-user">
		<div class="user_menu m-dropdown" data-usermenu>

			{!! UserMenuHelper::renderUser('header') !!}

			<div class="user_menu--dropdown" data-usermenu-el="dropdown">

				{!! UserMenuHelper::renderUser('dropdown') !!}
				{!! UserMenuHelper::renderUserProfiles('dropdown') !!}

				<footer class="user_menu--footer">
					<a href="{{route('dashboard-myprofile')}}" class="user_menu--manage btn m-blue">{{_('Dashboard')}}</a>
					<a href="{{route('auth-logout')}}" class="user_menu--logout btn m-white">{{_('Log Out')}}</a>
				</footer>
			</div>
		</div>
	</li>
</ul>

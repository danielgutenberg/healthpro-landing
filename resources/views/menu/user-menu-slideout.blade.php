<div class="user_menu m-slideout" data-usermenu>

	{!! UserMenuHelper::renderUser('slideout') !!}
	{!! UserMenuHelper::renderUserProfiles('slideout') !!}

	<footer class="user_menu--footer m-slideout">
		<a href="{{route('dashboard-myprofile')}}" class="user_menu--manage btn m-blue">{{_('Dashboard')}}</a>
		<a href="{{route('auth-logout')}}" class="user_menu--logout btn m-white">{{_('Log Out')}}</a>
	</footer>
</div>

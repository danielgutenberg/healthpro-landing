@extends('site.layouts.maintenance')
@section('title')
Undergoing Maintenance
@stop
@section('wrapper-class')
m-503
@stop
@section('content')

<div class="error_503">
	<div class="error_503--inner">
		<div class="error_503--text">
			<h1>We are working on getting you a better experience</h1>
			<p>We will be back up and available any minute now.</p>
		</div>
		<ul class="error_503--social">
			<li>
				<a href="{{ Setting::get('social_facebook') }}" class="error_503--social_link m-facebook" role="external" target="_blank" title="Facebook"></a>
			</li>
			<li>
				<a href="{{ Setting::get('social_twitter') }}" class="error_503--social_link m-twitter" role="external" target="_blank" title="Twitter"></a>
			</li>
			<li>
				<a href="{{ Setting::get('social_googleplus') }}" class="error_503--social_link m-googleplus" role="external" target="_blank" title="Google+"></a>
			</li>
			<li>
				<a href="{{ Setting::get('social_linkedin') }}" class="error_503--social_link m-linkedin" role="external" target="_blank" title="LinkedIn"></a>
			</li>
			<li>
				<a href="{{ Setting::get('social_youtube') }}" class="error_503--social_link m-youtube" role="external" target="_blank" title="YouTube"></a>
			</li>
		</ul>
	</div>
</div>

@stop

@extends('site.layouts.default')
@section('title')
Oh my! We lost it!
@stop
@section('wrapper-class')
m-404
@stop
@section('content')

	<div class="error_404">
		<div class="error_404--inner">
			<div class="error_404--text">
				<h1>{{$status}}...</h1>
				<p>The page you are looking for could not <br> be found or doesn't exist.</p>
			</div>
		</div>
	</div>

@stop

@section('contact-form--title')
Need help finding something?
@stop

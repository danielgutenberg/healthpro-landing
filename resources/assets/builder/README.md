# HealthPro Web Application


## Installation

### Preinstall Requirements

1. To make sure the web app works properly you should download and install Virtualbox, Vagrant, and Ansible:

 - [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
 - [Vagrant](http://www.vagrantup.com/)
 - [Ansible](http://www.ansible.com/)

2. Add the `healthpro.app` to the /etc/hosts file and map it to 127.0.0.1

3. To fix an error in ansible <= 1.7.2 run (locally on your computer) `export PYTHONIOENCODING='utf-8'`


### Application Installation

1. Create a base folder.
2. Clone both the homestead and healthpro-com repo into the folder
	* Clone the homestead repo from [http://alab.us/healthpro/homestead](http://alab.us/healthpro/homestead) and run
	* `git clone git@alab.us:healthpro/homestead.git`
	* Clone the healthpro repo from [http://alab.us/healthpro/healthpro-com](http://alab.us/healthpro/healthpro-com) and run
	* `git clone git@alab.us:healthpro/healthpro-com.git`
3. Cd into the homstead folder and run `vagrant up`:
  ```
  cd homestead
  vagrant up
  ```

4. Go to [healthpro.app:8000](http://healthpro.app:8000) If you did everything right you will see the application homepage.

5. Copy .env.example to .env

6.  Ssh into the vagrant server, run `sudo nano /etc/php5/fpm/php.ini` to edit the file and add `xdebug.max_nesting_level = 1000` at the top/bottom/anywhere then run `sudo service php5-fpm restart`

*More information about Homestead can be found here:*

[http://laravel.com/docs/homestead](http://laravel.com/docs/homestead)

### Updating the site

When the site is working, you can run `git pull`/`composer install` on your local machine but migrations must be run within the vagrant. Keep in mind there are errors that can occur when you have different migrations than another branch, so ALWAYS use `rebuildOnBranch` to switch branches

To udate the vagrant, you can use the ZSH helper function rebuildOnBranch like so

```
vagrant ssh
rebuildOnBranch ~/www/healthpro master
```

Which runs the following:

```
cd ~/www/healthpro
php artisan migrate:reset --force
git fetch
git checkout master
git pull
composer update --no-scripts
composer run-script post-update-cmd 
php artisan migrate --force
php artisan db:seed --force
```

First, we rollback the db to a prisitine clean state
We run composer in two steps to ensure new Packages are downloaded and installed before artisan is loaded.
This is because when we git pull with a new package that hasn't been install'd it will break composer and artisan.

###To update the CSS/JS run:

`doDesignStuff ~/www/healthpro`

Which runs the following:
```
cd ~/www/healthpro
cd resources/assets/builder
npm install 
gulp all
cd /$1
cd resources/assets/admin/src
npm install
gulp
```



### Default Info

Default admin user is:
admin@healthpro.com
admin

Default member is:
zozo@zozo.com
zozo

## When Vagrant Provisioning fails.

Homestead will run the following tasks when you run `vagrant up` if there was an error, or you want run the app outside of vagrant, you will need to perform the steps manually.

### Install ZSH

We use zsh to add a more functional command line and add some nice helper functions.

```
sudo apt-get install zsh
Sudo chsh -s $(which zsh) vagrant
git clone https://github.com/robbyrussell/oh-my-zsh.git /home/vagrant/.oh-my-zsh
rm -f ~/.zshrc
ln -s ~/dotfiles/.zshrc ~/.zshrc
chown vagrant:vagrant ~/.zshrc
```

### Setup HealthPro


```
cp ~/homestead_configs/known_hosts ~/.ssh/known_hosts
composer install
php artisan migrate
php artisan db:seed
```

### Building CSS and other Assets

Admin CSS:

```
cd ~/www/healthpro/resources/assets/src/admin
npm install
gulp
```

Front End CSS:

```
cd ~/www/healthpro/resources/assets/builder
npm install
gulp all
```

### Common Failures

Sometimes, the installation will fail (it tends to happen at the composer install or artisan migrate steps.)

#### Recovering if Composer Fails

If the composer install fails, it's usually because of the github rate limiting. To recover you must ssh into the vagrant box, run composer and when github asks for your user/password enter it.

```
vagrant ssh
cd ~/www/healthpro
composer install
```

You may continue the provisioning scripts with:

```
exit
vagrant provision
```


### Database manipulation

To run the application you have to go to the Homestead folder and run

```
vagrant up
vagrant ssh
```

#### Create the database

Default Homestead MySQL credentials:

```
user: homestead
pass: secret
```


#### Migrations

After database creation go to your application folder (vagrant) and run

```
php artisan migrate
php artisan db:seed
```


It will create all the tables in your database and seed them with temporary values.

**NOTE** you will have to run migration commands after every manipulation with the database. Please do not forget to add understandable comment messages to your commits to make sure everybody know that.

Read more about [Laravel Migrations](http://laravel.com/docs/migrations).



### Running the application

To run the application you have to run your vagrant from the Homestead folder:

```
vagrant up
```

Then simply open the link in your browser:

[healthpro.app:8000](http://healthpro.app:8000)

If you did everything right you will see the application homepage.



### Administration area

Application administration area is location under the `/admin` link.


#### Adding styles to the administration area

Go to `public/assets/admin`.
There you will see the [Foundation Framework](foundation.zurb.com) installation which is used for the admin area styles.

Then run

```
bower install
npm install
grunt
```
After that you can make changes to the JS and SCSS files.



#### Creating models, repositories.

There are multiple traits to use if you're willing to give your model some advanced functionality:

- FileTrait - attach files to the model
- MetaTrait - add custom meta fields to the model
- TreeTrait - use when you have to make a collection tree
- TranslatableTrait - add the translation to the model

If you want to give the model even more functionality - before adding methods directly to the model decide if it
should be wrapped in a trait so it is reusable.

Models shouldn't iteract with controllers and other parts of the application directly. You have to create a repository layer
for that. Repository will help you to wrap your model with an object with helper methods.

Repositories should be added to the app/WL/Repositories directory. One model - one repository folder.
Repository should have an interface and the implementation.

Example:

```
class DbPageRepository extends DbRepository implements PageRepository
{
    // your code goes here
}
```

To access your repository - add the dependency inversion to the app/Providers/RepositoriesServiceProvider.php and use it only
through your interface (in our case it is PageRepository).

Example:

```
class PageController extends ControllerBase
{
    public function __construct(PageRepository $repository) {
        // ...
    }
}
```

When you need to create the CRUD for your model - please make sure to not overfeed your controllers. Your controllers should run as few
as possible actions. There is a ruleset for these kind of actions.

1. Load your model through repository
2. All form iteractions should use the $request to validate form and get the values
3. Request should be placed into the app/Requests folder and should extend the FormRequest and implement the [Model]Feeder interface.
    * Feeder is an interface with set of methods to help you to get different values from your request. For example - you have a Page that
      uses MetaTrait. It means that you have to add/save the meta in your admin dashboard. To do that you have to create the PageFeeder interface
      in the app/WL/Feeders/[Model] directory.
    * Add the method meta() there to make sure your request implements one.
      Now in the request you will have methods for grabbing this particular part of its data. That way you make sure if someone adds another request
      it will be provided with the right methods and your code won't fail.
4. After implementing the methods from the feeder - append it to your model (Please notice - model, not repository, because we're iteracting with the loaded object)
   ```
   $entry->setFeeder( $request );
   ```
   It will help you to get needed data on observing events.
5. Create the model observer in the app/WL/Model/Observers directory and load it in the app/Providers/ObserversServiceProvider.php file.
6. Now you can observe the events through this class - created, creating, updated, deleting, etc. And here we will need the $request and can access
   it through the model feeder -> $model->getFeeder(). If you need the meta values, call $model->getFeeder()->meta().
7. To make sure the form populates properly after the submission/error you have to create the custom Populator. DO NOT iteract with Former/Form
   classes directly.

   * Create the populator and its interface in the app/WL/Populators/[Model] directory.
   * Your populator shoud extend the FormerPopulator (if you are using Former as your builder) and implement [Model]Populator (event if it's empty, doesn't matter)
   * Add your populator to the provider in app/Providers/PopulatorsServiceProvider.php
   * Add your populator to the controller action through the dependency injection and you're ready to go
   * Call the population
   ```
   $populator->setModel($entry)->populate();
   ```
8. Write tests to your
   * Repository (unit)
   * Model (unit)
   * Controller (functional)

   Don't forget to mock objects with Mockery.


#### Using AssetManager
If you want to add some javascript, css, favicons to the website - please use AssetManager.
AssetManager can handle multiple containers that you can add anywhere on the page. The most common are - *header* and *footer*.
The manager knows what type of files you're trying to add and adds them to the right collections.


To add a file to AssetManager use it's Facade. Here's a simple example:
```
AssetManager::add([
    'js/app.js',
    'css/app.css'
]);

AssetManager::add('css/app.css');
AssetManager::add('css/app.css', 'header');
AssetManager::add('js/app.js', 'footer');
```

Also you can add the libraries from external resources.
```
AssetManager::add('http://youramazinghost.com/css/app.css');
```
You can also add IE specific CSS/JS.
```
AssetManager::setPrefix('assets/admin')
    ->add([
        [
            'asset' => '//html5shim.googlecode.com/svn/trunk/html5.js',
            'params' => ['if' => 'lt IE 9']
        ]
    ]);
```
Also if you need to add the scripts and styles from a folder - you can prefix all your files like this:
```
AssetManager::setPrefix('assets/front')
    ->add([
        'js/app.js',
        'css/app.css',
    ]);
```
If you need to pass parameters to the manager, e.g. charset, type, rel, etc. - you can pass needed params.
```
AssetManager::setPrefix('assets/admin')
    ->add([
        [
            'asset' => 'js/app.js',
            'params' => ['type' => 'text/javascript', 'charset' => 'UTF-8']
        ],
        [
            'asset' => 'css/app.css',
            'params' => ['rel' => 'stylesheet', 'type' => 'text/css']
        ]
    ]);
```
*Please note that when you're adding simple css files - you don't need to pass the rel and type - manager will add them by default.*

To show the assets on the page use these calls.
```
	{!! AssetManager::css('header') !!}
	{!! AssetManager::js('header') !!}
```

More documentation avaliable in the AssetManager itself. Please don't be scared to read the code :).

# Front-end part of HealthPro

## How to work with

### assets
By default we have `src` folder. This folder is for development.
After build we'll have `built` folder with css/js/images.
Developer must work only with `src` folder.

For compiling `src` folder you need to install `gulp`, `coffee-script` globaly.
Go to `assets/builder` and make `npm i` in console. This will install all required plugins. 

## Tasks

### Common part
- `gulp common` build common folder
- `gulp common:dev` - build common folder + watch
- 
### Front part
- `gulp front` build front folder + common
- `gulp front:dev` - build front folder + common + watch
### Common part
- `gulp dashboard` build dashboard folder
- `gulp dashboard:dev` - build dashboard folder + common + watch


## Structure

- `src`
    + `section`
        + `styles` - .styl
            * `helpers` - userful libs and mixins for stylus
                - `ak` - my own mixins
        + `scripts` - .coffee
        + `images`
            * `content` - content images only
            * `design` - decorative images or svgs
- `built`
    + `section`
        + `libs` — bower components. generates automatic
        + `styles`
        + `scripts`
        + `images`

## Helpful Links
- Font Awesome SVG icons — https://github.com/encharm/Font-Awesome-SVG-PNG
- icons8 — http://icons8.com/
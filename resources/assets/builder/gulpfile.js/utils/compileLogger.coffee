gutil = require 'gulp-util'
handleErrors = require './handleErrors'
prettifyTime = require './prettifyTime'

module.exports = (err, stats) ->
	if err
		throw new (gutil.PluginError)('webpack', err)

	return unless stats?

	statColor = if stats.compilation.warnings.length < 1 then 'green' else 'yellow'

	if stats.compilation.errors.length > 0
		stats.compilation.errors.forEach (error) ->
			handleErrors error
			statColor = 'red'
			return

	compileTime = prettifyTime(stats.endTime - (stats.startTime))
	gutil.log gutil.colors[statColor](stats)
	gutil.log 'Compiled with', gutil.colors.cyan('webpack'), 'in', gutil.colors.magenta(compileTime)

	return

pathBuilder = require './pathBuilder'
fs = require 'fs'

module.exports = ->
	filepath = pathBuilder('', 'dest') + '/' + 'manifest.json'

	try
		manifest = require filepath
	catch e
		return ''

	return manifest.assetsVersion

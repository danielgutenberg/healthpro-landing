module.exports = (milliseconds) ->
  if milliseconds > 999
    (milliseconds / 1000).toFixed(2) + ' s'
  else
    milliseconds + ' ms'

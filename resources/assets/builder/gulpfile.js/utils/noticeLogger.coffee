gutil = require 'gulp-util'

module.exports = (notice, color = 'blue') ->
	# format JSON object
	if typeof notice is 'object'
		notice = JSON.stringify notice, null, 4

	gutil.log gutil.colors[color](notice)

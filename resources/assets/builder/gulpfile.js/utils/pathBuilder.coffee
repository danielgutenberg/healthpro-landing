path = require 'path'
gulp = require 'gulp'
_ = require 'lodash'

module.exports = (key, type = 'src') ->

	config = switch type
		when 'src' then gulp.config.src
		when 'dest' then gulp.config.dest

	dir = if key then _.get(config, key) else ''

	return path.resolve(config.path + dir)

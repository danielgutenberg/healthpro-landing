gulp = require 'gulp'
fs = require 'fs'
yaml = require 'js-yaml'
fwdRef = require 'undertaker-forward-reference'
requireDir = require 'require-dir'

# load config file
gulp.config = yaml.load fs.readFileSync("./config.yml", "utf8")

# simple environment var
gulp.HP_ENV = 'development'


# gulp 4
# register forward references
#gulp.registry fwdRef()

# load tasks
requireDir './tasks',
	recurse: true

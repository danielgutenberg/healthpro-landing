gulp = require 'gulp'
consolidate = require 'gulp-consolidate'
iconfont = require 'gulp-iconfont'
pathBuilder = require '../utils/pathBuilder'
assetsVersion = require '../utils/assetsVersion'


# generate iconfont from svgs
iconfontTask = ->

	src = pathBuilder 'svg.icons.all', 'src'
	dest = pathBuilder 'fonts.icons.path', 'dest'

	iconsSrc = pathBuilder 'framework.icons.path', 'src'
	template = pathBuilder 'framework.icons.template.path', 'src'

	gulp.src(src)
		.pipe iconfont
			fontName: "icons"
			appendCodepoints: false
			normalize: true
		.on 'glyphs', (glyphs) =>
			glyphs.forEach (glyph, idx, arr) ->
				arr[idx].codepoint = glyph.unicode[0].charCodeAt(0).toString(16).toUpperCase()

			gulp.src(template)
				.pipe consolidate 'lodash',
					glyphs: glyphs
					version: assetsVersion()
					fontName: 'iconfont'
					fontPath: dest
				.pipe gulp.dest iconsSrc
		.pipe(gulp.dest(dest))

gulp.task 'iconfont', iconfontTask

module.exports = iconfontTask


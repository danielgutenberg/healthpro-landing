gulp = require 'gulp'
watch = require 'gulp-watch'
sequence = require 'gulp-sequence'
livereload = require 'gulp-livereload'

pathBuilder = require '../utils/pathBuilder'
noticeLogger = require '../utils/noticeLogger'

images = require './images'
styl = require './styl'
iconfont = require './iconfont'

watcher = ->
	livereload.listen()

	imagesSrc = pathBuilder 'images.all', 'src'
	stylSrc = pathBuilder 'styles.all', 'src'
	iconFontSrc = pathBuilder 'svg.icons.all', 'src'

	watch imagesSrc, ->
		noticeLogger '--> Starting images task', 'blue'
		images()
	watch stylSrc, ->
		noticeLogger '--> Starting styl task', 'blue'
		styl()
	watch iconFontSrc, ->
		noticeLogger '--> Starting iconfont task', 'blue'
		iconfont()

	noticeLogger '--> Watching files...', 'blue'

# gulp 4
#	gulp.watch imagesSrc, gulp.parallel('images')
#	gulp.watch stylSrc, gulp.parallel('styl')
#	gulp.watch iconFontSrc, gulp.parallel('iconfont')

# gulp 4
#gulp.task 'watch', gulp.series('webpack:watch', watcher)

gulp.task 'watcher', watcher
gulp.task 'watch', sequence('webpack:watch', 'watcher')

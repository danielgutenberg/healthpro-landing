livereload = require('gulp-livereload')
changed = require('gulp-changed')
gulp = require('gulp')
imagemin = require('gulp-imagemin')
pathBuilder = require '../utils/pathBuilder'

imagesTask = ->
	src = pathBuilder 'images.all', 'src'
	dest = pathBuilder 'images.path', 'dest'

	gulp.src(src)
		.pipe(changed(dest))
#		.pipe(imagemin())
		.pipe(gulp.dest(dest))
		.pipe livereload()

gulp.task 'images', imagesTask

module.exports = imagesTask


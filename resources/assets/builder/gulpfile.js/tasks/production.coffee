gulp = require('gulp')
sequence = require('gulp-sequence')

productionTask = (cb) ->
	# simple environment variable
	gulp.HP_ENV = 'production'
	sequence('clean', 'manifest', 'build', cb)

gulp.task 'production', productionTask

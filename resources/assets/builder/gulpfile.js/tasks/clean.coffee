gulp = require 'gulp'
del = require 'del'
pathBuilder = require '../utils/pathBuilder'

gulp.task 'clean', (cb) ->
	builtFiles = pathBuilder 'path', 'dest'
	del(["#{builtFiles}/*"], force: true).then -> cb()
	return

gulp = require('gulp')
livereload = require('gulp-livereload')
stylus = require('gulp-stylus')
sourcemaps = require('gulp-sourcemaps')
autoprefixer = require('gulp-autoprefixer')
plumber = require('gulp-plumber')
gulpif = require('gulp-if')
cssnano = require('gulp-cssnano')

handleErrors = require('../utils/handleErrors')
pathBuilder = require '../utils/pathBuilder'

stylTask = ->
	src = pathBuilder 'styles.main', 'src'
	dest = pathBuilder 'styles.path', 'dest'

	gulp.src(src)
		.pipe(plumber({errorHandler: handleErrors}))
		.pipe(gulpif(gulp.HP_ENV isnt 'production', sourcemaps.init()))
		.pipe(stylus({'include css': true}))
		.pipe(autoprefixer({browsers: ['last 3 version', 'last 4 iOS versions']}))
		.pipe(gulpif(gulp.HP_ENV is 'production', cssnano())) # minify only on production
		.pipe(gulpif(gulp.HP_ENV isnt 'production', sourcemaps.write())) # external sourcemaps
		.pipe(gulp.dest(dest))
		.pipe(livereload())

gulp.task 'styl', stylTask

module.exports = stylTask

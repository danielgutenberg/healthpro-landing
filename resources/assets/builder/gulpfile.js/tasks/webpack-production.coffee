gulp = require('gulp')
webpack = require('webpack')

compileLogger = require('../utils/compileLogger')
config = require('../lib/webpack.config')('production')

gulp.task 'webpack:production', (cb) ->
	webpack config, (err, stats) ->
		# do not log stats for production
		compileLogger err, stats
		cb()

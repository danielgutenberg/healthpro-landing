gulp = require 'gulp'
fs = require 'fs'
gitRev = require 'git-rev'
pathBuilder = require '../utils/pathBuilder'

gulp.task 'manifest', (cb) ->

	dirpath = pathBuilder('', 'dest')
	filepath = "#{dirpath}/manifest.json"

	# make sure we have the dest directory
	unless fs.existsSync dirpath
		fs.mkdirSync dirpath

	gitRev.short (str) ->
		json =
			assetsVersion: str

		fs.writeFileSync filepath, JSON.stringify(json)
		cb()

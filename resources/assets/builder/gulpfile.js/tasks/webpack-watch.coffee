gulp = require 'gulp'
livereload = require 'gulp-livereload'
webpack = require 'webpack'

webpackConfig = require '../lib/webpack.config'
compileLogger = require '../utils/compileLogger'

gulp.task 'webpack:watch', (callback) ->
	initialCompile = false
	webpack(webpackConfig('development')).watch 200, (err, stats) ->
		compileLogger err, stats
		if initialCompile
			# trigger livereload after changing the files
			livereload.reload('/')
		else
			# On the initial compile, let gulp know the task is done
			initialCompile = true
			callback()

gulp = require 'gulp'
gutil = require 'gulp-util'
path = require 'path'
webpack = require 'webpack'
yaml = require 'js-yaml'
fs = require 'fs'

pathBuilder = require '../utils/pathBuilder'

module.exports = (env) ->

	aliases = yaml.load fs.readFileSync("./aliases.yml", "utf8")

	src = pathBuilder 'scripts.path', 'src'
	dest = pathBuilder 'scripts.path', 'dest'

	nodeModulesSrc = path.resolve("node_modules")

	filenamePattern = '[name].js'
	chunkFilenamePattern = "[id]-[hash]-bundle.js"

	webpackConfig =
		context: src
		plugins: []
		resolve:
			root: [nodeModulesSrc, src]
			extensions : ['', '.js', '.coffee']
			alias: aliases.aliases
		resolveLoader:
			root: nodeModulesSrc
		module:
			loaders : [
				{
					test    : /\.coffee$/
					loader  : 'coffee-loader'
					exclude : /node_modules/
				}
				{
					test: /\.css$/
					loader: "style-loader!css-loader"
				}
				{
					test: /\.styl/
					loader: "style-loader!css-loader?{url:false}!stylus-loader"
					exclude : /node_modules/
				}
			]

	webpackConfig.entry = gulp.config.tasks.webpack.entries
	webpackConfig.output =
		path: dest
		pathinfo: false
		filename: filenamePattern
		chunkFilename: chunkFilenamePattern
		publicPath: gulp.config.tasks.webpack.publicPath
		libraryTarget : 'umd'

	webpackConfig.plugins.push new webpack.optimize.CommonsChunkPlugin(
		name: 'common'
		filename: filenamePattern
		minChunks: 2
	)

	# ignore plugin
	# when we require momentJS we don't want to load its locales because of the size
	webpackConfig.plugins.push new webpack.IgnorePlugin(/^\.\/locale$/, [/moment$/])

	# make these libraries be available globally
	# we use jQuery & lodash everywhere so they should be here.
	# the vex and script library are also used all over the app so decided to put them here also
	webpackConfig.plugins.push new webpack.ProvidePlugin({
		_                : 'lodash'
		$s               : "$s"
	})

	switch env
		when 'development'
			webpackConfig.devtool = gutil.env.wsm ? 'eval'
			webpack.debug = true

		when 'production'
			webpackConfig.devtool = false
			webpack.debug = false

			webpackConfig.plugins.push new webpack.optimize.DedupePlugin()
			webpackConfig.plugins.push new webpack.optimize.UglifyJsPlugin({sourceMap: false})
			webpackConfig.plugins.push new webpack.NoErrorsPlugin()

	webpackConfig


# dependecies
gulp = require 'gulp'
sass = require 'gulp-sass'
coffee = require 'gulp-coffee'
bower = require 'gulp-bower'
util = require 'gulp-util'
minify = require 'gulp-minify'
concat = require 'gulp-concat'
plumber = require 'gulp-plumber'
prettify = require 'gulp-jsbeautifier'

# paths
config =
	src:
		scripts:
			path: 'scripts/'
			all: 'scripts/**/*.coffee'

		styles:
			paths: 'styles/'
			all: 'styles/**/*.scss'
			app: 'styles/app.scss'

		images:
			paths: 'images/'
			all: 'images/**/*.*'
	built:
		libs:
			path: '../../../../public/assets/admin/libs'

		scripts:
			path: '../../../../public/assets/admin/scripts'

		styles:
			path: '../../../../public/assets/admin/styles'

		images:
			path: '../../../../public/assets/admin/images'


# Play sound when error
consoleErorr = (err) ->
	util.beep()
	console.log err

	return

gulp.task 'bower', ->
	bower config.built.libs.path

gulp.task 'sass', ->
	gulp.src config.src.styles.app
		.pipe plumber
			errorHandler: consoleErorr
		.pipe sass()
		.pipe gulp.dest config.built.styles.path

gulp.task 'coffee', ->
	gulp.src config.src.scripts.all
		.pipe plumber
			errorHandler: consoleErorr
		.pipe coffee()
    .pipe(prettify({config: '.jsbeautifyrc', mode: 'VERIFY_AND_WRITE'}))
    .pipe concat('app.js')
    .pipe gulp.dest(config.built.scripts.path)

gulp.task 'images', ->
	gulp.src config.src.images.all
		.pipe plumber
			errorHandler: consoleErorr
		.pipe gulp.dest config.built.images.path

gulp.task 'watch', ->
	gulp.watch config.src.images.all, ['images']
	gulp.watch config.src.styles.all, ['sass']
	gulp.watch config.src.scripts.all, ['coffee']

gulp.task 'default', ['bower'], ->
	gulp.start ['sass', 'coffee', 'images']

gulp.task 'dev', ['default', 'watch']

jQuery ($) ->
# do pages stuff

	$.fn.initReviewsPage = ->
		$(this).each ->
			$page = $(this)

			$statusActions = $page.find('.status-actions a')

			$statusActions.on "click", (e) ->
				e.preventDefault()
				$this = $(this)
				ids = {}
				ids[$this.data('id')] = 1
				data =
					ids: ids
					status: $this.data('status')
					_token: $page.find("input[name=_token]").val()
					_method: "PUT"
				$.post($this.prop("href"), data, (response) ->
					$.notify response.message,
						type: (if response.success then "success" else "alert")
					$this.parents("tr").attr('class', "status-#{data.status}") if response.success
				, "json").fail (response, type, message) ->
					$.notify message,
						type: "alert"




	$('#page-reviews').initReviewsPage()

jQuery ($) ->
  $.fn.initTranslationsTable = ->

    _get_translation_html = (options) ->
      options = options or {}
      html = "<div class=\"translation\"><input name=\"translation[{{translation_id}}][translations][content][]\" type=\"text\" value=\"\"/><input name=\"translation[{{translation_id}}][translations][frequency][]\" type=\"number\" value=\"0\"/><a href=\"#\" data-new=\"1\" class=\"translation-value-remove\">Remove Value</a></div>"
      $.each options, (k, v) ->
        html = html.replace(new RegExp("{{" + k + "}}", "g"), v)
      html

    $(this).each ->

      $table = $(this)

      $table.on "click", ".translation-value-add", (e) ->
        e.preventDefault()
        $this = $(this)
        $td = $this.parents("td")
        $group_id = $td.siblings(".cell-name").find("input[type=text]").data("id")
        $td.find(".translations").append _get_translation_html(translation_id: $group_id)

      $table.on "click", ".translation-value-remove", (e) ->
        e.preventDefault()
        $this = $(this)
        $translation = $this.parents(".translation")

        if $this.data("new")
          $translation.remove()

        $.get($this.attr("href"), (response) ->
          $.notify response.message,
            type: (if response.success then "success" else "alert")

          if response.success
            $translation.fadeOut "fast", ->
              $(this).remove()

          return
        , "json").fail (response, type, message) ->
          $.notify message,
            type: "alert"

  $('#translations-wrap').initTranslationsTable()

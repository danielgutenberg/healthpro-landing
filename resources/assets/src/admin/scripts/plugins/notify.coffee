jQuery ($) ->
	$.notify = (message, options) ->
		return if "undefined" is typeof (message)

		options = $.extend(
			type: "info"
			empty: true
			messages:
				alert: "Error!"
				warning: "Warning!"
				info: "Info!"
				success: "Success!"
		, options)

		$notifications = $("#notifications")
		$box = "<div data-alert class=\"alert-box " + options.type + "\"><a href=\"#\" class=\"close\">&times;</a><strong>" + options.messages[options.type] + "</strong> " + message + "</div>"

		$notifications.html '' if options.empty
		$notifications.append($box).foundation()

	$("div.alert-box .close").on "click", ->
		$(this).closest("div").hide 300

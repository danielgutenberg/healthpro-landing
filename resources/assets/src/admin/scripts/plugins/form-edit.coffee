jQuery ($) ->
  $.fn.initEditForm = ->
    $(this).each ->

      $form   = $(this)
      $method = $form.find("input[name=_method]")

      $form.find(".entry-remove").on "click", (e) ->
        e.preventDefault()
        $method.val "DELETE"
        $form.submit()

      # a temporary and ugly solution for this bug
      # https://github.com/laravel/framework/issues/6189
      $form.on "submit", (e) ->
        $files = $form.find("input[type=file]")
        $.each $files, ->
          $(this).remove()  if "" is $(this).val()
        true

  $('#form-entry-edit').initEditForm()

jQuery ($) ->
  implementRestApiMethod = ->
    dataMethod = $("[data-method]")
    dataMethod.append(->
      "\n" + "<form action='" + $(this).attr("href") + "' method='POST' style='display:none'>" + "<input type='hidden' name='_method' value='" + $(this).attr("data-method") + "'>" + "<input type='hidden' name='_token' value='" + window.GLOBALS._TOKEN + "'>" + "</form>"
    ).removeAttr("href").attr "style", "cursor:pointer;"
    if dataMethod.attr("data-method") is "delete"
      dataMethod.attr "onclick", "if(confirm(\"Are you sure?\")) { $(this).find(\"form\").submit() } else { return false; };"
    else
      dataMethod.attr "onclick", "$(this).find(\"form\").submit();"

  implementRestApiMethod()

jQuery ($) ->
	$.fn.initListForm = ->

		_collectIds = ($checkboxes) ->
			ids = {}
			$.each $checkboxes, (k, v) ->
				checkbox = $(this)
				ids[checkbox.data("id")] = checkbox.is(":checked") * 1
			ids



		$(this).each ->
			$form = $(this)
			$table = $form.find("table")
			$checkboxes = $table.find("tbody tr td input[type=checkbox]")
			$checkboxToggle = $table.find("thead tr th input[type=checkbox]")
			$removeLink = $form.find(".remove-selected")
			$statusLink = $form.find(".status-selected")

			$checkboxToggle.on "click", ->
				$checkboxes.prop "checked", $checkboxToggle.is(":checked")

			$checkboxes.on "click", ->
				$checkboxToggle.prop "checked", $checkboxes.filter(":checked").length is $checkboxes.length

			$removeLink.on "click", (e) ->
				e.preventDefault()
				data =
					ids: _collectIds $checkboxes
					_token: $form.find("input[name=_token]").val()
					_method: "DELETE"
				$.post($removeLink.prop("href"), data, (response) ->
					$.notify response.message,
						type: (if response.success then "success" else "alert")
					$checkboxes.filter(":checked").parents("tr").remove()  if response.success
				, "json").fail (response, type, message) ->
					$.notify message,
						type: "alert"


			$statusLink.on "click", (e) ->
				e.preventDefault()
				$this = $(this)
				data =
					ids: _collectIds $checkboxes
					status: $this.data('status')
					_token: $form.find("input[name=_token]").val()
					_method: "PUT"
				$.post($statusLink.prop("href"), data, (response) ->
					$.notify response.message,
						type: (if response.success then "success" else "alert")
					$checkboxes.filter(":checked").attr('checked', false).parents("tr").attr('class', "status-#{data.status}")  if response.success
				, "json").fail (response, type, message) ->
					$.notify message,
						type: "alert"

	$('#form-entries-index').initListForm()

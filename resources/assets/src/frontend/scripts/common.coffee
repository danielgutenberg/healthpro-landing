################################
# require all needed libraries #
################################

# make sure jquery is available globally.
# hubspot scripts can't find it when it's loaded as a CommonJS module
# so we have to load it here.
window.jQuery = window.$ = require 'jquery'

# cache $script
require '$s'

# cache config
require 'hp.api'

# cache vex plugin
window.vex = require 'vex'
window.vexDialog = require 'vexDialog'

# cache backbone
require 'backbone'
require 'backbone.validation'
require 'backbone.stickit'
require 'backbone.cocktail'
require 'backbone.deepmodel'

# cache handlebars
require 'handlebars'

# cache jquery ui stuff
require 'jquery-ui/autocomplete'
require 'jquery-ui/datepicker'

# cache momentjs
require 'moment'

# cache form
require 'framework/form'
# init tooltips
require 'framework/tooltip'
# init alerts
require 'framework/alerts'
# init buggyf
require 'framework/vubuggyfill'
# init slideout menu
require 'framework/slideout'
# init popups
require 'framework/popups_manager'
# init external links
require 'framework/external_links'
# init error handler
require 'framework/error_handler'
# init scroll to lib
require 'framework/scroll_to'
# init google maps (NOT LOADED)
require 'framework/gmaps'

Device = require 'utils/device'
$('body').addClass "m-device_#{Device.slug}"

# start backbone history
unless Backbone.History.started
	Backbone.history.start
		pushState: true

###
  original: https://github.com/uzikilon/backbone-poller
###

# Default settings
defaults =
	delay: 1000
	condition: ->
		true
# Available events
events = [
	'start'
	'stop'
	'fetch'
	'success'
	'error'
	'complete'
]
timers = []
TimerManager =
	get: (model, options) ->
		timer = findTimer(model)
		if !timer
			timer = new Timer(model, options)
			timers.push timer
		else
			timer.set options
		if options and options.autostart == true
			timer.start silent: true
		timer
	size: ->
		timers.length
	reset: ->
		while timers.length
			timers[0].destroy()
		return

findTimer = (model) ->
	_.find timers, (timer) ->
		timer.model == model

Timer = (model, options) ->
	@model = model
	@cid = _.uniqueId('timer')
	@set options
	if @model instanceof Backbone.Model
		@listenTo @model, 'destroy', @destroy
	return

run = (timer) ->
	if validate(timer)
		options = _.extend({
			fetchMethod: 'fetch'
		}, timer.options,
			success: (model, resp) ->
				timer.trigger 'success', model, resp
				delayedRun timer
				return
			error: (model, resp) ->
				if timer.options.continueOnError
					timer.trigger 'error', model, resp
					delayedRun timer
				else
					timer.stop silent: true
					timer.trigger 'error', model, resp
				return
		)
		timer.trigger 'fetch', timer.model
		timer.xhr = timer.model[options.fetchMethod](options)

	return

getDelay = (timer) ->
	if _.isNumber(timer.options.delay)
		return timer.options.delay
	min = timer.options.delay[0]
	max = timer.options.delay[1]
	interval = timer.options.delay[2] or 2
	if backoff[timer.cid]
		if _.isFunction(interval)
			backoff[timer.cid] = interval(backoff[timer.cid])
		else
			backoff[timer.cid] *= interval
	else
		backoff[timer.cid] = 1
	res = Math.round(min * backoff[timer.cid])
	if max and max > 0
		res = Math.min(res, max)
	res

delayedRun = (timer, delay) ->
	if validate(timer)
		timer.timeoutId = _.delay(run, delay or getDelay(timer), timer)
	return

validate = (timer) ->
	if !timer.options.active
		return false
	if timer.options.condition(timer.model) != true
		timer.stop silent: true
		timer.trigger 'complete', timer.model
		return false
	true

_.extend Timer.prototype, Backbone.Events,
	set: (options) ->
		@options = _.extend({}, defaults, options or {})
		if @options.flush
			@off()
		_.each events, ((name) ->
			callback = @options[name]
			if _.isFunction(callback)
				@off name, callback, this
				@on name, callback, this
			return
		), this
		@stop silent: true
	start: (options) ->
		if !@active()
			options and options.silent or @trigger('start', @model)
			@options.active = true
			if @options.delayed
				delayedRun this, _.isNumber(@options.delayed) and @options.delayed
			else
				run this
		this
	stop: (options) ->
		options and options.silent or @trigger('stop', @model)
		@options.active = false
		@xhr and @xhr.abort and @xhr.abort()
		@xhr = null
		clearTimeout @timeoutId
		@timeoutId = null
		this
	active: ->
		@options.active == true
	destroy: ->
		index = _.indexOf(timers, this)
		if index > -1
			@stop().stopListening().off()
			timers.splice index, 1
		return
backoff = {}

TimerManager.getDelay = getDelay
TimerManager.prototype = Timer.prototype

module.exports = TimerManager

ServiceArea = require 'framework/service_area'

module.exports =

	getLocationName : (location) -> location.name

	getLocationAddressLabel : (location, extendedLabel = false, forceRealAddressForHomeVisits = false) ->
		locationType = if _.isObject location.location_type then location.location_type.type else location.location_type

		if locationType is 'virtual'
			return location.address.address

		if locationType is 'home-visit' and forceRealAddressForHomeVisits is false
			locationAddress = do ->
				if location.address.address_label
					location.address.address_label
				else
					"The area of #{location.address.postal_code}"

			if extendedLabel and location.service_area
				area = new ServiceArea location.service_area
				locationAddress += " (#{area.format('full', 'mi')})"

			return locationAddress

		locationCity = location.address.city ? location.address.postal_code.city
		locationProvince = location.address.province ? location.address.postal_code.province
		locationPostalCode = if _.isObject(location.address.postal_code) then location.address.postal_code.code else location.address.postal_code

		fullAddress = "#{location.address.address}"
		fullAddress += ", #{location.address.address_line_two}" if location.address.address_line_two
		fullAddress += ", #{locationCity}, #{locationProvince} #{locationPostalCode}"

		fullAddress

	getLocationTypeLabel : (location) ->
		switch location.location_type
			when 'office' then 'Office'
			when 'home-visit' then 'Home Visit'
			when 'virtual' then 'Virtual'

	getLocationGmapsUrl : (location) ->
		if location.location_type is 'office'
			return "https://maps.google.com/?q=#{location.address.latitude},#{location.address.longitude}"
		''

	gmapsLocation : (location) -> "#{location.address.latitude},#{location.address.longitude}"

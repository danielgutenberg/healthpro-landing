module.exports =
	getProfessionalName: (professional, withTitle = false) ->
		name = professional.full_name
		name = "#{professional.first_name} #{professional.last_name}" unless name
		if withTitle and professional.title
			name = "#{professional.title} #{name}"
		name

	getProfessionalAvatar: (professional, returnImg = false) ->
		avatar = professional.avatar
		if returnImg and avatar
			avatar = "<img src='#{avatar}' alt=''>"
		avatar

	getProfessionalUrl: (professional) -> professional.public_url

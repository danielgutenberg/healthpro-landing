module.exports =
	getServiceName: (service) ->
		name = [service.name]
		if service.sub_services?.length > 1
			subServices = _.filter service.sub_services, (subService) -> subService.name isnt service.name
			name = name.concat _.pluck(subServices, 'name')
		name.join(', ')


	detailedServicesName: (services) ->
		_.each services, (service) =>
			if _.where(services, { name: service.name }).length > 1
				locations = []
				_.each service.locations, (location) -> locations.push location.name
				service.detailed_name = service.name + ' - ' + locations.join(', ')
			else
				service.detailed_name = service.name
		services

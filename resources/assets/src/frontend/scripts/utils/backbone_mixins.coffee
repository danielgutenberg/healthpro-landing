module.exports =
	Models:
		Validation:
			handleErrors: (response) ->
				if response.responseJSON?
					errors = response.responseJSON.errors

	Views:
		Base:

			subViews: []
			instances: []

			# Backbone.Validation processing/rendering
			addValidation: (options) ->

				buildSelector = (view, attr, selector) ->
					if attr.indexOf('.') isnt -1
						attr = attr.split('.')
						$field = view.$el.find('[' + selector + '*="' + attr[0] + '[' + attr[1] + ']"]')
					else
						$field = view.$el.find('[' + selector + '*="' + attr + '"]')

				Backbone.Validation.bind @,
					selector: options?.selector ? 'name'
					valid: options?.valid ? (view, attr, selector) ->
						$field = buildSelector(view, attr, selector)
						$field.parents('.field, .select')
							.removeClass('m-error')
							.find('.field--error')
							.html('')

					invalid: options?.invalid ? (view, attr, error, selector) ->
						$field = buildSelector(view, attr, selector)
						$field.parents('.field, .select')
							.addClass('m-error')
							.find('.field--error')
							.html(error)
						$field.trigger('error')
						$field.on('focus.validation', ->
							$field.parents('.field, .select').removeClass('m-error')
							$field.off('focus.validation')
							)

			# universal closing method
			close: ->
				@removeSubViews()
				@removeInstances()

				@model?.delete?()
				@model?.set('id', null).destroy()
				@remove()

				return @

			removeSubView: (name, array) ->
				result = _.first( _.filter( @[array], {name: name} ) )
				if result?
					result.view.close?()
					result.view.remove?()

				if @[name]? then @[name] = null

				@[array] = _.reject( @[array], {name: name} )

			removeSubViews: (valueAfterRemoval = null) ->
				if @subViews?
					_.forEach @subViews, (subView) ->
						if subView.view?
							subView.view.close?()
							subView.view.remove?()
						else
							subView.close?()
							subView.remove?()
						subView = null
					@subViews = valueAfterRemoval

			removeInstances: (valueAfterRemoval = null) ->
				if @instances?
					_.forEach @instances, (instance) ->
						return unless instance?
						if instance.view?
							instance.view.close?()
							instance.view.remove?()
						else
							instance.close?()
							instance.remove?()
						instance = null
					@instances = valueAfterRemoval

			clearErrors: ->
				$field = @$el.find('.field.m-error, .select.m-error')
				$field.removeClass('m-error')
				$field.find('.field--error').html()

				@trigger 'error:cleared'

			showErrors: (errors, options) ->
				selector = options?.selector ? 'name'
				_.forEach errors, (data, name) =>
					error = data.messages[0]

					if name.indexOf('-') isnt -1
						name = name.split('-')
						if name.length > 2
							number = name[1]
							$field = @$el.find('[' + selector + '*="' + name[0] + '[' + name[2] + ']"]').eq(number)
						else
							$field = @$el.find('[' + selector + '*="' + name[0] + '[' + name[1] + ']"]')
					else
						$field = @$el.find('[' + selector + '*="' + name + '"]')

					$field.parents('.field, .select').addClass('m-error')
					$field.parents('.field, .select').find('.field--error').html(error)

					@trigger 'errors:showed'

			showLoading: ->
				@$el.$loading.removeClass('m-hide') if @$el.$loading and @$el.$loading.length

			hideLoading: ->
				@$el.$loading.addClass('m-hide') if @$el.$loading and @$el.$loading.length

	Common:
		# set options from constructor arguments
		setOptions: (options) ->
			_.forEach options, (val, key) =>
				@[key] = val
				return

class LoadingStack

	constructor: (@bindTo) ->
		@current = 0

		{
			show: @show
			hide: @hide
			trigger: @trigger
		}

	loading: (caller) ->
#		console.log 'TRIGGERING LOADING', @current, caller

		@trigger 'stack:loading' if @current is 0
		@current++
		#		console.log 'TRIGGER LOADING FROM LOADING STACK', @current
		@

	ready: (caller) ->

#		console.log 'TRIGGERING READY', @current, caller

		return unless @current > 0
		@current--
		@trigger 'stack:ready' if @current is 0
		#		console.log 'TRIGGER READY FROM LOADING STACK', @current
		@

	reset: ->
		@current = 0
		@trigger 'stack:reset'
		@

	trigger: (event) ->

#		console.log "TRIGGERING EVENT", event

		@bindTo.trigger event


module.exports = LoadingStack

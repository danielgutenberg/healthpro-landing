module.exports = (phone) ->
	res = /^\s*1?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/
	res.test(phone)

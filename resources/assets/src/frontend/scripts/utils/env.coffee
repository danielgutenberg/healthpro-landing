module.exports = {
	env: ->
		# @TODO should be improved
		switch window.location.hostname
			when 'healthpro.com' then 'production'
			when 'therpedia.com' then 'demo'
			else 'dev'

	isProduction: -> @env() is 'production'
	isDemo: -> @env() is 'demo'
	isDev: -> @env() is 'dev'
}

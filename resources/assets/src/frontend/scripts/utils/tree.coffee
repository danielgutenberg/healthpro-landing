class Tree

	defaults:
		sortBy: 'name'
		prefix: '-'
		labelPath: 'name'

	data: []

	constructor: (@data, @options = {}) ->
		_.defaults @options, @defaults

	setData: (data) -> @data = data

	getData: -> @data

	getTree: -> @_buildTree @data

	getFlatTree: -> @_buildFlatTree @getTree()

	_buildTree: (data, parentId = null) ->
		tree = _.sortBy _.filter(data, {parent_id: parentId}), @options.sortBy

		# if search doesn't return parent service then return original results
		return data unless tree.length > 0 or parentId != null
		_.each tree, (group) =>
			group.children = @_buildTree data, group.id
		tree

	_buildFlatTree: (tree, results = [], level = 0) ->
		_.each tree, (item) =>
			results.push
				label: @getName item[@options.labelPath], level
				value: item.id
				level: level
				parent_id: item.parent_id
			if item.children and item.children.length
				results = @_buildFlatTree item.children, results, ++level
				level = level - 1
		results

	getName: (name, level = 0) ->
		prefix = if level then (Array(level+1).join(@options.prefix) + ' ') else ''
		"#{prefix}#{name}"



module.exports = Tree


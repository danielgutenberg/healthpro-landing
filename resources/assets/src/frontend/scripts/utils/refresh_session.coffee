getApiRoute = require 'hp.api'

class RefreshSession
	constructor: ->
		setInterval (->
			$.ajax
				url: getApiRoute('ajax-refresh-session')
				method: 'get'
		), 900000

module.exports = RefreshSession

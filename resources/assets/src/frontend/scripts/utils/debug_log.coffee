isDev = window.location.host.search('app:8000') isnt -1 || window.location.host.search('nerdlab.us') isnt -1
startTime = ( new Date() ).getTime()

module.exports = (msg, moduleName) ->
	if isDev
		t = '[' + ( ( (new Date() ).getTime() - startTime ) / 1000).toFixed(3) + '] '
		moduleName = if moduleName? then moduleName + ': ' else ''

		console.log( t + moduleName + msg )

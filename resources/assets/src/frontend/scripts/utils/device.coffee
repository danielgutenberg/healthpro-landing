isMobile = require 'isMobile'

module.exports =
	slug		: if isMobile.phone or isMobile.tablet then 'mobile' else 'desktop'
	name 		: if isMobile.phone or isMobile.tablet then 'Mobile' else 'Desktop'
	isMobile	: -> isMobile.phone or isMobile.tablet
	isDesktop	: -> !isMobile.phone and !isMobile.tablet

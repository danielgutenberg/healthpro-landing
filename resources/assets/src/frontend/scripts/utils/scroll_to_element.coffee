module.exports = ($el, offset = 80, $container = null) ->

	if $container is null
		$container = $('html, body')

	if $el
		$container.animate({
			scrollTop: $el.offset().top - offset
		}, 300)

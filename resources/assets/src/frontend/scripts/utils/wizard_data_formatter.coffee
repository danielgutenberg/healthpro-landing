URI = require 'URIjs/URI'
Moment = require 'moment'
datetimeParsers = require 'utils/datetime_parsers'
LocationFormatter = require 'utils/formatters/location'
ProfessionalFormatter = require 'utils/formatters/professional'

###
  Booking Data helper to format data before sending it forward
###
module.exports =

	###
	pure function returns formatted data from the API response data
	###
	formatFromResponse: (responseWithData, wizardModel) ->
		responseData = responseWithData.data

		data =
			title: responseData.title
			'current_step.number': do ->
				step = 1
				if _.isNumber(responseData.current_step)
					step = responseData.current_step
				step = step ? 1
				step
			'current_step.slug': wizardModel.detectCurrentStep(responseData)
			steps: if responseData.steps?.length then responseData.steps else wizardModel.get('steps')
			next_step: responseData.next_step
			previous_step: responseData.previous_step
			total_steps: responseData.total_steps
			profileId: responseData.profileId
			step_validation: responseData.step_validation ? null
			data: do =>
				if responseData.data? and responseData.data isnt ''
					JSON.parse(responseData.data)
				else
					wizardModel.get('data')

		data

	###
	pure function returns formatted data from the search string
	###
	formatFromSearchString: (dataFromSearchString) -> URI.parseQuery(dataFromSearchString)

	###
	pure function returns formatted data from the book appointment module model.
	###
	formatFromBookAppointment: (bookAppointmentData) ->

		date = datetimeParsers.parseToMoment(bookAppointmentData.date).format('YYYY-MM-DD')
		from = if bookAppointmentData.from then datetimeParsers.parseToMoment(bookAppointmentData.from).format() else date

		currentService = _.findWhere bookAppointmentData.services, {service_id: bookAppointmentData.current_service_id}
		currentSession = _.findWhere currentService.sessions, {session_id: bookAppointmentData.current_session_id}

		data =
			resetToFirstStep : bookAppointmentData.resetToFirstStep
			from             : from
			date             : date
			client_id        : bookAppointmentData.client_id || +window.GLOBALS?._PID
			provider_id      : bookAppointmentData.provider_id || window.GLOBALS?.PROVIDER_ID
			services         : bookAppointmentData.services
			certificate_id   : bookAppointmentData.certificate_id ? null
			selected_time    :
				service_id   : currentService.service_id
				service_name : currentService.name
				session_id   : currentSession.session_id
				duration     : currentSession.duration
				price        : currentSession.price
				total_price  : currentSession.price

		if bookAppointmentData.current_package_id
			currentPackage = _.findWhere currentSession.packages, {package_id: bookAppointmentData.current_package_id}
			data.selected_time.package = currentPackage
			data.selected_time.total_price = currentPackage.price

		if bookAppointmentData.availabilitiesSearchUrl
			data.availabilitiesSearchUrl = bookAppointmentData.availabilitiesSearchUrl

		if bookAppointmentData.availability_id
			availability = _.findWhere bookAppointmentData.scheduleCollection, availability_id: bookAppointmentData.availability_id

			data.availability_id = bookAppointmentData.availability_id
			data.selected_time.availability_id = bookAppointmentData.availability_id

			data.selected_time.location_id = availability?.location_id
			data.selected_time.location_name = availability?.location_name

		else if bookAppointmentData.location_id
			currentLocation = _.findWhere currentService.locations, id: bookAppointmentData.location_id

			data.selected_time.location_id = currentLocation?.id
			data.selected_time.location_name = currentLocation?.name

		if bookAppointmentData.appointment_id
			data.appointment_id = bookAppointmentData.appointment_id
			data.selected_time.appointment_id = bookAppointmentData.appointment_id

		if bookAppointmentData.from
			data.selected_time.from = from

		data

	###
		pure function returns formatted data from the book appointment module model to book recurring appointment.
	###
	formatRecurringDataFromBookAppointmnet: (bookAppointmentData) ->

		currentService = _.findWhere bookAppointmentData.services, {service_id: bookAppointmentData.current_service_id}

		unless currentService?
			currentService = _.first bookAppointmentData.services

		data =
			resetToFirstStep: bookAppointmentData.resetToFirstStep
			client_id: bookAppointmentData.client_id || +window.GLOBALS?._PID
			provider_id: bookAppointmentData.provider_id || window.GLOBALS?.PROVIDER_ID
			services: bookAppointmentData.services
			selected_time:
				service_id: currentService.service_id
				service_name: currentService.name

		if bookAppointmentData.location_id
			currentLocation = _.findWhere currentService.locations, id: bookAppointmentData.location_id
			data.selected_time.location_id = currentLocation?.id
			data.selected_time.location_name = currentLocation?.name

		if bookAppointmentData.appointment_id
			data.selected_time.appointment_id = bookAppointmentData.appointment_id

		data

	###
  		format booking data to for the booking info module
  		needs more refactoring. was taken from the book_appointment module
	###
	formatAdditionalData: (bookAppointmentData) ->
		updateData =
			'additional.appointment.service_name'      : null
			'additional.appointment.date'              : null
			'additional.appointment.time'              : null
			'additional.location.name'                 : null
			'additional.location.address'              : null
			'additional.location.id'                   : null
			'additional.registration_location.name'    : null
			'additional.registration_location.address' : null
			'additional.registration_location.id'      : null
			'additional.certificate_id'                : null
			'additional.package.id'                    : null
			'additional.package.price'                 : null
			'additional.package.number_of_visits'      : null
			'additional.package.number_of_months'      : null



		currentService = _.findWhere bookAppointmentData.services, {service_id: bookAppointmentData.current_service_id}
		return updateData unless currentService?

		currentSession = _.findWhere currentService.sessions, {session_id: bookAppointmentData.current_session_id}

		# find current location by availability
		if bookAppointmentData.availability_id
			currentLocation = _.find currentService.locations, (l) ->
				availability = _.find l.availabilities, (a) -> a.id is bookAppointmentData.availability_id
				availability?

		# highlight the location from the booking data if availability is not provider
		else if bookAppointmentData.location_id
			currentLocation = _.findWhere currentService.locations, id: bookAppointmentData.location_id

		# set location if its the only location for the service
		else if currentService.locations.length is 1
				currentLocation = _.first currentService.locations

		updateData['additional.appointment.service_name'] = currentService.name

		if currentLocation?
			updateData['additional.location.timezone'] = currentLocation.timezone
			updateData['additional.location.id'] = currentLocation.id
			updateData['additional.location.name'] = currentLocation.name
			updateData['additional.location.address'] = LocationFormatter.getLocationAddressLabel currentLocation

			# when we set the data on the first step from the current location we also update the registration_location fields
			updateData['additional.registration_location.id'] = updateData['additional.location.id']
			updateData['additional.registration_location.name'] = updateData['additional.location.name']
			updateData['additional.registration_location.address'] = updateData['additional.location.address']

		if bookAppointmentData.date?
			updateData['additional.appointment.date'] = datetimeParsers.parseToFormat(bookAppointmentData.date, 'YYYY-MM-DD')

		if bookAppointmentData.from?
			updateData['additional.appointment.time'] = datetimeParsers.parseTime(bookAppointmentData.from)

		if currentSession?
			updateData['additional.appointment.duration'] = currentSession.duration

			if bookAppointmentData.current_package_id
				currentPackage = _.findWhere currentSession.packages, {package_id: bookAppointmentData.current_package_id}
				if currentPackage?
					updateData['additional.package.id'] = currentPackage.id
					updateData['additional.package.price'] = currentPackage.price
					updateData['additional.package.number_of_visits'] = currentPackage.number_of_visits
					updateData['additional.package.number_of_months'] = currentPackage.number_of_months

		if bookAppointmentData.certificate_id?
			updateData['additional.certificate_id'] = bookAppointmentData.certificate_id

		updateData


	###
		format professional information for the wizard model
	###
	formatBookingProfessionalFromResponse: (responseWithData) ->
		responseData = responseWithData.data
		data =
			additional :
				professional :
					name       : ProfessionalFormatter.getProfessionalName responseData
					avatar     : ProfessionalFormatter.getProfessionalAvatar responseData
					public_url : ProfessionalFormatter.getProfessionalUrl responseData
		data

	###
		format appointment information for the wizard model
	###
	formatBookingAppointmentFromResponse: (responseWithData) ->

		responseData = responseWithData.data
		momentDate = Moment.parseZone responseData.appointment.date

		data =
			additional:
				location:
					id: responseData.location.id
					address: responseData.location.address.address
					name: responseData.location.name
					timezone: responseData.location.timezone
				appointment:
					duration: responseData.appointment.duration
					price: responseData.appointment.price
					date: momentDate
					time: momentDate.format 'hh:mm a'
				registration_location:
					id: responseData.registration_location.id
					name: responseData.registration_location.name
					address: LocationFormatter.getLocationAddressLabel responseData.registration_location
					timezone: responseData.registration_location.timezone

		# if the location ID is the same as registration location ID - simply grab the registration location
		if responseData.registration_location.id is responseData.location.id
			data.additional.location = data.additional.registration_location
		else
			data.additional.location =
				id: responseData.location.id
				name: responseData.location.name
				address: LocationFormatter.getLocationAddressLabel responseData.location, false, true
				timezone: responseData.location.timezone

		data

	###
		pure function returns formatted data from the book introductory session module model.
	###
	formatAdditionalDataFromIntroductorySession: (bookAppointmentData) ->

		updateData =
			'additional.appointment.service_name': bookAppointmentData.service.name
			'additional.appointment.duration': bookAppointmentData.session.duration
			'additional.appointment.date': null
			'additional.appointment.time': null

			'additional.location.name': null
			'additional.location.address': null
			'additional.location.id': null
			'additional.location.timezone': null

		currentLocation = _.find bookAppointmentData.locations, (l) ->
			availability = _.find l.availabilities, (a) -> a.id is bookAppointmentData.availability_id
			availability?

		if currentLocation?
			updateData['additional.location.timezone'] = currentLocation.timezone
			updateData['additional.location.id'] = currentLocation.id
			updateData['additional.location.name'] = currentLocation.name
			updateData['additional.location.address'] = LocationFormatter.getLocationAddressLabel currentLocation

			# when we set the data on the first step from the current location we also update the registration_location fields
			updateData['additional.registration_location.id'] = updateData['additional.location.id']
			updateData['additional.registration_location.name'] = updateData['additional.location.name']
			updateData['additional.registration_location.address'] = updateData['additional.location.address']

		if bookAppointmentData.date?
			updateData['additional.appointment.date'] = datetimeParsers.parseToFormat(bookAppointmentData.date, 'YYYY-MM-DD')

		if bookAppointmentData.from?
			updateData['additional.appointment.time'] = datetimeParsers.parseTime(bookAppointmentData.from)

		updateData

	###
		pure function returns formatted data from the book appointment module model.
	###
	formatFromBookIntroductorySession : (bookAppointmentData) ->
		date = datetimeParsers.parseToMoment(bookAppointmentData.date).format('YYYY-MM-DD')
		from = if bookAppointmentData.from then datetimeParsers.parseToMoment(bookAppointmentData.from).format() else date

		data =
			introductory_confirmed : true
			resetToFirstStep       : bookAppointmentData.resetToFirstStep
			from                   : from
			date                   : date
			client_id              : bookAppointmentData.client_id || +window.GLOBALS?._PID
			provider_id            : bookAppointmentData.provider_id || window.GLOBALS?.PROVIDER_ID
			services               : bookAppointmentData.services
			selected_time          :
				service_id   : bookAppointmentData.service.service_id
				service_name : bookAppointmentData.service.name
				session_id   : bookAppointmentData.session.session_id
				duration     : bookAppointmentData.session.duration
				price        : bookAppointmentData.session.price
				total_price  : bookAppointmentData.session.price

		if bookAppointmentData.availability_id
			availability = _.findWhere bookAppointmentData.scheduleCollection, {availability_id : bookAppointmentData.availability_id}

			data.availability_id = bookAppointmentData.availability_id
			data.selected_time.availability_id = bookAppointmentData.availability_id

			data.selected_time.location_id = availability?.location_id
			data.selected_time.location_name = availability?.location_name

		if bookAppointmentData.appointment_id
			data.appointment_id = bookAppointmentData.appointment_id
			data.selected_time.appointment_id = bookAppointmentData.appointment_id

		if bookAppointmentData.from
			data.selected_time.from = from

		data

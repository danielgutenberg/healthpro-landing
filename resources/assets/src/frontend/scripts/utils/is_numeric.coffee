module.exports = (value) ->
	if typeof value == 'number'
		return true
	str = (value or '').toString()
	if !str
		return false
	!isNaN(str)

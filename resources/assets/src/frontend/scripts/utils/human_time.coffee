module.exports =
	minutes: (minutes, options = {}) ->

		_.defaults options,
			hourLabel: 'hour'
			hoursLabel: 'hours'
			minuteLabel: 'min'
			minutesLabel: 'mins'

		m = minutes % 60
		h = (minutes - m) / 60
		time = []
		time.push "#{h} " + (if h is 1 then options.hourLabel else options.hoursLabel) if h
		time.push "#{m} " + (if h is 1 then options.minuteLabel else options.minutesLabel) if m
		time.join ' '





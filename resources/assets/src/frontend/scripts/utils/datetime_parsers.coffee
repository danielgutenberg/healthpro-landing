Moment = require 'moment'

DateTimeParser = {}

DateTimeParser.parseToMoment = (date = null) ->
	if null is date
		return Moment()

	if date.indexOf('T') > -1
		return Moment.parseZone(date)

	return Moment(date)

###
  simple method that parses the date from the date, datetime, moment objects and returns it in the YYYY-MM-DD
###
DateTimeParser.parseToFormat = (date = null, format = 'YYYY-MM-DD') ->
	DateTimeParser.parseToMoment(date).format format


DateTimeParser.parseTime = (date, format = 'hh:mm a') ->
	return null unless date
	return null unless date.indexOf('T') > -1
	DateTimeParser.parseToMoment(date).format format

module.exports = DateTimeParser

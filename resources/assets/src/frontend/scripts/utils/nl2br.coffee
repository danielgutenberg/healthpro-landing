module.exports = (str, xhtml) ->
	breakTag = if xhtml then '<br />' else '<br>'
	(str + '').replace /([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, "$1#{breakTag}$2"

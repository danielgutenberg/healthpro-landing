URI = require 'URIjs/URI'
getApiRoute = require 'hp.api'
Qs = require 'qs'

module.exports =
	getCurrentUrl: -> new URI().href()

	prepareUrl: (url) ->
		result = @parseUrl(url)
		if result
			return @buildUrl(result.type, result.query)
		else
			return false

	parseUrl: (url) ->
		query = @parseQuery(url)
		queryJSON = @getQueryJson(query) # get query's json
		type = queryJSON.type ? 'single'
		delete queryJSON.type if queryJSON.type?
		query = Qs.stringify( queryJSON )

		return {
			type: type
			query: query
		}

	parseQuery: (url) ->
		query = URI(url).search()
		query = query.slice(1) if query.charAt(0) is '?' # remove question

	buildQuery: (queryJSON) ->
		return Qs.stringify( queryJSON )

	getQueryJson: (query) ->
		return Qs.parse(query) # get query's json


	buildUrl: (type, query) ->
		switch type
			when 'single' then getApiRoute('providers-search', {query: query})
			when 'class' then getApiRoute('classes-search', {query: query})

	# check if url query isn't empty
	checkUrl: (url) ->
		return URI(url).search().length

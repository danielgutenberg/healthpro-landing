module.exports = ($el, display = null, className = 'm-hide') ->
	if display is 1 then display = true
	switch display
		when true
			$el.removeClass className
		when false
			$el.addClass className
		else
			$el.toggleClass className
	$el

env = require 'utils/env'

module.exports = ->
	unless env.isProduction()
		console.log 'capterra track'
		return

	capterra_vkey = '35f6f90c46af9b0669211c9bd0740641'
	capterra_vid = '2107470'
	capterra_prefix = if 'https:' == document.location.protocol then 'https://ct.capterra.com' else 'http://ct.capterra.com'
	do ->
		ct = document.createElement('script')
		ct.type = 'text/javascript'
		ct.async = true
		ct.src = capterra_prefix + '/capterra_tracker.js?vid=' + capterra_vid + '&vkey=' + capterra_vkey
		s = document.getElementsByTagName('script')[0]
		s.parentNode.insertBefore ct, s
		return
	return

module.exports =
	convertCollectionToArray: (collection) ->
		if _.isPlainObject(collection)
			return _.values(collection)
		else
			return collection

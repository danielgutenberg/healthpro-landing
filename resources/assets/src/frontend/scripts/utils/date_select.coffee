module.exports =
	getDaysOptions: ->
		return [
			{
				value: '2'
				label: 'Monday'
			}
			{
				value: '3'
				label: 'Tuesday'
			}
			{
				value: '4'
				label: 'Wednesday'
			}
			{
				value: '5'
				label: 'Thursday'
			}
			{
				value: '6'
				label: 'Friday'
			}
			{
				value: '7'
				label: 'Saturday'
			}
			{
				value: '1'
				label: 'Sunday'
			}
		]

	getHoursOptions: ->
		hours = []
		val = null

		for i in [1..12]
			if i < 10 then val = '0'+i else val = i

			hours.push({
				value: val.toString()
				})

		return hours

	getMinutesOptions: ->
		minutes = []
		val = null

		for i in [0..55] by 5
			switch i
				when 0 then val = '00'
				when 5 then val = '05'
				else val = i.toString()

			minutes.push({
				value: val
				})

		return minutes

	getFormatOptions: ->
		return [
			{
				label: 'am'
				value: 'am'
			}
			{
				label: 'pm'
				value: 'pm'
			}
		]

	getMonthsOptions: ->
		return [
			{
				value: "01"
				label: "January"
			}
			{
				value: "02"
				label: "February"
			}
			{
				value: "03"
				label: "March"
			}
			{
				value: "04"
				label: "April"
			}
			{
				value: "05"
				label: "May"
			}
			{
				value: "06"
				label: "June"
			}
			{
				value: "07"
				label: "July"
			}
			{
				value: "08"
				label: "August"
			}
			{
				value: "09"
				label: "September"
			}
			{
				value: "10"
				label: "October"
			}
			{
				value: "11"
				label: "November"
			}
			{
				value: "12"
				label: "December"
			}
		]

	getYearsOptions: (start, add) ->
		start = +start
		finish = +start + +add
		return [start..finish]

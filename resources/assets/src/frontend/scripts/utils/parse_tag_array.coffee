module.exports = (array, field = 'id') -> array.map (item) -> item[field]

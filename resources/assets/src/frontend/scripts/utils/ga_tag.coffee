tagEvents = require 'config/tag_events'

module.exports = (event, additionalParams = {}) ->
	# return if "ga" object wasn't found
	return unless ga?

	if _.isString event
		gaEvent = _.get tagEvents, "events.#{event}"
		_.assign gaEvent, additionalParams


	else if _.isObject event
		gaEvent = event
	else
		console.error 'please specify event for GA Tag Manager'
		return

	# fill default values
	_.defaults gaEvent, tagEvents.defaults

	ga 'send', gaEvent

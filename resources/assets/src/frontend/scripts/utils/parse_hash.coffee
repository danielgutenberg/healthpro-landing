module.exports = (removeHash = false) ->
	hash = _.object _.compact _.map window.location.hash.replace(/^#/, '').split('&'), (item) -> if item then item.split ':'
	if removeHash
		window.location.hash = ''

	hash

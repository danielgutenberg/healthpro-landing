HP_App = require 'app/app'

class HP_Front
	constructor: ->
		@app = new HP_App 'front'

		# init popups
		@initPagePricing() if $('[data-pricing]').length
		@initMembershipSignup() if $('[data-popup="popup_membership"]').length

		# init pages
		@initHomepage() if $('.home-professional').length
		@initHomepageClient() if $('.home-client').length
		@initPageFeatures() if $('.page-features').length
		@initSearch() if $('.search').length
		@initProfile() if $('[data-profile]').length
		@initClientsProfile() if $('[data-clients-profile]').length
		@initOfferCoupon() if $('.home-offer').length
		@initHowItWorks() if $('.how_it_works').length

		# other blocks
		@initArticles() if $('.article').length
		@initArticlesDrafts() if $('.js-blog_drafts').length

		# @initHelp() if $('.help').length
		#
		# @initPartnerContact() if $('.partner_contact').length
		#
		# @initAffiliateProgram() if $('.affiliate_program').length
		#
		# @initDemoRequest() if $('.demo_request').length

		@initCsForms() if $('[data-cs-form]').length

		@initFeatures() if $('.home-grow').length

		@initTestimonials() if $('.testimonials').length
		@initNewestPros() if $('.js-newest_pros').length

		@initScrollTopButton() if $('.scroll_top').length

		@initCarousel() if $('.js-carousel').length

		@initSlider() if $('.js-slider').length

		@initInlineForm() if $('.inline_auth_form').length

		@initReviews() if $('.js-rating').length
		@initReviews() if $('.reviews').length

		@initIeWarning()

		@initSticky() if $('[data-sticky-block]').length

		@initBlog() if $('.article_edit--body').length

		# refactor later
		require 'app/modules/booking_helper'
		require 'app/modules/review_helpful'
		require 'app/modules/open_wizard'

	initMembershipSignup: ->
		require.ensure [], ->
			# membership sign up popup
			require 'app/modules/membership_signup'

	initHomepage: ->
		require.ensure [], ->
			# load professional homepage scripts
			require 'app/pages/home_professional'

	initHomepageClient: ->
		require.ensure [], ->
			# load client homepage scripts
			require 'app/pages/home_client'

	initPageFeatures: ->
		require.ensure [], ->
			# load client homepage scripts
			require 'app/pages/features'

	initPagePricing: ->
		require.ensure [], ->
			require 'app/pages/pricing'

	initHowItWorks: ->
		require.ensure [], ->
			require 'app/pages/how_it_works'

	initSearch: ->
		require.ensure [], ->
			# load search scripts
			require 'app/modules/search'

	initArticles: ->
		require.ensure [], ->
			# load article
			require 'app/blocks/article'

		if $('.article_edit').length
			require.ensure [], ->
				# load article
				require 'app/blocks/article/edit'

	initArticlesDrafts: ->
		require.ensure [], ->
			# load article drafts
			require 'app/blocks/article/drafts'

	# initHelp: ->
	# 	require.ensure [], ->
	# 		require 'app/blocks/help'
	# 		require 'app/blocks/hubspot/help'
	#
	# initPartnerContact: ->
	# 	require.ensure [], ->
	# 		require 'app/blocks/hubspot/partner_contact'
	#
	# initAffiliateProgram: ->
	# 	require.ensure [], ->
	# 		require 'app/blocks/hubspot/affiliate_program'
	#
	# initDemoRequest: ->
	# 	require.ensure [], ->
	# 		require 'app/blocks/hubspot/demo_request'

	initCsForms: ->
		require.ensure [], ->
			require 'app/modules/cs_form'

	initFeatures: ->
		require.ensure [], ->
			require 'app/blocks/features'

	initTestimonials: ->
		require.ensure [], ->
			require 'app/blocks/testimonials'

	initNewestPros: ->
		require.ensure [], ->
			require 'app/blocks/newest_pros'

	initScrollTopButton: ->
		require.ensure [], ->
			require 'app/blocks/scroll_top'

	initSlider: ->
		require.ensure [], ->
			require 'app/blocks/slider'

	initInlineForm: ->
		require.ensure [], ->
			require 'app/blocks/inline_auth_form'

	initCarousel: ->
		require.ensure [], ->
			require 'app/blocks/carousel'

	initBlog: ->
		require.ensure [], ->
			require 'app/blocks/blog'

	initReviews: ->
		require.ensure [], ->
			require 'app/modules/review_professional'

	initProfile: ->
		require.ensure [], ->
			require 'app/pages/profile'

	initClientsProfile: ->
		require.ensure [], ->
			require 'app/pages/clients_profile'

	initOfferCoupon: ->
		require.ensure [], ->
			require 'app/pages/offer_coupon'

	initIeWarning: ->
		require.ensure [], ->
			require 'app/blocks/ie_warning'

	initSticky: ->
		require.ensure [], ->
			require 'app/blocks/sticky'

new HP_Front()

vexDialog = require 'vexDialog'
require 'imagesloaded'
getApiRoute = require 'hp.api'

class Popup

	constructor: (@$block, @popupsManager) ->
		# define variables
		@opened = false
		@centered = false
		@hasVideo = false
		@name = @$block.attr('data-popup')
		@options = if typeof @$block.data('options') isnt 'undefined' then @$block.data('options') else null

		# dom elements
		@$block.container = $('[data-popup-container]', @$block)
		@$block.closer = $('[data-popup-close]', @$block)

		@$dom = $(document)
		@$dom.html = $('html')
		@$dom.body = $('body')
		@$dom.header = $('.header')
		@$dom.header.paddingRight = parseInt(@$dom.header.css('padding-right'))
		@$window = $(window)

		@scrollWidth = @detectScrollWidth()

		@isResponsive = @$dom.body.hasClass 'm-mobile_ready'

		@closeOnEsc = true

		# bind actions
		@bind()


		if typeof @options isnt 'null' and @options isnt null
			# check if it's video Popup
			if typeof @options.type isnt 'undefined' and @options.type is 'video'
				@initVideo()
			# open popup if it needs to be opened
			if @options.open
				@openPopup()
			if @options.closeOnEsc isnt 'undefined' and !@options.closeOnEsc
				@closeOnEsc = false

	bind: ->
		@$block.closer.on 'click', (el) =>
			@closePopup()

		@$window.on 'resize', =>
			if @opened
				@centerPopup()


	isOpened: -> @opened

	openPopup: ->
		@$block.show()
		@removeJumping()
		@detectBigPopup()
		@centerAfterLoad()
		@opened = true

		# if @centered is false
		#     @centerPopup()

		if @hasVideo
			@startVideo()

		@$block.trigger 'popup:opened'
		# close the slideout if opened
		$(document).trigger 'slideout:close'

	closePopup: ->
		@$block.hide()
		@opened = false
		@$block.removeClass('m-big_popup')

		if @hasVideo
			@stopVideo()

		@unRemoveJumping()
		@$block.trigger 'popup:closed'

	centerPopup: ->
		@resizeVideo() if @hasVideo

		if @isResponsive and @$window.width() < 768
			@$block.container.css
				marginTop: 0
				marginLeft: 0
				top: 0
				left: 0
			return

		@$block.container.css
			marginTop: Math.round(-@$block.container.height()/2)
			marginLeft: Math.round(-@$block.container.width()/2)

		if @$block.container.width() > @$window.width()
			@$block.container.css
				marginLeft: 0
				left: '1%'
		else
			@$block.container.css
				left: '50%'

		if @$block.container.height() > @$window.height()
			@$block.container.css
				marginTop: 0
				top: 0
		else
			@$block.container.css
				top: '50%'

		@centered = true

	# compare Window and popup heights
	detectBigPopup: ->
		if @$block.container.outerHeight() > @$window.height()
			@$block.addClass('m-big_popup')
			@centerPopup()
		else
			@$block.removeClass('m-big_popup')
			@centerPopup()

	centerAfterLoad: ->
		# load all images and center popup
		if @$block.find('img').length
			@$block.imagesLoaded => @centerPopup()

	# remove window jumping when popup is opening
	removeJumping: ->
		@scrollTopOnOpening = @$dom.scrollTop()

		@$dom.html.addClass 'm-popup_opened'

		if @$dom.height() > @$window.height()
			@$dom.html.css
				paddingRight: @scrollWidth

			@$dom.header.css
				paddingRight: @$dom.header.paddingRight + @scrollWidth

	# return window jumping behavior and remove html class
	unRemoveJumping: ->
		@$dom.html.removeClass 'm-popup_opened'
		@$dom.html.css
			paddingRight: 0

		@$dom.header.css
			paddingRight: @$dom.header.paddingRight

		$('html, body').scrollTop(@scrollTopOnOpening)

	# detect scrollbar width
	detectScrollWidth: ->
		outer = $('<div/>').css({
			width: '100px'
			overflow: 'scroll'
		})

		@$dom.body.append( outer )
		inner = $('<div/>')
		outer.append( inner )
		scrollWidth = outer.width() - inner.width()
		outer.remove()

		return scrollWidth

	# generate html for video popup
	initVideo: ->
		@hasVideo = true

		if @options.videoUrl and !@options.keepSizes
			if @options.videoSizes.width || @options.videoSizes.height
				@options.videoSizes.width = 900
				@options.videoSizes.height = 506

		@videoRatio = Math.round(parseInt(@options.videoSizes.height) / parseInt(@options.videoSizes.width) * 10000) / 100

		# wrapper for responsive video
		@videoWrapper = $('<div class="popup--video_wrap" style="padding-top:' + @videoRatio + '%;"></div>')
			.appendTo @$block.container

		@videoPlayerEl = $('<iframe id="'+@options.name+'" width="'+@options.videoSizes.width+'" height="'+@options.videoSizes.height+'" frameborder="0" allowfullscreen></iframe>')
			.appendTo @videoWrapper
		@videoPlayerEl.attr('src', @options.videoUrl)

	resizeVideo: ->
		return unless @hasVideo
		windowWidth = @$window.width() - 30 # add some paddings to the side
		videoWidth = if @options.videoSizes.width > windowWidth then windowWidth else @options.videoSizes.width
		@videoWrapper.width videoWidth

	startVideo: ->
		# @videoPlayerEl.attr('src', @videoPlayerEl.attr('src')+'?autoplay=1')

	# dirty hack for stop video
	stopVideo: ->
		@videoPlayerEl.attr('src', '') # remove src data
		@videoPlayerEl.attr('src', @options.videoUrl) # set again

	setCloseOnEsc: (@closeOnEsc) ->

	canCloseOnEsc: ->
		return false unless @opened
		return false unless @closeOnEsc
		return false if _.indexOf(['popup_wizard', 'wizard_booking_popup', 'popup_pre_wizard'], @name) > -1
		return true

module.exports = Popup

class ExternalLinks
	constructor: ->
		$("a[rel~=external]").each (index, item) -> $(item).attr "target", "_blank"

new ExternalLinks()

module.exports = ExternalLinks

class ErrorHandler
	constructor: ->
		# error handler
		$(document).ajaxError (event, request, settings) ->
			if request.status and request.status.toString() isnt '422' and request.status.toString() isnt '401' and request.responseJSON?.errors?.error?
				if request.status.toString() is '419'
					vexDialog.alert
						message: 'Your session has timed out. Please click OK to refresh the page.'
						callback: (value) =>
							location.reload()
					return

				# dont'show cards errors
				# dont'show if we have show_alerts: false in settings
				# TODO: improve this
				unless settings.url is '/ajax/v1/profiles/me/credit_cards' or settings.show_alerts is false
					window.Alerts.addNew('error', 'Error: ' + request.responseJSON?.errors.error.messages)


new ErrorHandler()

module.exports = ErrorHandler

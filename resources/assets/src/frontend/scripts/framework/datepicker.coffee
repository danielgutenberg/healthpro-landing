require 'jquery-ui/datepicker'

class Datepicker
	constructor: (@$node, @options) ->
		@defaultOptions =
			dateFormat: 'm/d/y'
			showOtherMonths: true

		@$node.$input = $('[data-datepicker-input]', @$node)
		@$node.$container = $('[data-datepicker-container]', @$node)

		@setOptions()
		@initDatepicker()

	setOptions: ->
		@options = @options ? null
		@options = _.extend @defaultOptions, @options

	initDatepicker: ->
		@$node.$input.datepicker @options

	updateOption: (option, val) ->
		@$node.$input.datepicker 'option', option, val

# init datepickers on the page
_.each $('[data-datepicker]'), (item) ->
	$item = $(item)
	autoinit = if typeof $item.data('datepicker').autoinit isnt 'undefined' then $item.data('datepicker').autoinit else true
	if autoinit
		new Datepicker $item

module.exports = Datepicker

Moment = require 'moment'
dateSelect = require 'utils/date_select'

class Timepicker

	###*
	 * [constructor description]
	 * @param {object} @$input
	 * @param {object} @options
	###
	constructor: (@$input, @settings) ->
		@initVariables()
		@setSettings()
		@hideInput()
		@createNodes()
		@addListeners()
		@setInitValue()

		return @

	initVariables: ->
		@defaultSettings =
			className: ""
			timeFormat: "HH:mm"
			buttonLabel: "Set"
			offset: 10
			relativeTo: null
			allowEmpty: false

		@isReadonly = @$input.is('[readonly]')

		@isOpened = false
		@nodes = {}
		@values =
			hours: null
			minutes: null
			period: null

	hideInput: ->
		if @settings.$target?.length
			@$input[0].style.display = 'none'
		else
			@$input.parent()[0].style.display = 'none'

	###*
	 * Parse initial input value
	 * @param  {string} value For success parsing use next format "01:15 am"
	 * @return {object}
	###
	parseInitValue: (value) ->

		# we parse the time with use of MomentJS
		if value.length
			dateTimeObj = Moment(value, @settings.timeFormat)

			# rounding time to closest 5 minutes interval
			round_interval = 5
			remainder = dateTimeObj.minutes() % round_interval
			dateTimeObj.subtract(remainder, 'minutes').add((if remainder > round_interval / 2 then round_interval else 0), 'minutes')

			hour = dateTimeObj.format('hh')
			minutes = dateTimeObj.format('mm')
			period = dateTimeObj.format('a')

		if @settings.allowEmpty
			timePlaceholder = '--'
			periodPlaceholder = '--'
		else
			timePlaceholder = '00'
			periodPlaceholder = 'am'

		return {
			hours: hour ? timePlaceholder
			minutes: minutes ? timePlaceholder
			period: period ? periodPlaceholder
		}

	addListeners: ->
		@nodes.$inputLabel.on 'click', () =>
			unless @isReadonly
				@toggleDropdown()
				@removeError()

		@nodes.$hours.on 'change', () => @updateValues()
		@nodes.$minutes.on 'change', () => @updateValues()
		@nodes.$period.on 'change', () => @updateValues()
		@nodes.$component.on 'init_value:ready', () => @updateValues()
		@nodes.$submit.on 'click', () => @closeDropdown()
		@$input.on 'error', () => @showError()


		$(window).on 'click', (e) =>
			$target = $(e.target)
			if @isOpened and !@checkClickOutsideDropdown( $target ) then @closeDropdown()

		return @

	checkClickOutsideDropdown: ($target) ->
		return !!($target.closest( @nodes.$dropdown ).length || $target.closest( @nodes.$input ).length)


	setSettings: ->
		@settings = _.merge(@defaultSettings, @settings || {})

		return @

	toggleDropdown: ->
		if @isOpened
			@closeDropdown()
		else
			@openDropdown()

		return @

	openDropdown: ->
		@nodes.$component.addClass 'm-opened'
		@isOpened = true

		@positionDropdown()
		$(window).on 'resize.hp_dropdown.' + @$input.attr('name'), => @positionDropdown()

		return @

	closeDropdown: ->
		@nodes.$component.removeClass 'm-opened'
		@isOpened = false

		@resetDropdownPosition()
		$(window).off 'resize.hp_dropdown.' + @$input.attr('name')

		return @

	positionDropdown: ->
		left = 0

		dropdownWidth = @nodes.$dropdown.outerWidth()
		componentWidth = @nodes.$component.outerWidth()

		# position element relative to the window
		if @settings.relativeTo is null
			regionWidth = $(window).width()
			componentOffset = @nodes.$component.offset().left
			# on mobile view position() returning not the actual value
			accurateComponentOffset = @nodes.$component[0].getBoundingClientRect().left
		# make sure the dropdown fits inside the relative element
		else
			regionWidth = @settings.relativeTo.width()
			componentOffset = @nodes.$component.position().left - @settings.relativeTo.position().left
			# on mobile view position() returning not the actual value
			accurateComponentOffset = @nodes.$component[0].getBoundingClientRect().left - @settings.relativeTo.position().left

		scrollLeft = $(window).scrollLeft()

		# calculate distance to the right edge of the relative element
		distanceToRightEdge = parseInt(regionWidth - componentOffset - dropdownWidth - @settings.offset + scrollLeft)

		if distanceToRightEdge < 0
			left = distanceToRightEdge
			if dropdownWidth + distanceToRightEdge < componentWidth
				left = componentWidth - dropdownWidth

		if dropdownWidth > window.innerWidth
			# use appropriate offset value depeding on view options (default or scaled)
			if window.innerWidth is $(window).width()
				distanceToLeftEdge = scrollLeft - componentOffset
			else
				distanceToLeftEdge = scrollLeft - accurateComponentOffset

			left = distanceToLeftEdge if distanceToLeftEdge < 0

		@nodes.$dropdown.css 'left', left


	resetDropdownPosition: ->
		@nodes.$dropdown.css 'left', 0

	setInitValue: ->
		initValues = @parseInitValue( @$input.val() )

		if initValues.hours.search('--') is -1
			@nodes.$hours.val( initValues.hours )
			@nodes.$minutes.val( initValues.minutes )
			@nodes.$period.val( initValues.period )

		@nodes.$component.trigger('init_value:ready')

		return @

	updateValues: ->
		@setValues()
		@updateInputValue()
		@updateFakeInputValue()

	setValues: ->
		@values.hours = @nodes.$hours.val()
		@values.minutes = @nodes.$minutes.val()
		@values.period = @nodes.$period.val()

	updateInputValue: ->
		hours = @convertTo24(@values.hours)
		if hours?.length
			@$input.val( hours+':'+@values.minutes)
		else
			if @settings.allowEmpty then @$input.val('--:--') else @$input.val('00:00')

		@$input.trigger 'change'

		return @

	convertTo24: (hours) ->
		if @values.period is 'am' and +hours is 12
			hours = 0

		if @values.period is 'pm' and +hours < 12
			hours = +hours + 12

		return hours?.toString()

	updateFakeInputValue: ->
		if @values.hours?.length
			@nodes.$input.val( @values.hours+':'+@values.minutes+' '+@values.period )
		else
			if @settings.allowEmpty then @nodes.$input.val('--:--') else @nodes.$input.val('00:00')

		return @

	getValues: ->
		return {
			hours: @nodes.$hours.val()
			minutes: @nodes.$minutes.val()
			period: @nodes.$period.val()
		}

	createNodes: ->
		@createComponent()
		@createInput()
		@createDropdown()
		@createHoursSelect()
		@createMinutesSelect()
		@createPeriodSelect()
		@createSubmit()

		return @

	createComponent: ->
		@nodes.$component = $('<div>', {class: 'timepicker'})

		if @settings.$target?.length
			@nodes.$component.appendTo( @settings.$target )
		else
			@nodes.$component.insertAfter( @$input.parent() )

		return @

	createInput: ->
		@nodes.$inputLabel = $('<label class="field m-icon m-o_clock"></label>').appendTo( @nodes.$component )
		@nodes.$input = $('<input type="text" name="'+@settings.inputName+'" readonly>').appendTo( @nodes.$inputLabel )
		@nodes.$error = $('<span class="field--error"></span>').appendTo( @nodes.$inputLabel )

		return @

	createDropdown: ->
		id = (new Date()).getTime()
		@nodes.$dropdown =
			$('<div class="timepicker_dropdown" id="timepicker_'+id+'"></div>')
				.appendTo( @nodes.$component )
		@nodes.$arrow = $('<div class="timepicker_dropdown_arrow"></div>')
			.appendTo @nodes.$component

		return @

	createHoursSelect: ->
		if @settings.allowEmpty
			selectNode = $('<select name="hours"><option value="--">--</option></select>')
		else
			selectNode = $('<select name="hours"></select>')

		@nodes.$hours =
			selectNode
				.appendTo( @nodes.$dropdown )
				.wrap('<div class="select"></div>')

		@createSelectOptions( @nodes.$hours, @generateHoursOptions() )

		return @

	createMinutesSelect: ->
		if @settings.allowEmpty
			selectNode = $('<select name="minutes"><option value="--">--</option></select>')
		else
			selectNode = $('<select name="minutes"></select>')

		@nodes.$minutes =
			selectNode
				.appendTo( @nodes.$dropdown )
				.wrap('<div class="select"></div>')

		@createSelectOptions( @nodes.$minutes, @generateMinutesOptions() )

		return @

	createPeriodSelect: ->
		@nodes.$period =
			$('<select name="period"></select>')
				.appendTo( @nodes.$dropdown )
				.wrap('<div class="select"></div>')

		@createSelectOptions( @nodes.$period, @generatePeriodsOptions() )

		return @

	createSubmit: ->
		@nodes.$submit = $('<button class="btn m-green">' + @settings.buttonLabel + '</button>')
			.appendTo( @nodes.$dropdown )

	###*
	 * append to Select options
	 * @param  {jquery object} $target
	 * @param  {array} options
	###
	createSelectOptions: ( $target, options ) ->
		options.forEach (option) =>
			label = option.label || option.value
			value = option.value
			$target.append( '<option value="'+value+'">'+label+'</option>' )

		return @

	generateHoursOptions: ->
		return dateSelect.getHoursOptions()

	generateMinutesOptions: ->
		return dateSelect.getMinutesOptions()

	generatePeriodsOptions: ->
		return dateSelect.getFormatOptions()

	showError: ->
		errorText = @$input.next('.field--error').text()
		@nodes.$inputLabel.addClass('m-error')
		@nodes.$error.text( errorText )

	removeError: ->
		if @nodes.$inputLabel.hasClass('m-error')
			@nodes.$inputLabel.removeClass('m-error')
			@nodes.$error.text('')

module.exports = Timepicker

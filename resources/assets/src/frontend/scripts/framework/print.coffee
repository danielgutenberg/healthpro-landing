Datepicker = require 'framework/datepicker'
Moment = require 'moment'

class Print
	constructor: (@$block, @type, @view) ->
		@popupName = @$block.data 'popup'
		@printIframe = null
		@cacheDom()
		@initDatepicker()
		@setPdfQuery()
		@bind()

	cacheDom: ->
		@$block.$datepickerGroup = $('[data-datepicker-group]', @$block)
		@$block.$datepicker = $('[data-print-datepicker]', @$block)
		@$block.$datepicker.$input = $('[data-datepicker-input]', @$block.$datepicker)
		@$block.$datepickerStart = $('[data-print-datepicker="start"]', @$block)
		@$block.$datepickerStart.$input = $('[name="print_day_from"]', @$block.$datepickerStart)
		@$block.$datepickerEnd = $('[data-print-datepicker="end"]', @$block)
		@$block.$datepickerEnd.$input = $('[name="print_day_to"]', @$block.$datepickerEnd)
		@$block.$dateToggler = $('[name="date"]', @$block)
		@$block.$orientationToggler = $('[name="orientation"]', @$block)
		@$block.$downloadPdf = $('[data-download-pdf]', @$block)
		@$block.$printPdf = $('[data-print-pdf]', @$block)
		@$block.$loading = $('.loading_overlay', @$block)

	bind: ->
		@$block.$dateToggler.on 'change', @toggleDatepicker
		@$block.$downloadPdf.on 'click', @openPdf
		@$block.$printPdf.on 'click', @printPdf
		@$block.$datepickerStart.$input.on 'change', @adjustEndBounds
		@$block.$datepickerEnd.$input.on 'change', @adjustStartBounds

	removePopup: ->
		if @popupView
			@popupView.remove()
			@popupView = null
			window.popupsManager.popups = []

	openPopup: ->
		window.popupsManager.addPopup @$block
		window.popupsManager.openPopup @popupName

	initDatepicker: ->
		maxDate = if @type is 'captured' then 0 else null
		minDate = if @type is 'authorized' then 0 else null
		@datepickerStart = new Datepicker @$block.$datepickerStart,
			dateFormat: "DD, MM dd, yy"
			selectOtherMonths: true
			maxDate: maxDate
			minDate: minDate

		if @$block.$datepickerEnd.length
			@datepickerEnd = new Datepicker @$block.$datepickerEnd,
				dateFormat: "DD, MM dd, yy"
				selectOtherMonths: true
				maxDate: maxDate
				minDate: minDate

		unless @datepickerEnd
			@$block.$datepickerStart.$input.datepicker('setDate', Moment().add(2, 'days').toDate())
		else
			@$block.$datepickerStart.$input.datepicker('setDate', Moment().startOf('month').toDate())

			if @type is 'captured'
				@$block.$datepickerEnd.$input.datepicker('setDate', Moment().endOf('day').toDate())
			else
				@$block.$datepickerEnd.$input.datepicker('setDate', Moment().endOf('month').toDate())

	toggleDatepicker: (e) =>
		if $(e.target).val() is 'other'
			@$block.$datepickerGroup.removeClass 'm-hide'
		else
			@$block.$datepickerGroup.addClass 'm-hide'

	openPdf: (e, print=false) =>
		query = @setPdfQuery()
		query += '&print=1' if print
		if @view is 'schedule'
			url = '/dashboard/appointments/report?' + query
		else
			url = '/dashboard/my-transactions/report?' + query

		@$block.$downloadPdf.attr 'href', url

		unless e # go to page even if no click event
			window.open url, '_blank'

		window.popupsManager.closePopup @popupName

	setPdfQuery: ->
		begin = @getSelectedStart()
		end = @getSelectedEnd()

		if @view is 'schedule'
			query = 'status=' + encodeURIComponent 'confirmed,pending'
			query += '&date=' + encodeURIComponent begin.format('YYYY-MM-DD')
			query += '&orientation=' + @$block.$orientationToggler.filter(':checked').val()
		else
			unless end? then end = begin
			endFormatted = encodeURIComponent(end.endOf('day').format('YYYY-MM-DD H:m:s'))

			query = 'begin=' + encodeURIComponent(begin.startOf('day').format('YYYY-MM-DD H:m:s'))
			query += '&end=' + endFormatted
			query += '&type=' + @type
			query += '&orientation=' + @$block.$orientationToggler.filter(':checked').val()
			query += '&view=' + @view if @view

		query

	initIframe: =>
		unless @printIframe
			@printIframe = document.createElement('iframe')
			document.body.appendChild @printIframe
			@printIframe.style.display = 'none'

	printPdf: =>
		# firefox doesn't allow to print from iframe. new tab with print enabled pdf instead
		if navigator.userAgent.toLowerCase().indexOf('firefox') > -1
			@openPdf null, true
		else
			@showLoading()
			@initIframe()

			iframe = @printIframe

			iframe.onload = =>
				setTimeout (=>
					iframe.focus()
					iframe.contentWindow.print()

					@hideLoading()
					window.popupsManager.closePopup @popupName
				), 1

			query = @setPdfQuery()
			if @view is 'schedule'
				iframe.src = '/dashboard/appointments/report?' + query
			else
				iframe.src = '/dashboard/my-transactions/report?' + query

	getSelectedStart: ->
		if @$block.$dateToggler.length
			begin = @$block.$dateToggler.filter(':checked').val()
			switch begin
				when 'today' then begin = Moment().startOf('day')
				when 'tomorrow' then begin = Moment().add(1, 'day').startOf('day')
				when 'yesterday' then begin = Moment().subtract(1, 'day').startOf('day')
				when 'other'
					datepickerDate = @$block.$datepickerStart.$input.datepicker 'getDate'
					begin = Moment(datepickerDate)
		else
			datepickerDate = @$block.$datepickerStart.$input.datepicker 'getDate'
			begin = Moment(datepickerDate)

		begin

	getSelectedEnd: ->
		if @$block.$datepickerEnd.length
			datepickerDate = @$block.$datepickerEnd.$input.datepicker 'getDate'
			end = Moment(datepickerDate)
		end

	showLoading: ->
		@$block.$loading.removeClass 'm-hide'

	hideLoading: ->
		@$block.$loading.addClass 'm-hide'

	adjustStartBounds: =>
		endDate = @$block.$datepickerEnd.$input.datepicker 'getDate'
		@$block.$datepickerStart.$input.datepicker('option',
			maxDate: Moment(endDate).toDate()
		)

	adjustEndBounds: =>
		startDate = @$block.$datepickerStart.$input.datepicker 'getDate'
		@$block.$datepickerEnd.$input.datepicker('option',
			minDate: Moment(startDate).toDate()
		)

module.exports = Print

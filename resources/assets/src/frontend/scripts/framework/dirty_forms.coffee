Backbone = require 'backbone'

class DirtyForms
	constructor: ->
		_.extend @, Backbone.Events # add power of Backbone.Events

		@initCollection()
		@addOnUnloadEvent()

	initCollection: ->
		@collection = new Backbone.Collection(0, {
			model: Backbone.Model.extend({
				defaults: {isDirty: false}
				})
			})

	addOnUnloadEvent: ->
		window.onbeforeunload = =>
			if @hasUnsaved()
				return 'You have unsaved changes!'

	hasUnsaved: ->
		return @collection.where({isDirty: true}).length

	# add form to collection and return model
	addForm: (form) ->
		@collection.push form, (model) ->
			 return model

	reset: -> @collection.reset()

	# scroll to dirty $el
	scrollTo: () ->

module.exports = new DirtyForms()

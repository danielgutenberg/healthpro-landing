class ServiceArea
	UNITS =
		MI:
			SHORT: 'mi'
			FULL: 'miles'
		KM:
			SHORT: 'km'
			FULL: 'kilometers'

	MI_IN_KM = 1.609344
	KM_IN_MI = 0.621371

	constructor: (@serviceArea, @unit = UNITS.KM.SHORT) ->

	convert: (unit) ->
		return @serviceArea unless @serviceArea
		return @serviceArea if unit is @unit
		switch unit
			when UNITS.MI.SHORT
				area = @serviceArea * KM_IN_MI
			when UNITS.KM.SHORT
				area = @serviceArea * MI_IN_KM
		Math.round area

	format: (format = 'full', unit) ->
		area = @convert unit
		unitOptions = @getUnitOptions unit

		label = "#{area} "
		label += switch (format)
			when 'full' then unitOptions.FULL
			else unitOptions.SHORT

		label

	getUnitOptions: (unit) ->
		key = _.findKey UNITS, (U) -> U.SHORT is unit
		return UNITS[key] if UNITS[key]?
		null



module.exports = ServiceArea

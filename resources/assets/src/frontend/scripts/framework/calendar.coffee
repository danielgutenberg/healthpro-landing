require 'select2'

class Calendar
	constructor: (@$node) ->
		@$node.select = $('.m-calendar_day .select2', @$node)
		@$node.day = $('.m-calendar_day select', @$node)
		@$node.month = $('.m-calendar_month select', @$node)
		@$node.year = $('.m-calendar_year select', @$node)
		@bind()

		@checkMonth()

	bind: ->
		@$node.month.on 'change', =>
			@checkMonth()
		@$node.year.on 'change', =>
			@checkMonth()

	checkMonth: ->
		year = $(@$node.year).val()
		leapYear = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)


		switch $(@$node.month).val()
			when '1'
				@listDates(31)
			when '2'
				if leapYear && $(@$node.year).val()
					@listDates(29)
				else
					@listDates(28)
			when '3'
				@listDates(31)
			when '4'
				@listDates(30)
			when '5'
				@listDates(31)
			when '6'
				@listDates(30)
			when '7'
				@listDates(31)
			when '8'
				@listDates(31)
			when '9'
				@listDates(30)
			when '10'
				@listDates(31)
			when '11'
				@listDates(30)
			when '12'
				@listDates(31)
			else
				@listDates(31)

	listDates: (count) ->
		$(@$node.select).remove()
		$(@$node.day.selector + ' option').removeAttr('disabled')
		$(@$node.day.selector + ' option:nth-child(n+'+(count+2)+')').attr('disabled', 'true')

		if $(@$node.day).val() == null && $(@$node.month).val()
			$(@$node.day.selector + ' option').removeAttr('selected')
			$(@$node.day.selector + ' option:nth-child('+(count+1)+')').attr('selected', 'selected')

		$(@$node.day).select2()

_.each $('.m-birdth'), (item, i) ->
	$item = $(item)
	new Calendar $item

module.exports = Calendar

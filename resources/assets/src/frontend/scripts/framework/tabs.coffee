class Tabs

	constructor: (@$el) ->
		@setOptions()
		@cacheDom()
		@addListeners()
		@init()

	addListeners: ->
		@$el.$switch.on 'click', (e) =>
			e.preventDefault()
			@activateTab $(e.currentTarget)

	init: ->
		@$el.$switch.first().trigger 'click'

	setOptions: ->


	activateTab: ($switch) ->
		return if $switch.hasClass 'm-active'

		@$el.$switch.removeClass 'm-active'
		$switch.addClass 'm-active'

		@$el.$tabs.removeClass 'm-active'
		@$el.$tabs.filter('[data-tab="' + $switch.data('tab-switch') + '"]').addClass 'm-active'

	cacheDom: ->
		@$el.$switch 	= @$el.find('[data-tab-switch]')
		@$el.$tabs		= @$el.find('[data-tab-list] [data-tab]')


$("[data-tabs]").each -> new Tabs $(this)

module.exports = Tabs

class AccordionPane
	constructor: (@$block, @accordion) ->
		@$header = $('[accordion-el="pane-header"]', @$block)
		@$switcher = $('[accordion-el="pane-switcher"]', @$block)
		@$container = $('[accordion-el="pane-container"]', @$block)
		@isOpened = @$container.hasClass('m-open')

		@bind()

	bind: ->
		@$header.on 'click', (e) =>
			@toggle()

	toggle: ->
		if @isOpened is true then @close() else @open()

	open: ->
		_.each @accordion.panes, (item) =>
			item.close()
			return

		@$container.removeClass('m-hide')
		@$block.addClass('m-open')
		@isOpened = true

	close: ->
		@$container.addClass('m-hide')
		@$block.removeClass('m-open')
		@isOpened = false

module.exports = AccordionPane

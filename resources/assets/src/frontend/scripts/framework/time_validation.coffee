class TimeValidation
	constructor: (@$node) ->
		@cacheDom()

	validate: (model) ->
		@clearErrors()
		if model && model.get('education_name')
			if model.get('from_month') == 'Month' || model.get('from_month') == null
				@showErrors('fromMonthError')
			if model.get('from_year') == 'Year' || model.get('from_year') == null
				@showErrors('fromYearError')

			if model.get('from_month') == 'Month' || model.get('from_month') == null || model.get('from_year') == 'Year' || model.get('from_year') == null
			  return false

		if !@$node.toMonthVal && !@$node.toYearVal
			return true
		else
			if @$node.fromYearVal > @$node.toYearVal
				@showErrors('yearError')
				return false
			else
				if @$node.fromYearVal == @$node.toYearVal && @$node.fromMonthVal > @$node.toMonthVal
					@showErrors('monthError')
					return false

		return true

	cacheDom: ->
		@$node.$fromMonth = $('[name*="[from][month]"]', @$node)
		@$node.$fromYear = $('[name*="[from][year]"]', @$node)
		@$node.$toMonth = $('[name*="[to][month]"]', @$node)
		@$node.$toYear = $('[name*="[to][year]"]', @$node)

		@$node.fromMonthVal = @$node.$fromMonth.val()
		@$node.fromYearVal = @$node.$fromYear.val()
		@$node.toMonthVal = @$node.$toMonth.val()
		@$node.toYearVal = @$node.$toYear.val()


		return @$node

	showErrors: (error) ->
		switch error
			when 'yearError'
				@$node.$toYear.parent().addClass('m-error')
				@$node.$toYear.parent().find('.field--error').html('Year error')

			when 'monthError'
				@$node.$toMonth.parent().addClass('m-error')
				@$node.$toMonth.parent().find('.field--error').html('Month error')


			when 'fromMonthError'
				@$node.$fromMonth.parent().addClass('m-error')
				@$node.$fromMonth.parent().find('.field--error').html('Field should be selected')

			when 'fromYearError'
				@$node.$fromYear.parent().addClass('m-error')
				@$node.$fromYear.parent().find('.field--error').html('Field should be selected')

			else return

	clearErrors: ->
		@$node.$toYear.parent().removeClass('m-error')
		@$node.$fromMonth.parent().find('.field--error').html('')

module.exports = TimeValidation

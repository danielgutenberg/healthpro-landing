require 'caret'
require 'phonenumber'

class Phone

	defaults:
		defaultPrefix: '+1'
		allowedPrefixes: ['+1']
		value: null

	options = {}

	constructor: (@$node, options = {}) ->
		@options = _.extend _.clone(@defaults), options
		@init()

	init: ->
		if @options.value
			@$node.val @options.value

		@$node.mobilePhoneNumber
			defaultPrefix: @options.defaultPrefix

		@addClasses()
		@bindEvents()


	bindEvents: ->
		@$node.on 'keyup', => @touch()
		@$node.on 'keyup', @removePlus
		@$node.on 'keydown', @preventPlus
		@$node.on 'change', => @limitDigits()

	formattedVal: (ignorePrefix = false) ->
		val = @$node.mobilePhoneNumber('val')
		if ignorePrefix
			prefix = @$node.mobilePhoneNumber('prefix')
			val = val.replace prefix, ''
		val

	val: -> @$node.val()

	touch: -> @$node.trigger 'change'

	removePlus: (e) ->
		keycode = if e.keyCode then e.keyCode else e.which
		if keycode == 187
			@inputText = $(e.target).val()
			if @inputText[0] == '+'
				$(e.target).val(@inputText.slice(0, -1))

	preventPlus: (e) ->
		keycode = if e.keyCode then e.keyCode else e.which
		if keycode == 187
			e.preventDefault()

	addClasses: ->
		if @$node.parents('.field').length
			parent = @$node.parents('.field')
		else if @$node.parents('[data-field-parent]').length
			parent = @$node.parents('[data-field-parent]')

		if parent then parent.addClass('m-tel_prefix')

	validate: ->
		prefix = @$node.mobilePhoneNumber('prefix')
		if _.indexOf(@options.allowedPrefixes, prefix) < 0
			return false

		@$node.mobilePhoneNumber.validate()

	limitDigits: ->
		val = @formattedVal(true)
		if val.length > 10
			val = val.slice 0, 10
			@$node.val("#{val}").keyup()


$('[data-phone]').each -> new Phone $(this)

module.exports = Phone

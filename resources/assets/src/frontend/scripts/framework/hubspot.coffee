class Hubspot

	defaults:
		form:
			css: ''
			cssRequired: ''
			portalId: ''
			formId: ''
		cta:
			portalId: ''
			ctaId: ''
			new_window: false
			replace: false

	scriptsLoaded: false
	scriptsLoading: false

	constructor: ->
		@loadScripts()

	loadScripts: ->
		return if @scriptsLoaded or @scriptsLoading
		@scriptsLoading = true
		$s ['//js.hsforms.net/forms/v2.js', '//js.hscta.net/cta/current.js'], 'hp.hubspot', =>
			@scriptsLoaded = true
			@scriptsLoading = false

	initForm: (options) ->
		$s.ready 'hp.hubspot', =>
			options = _.extend _.clone(@defaults.form), options
			console.error 'portalId parameter is required' unless options.portalId
			console.error 'formId parameter is required' unless options.formId

			target = $(options.target)
			return unless target.length

			hbspt.forms.create options

	initCta: (options) ->
		$s.ready 'hp.hubspot', =>
			options = _.extend _.clone(@defaults.cta), options
			console.error 'portalId parameter is required' unless options.portalId
			console.error 'ctaId parameter is required' unless options.ctaId
			console.error 'target parameter is required' unless options.target

			target = $(options.target)

			linkAttrs = ''
			linkAttrs += ' target="_blank"' if options.new_window

			html = "<span class='hs-cta-wrapper' id='hs-cta-wrapper-#{options.ctaId}'>"
			html += "<span class='hs-cta-node hs-cta-#{options.ctaId}' id='hs-cta-#{options.ctaId}'>"
			html += "<a href='http://cta-redirect.hubspot.com/cta/redirect/#{options.portalId}/#{options.ctaId}' #{linkAttrs}>"
			html += "<img class='hs-cta-img m-hide' src='https://no-cache.hubspot.com/cta/default/#{options.portalId}/#{options.ctaId}.png' alt='#{options.label}' style='border-width:0px'>"
			html += '</a></span></span>'

			if options.replace
				target.html html
			else
				target.append html

			hbspt.cta.load options.portalId, options.ctaId, {}

# make sure we have only one hubspot instance.
module.exports = new Hubspot()

class Select
	constructor: (@parentClass, @itemClass, @options) ->
		@parentClass = '.js-highlight_on_hover' unless @parentClass
		@itemClass = '.js-highlight_on_hover--item' unless @itemClass
		@init()

	init: ->
		$(@itemClass).hover ( (e) =>
			$(e.currentTarget).parents(@parentClass).find(@itemClass).addClass 'm-hover'
		), (e) =>
			$(e.currentTarget).parents(@parentClass).find(@itemClass).removeClass 'm-hover'

module.exports = Select

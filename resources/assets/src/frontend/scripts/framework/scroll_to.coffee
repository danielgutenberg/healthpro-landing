class ScrollTo
	constructor: (@$block) ->
		@bind()

	bind: ->
		@$block.on 'click', (e) ->
			e.preventDefault()
			$item = $(this)

			$(document).trigger 'slideout:close'

			scrollTo = $item.data('scroll')
			unless scrollTo.length
				scrollTo = '#' + $item.attr('href').replace('#', '')

			$scrollTo = $(scrollTo)
			scrollToMoreOffset = if $item.data('scroll-offset') then $item.data('scroll-offset') else 0

			return unless $scrollTo.length
			$('html, body').animate({
				scrollTop: $scrollTo.offset().top - scrollToMoreOffset - (if $(window).width() > 1000 then $('header.header').outerHeight() else 0)
			}, 400)

$("[data-scroll]").each -> new ScrollTo $(this)

module.exports = ScrollTo

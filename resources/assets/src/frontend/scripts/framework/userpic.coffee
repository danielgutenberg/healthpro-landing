class Userpic
	constructor: (@$block, @options) ->

		require.ensure [], =>
			require 'imports?define=>false!jquery.fileupload'

			@options = if @$block.data('options')? then @$block.data('options') else null
			@$block.$image = $('img', @$block)
			@$allUserpics = $('.userpic img')
			@$block.$inner = $('.userpic--inner', @$block)

			if @options.hasUpload?
				@$block.uploadButton = $('.js-userpic--upload_btn', @$block)
				@$block.filesInput = $('.js-userpic--files_input', @$block)
				@$block.loading = $('<span class="loading_overlay m-light"><span class="spin m-cog"></span></span>')
					.appendTo(@$block.$inner)
					.hide()
				@uploadInit()

	uploadInit: ->
		@$block.filesInput.fileupload
			url: @options.uploadUrl
			type: 'post'
			formData:
				_token: @options._token
			maxFileSize: 10000000
			add: (e, data) => @addUserpic(e, data)
			done: (e, data) =>
				@uploading = false
				@successUpload(e, data)

	uploadEdited: (blob) ->
		@$block.filesInput.fileupload('option', 'formData').file = blob
		@$block.filesInput.fileupload 'add', files: [ blob ]

	addUserpic: (e, data) ->
		return if @uploading
		@uploading = true
		if data.files and data.files[0]
			acceptFileTypes = /^image\/(gif|jpe?g|png)$/i

			vex.defaultOptions.className = 'vex-theme-default'
			vexDialog.buttons.YES.text = 'Ok'

			if data.files[0]['type']? and !acceptFileTypes.test(data.files[0]['type'])
				vexDialog.alert 'The file you are attempting to upload does not contain an acceptable file type. Accepted types are GIF, JPG, JPEG, and PNG.'
				@uploading = false
				return

			if data.files[0]['size']? and data.files[0]['size'] > 10000000 and acceptFileTypes.test(data.files[0]['type'])
				vexDialog.alert 'The file you are attempting to upload is too large.'
				@uploading = false
				return

			reader = new FileReader()
			reader.onload = (e) =>
				@showImage(e.target.result)
			reader.readAsDataURL data.files[0]
		data.submit()
		@showLoading()

	showImage: (imageSrc, fullImageSrc) ->
		@$block.$image.attr('src', imageSrc)
		@$block.$image.data('original', fullImageSrc)
		@$allUserpics.attr('src', imageSrc)

	showLoading: ->
		@$block.loading.show()

	hideLoading: ->
		@$block.loading.hide()

	successUpload: (e, data) ->
		@hideLoading()
		@showImage(data.result.data.urls.avatar[0]['250x250'], data.result.data.urls.avatar[0]['original'])

		# event to be listened outside
		$.event.trigger
			type: 'image:uploaded'

		@uploadInit()


$('.js-userpic').each -> new Userpic $(this)

module.exports = Userpic

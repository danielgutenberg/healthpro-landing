class Alert
	constructor: (@$node) ->
		@$node.close = @$node.find('.alert--close')
		@$node.links = @$node.find('[data-alert-link]')

		@$dom = {}
		@$dom.body = $('body')

		@show()
		@bind()

	bind: ->
		@$node.close.on 'click', => @hide()
		@$node.links.on 'click', (e) =>
			@linkClick e

	show: ->
		@$node.removeClass('m-hide')
		@$node.addClass('m-show')
		@$dom.body.addClass('m-alert')

	hide: ->
		@$dom.body.removeClass('m-alert')
		@$node.addClass('m-hide')
		@$node.removeClass('m-show')
		return false

	linkClick: (e) ->
		$link = $(e.currentTarget)
		options = $link.data('options')
		return unless options.type

		e.preventDefault()

		_.defaults options,
			successMsg: 'Success'
			errorMsg: 'Something went wrong. Please reload the page and try again.'
			url: null

		$alert = $link.closest('.alert')

		url 		= if options.url then options.url else $link.attr('href')
		method 		= if options.method then options.method else "get"
		data 		= if method is 'get' then {} else JSON.stringify({_token: window.GLOBALS._TOKEN})

		$alert.addClass('m-loading')
		$.ajax
			url: url
			method: method
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: data
			success: @getCallback($alert, options)
			error: @getErrorCallback()


	getErrorCallback: ($alert, options) ->
		->
			$alert.remove()
			alert(options.errorMsg)

	getCallback: ($alert, options) ->

		if !options.type?
			options.type = ''

		callback = null

		switch options.type
			when 'send_activation'
				callback = (response) =>
					if response.message isnt 'success'
						cb = @getErrorCallback($alert, options)
						cb()
						return
					popup = window.popupsManager.getPopup('resend_email')
					return unless popup
					popup.openPopup()
					popup.$block.on 'popup:closed', ->
						$alert.remove()

			# default action
			else
				callback = (response) =>
					msg = if response.message is 'success' then options.successMsg else options.errorMsg
					$alert.removeClass('m-loading').html("<p>#{msg}</p>")

		callback


module.exports = Alert

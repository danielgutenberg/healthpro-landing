require 'maskedInput'

class MaskedInput
	constructor: (@$node) ->
		@$node.mask @$node.data('masked')


_.each $('[data-masked]'), (item) ->
	$item = $(item)
	new MaskedInput $item


module.exports = MaskedInput

class GoogleMaps
	url: '//maps.googleapis.com/maps/api/js?libraries=places'
	gmaps: null

	load: ->
		@loadScript() unless @gmaps
		@

	loadScript: ->
		$s [@url], 'hp.gmaps', =>
			@gmaps = window.google.maps
		@

	ready: (ready) ->
		$s.ready 'hp.gmaps', ready

window.GMaps = new GoogleMaps() unless window.GMaps

module.exports = GMaps

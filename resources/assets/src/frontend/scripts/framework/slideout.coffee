class Slideout
	constructor: (@$block) ->
		@setOptions()

		@$block.$slideout = $(@options.slideout)
		@$block.$mask = $(@options.mask)

		@$body = $('body')
		@$document = $(document)
		@$window = $(window)

		@bind()

	bind: ->
		@$block.on 'click', (e) =>
			e.preventDefault()
			@$body.toggleClass @options.bodyClass

		@$block.$mask.on 'click', (e) =>
			e.preventDefault()
			@$body.toggleClass @options.bodyClass

		@$window.on 'resize', =>
			@$body.removeClass @options.bodyClass

		@$document.on 'slideout:close', =>
			@$body.removeClass @options.bodyClass

		@$document.on 'slideout:open', =>
			@$body.addClass @options.bodyClass

	setOptions: ->
		options = @$block.data('options') ? {}
		defaults =
			slideout: '#slideout'
			mask: '#slideout-mask'
			bodyClass: 'm-slideout'

		@options = _.extend defaults, options


$('[data-slideout]').each -> new Slideout $(this)

module.exports = Slideout

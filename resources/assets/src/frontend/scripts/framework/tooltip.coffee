require 'tooltipster'

class Tooltip
	constructor: (@$block) ->
		content = $('[data-tooltip-content]', @$block)
		@$block.tooltipster
			theme: 'tooltipster-' + @$block.data('tooltip')
			position: @$block.data('tooltip-position') if @$block.attr('data-tooltip-position')
			content: content if content.length
			contentCloning: true
			delay: $(@$block.data('tooltip-delay')) if @$block.attr('data-tooltip-delay')

$('[data-tooltip]').each -> new Tooltip $(this)

module.exports = Tooltip

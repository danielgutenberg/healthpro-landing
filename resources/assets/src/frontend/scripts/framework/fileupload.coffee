# right now file upload doesn't support multiple files
# please add support when needed
class FileUpload
	options:
		captionMaxLength: 20
		labels:
			browse: 'Browse File'
			noFileSelected: 'No file chosen'
		classes:
			fileSelected: 'm-file_selected'
			inited: 'm-inited'

	constructor: (@$el) ->
		@init()

	init: ->
		@cacheDom()
		@appendBlocks()
		@bind()

		@$el.trigger 'fileupload:inited'

	cacheDom: ->
		@$el.$file = @$el.find('input:file')

	appendBlocks: ->
		@$el.addClass @options.classes.inited

		@$el.$button = $('<button class="fileupload--cta">' + @options.labels.browse + '</button>')

		@$el.$reset = $('<button class="fileupload--reset"></button>')
		@$el.$caption = $('<span class="fileupload--caption">' + @options.labels.noFileSelected + '</span>')

		@$el.$container = $('<div class="fileupload--container"></div>')

		@$el.$container.append @$el.$button
		@$el.$container.append @$el.$caption
		@$el.$container.append @$el.$reset

		@$el.append @$el.$container

	bind: ->
		# prevent focus
		@$el.$file.attr 'tabIndex', -1
		@$el.$button.attr 'tabIndex', -1

		@$el.$button.on 'click', => @dialog()
		@$el.$file.on 'change', => @fileChanged()
		@$el.$reset.on 'click', => @reset()


	dialog: ->
		@$el.$file.focus().click()
		@$el.trigger 'fileupload:dialog'
		@

	reset: ->
		@$el.$file.val ''
		@$el.$file.trigger 'change'
		@$el.trigger 'fileupload:reset'
		@

	fileChanged: ->
		@$el.$caption.html @getCaption()

		if @isFileSelected()
			@$el.addClass @options.classes.fileSelected
		else
			@$el.removeClass @options.classes.fileSelected

		@$el.trigger 'fileupload:changed'
		@

	getFilesList: -> @$el.$file[0].files

	getFile: -> return if @isFileSelected() then @getFilesList()[0] else null

	getCaption: ->
		if @isFileSelected()
			caption = @getFile().name
			if caption.length > @options.captionMaxLength
				caption = caption.substr(0, @options.captionMaxLength - 1) + '&hellip;'
			return caption
		else
			return @options.labels.noFileSelected

	isFileSelected: -> @getFilesList().length > 0

_.each $('[data-fileupload]'), (item) -> new FileUpload $(item), options

module.exports = FileUpload

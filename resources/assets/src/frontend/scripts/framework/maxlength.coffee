class Maxlength
	constructor: (@$node) ->
		@currentLength = 0
		@defaultOptions = {}
		@options = @$node.data('options') || {}
		_.merge @options, @defaultOptions

		@$node.field = $('textarea', @$node)

		# set maxlength
		if typeof @$node.field.attr('data-maxlength') isnt 'undefined'
			@maxlength = parseInt @$node.field.attr('data-maxlength')
		else if @options.maxlength
			@maxlength = parseInt @options.maxlength
		else
			@maxlength = 140

		@$node.$counter = $('<p class="field--length"><span></span> out of ' +@maxlength+ '</p>').appendTo(@$node)

		# set currentLength
		@checkLeftLength()

		@$node.attr('data-maxlength', 'inited')

		@bind()

	bind: ->
		# prevent pageup and page down events in textarea
		@$node.field.on 'keydown', (e) =>
			if e.keyCode? and (e.keyCode is 33 or e.keyCode is 34)
				e.preventDefault()

		@$node.field
			.on 'keyup focus change', (event) =>
				@checkLeftLength()

	checkLeftLength: ->
		current = @$node.field.val().length

		if current >= @maxlength
			@$node.$counter.addClass('m-overlap')
		else
			@$node.$counter.removeClass('m-overlap')

		@$node.$counter.find('span').text( current  )

# init
_.each $('.js-maxlength'), (item) ->
	$item = $(item)
	instance = new Maxlength($item)

module.exports = Maxlength

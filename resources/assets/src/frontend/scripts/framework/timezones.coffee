Moment = require 'moment'

Moment = require 'moment'
require 'moment-timezone'

class Timezones
	constructor: ->
		@timezonesNames = Moment.tz.names()
		@_timezones = []

	current: -> Moment().format('Z')

	default: -> 'UTC'

	timezones: ->
		return @_timezones if @_timezones.length
		@_timezones = @timezonesNames.map (name) =>
			timezone =
				name: name
				offset: Moment.tz(name).format('Z')
			timezone.full_name = "(GMT #{timezone.offset}) #{name}"
			timezone

		@_timezones.sort (a, b) -> parseInt(a.offset.replace(':', ''), 10) - parseInt(b.offset.replace(':', ''), 10)

		@_timezones


module.exports = Timezones

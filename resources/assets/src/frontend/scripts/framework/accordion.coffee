AccordionPane = require 'framework/accordion_pane'

class Accordion
	constructor: (@element, @options) ->
		return unless @element.length
		@panes = []
		@collectPanes()

		@openPane @options?.defaultOpenedPane

	collectPanes: ->
		_.each $('[accordion-pane]', @element), (item) =>
			@panes.push new AccordionPane($(item), @)

	openPane: (order) ->
		return unless order
		index = -1
		if order? and order is 'last'
			index = @panes.length - 1
		else if order? and order is 'first'
			index = 0
		else if order?
			index = order - 1
		@panes[index].open() if index > -1

module.exports = Accordion

Alert = require 'framework/alert'

class Alerts
	constructor: () ->
		@initAlertsContainer()
		@init()

	init: ->
		$('.alert').each -> new Alert $(this)

	initAlertsContainer: ->
		if !$('.alerts')[0]
			$('body').append('<div class="alerts m-sticky"></div>')

	addNew: (@$type, @$msg) ->
		alertHtml = '<div class="alert m-show m-'+@$type+'">'+@$msg+' <a href="#" class="alert--close"></a></div>'
		$alert = $(alertHtml).appendTo('.alerts')

		new Alert $alert

window.Alerts = new Alerts()
#Example: window.Alerts.addNew('warning', 'Message')

module.exports = Alerts

class NumericInput

	defaults:
		decimal: '.'
		leadingZeroCheck: true
		initialParse: true
		parseOnBlur: true
		clearInputOnBlurForZeroValue: true
		allowNegative: false
		allowEmpty: true
		numberOfDecimals: 2
		callback: ->

	constructor: (@$node, options) ->
		@options = $.extend {}, @defaults, options
		@bind()

	bind: ->
		# bind the keypress event
		@$node.keypress (e) =>
			if @preventDefaultForKeyCode(e.which) is true
				e.preventDefault()
			newValue = @getNewValueForKeyCode(e.which, @$node.val())
			if newValue != false
				@$node.val newValue
				@options.callback.call @, newValue
			return

		if @options.parseOnBlur is true
			# bind the blur event to (re)parse value
			@$node.on 'blur', =>
				parsedValue = @parseValue(@$node.val())
				@$node.val parsedValue
				return

		if @options.clearInputOnBlurForZeroValue is true
			@$node.on 'focus', =>
				result = @$node.val()
				result = result.replace(/[A-Za-z$]/g, '')
				result = result.replace('-', '')
				result = result.replace(@options.decimal, '.')
				if parseFloat(result) == 0
					@$node.val ''
				return

		# trigger callback on change
		@$node.on 'input propertychange', =>
			parsedValue = @parseValue(@$node.val())
			@options.callback.call @, parsedValue
			return

		# initial parse values
		if @options.initialParse == true
			parsedValue = @parseValue(@$node.val())
			@$node.val parsedValue
		@

	preventDefaultForKeyCode: (keyCode) ->
		# numeric
		if keyCode >= 48 and keyCode <= 57
			return false
		# special keys
		switch keyCode
			when 0, 8, 9, 35, 36, 37, 39, 144
				# num lock
				return false
			else
				return true
		return

	getNewValueForKeyCode: (keyCode, currentValue) ->

		# if a comma or a dot is pressed...
		if keyCode == 44 or keyCode == 46 or keyCode == 188 or keyCode == 190
			# if we allow decimals
			unless @options.numberOfDecimals == 0
				# and we do not have a options.decimal present...
				if currentValue.indexOf(@options.decimal) == -1
					# append leading zero if currentValue is empty and leadingZeroCheck is active
					currentValue = '0' if $.trim(currentValue) == '' and @options.leadingZeroCheck

					# append the options.decimal instead of the dot or comma
					return currentValue + @options.decimal

		# prepend the minus
		if keyCode == 45 and @options.allowNegative
			if currentValue.charAt(0) != '-'
				return '-' + currentValue

		if @options.numberOfDecimals != null
			number = currentValue.split(@options.decimal)
			integer = number[0]
			decimals = number[1]
			if decimals?.length >= @options.numberOfDecimals
				decimals = decimals.slice(0, 1)
				return "#{integer}.#{decimals}"

		# do not allow star with 0 if no decimals allowed
		if keyCode == 48 or keyCode == 96
			if @options.numberOfDecimals == 0 and currentValue == '0'
				return ''

		false

	parseValue: (value) ->
		minusWasStripped = false
		result = value.replace(/[A-Za-z$]/g, '')
		if result.length == 0 and @options.allowEmpty
			return ''
		# strip minus and prepend later
		if result.indexOf('-') != -1
			result = result.replace('-', '')
			minusWasStripped = true
		if result.indexOf('.') != -1 or result.indexOf(',') != -1
			result = result.replace('.', @options.decimal)
			result = result.replace(',', @options.decimal)
		if result.indexOf(@options.decimal) == 0
			result = '0' + result
		if minusWasStripped == true and @options.allowNegative == true
			result = '-' + result

		if @options.numberOfDecimals != null
			decimals = result.split(@options.decimal)[1]
			if decimals != undefined
				if decimals.length > @options.numberOfDecimals
					result = Number(Number(String('0.' + decimals)).toFixed(@options.numberOfDecimals)) + Math.floor(Number(result))

				if decimals.length < @options.numberOfDecimals
					result += new Array(@options.numberOfDecimals - (decimals.length) + 1).join('0')
			result = String(Number(result).toFixed(@options.numberOfDecimals))

		result

module.exports = NumericInput

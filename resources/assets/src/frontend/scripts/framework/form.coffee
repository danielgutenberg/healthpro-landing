NameValidator = require 'utils/validators/name'
EmailValidator = require 'utils/validators/email'

class Form
	constructor: (@$node, options) ->
		@options = {}
		@isValid = false
		@errors = 0
		@errorTemplate = '<span class="field--error"></span>'
		@errorMessages =
			required: 'Please fill this field in.'
			name:
				format: "Please enter only letters."
			email:
				format: "Please enter a valid email address."
			minLength: "Sorry, it's a bit too short."
			maxLength: "Sorry, that is too many characters"
			terms: "Please agree to terms of service before registering"

		@setOptions(options)
		@cacheDom()
		@bindEvents()

	cacheDom: ->
		@$node.$submit = $('[data-form-el="submit"]', @$node)
		@$node.$submit.text = @$node.$submit.html()
		@$node.$input = $('input', @$node)
		@$node.$fields = $('input:not([type="hidden"]):not([type="radio"])', @$node) # do normal selector for inputs data-form-field

		if @options.customLoading
			if @options.$loading?
				@$node.$loading = @options.$loading
		else
			@$node.$loading = $('<span class="loading_overlay m-light m-hide"><span class="spin m-logo"></span></span>').appendTo @$node

	setOptions: (options) ->
		if @$node.data('form')?
			@options = @$node.data('form')

		if options?
			_.extend @options, options

	bindEvents: ->
		@$node.$fields.on 'focus', (e) =>
			$field = $(e.target)
			$field.parents('.field').removeClass('m-error')
			$field.parents('.form--group').removeClass('m-error')

		if @$node.$submit.length
			@$node.$submit.on 'click', (e) =>
				e.preventDefault()
				e.stopImmediatePropagation()
				@submitForm()

	showLoading: ->
		if @$node.$loading?.length
			@$node.$loading.removeClass('m-hide')

	hideLoading: ->
		if @$node.$loading?.length
			@$node.$loading.addClass('m-hide')

	submitForm: ->
		@checkForm()

		formData = {}
		@$node.serializeArray().map( (x) -> formData[x.name] = x.value )
		if !formData._token? then formData._token = window.GLOBALS._TOKEN

		if !@errors
			@showLoading()
			$.ajax
				url: @options.url
				type: 'post'
				data: formData
				success: (response) =>
					# @hideLoading()
					switch response.message
						when 'success'
							if @options.onSuccess?
								@options.onSuccess response
							else
								if @options.name == 'password_request'
									Alerts.addNew('success', 'An email with further instructions has been sent')
									@hideLoading()
								else
									location.reload()

						when 'error' then @setErrors(response.errors.messages)
				error: (response) =>
					@hideLoading()
					if response?
						@setErrors response.responseJSON.errors

	checkForm: ->
		@unsetErrors()
		_.each @$node.$fields, (field) =>
			@checkField $(field)

	unsetErrors: ->
		@errors = 0
		@$node.find('.field--error').remove()
		@$node.find('.field.m-error, .form--group.m-error').removeClass('m-error')

	setErrors: (errors) ->
		_.forIn errors, (value, key) =>
			$field = $('input[name='+key+']', @$node)
			@showFieldError($field, value.messages[0])

	showFieldError: ($field, errors) ->
		$field
			.parent().addClass('m-error').end()
			.parents('.form--group').addClass('m-error')

		if errors?
			$errorContainer = $('<span class="field--error" />').appendTo( $field.parent() )
			errors = [errors] unless $.isArray(errors)
			errors.forEach (error) =>
				html = $errorContainer.html()
				html += error + '<br>' + html
				$errorContainer.html( html )

			@hideLoading()

	checkField: ($field) ->
		type = $field.attr('type')
		letters_only = $field.attr('data-letters-only')
		allow_empty = if $field.data('allow-empty')? then $field.data('allow-empty')

		if $field.val() is '' and !allow_empty
			@showFieldError($field, @errorMessages.required)
			@errors++

			return

		if $field.attr('min')
			if $field.val().length < parseInt $field.attr('min')
				@showFieldError($field, @errorMessages.minLength)
				@errors++

		if $field.attr('max')
			maxNumber = parseInt $field.attr('max')
			if $field.val().length > maxNumber
				@showFieldError($field, 'Please limit this field to ' + maxNumber + ' characters')
				@errors++

		switch type
			when 'email'
				if @checkEmail($field.val())
					return
				else
					@showFieldError($field, @errorMessages.email.format)
					@errors++
					return
			when 'checkbox'
				if $field.is(':checked') == false and $field.attr('class') == 'auth_form--terms'
					@showFieldError($field, @errorMessages.terms)
					@errors++

		if typeof letters_only != typeof undefined and letters_only != false
			if @checkNames($field.val())
				return
			else
				@showFieldError($field, @errorMessages.name.format)
				@errors++
				return

	checkEmail: (email) -> EmailValidator(email)

	checkNames: (name) -> NameValidator(name)

_.each $('[data-form]'), (form) ->
	$form = $(form)
	return if $form.data('noinit')
	new Form $form

module.exports = Form

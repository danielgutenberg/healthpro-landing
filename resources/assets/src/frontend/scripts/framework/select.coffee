require 'select2'

class Select
	constructor: (@$node, @options) ->
		@query = ''
		@defaultOptions =
			minimumResultsForSearch: Infinity
		@setOptions()
		@init()

	setOptions: ->
		unless @options
			@options = @$node.data('options') ? @$node.data('select')

		if @options?
			if @options.type? and @options.type is 'multichoice' then @selectType = 'multichoice'
			if @options.allowCustomOptions? and @options.allowCustomOptions is true then @allowCustomOptions = true
			if @options.hasAjax? and @options.hasAjax then @addAjax()
			if @options.userOptions? and @options.userOptions
				@options.tags = true
				@options.createTag = (params) =>
					return {
						id: params.term
						text: params.term
						newOption: true
					}

			if @options.dropdownParent? and @options.dropdownParent is 'parent'
				@options.dropdownParent = @$node.parent()

		@options = _.merge(@defaultOptions, @options)

	userOptions: ->
		@$node.on 'change', =>
			if @$node.find(":selected").data("select2-tag")
				@$node.parent().find('.select2 .select2-selection__rendered').html(@$node.val())

	educationSwitch: ->
		@$node.on 'change', =>
			nameValue = @$node.attr('name')
			if @$node.find(":selected").data("select2-tag")
				@$node.attr('name', nameValue.replace("education_id", "education_name"))
			else
				@$node.attr('name', nameValue.replace("education_name", "education_id"))

	addAjax: ->
		@options.ajax =
			url: @options.ajaxUrl
			dataType: 'json'
			delay: 250
			data: (params) =>
				# save the current search to be included in results if we are allowing free text
				if @options.freetext?
					@query =
						name: params.term
						id: -1
				return {
					q: params.term
				}
			processResults: (response) =>
				# if we are allowing free text then we include the search as one of the results
				if @options.freetext?
					response.data.push(@query)
				return {
					results: response.data
				}
			cache: true

		@options.escapeMarkup = (markup) ->
			return markup

		@options.templateResult = (item) ->
			if item.loading then return item.text
			return item.name

		@options.templateSelection = (item) ->
			if item.selected is true
				return item.text
			else
				return item.name

		@options.minimumInputLength = 1

	init: ->
		@$node.select2(@options)
		@$node.parent().addClass('m-inited')

		if @options?
			if @options.visibleDisabled?
				@$node.parent().addClass('m-visible_disabled')

			if @options.wrapperClass?
				@$node.parent().addClass(@options.wrapperClass)

			if @options.dropdownClass?
				@setDropdownClass()

			if @options.userOptions?
				@userOptions()

			if @options.education?
				@educationSwitch()

	reInit: ->
		@$node.select2(@options)

	setDropdownClass: ->
		@$node.on 'select2:open', (e) =>
			$('.select2-container').addClass(@options.dropdownClass)

_.each $('[data-select]'), (item) ->
	$item = $(item)
	options = $item.data('options') ? $item.data('select')
	autoinit = if typeof $item.data('select').autoinit isnt 'undefined' then $item.data('select').autoinit else true
	if autoinit
		new Select $item, options


module.exports = Select

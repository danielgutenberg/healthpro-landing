Popup = require 'framework/popup'

# popups managment
class PopupsManager

	constructor: ->
		window.popups = @popups = []

		@collectPopups()
		@bind()

	collectPopups: ->
		_.each $('[data-popup]'), (item) =>
			$item = $(item)
			@addPopup $item

	addPopup: ($item) ->
		@popups.push(new Popup($item, @))

	bind: ->

		# find all popup openers and bind onclick
		@bindForElement $('[data-toggle="popup"]')

		# close all opened on keyup
		$(window).on 'keyup.popups_manager', (e) =>
			if e.keyCode is 27 || e.which is 27
				for popup in @popups
					popup.closePopup() if popup.canCloseOnEsc()


		# recalculate popup sizes and position
		$(document).on 'popup:center', =>
			_.each @popups, (item) =>
				if item.opened
					item.detectBigPopup()

	bindForElement: ($el) ->
		$el.on 'click.popups_manager', (e) =>
			e.preventDefault()
			$item = $(e.currentTarget)
			target = $item.attr('data-target')
			@openPopup(target)


	unBind: ->
		$('[data-toggle="popup"]').off('click.popups_manager')
		$(window).off('keyup.popups_manager')
		$(document).off('popup:center')

	openPopup: (target) ->
		for popup in @popups
			if popup.name is target
				popup.openPopup()

	closePopup: (target) ->
		for popup in @popups
			popup.closePopup() if popup.name is target

	getPopup: (target) ->
		for popup in @popups
			return popup if popup.name is target
		null

	closePopups: ->
		for popup in @popups
			popup.closePopup() if popup.isOpened()

	removePopup: (target) ->
		@closePopup target
		@popups = _.reject @popups, (popup) ->
			popup.name is target


window.popupsManager = new PopupsManager()

module.exports = PopupsManager

class RotateImage
	constructor: (@$block) ->
		@init()

	init: ->

	triggerReady: (src) ->
		$.event.trigger
			type: 'image:rotated'
			imageSrc: src

	rotate: (direction) ->
		switch direction
			when 'left' then @angle = -90
			when 'right' then @angle = 90

		@$block.angle = 0 if @$block.angle is undefined
		@$block.angle = (@$block.angle + @angle) % 360

		if @$block.angle >= 0
			rotation = Math.PI * @$block.angle / 180
		else
			rotation = Math.PI * (360+@$block.angle) / 180

		costheta = Math.cos(rotation)
		sintheta = Math.sin(rotation)

		if document.all and !window.opera
			canvas = document.createElement('img')
			canvas.src = @$block.src
			canvas.height = @$block.height
			canvas.width = @$block.width

			canvas.style.filter = 'progid:DXImageTransform.Microsoft.Matrix(M11=' + costheta + ',M12=' + -sintheta + ',M21=' + sintheta + ',M22=' + costheta + ',SizingMethod=\'auto expand\')'

		else
			canvas = document.createElement('canvas')
			if !@$block.oImage
				canvas.oImage = new Image
				canvas.oImage.src = @$block.attr 'src'

			else
				canvas.oImage = @$block.oImage

			canvas.style.width = canvas.width = Math.abs(costheta * canvas.oImage.width) + Math.abs(sintheta * canvas.oImage.height)
			canvas.style.height = canvas.height = Math.abs(costheta * canvas.oImage.height) + Math.abs(sintheta * canvas.oImage.width)
			context = canvas.getContext('2d')
			context.save()

			if rotation <= Math.PI / 2
				context.translate sintheta * canvas.oImage.height, 0
			else if rotation <= Math.PI
				context.translate canvas.width, -costheta * canvas.oImage.height
			else if rotation <= 1.5 * Math.PI
				context.translate -costheta * canvas.oImage.width, canvas.height
			else
				context.translate 0, -sintheta * canvas.oImage.width

			context.rotate rotation
			context.drawImage canvas.oImage, 0, 0, canvas.oImage.width, canvas.oImage.height
			nImage = document.createElement('img')
			nImage.src = canvas.toDataURL('image/png')
			context.restore()

		if @$block.id? then canvas.id = @$block.id
		canvas.angle = @$block.angle
		# @$block.replaceWith(nImage)
		@$block = $(nImage)

		# @triggerReady(nImage.src)
		return nImage.src

module.exports = RotateImage

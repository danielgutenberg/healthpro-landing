module.exports =
	defaults:
		hitType: 'event'

	events:
		wizard:
			booking:
				pick_time:
					eventCategory: 'Wizard/ Booking'
					eventAction: 'open'
					eventLabel: 'Pick Your Time'
				login:
					eventCategory: 'Wizard/ Booking'
					eventAction: 'open'
					eventLabel: 'Client Login'
				payment:
					eventCategory: 'Wizard/ Booking'
					eventAction: 'open'
					eventLabel: 'Payment'
				pay_with_package:
					eventCategory: 'Wizard/ Booking'
					eventAction: 'open'
					eventLabel: 'Pay With Package'
				confirmation:
					eventCategory: 'Wizard/ Booking'
					eventAction: 'open'
					eventLabel: 'Confirmation'
				success:
					eventCategory: 'Wizard/ Booking'
					eventAction: 'open'
					eventLabel: 'Success'
				client_create:
					eventCategory: 'Wizard/ Booking'
					eventAction: 'open'
					eventLabel: 'Create Your Client Profile'
				client_choice:
					eventCategory: 'Wizard/ Booking'
					eventAction: 'open'
					eventLabel: 'Select Your Client Profile'
				set_password:
					eventCategory: 'Wizard/ Booking'
					eventAction: 'open'
					eventLabel: 'Set Your Password'
			professional:
				personal_details:
					eventCategory: 'Wizard/ Professional'
					eventAction: 'open'
					eventLabel: 'Personal Details Step'
				locations:
					eventCategory: 'Wizard/ Professional'
					eventAction: 'open'
					eventLabel: 'Locations Step'
				services:
					eventCategory: 'Wizard/ Professional'
					eventAction: 'open'
					eventLabel: 'Services Step'
				payments:
					eventCategory: 'Wizard/ Professional'
					eventAction: 'open'
					eventLabel: ' Payment Information'
				payment_selected:
					eventCategory: 'Wizard/ Professional'
					eventAction: 'open'
					eventLabel: ' Payment [payment method] Selected'
				success:
					eventCategory: 'Wizard/ Professional'
					eventAction: 'open'
					eventLabel: 'Success Step'
			gift_certificate:
				select:
					eventCategory: 'Wizard/ Gift Certificate'
					eventAction: 'open'
					eventLabel: 'Select Certificate Step'
				payment:
					eventCategory: 'Wizard/ Gift Certificate'
					eventAction: 'open'
					eventLabel: ' Payment Information'
				confirmation:
					eventCategory: 'Wizard/ Gift Certificate'
					eventAction: 'open'
					eventLabel: 'Confirmation'
				success:
					eventCategory: 'Wizard/ Gift Certificate'
					eventAction: 'open'
					eventLabel: 'Success Step'

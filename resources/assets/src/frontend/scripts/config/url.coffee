routes =
	'professional-public-profile-url': 'p/{slug}'
	'client-public-profile-url': 'c/{slug}'
	'professional-setup-continue': 'wizard/continue/profile_setup'
	'professional-review': 'review/provider/{slug}'
	'dashboard': 'dashboard'
	'dashboard-edit-locations-services': 'dashboard/appointments/setup'
	'dashboard-conversations': 'dashboard/conversations'
	'dashboard-conversation': 'dashboard/conversations#to:{profileId}'
	'dashboard-appointments': 'dashboard/appointments'
	'dashboard-appointment': 'dashboard/appointments#appointment:{appointmentId}'
	'dashboard-packages': 'dashboard/appointments/my-packages'
	'dashboard-package': 'dashboard/appointments/my-packages#package:{packageId}'
	'dashboard-billling-invoice': 'dashboard/billing/invoice/{orderId}'
	'dashboard-settings-payment': 'dashboard/settings/payments'
	'wizard': 'wizard/{wizard}/{step}'

	'activate-old-professional': 'activate/old-professional/free-year'

	'terms': 'terms'
	'social-auth': 'auth/{service}/authorize'
	'login': 'auth/login'

###
 * [getRoute description]
 * @param {string} routeName name of route
 * @param {object} params    object with parameters, which will set in route
 * example: getRoute('professionalPublicProfileUrl', {'id': 5, 'assetId': 10})
###
getRoute = (routeName, params = {}) ->
	route = routes[routeName]
	for key, value of params
		route = route.replace "{#{key}}", value
	"/#{route}"

module.exports = getRoute

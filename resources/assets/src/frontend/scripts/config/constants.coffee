module.exports =
	IntroductorySessionSlug: 'introductory-session'
	Plans:
		1:
			id: 1
			slug: 'part_timer'
			title: 'Part timer'
			cycle: 'monthly'
			price: 18.89
			color: 'green'
		2:
			id: 2
			slug: 'part_timer'
			title: 'Part timer'
			cycle: 'annual'
			price: 189
			color: 'green'
		3:
			id: 3
			slug: 'healthpro'
			title: 'Healthpro'
			cycle: 'monthly'
			price: 27
			color: 'red'
		4:
			id: 4
			slug: 'healthpro'
			title: 'Healthpro'
			cycle: 'annual'
			price: 270
			color: 'red'
		5:
			id: 5
			slug: 'healthpro_plus'
			title: 'Healthpro Plus'
			cycle: 'monthly'
			price: 36
			color: 'blue'
		6:
			id: 6
			slug: 'healthpro_plus'
			title: 'Healthpro Plus'
			cycle: 'annual'
			price: 360
			color: 'blue'

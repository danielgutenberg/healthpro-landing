apiRoutes =
	#wizard
	'wizard_endpoint': 'wizard/{wizard}/{step}'
	'api-wizards': 'wizards'

	#profile
	'ajax-profiles-self': 'profiles/me'
	'ajax-switch-profile': 'profiles/switch-profile/{id}'
	'ajax-auth-profile': 'auth/profile/{profileId}'
	'ajax-profiles': 'profiles'
	'ajax-profile': 'profiles/{id}'
	'ajax-profiles-get-simple': 'profiles/{id}/simple'
	'ajax-profiles-upload-file': 'profiles/{id}/files'
	'ajax-profiles-delete-file': 'profiles/file/{id}'
	'ajax-profiles-upload-asset': 'profiles/{id}/asset'
	'ajax-profiles-delete-asset': 'profiles/{id}/asset/{assetId}'
	'ajax-profiles-update-asset': 'profiles/{id}/asset/{assetId}'
	'ajax-profiles-exists': 'profiles/exists/client/{email}'

	#clients profile
	'ajax-profile-notes': 'profiles/{profileId}/notes/{elmType}/{elmId}'
	'ajax-profile-notes-update': 'profiles/{profileId}/notes/{elmType}/{elmId}/{noteId}'
	'ajax-profile-notes-delete': 'profiles/{profileId}/notes/{elmType}/{elmId}/{noteId}'

	# photos/videos/certificates/articles/podcasts/files
	'ajax-profile-assets': 'profiles/{profileId}/{assetsType}'
	'ajax-profile-asset': 'profiles/{profileId}/{assetType}/{assetId}'

	'ajax-profiles-available-tags': 'profiles/available-tags'
	'ajax-provider': 'providers/{providerId}'
	'ajax-provider-full-get': 'providers/{providerId}/full'
	'ajax-provider-service-types': 'providers/services'
	'ajax-provider-services': 'providers/{providerId}/services'
	'ajax-provider-service-add': 'providers/{providerId}/services'
	'ajax-profile-provider-services': 'providers/{providerId}/services'
	'ajax-provider-availabilities': 'providers/{providerId}/availabilities'
	'ajax-provider-availability-get': 'providers/{providerId}/availabilities'
	'ajax-provider-availability-block': 'providers/me/availabilities'
	'ajax-provider-availability-get-blocked': 'providers/me/blocked'
	'ajax-provider-availability-update-blocked': 'providers/me/registrations/{registrationId}'
	'ajax-provider-service': 'providers/{providerId}/services/{serviceId}'
	'ajax-provider-service-delete': 'providers/{providerId}/services/{serviceId}'
	'ajax-provider-service-update': 'providers/{providerId}/services/{serviceId}'
	'ajax-provider-service-session-delete': 'providers/{providerId}/servicesessions/{serviceSessionId}'
	'ajax-provider-service-session-package-delete': 'profiles/{providerId}/packages/{packageId}'
	'ajax-provider-package': 'profiles/{providerId}/packages/{packageId}'
	'ajax-provider-import-csv': 'profiles/{providerId}/import/csv'
	'ajax-provider-import-list': 'profiles/{providerId}/import/list'
	'ajax-provider-toggle-client-meta': 'providers/{providerId}/clients/{clientId}/meta/toggle'

	'ajax-profiles-upload-asset-certificate': 'profiles/{id}/certificate'
	'ajax-profiles-update-asset-certificate': 'profiles/{id}/certificate/{assetId}'
	'ajax-profiles-delete-asset-certificate': 'profiles/{id}/certificate'

	#signup - auth
	'ajax-auth-sign-up': 'auth/register'
	'ajax-auth-login': 'auth/login'
	'ajax-auth-reset-password': 'auth/reset'
	'ajax-auth-set-password': 'auth/set_password'
	'ajax-auth-send-activation': 'auth/send-activation'
	'ajax-auth-check-email': 'profiles/exists/provider/{email}'

	#locations
	'ajax-provider-locations': 'providers/{providerId}/locations'
	'ajax-location-add': 'providers/{providerId}/locations'
	'ajax-location-get': 'providers/{providerId}/locations'
	'ajax-location-types-get': 'core/locations/types'
	'ajax-location-countries-get': 'core/locations/countries'
	'ajax-location-availabilities-add': 'providers/{providerId}/locations/{locationId}/availabilities'
	'ajax-location-availabilities-get': 'providers/{providerId}/locations/{locationId}/availabilities'
	'ajax-location-availabilities-update': 'providers/{providerId}/locations/{locationId}/availabilities'
	'ajax-location-remove': 'providers/{providerId}/locations/{locationId}'
	'ajax-location-update': 'providers/{providerId}/locations/{locationId}'
	'ajax-location-check-radius': 'locations/check-radius/{locationId}/{homeLocationId}'


	# taxonomy
	'ajax-taxonomy-tags': 'core/taxonomy/{taxonomySlug}/tags'
	'ajax-tags-child-tags': 'tags/{tagId}'
	'ajax-core-education-search': 'core/educations/search'

	# classes
	'api-profile-classes-get': 'profile/{profileId}/classes'
	'api-profile-class-create': 'classes/'
	'api-profile-class-update': 'classes/{id}'
	'api-profile-class-delete': 'classes/{id}'
	'api-profile-class-asset-upload': 'classes/{classId}/asset'
	'api-profile-class-asset-update': 'classes/{classId}/asset/{id}'
	'api-profile-class-asset-delete': 'classes/{classId}/asset/{id}'
	'api-profile-class-tags-associate': 'classes/{classId}/tags'
	'api-profile-class-schedule-add': 'classes/{classId}/schedule'
	'api-profile-class-schedule-day': 'classes/{classId}/schedule/{id}'
	'api-profile-class-packages': 'classes/{classId}/packages'
	'api-profile-class-package': 'classes/{classId}/packages/{packageId}'

	'api-profile-class-upcoming': 'clients/{clientId}/classes'
	'api-provider-classes-get': 'provider/{providerId}/classes'

	# profile wizard
	'api-wizard-profile-initial': 'wizard/profile_setup'
	'api-wizard-profile-step': 'wizard/profile_setup/{step}'

	# booking wizard
	'api-wizard-booking-initial': 'wizard/booking'
	'api-wizard-booking-step': 'wizard/booking/{step}'

	# booking wizard
	'api-wizard-recurring-initial': 'wizard/recurring'
	'api-wizard-recurring-step': 'wizard/recurring/{step}'

	# gift certificate wizard
	'api-wizard-gift-certificate-initial': 'wizard/gift_certificate'
	'api-wizard-gift-certificate-step': 'wizard/gift_certificate/{step}'

	'ajax-wizard-store-registrations': 'wizard'

	# orders
	'api-orders': 'orders'
	'api-order': 'orders/{orderId}'
	'api-order-book': 'orders/{orderId}/book'

	# packages
	'ajax-profile-package-credits': 'profiles/{profileId}/packages/credits/{entityType}/{entityId}'
	'ajax-profile-active-packages': 'profiles/{profileId}/packages'


	# coupons
	'ajax-coupon-get': 'coupons/{coupon}'

	# gift certificates
	'ajax-gift-certificates' : 'gift-certificates'
	'ajax-gift-certificate'  : 'gift-certificates/{certificateId}'

	# appointments
	'ajax-appointments': 'appointments'
	'ajax-appointment': 'appointments/{appointmentId}'
	'ajax-appointment-location': 'appointments/{appointmentId}/location'
	'ajax-provider-appointments': 'providers/{providerId}/appointments'
	'ajax-provider-appointment': 'appointments/{appointmentId}'
	'ajax-appointment-confirm': 'appointments/{appointmentId}/confirm'
	'ajax-appointment-mark': 'appointments/{appointmentId}/mark'
	'ajax-appointment-notify': 'appointments/{appointmentId}/notify'
	'ajax-appointment-payment-options': 'appointments/{appointmentId}/paymentOptions'
	'ajax-provider-payment-options': 'providers/{providerId}/paymentOptions'

	# provider's clients data
	'ajax-provider-client-history': 'providers/{providerId}/clients/{clientId}/appointments'
	'ajax-provider-client-meta-get': 'providers/{providerId}/clients/{clientId}/meta'
	'ajax-provider-client-meta-toggle': 'providers/{providerId}/clients/{clientId}/meta/toggle'

	'ajax-provider-clients': 'providers/{providerId}/clients'
	'ajax-provider-can-invite': 'providers/{providerId}/can-invite/{clientId}'

	'ajax-provider-client-packages': 'providers/me/clients/{clientId}/packages'

	'ajax-provider-plan-estimate': 'providers/{providerId}/membership/estimate'

	'ajax-client-providers': 'clients/{clientId}/providers'
	'ajax-client-provider': 'clients/{clientId}/providers/{providerId}'
	'ajax-client-provider-status': 'clients/{clientId}/providers/{providerId}/status'

	'ajax-client-locations': 'clients/{clientId}/locations'
	'ajax-client-location': 'clients/{clientId}/locations/{locationId}'

	'ajax-packages': 'packages'
	'ajax-package': 'packages/{packageId}'
	'ajax-package-status': 'packages/{packageId}/status'
	'ajax-package-active': 'packages/{packageId}/active'
	'ajax-package-schedule': 'packages/{packageId}/schedule/{scheduleId}'
	'ajax-package-schedule-add': 'packages/{packageId}/schedule'
	'ajax-package-can-purchase': 'packages/can-purchase/{sessionId}'

	# invite client
	'ajax-invite-client': 'providers/me/clients/{clientId}/invite'
	'ajax-reinvite-client': 'providers/{providerId}/clients/{clientId}/invite'

	# transactions
	'ajax-transactions' : 'profiles/me/transactions'

	# cards
	'ajax-credit-cards': 'profiles/me/credit_cards'
	'ajax-credit-card': 'profiles/me/credit_cards/{creditCardId}'

	# bank accounts
	'ajax-bank-accounts': 'profiles/me/bank_accounts'
	'ajax-bank-account': 'profiles/me/bank_accounts/{bankAccountId}'

	'ajax-payment-options': 'profiles/me/payment_options'

	# credit
	'ajax-credit-balance': 'profiles/me/credit_balance'

	# trial
	'ajax-commission-free-period': 'profiles/me/commission_free_period'

	# phones
	'ajax-profile-add-phone': 'profiles/me/phones'
	'ajax-profile-verify-phone': 'profiles/me/phones/{phoneId}/verify',
	'ajax-profile-resend-verify-phone': 'profiles/me/phones/{phoneId}/resend',
	'ajax-profile-edit-phone': 'profiles/me/phones/{phoneId}'

	# billing and membership
	'ajax-billing-details': 'profiles/{profile_id}/billing_details'
	'ajax-provider-membership-details': 'providers/{providerId}/membership'
	'ajax-provider-membership-change': 'providers/{providerId}/membership/change'
	'ajax-provider-membership-cancel': 'providers/{providerId}/membership/cancel'
	'ajax-provider-membership-revive': 'providers/{providerId}/membership/revive'
	'ajax-provider-accept-methods-update': 'providers/{providerId}/acceptPayment'

	# billing history
	'ajax-order-billing-get': 'profiles/me/billing'

	# payments
	'ajax-payment-methods': 'profiles/me/payment_methods'
	'ajax-paypal-delete': 'profiles/me/paypal/{paypalId}'
	'ajax-stripe-delete': 'profiles/me/stripe'

	# search
	'providers-search': 'providers/search?{query}'
	'classes-search': 'classes/search?{query}'

	# locations for autocomplete
	'api-core-locations-search': 'core/locations/search'

	# services for autocomplete
	'api-core-services-search': 'core/services/search'

	# names for autocomplete
	'professionals-names-search': 'profiles/professionals/search/name'

	# reviews
	'provider-review-helpful-set': 'providers/{providerId}/reviews/{reviewId}/helpful'
	'provider-review' : 'providers/{providerId}/reviews'

	# pages
	'ajax-pages': 'pages/{slug}/'

	#conversations
	'ajax-conversations': 'conversations'
	'ajax-conversation-messages': 'conversations/{conversationId}'
	'ajax-core-conversations-receivers': 'core/conversations/receivers'

	# notifications
	'ajax-notifications-inbox': 'notifications/{profileId}/inbox'
	'ajax-notifications-inbox-read' : 'notifications/{profileId}/inbox/read'
	'ajax-notification-inbox-read' : 'notifications/{profileId}/inbox/{notificationId}/read'

	# syncing calendar
	'ajax-calendar-sync': 'schedule/calendar/sync/{driver}'
	'ajax-calendar-calendars': 'schedule/calendar/calendars'
	'ajax-calendar-connect': 'schedule/calendar/connect/{remoteId}'
	'ajax-calendar-disconnect': 'schedule/calendar/disconnect/{calendarId}'
	'ajax-calendar-mark': 'schedule/calendar/mark/{calendarId}'
	'ajax-oauth-disconnect': 'oauth/{oauthId}/disconnect'

	# user account
	'user-account': 'user/me'

	# refresh session
	'ajax-refresh-session': 'auth/caffeine'

	# invites
	'ajax-clients-invites-get': 'clients/{clientId}/invites'
	'ajax-provider-invites-get': 'providers/{providerId}/invites'
	'ajax-client-provider-confirm': 'clients/{clientId}/providers/{providerId}'
	'ajax-client-provider-decline': 'clients/{clientId}/providers/{providerId}?decline=1'
	'ajax-client-provider-add': 'clients/{clientId}/providers/{providerId}/add'

	# payments
	'ajax-payments-refund': '/payments/refund/{operationId}'

	# paypal
	'ajax-paypal-process': 'paypal/process'
	'ajax-paypal-complete': 'paypal/complete'

	# cs forms
	'ajax-cs-add-service-request': 'cs-requests/add-service-request'
	'ajax-cs-demo-request': 'cs-requests/demo-request'
	'ajax-cs-partner-request': 'cs-requests/partner-request'
	'ajax-cs-affiliate-request': 'cs-requests/affiliate-request '
	'ajax-cs-contact': 'cs-requests/contact '
	'ajax-cs-newsletter': 'cs-requests/newsletter '


###*
 * [getApiRoute description]
 * @param {string} routeName name of route
 * @param {object} params    object with parameters, which will set in route
 * example: getApiRoute('ajax-profiles-update-asset', {'id': 5, 'assetId': 10})
###
getApiRoute = (routeName, params) ->
	route = apiRoutes[routeName]
	if params?
		for key, value of params
			route = route.replace('{'+key+'}', value)

	return '/ajax/v1/' + route

module.exports = getApiRoute

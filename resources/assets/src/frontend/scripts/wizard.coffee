HP_App = require 'app/app'

class HP_Wizard
	constructor: ->
		@app = new HP_App 'wizard'

		@$wizard = $('[data-wizard]')

		@initWizard()

	initWizard: ->

		switch @$wizard.data('wizard')
			when 'profile'
				require.ensure [], ->
					require 'app/modules/wizard_professional/wizard_professional'

			when 'booking'
				require.ensure [], ->
					require 'app/modules/wizard_booking/wizard_booking'

			when 'recurring'
				require.ensure [], ->
					require 'app/modules/wizard_recurring/wizard_recurring'

			when 'gift_certificate'
				require.ensure [], ->
					require 'app/modules/wizard_gift_certificate/wizard_gift_certificate'

new HP_Wizard()

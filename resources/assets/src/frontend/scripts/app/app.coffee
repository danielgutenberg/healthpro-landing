vexDialog = require 'vexDialog'

class HP_App
	constructor: ->
		if window.GLOBALS._PID
			@initLogged()

		@initPopups()
		@initFooter()
		@initAlerts()

	initLogged: ->
		# init user menu
		$userMenu = $('[data-usermenu]')
		if $userMenu.length
			UserMenu = require 'app/blocks/user_menu'
			new UserMenu $userMenu

		# init session
		RefreshSession = require 'utils/refresh_session'
		@refreshSession = new RefreshSession()

	initPopups: ->
		# init auth popup
		$authPopup = $('[data-authpopup]')
		if $authPopup.length
			AuthPopup = require 'app/blocks/auth_popup'
			window.authPopup = new AuthPopup $authPopup

	initFooter: ->
		# init feedback form
		require 'app/blocks/hubspot/feedback'
		# init newsletter form
		require 'app/blocks/hubspot/newsletter'

	initAlerts: ->
		return unless window.DISPLAY_ALERT
		vexDialog.alert
			buttons: [$.extend({}, vexDialog.buttons.YES, {text: 'Ok'})]
			message: window.DISPLAY_ALERT

module.exports = HP_App

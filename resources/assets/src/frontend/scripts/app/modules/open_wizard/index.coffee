getApiRoute = require 'hp.api'
dateTimeParsers = require 'utils/datetime_parsers'
WizardAuth = require 'helpers/wizard_auth'
require 'app/modules/booking_helper'

class OpenBookingWizard
	constructor: () ->

		@appointmentId = window.GLOBALS.APPOINTMENT_ID
		if window.GLOBALS.SHOW_BOOKING_WIZARD
			$.when(
				@getAppointment()
			).then(
				=> @getProviderServices()
			).then(
				=> @openWizard()
			)

	getAppointment: ->
		$.ajax
			url: getApiRoute('ajax-appointment', {appointmentId: @appointmentId})
			method: 'get'
			success: (res) =>
				@providerId = res.data.provider.id
				@setBookingData res.data

	getProviderServices: ->
		$.ajax
			url: getApiRoute('ajax-profile-provider-services', {providerId: @providerId})
			method: 'get'
			success: (res) =>
				@setServices res.data

	openWizard: ->
		bookingData = @bookingData
		openWizard = ->
			window.bookingHelper.openWizard
				bookingData: bookingData
		WizardAuth openWizard
		@

	setBookingData: (data) ->
		@bookingData =
			appointment_id: @appointmentId
			date: dateTimeParsers.parseToFormat(data.appointment.date, 'YYYY-MM-DD')
			from: data.appointment.date
			appointment_from_professional: true
			resetToFirstStep: 'recalculate'
			client_id: window.GLOBALS._PID
			provider_id: @providerId
			location_id: data.location.id
			services: []
			selected_time:
				from: data.appointment.date
				location_id: data.location.id
				location_name: data.location.name
				service_name: data.appointment.service_name
				service_id: data.appointment.service_id
				duration: data.appointment.duration
				price: parseFloat(data.appointment.price)
				appointment_id: @appointmentId


	setServices: (services) ->
		_.each services, (service) =>
			ser =
				service_id: service.service_id
				service_type_id: service.service_type_id
				slug: service.slug
				name: service.name
				sessions: []
				locations: []

			_.each service.sessions, (session) =>
				ser.sessions.push
					session_id: session.session_id
					duration: session.duration
					price: session.price
					packages: session.packages

				selectedTime = @bookingData.selected_time
				if service.service_id == selectedTime.service_id and session.duration == selectedTime.duration and parseFloat(session.price) == selectedTime.price
					@bookingData.selected_time.session_id = session.session_id

			_.each service.locations, (location) ->
				ser.locations.push
					id: location.id
					name: location.name
					service_area: location.service_area
					address: location.address

			@bookingData.services.push ser

new OpenBookingWizard()

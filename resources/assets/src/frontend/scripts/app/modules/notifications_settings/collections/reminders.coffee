Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Moment = require 'moment'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		super
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			success: (res) =>
				if res.data?.reminders? then _.forEach res.data.reminders, (reminder) =>
					@add reminder
				@trigger 'data:ready'


module.exports = Collection

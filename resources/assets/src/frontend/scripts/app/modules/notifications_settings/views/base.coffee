Backbone = require 'backbone'
Validation = require 'backbone.validation'
Stickit = require 'backbone.stickit'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'

class View extends Backbone.View

	initialize: (options) ->
		super
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins

		@setOptions options
		@reminders = []

		if @$el.data('profile-type') is 'client'
			@render()
			@cacheDom()
			@addListeners()
			@bind()

	addListeners: ->
		@listenTo @collections.reminders, 'add', @renderReminder
		@listenTo @collections.reminders, 'data:ready', =>
			@hideLoading()

	bind: ->
		@$el.$submit.on 'click', (e) =>
			e.preventDefault()
			@submitReminders()

	render: ->
		@$el.html NotificationsSettings.Templates.Base
		@

	cacheDom: ->
		@$el.$loading = @$el.find('.loading_overlay')
		@$el.$listing = @$el.find('[data-reminders-listing]')
		@$el.$submit = @$el.find('[data-submit]')

	showLoading: ->
		@$el.$loading.removeClass('m-hide')

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

	renderReminder: (model) ->
		@reminders.push new NotificationsSettings.Views.Reminder
			parentView: @
			model: model
			collections: @collections

	submitReminders: () ->
		@showLoading()
		$.when(@model.save()).then(
			(response) =>
				@hideLoading()
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: NotificationsSettings.Config.messages.updateSuccess

			, (error) =>
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: NotificationsSettings.Config.messages.updateError
				@hideLoading()

		)

module.exports = View

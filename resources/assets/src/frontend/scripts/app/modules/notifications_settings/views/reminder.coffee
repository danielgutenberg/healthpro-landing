Backbone = require 'backbone'
Validation = require 'backbone.validation'
Stickit = require 'backbone.stickit'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	tagName: 'li'
	className: 'm-reminders'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins
		@setOptions options

		@addStickit()
		@render()

	addStickit: ->
		@bindings =
			'[name="email_appointment_reminder"]':
				observe: 'is_active'
				onSet: (val) ->
					if val?
						@model.set('value', 1) unless @model.get('value')?
					val

			'[name="email_appointment_reminder_time"]':
				observe: 'value'
				selectOptions:
					collection: NotificationsSettings.Config.timeOptions
					labelPath: 'label'
					valuePath: 'value'
				onGet: (val) ->
					val unless val is -1 or val is null
				attributes: [
					name: 'disabled'
					observe: 'is_active'
					onGet: (val) ->
						!val
				]

	render: ->
		@$el.html NotificationsSettings.Templates.Reminder
		@parentView.$el.$listing.append @$el
		@stickit()
		@

module.exports = View

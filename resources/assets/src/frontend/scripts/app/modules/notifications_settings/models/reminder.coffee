Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	initialize: () ->
		@setActive()

	setActive: ->
		if @get('value') is -1 or @get('value') is null
			@set 'is_active', false
		else
			@set 'is_active', true

module.exports = Model

Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	initialize: (options) ->
		@collections = options.collections

	save: ->
		$.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'put'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				reminders: @prepareData()
				_token: window.GLOBALS._TOKEN

	prepareData: ->
		reminders = []
		_.each @collections.reminders.models, (model) =>
			reminders.push
				id: model.get('id')
				value: if model.get('is_active') then model.get('value') else -1

		reminders

module.exports = Model

Handlebars = require 'handlebars'

# views
BaseView = require './views/base'
ReminderView = require './views/reminder'

# templates
BaseTemplate = require 'text!./templates/base.html'
ReminderTemplate = require 'text!./templates/reminder.html'

# models
BaseModel = require './models/base'
ReminderModel = require './models/reminder'

#require ''

# Collections
RemindersCollection = require './collections/reminders'

window.NotificationsSettings =
	Config:
		timeOptions: [
			{
				label: '1 hour'
				value: 1
			}
			{
				label: '6 hours'
				value: 6
			}
			{
				label: '12 hours'
				value: 12
			}
			{
				label: '24 hours'
				value: 24
			}
		]
		messages:
			hasChanges: "All changes will be lost. Continue?"
			updateSuccess: 'Your changes have been saved'
			updateError: 'Something went wrong. Please reload the page and try again'
	Views:
		Base: BaseView
		Reminder: ReminderView
	Templates:
		Base: BaseTemplate
		Reminder: ReminderTemplate
	Models:
		Base: BaseModel
		Reminder: ReminderModel
	Collections:
		Reminders: RemindersCollection

class NotificationsSettings.App
	constructor: (options) ->
		@collections =
			reminders: new NotificationsSettings.Collections.Reminders [],
				model: NotificationsSettings.Models.Reminder

		@model = new NotificationsSettings.Models.Base
			collections: @collections

		@view = new window.NotificationsSettings.Views.Base
			$el: options.$el
			model: @model
			collections: @collections

if $('[data-notifications-settings]').length
	NotificationsSettings.app = new NotificationsSettings.App
		$el: $('[data-notifications-settings]')

module.exports = NotificationsSettings

Backbone = require 'backbone'
Handlebars = require 'handlebars'

getApiRoute = require 'hp.api'

userpicHtml = require 'text!./templates/block.html'
userpicMyProfileHtml = require 'text!./templates/block-my_profile.html'

class UserpicModule extends Backbone.View

	template: Handlebars.compile userpicHtml
	template_myprofile: Handlebars.compile userpicMyProfileHtml

	initialize: (@$appendTo, @options) ->
		@render()

	render: ->
		data =
			"_token": window.GLOBALS._TOKEN
			"uploadUrl": getApiRoute('ajax-profiles-upload-file', {'id': @profileId})

		if @options.type? is 'my_profile'
			template = @template_myprofile
		else
			template = @template

		$(template(data)).appendTo @$appendTo

		@trigger 'rendered'

		return @

module.exports = UserpicModule

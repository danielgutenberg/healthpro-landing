Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'

# models
BaseModel = require './models/base'

# collections

# templates
BaseTmpl = require 'text!./templates/base.html'


window.ProfileInvites =
	Models:
		Base: BaseModel
	Views:
		Base: BaseView
	Templates:
		Base: Handlebars.compile BaseTmpl

_.extend ProfileInvites, Backbone.Events

class ProfileInvites.App
	constructor: (options) ->
		@eventBus = _.extend {}, Backbone.Events

		@model = new ProfileInvites.Models.Base()
		@view = new ProfileInvites.Views.Base
			el: $('[data-profile-invites]')
			model: @model
			eventBus: @eventBus

		return {
			view: @view
			model: @model
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

if $('[data-profile-invites]').length and window.GLOBALS.PROVIDER_ID and window.GLOBALS._PTYPE is 'client'
	new ProfileInvites.App

module.exports = ProfileInvites

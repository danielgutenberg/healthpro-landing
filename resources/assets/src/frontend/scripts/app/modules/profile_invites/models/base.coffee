Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		is_clients_pro: false
		invites_client: false

	initialize: (@options)->
		@checkClientsPro() # first we check if it's already my pro

	checkClientsPro: ->
		@trigger 'data:loading'
		$.ajax
			url: getApiRoute('ajax-client-provider-status', { 'clientId': 'me', 'providerId': window.GLOBALS.PROVIDER_ID })
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				@.set 'is_clients_pro', response.data.status == 'approved' # it is client's pro
				@.set 'invites_client', response.data.status == 'invited' # sent invite to the client
				@trigger 'data:ready'

			error: (response) =>
				console.log response

	confirmInvite: ->
		@trigger 'data:loading'
		$.ajax
			url: getApiRoute('ajax-client-provider-confirm', { 'clientId': 'me', 'providerId': window.GLOBALS.PROVIDER_ID })
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: (response) =>
				@.set 'is_clients_pro', true
				@.set 'invites_client', false
				@trigger 'data:ready'

	declineInvite: ->
		vexDialog.confirm
			message: 'Are you sure you want to decline professional\'s invitation? <p>You will still be able to book appointments with this professional in the future.</p>'
			callback: (value) =>
				return unless value
				@trigger 'data:loading'
				$.ajax
					url: getApiRoute('ajax-client-provider-decline', { 'clientId': 'me', 'providerId': window.GLOBALS.PROVIDER_ID })
					method: 'put'
					type: 'json'
					contentType: 'application/json; charset=utf-8'
					data: JSON.stringify
						_token: window.GLOBALS._TOKEN
					success: (response) =>
						@.set 'is_clients_pro', false
						@.set 'invites_client', false
						@trigger 'data:ready'

	addClientProfessional: ->
		$.ajax
			url: getApiRoute('ajax-client-provider-add', { 'clientId': 'me', 'providerId': window.GLOBALS.PROVIDER_ID })
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: (response) =>
				@.set 'is_clients_pro', true
				@.set 'invites_client', false

module.exports = Model

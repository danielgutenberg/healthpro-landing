Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View
	events:
		'click [data-action-confirm]': 'confirmInvite'
		'click [data-action-decline]': 'declineInvite'
		'click [data-action-add]': 'addClientProfessional'

	initialize: (@options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions(@options)

		@cacheDom()
		@showLoading()
		@addListeners()

	addListeners: ->
		@listenTo @model, 'data:loading', ->
			@showLoading()

		@listenTo @model, 'data:ready', ->
			@render()
			@hideLoading()

	cacheDom: ->
		@$el.confirm = $('[data-action-confirm]', @$el)
		@$el.decline = $('[data-action-decline]', @$el)
		@$el.add = $('[data-action-add]', @$el)
		@$container = @$el.parents('[data-profile-invites-container]')
		@$el.loading = $('.profile_card--invitations--loading', @$container)

	render: ->
		@$el.html ProfileInvites.Templates.Base
			'is_clients_pro': @model.get 'is_clients_pro'
			'invites_client': @model.get 'invites_client'
		@

	showLoading: ->
		@$el.loading.removeClass 'm-hide'

	hideLoading: ->
		@$el.loading.addClass 'm-hide'

	confirmInvite: ->
		@showLoading()
		$.when( @model.confirmInvite() )
			.then( =>
				@render()
				@hideLoading()
				@showReviewButton()
				)

	declineInvite: ->
		@showLoading()
		$.when( @model.declineInvite() )
			.then( =>
				@render()
				@hideLoading()
				)

	addClientProfessional: ->
		@showLoading()
		$.when( @model.addClientProfessional() )
			.then( =>
				@render()
				@hideLoading()
				@showReviewButton()
				)

	showReviewButton: ->
		if $('[data-review-professional]').length
			$('[data-review-professional]').removeClass 'm-hide'

module.exports = View

Backbone = require 'backbone'
Handlebars = require 'handlebars'

#	# list of all instances
window.BookingHelper = BookingHelper =
	Views     :
		Base : require('./views/base')
	Templates :
		Base  : Handlebars.compile require('text!./templates/base.html')
		Popup : Handlebars.compile require('text!./templates/popup.html')

# events bus
_.extend BookingHelper, Backbone.Events

class BookingHelper.App
	constructor : ->
		@view = new BookingHelper.Views.Base()

	openWizard                : (options = {}) -> @view.openWizard options
	openWizardRecurring       : (options = {}) -> @view.openWizardRecurring options
	openWizardGiftCertificate : (options = {}) -> @view.openWizardGiftCertificate options

window.bookingHelper = new BookingHelper.App() unless window.bookingHelper

module.exports = BookingHelper

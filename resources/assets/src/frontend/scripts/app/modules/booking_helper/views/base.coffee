Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

BookingWizard = require 'app/modules/wizard_booking/wizard_booking'
RecurringWizard = require 'app/modules/wizard_recurring/wizard_recurring'
GiftCertificateWizard = require 'app/modules/wizard_gift_certificate/wizard_gift_certificate'

class View extends Backbone.View

	className : 'booking_helper'
	events    :
		'click .booking_helper--opener' : 'openWizard'

	initialize : ->
		Cocktail.mixin @, Mixins.Views.Base

		@wizardInited = false
		@loading = false

	renderPopup : (wizardType = 'booking') ->
		return if @isWizardRendered()

		bookingWizards = ['booking', 'recurring']
		isBookingWizard = _.contains(bookingWizards, wizardType)

		bookingTypeWizards = ['booking', 'recurring', 'gift_certificate']
		isBookingTypeWizard = _.contains(bookingTypeWizards, wizardType)

		params =
			wizard_type         : wizardType
			wizard_classes      : if isBookingTypeWizard then 'wizard_booking' else "wizard m-#{wizardType}"
			popup_classes       : if isBookingTypeWizard then 'popup wizard_booking_popup' else "popup popup_wizard"
			popup_name          : if isBookingWizard then 'wizard_booking_popup' else 'popup_wizard'
			render_close_button : !isBookingTypeWizard

		@$popup = $(BookingHelper.Templates.Popup(params)).appendTo('body')
		window.popupsManager.addPopup @$popup
		@

	renderLoading : ->
		return if @loading
		@$loading = $('<div class="loading_overlay m-light m-fixed"><span class="spin m-logo"></span></div>').appendTo('body')
		$('body').addClass 'm-fixed'
		@loading = true
		@

	initWizard : (options = {}) ->
		# that way we'll have the instance straight away
		# make sure we clean up the wizard before initing
		Wizard.close().init
			el          : @getWizardEl(options)
			bookingData : options.bookingData ? null
			forcePage   : options.forcePage ? false

	getWizardEl : (options) ->
		forcePage = options.forcePage ? false
		if forcePage
			return $('<div data-wizard></div>').appendTo $('<div class="popup"></div>') # create fake popup
		else
			return @$popup.find('[data-wizard]')

	isWizardRendered : -> $('[data-wizard="booking"]').length or $('[data-wizard="recurring"]').length

	openWizard : (options) ->
		Wizard?.close()
		window.Wizard = BookingWizard.get()
		window.Wizard.reset()

		if options.forcePage
			@openWizardOnPage(options)
		else
			@openWizardInPopup 'booking', options
		@

	openWizardRecurring : (options) ->
		Wizard?.close()
		window.Wizard = RecurringWizard.get()
		window.Wizard.reset()

		if options.forcePage
			@openWizardOnPage(options)
		else
			@openWizardInPopup 'recurring', options
		@

	openWizardGiftCertificate : (options) ->
		Wizard?.close()
		window.Wizard = GiftCertificateWizard.get()
		window.Wizard.reset()

		if options.forcePage
			@openWizardOnPage(options)
		else
			@openWizardInPopup 'gift_certificate', options
		@

	openWizardOnPage : (options) ->
		@renderLoading()
		@initWizard options
		@

	openWizardInPopup : (popupName, options) ->
		@renderPopup popupName
		@initWizard options
		Wizard.showWizard()
		@

module.exports = View

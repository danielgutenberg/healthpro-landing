Abstract = require('./abstract')

Numeral = require 'numeral'
Formats = require 'config/formats'
HumanTime = require 'utils/human_time'
LocationFormatter = require 'utils/formatters/location'

require 'tooltipster'

class View extends Abstract

	className : 'client_package m-recurring'

	initialize : ->
		super

		@events['click [data-schedule-appointments-more]'] = 'toggleMoreScheduledAppointments'
		@events['click [data-schedule-toggle]'] = 'toggleScheduleView'

		@delegateEvents()

	cacheDom : ->
		super
		@$el.$appointment = @$el.find('[data-schedule-appointment]')
		@$el.$schedule = @$el.find('[data-schedule]')
		@$el.$schedule.$toggle = @$el.find('[data-schedule-toggle]')
		@$el.$schedule.$add = @$el.find('[data-schedule-appointment-add-time]')

		@

	afterRender : ->
		super
		@displayScheduledAppointments(3)
		@displayScheduleView 'times'

		@$el.find('[data-tooltip]').tooltipster
			theme    : 'tooltipster-dark'
			maxWidth : 200

		@

	bind : ->
		super
		@bindings['[data-expires-on]'] =
			observe : 'active'
			onGet   : (val) =>
				if val
					date = @model.get('expires_on')
				else
					date = @model.get('appointments').getLastAppointment()?.get('from')
				date.format('MMMM D, YYYY')
		@bindings['[data-package-canceled]'] =
			classes :
				'm-hide' :
					observe : 'active'
					onGet   : (val) -> val
		@bindings['[data-package-cancel]'] =
			classes :
				'm-hide' :
					observe : 'active'
					onGet   : (val) -> !val
		@

	getTemplate : -> ProfessionalPackages.Templates.Package.Recurring

	getTemplateData : ->
		data = super

		data.package =
			price                  : @model.get('price')
			price_label            : Numeral(@model.get('price')).format(Formats.Price.Simple) + " / month"
			max_visits             : @model.get('number_of_visits')
			max_visits_label       : do =>
				numberOfVisits = @model.get('number_of_visits')
				return 'unlimited visits per week' unless numberOfVisits
				"#{numberOfVisits} visit" + (if numberOfVisits > 1 then "s" else "") + " per week"
			duration               : @model.get('duration')
			duration_label         : HumanTime.minutes(@model.get('duration')) + " session"
			number_of_months       : @model.get('number_of_months')
			number_of_months_label : do =>
				numberOfMonths = @model.get('number_of_months')
				"#{numberOfMonths} month" + (if numberOfMonths > 1 then "s" else "")
			expires_on             : @model.get('expires_on').format('MMMM D, YYYY')
			auto_renew             : @model.get('auto_renew')
			cancelled              : !@model.get('active')

		data.appointments = @model.get('appointments').map (model) ->
			{
				location_name          : LocationFormatter.getLocationName model.get('location')
				location_type_label    : LocationFormatter.getLocationTypeLabel model.get('location')
				location_type          : model.get('location').location_type
				location_is_home_visit : model.get('location').location_type is 'home-visit'
				weekday                : model.get('date').format('dddd')
				date                   : model.get('date').format('MMMM D, YYYY')
				from                   : model.get('from').format('hh:mm a')
			}

		data.times = @model.get('schedule').getSchedule().map (model) ->
			{
				id      : model.get('id')
				weekday : model.get('date').format('dddd')
				from    : model.get('date').format('hh:mm a')
			}

		data

	toggleMoreScheduledAppointments : (e) ->
		e?.preventDefault()

		$btn = $(e.currentTarget)
		if $btn.data('more')
			@displayScheduledAppointments 3
			$btn.data('more', 0).text $btn.data('label-more')
		else
			@displayScheduledAppointments -1
			$btn.data('more', 1).text $btn.data('label-less')
		@

	displayScheduledAppointments : (amount = -1) ->
		if amount < 0
			@$el.$appointment.removeClass 'm-hide'
			return @

		@$el.$appointment.addClass 'm-hide'
		@$el.$appointment.slice(0, amount).removeClass 'm-hide'
		@

	toggleScheduleView : (e) ->
		e?.preventDefault()
		$btn = $(e.currentTarget)
		@displayScheduleView $btn.data('view') ? 'times'

	displayScheduleView : (view = 'times') ->
		return @ if @$el.$schedule.hasClass "m-#{view}"
		switch view
			when 'times'
				@$el.$schedule.removeClass('m-appointments').addClass('m-times')
				@$el.$schedule.$toggle.data('view', 'appointments').text @$el.$schedule.$toggle.data('label-times')
			when 'appointments'
				@$el.$schedule.removeClass('m-times').addClass('m-appointments')
				@$el.$schedule.$toggle.data('view', 'times').text @$el.$schedule.$toggle.data('label-appointments')
				@displayScheduledAppointments 3
		@

	reload : ->
		@showLoading()
		$.when(
			@model.load()
		).then(
			=>
				@hideLoading()
				@reRender()
		).fail(
			=> @hideLoading()
		)
		@

module.exports = View

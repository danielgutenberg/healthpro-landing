Backbone = require 'backbone'
getApiRoute = require 'hp.api'
datetimeParsers = require 'utils/datetime_parsers'

class Collection extends Backbone.Collection

	initialize : ->
		@getData()

	getFilterOptions : -> [
		{
			value : 'active', label : 'Active Packages'
		}
		{
			value : 'finished', label : 'Expired Packages'
		}
	]

	getData : (options = {}) ->
		@reset() if options.reset
		@trigger 'data:loading'

		$.ajax
			url         : getApiRoute('ajax-packages')
			method      : 'get'
			type        : 'json'
			data        : {}
			contentType : 'application/json; charset=utf-8'
			success     : (response) =>
				_.each response.data, (item) => @push @formatPackage(item)
				@trigger 'data:ready'
		@

	formatPackage : (item) ->
		{
			id                 : item.id
			provider           : item.provider
			client             : item.client
			appointments       : item.appointments
			entities_left      : item.entities_left
			expires_on         : datetimeParsers.parseToMoment(item.expires_on)
			number_of_entities : item.number_of_entities
			package_id         : item.package_id
			price              : item.price
			schedule           : item.schedule
			service            : item.service
			type               : if item.number_of_entities > -1 then 'standard' else 'recurring'
			auto_renew         : item.auto_renew and item.active
			active             : item.active ? 0
		}

module.exports = Collection

Abstract = require('./abstract')

Moment = require 'moment'
Numeral = require 'numeral'
Formats = require 'config/formats'
HumanTime = require 'utils/human_time'
LocationFormatter = require 'utils/formatters/location'
ServiceFormatter = require 'utils/formatters/service'

class View extends Abstract

	className : 'client_package m-standard'

	initialize : ->
		super

		@events['click [data-book-appointment]'] = 'bookAppointment'
		@delegateEvents()

	getTemplate : -> ProfessionalPackages.Templates.Package.Standard

	getTemplateData : ->
		data = super

		durationLabel = HumanTime.minutes(@model.get('duration')) + " session"

		data.package =
			price                : @model.get('price')
			price_label          : Numeral(@model.get('price')).format(Formats.Price.Simple)
			duration             : @model.get('duration')
			duration_label       : durationLabel
			entities_left        : @model.get('entities_left')
			number_of_entities   : @model.get('number_of_entities')
			entities_label_short : do =>
				total = @model.get('number_of_entities')
				left = @model.get('entities_left')
				if left < 1
					"There are no credits remaining"
				else
					"#{left} of #{total} credits available"
			entities_label       : do =>
				total = @model.get('number_of_entities')
				left = @model.get('entities_left')
				if left < 1
					"There are no credits remaining for this package."
				else
					"<strong>#{@model.get('entities_left')} of #{total} credits</strong> available for #{durationLabel} of #{@model.get('service.name')}"

		data.locations = @model.get('service.locations').map (location) ->
			moment = Moment()

			availabilites = location.availabilities.map (availability) ->
				fromTime = availability.from.split(':')
				untilTime = availability.until.split(':')

				moment.set
					weekday : availability.day - 1
					h       : fromTime[0]
					m       : fromTime[1]

				{
					day     : availability.day
					weekday : moment.format('dddd')
					from    : moment.format('hh:mm a')
					until   : moment.set({h : untilTime[0], m : untilTime[1]}).format('hh:mm a')
				}

			availabilites = _.sortBy availabilites, (availability) -> availability.day

			{
				type           : location.location_type
				name           : LocationFormatter.getLocationName location
				address        : LocationFormatter.getLocationAddressLabel location
				type_label     : LocationFormatter.getLocationTypeLabel location
				url            : LocationFormatter.getLocationGmapsUrl location
				availabilities : availabilites
			}

		data

	bookAppointment : (e) ->
		e?.preventDefault()
		window.bookingHelper.openWizard
			bookingData : @getBookingData()
		@


	getBookingData : ->
		date = Moment().format('YYYY-MM-DD')

		data =
			resetToFirstStep : 'recalculate'
			from             : date
			date             : date
			provider_id      : @model.get('provider.id')
			selected_time    :
				service_id   : @model.get('service.service_id')
				service_name : ServiceFormatter.getServiceName @model.get('service')
				session_id   : @model.get('session_id')
				duration     : @model.get('duration')
				price        : @model.get('price')

		data

module.exports = View

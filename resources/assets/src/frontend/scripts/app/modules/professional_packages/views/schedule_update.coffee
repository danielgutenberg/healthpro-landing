Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	className: 'client_package_schedule'

	events:
		'click [data-cancel]': 'cancel'
		'click [data-save]': 'save'
		'click [data-weekday-select]': 'activateWeekday'
		'click [data-availability-select]': 'selectAvailability'


	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@selectedAvailability = null

		@collection = new ProfessionalPackages.Collections.Availabilities [],
			packageModel: @packageModel
			scheduleModel: @scheduleModel

		@addListeners()

	addListeners: ->
		@listenToOnce @collection, 'data:loading', => @popupView.showLoading()
		@listenToOnce @collection, 'data:ready', => @popupView.hideLoading()
		@listenToOnce @collection, 'availabilities:ready', @afterAvailabilitiesReady
		@

	render: ->
		@$el.html ProfessionalPackages.Templates.ScheduleUpdate @getTemplateData()
		@

	cacheDom: ->
		@$el.$availabilities = @$el.find('[data-availabilities]')
		@$el.$availabilitiesEmpty = @$el.find('[data-availabilities-empty]')
		@$el.$save = @$el.find('[data-save]')
		@

	loadAvailabilities: ->
		@collection.fetch()
		@

	afterAvailabilitiesReady: ->
		@render()
		@cacheDom()
		@disableWeekdaysWithoutAvailabilities()
		@selectAvailableWeekday()
		$(document).trigger 'popup:center'
		@

	getTemplateData: ->
		data =
			weekdays: @collection.getWeekdays()

		data

	disableWeekdaysWithoutAvailabilities: ->
		_.each @collection.getWeekdays(), (weekday) =>
			$weekdayEl = @$el.find("[data-date='#{weekday.date}']")

			# deactivate weekdays without availabilities
			if @collection.getForDate(weekday.date).length > 0
				$weekdayEl.removeClass 'm-inactive'
			else
				$weekdayEl.addClass 'm-inactive'
		@

	selectAvailableWeekday: ->
		@$el.find("[data-date]").not('.m-inactive').first().find('[data-weekday-select]').trigger 'click'
		@

	activateWeekday: (e) ->
		$weekday = $(e.currentTarget).parent()
		return if $weekday.hasClass('m-inactive') or $weekday.hasClass('m-active')

		@setSelectedAvailability null

		$weekday.addClass('m-active').siblings('.m-active').removeClass('m-active')
		availabilities =  @collection.getForDate $weekday.data('date')

		@renderAvailabilities availabilities

		$(document).trigger 'popup:center'
		@

	setSelectedAvailability: (availability = null) ->
		@selectedAvailability = availability
		@toggleSave()
		@

	toggleSave: ->
		@$el.$save.prop 'disabled', if @selectedAvailability then false else true
		@

	toggleAvailabilitiesEmpty: (availabilities) ->
		if availabilities.length
			@$el.$availabilities.removeClass 'm-hide'
			@$el.$availabilitiesEmpty.addClass 'm-hide'
		else
			@$el.$availabilities.addClass 'm-hide'
			@$el.$availabilitiesEmpty.removeClass 'm-hide'
		@

	renderAvailabilities: (availabilities) ->
		@resetAvailabilities()
		@toggleAvailabilitiesEmpty availabilities

		availabilities.forEach (availability) => @renderAvailability availability
		@

	renderAvailability: (availability) ->
		@$el.$availabilities.append """
			<li data-cid='#{availability.cid}'>
				<button data-availability-select>#{availability.get('label')}</button>
			</li>
		"""
		@

	resetAvailabilities: ->
		@$el.$availabilities.html ''
		@

	selectAvailability: (e) ->
		$availability = $(e.currentTarget).parent()
		$availability.addClass('m-active').siblings('.m-active').removeClass('m-active')

		availability = @collection.get $availability.data('cid')

		@setSelectedAvailability
			availability_id: availability.get('availability_id')
			from: availability.get('from')
			until: availability.get('until')
		@

	cancel: ->
		@popupView.$el.find('[data-popup-close]').trigger 'click'
		@

	save: ->
		return unless @selectedAvailability

		@popupView.showLoading()

		$.when(
			@scheduleModel.save @selectedAvailability.from, @selectedAvailability.until
		).then(
			=>
				@popupView.hideLoading()
				@parentView.reload()
				@baseView.removePopup()
		).fail(
			=> @popupView.hideLoading()
		)


module.exports = View

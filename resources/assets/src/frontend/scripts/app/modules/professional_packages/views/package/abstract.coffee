Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
LocationFormatter = require 'utils/formatters/location'
ServiceFormatter = require 'utils/formatters/service'
getRoute = require 'hp.url'
vexDialog = require 'vexDialog'
require 'backbone.stickit'

class Abstract extends Backbone.View

	className : 'client_package'
	tagName   : 'li'

	events :
		'click [data-package-header]' : 'toggleContent'
		'click [data-package-cancel]' : 'cancelConfirm'

	initialize : (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@instances = []

		@bind()
		@addListeners()
		@render()

	bind : ->
		@bindings = {}

	addListeners : ->
		@listenTo @baseView.model, 'change:filter', @filterToggle
		@listenTo @, 'rendered', =>
			@cacheDom()
			@stickit()
			@afterRender()

	reRender : ->
		@undelegateEvents()
		@$el.html ''

		@delegateEvents()
		@render()

		@

	render : ->
		@$el.html @getTemplate()(@getTemplateData())
		@trigger 'rendered'
		@

	afterRender : -> @

	cacheDom : ->
		@$el.$list = @$el.find('[data-packages-list]')
		@$el.$loading = @$el.find('.loading_overlay')
		@

	toggleContent : ->
		if @$el.hasClass 'm-opened'
			@$el.removeClass 'm-opened'
		else
			@$el.addClass 'm-opened'
		@

	getTemplateData : ->
		service = @model.get('service')
		service.detailed_name = ServiceFormatter.getServiceName service
		data =
			type      : @model.get('type')
			is_active : @model.isActive()
			service   : service
			client    :
				full_name : @model.get('client.full_name')
				avatar    : do =>
					avatar = @model.get('client.avatar')
					return '' unless avatar
					"<img src='#{avatar}' alt=''>"

		nextAppointment = @model.getUpcomingAppointment()
		if nextAppointment and @model.isActive()
			location = nextAppointment.get('location')
			data.location =
				type          : location.location_type
				is_home_visit : location.location_type is 'home-visit'
				is_virtual    : location.location_type is 'virtual'
				is_office     : location.location_type is 'office'
				name          : if location.location_type is 'home-visit' then 'Home Visit' else LocationFormatter.getLocationName(location)
				address       : LocationFormatter.getLocationAddressLabel location, false, true
				type_label    : LocationFormatter.getLocationTypeLabel location
				url           : LocationFormatter.getLocationGmapsUrl location
			data.next_appointment =
				weekday : nextAppointment.get('date').format('dddd')
				date    : nextAppointment.get('date').format('MMMM D, YYYY')
				from    : nextAppointment.get('from').format('hh:mm a')
				until   : nextAppointment.get('until').format('hh:mm a')
				url     : getRoute('dashboard-appointment', {appointmentId : nextAppointment.get('id')})
		data

	filterToggle : ->
		if @baseView.model.get('filter') is @model.get('filter_type')
			@$el.removeClass 'm-hide'
		else
			@$el.addClass 'm-hide'
		@

	cancelConfirm : (e) ->
		e?.preventDefault()

		vexDialog.confirm
			buttons  : [
				$.extend {}, vexDialog.buttons.YES, {text : 'Yes'}
				$.extend {}, vexDialog.buttons.NO, {text : 'Cancel'}
			]
			message  : '<span class="m-error">WARNING</span> Are you sure you want to cancel this package?'
			callback : (status) => @cancel() if status

	cancel : ->
		@baseView.showLoading()
		$.when(
			@model.cancel()
		).done(
			=> @baseView.hideLoading()
		)

	isHidden : -> @$el.hasClass('m-hide')

	isVisible : -> !@isHidden()

module.exports = Abstract

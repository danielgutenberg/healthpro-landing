Handlebars = require 'handlebars'
require 'app/modules/booking_helper'

require '../../../../styles/modules/client_packages.styl'

window.ProfessionalPackages = ProfessionalPackages =
	Views:
		Base: require('./views/base')
		Popup: require('./views/popup')
		Packages: require('./views/packages')
		Package:
			Recurring: require('./views/package/recurring')
			Standard: require('./views/package/standard')
		ScheduleUpdate: require('./views/schedule_update')

	Models:
		Base: require('./models/base')
		Package: require('./models/package')
		Appointment: require('./models/appointment')
		ScheduleRow: require('./models/schedule_row')

	Collections:
		Packages: require('./collections/packages')
		Appointments: require('./collections/appointments')
		Availabilities: require('./collections/availabilities')
		Schedule: require('./collections/schedule')

	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')
		Popup: Handlebars.compile require('text!./templates/popup.html')
		Packages: Handlebars.compile require('text!./templates/packages.html')
		Package:
			Standard: Handlebars.compile require('text!./templates/package/standard.html')
			Recurring: Handlebars.compile require('text!./templates/package/recurring.html')
		ScheduleUpdate: Handlebars.compile require('text!./templates/schedule_update.html')

class ProfessionalPackages.App
	constructor: (options) ->
		@collections =
			packages: new ProfessionalPackages.Collections.Packages [],
				model: ProfessionalPackages.Models.Package

		@model = new ProfessionalPackages.Models.Base()

		_.extend options,
			collections: @collections
			model: @model

		@view = new ProfessionalPackages.Views.Base options

	close: ->
		@view.close?()
		@view.remove?()

if $('[data-professional-packages]').length
	new ProfessionalPackages.App
		type: 'dashboard'
		$container: $('[data-professional-packages]')

module.exports = ProfessionalPackages

Backbone = require 'backbone'
datetimeParsers = require 'utils/datetime_parsers'
Moment = require 'moment'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		@setData options.data

	setData: (items) ->
		_.each items, (item) => @push @formatAppointment(item)

	formatAppointment: (item) ->
		{
			id: item.id
			location: item.location
			date: datetimeParsers.parseToMoment(item.registration.date)
			from: datetimeParsers.parseToMoment(item.registration.from)
			until: datetimeParsers.parseToMoment(item.registration.until)
			initiated_by: item.initiated_by
			price: item.price
			duration: item.duration
			status: item.status
		}

	getUpcomingAppointment: ->
		return null unless @length

		@sort()

		# make sure we get the future appointment
		now = Moment()
		appointments = @reject (appointment) -> appointment.get('from').isBefore now

		_.first appointments

	getLastAppointment: ->
		return null unless @length
		@sort()
		@last()

	comparator: (a, b) -> if a.get('from').isBefore(b.get('from')) then -1 else 1


module.exports = Collection

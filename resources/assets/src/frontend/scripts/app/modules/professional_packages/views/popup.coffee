Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	attributes:
		'data-popup': 'client_packages'
		'class': 'popup client_packages_popup'
		'id': 'popup_client_packages'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@contentView.popupView = @

		@render()
		@cacheDom()
		@appendView()

	render: ->
		@$el.html ProfessionalPackages.Templates.Popup
			popupTitle: @popupTitle
			popupDescription: @popupDescription
			contentClass: @contentClass ? ''
		@

	cacheDom: ->
		@$el.$header = @$el.find('[data-popup-header]')
		@$el.$content = @$el.find('[data-popup-content]')
		@$el.$footer = @$el.find('[data-popup-footer]')
		@$el.$loading = @$el.find('[data-popup-loading]')
		@

	appendView: ->
		@$el.$content.append @contentView.el
		@

module.exports = View

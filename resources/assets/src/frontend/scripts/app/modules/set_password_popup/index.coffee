Handlebars = require 'handlebars'

# views
BaseView = require './views/base'

# templates
BaseTemplate = Handlebars.compile require('text!./templates/base.html')

# models
Password = require './models/password'

window.SetPasswordPopup =
	views:
		base: BaseView
	templates:
		base: BaseTemplate
	models:
		password: Password

class SetPasswordPopup.App
	constructor: (options) ->
		@view = new window.SetPasswordPopup.views.base
			$el: options.$el

if $('.require_password_popup').length
	RequrePassword = new SetPasswordPopup.App
		$el: $('.require_password_popup')

module.exports = SetPasswordPopup

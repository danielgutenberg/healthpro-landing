Backbone = require 'backbone'
Validation = require 'backbone.validation'
Stickit = require 'backbone.stickit'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
getApiRoute = require 'hp.api'

class View extends Backbone.View

	initialize: (options) ->
		super
		if @checkReferrer()
			Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins

			@$el = options.$el
			@model = new SetPasswordPopup.models.password

			@addValidation
				selector: "name"

			@requirePassword()

			@addStickit()
			@stickit()
			@cacheDom()
			@bind()

	checkReferrer: ->
		return getUrlParam('referrer') == 'confirm'

	getUrlParam = (param) ->
		@pageURL = decodeURIComponent(window.location.search.substring(1))
		@variables = @pageURL.split('&')
		@paramName = undefined

		i = 0
		while i < @variables.length
			@paramName = @variables[i].split('=')
			if @paramName[0] == param
				return if @paramName[1] == undefined then true else @paramName[1]
			i++
		return

	bind: ->
		@$('.btn').on 'click', =>
			@savePassword()

		@$('[name="confirmNewPassword"]').on 'keyup', =>
			@model.isValid 'confirmNewPassword'

	addStickit: ->
		@bindings =
			'[name="newPassword"]': 'newPassword'
			'[name="confirmNewPassword"]': 'confirmNewPassword'
			'[name="acceptTerms"]': 'acceptTerms'

		return @

	requirePassword: ->
		@removePopup()

		$('body').append @render().$el
		window.popupsManager.addPopup @$el
		window.popupsManager.openPopup 'require_password'

	removePopup: ->
		window.popupsManager.closePopups()
		window.popupsManager.removePopup 'require_password'
		window.popupsManager.popups = []

	render: ->
		data =
			terms: window.location.origin + '/terms'

		@$el.html SetPasswordPopup.templates.base(data)
		@

	cacheDom: ->
		@$el.$loading = @$el.find('.loading_overlay')

	showLoading: ->
		@$el.$loading.removeClass('m-hide')

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

	savePassword: ->
		unless @model.validate()
			@showLoading()
			$.ajax
				url: getApiRoute('user-account')
				method: 'put'
				type  : 'json'
				contentType: 'application/json; charset=utf-8'
				data: JSON.stringify
					password: @model.get('newPassword')
					confirmed_password: @model.get('confirmNewPassword')
					_token: window.GLOBALS._TOKEN
				success: (response) =>
					@hideLoading()
					@removePopup()
					if window.GLOBALS.PasswordRequired == '1'
						$('.m-warning').remove()
					@updateUser 'Your password has been successfully set'
				error: (error) =>
					@hideLoading()
					@removePopup()
					res = error.responseJSON
					if res? and res.errors.currentPassword
						return @updateUser res.errors.currentPassword.messages[0]
					@updateUser 'An error has occured, please try again'

	updateUser: (msg) ->
		vexDialog.buttons.YES.text = 'Ok'
		vexDialog.alert
			message: msg

module.exports = View

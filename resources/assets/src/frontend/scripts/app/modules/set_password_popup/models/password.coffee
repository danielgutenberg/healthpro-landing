Backbone = require 'backbone'

class Model extends Backbone.Model

	initialize: () ->
		@addValidationRules()

	addValidationRules: ->
		@validation =
			newPassword:
				fn: @passwordValidation
			confirmNewPassword:
				fn: @passwordValidation
			acceptTerms:
				required: true
				msg: 'Please accept Terms of Service to set your password'

	passwordValidation: (value, attr, computedState) ->
		if attr is 'newPassword' and value is undefined
			return 'Please enter your new password'

		if attr is 'newPassword' and value.length < 7
			return 'Please enter password that is at least 7 characters'

		if attr is 'confirmNewPassword' and value != computedState['newPassword']
			return 'Please make sure your passwords match'

module.exports = Model

Backbone = require 'backbone'
getApiRoute = require 'hp.api'

require 'backbone.validation'

class Model extends Backbone.Model

	ratings: [
		{
			label: 'Overall'
			id: 'overall'
		}
		{
			label: 'Professionalism'
			id: 'professionalism'
		}
		{
			label: 'Value'
			id: 'value'
		}
		{
			label: 'Personal Touch'
			id: 'personal_touch'
		}
		{
			label: 'Effectiveness'
			id: 'effectiveness'
		}
	]

	validation:
		comment: [
			{
				required: true
				msg: 'Please write your review'
			}
			{
				maxLength: 2100
				msg: 'Your review is too long, please enter a maximum of 2100 characters'
			}
		]


	postReview: (profileId) ->
		data =
			comment: @get('comment')
			ratings: _.omit @toJSON(), 'comment'
			_token: window.GLOBALS._TOKEN

		$.ajax
			url: getApiRoute('provider-review', {providerId: profileId})
			method: 'POST'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify data

module.exports = Model

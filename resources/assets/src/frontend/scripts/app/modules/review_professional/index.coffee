Backbone = require 'backbone'
Handlebars = require 'handlebars'

window.ReviewProfessional = ReviewProfessional =

	Views:
		Base: require('./views/base')
		Form: require('./views/form')
		BookingInfo: require('./views/booking_info')
		ProviderInfo: require('./views/provider_info')

	Models:
		Base: require('./models/base')

	Templates:
		BasePopup: Handlebars.compile require('text!./templates/base_popup.html')
		BasePage: Handlebars.compile require('text!./templates/base_page.html')
		Form: Handlebars.compile require('text!./templates/form.html')
		BookingInfo: Handlebars.compile require('text!./templates/booking_info.html')
		ProviderInfo: Handlebars.compile require('text!./templates/provider_info.html')

_.extend ReviewProfessional, Backbone.Events

class ReviewProfessional.App
	constructor: (options) ->
		@model = new ReviewProfessional.Models.Base

		_.extend options, {model: @model}

		@view = new ReviewProfessional.Views.Base options

		return {
			view: @view
			model: @model
			close: @close
		}

	close: ->
		@view?.closeView()

if $('.review--container').length
	new ReviewProfessional.App $('.review--container')

module.exports = ReviewProfessional

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'
Maxlength = require 'framework/maxlength'

require 'backbone.stickit'
require 'backbone.validation'


class View extends Backbone.View

	events:
		'click .leave_review--rating span': 'setStars'

	initialize: (options) ->
		super
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions options

		@bind()
		@addValidation()

		@

	render: ->
		@$el.html ReviewProfessional.Templates.Form
			ratings: @model.ratings

		@stickit()
		@


	bind: ->
		@bindings =
			'[data-review="comment"]':
				observe: 'comment'
				onGet: (val, options) =>
					@initTextarea @$el.find(options.selector).parent(), val
					val

	initTextarea: ($el, val) ->
		unless $el.attr('data-maxlength') is 'inited'
			$('textarea', $el).val(val)
			new Maxlength( $el )

	setStars: (ev) =>
		value = 5 - $(ev.currentTarget).index()
		div = $(ev.currentTarget).closest('div')
		div.find('span').each (key, span) ->
			$(span).removeClass('checked')
		$(ev.currentTarget).addClass('checked')
		@model.set div.data('rating'), value

	postReview: ->
		return unless @model.validate() is undefined
		if _.size(@model.attributes) < 2
			vexDialog.alert
				message: 'Please rate at least one field before submitting the review'
			return

		@baseView.showLoading()
		$.when(@model.postReview(@profileId)).then(
			(res) =>
				@baseView.hideLoading()
				@baseView.afterReviewPosted(res)

			, (res) =>
				@baseView.hideLoading()
				msg = 'An error occured. Please try submitting the review again'
				if res.status = 401
					msg = 'You are not permitted to review this professional'
				vexDialog.alert
					message: msg

		)

module.exports = View

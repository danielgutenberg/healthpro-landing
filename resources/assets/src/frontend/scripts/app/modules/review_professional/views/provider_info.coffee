Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	initialize: (options) ->
		super
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions options

		@

	render: ->
		@$el.html ReviewProfessional.Templates.ProviderInfo
			professional: @templateData()
		@

	templateData: ->

		templateData = _.clone @professionalDetails

		templateData.overall_rating = templateData.ratings.overall?.toString().replace('.0', '').replace('.', '_')
		templateData.reviews_count = templateData.reviews.length
		templateData.reviews_count_label = if templateData.reviews.length is 1 then 'review' else 'reviews'
		unless templateData.full_name is templateData.first_name + ' ' + templateData.last_name # hack
			templateData.business_name = templateData.full_name

		templateData

module.exports = View

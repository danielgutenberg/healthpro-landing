Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Moment = require 'moment'

class View extends Backbone.View

	initialize: (options) ->
		super
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions options

		@

	render: ->
		@$el.html ReviewProfessional.Templates.BookingInfo
			professional: @templateData()
		@


	templateData: ->

		templateData = _.clone @professionalDetails

		if templateData.last_appointment
			fromDate = Moment(templateData.last_appointment.from)
			untilDate = Moment(templateData.last_appointment.until)

			templateData.last_appointment.date = fromDate.format('MMMM DD, YYYY')
			templateData.last_appointment.time = fromDate.format('hh:mm a')
			templateData.last_appointment.weekday = fromDate.format('dddd')

			# calculate duration
			unless templateData.last_appointment.duration
				diff = untilDate.diff fromDate, 'minutes'
				templateData.last_appointment.duration = diff


		templateData.overall_rating = templateData.ratings.overall?.toString().replace('.0', '').replace('.', '_')
		templateData.reviews_count = templateData.reviews.length
		templateData.reviews_count_label = if templateData.reviews.length is 1 then 'review' else 'reviews'

		templateData




module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
navigate = require 'utils/navigate'

getApiRoute = require 'hp.api'


class View extends Backbone.View

	popupName: 'leave_review_popup'

	showCloseButton: true

	events:
		'click [data-button-submit]': 'postReview'
		'click [data-button-cancel]': 'cancelReview'
		'click [data-button-success]': 'success'

	initialize: (options) ->
		super
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions options

		@render()
		@cacheDom()
		@bind()
		@loadProfessionalDetails()

	bind: ->
		@listenTo @, 'professional_details:loaded', ->
			@togglePopupClass()
			@appendParts()

	render: ->
		if @isPopup
			@$el.html ReviewProfessional.Templates.BasePopup
				showCloseButton: @showCloseButton
				hasLastAppointment: @professionalDetails?.last_appointment?
				professional: @professionalDetails

			@$el.addClass 'popup leave_review_popup'
			@$el.attr 'id', 'popup_leave_review_popup'
			@$el.attr 'data-popup', @popupName

			$('body').append @$el

			window.popupsManager.addPopup @$el
			window.popupsManager.openPopup @popupName

		else
			@$el.html ReviewProfessional.Templates.BasePage()
			# @TODO append to the page element

	appendParts: ->
		@subViews.push @form = new ReviewProfessional.Views.Form
			model: @model
			profileId: @profileId
			professionalDetails: @professionalDetails
			baseView: @
		@$el.$form.html @form.render().el

		if @professionalDetails?.last_appointment?
			@subViews.push @bookingInfo = new ReviewProfessional.Views.BookingInfo
				model: @model
				profileId: @profileId
				professionalDetails: @professionalDetails
				baseView: @
			@$el.$bookingInfo.html @bookingInfo.render().el
			@$el.$bookingInfo.removeClass 'm-hide'

		unless @professionalDetails?.last_appointment?
			@subViews.push @providerInfo = new ReviewProfessional.Views.ProviderInfo
				model: @model
				profileId: @profileId
				professionalDetails: @professionalDetails
				baseView: @
			@$el.$providerInfo.html @providerInfo.render().el

		if @isPopup
			$(document).trigger 'popup:center'

	togglePopupClass: ->
		unless @professionalDetails?.last_appointment?
			@$el.$popupContainer.addClass 'm-without_appointment'
		else
			@$el.$popupContainer.removeClass 'm-without_appointment'

	cacheDom: ->
		@$el.$popupContainer = @$el.find('[data-popup-container]')
		@$el.$loading = @$el.find('.loading_overlay')
		@$el.$form = @$el.find('[data-form]')
		@$el.$bookingInfo = @$el.find('[data-booking-info]')
		@$el.$providerInfo = @$el.find('[data-provider-info]')

	postReview: (e) ->
		e?.preventDefault()
		@form.postReview()

	cancelReview: (e) ->
		e?.preventDefault()
		unless @isPopup
			@maybeRedirect()
			return
		@closeView()

	afterReviewPosted: (review) ->

		ReviewProfessional.trigger 'review:posted',
			profileId: @profileId
			review: review

		if @isPopup
			@$el.addClass 'm-success'
			$(document).trigger 'popup:center'
			return


	loadProfessionalDetails: ->
		if @professionalDetails
			@trigger 'professional_details:loaded'
			return

		@showLoading()
		$.ajax
			url: getApiRoute('ajax-client-provider', {clientId: 'me', providerId: @profileId})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data:
				return_fields: 'all'
			success: (response) =>
				@hideLoading()
				return unless response.data
				@professionalDetails = @formatProfessionalDetails(response.data)
				@trigger 'professional_details:loaded'
			error: () =>
				@hideLoading()
				console.log 'error', arguments
				@closeView()

	showLoading: -> @$el.$loading.removeClass('m-hide')
	hideLoading: -> @$el.$loading.addClass('m-hide')

	formatProfessionalDetails: (data) ->
		data.overall_rating = data.ratings.overall?.toString().replace('.0', '').replace('.', '_')
		data.reviews_count = data.reviews.length
		data.reviews_count_label = if data.reviews.length is 1 then 'review' else 'reviews'

		return data

	success: ->
		# redirect after submittion
		@maybeRedirect()
		@closeView()

	maybeRedirect: ->
		if @redirectUrl
			navigate @redirectUrl
			return

	closeView: ->
		if @isPopup
			window.popupsManager.removePopup @popupName

		@close?()
		@remove?()

module.exports = View

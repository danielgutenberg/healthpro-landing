Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
parseHash = require 'utils/parse_hash'

require 'backbone.stickit'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@hash = parseHash false
		@instances = []

		@bind()
		@addListeners()
		@render()
		@cacheDom()
		@appendCertificates()

	addListeners: ->
		@listenTo @collections.certificates, 'data:loading', @showLoading
		@listenTo @collections.certificates, 'data:ready', @afterDataLoaded
		@

	bind: ->
		@bindings =
			'[data-gift-certificates-filter]':
				observe: 'filter'
				selectOptions:
					collection: @collections.certificates.getFilterOptions()
		@

	render: ->
		@$el.html ClientGiftCertificates.Templates.Base()
		@$container.html @$el
		@stickit()
		@

	appendCertificates: ->
		@instances.push @certificatesView = new ClientGiftCertificates.Views.Certificates
			collections: @collections
			baseView: @

		@$el.$certificates.append @certificatesView.el
		@

	cacheDom: ->
		@$body = $('body')
		@$el.$certificates = @$el.find('[data-gift-certificates]')
		@$el.$filter = @$el.find('[data-gift-certificates-filter]')
		@$el.$loading = @$el.find('.loading_overlay')
		@

	afterDataLoaded: ->
		return if @maybeRedeemCertificate()
		@hideLoading()

	redeemCertificate: (certificateModel) ->
		window.bookingHelper.openWizard
			bookingData: certificateModel.getBookingData()
			forcePage: true
		@

	maybeRedeemCertificate: ->
		if window.GLOBALS.REDEEM_CERTIFICATE
			certificateModel = @collections.certificates.get(window.GLOBALS.REDEEM_CERTIFICATE)
			if certificateModel
				@redeemCertificate certificateModel
				return true
		false

module.exports = View

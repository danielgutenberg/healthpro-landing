Handlebars = require 'handlebars'
require 'app/modules/booking_helper'

require '../../../../styles/modules/client_gift_certificates.styl'

window.ClientGiftCertificates = ClientGiftCertificates =
	Config:
		DisplaySelectedWeekdays: false
		DateFormat: 'dddd, MMMM Do, YYYY'
		TimeFormat: 'HH:mm'

	Views:
		Base: require('./views/base')
		Certificates: require('./views/certificates')
		Certificate: require('./views/certificate')

	Models:
		Base: require('./models/base')
		Certificate: require('./models/certificate')

	Collections:
		Certificates: require('./collections/certificates')

	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')
		Certificates: Handlebars.compile require('text!./templates/certificates.html')
		Certificate: Handlebars.compile require('text!./templates/certificate.html')

class ClientGiftCertificates.App
	constructor: (options) ->
		@collections =
			certificates: new ClientGiftCertificates.Collections.Certificates [],
				model: ClientGiftCertificates.Models.Certificate

		@model = new ClientGiftCertificates.Models.Base()

		_.extend options,
			collections: @collections
			model: @model

		@view = new ClientGiftCertificates.Views.Base options

	close: ->
		@view.close?()
		@view.remove?()

if $('[data-client-gift-certificates]').length
	new ClientGiftCertificates.App
		type: 'dashboard'
		$container: $('[data-client-gift-certificates]')

module.exports = ClientGiftCertificates

Backbone = require 'backbone'
Moment = require 'moment'

class Model extends Backbone.Model

	getBookingData: ->
		now = Moment()

		service = @get('service')
		session = @get('session')

		formattedService =
			service_id: service.service_id
			name: service.name
			sessions: _.filter service.sessions, (sess) -> sess.session_id is session.session_id
			locations: service.locations
			detailed_name: service.name

		{
			resetToFirstStep: 'recalculate'
			from: now.format()
			date: now.format('YYYY-MM-DD')
			client_id: +window.GLOBALS?._PID
			provider_id: @get('provider').id
			certificate_id: @get('id')
			bookedFrom: 'profile'
			services: [formattedService]
			selected_time:
				service_id: service.service_id
				service_name: service.name
				session_id: session.session_id
				duration: session.duration
				price: session.price
		}

module.exports = Model

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
HumanTime = require 'utils/human_time'
require 'backbone.stickit'

class View extends Backbone.View

	className: 'client_gift_certificate'
	tagName: 'li'

	events:
		'click [data-redeem-certificate]': 'redeem'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@instances = []

		@bind()
		@addListeners()
		@render()

	bind: ->
		@bindings = {}

	addListeners: ->
		@listenTo @baseView.model, 'change:filter', @filterToggle

	render: ->
		@$el.html ClientGiftCertificates.Templates.Certificate @getTemplateData()
		@trigger 'rendered'
		@

	getTemplateData: ->
		{
			serviceName: @model.get('service').name
			professionalName: @model.get('provider').full_name
			senderName: @model.get('sender').full_name
			duration: HumanTime.minutes @model.get('session').duration
			receivedDate: @model.get('purchased_date').format('MMMM D, YYYY')
			canRedeem: @model.get('filter_type') is 'active'
			redeemedDate: @model.get('redeemed_date')?.format('MMMM D, YYYY')
		}

	filterToggle: ->
		filter = @baseView.model.get('filter')
		if filter is @model.get('filter_type') or filter is 'all'
			@$el.removeClass 'm-hide'
		else
			@$el.addClass 'm-hide'
		@

	isHidden: -> @$el.hasClass('m-hide')

	isVisible: -> !@isHidden()

	redeem: ->
		@baseView.redeemCertificate @model
		@

module.exports = View

Backbone = require 'backbone'

class Model extends Backbone.Model

	defaults: -> {
		filter: 'active'
	}

module.exports = Model

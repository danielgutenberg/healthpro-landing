Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
scrollTo = require 'utils/scroll_to_element'

class View extends Backbone.View

	className: 'client_gift_certificates--list_wrap'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@instances = []

		@addListeners()
		@render()
		@cacheDom()

	addListeners: ->
		@listenTo @collections.certificates, 'data:ready', @renderCertificates
		@listenTo @baseView.model, 'change:filter', @filterToggle
		@

	render: ->
		@$el.html ClientGiftCertificates.Templates.Certificates()
		@

	renderCertificates: ->
		@toggleEmpty @collections.certificates.length is 0
		@collections.certificates.forEach (model) => @renderCertificate model
		@baseView.model.trigger 'change:filter'
		@

	renderCertificate: (model) ->
		@instances.push view = new ClientGiftCertificates.Views.Certificate
			baseView: @baseView
			parentView: @
			model: model
			collections: @collections
		@$el.$list.append view.el
		@

	cacheDom: ->
		@$el.$list = @$el.find('[data-gift-certificates-list]')
		@$el.$empty = @$el.find('[data-gift-certificates-empty]')
		@

	toggleEmpty: (display = true) ->
		if display
			@$el.$empty.removeClass 'm-hide'
		else
			@$el.$empty.addClass 'm-hide'

	filterToggle: ->
		return if @collections.certificates.length is 0
		@toggleEmpty @collections.certificates.getFiltered(@baseView.model.get('filter')).length is 0

module.exports = View

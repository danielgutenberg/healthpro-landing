Backbone = require 'backbone'
getApiRoute = require 'hp.api'
datetimeParsers = require 'utils/datetime_parsers'

class Collection extends Backbone.Collection

	initialize: ->
		@getData()

	getFilterOptions: -> [
		{
			value: 'all', label: 'All'
		}
		{
			value: 'active', label: 'Active'
		}
		{
			value: 'finished', label: 'Finished'
		}
	]

	getData: ->
		@trigger 'data:loading'
		$.ajax
			url: getApiRoute('ajax-gift-certificates')
			method: 'get'
			type  : 'json'
			data: {}
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.each response.data, (item) => @push @formatCertificate(item)
				@trigger 'data:ready'
		@

	formatCertificate: (item) ->
		{
			id: item.id
			provider: item.provider
			client: item.client
			sender: item.sender
			purchased_date: datetimeParsers.parseToMoment(item.purchased_date)
			redeemed_date: if item.redeemed_date then datetimeParsers.parseToMoment(item.redeemed_date) else null
			service: item.service
			session: item.purchased_session
			filter_type: if item.redeemed_date then 'finished' else 'active'
		}

	getFiltered: (filter = 'active')  ->
		@filter (model) -> model.get('filter_type') is filter or filter is 'all'

module.exports = Collection

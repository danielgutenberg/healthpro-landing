Abstract = require 'app/modules/wizard/models/base'
getApiRoute = require 'hp.api'

class Model extends Abstract

	defaults: ->
		defaults = super
		defaults.proceedLabel = 'Proceed'
		defaults.backLabel = 'Back'
		defaults.isPaymentStep = false
		defaults

	initialize: ->
		@urls =
			initial : 'api-wizard-booking-initial'
			step    : 'api-wizard-booking-step'

		@bind()

		# when booking data is passed save it first and then initialize wizard.
		$.when(
		).then(
			=>
				@saveWizardData()
		).then(
			=> @initWizard()
		)

	# rewrite wizard's default getInitialWizardData
	getInitialWizardData: ->
		Wizard.loading()
		Wizard.trigger 'initial_data:loading'

		@xhr = $.ajax
			method: 'get'
			url: getApiRoute(@urls.initial)
			data: if @get('data.resetToFirstStep')? then 'resetToFirstStep=' + @get('data.resetToFirstStep') else {}
			success: (response) =>
				@setData response
				Wizard.trigger 'initial_data:ready'
				@set 'isFirstLoad', false
				Wizard.ready()
				return

	getAdditionalWizardData: ->
#		if @get('data.appointment_id')
#			route = getApiRoute('ajax-provider-appointment', {appointmentId: @get('data.appointment_id')})
#			formatter = Wizard.formatter.formatBookingAppointmentFromResponse
#		else
#			route = getApiRoute('ajax-provider', {providerId: @get('data.provider_id')})
#			formatter = Wizard.formatter.formatBookingProfessionalFromResponse

		$.when(
			@loadLockedAppointment(), @loadProfessionalProfile()
		).then(
			-> Wizard.trigger 'additional_data:ready'
		)

	loadLockedAppointment: ->
		return true unless @get('data.appointment_id')
		$.ajax
			url: getApiRoute('ajax-provider-appointment', {appointmentId: @get('data.appointment_id')})
			method: 'get'
			success: (res) =>
				@set Wizard.formatter.formatBookingAppointmentFromResponse(res)

	loadProfessionalProfile: ->
		$.ajax
			url: getApiRoute('ajax-provider', {providerId: @get('data.provider_id')})
			method: 'get'
			success: (res) =>
				@set Wizard.formatter.formatBookingProfessionalFromResponse(res)

	setData: (response) ->
		if @get 'isSwitchedAppointment'
			response.data.next_step = @detectCurrentStep(response.data)
		super(response)

		@set 'isSwitchedAppointment', false

	# rewrite wizard's default resetWizardData
	# gets called on the confirmation step to make sure
	# if the user refreshes the page no current appointment data
	# will be passed to the first step
	resetWizardData: ->
		@set
			'data.resetToFirstStep' : 'recalculate'
			'data.selected_time'    : {}
			'data.availability_id'  : null
			'data.appointment_id'   : null

		@saveWizardData()

	# rewrite wizard's default getCurrentStepData
	getCurrentStepData: ->
		Wizard.loading()
		@xhr = $.ajax
			method: 'get'
			url: getApiRoute(@urls.step, { step: @get('current_step.slug') })
			success: (response) =>
				@setData response

				if @get('data.resetToFirstStep')? and !@get('isFirstLoad')
					@set 'data.resetToFirstStep', null

				Wizard.trigger 'booking_data:ready'
				Wizard.ready()
				return

	getPresetData: ->
		data =
			resetToFirstStep              : @get('data.resetToFirstStep') ? 'recalculate'
			appointment_from_professional : @get('data.appointment_from_professional') ? false
			appointment_from_dashboard    : @get('data.appointment_from_dashboard') ? false
			introductory_confirmed        : @get('data.introductory_confirmed') ? false
			from                          : @get('data.from')
			date                          : @get('data.date')
			client_id                     : @get('data.client_id')
			provider_id                   : @get('data.provider_id')
			certificate_id                : @get('data.certificate_id') ? null
			service_id                    : @get('data.selected_time.service_id') ? null
			session_id                    : @get('data.selected_time.session_id') ? null
			package_id                    : @get('data.selected_time.package.package_id') ? null
			availability_id               : @get('data.selected_time.availability_id') ? null
			appointment_id                : @get('data.selected_time.appointment_id') ? null
			location_id                   : @get('data.selected_time.location_id') ? null
			services                      : @get('data.services')
			bookedFrom                    : @get('data.bookedFrom')

		data


module.exports = Model

Backbone = require 'backbone'
getApiRoute = require 'hp.api'
HumanTime = require 'utils/human_time'

class Model extends Backbone.Model

	defaults :
		sessions_left      : 0
		sessions_purchased : 0
		package_id         : null

	initialize : ->
		@getData()

	getData : ->
		session = @getSession()
		$.ajax
			url     : getApiRoute('ajax-profile-package-credits', {
				profileId  : 'me'
				entityId   : session.session_id
				entityType : 'service_session'
			})
			method  : 'get'
			success : (res) =>
				@set
					price              : res.data.price
					sessions_left      : res.data.entities_left
					sessions_purchased : res.data.number_of_entities
					package_id         : res.data.package_id

				Wizard.model.set 'data.selected_time.used_package',
					price              : @get('price')
					package_id         : @get('package_id')
					sessions_left      : @get('sessions_left')
					number_of_visits   : @get('sessions_purchased')
					formatted_duration : HumanTime.minutes Wizard.model.get('data.selected_time.duration')

				@trigger 'data:ready'
		@

	save : ->
		data = @getOrderData()
		data._token = window.GLOBALS._TOKEN

		$.ajax
			url         : getApiRoute('api-orders')
			method      : 'post'
			type        : 'json'
			contentType : 'application/json; charset=utf-8'
			data        : JSON.stringify(data)
			success     : (res) =>
				Wizard.model.set 'data.order', res.data # save the order

	bookOrder: ->
		data = @getBookingData()
		$.ajax
			url         : getApiRoute('api-order-book', {orderId : Wizard.model.get('data.order.id')})
			method      : 'post'
			type        : 'json'
			contentType : 'application/json; charset=utf-8'
			data        : JSON.stringify(data)

	getBookingData: ->
		data =
			_token         : window.GLOBALS._TOKEN

		data

	getOrderData : ->
		data =
			profile_id : Wizard.model.get('profileId')
			items      : [
				{
					id   : Wizard.model.get('data.appointment_id')
					name : 'appointment'
				}
			]
		data

	getService : ->
		_.findWhere Wizard.model.get('data.services'),
			service_id : Wizard.model.get('data.selected_time.service_id')

	getSession : ->
		service = @getService()

		# if we have session ID then use it
		sessionId = Wizard.model.get('data.selected_time.session_id')
		if sessionId
			return _.findWhere service.sessions, {session_id: sessionId}

		_.findWhere service.sessions,
			duration : Wizard.model.get('data.selected_time.duration')
			active   : 1

module.exports = Model

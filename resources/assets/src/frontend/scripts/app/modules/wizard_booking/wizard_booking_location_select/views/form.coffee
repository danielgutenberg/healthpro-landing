Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	events:
		'submit': 'submitForm'
		'click [data-location-cancel]': 'cancelForm'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@errors = []
		@modelDefaults = @model.toJSON()
		@isNew = if @model.get('id') then false else true
		@placeChanging = false

		@addListeners()
		@initValidation()
		@bind()
		@render()

	addListeners: ->
		@listenTo @model, 'error:postal_code', => @raiseError 'postal_code', 'The location cannot be saved because the address is not supported'
		@listenTo @model, 'error:address', => @raiseError 'address_line', 'Address already exists'
		@listenTo @model, 'error:generic', (errorMessage) => @baseView.raiseGenericError errorMessage
		@listenTo Wizard, 'current_step_back', => @baseView.cancelEditLocation()

	bind: ->
		@bindings =
			'input[name="name"]': 'name'
			'input[name="address_line"]':
				observe: 'full_address'
				initialize: ($el, model) =>

					@autocomplete = new GMaps.gmaps.places.Autocomplete $el[0], {
						types: ['address']
						componentRestrictions:
							country: 'us'
					}
					GMaps.gmaps.event.addListener @autocomplete, 'place_changed', => @parseAutocomplete()
					$el.on 'input', (e) =>
						@placeChanging = true
						val = $el.val()
						newVal = val.replace(/[a-zA-Z]\d/g, (match) ->  match[0] + ' ' + match[1])
						unless val == newVal
							$el.val(newVal)
							GMaps.gmaps.event.trigger $el[0], 'focus', {}
				onSet: (val) =>
					# when we're typing the address we need to make sure it gets parsed
					# we can parse the address only firing 'place_changed' event on autocomplete
					@model.resetAddressFields()
					val

	parseAutocomplete: ->
		@model.parseAddressFields @autocomplete.getPlace()
		@placeChanging = false
		@trigger 'place:changed'

	submitForm: (e) ->
		e?.preventDefault()

		@baseView.raiseGenericError null

		# wait for 'place_changed' event
		if @placeChanging
			@listenToOnce @, 'place:changed', @saveLocation
		else
			@saveLocation()

	saveLocation: ->
		return if @model.validate()

		@baseView.showLoading()
		$.when(
			@model.save()
		).then(
			=>
				@collections.locations.push @model if @isNew
				@baseView.hideLoading()
				@baseView.cancelEditLocation()
		).fail(
			(error) =>
				@baseView.hideLoading()
		)

	cancelForm: ->
		@model.set @modelDefaults
		@baseView.cancelEditLocation()

	render: ->
		@$el.html WizardBookingLocationSelect.Templates.Form()
		@baseView.$el.$side.html @$el

		@stickit()
		@

	initValidation: ->
		Backbone.Validation.bind @,
			valid: (view, attr) => @raiseError attr, null
			invalid: (view, attr, error) => @raiseError attr, error
		@

	raiseError: (attr, error) ->
		# show all address errors on the address field
		if @model.addressFields.indexOf(attr) isnt -1
			field = @$el.find('[name="address_line"]')
		else
			field = @$el.find('[name="' + attr + '"]')
		if error?
			field.parents('.field').addClass 'm-error'
			field.prev('.field--error').html error
			field.on 'focus.validation', ->
				field.parents('.field').removeClass('m-error')
				field.off('focus.validation')
		else
			field.parents('.field').removeClass 'm-error'
			field.prev('.field--error').html ''

module.exports = View

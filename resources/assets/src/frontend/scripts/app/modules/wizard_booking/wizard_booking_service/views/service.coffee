Backbone = require 'backbone'
Select = require 'framework/select'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Select = require 'framework/select'
HumanTime = require 'utils/human_time'
Formats = require 'config/formats'
Numeral = require 'numeral'

SessionSelect = require 'app/modules/wizard_booking/utils/session_select'


require 'backbone.stickit'

class View extends Backbone.View

	className : 'wizard_booking--service_select'

	initialize : (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addBindings()
		@render()

	addBindings : ->
		@bindings =
			'[name="service"]' :
				observe       : 'current_service_id'
				selectOptions :
					collection : @generateServiceSelect()
					labelPath  : 'detailed_name'
					valuePath  : 'service_id'
				getVal        : @onGetValService
				updateModel   : false
				afterUpdate   : (el) -> el.change() # forcing select2 to update selected option
				initialize    : (el) ->
					new Select el

			'[name="session"]' :
				observe       : 'current_session_id'
				selectOptions :
					collection : @generateSessionSelect
					labelPath  : 'duration_text'
					valuePath  : 'session_id'
				getVal        : @onGetValSession
				updateModel   : false
				afterUpdate   : (el) -> el.change() # forcing select2 to update selected option
				initialize    : (el) ->
					new Select el,
						dropdownClass     : 'select_session--dropdown'
						templateSelection : @selectionTemplate

	onGetValService : ($el) ->
		val = $el.val()
		newService = @baseModel.getService +val
		if newService?
			newSession = _.first newService.sessions
			@baseModel.set
				current_service_id : newService.service_id
				current_session_id : newSession.session_id
		else
			@baseModel.set
				current_service_id : null
				current_session_id : null
		val

	onGetValSession : ($el) ->
		val = $el.val()
		session = @baseModel.getSession +val
		@baseModel.set
			current_session_id : session?.session_id

		val

	render : ->
		@$el.html WizardBookService.Templates.Service()
		@stickit @baseModel
		@

	selectionTemplate : (data) ->
		# Customizing selecte option to show the price
		price = $(data.element).parent().attr('label')
		if price
			$('<span>').html(data.text).prepend $('<b>').html(price)
		else
			$('<span>').html(data.text)

	# We need only services with active sessions here
	generateServiceSelect : -> @baseModel.getServicesWithActiveSessions()

	# session select
	generateSessionSelect : -> SessionSelect(@baseModel)

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	className: 'wizard_booking--step m-create_profile'

	tagName: 'form'

	events :
		'submit'         : 'submitForm'
		'keypress input' : 'detectKey'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@addStickit()
		@addValidation()

	addListeners: ->
		@listenTo Wizard, 'proceed', @submitForm

	addStickit: ->
		@bindings =
			'[name="first_name"]' : 'first_name'
			'[name="last_name"]'  : 'last_name'

	submitForm: (e) ->
		if e? then e.preventDefault?()
		errors = @model.validate()
		return if errors

		Wizard.loading()
		$.when(
			@model.save()
		).then(
			(res) =>
				Wizard.ready()
				window.GLOBALS._PID = res.data?.profile_id
				Wizard.trigger 'request_next_step'
		).fail(
			-> Wizard.ready()
		)

	render: ->
		@$el.html CreateProfile.Templates.BaseWizard()
		@stickit()
		@

	detectKey: (e) ->
		# enter key
		if e.keyCode is 13 then @$el.trigger('submit')

module.exports = View

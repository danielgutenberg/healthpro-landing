Backbone = require 'backbone'
getApiRoute = require 'hp.api'
require 'backbone.validation'

class Model extends Backbone.Model

	defaults : ->
		location_id : null

	initialize : (data, options) ->
		@type = options.type
		@wizard = options.wizard

	save : ->
		return unless @type is 'wizard'
		switch @wizard
			when 'booking' then @saveBookingWizard()
			when 'recurring' then @saveRecurringWizard()

	saveBookingWizard : ->
		location = @getSelectedLocation()
		return unless location

		Wizard.loading()
		$.ajax
			url         : getApiRoute('ajax-appointment-location', {appointmentId : Wizard.model.get('data.appointment_id')})
			method      : 'put'
			show_alerts : false
			data        :
				location_id : location.get('id')
				_token      : window.GLOBALS._TOKEN
			success     : (res) =>
				Wizard.ready()

				if res.data
					@setWizardData()
					Wizard.trigger 'next_step'

				else
					@handleError res

			error : (res) =>
				# handle errors
				Wizard.ready()
				@handleError res.responseJSON, 'service_area'


	saveRecurringWizard : ->
		location = @getSelectedLocation()
		return unless location

		selectedTime = Wizard.model.get('data.selected_time')
		professionalLocationId = selectedTime.professional_location_id ? selectedTime.location_id

		Wizard.loading()
		$.ajax
			url         : getApiRoute('ajax-location-check-radius', {
				locationId     : professionalLocationId,
				homeLocationId : location.get('id')
			})
			method      : 'get'
			show_alerts : false
			success     : (res) =>
				Wizard.ready()
				if res.data
					@setWizardData()
					Wizard.trigger 'next_step'
				else
					@handleError res

			error : (res) =>
				# handle errors
				Wizard.ready()
				@handleError res.responseJSON, 'service_area'

	setWizardData : ->
		location = @getSelectedLocation()
		selectedTime = Wizard.model.get('data.selected_time')

		if !selectedTime.professional_location_id?
			Wizard.model.set 'data.selected_time.professional_location_id', selectedTime.location_id

		Wizard.model.set
			'data.selected_time.client_location_id'      : location.get('id')
			'data.selected_time.client_location_name'    : location.get('name')
			'data.selected_time.client_location_address' : location.get('address')

			'additional.location.id'      : location.get('id')
			'additional.location.name'    : location.get('name')
			'additional.location.address' : location.get('address')
		@


	handleError : (err, errorType = 'generic') ->
		error = err.errors?.error.messages?[0]
		@trigger "error:#{errorType}", error

	getSelectedLocation : ->
		locationId = @get('location_id')
		return null unless locationId
		@collections.locations.findWhere {id : locationId}

	hasSelectedLocation : -> @getSelectedLocation()?


module.exports = Model

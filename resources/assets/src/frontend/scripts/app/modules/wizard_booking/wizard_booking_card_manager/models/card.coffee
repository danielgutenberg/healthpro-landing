Backbone = require 'backbone'
Validation = require 'backbone.validation'
getApiRoute = require 'hp.api'

Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class Model extends Backbone.Model

	defaults:
		id          : null
		card_holder : ''
		card_number : ''
		card_type   : ''
		exp_month   : ''
		exp_year    : ''

	initialize: ->
		super
		Cocktail.mixin @, Mixins.Models.Validation

	validation:
		card_holder:
			required: true
			msg: 'Please enter a name'

		card_number: (val, field, model) ->
			# we don't need to check anything if we have the model ID
			return if model.id
			return 'Please enter a credit card number' unless val
			return 'Min length is 12' if val.length < 12

		exp_month:
			required: true
			msg: 'Please select a month'

		exp_year:
			required: true
			msg: 'Please select a year'

		cvv: [
			{
				required: true
				msg: 'Please enter a cvv'
			}
			{
				pattern: 'digits'
				msg: 'Only digits'
			}
			{
				maxLength: 4
				msg: 'Max length is 4'
			}
			{
				minLength: 3
				msg: 'Min length is 3'
			}
		]

	save: ->
		if @get('id')
			method = 'put'
			route = getApiRoute('ajax-credit-card', {creditCardId: @get('id')})
		else
			method = 'post'
			route = getApiRoute('ajax-credit-cards')

		$.ajax
			url: route
			method: method
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				name: @get('card_holder')
				number: @get('card_number').replace(/\s+/g, '')
				expiryMonth: @get('exp_month')
				expiryYear: @get('exp_year')
				cvv: @get('cvv')
				type: 'credit'
				_token: window.GLOBALS._TOKEN

			error: (error) =>
				@handleErrors(error)

	remove: ->
		$.ajax
			url: getApiRoute('ajax-credit-card', {creditCardId: @get('id')})
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			error: (error) =>
				@handleErrors(error)

module.exports = Model

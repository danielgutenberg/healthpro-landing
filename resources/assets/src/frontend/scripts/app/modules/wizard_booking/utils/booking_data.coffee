WizardDataFormatter = require 'utils/wizard_data_formatter'

_getBookedFrom = (model) -> 'profile'

module.exports =
	getBookingData : (model) ->
		formattedData = WizardDataFormatter.formatFromBookAppointment model.toJSON()
		formattedData.bookedFrom = _getBookedFrom(model)
		formattedData

	getRecurringData : (model) ->
		formattedData = WizardDataFormatter.formatRecurringDataFromBookAppointmnet model.toJSON()
		formattedData.bookedFrom = _getBookedFrom(model)
		formattedData

	getAdditionalData : (model) -> WizardDataFormatter.formatAdditionalData model.toJSON()

	getIntroductoryData : (model) -> WizardDataFormatter.formatFromBookIntroductorySession model.toJSON()

	getAdditionalIntroductoryData : (model) -> WizardDataFormatter.formatAdditionalDataFromIntroductorySession model.toJSON()



debugLog = require 'utils/debug_log'

Abstract = require 'app/modules/wizard/router/router'

BookAppointment     = require 'app/modules/wizard_booking/wizard_booking_service'
Payment             = require 'app/modules/wizard_booking/wizard_booking_payment'
PayWithPackage      = require 'app/modules/wizard_booking/wizard_booking_pay_with_package'
BookingSuccess      = require 'app/modules/wizard_booking/wizard_booking_success'
CreateProfile       = require 'app/modules/wizard_booking/wizard_booking_create_profile'
SwitchProfile       = require 'app/modules/wizard_booking/wizard_booking_switch_profile'
LocationSelect      = require 'app/modules/wizard_booking/wizard_booking_location_select'
IntroductorySession = require 'app/modules/wizard_booking/wizard_booking_introductory_session'
SetPassword         = require 'app/modules/wizard_booking/wizard_booking_set_password'

Device = require 'utils/device'

isMobile = require 'isMobile'

class Router extends Abstract

	routes :
		'wizard/booking/introductory_session' : 'introductorySessionStep'
		'wizard/booking/pick_your_time'       : 'pickYourTimeStep'
		'wizard/booking/payment'              : 'paymentStep'
		'wizard/booking/location_select'      : 'locationSelectStep'
		'wizard/booking/pay_with_package'     : 'payWithPackageStep'
		'wizard/booking/success'              : 'successStep'
		'wizard/booking/profile_client_create': 'clientCreateStep'
		'wizard/booking/profile_client_choice': 'clientChoiceStep'
		'wizard/booking/profile_client_set_password': 'clientSetPassword'

	initialize: (options) ->
		super

		@wizardModel = options.model
		@wizardView = options.view

		@$dom =
			container : @wizardView.$el.$container
			content   : @wizardView.$el.$content
			sidebar   : @wizardView.$el.$sidebar

		# array for all inited instances
		@instances = []

		@bind()
		@start()

	updateScrollBar  : -> @
	destroyScrollBar : -> @
	initScrollBar    : -> @

	start: ->
		@openStep '' if @wizardView.place is 'page' # dirty hack to start app on page
		@openStep @wizardModel.get('current_step.slug')

	hideSidebar: ->
		@$dom.sidebar.addClass 'm-hide'

	showSidebar: ->
		@$dom.sidebar.removeClass 'm-hide'

	openStep: (step) ->
		debugLog('open step ' + step, 'wizard_booking.router')
		@navigate "wizard/booking/#{step}", {trigger: true, replace: true}

		# temporapy, we should open page version of wizard on mobile
		if @wizardView.place is 'popup' and Device.isMobile()
			window.location.reload()

	introductorySessionStep: ->
		@hideSidebar()
		@wizardModel.set 'canGoAhead', true

		@instances.push introductorySession = new IntroductorySession.App()

		@wizardView.$el.$content.html introductorySession.view.render().el
		Wizard.trigger 'step:changed', 'introductory_session'

	## Step: 1
	pickYourTimeStep: ->
		@hideSidebar()

		@addGaTag 'wizard.booking.pick_time'

		@wizardModel.set 'canGoAhead', true

		@instances.push bookAppointment = new BookAppointment.App
			type: 'wizard'

		@wizardView.$el.$content.html bookAppointment.view.render().el
		Wizard.trigger 'step:changed', 'pick_time'

	## Step: 3a
	paymentStep: ->
		@showSidebar()

		@addGaTag 'wizard.booking.payment'

		@wizardModel.set 'canGoAhead', true
		@instances.push payment = new Payment.App()

		@wizardView.$el.$content.html payment.view.render().el
		Wizard.trigger 'step:changed', 'payment'

	## Step: 3b
	payWithPackageStep: ->
		@showSidebar()

		@addGaTag 'wizard.booking.pay_with_package'

		@wizardModel.set 'canGoAhead', true
		@instances.push payment = new PayWithPackage.App()

		@wizardView.$el.$content.html payment.view.render().el
		Wizard.trigger 'step:changed', 'pay_with_package'

	## Step: 5
	successStep: ->
		@hideSidebar()
		@addGaTag 'wizard.booking.success'

		@wizardModel.set 'canGoAhead', true
		@wizardModel.set 'isSuccessStep', true
		@wizardModel.set 'exitWithoutDeletingAppointment', true
		@instances.push success = BookingSuccess.App()

		@wizardView.$el.$content.html success.view.render().el
		Wizard.trigger 'step:changed', 'success'

	clientCreateStep: ->
		@hideSidebar()
		@addGaTag 'wizard.booking.client_create'

		@wizardModel.set 'canGoAhead', true
		@wizardModel.set 'exitWithoutDeletingAppointment', true
		@instances.push createProfile = new CreateProfile.App()

		@wizardView.$el.$content.html createProfile.view.render().el
		Wizard.trigger 'step:changed', 'client_create'

	clientChoiceStep: ->
		@hideSidebar()
		@addGaTag 'wizard.booking.client_choice'

		@wizardModel.set 'canGoAhead', false
		@wizardModel.set 'exitWithoutDeletingAppointment', true
		@instances.push switchProfile = new SwitchProfile.App()

		@wizardView.$el.$content.html switchProfile.view.render().el
		Wizard.trigger 'step:changed', 'client_choice'

	locationSelectStep: ->
		@wizardModel.set 'canGoAhead', false
		@instances.push locationSelect = new LocationSelect.App
			type   : 'wizard'
			wizard : 'booking'

		@wizardView.$el.$content.html locationSelect.view.render().el

		Wizard.trigger 'step:changed', 'location_select'

	clientSetPassword: ->
		@instances.push setPassword = new SetPassword.App()

		@wizardView.$el.$content.html setPassword.view.render().el
		Wizard.trigger 'step:changed', 'client_set_password'

module.exports = Router

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	className: 'wizard_booking--step m-set_password'

	tagName: 'form'

	events :
		'submit'         : 'submitForm'
		'keypress input' : 'detectKey'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@addStickit()
		@addValidation()

	addListeners: ->
		@listenTo Wizard, 'proceed', @submitForm

	addStickit: ->
		@bindings =
			'[name="password"]' : 'password'
			'[name="password_confirm"]'  : 'password_confirm'
			'[name="acceptTerms"]': 'acceptTerms'

	submitForm: (e) ->
		if e? then e.preventDefault?()
		errors = @model.validate()
		return if errors

		Wizard.loading()
		$.when(
			@model.save()
		).then(
			(res) =>
				Wizard.ready()
				Wizard.trigger 'request_next_step'
		).fail(
			-> Wizard.ready()
		)

	render: ->
		data =
			terms: window.location.origin + '/terms'

		@$el.html SetPassword.Templates.BaseWizard(data)
		@stickit()
		@

	detectKey: (e) ->
		# enter key
		if e.keyCode is 13 then @$el.trigger('submit')

module.exports = View

getApiRoute = require 'hp.api'
DeepModel = require 'backbone.deepmodel'

class Model extends DeepModel

	defaults :
		payment_method : ''
		credit_card_id : null
		coupon         :
			applied_coupons : []
			entered_coupon  : null
			display_form    : false

	initialize: (options) ->
		@bookingType = options.bookingType
		@getData()
		@setAppliedCoupons()

	getData: ->
		return unless Wizard.model.get('data.appointment_id') and @bookingType is 'booking'
		$.ajax
			url    : getApiRoute 'ajax-provider-appointment',
				appointmentId : Wizard.model.get('data.appointment_id')
			method : 'get'

	setAppliedCoupons: ->
		return unless Wizard.model.get 'data.order.line_items.coupons'
		@set 'coupon.applied_coupons', Wizard.model.get('data.order.line_items.coupons').map (coupon) ->
			{
				id              : coupon.id
				amount          : coupon.amount
				type            : coupon.payload.type
				description     : coupon.payload.description
			}

	createOrder: ->
		data = @getOrderData()
		data._token = window.GLOBALS._TOKEN
		$.ajax
			url: getApiRoute('api-orders')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify(data)
			success: (res) =>
				Wizard.model.set 'data.payment_method', @get('payment_method')
				Wizard.model.set 'data.credit_card_id', @get('credit_card_id')
				Wizard.model.set 'data.order', res.data # save the order

	bookOrder: ->
		data = @getBookingData()
		$.ajax
			url         : getApiRoute('api-order-book', {orderId : Wizard.model.get('data.order.id')})
			method      : 'post'
			type        : 'json'
			contentType : 'application/json; charset=utf-8'
			data        : JSON.stringify(data)


	getBookingData: ->
		data =
			payment_method : @get('payment_method')
			_token         : window.GLOBALS._TOKEN

		if Wizard.model.get('auto_renew')
			data.auto_renew = true

		data

	getOrderData: ->
		data =
			profile_id : GLOBALS._PID
			items      : []

		if @get('credit_card_id')
			data.card_id = @get('credit_card_id')

		@appendRecurringOrderData(data)
		@appendBookingOrderData(data)

		_.each @get('coupon.applied_coupons'), (coupon) ->
			data.items.push
				id: coupon.id
				name: 'coupon'

		data

	appendBookingOrderData: (data) ->
		return if @bookingType is 'recurring'
		if Wizard.model.get('data.selected_time.package.package_id')
			data.items.push
				id   : Wizard.model.get('data.selected_time.package.package_id')
				name : 'package'
		data.items.push
			id: Wizard.model.get('data.appointment_id')
			name: 'appointment'
		return

	appendRecurringOrderData: (data) ->
		return unless @bookingType is 'recurring'
		selectedTime = Wizard.model.get('data.selected_time')
		data.items.push
			id   : selectedTime.package_id
			name : 'package'
		_.each selectedTime.availabilities, (availability) ->
			item =
				name            : 'appointment_to_lock'
				availability_id : availability.availability_id
				from            : availability.from
				session_id      : selectedTime.session_id
			if selectedTime.client_location_id?
				item.location_id = selectedTime.client_location_id
			data.items.push item
		return

	# getting coupon ID by name
	addCoupon: ->
		couponName = @get('coupon.entered_coupon')
		$.ajax
			url         : getApiRoute('ajax-coupon-get', {coupon : couponName})
			data        :
				types : ['order']
				for   : 'client'
			method      : 'get'
			type        : 'json'
			contentType : 'application/json; charset=utf-8'
			success     : (res) =>
				if @isCouponApplied(res.data.id)
					message = "Coupon code <b>#{couponName}</b> already applied"
					vexDialog.buttons.YES.text = 'Ok'
					vexDialog.alert
						message: message
					return
				@applyCoupon(res.data.id)

	# checking for applied coupons
	isCouponApplied: (couponId) ->
		applied = false
		_.each @get('coupon.applied_coupons'), (coupon) ->
			applied = true if couponId is coupon.id
		applied

	# applying coupon to the order
	applyCoupon: (id) ->
		Wizard.loading()
		if Wizard.model.get 'data.order'
			data =
				items: []
			data.items.push
				id: id
				name: 'coupon'
			data._token = window.GLOBALS._TOKEN

			$.ajax
				url: getApiRoute('api-order', {orderId: Wizard.model.get 'data.order.id'})
				method: 'put'
				type: 'json'
				contentType: 'application/json; charset=utf-8'
				data: JSON.stringify(data)
				success: (res) =>
					Wizard.ready()
					Wizard.model.set 'data.order', res.data # save updated order
					Wizard.trigger 'save_step' # saving wizard data on server (in case when user refreshes the page on current step)
					@setCoupons(res.data.line_items.coupons) # save coupons in current model
					@trigger 'coupon:added'

		else
			data = @getOrderData()
			data.items.push
				id: id
				name: 'coupon'
			data._token = window.GLOBALS._TOKEN

			# creating pre-order to check validity and store the response
			$.ajax
				url: getApiRoute('api-orders')
				method: 'post'
				type: 'json'
				contentType: 'application/json; charset=utf-8'
				data: JSON.stringify(data)
				success: (res) =>
					Wizard.ready()
					Wizard.model.set 'data.order', res.data # save the order
					@setCoupons res.data.line_items.coupons # save coupons in current model
					@trigger 'coupon:added'

	setCoupons: (coupons) ->
		couponsArr = coupons.map (coupon) ->
			{
				id              : coupon.orderItemEntity.entity_id
				amount          : coupon.amount
				type            : coupon.payload.type
				description     : coupon.payload.description
			}
		@set 'coupon.applied_coupons', couponsArr
		@set 'coupon.display_form', false

	removeCoupon: ->
		@set 'coupon.applied_coupons', []
		@set 'coupon.entered_coupon', ''
		@set 'coupon.display_form', true
		Wizard.model.set 'data.order', null # to create new order with the next coupon
		@trigger 'coupon:removed'

module.exports = Model

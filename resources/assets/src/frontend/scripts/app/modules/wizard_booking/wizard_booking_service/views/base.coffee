Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
BookingData = require 'app/modules/wizard_booking/utils/booking_data'
ScrollToEl = require 'app/modules/wizard_booking/utils/scroll_to_element'
vexDialog = require 'vexDialog'

require 'backbone.validation'
require 'backbone.stickit'

class AbstractView extends Backbone.View

	className: 'wizard_booking--step m-service'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@instances = []

		@prepareStep()

	prepareStep: ->
		Wizard.view.toggleFooter true

		if @model.get('appointment_id')
			@model.set 'service_confirmed', true
			@model.set 'location_confirmed', true

		Wizard?.model?.set 'proceedLabel', do =>
			if @model.get('service_confirmed') && @model.get('location_confirmed')
				'Proceed to Book'
			else
				'Continue'

	bind: ->
		@listenTo @model, 'change:from change:availability_id change:location_id change:current_session_id', @prepareStep
		@listenTo @model, 'change:service_confirmed change:location_confirmed change:current_service_id change:current_session_id', @centerPopup

		@listenTo @, 'rendered', =>
			if @model.shouldFetchServices()
				@listenTo @model, 'services:ready', => @doWhenServicesReady()
			else
				@doWhenServicesReady()

		@listenTo Wizard, 'proceed', @proceed

		# trigger next step after the appointment was created in wizard
		@listenTo @model, 'booked', =>
			Wizard.model.set 'data', BookingData.getBookingData(@model)
			$.when(
				Wizard.model.saveWizardData()
			).then(
				=>
					Wizard.model.set
						'data.resetToFirstStep' : 'recalculate'
						'isSwitchedAppointment' : true
			).then(
				=> Wizard.model.getInitialWizardData()
			).then(
				=> Wizard.trigger 'next_step'
			)

		@listenTo @model, 'data:ready', =>
			@model.trigger 'change:location_id' #forcing to update timezone

		@bindings =
			'[data-wizard-timezone]':
				observe: 'location_id'
				onGet: (val) -> @model.getCurrentLocation()?.timezone.replace(/_/g, ' ') if val

			'[data-booking-location-select]':
				classes:
					'm-hide':
						observe: ['service_confirmed']
						onGet: (val) -> !val[0]

			'[data-booking-datetime-select]':
				classes:
					'm-hide':
						observe: ['service_confirmed', 'location_confirmed']
						onGet: (val) -> !val[0] or !val[1]

	doWhenServicesReady: ->
		@updateWizardAdditionalData()
		@listenTo @model, 'change:date change:from change:location_id change:current_service_id change:current_session_id change:current_package_id change:availability_id', @updateWizardAdditionalData
		@appendParts()
		@

	# set data to wizard's info sidebar
	updateWizardAdditionalData: ->
		Wizard.model.set BookingData.getAdditionalData(@model)
		@

	render: ->
		@$el.html WizardBookService.Templates.Base()
		@delegateEvents()
		@cacheDom()
		@bind()
		@stickit()
		@trigger 'rendered'
		@

	cacheDom: ->
		@$el.$service = @$('[data-booking-service]')
		@$el.$packages = @$('[data-booking-package]')
		@$el.$locations = @$('[data-booking-location]')
		@$el.$datepicker = @$('[data-booking-date]')
		@$el.$schedule = @$('[data-booking-time]')

		@$el.$blockLocation = @$('[data-booking-location-select]')
		@$el.$blockDatetime = @$('[data-booking-datetime-select]')

		@

	appendParts: ->
		@appendService()
		@appendDatepicker()
		@appendSchedule()
		@appendPackages()
		@appendLocations()
		@centerPopup()
		@

	appendService : ->
		@instances.push @serviceView = new WizardBookService.Views.Service
			el        : @$el.$service
			baseView  : @
			baseModel : @model

	appendPackages : ->
		@instances.push @packagesView = new WizardBookService.Views.Packages
			el        : @$el.$packages
			baseView  : @
			baseModel : @model

	appendLocations : ->
		@instances.push @locationsView = new WizardBookService.Views.Locations
			el        : @$el.$locations
			baseView  : @
			baseModel : @model

	appendDatepicker : ->
		@instances.push @datepickerView = new WizardBookService.Views.Datepicker
			el        : @$el.$datepicker
			baseModel : @model
			baseView  : @

	appendSchedule : ->
		@instances.push @scheduleView = new WizardBookService.Views.Schedule
			el        : @$el.$schedule
			baseModel : @model
			baseView  : @

	proceed : ->
		# confirm location before submitting
		unless @model.get('service_confirmed')
			@model.set('service_confirmed', true)
			@scrollTo @$el.$blockLocation
			return
		# confirm location before submitting
		unless @model.get('location_confirmed')
			@model.set('location_confirmed', true)
			@scrollTo @$el.$blockDatetime
			return

		# finally save model and go to the next step
		Wizard.loading()
		$.when(
			@save()
		).then(
			=> Wizard.ready()
		).fail(
			=> Wizard.ready()
		)


	save: ->
		if @model.validateAvailability()
			@model.save()
		else
			if @model.get('errors') then @showError(@model.get('errors'))

	showError: (errors) -> vexDialog.alert errors

	centerPopup: ->
		setTimeout( ->
			Wizard.view.centerPopup()
		, 50)

	scrollTo: ($el) ->
		setTimeout( ->
			ScrollToEl $el
		, 50)




module.exports = AbstractView

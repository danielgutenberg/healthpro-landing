Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
getRoute = require 'config/url'

class View extends Backbone.View

	className: 'wizard_booking--step m-client_location'

	events:
		'click [data-location-add]': 'addLocation'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@instances = {}

		Wizard?.model?.set 'proceedLabel', 'Proceed to Payment'
		Wizard?.model?.set 'backLabel', 'Change Your Booking Details'
		Wizard?.model?.set 'isPaymentStep', false
		Wizard.view.toggleFooter true

		@showLoading()
		@bind()

	bind: ->
		@listenTo @collections.locations, 'data:ready', @hideLoading
		@listenTo @model, 'change:location_id', => @toggleGoAhead true
		@listenTo @model, 'error:generic', (msg) => @raiseGenericError msg
		@listenTo @model, 'error:service_area', (msg) => @raiseServiceAreaError msg
		@listenTo Wizard, 'proceed', @proceed
		@listenTo Wizard, 'back', =>
			Wizard.trigger 'prev_step'

		@listenToOnce @, 'rendered', =>
			GMaps.load().ready =>
				@cacheDom()
				@appendLocations()
				Wizard.view.centerPopup()

	proceed: ->
		@model.save()

	render: ->
		@$el.html WizardBookingLocationSelect.Templates.Base()
		@trigger 'rendered'

		@

	appendLocations: ->
		@instances.locations = new WizardBookingLocationSelect.Views.Locations
			collections: @collections
			baseView: @
			baseModel: @model

		@$el.$listing.append @instances.locations.el

	cacheDom: ->
		@$body = $('body')
		@$el.$main = @$el.find('[data-main]')
		@$el.$listing = @$el.find('[data-listing]')
		@$el.$side = @$el.find('[data-side]')
		@$el.$error = @$el.find('[data-generic-error]')

	addLocation: (e) ->
		e?.preventDefault()
		@initEditLocation new WizardBookingLocationSelect.Models.Location()

	cancelEditLocation: ->
		@hideSide()
		@instances.edit_location.remove() if @instances.edit_location
		@instances.edit_location = null
		@toggleEdit false

	initEditLocation: (model) ->
		@cancelEditLocation()
		@showSide()
		@instances.edit_location = form = new WizardBookingLocationSelect.Views.Form
			collections: @collections
			baseView: @
			model: model

		@toggleEdit true

	showLoading: -> Wizard.loading()
	hideLoading: -> Wizard.ready()

	hideSide: ->
		@$el.$main.removeClass('m-hide')
		@$el.$side.addClass('m-hide')
		@$el.$side.html ''

	showSide: ->
		@$el.$main.addClass('m-hide')
		@$el.$side.removeClass('m-hide')

	toggleGoAhead: (enable = null) ->
		if enable and @model.hasSelectedLocation()
			canGoAhead = true
		else
			canGoAhead = false
		Wizard.model.set 'canGoAhead', canGoAhead

	toggleEdit: (enabled = true) ->
		$('.radio--i').trigger('click');
		if enabled
			@$body.addClass 'm-wizard_edit'
			@toggleGoAhead false
		else
			@$body.removeClass 'm-wizard_edit'
			@toggleGoAhead true

	raiseGenericError: (message = null, type = 'error') ->
		if message
			@$el.$error.removeClass 'm-hide'
			@$el.$error.html "<div class='alert m-#{type} m-show'>#{message}</div>"
		else
			@$el.$error.addClass 'm-hide'
			@$el.$error.html ''

	raiseServiceAreaError: (message) ->
		chatUrl = getRoute('dashboard-conversation', {profileId: Wizard.model.get('data.provider_id')})
		message = "<p>#{message}</p>"
		message += "<p>If you would like to discuss this with the professional, you can send him a <a href='#{chatUrl}' target='_blank' rel='noopener'>chat</a></p>"

		@raiseGenericError message

	close: ->
		_.each @instances, (instance) ->
			if instance
				instance.close?()
				instance.remove?()
				instance = null
		@instances = null
		@model?.set('id', null).destroy()
		@remove()
		@

module.exports = View

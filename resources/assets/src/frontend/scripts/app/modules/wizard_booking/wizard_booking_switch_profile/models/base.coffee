DeepModel = require 'backbone.deepmodel'
getApiRoute = require 'hp.api'

class Model extends DeepModel

	defaults:
		'profile_id': null
		'profiles': []

	initialize: (attrs, options) ->
		@fetch()

	fetch: ->
		Wizard.loading()

		@xhr = $.ajax
			url: getApiRoute('ajax-profiles')
			method: 'get'
			success: (res) =>
				@fetchProfiles(res.data)

				@trigger 'data:ready'
				Wizard.ready()
				@

			error: (res) ->
				Wizard.ready()
				@


	fetchProfiles: (profiles) ->
		array = []
		profiles.forEach (profile) ->
			if profile.type == 'client'
				array.push profile

		@set('profiles', array)

	save: ->
		Wizard.loading()

		@xhr = $.ajax
			url: getApiRoute('ajax-auth-profile', {profileId: @get('profile_id')})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

			success: (res) =>
				Wizard.trigger 'request_next_step'
				Wizard.model.set 'canGoAhead', true
				Wizard.ready()
				GLOBALS._PID = @get('profile_id')
				Wizard.model.set
					'data.client_id': @get('profile_id')
				@

			error: (res) =>
				Wizard.ready()
				@

module.exports = Model

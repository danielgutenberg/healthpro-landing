Abstract = require 'app/modules/wizard/views/main_view'
BookingInfo = require 'app/modules/wizard_booking/wizard_booking_sidebar'
isMobile = require 'isMobile'

Handlebars = require 'handlebars'
TemplateLayout = Handlebars.compile require('text!../templates/layout.html')
ToggleEl = require 'utils/toggle_el'
vexConfirm = require 'app/modules/wizard_booking/utils/confirm'

getRoute = require 'hp.url'
getApiRoute = require 'hp.api'

class View extends Abstract

	events:
		'click [data-wizard-close]': 'closeWizard'
		'click [data-wizard-proceed]': 'proceed'
		'click [data-wizard-back]': 'back'

	startup: ->
		super
		@canExit = true
		@

	bind: ->
		super
		@listenTo Wizard, 'initial_data:ready', => @checkDataExistence() if @model.get('isFirstLoad')
		@listenTo Wizard, 'stack:loading', => @allowExit(false)
		@listenTo Wizard, 'stack:ready', => @allowExit(true)
		@

	# @TODO do we really need this??
	checkDataExistence: ->
		unless @model.has('data.provider_id')
			@unstickit()
			Alerts.addNew('error', 'not enough data for wizard')
			return false

		@initBookingInfo()
		true

	addStickit: ->
		@bindings =
			'[data-wizard-title]':
				observe: 'title'
			':el':
				classes:
					'm-success': 'isSuccessStep'

			'[data-wizard-proceed]':
				observe: 'proceedLabel'
				classes:
					'm-green': 'isPaymentStep'
				attributes: [{
					name: 'disabled'
					observe: 'canGoAhead'
					onGet: (val) -> if val? and val then !val else true
				}]

			'[data-wizard-back]':
				observe: 'backLabel'
				classes:
					'm-hide':
						observe: ['previous_step', 'disableBack']
						onGet: (val) ->
							return true if val[1] is true
							return if val[0]? then false else true


	renderLayout: ->
		@$el.html TemplateLayout()
		@cacheDom()
		@centerPopup()

	cacheDom: ->
		super
		@$el.$close = @$el.find('[data-wizard-close]')
		@$el.$back = @$el.find('[data-wizard-back]')
		@$el.$proceed = @$el.find('[data-wizard-proceed]')
		@$el.$footer = @$el.find('[data-wizard-footer]')
		@

	centerPopup: ->
		if @place is 'popup'
			$(document).trigger 'popup:center'

	showLoading: ->
		@$el.$loading.removeClass 'm-hide'
		@

	hideLoading: ->
		@$el.$loading.addClass 'm-hide'
		@

	initBookingInfo: ->
		@subViews.push @bookingInfo = new BookingInfo.App
			$container   : @$el.$sidebar
			bookingModel : @model
		@bookingInfo.view.render()
		@

	closeWizard: ->
		return unless @canExit
		if @place is 'popup'
			if @model.get 'exitWithoutDeletingAppointment'
				@closeWizardPopup()
			else
				@closeWithCallback =>
					Wizard.loading()
					$.when(@cancelAppointment()).then(
						=>
							@closeWizardPopup()
					)
			return

		if @place is 'page'
			Wizard.loading()
			if @model.get 'exitWithoutDeletingAppointment'
				location.href = @model.get('additional.professional.public_url') ? getRoute('dashboard-appointments')
			else
				@closeWithCallback =>
					location.href = @model.get('additional.professional.public_url') ? getRoute('dashboard-appointments')
			return

	closeWithCallback: (callback) ->
		vexConfirm 'Exiting the wizard will cancel your appointment<br>Are you sure you want to exit?', callback, 'Yes', 'Cancel'
		return

	closeWizardPopup: ->
		Wizard.close()
		window.popupsManager.closePopup('wizard_booking_popup')
		window.popupsManager.removePopup('wizard_booking_popup')
		if !window.GLOBALS._PID? and Wizard.model.get('profileId')?
			location.reload()

	allowExit: (allow = true) ->
		@$el.$close.prop 'disabled', !allow
		@canExit = allow
		@

	appendExitButton: -> @

	removeExitButton: -> @

	proceed: ->
		Wizard.trigger 'proceed', @model
		@

	back: ->
		Wizard.trigger 'back', @model
		@

	updateProceed: (label = 'proceed', classes = '') ->
		@$el.$proceed.html(label)
		if oldClasses = @$el.$proceed.data('classes')
			@$el.$proceed.removeClass oldClasses
		if classes
			@$el.$proceed.data('classes', classes)
			@$el.$proceed.addClass classes
		@

	updateBack: (label = 'proceed', classes = '') ->
		@$el.$back.html(label)
		if oldClasses = @$el.$back.data('classes')
			@$el.$back.removeClass oldClasses
		if classes
			@$el.$back.data('classes', classes)
			@$el.$back.addClass classes
		@

	cancelAppointment: (appointmentId) ->
		appointmentId =  @model.get('data.appointment_id')
		if appointmentId? and window.GLOBALS._PID
			$.ajax
				url         : getApiRoute('ajax-appointment', {appointmentId: appointmentId})
				method      : 'delete'
				type        : 'json'
				contentType : 'application/json; charset=utf-8'
				data        : JSON.stringify
					_token : window.GLOBALS._TOKEN
		else
			true

	toggleFooter: (display = null) -> ToggleEl @$el.$footer, display


module.exports = View

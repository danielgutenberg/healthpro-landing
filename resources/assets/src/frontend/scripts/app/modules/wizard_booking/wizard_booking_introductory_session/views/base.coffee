Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
BookingData = require 'app/modules/wizard_booking/utils/booking_data'
ScrollToEl = require 'app/modules/wizard_booking/utils/scroll_to_element'
getRoute = require 'hp.url'

require 'backbone.validation'

class View extends Backbone.View

	className : 'wizard_booking--step m-introductory_session'

	events :
		'click [data-send-message]'     : 'sendMessage'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@instances = []

		@prepareStep()

	prepareStep: ->
		Wizard.view.toggleFooter true

		if @model.get('appointment_id')
			@model.set 'introductory_confirmed', true
			@model.set 'service_confirmed', true
			@model.set 'location_confirmed', true

		Wizard?.model?.set 'proceedLabel', do =>
			if !@model.get('introductory_confirmed')
				'Book Now'
			else if @model.get('introductory_confirmed') && @model.get('service_confirmed') && @model.get('location_confirmed')
				'Proceed to Book'
			else
				'Continue'

		Wizard?.model?.set 'isPaymentStep', !@model.get('introductory_confirmed')

	addListeners: ->
		@listenTo @model, 'change:from change:availability_id change:location_id change:introductory_confirmed', @prepareStep
		@listenTo @model, 'change:introductory_confirmed change:service_confirmed change:location_confirmed', @centerPopup

		@listenTo @, 'rendered', =>
			if @model.shouldFetchServices()
				@listenTo @model, 'services:ready', => @doWhenServicesReady()
			else
				@doWhenServicesReady()

		@listenTo Wizard, 'proceed', @proceed

		# trigger next step after the appointment was created in wizard
		@listenTo @model, 'booked', =>
			Wizard.model.set 'data', BookingData.getIntroductoryData(@model)
			$.when(
				Wizard.model.saveWizardData()
			).then(
				=>
					Wizard.model.set
						'data.resetToFirstStep' : 'recalculate'
						'isSwitchedAppointment' : true
			).then(
				=> Wizard.model.getInitialWizardData()
			).then(
				=>
					Wizard.trigger 'next_step'
			)
		@

	bind: ->
		@bindings =
			'[data-wizard-timezone]':
				observe: 'location_id'
				onGet: (val) -> @model.getCurrentLocation()?.timezone.replace(/_/g, ' ') if val

			'[data-booking-introductory-intro]':
				classes:
					'm-hide':
						observe: ['introductory_confirmed']
						onGet: (val) -> val[0]

			'[data-booking-service-select]':
				classes:
					'm-hide':
						observe: ['introductory_confirmed']
						onGet: (val) -> !val[0]

			'[data-booking-location-select]':
				classes:
					'm-hide':
						observe: ['introductory_confirmed', 'service_confirmed']
						onGet: (val) -> !val[0] or !val[1]

			'[data-booking-datetime-select]':
				classes:
					'm-hide':
						observe: ['introductory_confirmed', 'service_confirmed', 'location_confirmed']
						onGet: (val) -> !val[0] or !val[1] or !val[2]
		@

	doWhenServicesReady: ->
		@updateWizardAdditionalData()
		@listenTo @model, 'change:date change:from change:location_id change:availability_id', @updateWizardAdditionalData
		@appendParts()
		@

	# set data to wizard's info sidebar
	updateWizardAdditionalData: ->
		Wizard.model.set BookingData.getAdditionalIntroductoryData(@model)
		@

	render: ->
		@$el.html WizardBookIntroductory.Templates.Base()
		@delegateEvents()
		@cacheDom()
		@addListeners()
		@bind()
		@stickit()
		@trigger 'rendered'
		@

	cacheDom: ->
		@$el.$service = @$('[data-booking-service]')
		@$el.$locations = @$('[data-booking-location]')
		@$el.$datepicker = @$('[data-booking-date]')
		@$el.$schedule = @$('[data-booking-time]')

		@$el.$blockIntro = @$('[data-booking-introductory-intro]')
		@$el.$blockService = @$('[data-booking-service-select]')
		@$el.$blockLocation = @$('[data-booking-location-select]')
		@$el.$blockDatetime = @$('[data-booking-datetime-select]')
		@

	appendParts: ->
		@appendService()
		@appendDatepicker()
		@appendSchedule()
		@appendLocations()
		@centerPopup()

	appendService : ->
		@instances.push @serviceView = new WizardBookIntroductory.Views.Service
			el        : @$el.$service
			baseView  : @
			baseModel : @model

	appendLocations : ->
		@instances.push @locationsView = new WizardBookIntroductory.Views.Locations
			el        : @$el.$locations
			baseView  : @
			baseModel : @model

	appendDatepicker : ->
		@instances.push @datepickerView = new WizardBookIntroductory.Views.Datepicker
			el        : @$el.$datepicker
			baseModel : @model
			baseView  : @

	appendSchedule : ->
		@instances.push @scheduleView = new WizardBookIntroductory.Views.Schedule
			el        : @$el.$schedule
			baseModel : @model
			baseView  : @

	proceed : ->
		# confirm location before submitting
		unless @model.get('introductory_confirmed')
			@model.set('introductory_confirmed', true)
			@scrollTo @$el.$blockService
			return

		# confirm location before submitting
		unless @model.get('service_confirmed')
			@model.set('service_confirmed', true)
			@scrollTo @$el.$blockLocation
			return
		# confirm location before submitting
		unless @model.get('location_confirmed')
			@model.set('location_confirmed', true)
			@scrollTo @$el.$blockDatetime
			return

		# finally save model and go to the next step
		Wizard.loading()
		$.when(
			@save()
		).then(
			=> Wizard.ready()
		).fail(
			=> Wizard.ready()
		)

	save: ->
		return @model.save() if @model.validateAvailability()
		@showError @model.get('errors') if @model.get('errors')
		return

	showError: (errors) -> vexDialog.alert errors

	sendMessage: ->
		Wizard.redirect getRoute('dashboard-conversation', {profileId: @model.getProviderId()})
		@

	centerPopup: ->
		setTimeout( ->
			Wizard.view.centerPopup()
		, 50)

	scrollTo: ($el) ->
		setTimeout( ->
			ScrollToEl $el
		, 50)

module.exports = View

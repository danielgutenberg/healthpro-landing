Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Moment = require 'moment'
Numeral = require 'numeral'
Formats = require 'config/formats'
HumanTime = require 'utils/human_time'
dateTimeParsers = require 'utils/datetime_parsers'
collectionUtils = require 'utils/collection_utils'
serviceFormatter = require 'utils/formatters/service'
Config = require 'app/modules/wizard_booking/config'

class Model extends Backbone.Model

	defaults :
		resetToFirstStep              : 'recalculate'
		bookedFrom                    : null
		provider_id                   : null
		availability_id               : null
		location_id                   : null
		current_service_id            : null # ID
		current_session_id            : null # ID duplicates session_id
		current_package_id            : null # ID
		certificate_id                : null
		services                      : []
		availabilities                : []
		date                          : null
		from                          : null
		appointment_from_professional : false
		appointment_from_dashboard    : false
		appointment_id                : null
		schedule_registration_id      : null
		# these fields are only used to toggle location & datetime blocks
		service_confirmed             : false
		location_confirmed            : false

	validation :
		current_service_id :
			required : true
			msg      : 'Select service'

		current_session_id :
			required : true
			msg      : 'Select session'

	initialize : (attrs, options) ->
		@type = options.type ? null
		# if presetData was passed to the model init it
		@setInitPresetData options.presetData
		@addListeners()
		@fetch()

	initCollections : ->
		@set 'scheduleCollection', new WizardBookService.Collections.Schedule [],
			model     : WizardBookService.Models.Time
			baseModel : @

	addListeners : ->
		@listenToOnce @, 'services:ready', => @setInitData()
		@listenToOnce @, 'data:ready', => @initCollections()

	setInitPresetData : (data) ->
		if data?
			@set
				resetToFirstStep              : data.resetToFirstStep
				appointment_from_professional : data.appointment_from_professional
				appointment_from_dashboard    : data.appointment_from_dashboard
				bookedFrom                    : data.bookedFrom
				services                      : collectionUtils.convertCollectionToArray data.services
				availabilities                : data.availabilities
				from                          : data.from
				date                          : if data.date? then data.date else dateTimeParsers.parseToFormat(data.from, 'YYYY-MM-DD')
				provider_id                   : data.provider_id
				location_id                   : +data.location_id
				appointment_id                : data.appointment_id
				availability_id               : data.availability_id
				certificate_id                : data.certificate_id
				current_package_id            : data.package_id
				current_service_id            : data.service_id
				current_session_id            : data.session_id
				timezone                      : data.timezone
		@

	fetch : ->
		@fetchServices()
		@fetchPackages()

	# fetch active packages for client
	fetchPackages: ->
		client_id = if Wizard.model.get('data.client_id') > 0 then Wizard.model.get('data.client_id') else GLOBALS._PID
		$.ajax
			url     : getApiRoute('ajax-profile-active-packages', {profileId : client_id})
			method  : 'get'

	# get provider services
	fetchServices : ->
		unless @shouldFetchServices()
			@trigger 'services:ready'
			return true

		@xhr = $.ajax
			url     : getApiRoute('ajax-provider-services', {providerId : @get('provider_id') || window.GLOBALS?.PROVIDER_ID})
			method  : 'get'
			success : (response) =>
				@set 'services', @formatServices(response.data)
				@trigger 'services:ready'

	formatServices : (services) ->
		formatted = []
		_.each services, (service) =>
			sessions = service.sessions
			return if sessions.length is 0
			formatted.push @formatService service
		serviceFormatter.detailedServicesName formatted

	formatService : (service) ->
		isIntroductory = false
		_.each service.sub_services, (subServ) ->
			if subServ.slug is Config.IntroductorySessionSlug
				isIntroductory = true

		if isIntroductory
			name = service.sessions[0].title
			detailedName = name
		else
			name = service.name
			detailedName = serviceFormatter.getServiceName service

		{
			service_id    : service.service_id
			name          : name
			detailed_name : detailedName
			sessions      : service.sessions
			locations     : service.locations
		}

	save : ->
		@xhr = $.ajax
			url         : getApiRoute('ajax-appointments')
			method      : 'post'
			type        : 'json'
			contentType : 'application/json; charset=utf-8'
			data        : JSON.stringify
				session_id      : @get('current_session_id')
				from            : @get('from')
				availability_id : @get('availability_id')
				type            : 'client'
				_token          : window.GLOBALS._TOKEN
			success     : (res) =>
				@set
					schedule_registration_id : res.data.schedule_registration_id
					appointment_id           : res.data.id
				@trigger 'booked'

	# set data that was passed to module
	setInitData : ->
		# do nothing if we got services without active single sessions
		return if @hasOnlyMonthlyPackages()

		if !@get('date') and @get('from')
			momentDate = dateTimeParsers.parseToMoment @get('from')
			@set 'date', momentDate.format('YYYY-MM-DD')

		switch @type
			when 'wizard'
				@setInitServices()
				@listenTo @, 'change:current_session_id', =>
					@resetAvailability()
					@resetLocation()
			when 'widget'
				@setInitServices()

		@trigger 'data:ready'

		@

	setInitServices : ->
		# if the current service and session ids were passed do nothing
		return if @get('current_service_id') and @get('current_session_id')
		return unless services = @getServicesWithActiveSessions()

		service = @getCurrentService()
		service = _.first services unless service
		session = @getServiceFirstActiveSession service

		@set 'current_service_id', service.service_id
		@set 'current_session_id', session.session_id

		@

	validateAvailability : ->
		return true if @get('availability_id') and @get('from')

		# if client goes back to first step after opening wizard from invitation in an email we don't require him
		# to pick a new time, he is allowed to keep on to the time that the professional set for him
		if @get('appointment_id')? and @get('appointment_from_professional') and @get('from')
			@trigger 'booked'
			return false

		@set('errors', 'You must select a time to proceed with your booking')
		return false

	# for search design, when you want to change time
	resetServiceToDefault : ->
		@set
			current_service_id : null
			current_session_id : null

	resetAppointmentToDefault : ->
		@set
			appointment_id           : null
			schedule_registration_id : null

	resetAvailability : ->
		@set
			from            : null
			availability_id : null

	resetLocation : ->
		@set
			location_id : null

	# method to return a vaild from date for the datepicker
	returnFromDate : (date) ->
		if date?
			return Moment(date).format('YYYY-MM-DD')

		if @get('date')?
			date = Moment(@get('date')).format('YYYY-MM-DD')
		else
			date = Moment().format('YYYY-MM-DD')

		date

	returnFromTime : -> '00:00'

	getService : (serviceId) -> _.findWhere @get('services'), service_id : serviceId

	getSession : (sessionId) ->
		sessionObject = null
		_.each @get('services'), (service) =>
			_.each service.sessions, (session) =>
				if session.session_id is sessionId
					sessionObject = session
		sessionObject

	getCurrentService : ->
		currentServiceId = @get('current_service_id')
		if currentServiceId
			return @getService currentServiceId
		null

	getCurrentSession : ->
		currentSessionId = @get('current_session_id')
		if currentSessionId
			return @getSession currentSessionId
		null

	getCurrentLocation : ->
		currentService = @getService @get('current_service_id')
		if currentService?
			_.findWhere currentService.locations, id : @get 'location_id'

	shouldFetchServices : ->
		# in wizard we have the full list of services only when we book from the profile page.
		shouldLoadServices = true
		shouldLoadServices = false if @get('bookedFrom') is 'profile'
		shouldLoadServices

	# make sure we return a proper JSON object
	toJSON : ->
		attributes = _.clone @attributes
		if attributes.scheduleCollection instanceof WizardBookService.Collections.Schedule
			attributes.scheduleCollection = @attributes.scheduleCollection.toJSON()
		attributes


	###
  		Helper services/sessions/packages methods
	###


	getServicesWithActiveSessions : ->
		_.filter @get('services'), (service) =>
			@getServiceActiveSessions(service).length > 0

	getServiceFirstActiveSession : (service) ->
		_.first @getServiceActiveSessions(service)

	getServiceActiveSessions : (service) ->
		unless _.isObject service
			service = @getService service

		return [] unless service
		_.filter service.sessions, active : 1

	getServiceMonthlyPackages : (service, includeFormattedValues = false) ->
		packages = []
		_.each service.sessions, (sess) ->
			_.each sess.packages, (pkg) ->
				return unless pkg.rules? and pkg.rules.length > 0
				rule = _.findWhere pkg.rules, rule : 'max_weekly'
				return unless rule

				servicePackage =
					session_id       : sess.session_id
					rule_id          : rule.id
					package_id       : pkg.package_id
					max_visits       : rule.value
					price            : pkg.price * 1
					duration         : sess.duration
					number_of_months : pkg.number_of_months

				if includeFormattedValues
					servicePackage.max_visits_label = do ->
						return 'unlimited visits per week' if rule.value is 0
						"#{rule.value} visit" + (if rule.value > 1 then "s" else "") + " per week"

					servicePackage.price_label = Numeral(pkg.price).format(Formats.Price.Default) + " / month"
					servicePackage.duration_label = HumanTime.minutes(sess.duration) + " session"
					servicePackage.number_of_months_label = "#{pkg.number_of_months} month" + (if pkg.number_of_months > 1 then "s" else "")

				packages.push servicePackage

		packages

	getServicesWithMonthlyPackages : ->
		_.filter @get('services'), (service) =>
			@getServiceMonthlyPackages(service).length > 0

	hasOnlyMonthlyPackages : ->
		@getServicesWithActiveSessions().length is 0

	hasMonthlyPackages : ->
		@getServicesWithMonthlyPackages().length > 0

	serviceHasMonthlyPackages : (service = null) ->
		if service is null
			service = @getCurrentService()

		hasMonthlyPackages = false
		_.each service.sessions, (session) =>
			if @sessionHasMonthlyPackages session
				hasMonthlyPackages = true

		hasMonthlyPackages

	sessionHasMonthlyPackages : (session = null) ->
		if session is null
			session = @getCurrentSession()
		# inactive sessions always have monthly packages
		return true if session.active is 0 and session.packages.length > 0
		# no packages
		return false if session.packages.length is 0
		_.where(session.packages, {number_of_visits : -1}).length > 0


	getSessionSimplePackages: (session = null) ->
		if session is null
			session = @getCurrentSession()

		return [] if session.packages.length is 0
		_.filter session.packages, (pkg) ->
			pkg.number_of_visits > 0

	sessionHasSimplePackages: (session = null) -> @getSessionSimplePackages(session).length > 0

module.exports = Model

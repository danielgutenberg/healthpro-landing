Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: ->
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-client-locations', {clientId: 'me'})
			method: 'get'
			success: (res) =>
				_.each res.data, (card) => @add @format(card)
				@trigger 'data:ready'
			error: => @trigger 'data:ready'

	format: (item) ->
		location =
			id: item.id
			name: item.name
			address: item.address.address
			address_id: item.address.address_id
			postal_code: item.address.postal_code
			country_code: item.address.country
			city: item.address.city
			province: item.address.province

		location

	isUnique: (modelId, field, value) ->
		models = _.reject @toJSON(), {id: modelId}
		duplicate = _.where models,
			"#{field}": value
		duplicate.length is 0

module.exports = Collection

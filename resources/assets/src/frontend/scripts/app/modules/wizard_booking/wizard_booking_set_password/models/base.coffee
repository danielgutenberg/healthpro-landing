Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		first_name: ''
		last_name: ''
		profile_type: 'client'

	validation:
		password: [
			{
				required: true
				msg: 'Please enter password'
			},
			{
				minLength: 7
				msg: 'Minimum 7 characters'
		  }
		]
		password_confirm:[
			{
				required: true
				msg: 'Please confirm password'
			},
			{
				equalTo: 'password'
				msg: 'The passwords does not match'
		  }
		]
		acceptTerms: [
			{
				required: true
				msg: 'Please accept Terms of Service to set your password'
			}
		]

	initialize: ->

	save: ->
		$.ajax
			url: getApiRoute('ajax-auth-set-password')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				newPassword: @get 'password'
				newPassword_confirmation: @get 'password_confirm'
				_token: window.GLOBALS._TOKEN

module.exports = Model

Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		first_name: ''
		last_name: ''
		profile_type: 'client'

	validation:
		first_name: [
			{
				required: true
				msg: 'Please enter First Name'
			},
			{
				pattern: /^[a-zA-Z'\s]*$/
				msg: 'Please enter a valid First Name'
		  }
		]
		last_name:[
			{
				required: true
				msg: 'Please enter Last Name'
			},
			{
				pattern: /^[a-zA-Z']+$/
				msg: 'Please enter a valid Last Name'
		  }
		]

	initialize: ->
		@getName()

	getName: ->
		$.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			success: (res) =>
				@set
					first_name: res.data.first_name
					last_name: res.data.last_name

	save: ->
		$.ajax
			url: getApiRoute('ajax-profiles')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify _.assign( @toJSON(), {_token: window.GLOBALS._TOKEN} )
module.exports = Model

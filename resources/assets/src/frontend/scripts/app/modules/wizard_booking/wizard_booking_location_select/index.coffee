Handlebars = require 'handlebars'

window.WizardBookingLocationSelect =
	Config:
		Messages:
			error: 'Something went wrong. Please reload the page and try again.'
			remove: 'Are you sure you want to remove this location?'
	Views:
		Base: require('./views/base')
		Locations: require('./views/locations')
		Location: require('./views/location')
		Form: require('./views/form')
	Collections:
		Locations: require('./collections/locations')
	Models:
		Base: require('./models/base')
		Location: require('./models/location')
	Templates:
		Base:  Handlebars.compile require('text!./templates/base.html')
		Location:  Handlebars.compile require('text!./templates/location.html')
		Form:  Handlebars.compile require('text!./templates/form.html')

class WizardBookingLocationSelect.App
	constructor: (options) ->
		@collections =
			locations: new WizardBookingLocationSelect.Collections.Locations [],
				model: WizardBookingLocationSelect.Models.Location

		# assign collection to the model. used in validation
		WizardBookingLocationSelect.Models.Location.prototype.collection = @collections.locations
		WizardBookingLocationSelect.Models.Base.prototype.collections = @collections

		_.extend options,
			collections: @collections
			model: new WizardBookingLocationSelect.Models.Base null,
				type: options.type ? 'wizard'
				wizard: options.wizard ? false

		@view = new WizardBookingLocationSelect.Views.Base options

	close: ->
		@view.close?()
		@view.remove?()

module.exports = WizardBookingLocationSelect

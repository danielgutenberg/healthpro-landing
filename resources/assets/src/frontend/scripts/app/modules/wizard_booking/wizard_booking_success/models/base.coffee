Backbone = require 'backbone'
getApiRoute = require 'hp.api'
datetimeParsers = require 'utils/datetime_parsers'
LocationFormatter = require 'utils/formatters/location'
ProfessionalFormatter = require 'utils/formatters/professional'
HumanTime = require 'utils/human_time'

class Model extends Backbone.Model

	initialize : (options) ->
		@bookingType = options.bookingType
		@set
			is_recurring : @bookingType is 'recurring'
			is_booking   : @bookingType is 'booking'

	fetch : ->
		if @bookingType is 'recurring'
			@fetchRecurring()
		else
			@fetchBooking()

	fetchBooking : ->
		@set
			duration     : HumanTime.minutes Wizard.model.get('data.selected_time.duration')
			service_name : Wizard.model.get('data.selected_time.service_name')

		@fetchAppointment Wizard.model.get('data.appointment_id')

	fetchRecurring : ->
		@set
			duration         : Wizard.model.get('data.selected_time.duration_label')
			max_visits       : Wizard.model.get('data.selected_time.max_visits_label')
			number_of_months : Wizard.model.get('data.selected_time.number_of_months_label')
			service_name     : Wizard.model.get('data.selected_time.service_name')

		orderAppointmentRow = _.findWhere Wizard.model.get('data.order.line_items.purchasables'), name : 'appointment'
		return unless orderAppointmentRow

		@fetchAppointment orderAppointmentRow.orderItemEntity.entity_id

	fetchAppointment : (appointmentId) ->
		$.ajax
			url     : getApiRoute('ajax-provider-appointment', {appointmentId : appointmentId})
			method  : 'get'
			success : (res) =>
				date = datetimeParsers.parseToMoment(res.data.appointment.date)
				@set
					appointment_pending    : false # handle it later
					date_time              : date.format()
					day                    : date.format('dddd')
					date                   : date.format('MMMM D, YYYY')
					time                   : date.format('hh:mm a')
					location_id            : res.data.location.id
					location_is_virtual    : res.data.location.location_type is 'virtual'
					location_is_home_visit : res.data.registration_location.location_type is 'home-visit'
					location_address       : LocationFormatter.getLocationAddressLabel res.data.location
					location_name          : LocationFormatter.getLocationName res.data.location
					gmaps_url              : LocationFormatter.getLocationGmapsUrl res.data.location
					professional_name      : ProfessionalFormatter.getProfessionalName res.data.provider
					professional_avatar    : ProfessionalFormatter.getProfessionalAvatar res.data.provider, true

module.exports = Model

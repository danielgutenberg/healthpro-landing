Handlebars = require 'handlebars'

# list of all instances
window.SetPassword = SetPassword =
	Views:
		Base: require('./views/base')
	Models:
		Base: require('./models/base')
	Templates:
		BaseWizard: Handlebars.compile require('text!./templates/base.html')

class SetPassword.App
	constructor: ->
		@model = new SetPassword.Models.Base()
		@view = new SetPassword.Views.Base
			model: @model

		return {
			model: @model
			view: @view
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = SetPassword

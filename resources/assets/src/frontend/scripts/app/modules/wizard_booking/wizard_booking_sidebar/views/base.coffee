Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Formats = require 'config/formats'
Numeral = require 'numeral'
HumanTime = require 'utils/human_time'
Moment = require 'moment'

require 'backbone.stickit'

class View extends Backbone.View

	className: 'wizard_booking--sidebar_info'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addStickit()
		@render()

	addStickit: ->
		@bindings =
			'[data-professional-name]':
				observe: ['additional.professional.name', 'additional.professional.title']
				onGet: (values) ->
					val = values[0]
					if values[1]
						val = "#{values[1]} #{val}"
					val
			'[data-professional-avatar]':
				observe: 'additional.professional.avatar'
				updateMethod: 'html'
				onGet: (val) ->
					if val
						val = "<img src='#{val}' alt=''>"
					val

			'[data-appointment-type]':
				observe: ['additional.location', 'additional.registration_location']
				updateMethod: 'html'
				onGet: (values) ->
					'' if values[0] and values[1] and values[0].id is values[1].id
					"<span>home visit</span>"

				classes:
					# registration location equals location meaning
					# that we don't have a client location set
					'm-hide':
						observe: ['additional.location.id', 'additional.registration_location.id']
						onGet: (values) -> !values[0] or !values[1] or values[0] is values[1]

			'[data-appointment-date]':
				observe: 'additional.appointment.date'
				updateMethod: 'html'
				onGet: (val) ->
					if val and date = Moment(val)
						val = '<strong>' + date.format('dddd') + '</strong>, ' + date.format('MMMM D, YYYY')
					else
						val = 'Select Date'
					val

			'[data-appointment-time]':
				observe: ['additional.appointment.time', 'additional.location.timezone']
				updateMethod: 'html'
				onGet: (val) =>
					return "Select Time" unless val[0]
					"<strong>#{val[0]} (#{val[1].replace(/_/g, ' ')})</strong>"

			'[data-appointment-location]':
				observe: ['additional.location.name', 'additional.location.address']
				updateMethod: 'html'
				onGet: (values) ->
					return "Select Location" unless values[0] and values[1]
					html ="<strong>#{values[0]}</strong>"
					if values[0] isnt "Virtual"
						html += " #{values[1]}"
					html

			'[data-appointment-service-name]':
				observe: 'additional.appointment.service_name'

			'[data-appointment-duration]':
				observe: 'additional.appointment.duration'
				onGet: (val) ->
					return "" unless val
					HumanTime.minutes val

			'[data-appointment-price]':
				updateMethod : 'html'
				observe      : 'data.selected_time'
				onGet: (val) ->
					if val.used_package
						html = "<strong class='m-credit'>1 Credit</strong>"
						html +=" (1 session x #{val.used_package.formatted_duration})"
					else
						html = "<strong>#{Numeral(val.total_price).format(Formats.Price.Default)}</strong>"
						if val.package
							html +=" (#{val.package.number_of_visits} sessions x #{val.package.formatted_duration})"
					html

			'[data-gift-certificate]':
				classes:
					'm-hide':
						observe: 'additional.certificate_id'
						onGet: (val) -> if val then false else true

			'[data-payment-info]':
				classes:
					'm-hide':
						observe: 'additional.payment_info'
						onGet: (val) -> if val then false else true

			'[data-payment-info-title]':
				updateMethod: 'html'
				observe: 'additional.payment_info.title'

			'[data-payment-info-content]':
				updateMethod: 'html'
				observe: 'additional.payment_info.content'
		@

	render: ->
		@$container.find('.wizard_booking--sidebar_info').remove()
		@$el.html BookingInfo.Templates.Base()
		@$el.appendTo @$container

		@delegateEvents()
		@stickit @bookingModel
		@

module.exports = View

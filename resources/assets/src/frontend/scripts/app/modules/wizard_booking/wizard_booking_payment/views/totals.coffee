Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Numeral = require 'numeral'
Formats = require 'config/formats'
HumanTime = require 'utils/human_time'
Device = require 'utils/device'
Config = require 'app/modules/wizard_booking/config'

class View extends Backbone.View

	className: 'wizard_booking--payment_totals'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()

	addListeners: ->
		@listenTo @, 'updated', @updateAdditionalData

	render: ->
		@updateTotals()
		@$el.html WizardBookingPayment.Templates.Totals @totals
		@trigger 'updated'
		@

	updateTotals: ->
		@totals =
			items       : []
			bookingType : @baseView.bookingType

		@recurringItem(@totals)
		@sessionItem(@totals)
		@discountItem(@totals)
		@calculateTotals(@totals)

		if Device.isMobile()
			@totals.info =
				title   : Config.Payment.SidebarInfo.Title
				content : Config.Payment.SidebarInfo.Content

		@totals

	sessionItem: (data) ->
		return if @baseView.bookingType is 'recurring'

		selectedTime = Wizard.model.get('data.selected_time')
		if selectedTime.package
			item = {
				label: "#{selectedTime.package.number_of_visits} sessions #{selectedTime.service_name} - #{selectedTime.package.formatted_duration}"
				value : selectedTime.package.formatted_price
				price : Numeral(selectedTime.package.price).value()
			}
		else
			item = {
				label : "1 session #{selectedTime.service_name} - " + HumanTime.minutes(selectedTime.duration)
				value : Numeral(selectedTime.price).format(Formats.Price.Default)
				price : Numeral(selectedTime.price).value()
			}
		data.items.push item
		return

	recurringItem: (data) ->
		return if @baseView.bookingType isnt 'recurring'
		selectedTime = Wizard.model.get('data.selected_time')
		item = {
			label : "<strong>#{selectedTime.service_name}</strong><br>#{selectedTime.duration_label} x #{selectedTime.max_visits_label}"
			value : Numeral(selectedTime.price).format(Formats.Price.Default)
			price : Numeral(selectedTime.price).value()
		}
		data.items.push item
		return

	discountItem: (data) ->
		coupon = @baseModel.get('coupon')
		unless coupon.applied_coupons?.length
			return

		applied = coupon.applied_coupons[0]

		amount = Numeral(applied.amount)
		item = {
			label: "Coupon #{coupon.entered_coupon?.toUpperCase()}"
			value : amount.format(Formats.Price.Default)
			price : amount.value()
		}
		data.items.push item
		return

	calculateTotals: (data) ->
		total = _.reduce data.items, (t, item) ->
			t += item.price
		, 0
		data.total = Numeral(_.max([total, 0])).format(Formats.Price.Default)
		return


	updateAdditionalData: ->
		Wizard.model.set 'data.selected_time.total_price', @totals.total

module.exports = View

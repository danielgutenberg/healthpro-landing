HumanTime = require 'utils/human_time'
Formats = require 'config/formats'
Numeral = require 'numeral'

module.exports = (baseModel) ->
	currentService = baseModel.getCurrentService()
	return {} unless currentService

	sessionWithOptions =
		opt_labels: []

	_.each currentService.sessions, (item) =>
		return unless item.active
		price = Numeral(item.price.replace(',', '')).format(Formats.Price.Default)
		sessionWithOptions.opt_labels.push(price)
		sessionWithOptions[price] = [
			{
				duration      : item.duration
				duration_text : HumanTime.minutes item.duration,
					hourLabel  : 'hr'
					hoursLabel : 'hrs'
				session_id    : item.session_id
			}
		]

	sessionWithOptions

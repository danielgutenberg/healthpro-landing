Backbone = require 'backbone'
Handlebars = require 'handlebars'

# list of all instances
window.WizardBookingSuccess = WizardBookingSuccess =
	Views     :
		Base : require('./views/base')
	Models    :
		Base : require('./models/base')
	Templates :
		Base : Handlebars.compile require('text!./templates/base.html')

# events bus
_.extend WizardBookingSuccess, Backbone.Events

class WizardBookingSuccess.App
	constructor : (options) ->
		options = _.extend {
			booking_type : 'booking'
		}, options

		@model = new WizardBookingSuccess.Models.Base
			bookingType : options.booking_type

		@view = new WizardBookingSuccess.Views.Base
			model       : @model
			bookingType : options.booking_type

		return {
			model : @model
			view  : @view
			close : @close
		}

	close : ->
		@view.close?()
		@view.remove?()

module.exports = WizardBookingSuccess

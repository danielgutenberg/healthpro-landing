Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexConfirm = require 'app/modules/wizard_booking/utils/confirm'
require 'backbone.stickit'

class View extends Backbone.View

	className: 'wizard_booking--client_location--list--item'

	events:
		'click [data-location-edit]': 'editLocation'
		'click [data-location-remove]': 'removeLocation'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@bind()
		@render()
		@cacheDom()
		@addListeners()

	addListeners: ->
		@listenTo @model, 'mark_selected', =>
			@$el.$radio.attr('checked', true).trigger 'change'

	bind: ->
		@bindings =
			'[data-location-name]': 'name'
			'[data-location-address]': 'full_address'

	cacheDom: ->
		@$el.$radio = @$el.find('input[type=radio]')
		@$el.$radio.on 'change', => @baseModel.set 'location_id', @model.get('id')

	render: ->
		@$el.html WizardBookingLocationSelect.Templates.Location()
		@parentView.$el.append @$el
		@stickit()

	editLocation: -> @baseView.initEditLocation @model

	removeLocation: ->
		callback = (value) =>
			return unless value
			@baseView.showLoading()
			$.when(
				@model.remove()
			).then(
				=>
					@baseView.hideLoading()

					if @model.get('id') is @baseModel.get('location_id')
						@baseModel.set 'location_id', null

					@model.set('id', null).destroy()
					@collections.locations.remove @model
					@parentView.subViews.splice @parentView.subViews.indexOf(@), 1
					@close()
			).fail(
				=> @baseView.hideLoading()
			)

		vexConfirm WizardBookingLocationSelect.Config.Messages.remove, callback, 'Yes', 'Cancel'
		@

	showError: -> alert WizardBookingLocationSelect.Config.Messages.error

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Config = require 'app/modules/wizard_booking/config'

require 'backbone.stickit'

class View extends Backbone.View

	className: 'wizard_booking--cards_manager'

	events:
		'click .wizard_booking--cards_manager--add_another': 'showNewCardForm'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base )

		@subViews = {}
		@collection = options.collection
		@model = options.model
		@isFormOpened = false

		@addStickit()
		@bind()

	addStickit: ->
		@bindings =
			'[name="card"]': 'card_id'

	bind: ->
		@listenTo @collection, 'data:ready', @selectCard
		@listenTo @collection, 'add', @addCard
		@

	addCard: (model) ->
		card = new WizardCardsManager.Views.Card
			model: model
			baseView: @

		@subViews[model.get('id')] = card

		@$el.$cards.$list.append card.render().el

		@stickit()
		@

	removeCard: (model) ->
		# a bit dirty but should work for now
		@hideCardForm()
		@collection.remove model
		@subViews[model.get('id')].destroy()
		delete @subViews[model.get('id')]
		model = null
		if @collection.length
			@stickit()
			@selectFirst()
		else
			@showNewCardForm()
		@

	initAddCardForm: (model) ->
		model = new WizardCardsManager.Models.Card() unless model
		@cardForm = new WizardCardsManager.Views.CardForm
			baseView: @
			model: model
			collection: @collection

		window.WizardCardsManager.trigger 'card:edit', model

		@$el.$formWrap.html @cardForm.render().el

	showCardForm: ->
		@$el.$formWrap.removeClass('m-hide')
		@hideList()
		@isFormOpened = true
		@

	showNewCardForm: ->
		@initAddCardForm()
		@showCardForm()
		@

	showEditCardForm: (model) ->
		@initAddCardForm(model)
		@showCardForm()
		@

	hideCardForm: ->
		@$el.$formWrap.addClass('m-hide')
		@cardForm.remove()
		@showList()
		@isFormOpened = false
		window.WizardCardsManager.trigger 'card:close'
		@

	showList: ->
		@$el.$cards.removeClass('m-hide')
		@

	hideList: ->
		@$el.$cards.addClass('m-hide')
		@

	selectFirst: ->
		@$el.$cards.$list
			.find('.wizard_booking--cards_manager--cards_item:first-child input')
			.prop('checked', true)
			.trigger('change')
		@

	selectLast: ->
		@$el.$cards.$list
			.find('.wizard_booking--cards_manager--cards_item:last-child input')
			.prop('checked', true)
			.trigger('change')
		@

	render: ->
		@$el.html WizardCardsManager.Templates.Base()

		@stickit()
		@cacheDom()
		@initCollection()
		@

	initCollection: ->
		return unless @collection.length
		@collection.forEach (model) =>
			@addCard(model)

	cacheDom: ->
		@$el.$cards = @$('.wizard_booking--cards_manager--cards')
		@$el.$cards.$list = @$('.wizard_booking--cards_manager--cards_list')
		@$el.$formWrap = @$('.wizard_booking--cards_manager--form_wrap')
		@$el.$addCard = @$('.wizard_booking--cards_manager--add_another')
		@$el.$addCard.addClass('m-hide') if Config.Payment.CardManager.MaxCards < 2

		@

	selectCard: ->
		if @collection.length
			@stickit()
			@selectFirst()
		else
			@showNewCardForm()

module.exports = View

Backbone = require 'backbone'
Handlebars = require 'handlebars'

# list of all instances
window.WizardCardsManager = WizardCardsManager =
	Views       :
		Base     : require('./views/base')
		Card     : require('./views/card')
		CardForm : require('./views/card_form')
	Models      :
		Base : require('./models/base')
		Card : require('./models/card')
	Collections :
		Cards : require('./collections/cards')
	Templates   :
		Base     : Handlebars.compile require('text!./templates/base.html')
		Card     : Handlebars.compile require('text!./templates/card.html')
		CardForm : Handlebars.compile require('text!./templates/credit_form.html')

# events bus
_.extend WizardCardsManager, Backbone.Events

class WizardCardsManager.App
	constructor: ->
		@model = new WizardCardsManager.Models.Base()

		@view = new WizardCardsManager.Views.Base
			model      : @model
			collection : new WizardCardsManager.Collections.Cards null, {model : WizardCardsManager.Models.Card}

		{
			model : @model
			view  : @view
			close : @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = WizardCardsManager

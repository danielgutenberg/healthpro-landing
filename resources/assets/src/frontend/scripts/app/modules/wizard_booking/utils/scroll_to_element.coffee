ScrollToEl = require 'utils/scroll_to_element'

module.exports = ($el, offset = 80) ->
	$parent = $('#wizard_booking_popup')
	$parent = $('html,body') unless $parent.length

	ScrollToEl $el, offset, $parent

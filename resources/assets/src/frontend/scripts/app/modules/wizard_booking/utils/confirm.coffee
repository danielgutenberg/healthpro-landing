vexDialog = require 'vexDialog'

module.exports = (msg, callback, yesLabel, noLabel) ->
	vexDialog.confirm
		buttons  : [
			$.extend {}, vexDialog.buttons.YES, {text : yesLabel}
			$.extend {}, vexDialog.buttons.NO, {text : noLabel}
		]
		message  : 'Exiting the wizard will cancel your booking <br>Are you sure you want to exit?'
		callback : (value) ->
			return unless value
			callback()

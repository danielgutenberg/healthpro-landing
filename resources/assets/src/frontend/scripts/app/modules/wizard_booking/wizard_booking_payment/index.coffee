Handlebars = require 'handlebars'

window.WizardBookingPayment = WizardBookingPayment =
	Views       :
		Base           : require('./views/base')
		Coupon         : require('./views/coupon')
		Totals         : require('./views/totals')
		PaymentMethods : require('./views/payment_methods')
		PaymentDetails : require('./views/payment_details')
	Models      :
		Base : require('./models/base')
	Collections :
		PaymentMethods : require('./collections/payment_methods')
	Templates   :
		Base           : Handlebars.compile require('text!./templates/base.html')
		Coupon         : Handlebars.compile require('text!./templates/coupon.html')
		Totals         : Handlebars.compile require('text!./templates/totals.html')
		PaymentMethods : Handlebars.compile require('text!./templates/payment_methods.html')
		PaymentDetails : Handlebars.compile require('text!./templates/payment_details.html')

class WizardBookingPayment.App
	constructor : (options) ->

		options = _.extend {
			booking_type : 'booking'
		}, options

		@model = new WizardBookingPayment.Models.Base
			bookingType : options.booking_type

		@collections =
			paymentMethods : new WizardBookingPayment.Collections.PaymentMethods()

		@view = new WizardBookingPayment.Views.Base
			model       : @model
			collections : @collections
			bookingType : options.booking_type

		return {
			model : @model
			view  : @view
			close : @close
		}

	close : ->
		@view.close?()
		@view.remove?()

module.exports = WizardBookingPayment

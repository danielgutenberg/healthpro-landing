Backbone = require 'backbone'
Moment = require 'moment'

datetimeParsers = require 'utils/datetime_parsers'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		@baseModel = options.baseModel
		@nextAvailableDays = null

	beforeGetData: ->
		@xhr.abort() if @xhr # abort previous call
		@reset()
		@trigger 'data:loading'
		@

	afterGetData: ->
		@trigger 'data:ready'
		@

	getData: (date) ->
		@beforeGetData()
		date = @baseModel.returnFromDate(date)
		@xhr = $.ajax
			url: getApiRoute 'ajax-provider-availabilities',
				providerId: @baseModel.getProviderId()
			method: 'get'
			data:
				from: "#{date}T00:00"
				location_id: @baseModel.returnLocationsIds()
				length: @baseModel.get('session').duration
			success: (res) =>
				@setData res, date
				@afterGetData()
			error: =>
				@afterGetData()

	# set data from collection's ajax call
	setData: (response, date) ->
		locations = response.data?.results[0].locations ? null
		availabilities = response.data?.results[0].availabilities ? null

		@nextAvailableDays = response.data?.results[0].nextAvailableDays

		if availabilities?[date]?
			@pushAvailabilities availabilities[date], date, locations
		@trigger 'availabilities:ready'

	pushAvailabilities: (availabilities, date, locations) ->

		selectedFrom = datetimeParsers.parseToMoment @baseModel.get('from')
		selectedLocationId = @baseModel.get('location_id')

		preAvailabilities = availabilities.map (availability) =>
			{
				availability_id: availability.id ? availability.availability_id
				from: Moment.parseZone(availability.from).format()
				until: Moment.parseZone(availability.until).format()
				location_id: availability.location_id
				location_name: _.findWhere(@baseModel.get('locations'), {id: availability.location_id})?.name
				selected: do =>
					return unless availability.location_id is selectedLocationId
					return unless date is selectedFrom.format('YYYY-MM-DD')
					datetimeParsers.parseToFormat(availability.from, 'HH:mm') is selectedFrom.format('HH:mm')
			}

		@sortTime(preAvailabilities).forEach (availability) => @add availability

	sortTime: (availabilities) ->
		result = _.sortBy availabilities, (availability) ->
			return +Moment.parseZone( availability.from ).format('HH:mm').replace(':', '')
		unique = _.uniq result, (availability) ->
			return availability.from + availability.location_id
		return unique

module.exports = Collection

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
getRoute = require 'hp.url'
datetimeParsers = require 'utils/datetime_parsers'
ToggleEl = require 'utils/toggle_el'

class View extends Backbone.View

	events:
		'click [data-display-available-date]' : 'showNextAvailabilities'
		'click [data-send-message]'           : 'goToMessages'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@times = []
		@timeModels = []
		@scheduleCollection = @baseModel.get('scheduleCollection')

		@render()
		@addListeners()
		@cacheDom()

		@scheduleCollection?.getData()

	addListeners: ->
		# listenTo collection, didn't work in search, but did on profile page
		# i decided to use custom event for this
		# in collection i listen to add event and trigger a new event to eventBus with data
		# so dirty.
		@listenTo @scheduleCollection, 'add', (model) => @addTime(model)
		@listenTo @scheduleCollection, 'data:loading', =>
			@showLoading()
			@hideEmpty()
		@listenTo @scheduleCollection, 'data:ready', => @hideLoading()
		@listenTo @scheduleCollection, 'reset', => @reset()
		@listenTo @scheduleCollection, 'availabilities:ready', =>
			@filterLocations()
			@toggleScheduleMessage()
			@baseView.centerPopup()

		@listenTo @baseModel, 'change:location_id', =>
			@toggleScheduleMessage()
			@baseView.centerPopup()

	render: ->
		@$el.html WizardBookService.Templates.Schedule
			conversations_link: getRoute('dashboard-conversation', {profileId: window.GLOBALS.PROVIDER_ID})
		@

	cacheDom: ->
		@$el.$list = $('[data-times-list]', @$el)
		@$el.$loading = $('.loading_overlay', @$el)
		@$el.$listEmpty = $('[data-times-list-empty]', @$el)
		@$el.$listMore = $('[data-times-list-more]', @$el)
		@$el.$listNoNextAvailable = $('[data-times-no-availability]', @$el)
		@$el.$nextAvailablitiesDate = $('[data-times-next-appointment-date]', @$el)
		@

	addTime: (model) ->
		@timeModels.push model
		@times.push time = new WizardBookService.Views.Time
			model     : model
			baseModel : @baseModel
			baseView  : @baseView

		@$el.$list.append time.render().el

	reset: ->
		@removeTimes()
		@scheduleCollection.reset() if @scheduleCollection.length
		if @$el.$list? and @$el.$list.length
			@$el.$list.html('')

	removeTimes: ->
		if @times.length
			_.each @times, (time) => time.remove()
		@times = []
		@

	toggleScheduleMessage: ->
		isEmpty = do =>
			locationId = @baseModel.get('location_id')
			if locationId
				@scheduleCollection.where(location_id: locationId).length is 0
			else
				@scheduleCollection.length is 0
		if isEmpty
			@showEmpty()
		else
			@hideEmpty()

	showEmpty: ->
		@hideEmpty()
		locationId = @baseModel.get('location_id')
		if @scheduleCollection.nextAvailableDays?[locationId]
			if nextAvailableDate = datetimeParsers.parseToFormat @scheduleCollection.nextAvailableDays[locationId], 'MMMM Do'
				@$el.$nextAvailablitiesDate.text nextAvailableDate
				@toggleListEmpty true
			else
				@toggleNoAvailability true
		else
			@toggleNoAvailability true
		@toggleList false
		@

	toggleNoAvailability : (display = true) -> ToggleEl @$el.$listNoNextAvailable, display
	toggleList           : (display = true) -> ToggleEl @$el.$list, display
	toggleListEmpty      : (display = true) -> ToggleEl @$el.$listEmpty, display

	hideEmpty: ->
		@toggleList true
		@toggleNoAvailability false
		@toggleListEmpty false
		@

	showNextAvailabilities: ->
		locationId = @baseModel.get('location_id')
		@scheduleCollection.trigger 'show_next_availabilities', @scheduleCollection.nextAvailableDays[locationId]
		@

	filterLocations: (locationId) ->
		# don't show a message when we don't have times
		return unless @times.length

		locationId = @baseModel.get('location_id') unless locationId

		_.each @times, (time) -> time.filterTime time.model.get('location_id') is locationId or locationId is null

		shownTimes = @times.filter (time) -> !time.$el.hasClass 'm-hide'
		if shownTimes.length
			@hideEmpty()
		else
			@showEmpty()

	getFirstTimeLocation: -> @times[0].model.get 'location_id' if @times.length

module.exports = View

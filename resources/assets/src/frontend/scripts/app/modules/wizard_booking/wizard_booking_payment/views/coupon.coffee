Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'

require 'backbone.stickit'

class View extends Backbone.View

	className: 'wizard_booking--payment_coupon'

	events:
		'click [data-payment-coupons-add-coupon-btn]'    : 'displayForm'
		'click [data-payment-coupons-apply-coupon-btn]'  : 'applyCoupon'
		'click [data-payment-coupons-remove-coupon-btn]' : 'removeCoupon'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@bind()
		@addListeners()
		@render()

	render: ->
		@$el.html WizardBookingPayment.Templates.Coupon()
		@stickit @baseModel
		@

	addListeners: ->
		@listenTo window.WizardCardsManager, 'card:added', => @baseModel.save()
		@

	bind : ->
		@bindings =
			'[data-payment-coupons-applied-coupon-list]' :
				observe : 'coupon.entered_coupon'
				onGet   : (val) -> val?.toUpperCase()

			'[data-payment-coupons-add-coupon]'          :
				classes :
					'm-hide' :
						observe : 'coupon'
						onGet   : (val) -> val.applied_coupons.length || val.display_form
			'[data-payment-coupons-applied-coupon]'      :
				classes :
					'm-hide' :
						observe : 'coupon.applied_coupons'
						onGet   : (val) -> !val.length
			'[data-payment-coupons-form]'                :
				classes :
					'm-hide' :
						observe : 'coupon.display_form'
						onGet   : (val) -> if val then false else true
			'input[name="coupon_code"]'                  : 'coupon.entered_coupon'
			'.btn'                                       :
				attributes : [{
					name    : 'disabled'
					observe : 'coupon.entered_coupon'
					onGet   : (val) -> !val
				}]
		@

	displayForm: ->
		@baseModel.set 'coupon.display_form', true
		@baseModel.set 'coupon.applied_coupons', []
		@baseModel.set 'coupon.entered_coupon', ''
		@

	applyCoupon: ->
		Wizard.loading()
		$.when(
			@baseModel.addCoupon()
		).then(
			=>
				Wizard.ready()
		).fail(
			(res) =>
				Wizard.ready()
				message = 'This coupon code cannot be used with selected service'
				if res.responseJSON?.errors?.coupon?
					if res.responseJSON.errors.coupon.messages[0] == 'Coupon is not exists'
						message = "Coupon code <b>#{@baseModel.get('coupon.entered_coupon')}</b> is not vald";
					else
						message = res.responseJSON.errors.coupon.messages[0]
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: message
		)

	removeCoupon: ->
		Wizard.loading()
		$.when(
			@baseModel.removeCoupon()
		).then(
			=>
				Wizard.ready()
		)

module.exports = View

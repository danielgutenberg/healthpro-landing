Backbone = require 'backbone'

class View extends Backbone.View

	className: 'wizard_booking--success'

	initialize: (options) ->
		@model = options.model

		Wizard.view.toggleFooter(false)

		Wizard.view.centerPopup()

		@fetch()

	fetch: ->
		Wizard.loading()
		$.when(
			@model.fetch()
		).then(
			=>
				@render()
				Wizard.ready()
				Wizard.view.centerPopup()
				Wizard.model.resetWizardData()
		).fail(
			-> Wizard.ready()
		)

	render: ->
		@$el.html WizardBookingSuccess.Templates.Base @model.toJSON()
		@

module.exports = View

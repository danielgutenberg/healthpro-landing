Config = require '../config'

module.exports = (location, size = 'large') ->
	size = _.capitalize size
	switch location.location_type
		when 'office'
			return Config.Location.Icons[size].office
		when 'home-visit'
			return Config.Location.Icons[size].homeVisit
		when 'virtual'
			return Config.Location.Icons[size].virtual

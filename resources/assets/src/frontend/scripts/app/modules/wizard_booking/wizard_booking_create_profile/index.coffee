Handlebars = require 'handlebars'

# list of all instances
window.CreateProfile = CreateProfile =
	Views:
		Base: require('./views/base')
	Models:
		Base: require('./models/base')
	Templates:
		BaseWizard: Handlebars.compile require('text!./templates/base.html')

class CreateProfile.App
	constructor: ->
		@model = new CreateProfile.Models.Base()
		@view = new CreateProfile.Views.Base
			model: @model

		return {
			model: @model
			view: @view
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = CreateProfile

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
HumanTime = require 'utils/human_time'

class View extends Backbone.View

	className: 'wizard_booking--pay_with_package'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@addListeners()

		Wizard?.model?.set 'proceedLabel', 'Confirm Booking'
		Wizard?.model?.set 'backLabel', 'Change Your Booking Details'
		Wizard?.model?.set 'isPaymentStep', true
		Wizard.view.toggleFooter true

	addListeners: ->
		@listenTo Wizard, 'proceed', @proceed
		@listenTo Wizard, 'back', @back
		@

	proceed: ->
		Wizard.model.set 'next_step', 'success'
		Wizard.loading()
		$.when(
			@model.save()
		).then(
			=>
				@model.bookOrder()
		).then(
			=>
				Wizard.ready()
				Wizard.trigger 'next_step'
		).fail(
			->
				Wizard.ready()
		)

	back: ->
		Wizard.trigger 'prev_step'

	render: ->
		@model.on 'data:ready', =>
			@$el.html WizardBookingPayWithPackage.Templates.Base @getTemplateData()
			Wizard.view.centerPopup()
		@

	getTemplateData: ->
		service = @model.getService()
		session = @model.getSession()

		purchased = @model.get('sessions_purchased')
		left = @model.get('sessions_left')

		data =
			serviceName       : service.name
			duration          : HumanTime.minutes session.duration
			sessionsLeft      : if purchased is -1 then 'unlimited' else left
			sessionsPurchased : if purchased is -1 then 'unlimited' else purchased
			sessionsLabel     : 'session' + (if left == 1 then '' else 's')
		data


module.exports = View

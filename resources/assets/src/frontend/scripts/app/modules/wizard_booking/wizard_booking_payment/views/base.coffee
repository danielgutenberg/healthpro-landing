Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Config = require 'app/modules/wizard_booking/config'
ToggleEl = require 'utils/toggle_el'

require 'backbone.stickit'

class View extends Backbone.View

	className: 'wizard_booking--payment'

	bindings: []

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@subViews = []

		Wizard?.model?.set 'proceedLabel', 'Confirm Booking'
		Wizard?.model?.set 'backLabel', 'Change Your Booking Details'
		Wizard?.model?.set 'isPaymentStep', true
		Wizard.view.toggleFooter true

		@updateSidebarData()
		@addListeners()

	preload: ->
		Wizard.loading()
		Wizard.view.centerPopup()
		$.when(
			@collections.paymentMethods.fetch()
		).then(
			=>
				@initTotals()
				@initPaymentMethods()
				@initPaymentDetails()
				@initCoupons()
				Wizard.view.centerPopup()
		).then(
			=>
				Wizard.ready()
		)

	updateSidebarData: ->
		Wizard.model.set 'additional.payment_info',
			title   : Config.Payment.SidebarInfo.Title
			content : Config.Payment.SidebarInfo.Content
		@

	addListeners: ->
		@listenTo Wizard, 'proceed', @proceed
		@listenTo Wizard, 'back', =>
			@removePaypalButtonFromFooter()
			Wizard.trigger 'prev_step'

		@listenTo @model, 'coupon:added coupon:removed', =>
			if @totalsView
				@$el.$totals.html @totalsView.render().el
		@

	render: ->
		@$el.html WizardBookingPayment.Templates.Base()
		@stickit()
		@cacheDom()
		@raiseGenericError null

		@preload()
		@

	initTotals: ->
		unless @totalsView
			@instances.push @totalsView = new WizardBookingPayment.Views.Totals
				baseView  : @
				baseModel : @model
		@$el.$totals.html @totalsView.render().el
		@

	initPaymentMethods: ->
		# set payment method after fetch
		if Wizard.model.get('data.payment_method')
			@model.set 'payment_method', Wizard.model.get('data.payment_method')
		else
			@model.set 'payment_method', @collections.paymentMethods.findWhere({enabled: true}).get('type')

		@instances.push @paymentMethodsView = new WizardBookingPayment.Views.PaymentMethods
			baseView  : @
			baseModel : @model
		@$el.$paymentMethods.html @paymentMethodsView.el
		@

	initPaymentDetails: ->
		@instances.push @paymentDetailsView = new WizardBookingPayment.Views.PaymentDetails
			baseView  : @
			baseModel : @model
		@$el.$paymentDetails.html @paymentDetailsView.el
		@

	initCoupons: ->
		@subViews.push @couponView = new WizardBookingPayment.Views.Coupon
			baseView  : @
			baseModel : @model
		@$el.$coupons.append @couponView.el

	proceed: ->
		$.when(
			@paymentDetailsView.processPaymentMethod()
		).then(
			(processed) =>
				return false if processed is false
				@book()
		).fail(
			-> # do something on fail.
		)

	book: ->
		Wizard.loading()
		$.when(
			@model.createOrder()
		).then(
			=>
				@model.bookOrder()
		).then(
			=>
				Wizard.ready()
				# force success step
				Wizard.model.set 'next_step', 'success'
				Wizard.trigger 'next_step'
		).fail(
			(err) =>
				Wizard.ready()
				@errorHandler(err)
		)

	cacheDom: ->
		@$el.$totals = @$('[data-payment-totals]')
		@$el.$paymentMethods = @$('[data-payment-methods]')
		@$el.$paymentDetails = @$('[data-payment-details]')
		@$el.$coupons = @$('[data-payment-coupons]')
		@$el.$error = @$el.find('[data-error]')
		@

	raiseGenericError: (message = null) ->
		if message
			@$el.$error.removeClass 'm-hide'
			@$el.$error.html "<p>#{message}</p>"
		else
			@$el.$error.addClass 'm-hide'
			@$el.$error.html ''
		@


	errorHandler: (err) ->
		if err.responseJSON?.errors?.error? and err.responseJSON.errors.error.messages[0] == "Provider not specified his CC"
			vexDialog.alert
				message: "Please be in touch with your professional to complete this booking."


	removePaypalButtonFromFooter: ->
		ToggleEl Wizard.view.$el.$proceed, true
		Wizard.view.$el.$footer.find("##{Config.Payment.PaymentMethods.ButtonId}").remove()
		@

	displayPaypalButtonInFooter: ->
		@removePaypalButtonFromFooter()

		ToggleEl Wizard.view.$el.$proceed, false
		@$paypalBtn = $('<div id="' + Config.Payment.PaymentMethods.ButtonId + '"></div>')
		Wizard.view.$el.$footer.append(@$paypalBtn)
		@



module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
HumanTime = require 'utils/human_time'
Formats = require 'config/formats'
Numeral = require 'numeral'

class View extends Backbone.View

	className: 'wizard_booking--service_select'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@render()

	render: ->
		@$el.html WizardBookIntroductory.Templates.Service
			service_name     : @baseModel.get('service').name
			session_duration : HumanTime.minutes @baseModel.get('session').duration
			session_price    : do =>
				price = Numeral(@baseModel.get('session').price)
				p = ""
				if price.value()
					p += price.format(Formats.Price.Default)
				else
					p += "free"
				" / <i>#{p}</i>"
		@

module.exports = View

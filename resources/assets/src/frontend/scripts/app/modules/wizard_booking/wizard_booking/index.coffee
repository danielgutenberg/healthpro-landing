Abstract = require 'app/modules/wizard/abstract'

BookingWizardView = require('./views/base')
BookingWizardModel = require('./models/base')
BookingWizardRouter = require('./router/router')

###
  BOOKING WIZARD PRIVATE CLASS
  AVAILABLE THROUGH THE SINGLETON PATTERN
###
class BookingWizard extends Abstract

	inited: false

	defaults: ->
		{
			bookingData: null
			el: $('[data-wizard="booking"]')
			forcePage: false
		}

	init: (options = null) ->
		if @inited
			console.error 'BookingWizard has already been inited'
			return @

		@setOptions options if options

		@addListeners()

		@model = new BookingWizardModel
			data: @options.bookingData
			wizard_name: 'booking'

		@view = new BookingWizardView
			model: @model
			el: @options.el

		@inited = true
		@

	initRouter: ->
		super
		@router = new BookingWizardRouter
			model: @model
			view: @view
		@

class BookingWizardSingleton
	instance = null

	@get: (options = {}) ->
		instance ?= new BookingWizard options

	@destroy: ->
		instance = null

if $('[data-wizard="booking"]').length
	window.Wizard = BookingWizardSingleton.get()
	window.Wizard.init()

module.exports = BookingWizardSingleton

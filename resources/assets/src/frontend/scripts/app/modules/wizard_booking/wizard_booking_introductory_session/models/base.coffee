Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Moment = require 'moment'
Config = require 'app/modules/wizard_booking/config'

class Model extends Backbone.Model

	defaults : ->
		resetToFirstStep       : 'recalculate'
		client_id              : null
		provider_id            : null
		availability_id        : null
		location_id            : null
		service                : null
		session                : null
		services               : []
		availabilities         : []
		date                   : null
		from                   : null
		appointment_id         : null
		# these fields are only used to toggle location & datetime blocks
		introductory_confirmed : false
		service_confirmed      : false
		location_confirmed     : false

	initialize : (attrs, options) ->
		# if presetData was passed to the model init it
		@setInitPresetData options.presetData
		@addListeners()
		@fetchServices()

	addListeners : ->
		@listenToOnce @, 'services:ready', =>
			@initCollections()
			@trigger 'change:location_id' #forcing to update timezone

	initCollections : ->
		@set 'scheduleCollection', new WizardBookIntroductory.Collections.Schedule [],
			model     : WizardBookIntroductory.Models.Time
			baseModel : @
		@

	fetchServices : ->
		@xhr = $.ajax
			url     : getApiRoute 'ajax-provider-services',
				providerId : @getProviderId()
			method  : 'get'
			success : (response) =>
				@setIntroductoryServiceAndSessionFromResponse response.data
				@trigger 'services:ready'
		@

	shouldFetchServices : -> true

	setIntroductoryServiceAndSessionFromResponse : (data) ->
		_.each data, (serv) =>
			_.each serv.sub_services, (subServ) =>
				if subServ.slug is Config.IntroductorySessionSlug
					session = serv.sessions[0]
					@set 'service',
						service_id : serv.service_id
						tag_id     : subServ.id
						slug       : subServ.slug
						name       : session.title
					@set 'session',
						session_id : session.session_id
						duration   : session.duration
						price      : session.price
						title      : session.title
					@set 'locations', serv.locations
					# used when the back button is pressed from the payment step
					# we can't return the client to introductory session step
					# and instead we pass this service as the only one to pick_time step
					@set 'services', [
						{
							service_id    : serv.service_id
							name          : session.title
							detailed_name : session.title
							sessions      : serv.sessions
							locations     : serv.locations
						}
					]
		@

	getCurrentLocation : -> _.findWhere @get('locations'), id : @get 'location_id'

	setInitPresetData : (data) ->
		if data?
			@set
				resetToFirstStep              : data.resetToFirstStep
				introductory_confirmed        : data.introductory_confirmed ? false
				provider_id                   : data.provider_id
		@

	# method to return a vaild from date for the datepicker
	returnFromDate : (date) ->
		if date?
			return Moment(date).format('YYYY-MM-DD')
		if @get('date')?
			date = Moment(@get('date')).format('YYYY-MM-DD')
		else
			date = Moment().format('YYYY-MM-DD')
		date

	returnLocationsIds : -> _.pluck(@get('locations'), 'id').join()

	save : ->
		@xhr = $.ajax
			url         : getApiRoute('ajax-appointments')
			method      : 'post'
			type        : 'json'
			contentType : 'application/json; charset=utf-8'
			data        : JSON.stringify
				session_id      : @get('session').session_id
				from            : @get('from')
				availability_id : @get('availability_id')
				type            : 'client'
				_token          : window.GLOBALS._TOKEN
			success     : (res) =>
				@set 'appointment_id', res.data.id
				@trigger 'booked'

	validateAvailability : ->
		return true if @get('availability_id') and @get('from')

		# if client goes back to first step after opening wizard from invitation in an email we don't require him
		# to pick a new time, he is allowed to keep on to the time that the professional set for him
		if @get('appointment_id')? and @get('appointment_from_professional') and @get('from')
			@trigger 'booked'
			return false

		@set('errors', 'You must select a time to proceed with your booking')
		return false

	getProviderId : -> @get('provider_id') || window.GLOBALS.PROVIDER_ID

	# make sure we return a proper JSON object
	toJSON : ->
		attributes = _.clone @attributes
		if attributes.scheduleCollection instanceof WizardBookIntroductory.Collections.Schedule
			attributes.scheduleCollection = @attributes.scheduleCollection.toJSON()
		attributes


module.exports = Model

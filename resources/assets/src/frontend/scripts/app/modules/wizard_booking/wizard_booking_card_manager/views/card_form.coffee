Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Moment = require 'moment'
scrollTo = require 'utils/scroll_to_element'
vexConfirm = require 'app/modules/wizard_booking/utils/confirm'
require 'payment'

class View extends Backbone.View

	tagName: 'form'
	className: 'wizard_booking--cards_manager--form'

	events:
		'click .wizard_booking--cards_manager--add_card': 'submitForm'
		'click .wizard_booking--cards_manager--delete': 'removeCard'
		'submit': 'submitForm'
		'click .wizard_booking--cards_manager--cancel': 'cancelForm'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base

		@collection = options.collection
		@baseView = options.baseView
		@model = options.model

		@isNew = if @model.get('id') then false else true

		@addStickit()
		@addValidation()

	addStickit: ->
		@bindings =
			'[name="card_holder"]': 'card_holder'
			'[name="card_number"]': 'card_number'
			'[name="card_exp"]':
				observe: ['exp_month', 'exp_year']
				onGet: (val) -> val[0] + ' / ' + val[1] if val[0] and val[1]
				onSet: (val) ->
					exp = val.split '/'
					exp[0] = if exp[0] then exp[0] * 1 else 0
					exp[1] = if exp[1] then exp[1] * 1 else 0
					exp
			'[name="cvv"]': 'cvv'
			'[data-cards-manager-card-logo]':
				classes:
					maestro:
						observe: 'card_type'
						onGet: (val) -> val is 'mastercard' || val is 'maestro' || val is 'master'
					visa:
						observe: 'card_type'
						onGet: (val) -> val is 'visa'
					amex:
						observe: 'card_type'
						onGet: (val) -> val is 'amex'
			'.wizard_booking--cards_manager--form_number--label':
				classes:
					'm-hide':
						observe: 'id'
						onGet: (val) -> if val then false else true

			'.wizard_booking--cards_manager--form_number--field':
				classes:
					'm-hide':
						observe: 'id'
						onGet: (val) -> if val then true else false

			'.wizard_booking--cards_manager--card_number':
				observe: 'card_number'
				updateMethod: 'html'
				onGet: (val) -> '<i>&bull;&bull;&bull;&bull;</i><i>&bull;&bull;&bull;&bull;</i><i>&bull;&bull;&bull;&bull;</i>' + val

			'[data-card-manager-footer]':
				classes:
					'm-hide':
						observe: 'id'
						onGet: (val) -> if val then false else true


	generateYears: ->
		years = []
		currentYear = +Moment().format('YYYY')
		lastYear = currentYear + 10
		for i in [currentYear..lastYear]
			years.push i.toString()
		years

	initCardCheck: ->
		@$el.$cardNumber.payment('formatCardNumber')
		@$el.$cardCvc.payment('formatCardCVC')
		@$el.$cardExp.payment('formatCardExpiry')

		@$el.$cardNumber.on 'keyup', (e) =>
			@model.set 'card_type', $.payment.cardType($(e.target).val())

		# don't allow numbers in card holders name
		@$el.$cardName.on 'keypress', (e) ->
			return false if e.charCode > 47 and e.charCode < 58

	submitForm: (e) ->
		e.preventDefault() if e?
		errors = @model.validate()

		if errors?
			@scrollToForm()
			return false

		Wizard.loading()
		$.when(
			@model.save()
		).then(
			(res) =>
				if @isNew
					@collection.push res.data
					@baseView.selectLast()

				@baseView.hideCardForm()

				Wizard.ready()
				if @isNew
					window.WizardCardsManager.trigger 'card:added' unless e?
				else
					# don't save cvv
					@model.set 'cvv', null
					window.WizardCardsManager.trigger 'card:updated'

		).fail(
			(request) =>
				Wizard.ready()
				if request.responseJSON?.errors
					_.each request.responseJSON.errors, (err) =>
						@showError err.messages[0] if err.messages?.length
				@scrollToForm()
		)

	scrollToForm: ->
		setTimeout(
			=> scrollTo @$el
			, 10
		)
		@

	cancelForm: ->
		@baseView.hideCardForm()
		@

	# base functions
	cacheDom: ->
		@$el.$cardLogo = @$el.find('[data-cards-manager-card-logo]')
		@$el.$cardNumber = @$el.find('[name="card_number"]')
		@$el.$cardCvc = @$el.find('[name="cvv"]')
		@$el.$cardName = @$el.find('[name="card_holder"]')
		@$el.$cardExp = @$el.find('[name="card_exp"]')

		@$el.$errorsContainer = @$el.find('[data-cards-manager-errors]')
		@

	render: ->
		@$el.html WizardCardsManager.Templates.CardForm()
		@cacheDom()
		@stickit()
		@initCardCheck()
		@

	removeCard: (e) ->
		e.preventDefault() if e?

		# we can't remove unsaved card
		return unless @model.get('id')

		callback = (value) =>
			return unless value
			Wizard.loading()
			$.when(
				@model.remove()
			).then(
				(res) =>
					@baseView.removeCard @model
					Wizard.ready()
					window.WizardCardsManager.trigger 'card:removed' unless e?
			).fail(
				=> Wizard.ready()
			)

		vexConfirm 'Are you sure you would like to delete this card?', callback, 'Yes', 'Cancel'

	showError: (error) ->
		@$el.$errorsContainer.html("<span>#{error}</span>").removeClass('m-hide')

	hideError: ->
		@$el.$errorsContainer.html('').addClass('m-hide')


module.exports = View

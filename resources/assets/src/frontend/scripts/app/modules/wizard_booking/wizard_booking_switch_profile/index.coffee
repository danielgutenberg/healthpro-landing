Handlebars = require 'handlebars'

window.WizardSwitchProfile = WizardSwitchProfile =
	Views:
		Base: require('./views/base')
	Models:
		Base: require('./models/base')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

class WizardSwitchProfile.App
	constructor: ->
		@model = new WizardSwitchProfile.Models.Base()
		@view = new WizardSwitchProfile.Views.Base
			model: @model

		return {
			view: @view
			model: @model
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()


module.exports =  WizardSwitchProfile

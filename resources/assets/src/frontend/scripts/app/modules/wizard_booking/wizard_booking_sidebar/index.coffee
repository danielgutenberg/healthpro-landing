Handlebars = require 'handlebars'

window.BookingInfo = BookingInfo =
	Views:
		Base: require('./views/base')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

class BookingInfo.App
	constructor: (options) ->
		@view = new BookingInfo.Views.Base
			bookingModel: options.bookingModel
			$container: options.$container

module.exports =  BookingInfo

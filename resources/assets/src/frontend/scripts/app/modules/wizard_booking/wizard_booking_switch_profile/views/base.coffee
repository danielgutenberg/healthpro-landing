Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	className: 'wizard_booking--step m-switch_profile'

	events:
		'click [data-select-user]': 'selectUser'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions(options)

		@addListeners()

	addListeners: ->
		@listenTo @model, 'data:ready', =>
			@render()
			@delegateEvents()

	render: ->
		@$el.html WizardSwitchProfile.Templates.Base @model.toJSON()
		@

	selectUser: (e) ->
		e.preventDefault()
		@model.set 'profile_id', $(e.currentTarget).data('id')
		@model.save()

module.exports = View

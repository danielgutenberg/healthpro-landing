Backbone = require 'backbone'
Handlebars = require 'handlebars'

window.WizardBookingPayWithPackage = WizardBookingPayWithPackage =
	Views:
		Base: require './views/base'
	Models:
		Base: require './models/base'
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

_.extend WizardBookingPayWithPackage, Backbone.Events

class WizardBookingPayWithPackage.App
	constructor: ->
		@model = new WizardBookingPayWithPackage.Models.Base()

		@view = new WizardBookingPayWithPackage.Views.Base
			model: @model

		return {
			model: @model
			view: @view

			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = WizardBookingPayWithPackage

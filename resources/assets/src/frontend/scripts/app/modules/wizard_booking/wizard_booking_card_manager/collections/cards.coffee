Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: ->
		@getData()

	getData: ->
		@trigger 'data:loading'
		Wizard.loading()
		$.ajax
			url: getApiRoute('ajax-credit-cards')
			method: 'get'
			success: (res) =>
				_.each res.data, (card) => @add @format(card)
				@trigger 'data:ready'
				Wizard.ready()
			error: => Wizard.ready()

	format: (card) ->
		{
			id          : card.id
			card_holder : card.card_holder
			card_number : card.card_number
			card_type   : card.card_type.toLowerCase()
			exp_month   : if card.exp_month.length is 1 then "0#{card.exp_month}" else card.exp_month
			exp_year    : card.exp_year
			account_id  : parseInt card.account_id, 10
		}

module.exports = Collection

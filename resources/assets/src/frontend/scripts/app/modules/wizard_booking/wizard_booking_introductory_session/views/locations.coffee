Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

LocationFormatter = require 'utils/formatters/location'
LocationIcon = require 'app/modules/wizard_booking/utils/location_icon'

require 'backbone.stickit'

class View extends Backbone.View

	canChangeLocation: false

	events:
		'click .wizard_booking--location_select--list--item': 'filterLocationEvent'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@scheduleCollection = @baseModel.get('scheduleCollection')

		@bind()
		@render()

	bind: ->
		@listenTo @, 'rendered', =>
			@renderLocations()
			@stickit @baseModel

		@listenTo @baseModel, 'change:current_service_id', => @render()
		@listenTo @scheduleCollection, 'data:loading', => @canChangeLocation = false
		@listenTo @scheduleCollection, 'data:ready', =>
			@canChangeLocation = true
			@maybeSelectLocation()

		@bindings =
			'[data-widget-timezone]':
				observe: 'location_id'
				onGet: (val) ->
					if val
						location = @baseModel.getCurrentLocation()
						location.timezone

	render: ->
		@$el.html WizardBookIntroductory.Templates.Locations()
		@trigger 'rendered'
		@

	renderLocations: ->
		@$el.$list = @$el.find('.wizard_booking--location_select--list')
		_.each @baseModel.get('locations'), (location) =>
			return unless location?
			@$el.$list.append WizardBookIntroductory.Templates.Location
				location_id      : location.id
				location_address : if location.location_type is 'virtual' then false else LocationFormatter.getLocationAddressLabel(location, true)
				location_name    : LocationFormatter.getLocationName(location)
				location_icon    : LocationIcon(location, 'small')
				location_type    : location.location_type
			return

		@$el.$locations = @$el.$list.find('[data-location-id]')
		@

	clearLocationFilter: ($location) ->
		if $location
			$location.siblings().removeClass 'm-current'
		else
			@$el.$locations.removeClass 'm-current'
		@

	filterLocationEvent: (ev) ->
		@filterLocation $(ev.currentTarget)
		@

	filterLocation: ($location) ->
		return unless @canChangeLocation

		locationId = +$location.data('location-id')
		isCurrentLocation = locationId is @baseModel.get 'location_id'

		@clearLocationFilter($location)
		$location.addClass 'm-current' if locationId

		unless isCurrentLocation
			@baseModel.set 'location_id', locationId

		@baseView.scheduleView?.filterLocations locationId

		@

	selectLocation: (locationId = null) ->
		if locationId is null
			locationId = @baseModel.get('location_id')

		if locationId
			$targetLocation =  @$el.$locations.filter("[data-location-id='#{locationId}']")
			@filterLocation($targetLocation) if $targetLocation.length
		else
			@$el.$locations.first().trigger 'click'
		@

	maybeSelectLocation: ->
		currentLocationId = @baseModel.get('location_id')

		# if no location selected - select the first available location
		unless currentLocationId
			@selectLocation @baseView.scheduleView?.getFirstTimeLocation()
			return

		# check if current service has selected location
		serviceLocationIds = _.pluck(@baseModel.get('locations'), 'id')
		if serviceLocationIds.indexOf(currentLocationId) > -1
			@selectLocation currentLocationId
			return

		# check if selected location is in the availabilities locations list
		currentLocationHasAvailabilities = @scheduleCollection.where(location_id: currentLocationId).length > 0
		if currentLocationHasAvailabilities
			@selectLocation currentLocationId
			return

		# select first available location
		if @scheduleCollection.length
			currentLocationId = @scheduleCollection.first().location_id
		else
			currentLocationId = @baseModel.get('locations')[0].id

		@selectLocation currentLocationId

		@

module.exports = View

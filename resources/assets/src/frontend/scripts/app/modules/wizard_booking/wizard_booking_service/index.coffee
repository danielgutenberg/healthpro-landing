Handlebars = require 'handlebars'

window.WizardBookService = WizardBookService =
	Views       :
		Base       : require('./views/base')
		Schedule   : require('./views/schedule')
		Locations  : require('./views/locations')
		Service    : require('./views/service')
		Datepicker : require('./views/datepicker')
		Time       : require('./views/time')
		Packages   : require('./views/packages')
	Models      :
		Base     : require('./models/base')
		Time     : require('./models/time')
		Location : require('./models/location')
	Collections :
		Schedule : require('./collections/schedule')
	Templates   :
		Schedule   : Handlebars.compile require('text!./templates/schedule.html')
		Locations  : Handlebars.compile require('text!./templates/locations.html')
		Location   : Handlebars.compile require('text!./templates/location.html')
		Base       : Handlebars.compile require('text!./templates/base.html')
		Time       : Handlebars.compile require('text!./templates/time.html')
		Service    : Handlebars.compile require('text!./templates/service.html')
		Datepicker : Handlebars.compile require('text!./templates/datepicker.html')
		Packages   : Handlebars.compile require('text!./templates/packages.html')

_.extend(WizardBookService, Backbone.Events)

class WizardBookService.App
	constructor : (options) ->

		# preset data set when booking wizard is opened from search results
		if options.presetData
			presetData = options.presetData
		else if Wizard? and Wizard.inited
			presetData = Wizard.model.getPresetData()
		else
			presetData = null

		@model = new WizardBookService.Models.Base {},
			type       : options.type
			presetData : presetData

		@view = new WizardBookService.Views.Base
			model : @model

		return {
			view  : @view
			model : @model
			close : @close
		}

	close : ->
		@view?.close?()
		@view?.remove?()

module.exports = WizardBookService

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	attributes:
		class: 'wizard_booking--client_location--list'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@bind()
		@cacheDom()

	bind: ->
		@listenTo @collections.locations, 'add', @renderLocation
		@listenTo @collections.locations, 'update', @toggleEmptyMessage
		@listenToOnce @collections.locations, 'data:ready', =>
			@toggleEmptyMessage()
			@selectLocation()

	cacheDom: ->
		@$el.$empty = $('<p class="wizard_booking--client_location--empty">Please enter an address where the professional would come see you</p>')

	renderLocation: (model) ->
		@subViews.push new WizardBookingLocationSelect.Views.Location
			parentView: @
			baseView: @baseView
			baseModel: @baseModel
			model: model
			collections: @collections

	toggleEmptyMessage: ->
		if @collections.locations.length
			@$el.$empty.remove()
		else
			@$el.html @$el.$empty

	selectLocation: ->
		if @collections.locations.length

			if @baseModel.get('location_id')
				model = @collections.locations.get @baseModel.get('location_id')
			else
				model = @collections.locations.first()

			model.trigger 'mark_selected'

module.exports = View

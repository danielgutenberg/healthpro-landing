Handlebars = require 'handlebars'

window.WizardBookIntroductory =
	Collections :
		Schedule : require('./collections/schedule')
	Views       :
		Base       : require('./views/base')
		Service    : require('./views/service')
		Schedule   : require('./views/schedule')
		Locations  : require('./views/locations')
		Datepicker : require('./views/datepicker')
		Time       : require('./views/time')
	Models      :
		Base : require('./models/base')
		Time : require('../wizard_booking_service/models/time')
	Templates   :
		Base       : Handlebars.compile require('text!./templates/base.html')
		Service    : Handlebars.compile require('text!./templates/service.html')
		Schedule   : Handlebars.compile require('text!../wizard_booking_service/templates/schedule.html')
		Time       : Handlebars.compile require('text!../wizard_booking_service/templates/time.html')
		Datepicker : Handlebars.compile require('text!../wizard_booking_service/templates/datepicker.html')
		Locations  : Handlebars.compile require('text!../wizard_booking_service/templates/locations.html')
		Location   : Handlebars.compile require('text!../wizard_booking_service/templates/location.html')

class WizardBookIntroductory.App
	constructor : (options = {}) ->
		if options.presetData
			presetData = options.presetData
		else if Wizard? and Wizard.inited
			presetData = Wizard.model.getPresetData()
		else
			presetData = null

		_.extend options,
			model : new WizardBookIntroductory.Models.Base {},
				presetData : presetData

		@view = new WizardBookIntroductory.Views.Base options

	close : ->
		@view.close?()
		@view.remove?()

module.exports = WizardBookIntroductory

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
WizardCardManager = require 'app/modules/wizard_booking/wizard_booking_card_manager'
Moment = require 'moment'
Config = require 'app/modules/wizard_booking/config'
ToggleEl = require 'utils/toggle_el'
getApiRoute = require 'hp.api'

class View extends Backbone.View

	className: 'wizard_booking--payment_details'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@render()

	addListeners: ->
		# re-render after changing payment method
		@listenTo @baseModel, 'change:payment_method', -> @render()

	render: ->
		@removeInstances()
		@instances = []

		@$el.html WizardBookingPayment.Templates.PaymentDetails @getTemplateData()
		@stickit @baseModel

		@cacheDom()
		@initCardManager()
		@initPaypal()
		@toggleEditCard false
		@

	cacheDom: ->
		@$el.$cardManager = @$('[data-payment-card-manager]')
		@

	getTemplateData: ->
		method = @baseModel.get('payment_method')
		templateData =
			payment_method_card   : method is 'card'
			payment_method_paypal : method is 'paypal'
			payment_method_in_person   : method is 'in_person'
		templateData

	initCardManager: ->
		return unless @baseModel.get('payment_method') is 'card'
		@instances.push @cardsManagerView = new WizardCardManager.App()
		@$el.$cardManager.append @cardsManagerView.view.render().el
		@addCardsBindings()
		@

	initPaypal: ->
		unless @baseModel.get('payment_method') is 'paypal'
			@baseView.removePaypalButtonFromFooter()
			return

		@baseView.displayPaypalButtonInFooter()
		Wizard.loading()
		$s ['https://www.paypalobjects.com/api/checkout.js'], 'paypal.btn', =>
			Wizard.ready()
			paypal.Button.render {
				env         : window.GLOBALS.PAYPAL_MODE
				client      :
					sandbox: 'AffY_sjx-saleAf88yyVxNyCqkuJZHhG2b-7RM8xcN6knxbW6mxhqGVfvxehKmQftjMlHcaPxHk-NVK5'
					production: 'AZBZg0zA0zNGFqjk00tUiX_Y2ex76UisHG_bjqhdV_RflGKNdTDDX8cpJBScD-r3-joQvpkCBSHg12x3'
				commit      : true
				payment     : @processPaypalPayment
				onAuthorize : (data, actions) => @processPaypalPaymentAuthorization(data, actions)
			}, "##{Config.Payment.PaymentMethods.ButtonId}"

	addCardsBindings: ->
		@listenTo @cardsManagerView.model, 'change:card_id', (model, val) =>
			@baseModel.set('credit_card_id', val)
			Wizard.model.set('credit_card_id', val)
			@baseView.raiseGenericError null

		@listenTo WizardCardManager, 'card:edit', (model) =>
			@baseView.raiseGenericError null
			@toggleEditCard !!model.get('id')

		@listenTo WizardCardManager, 'card:close', =>
			@toggleEditCard false
		@

	toggleEditCard: (isEdit = false) ->
		Wizard.view.toggleFooter !isEdit
		ToggleEl @baseView.$el.$totals, !isEdit
		ToggleEl @baseView.$el.$coupons, !isEdit
		@

	processPaymentMethod: ->
		if @baseModel.get('payment_method') is 'card'
			if @cardsManagerView.view.isFormOpened
				return @cardsManagerView.view.cardForm.submitForm()
			else
				return @validateExistingCard()
		else
			return true

	validateExistingCard: ->
		card = @cardsManagerView.view.collection.get @baseModel.get('credit_card_id')
		return false unless card

		year = +card.get('exp_year')
		month = +card.get('exp_month') - 1
		date = Moment.utc([year, month, 1, 0, 0, 0])
		now = Moment.utc()

		return true if now.isBefore(date)

		@baseView.raiseGenericError 'Selected card has expired'
		false



	processPaypalPaymentAuthorization: (data, actions) ->
		Wizard.loading()
		console.log 'processPaypalPaymentAuthorization', data, actions
		$.ajax
			url     : getApiRoute('api-order-book', {orderId: Wizard.model.get('data.order.id')})
			method  : 'post'
			type    : 'json'
			data    :
				_token       : window.GLOBALS._TOKEN
				payer_id      : data.payerID
				payment_id    : data.paymentID
				payment_method : 'paypal'
			success : (response) ->
				console.log 'processPaypalPaymentAuthorization response', response
				Wizard.ready()
				Wizard.model.set 'next_step', 'success'
				Wizard.trigger 'next_step'
				return



	processPaypalPayment: =>
		$.when(
			@baseModel.createOrder()
		).then(
			=>
				$.ajax
					url     : getApiRoute('ajax-paypal-process')
					method  : 'post'
					type    : 'json'
					data    :
						_token  : window.GLOBALS._TOKEN
						orderId : Wizard.model.get('data.order.id')
					success : (response) ->
						console.log response
						# window.location = response
						return
		).fail(
			=>
				console.log('paypal fail', arguments)
		)


module.exports = View

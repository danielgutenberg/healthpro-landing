Backbone = require 'backbone'
Moment = require 'moment'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Device = require 'utils/device'
Config = require 'app/modules/wizard_booking/config'

require 'jquery-ui/datepicker'

class View extends Backbone.View

	events:
		'click .booking_time--datepicker--prev': 'setPrevDate'
		'click .booking_time--datepicker--next': 'setNextDate'
		'change .booking_time--datepicker--input': 'loadSchedule'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@scheduleCollection = @baseModel.get('scheduleCollection')

		@inline = Device.isDesktop()

		@addListeners()
		@render()
		@cacheDom()
		@initDatepicker()

	addListeners: ->
		@listenTo @scheduleCollection, 'show_next_availabilities', @setNextAvailableDate

	render: ->
		@$el.html WizardBookService.Templates.Datepicker
			inline: @inline
		@

	initDatepicker: ->
		if @inline
			@initInlineDatepicker()
		else
			@initDropdownDatepicker()
		@

	cacheDom: ->
		@$el.$datepicker = $('[data-booking-time-datepicker]', @$el)
		@$el.$datepicker.$inline = @$('[data-booking-time-datepicker-inline]')
		@$el.$datepicker.$opener = $('[data-booking-time-datepicker-opener]', @$el.$datepicker)
		@$el.$datepicker.$opener.$text = $('[data-booking-time-datepicker-opener-text]', @$el.$datepicker)
		@$el.$datepicker.$input = $('[data-booking-time-datepicker-input]', @$el.$datepicker)
		@$el.$datepicker.$prevDay = $('[data-booking-time-datepicker-prev]', @$el.$datepicker)
		@$el.$datepicker.$nextDay = $('[data-booking-time-datepicker-next]', @$el.$datepicker)

	initDropdownDatepicker:  ->
		initDate = @baseModel.returnFromDate()

		_init = =>
			@datepicker = @$el.$datepicker.$input.datepicker
				minDate    : 0
				dateFormat : "yy-mm-dd"
				onSelect   : (dateText, b) =>
					@setDateAltField(dateText)
					@$el.$datepicker.$input.trigger('change')

			@datepicker.datepicker('setDate', initDate)

		@$el.$datepicker.$opener.$text.on 'click', (e) =>
			unless @$el.$datepicker.$input.hasClass('hasDatepicker')
				_init()
			@$el.$datepicker.$input.datepicker('show')

		@baseModel.set 'date', initDate
		@setDateAltField initDate

	initInlineDatepicker: ->
		initDate = @baseModel.returnFromDate()

		@datepicker = @$el.$datepicker.$inline.datepicker
			minDate    : 0
			dateFormat : "yy-mm-dd"
			onSelect   : (dateText, b) =>
				@$el.$datepicker.$input.val(dateText).trigger('change')

		@datepicker.datepicker 'setDate', initDate
		@baseModel.set 'date', initDate
		@

	setDateAltField: (dateText) ->
		@$el.$datepicker.$opener.$text.html @getAltDate(dateText)

	getAltDate: (dateText) ->
		altDate = Moment(dateText).format Config.Datepicker.DateFormat
		return '<b>' + @getAltDateDay(dateText) + '</b>, ' + altDate

	getAltDateDay: (date) ->
		selected = Moment(date).format('YYYY-MM-DD')
		today = Moment().format('YYYY-MM-DD')

		switch selected
			when today then 'Today'
			else Moment(date).format('ddd')

	setPrevDate: ->
		if @datepicker?
			date = @datepicker.datepicker 'getDate'
			prevDateMoment = Moment(date).subtract(1, 'day')
			@datepicker.datepicker 'setDate', prevDateMoment.toDate()
			@setDateAltField( prevDateMoment.toDate() )
			@$el.$datepicker.$input.trigger('change')
		else
			@initDatepicker(true)
			@setPrevDate()

		return false

	setNextDate: ->
		if @datepicker?
			date = @datepicker.datepicker 'getDate'
			nextDateMoment = Moment(date).add(1, 'day')
			@datepicker.datepicker 'setDate', nextDateMoment.toDate()
			@setDateAltField(nextDateMoment.toDate())
			@$el.$datepicker.$input.trigger('change')
		else
			@initDatepicker(true)
			@setNextDate()

		return false

	setNextAvailableDate: (date) ->
		if date?
			if @datepicker?
				@datepicker.datepicker 'setDate', date
				@setDateAltField(date)
				@$el.$datepicker.$input.val(date).trigger('change')
			else
				@initDatepicker(true)
				@setNextAvailableDate(date)

		return false

	loadSchedule: (e) ->
		date = $(e.target).val()
		@scheduleCollection.trigger 'data:reset'

		@baseModel.set('date', Moment(date).format('YYYY-MM-DD'))
		@baseModel.set('from', '')

		@scheduleCollection.getData(date)

module.exports = View

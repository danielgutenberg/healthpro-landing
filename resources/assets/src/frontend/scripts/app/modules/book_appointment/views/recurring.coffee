Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
LocationFormatter = require 'utils/formatters/location'

class View extends Backbone.View

	className : 'book_recurring'

	initialize : (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@render()

	render : ->
		@$el.html BookAppointment.Templates.Recurring @getTemplateData()
		@$container.append @$el if @$container
		@

	getTemplateData : ->
		services = []
		locations = []

		# get services with inactive sessions
		_.each @baseModel.get('services'), (s) =>
			services.push
				service_id   : s.service_id
				service_name : s.name
				packages     : @baseModel.getServiceMonthlyPackages s, true

			_.each s.locations, (location) ->
				return if _.where(locations, {location_id : location.id}).length > 0

				locations.push
					location_id      : location.id
					location_type    : location.location_type
					location_name    : LocationFormatter.getLocationName location
					location_address : do ->
						return null if location.location_type is 'virtual'
						LocationFormatter.getLocationAddressLabel location

				return
		{
			services  : services
			locations : locations
		}


module.exports = View

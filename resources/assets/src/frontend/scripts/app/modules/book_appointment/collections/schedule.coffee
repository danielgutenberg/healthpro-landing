Backbone = require 'backbone'
Moment = require 'moment'

datetimeParsers = require 'utils/datetime_parsers'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		@baseModel = options.baseModel
		@nextAvailableDays = null
		@addListeners()

	addListeners: ->
		if @baseModel.type is 'widget' or @baseModel.type is 'wizard'
			@listenTo @baseModel, 'change:current_session_id', =>
				currentSession = @baseModel.getCurrentSession()
				if currentSession.active
					@getData()
				else
					@reset()
		@

	getData: (date) ->
		@reset()
		@fetchByAvailabilities @baseModel.returnFromDate(date)
		@

	fetchByAvailabilities: (date) ->
		@xhr.abort() if @xhr # abort previous call
		@trigger 'data:loading'

		@xhr = $.ajax
			url: getApiRoute 'ajax-provider-availabilities',
				providerId: @baseModel.get('provider_id') || window.GLOBALS.PROVIDER_ID
			method: 'get'
			data:
				from: @baseModel.returnFromDate(date) + 'T' + @baseModel.returnFromTime()
				location_id: @returnLocationsIds()
				length: @returnLength()
			success: (res) =>
				@setData res, date
				@trigger 'data:ready'
			error: =>
				@trigger 'data:ready'

	returnLength: ->
		session = @baseModel.getCurrentSession()
		if session then session.duration else null

	returnLocationsIds: ->
		ids = []
		@baseModel.getCurrentService().locations.forEach (location) => ids.push location.id
		ids.join()

	# set data from collection's ajax call
	setData: (response, date) ->
		locations = response.data?.results[0].locations ? null
		availabilities = response.data?.results[0].availabilities ? null

		@nextAvailableDays = response.data?.results[0].nextAvailableDays

		if availabilities?[date]?
			@pushAvailabilities availabilities[date], date, locations
		@trigger 'availabilities:ready'

	# set preset data from search or set data from base model
	setAvailabilities: (availabilities) ->
		date = @baseModel.get('date')
		if availabilities[date]?
			@pushAvailabilities availabilities[date], date
		else
			# set next available days for all available locations without availabilities
			@nextAvailableDays = do =>
				days = {}
				_.each availabilities, (times, day) ->
					locationIds = _.uniq _.pluck(times, 'location_id')
					_.each locationIds, (locationId) ->
						return if days[locationId]
						days[locationId] = day
				days
			nextAvailablityDate = @nextAvailableDays[@baseModel.get('location_id')]

			# Setting availabilities from next available date
			if availabilities[nextAvailablityDate]?
				@pushAvailabilities availabilities[nextAvailablityDate], nextAvailablityDate
				@currentAvailabilitiesDate = nextAvailablityDate

		@trigger 'availabilities:ready'
		@trigger 'data:ready'

	pushAvailabilities: (availabilities, date, locations) ->
		preAvailabilities = []

		selectedFrom = datetimeParsers.parseToMoment @baseModel.get('from')
		selectedLocationId = @baseModel.get('location_id')

		availabilities.forEach (availability) =>
			preAvailabilities.push
				availability_id: availability.id ? availability.availability_id
				from: Moment.parseZone( availability.from ).format()
				until: Moment.parseZone( availability.until ).format()
				location_id: availability.location_id
				location_name: do =>
					if availability.location_name?
						return availability.location_name
					else
						if locations? and @baseModel.get('current_service_id')
							_.where( locations, {id: availability.location_id})[0]?.name
						else
							location_name = ''
							_.each @baseModel.get('services'), (service) ->
								_.each service.locations, (location) ->
									location_name = location.name if location.id is availability.location_id
									return
								return

							location_name

				services: availability.services ? null
				selected: do =>
					return unless availability.location_id is selectedLocationId
					return unless date is selectedFrom.format('YYYY-MM-DD')

					datetimeParsers.parseToFormat(availability.from, 'HH:mm') is selectedFrom.format('HH:mm')

		@sortTime(preAvailabilities).forEach (availability) => @add availability

	filterAvailabilitiesByService: (availabilities, serviceId) ->
		results = []
		_.forEach availabilities, (availability) ->
			_.forEach availability.services, (value, key) ->
				if +key is +serviceId
					results.push availability

		return results

	sortTime: (availabilities) ->
		result = _.sortBy availabilities, (availability) ->
			return +Moment.parseZone( availability.from ).format('HH:mm').replace(':', '')
		unique = _.uniq result, (availability) ->
			return availability.from + availability.location_id
		return unique

module.exports = Collection

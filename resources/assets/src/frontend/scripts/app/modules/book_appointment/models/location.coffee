Backbone = require 'backbone'

class Model extends Backbone.Model

	defaults :
		name    : ''
		address : ''

module.exports = Model

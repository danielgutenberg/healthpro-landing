Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

HumanTime = require 'utils/human_time'
Formats = require 'config/formats'
Numeral = require 'numeral'

require 'backbone.stickit'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@bind()
		@initPackages()

	addListeners: ->
		@listenTo @baseModel, 'change:current_session_id', =>
			@baseModel.set 'current_package_id', null
			@initPackages()
		@

	bind: ->
		@bindings =
			'[name="package_id"]':
				observe: 'current_package_id'
				update: ($el, val) ->
					$el.filter("[value='#{val}']").prop('checked', true)
					return
				getVal: ($el, ev, options) ->
					$checkbox = $(ev.currentTarget)
					$el.not($checkbox).prop 'checked', false
					return +$checkbox.val() if $checkbox.is(':checked')
					return null

	initPackages: ->
		@$el.html ''
		return unless @baseModel.sessionHasSimplePackages()
		@render()
		@stickit @baseModel

	render: ->
		@$el.html WizardBookService.Templates.Packages(@templateData())
		@

	templateData: ->
		session = @baseModel.getCurrentSession()

		session_price = Numeral(session.price)
		session.formatted_price = session_price.format(Formats.Price.Default)
		session.formatted_duration = HumanTime.minutes session.duration

		packages = @baseModel.getSessionSimplePackages().map (pkg) ->
			price = Numeral(pkg.price)
			pkg.formatted_price = price.format(Formats.Price.Default)
			pkg.formatted_duration = session.formatted_duration
			pkg.discount = Numeral((100 - (price.value() / pkg.number_of_visits) / session_price.value() * 100) / 100).format('0%')
			pkg

		{
			session: session
			packages : packages
		}


module.exports = View

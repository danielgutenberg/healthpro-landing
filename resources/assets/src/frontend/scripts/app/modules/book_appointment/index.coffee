Handlebars = require 'handlebars'

window.BookAppointment = BookAppointment =
	Views       :
		Base       : require('./views/base')
		Schedule   : require('./views/schedule')
		Locations  : require('./views/locations')
		Recurring  : require('./views/recurring')
		Service    : require('./views/service')
		Datepicker : require('./views/datepicker')
		Time       : require('./views/time')
		Packages   : require('./views/packages')
	Models      :
		Base     : require('./models/base')
		Time     : require('./models/time')
		Location : require('./models/location')
	Collections :
		Schedule : require('./collections/schedule')
	Templates   :
		ScheduleWidget : Handlebars.compile require('text!./templates/schedule.html')
		Recurring      : Handlebars.compile require('text!./templates/recurring.html')
		Base           : Handlebars.compile require('text!./templates/base.html')
		Time           : Handlebars.compile require('text!./templates/time.html')
		Service        : Handlebars.compile require('text!./templates/service.html')
		Datepicker     : Handlebars.compile require('text!./templates/datepicker.html')
		Packages       : Handlebars.compile require('text!./templates/packages.html')


_.extend BookAppointment, Backbone.Events

class BookAppointment.App
	constructor : (options) ->
		presetData = options.presetData

		@model = new BookAppointment.Models.Base {},
			type       : options.type
			presetData : presetData

		# type option:
		# widget - sidebar design
		# wizard - step design
		@view = new BookAppointment.Views.Base
			model : @model

		return {
			view  : @view
			model : @model
			close : @close
		}

	close : ->
		@view?.close?()
		@view?.remove?()

# init on the profile page
if $('.book_appointment').length and !window.bookAppointment
	options = $('.book_appointment').data('options')
	window.bookAppointment = new BookAppointment.App options

module.exports = BookAppointment

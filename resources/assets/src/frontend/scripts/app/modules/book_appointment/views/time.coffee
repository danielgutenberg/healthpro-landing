Backbone = require 'backbone'
Moment = require 'moment'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	className: 'booking_time--list--item'
	tagName: 'li'

	events:
		'click': 'setAvailability'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addStickit()

	addStickit: ->
		@bindings =
			':el':
				classes:
					'm-active':
						observe: 'selected'
					'm-hide':
						observe: 'hide'

	render: ->
		@$el.html BookAppointment.Templates.Time
			from: Moment.parseZone( @model.get('from') ).format('h:mm a')
			location_id: @model.get('location_id')
			location_name: _.trunc @model.get('location_name')
		@stickit()
		@

	setAvailability: ->
		@baseModel.set
			date: Moment.parseZone(@model.get('from')).format('YYYY-MM-DD')
			from: Moment.parseZone( @model.get('from') ).format()
			until: Moment.parseZone( @model.get('until') ).format()
			availability_id: @model.get('availability_id')
			resetToFirstStep: 'recalculate'

		@model.collection.forEach (model) -> model.set('selected', false)
		@model.set 'selected', true

		@baseModel.save()

	filterTime: (display) ->
		if display
			@$el.removeClass 'm-hide'
		else
			@$el.addClass 'm-hide'


module.exports = View

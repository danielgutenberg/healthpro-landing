Backbone = require 'backbone'
Moment = require 'moment'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

datetimeParsers = require 'utils/datetime_parsers'

class View extends Backbone.View

	events:
		'click .booking_time--list_empty--show_next': 'showNextAvailabilities'
		'click .booking_time--list_empty--message': 'goToMessages'
		'click .booking_time--list_more--btn': 'showMore'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@times = []
		@timeModels = []
		@scheduleCollection = @baseModel.get('scheduleCollection')

		@render()
		@addListeners()
		@cacheDom()

		@scheduleCollection?.getData()

	addListeners: ->
		# listenTo collection, didn't work in search, but did on profile page
		# i decided to use custom event for this
		# in collection i listen to add event and trigger a new event to eventBus with data
		# so dirty.
		@listenTo @scheduleCollection, 'add', (model) => @addTime(model)
		@listenTo @scheduleCollection, 'data:loading', =>
			@showLoading()
			@hideEmpty()
		@listenTo @scheduleCollection, 'data:ready', => @hideLoading()
		@listenTo @scheduleCollection, 'reset', => @reset()
		@listenTo @scheduleCollection, 'availabilities:ready', =>
			@filterLocations()
			@toggleScheduleMessage()
		@listenTo @baseModel, 'change:location_id', @toggleScheduleMessage

	render: ->
		@$el.html BookAppointment.Templates.ScheduleWidget
			conversations_link: window.location.origin + '/dashboard/conversations#to:' + window.GLOBALS.PROVIDER_ID
			timezone: @baseModel.get('timezone')
			# availabilities_from_date: @scheduleCollection.currentAvailabilitiesDate
		@

	cacheDom: ->
		@$el.$list = $('.booking_time--list', @$el)
		@$el.$listWrap = $('.booking_time--list_wrap', @$el)
		@$el.$listContainer = $('.booking_time--list_container', @$el)
		@$el.$listTimezone = $('.booking_time--list_timezones', @$el)
		@$el.$list.$loading = $('.loading_overlay', @$el.$listWrap)
		@$el.$listEmpty = $('.booking_time--list_empty', @$el)
		@$el.$listNone = $('.booking_time--list_none', @$el)
		@$el.$listMore = $('.booking_time--list_more', @$el)
		@$el.$listMore.$btn = $('.booking_time--list_more--btn', @$el.$listMore)
		@$el.$listNoNextAvailable = $('.booking_time--list_no_availability', @$el)
		@$el.$nextAvailablitiesDate = $('.booking_time--list_date', @$el)

	addTime: (model) ->
		@timeModels.push model
		@times.push time = new BookAppointment.Views.Time
			model: model
			baseModel: @baseModel
			baseView: @baseView

		@$el.$list.append time.render().el
		@hideMore()

	reset: ->
		@removeTimes()
		@scheduleCollection.reset() if @scheduleCollection.length
		if @$el.$list? and @$el.$list.length
			@$el.$list.html('')

	removeTimes: ->
		if @times.length
			_.each @times, (time) => time.remove()
		@times = []

	showLoading: ->
		@$el.$listWrap.addClass('m-loading')
		@$el.$list.$loading.removeClass('m-hide')
		$(document).trigger 'bookAppointment:schedule:loading' # dirty trigger for outside (profile/mobile_view.coffee)

	hideLoading: ->
		@$el.$listWrap.removeClass('m-loading')
		@$el.$list.$loading.addClass('m-hide')
		$(document).trigger 'bookAppointment:schedule:ready' # dirty trigger for outside (profile/mobile_view.coffee)

	toggleScheduleMessage: ->

		isEmpty = do =>
			locationId = @baseModel.get('location_id')
			if locationId
				@scheduleCollection.where(location_id: locationId).length is 0
			else
				@scheduleCollection.length is 0

		if isEmpty
			@showEmpty()
		else
			@hideEmpty()

	showEmpty: ->
		@hideEmpty()

		locationId = @baseModel.get('location_id')

		if @scheduleCollection.nextAvailableDays?[locationId]
			if nextAvailableDate = datetimeParsers.parseToFormat @scheduleCollection.nextAvailableDays[locationId], 'MMMM Do'
				@$el.$listEmpty.find('span').text nextAvailableDate
				@$el.$listEmpty.addClass('m-show')
			else
				@$el.$listNoNextAvailable.addClass('m-show')
		else
			@$el.$listNoNextAvailable.addClass('m-show')

		@$el.$listTimezone.addClass('m-hide')
		@$el.$listContainer.addClass('m-hide')
		@

	hideEmpty: ->
		@$el.$listTimezone.removeClass('m-hide')
		@$el.$listContainer.removeClass('m-hide')
		@$el.$listNoNextAvailable.removeClass 'm-show'
		@$el.$listEmpty.removeClass 'm-show'
		@

	showNextAvailabilities: ->
		locationId = @baseModel.get('location_id')
		@scheduleCollection.trigger 'show_next_availabilities', @scheduleCollection.nextAvailableDays[locationId]
		@

	filterLocations: (locationId) ->
		# don't show a message when we don't have times
		return unless @times.length

		locationId = @baseModel.get('location_id') unless locationId

		_.each @times, (time) -> time.filterTime time.model.get('location_id') is locationId or locationId is null

		shownTimes = @times.filter (time) -> !time.$el.hasClass 'm-hide'
		if shownTimes.length
			@hideEmpty()
		else
			@showEmpty()

	getFirstTimeLocation: ->
		if @times.length
			@times[0].model.get 'location_id'

	hideMore: ->
		if @times.length > 3
			@$el.$list.addClass 'm-hide_extra'
			@$el.$listMore.removeClass 'm-hide'
		else
			@$el.$list.removeClass 'm-hide_extra'
			@$el.$listMore.addClass 'm-hide'

	showMore: ->
		@$el.$list.removeClass 'm-hide_extra'
		@$el.$listMore.addClass 'm-hide'

module.exports = View

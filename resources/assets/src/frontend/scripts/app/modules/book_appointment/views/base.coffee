Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
WizardDataFormatter = require 'utils/wizard_data_formatter'
WizardAuth = require 'helpers/wizard_auth'
Device = require 'utils/device'
ToggleEl = require 'utils/toggle_el'

require 'backbone.validation'
require 'backbone.stickit'
require 'app/modules/wizard_booking/wizard_booking'

class AbstractView extends Backbone.View

	className: 'book_appointment'

	events:
		'click [data-book-appointment]': 'openBookingWizardWithButton'
		'click [data-book-reccuring]': 'openWizardRecurringWithButton'
		'click .wizard--errors--close': 'hideError'
		'click .book_appointment--hide_services': 'hideServices'
		'click .booking_widget--header--close': 'triggerClose'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@instances = []

		@$el = $('.book_appointment')

		@render()


	bind: ->
		@listenTo @model, 'loading', @showLoading
		@listenTo @model, 'ready', @hideLoading

		@listenTo @model, 'booked', => @openWizard()
		@listenTo @model, 'change:current_session_id', @toggleSchedule

		@listenTo @, 'rendered', =>
			if @model.shouldFetchServices()
				@listenTo @model, 'services:ready', => @doWhenServicesReady()
			else
				@doWhenServicesReady()

	doWhenServicesReady: ->
		@hideLoading()
		@appendParts()
		@

	render: ->
		@$el.html BookAppointment.Templates.Base()

		@delegateEvents()
		@cacheDom()
		@bind()
		@stickit()

		@trigger 'rendered'

		return @

	cacheDom: ->
		@$el.$service = @$('[data-booking-service]')
		@$el.$locations = @$('[data-booking-location]')
		@$el.$loading = @$('[data-booking-loading]')
		@$el.$datepicker = @$('[data-booking-datepicker]')
		@$el.$schedule = @$('[data-booking-schedule-times]')
		@$el.$packages = @$('[data-booking-package]')
		@$el.$content = @$el.find('[data-booking-content]')
		@$el.$footer = @$el.find('[data-booking-footer]')
		@$el.$bookBtn = @$el.find('[data-book-appointment]')
		@$el.$recurringBtn = @$el.find('[data-book-reccuring]')
		@$el.$bookingSchedule = @$el.find('[data-booking-schedule]')
		@

	appendParts: ->
		if @model.hasOnlyMonthlyPackages()
			@appendRecurring()
		else
			@appendService()
			@appendPackages()
			@appendDatepicker()
			@appendSchedule()
			@appendLocations()
			@toggleSchedule()

		ToggleEl @$el.$content, true
		ToggleEl @$el.$footer, true

	toggleSchedule: ->
		ToggleEl @$el.$recurringBtn, @model.serviceHasMonthlyPackages()

		currentSessionAction = @model.getCurrentSession().active
		ToggleEl @$el.$bookingSchedule, currentSessionAction
		ToggleEl @$el.$bookBtn, currentSessionAction

		@

	appendRecurring: ->
		@$el.$content.html ''
		@$el.$bookBtn.remove()
		@instances.push @recurring = new BookAppointment.Views.Recurring
			$container: @$el.$content
			baseView: @
			baseModel: @model

	appendService: ->
		@instances.push @serviceView = new BookAppointment.Views.Service
			el: @$el.$service
			baseView: @
			baseModel: @model

	appendPackages: ->
		@instances.push @packagesView = new BookAppointment.Views.Packages
			el        : @$el.$packages
			baseView  : @
			baseModel : @model

	appendLocations: ->
		@instances.push @locationsView = new BookAppointment.Views.Locations
			el: @$el.$locations
			baseView: @
			baseModel: @model

	appendDatepicker: ->
		@instances.push @datepickerView = new BookAppointment.Views.Datepicker
			el: @$el.$datepicker
			baseModel: @model
			baseView: @

	appendSchedule: ->
		@instances.push @scheduleView = new BookAppointment.Views.Schedule
			el: @$el.$schedule
			baseModel: @model
			baseView: @

	openBookingWizardWithButton: (e) ->
		e?.preventDefault()
		@model.set 'resetToFirstStep', 'recalculate'

		# display loading because the page will be reloaded
		if Device.isMobile()
			@showLoading()

		@openWizard()

	openWizardRecurringWithButton: (e) ->
		e?.preventDefault()
		@openWizardRecurring()

	openWizard: ->
		bookingData = @getBookingData()
		# after opening wizard from the widget/search results
		# we need to reset these fields to make sure if the wizard
		# is opened again we won't have invalid IDs in the wizard model
		@model.resetAppointmentToDefault()

		openWizard = ->
			window.bookingHelper.openWizard
				bookingData: bookingData

		WizardAuth openWizard

	openWizardRecurring: ->
		bookingData = @getRecurringData()
		@model.resetAppointmentToDefault()

		openWizard = ->
			window.bookingHelper.openWizardRecurring
				bookingData: bookingData

		WizardAuth openWizard,
			msg: 'Please log in to book a monthly package'


	getBookingData: ->
		formattedData = WizardDataFormatter.formatFromBookAppointment @model.toJSON()
		formattedData.bookedFrom = 'profile'
		formattedData

	getRecurringData: ->
		formattedData = WizardDataFormatter.formatRecurringDataFromBookAppointmnet @model.toJSON()
		formattedData.bookedFrom = 'profile'
		formattedData

#	save: ->

	# for small card view
	showServices: -> @$el.$service.show()
	hideServices: ->
		@$el.$service.hide()
		@model.resetServiceToDefault()

	# dirty trigger for outside (profile/mobile_view.coffee)
	triggerClose: -> $(document).trigger 'bookAppointment:close'

module.exports = AbstractView

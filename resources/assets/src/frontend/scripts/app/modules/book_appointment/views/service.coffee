Backbone = require 'backbone'
Select = require 'framework/select'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Select = require 'framework/select'
HumanTime = require 'utils/human_time'
Formats = require 'config/formats'
Numeral = require 'numeral'

require 'backbone.stickit'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@addBindings()
		@render()

	addListeners: ->

	addBindings: ->
		serviceOptions =
			observe: 'current_service_id'
			selectOptions:
				collection: @generateServiceSelect()
				labelPath: 'detailed_name'
				valuePath: 'service_id'
			getVal: @onGetValService
			updateModel: false
			afterUpdate: (el) -> el.change() # forcing select2 to update selected option
			initialize: (el) ->
				new Select el

		sessionOptions =
			observe: 'current_session_id'
			selectOptions:
				collection: @generateSessionSelect
				labelPath: 'duration_text'
				valuePath: 'session_id'
			getVal: @onGetValSession
			updateModel: false
			afterUpdate: (el) -> el.change() # forcing select2 to update selected option
			initialize: (el) ->
				new Select el,
					dropdownClass: 'select_session--dropdown'
					templateSelection: @selectionTemplate

		@bindings =
			'[name="service"]': serviceOptions
			'[name="session"]': sessionOptions

			'.booking_widget--price':
				classes:
					'm-show':
						observe: 'current_session_id'
						onGet: (val) =>
							session = @baseModel.getSession val
							return false unless session
							!!session.price

			'.booking_widget--packages':
				classes:
					'm-show':
						observe: 'current_session_id'
						onGet: (val) ->
							session = @baseModel.getSession val
							return false unless session
							pkgs = _.filter session.packages, (pkg) -> pkg.number_of_visits > -1
							pkgs.length

			'.booking_widget--price span':
				observe: 'current_session_id'
				updateMethod: 'html'
				onGet: (val) ->
					session = @baseModel.getSession val
					return '' unless session
					if session.active
						price = parseFloat session.price.replace(',', '')
						return Numeral(price).format(Formats.Price.Simple)
					else
						prices = session.packages.map (pkg) -> parseInt(pkg.price.replace(',', ''))
						price = _.min(prices)
						return "<i>from</i> " + Numeral(price).format(Formats.Price.Simple)


	# service select
	generateServiceSelect: ->
		services = @baseModel.get('services')
		if @baseModel.type is 'wizard'
			services = _.filter @baseModel.get('services'), (service) ->
				_.where(service.sessions, {active: 1}).length > 0
		services

	onGetValService: ($el) ->
		val = $el.val()
		newService = @baseModel.getService +val
		if newService?
			newSession = _.first newService.sessions
			@baseModel.set
				current_service_id: newService.service_id
				current_session_id: newSession.session_id
		else
			@baseModel.set
				current_service_id: null
				current_session_id: null
		val

	# session select
	generateSessionSelect: ->
		currentService = @baseModel.getCurrentService()

		opt_labels = []
		sessionWithOptions = {}

		return sessionWithOptions unless currentService

		_.each currentService.sessions, (item) =>
			if item.active
				price = Numeral(item.price.replace(',', '')).format(Formats.Price.Simple)
			else
				return if @baseModel.type is 'wizard'
				return unless item.packages.length
				return unless item.active is 1
				prices = item.packages.map (pkg) -> parseFloat(pkg.price.replace(',', ''))
				price = _.min(prices)
				price = Numeral(price).format(Formats.Price.Simple)

			opt_labels.push(price)

			sessionWithOptions['opt_labels'] = opt_labels
			sessionWithOptions[price] = [
				{
					duration : item.duration
					duration_text : HumanTime.minutes item.duration,
						hourLabel: 'hr'
						hoursLabel: 'hrs'
					session_id : item.session_id
				}
			]

		if _.isEmpty sessionWithOptions
			_.each currentService.sessions, (item) =>

				return if @baseModel.type is 'wizard'
				return unless item.packages.length
				prices = item.packages.map (pkg) -> parseFloat(pkg.price.replace(',', ''))
				price = _.min(prices)
				price = Numeral(price).format(Formats.Price.Simple)

				opt_labels.push(price)

				sessionWithOptions['opt_labels'] = opt_labels
				sessionWithOptions[price] = [
					{
						duration : item.duration
						duration_text : HumanTime.minutes item.duration,
							hourLabel: 'hr'
							hoursLabel: 'hrs'
						session_id : item.session_id
					}
				]
		sessionWithOptions

	onGetValSession: ($el) ->
		val = $el.val()
		session = @baseModel.getSession +val
		@baseModel.set
			current_session_id: session?.session_id

		val

	render: ->
		@$el.html BookAppointment.Templates.Service()
		@stickit @baseModel
		@

	selectionTemplate: (data) ->
		# Customizing selecte option to show the price
		price = $(data.element).parent().attr('label')
		priceBlock = $('<b>').html(price)
		if price
			$('<span>').html(data.text).prepend(priceBlock)
		else
			$('<span>').html(data.text)

module.exports = View

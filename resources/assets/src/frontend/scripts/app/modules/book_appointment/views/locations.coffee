Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

LocationFormatter = require 'utils/formatters/location'

require 'backbone.stickit'

class View extends Backbone.View

	canChangeLocation: false

	events:
		'click .booking_widget--locations--list--item': 'filterLocationEvent'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@scheduleCollection = @baseModel.get('scheduleCollection')

		@bind()
		@cacheDom()
		@render()
		@stickit @baseModel

	bind: ->
		@listenTo @baseModel, 'change:current_service_id', => @render()
		@listenTo @scheduleCollection, 'data:loading', => @canChangeLocation = false
		@listenTo @scheduleCollection, 'data:ready', =>
			@canChangeLocation = true
			@maybeSelectLocation()

		@bindings =
			'[data-widget-timezone]':
				observe: 'location_id'
				onGet: (val) ->
					if val
						location = @baseModel.getCurrentLocation()
						location.timezone

	cacheDom: ->
		@$el.$list = @$el.find('.booking_widget--locations--list')
		@

	render: ->
		@$el.$list.html('')
		currentService = @baseModel.getCurrentService()
		return @ unless currentService?

		currentLocations = []
		_.each currentService.locations, (location, index) =>
			return unless location?

			locationAddress = LocationFormatter.getLocationAddressLabel location, true
			locationName = LocationFormatter.getLocationName location

			html = "<li class='booking_widget--locations--list--item' data-location-id='#{location.id}'><strong>#{locationName}</strong>"
			if location.location_type isnt 'virtual'
				html += " <span>#{locationAddress}</span>"
			html += "<span class='booking_widget--locations--list--item--icon m-place'></span></li>"

			@$el.$list.append html

			if location.location_type isnt 'virtual'
				currentLocations.push
					id: location.id
					index: index

		profileMap?.setCurrentLocations currentLocations if @baseModel.type is 'widget'

		@$el.$locations = @$el.$list.find('[data-location-id]')
		@

	clearLocationFilter: ($location) ->
		if $location
			$location.siblings().removeClass 'm-current'
		else
			@$el.$locations.removeClass 'm-current'
		@

	filterLocationEvent: (ev) ->
		@filterLocation $(ev.currentTarget)
		@

	filterLocation: ($location) ->
		return unless @canChangeLocation

		locationId = +$location.data('location-id')
		isCurrentLocation = locationId is @baseModel.get 'location_id'

		@clearLocationFilter($location)
		$location.addClass 'm-current' if locationId

		unless isCurrentLocation
			@baseModel.set 'location_id', locationId

		@baseView.scheduleView?.filterLocations locationId

		if @baseModel.type is 'widget' and !isCurrentLocation and @baseModel.getCurrentLocation() # prevent filter null location
			profileMap?.openLocation locationId
		@

	selectLocation: (locationId = null) ->
		if locationId is null
			locationId = @baseModel.get('location_id')

		if locationId
			$targetLocation =  @$el.$locations.filter("[data-location-id='#{locationId}']")
			@filterLocation($targetLocation) if $targetLocation.length
		else
			@$el.$locations.first().trigger 'click'
		@

	maybeSelectLocation: ->
		currentLocationId = @baseModel.get('location_id')

		# if no location selected - select the first available location
		unless currentLocationId
			@selectLocation @baseView.scheduleView?.getFirstTimeLocation()
			return

		# check if current service has selected location
		serviceLocationIds = _.pluck(@baseModel.getCurrentService().locations, 'id')
		if serviceLocationIds.indexOf(currentLocationId) > -1
			@selectLocation currentLocationId
			return

		# check if selected location is in the availabilities locations list
		currentLocationHasAvailabilities = @scheduleCollection.where(location_id: currentLocationId).length > 0
		if currentLocationHasAvailabilities
			@selectLocation currentLocationId
			return

		# select first available location
		if @scheduleCollection.length
			currentLocationId = @scheduleCollection.first().location_id
		else
			currentLocationId = @baseModel.getCurrentService().locations[0].id

		@selectLocation currentLocationId

		@

module.exports = View

Backbone = require 'backbone'

class Model extends Backbone.Model

	defaults :
		id            : ''
		from          : ''
		until         : ''
		location_id   : ''
		location_name : ''
		selected      : false
		hide          : false

	remove : ->
		@destroy()

	destroy : ->
		@set 'id', null
		super

module.exports = Model

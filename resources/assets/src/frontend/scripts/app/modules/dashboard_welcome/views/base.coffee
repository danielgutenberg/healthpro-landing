Backbone = require 'backbone'

class View extends Backbone.View

	el: $('#dashboard_welcome')

	initialize: ->
		window.popupsManager.openPopup 'dashboard_welcome'
		window.popupsManager.openPopup 'dashboard_welcome_returning'
		@$el.on 'popup:closed', => $(document).trigger 'tour:start'

module.exports = View

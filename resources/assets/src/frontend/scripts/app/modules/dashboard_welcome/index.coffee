Handlebars = require 'handlebars'

# list of all instances
require '../../../../styles/modules/dashboard_welcome.styl'

window.DashboardWelcome = DashboardWelcome =
	Views:
		Base: require('./views/base')

class DashboardWelcome.App
	constructor: ->
		@view = new DashboardWelcome.Views.Base()

new DashboardWelcome.App()

module.exports = DashboardWelcome

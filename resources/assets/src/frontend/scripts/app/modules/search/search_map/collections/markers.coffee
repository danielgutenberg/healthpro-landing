LocationFormatter = require 'utils/formatters/location'
WizardAuth = require 'helpers/wizard_auth'

class Markers

	constructor: (options) ->
		@instances = []
		@map = options.gmap
		@infoWindow = options.infoWindow

	add: (options) ->
		unless options.coordinates.lat? and options.coordinates.lng?
			return false
		myLatlng = new GMaps.gmaps.LatLng(options.coordinates.lat, options.coordinates.lng)

		hasActiveSessions = false
		hasPackages = false
		_.each options.services, (service) ->
			_.each service.sessions, (session) ->
				hasActiveSessions = true if session.active
				if session.packages.length
					_.each session.packages, (pkg) ->
						hasPackages = true if pkg.number_of_visits < 0
						return
				return
			return

		# improve rating value
		options.provider.rating = options.provider.rating.toString().replace('.', '_')
		options.booking_data = JSON.stringify( options.booking_data )

		options.locationAddress = LocationFormatter.getLocationAddressLabel options.location, true
		options.locationName = LocationFormatter.getLocationName options.location

		options.can_book_appointment = hasActiveSessions
		options.can_book_recurring = hasPackages

		content = SearchMap.Templates.InfoWindow options

		if options.location.location_type is 'home-visit'
			marker = @addCircle options, myLatlng
		else
			marker = @addMarker options, myLatlng

		GMaps.gmaps.event.addListener marker, 'click', => @openWindow(marker, content, options)
		GMaps.gmaps.event.addListener @infoWindow, 'domready', => @addBookingButton()

		@instances.push marker


	addMarker: (options, latLng) ->
		marker = new GMaps.gmaps.Marker
			provider_id: options.provider.id
			position: latLng
			map: @map
			icon: SearchMap.Config.icon
			optimized: false
			type: 'marker'
		marker

	addCircle: (options, latLng) ->
		circle = new GMaps.gmaps.Circle
			strokeColor: '#3db7cb'
			strokeOpacity: 0
			strokeWeight: 2
			fillColor: '#3db7cb'
			fillOpacity: 0.25
			center: latLng
			map: @map
			provider_id: options.provider.id
			type: 'circle'
			radius: options.location.service_area * 1000 # radius in meters
		circle

	addBookingButton: ->
		$bookingButton = $('.search_map--bubble--booking', $(@infoWindow.content_))
		$bookingButton
			.off() # prevent attach event to all locations. dumb way
			.on 'click', (e) =>
				e.preventDefault()

				bookingData = $(e.target).data('booking-data')
				openWizard = ->
					window.bookingHelper.openWizard
						bookingData: bookingData
				WizardAuth openWizard

	openWindow: (marker, content, options) ->

		# display the full circle on the map
		if marker instanceof GMaps.gmaps.Circle
			@map.setCenter marker.getCenter()
			@map.fitBounds marker.getBounds()
			GMaps.gmaps.event.addListenerOnce @map, 'bounds_changed', -> @setZoom(15) if @getZoom() > 15
		# zoom to the marker
		else
			@map.setZoom 15


		@infoWindow.setContent(content)
		@adjustInfowindowHeight(@infoWindow)
		@infoWindow.open @map, marker

		SearchMap.trigger 'check_provider', options.provider.id

	reset: ->
		@instances.forEach (marker) ->
			marker.setMap(null)
			marker = null

		@instances = []

		GMaps.gmaps.event.clearListeners @map, 'bounds_changed'

	getSelected: -> _.where @instances, selected: true

	clearSelected: ->
		@instances.forEach (marker) ->
			marker.set 'selected', false
			if marker.type is 'marker'
				marker.setIcon SearchMap.Config.icon
			else if marker.type is 'circle'
				marker.setOptions
					strokeOpacity: 0
					fillOpacity: 0.25

	getPinsByProvider: (id) ->
		_.where(@instances, { 'provider_id': id })

	adjustInfowindowHeight: (infoWindow) ->
		# Dirty way to fix infowindow height calculation
		$('.search_map--dummy_bubble').remove()
		$('.search_map').append('<div class="search_map--bubble search_map--dummy_bubble" style="width:' + infoWindow.minWidth + '">' + infoWindow.getContent() + '</div>')
		calculatedHeight = $('.search_map--dummy_bubble').height()
		infoWindow.setMinHeight(calculatedHeight + 2)

module.exports = Markers


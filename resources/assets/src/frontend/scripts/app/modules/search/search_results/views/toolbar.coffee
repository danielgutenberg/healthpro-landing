Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Stickit = require 'backbone.stickit'

class ToolbarView extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common )
		@setOptions(options)
		@subViews = []
		@addStickit()
		@addListeners()
		@render()
		@initFilters()

	addStickit: ->
		@bindings =
			'[name="sorting"]': 'q.sort'

	addListeners: ->
		@listenTo @searchFiltersModel, 'change:q.sort', -> SearchFilters.trigger 'submit'

	render: ->
		@$el.html SearchResults.Templates.Toolbar

		@stickit(@searchFiltersModel)

		return @

	initFilters: ->
		@subViews.push @filters = new SearchResults.Views.Filters
			filtersCollection: @filtersCollection
			model: @model

		$( @filters.render().el ).prependTo( @$el )

module.exports = ToolbarView

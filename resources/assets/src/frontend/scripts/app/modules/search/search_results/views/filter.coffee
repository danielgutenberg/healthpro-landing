Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	className: 'filters_list--item'
	tagName: 'li'

	events:
		'click .filters_list--item--remove': 'close'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common )

		@setOptions(options)
		@delegateEvents()

	render: ->
		name = @model.get('name')
		value = @model.get('value')
		if name is 'radius' then value = 'radius: ' + value + ' miles'
		if value == '' then return false

		@$el.html SearchResults.Templates.Filter
			name: name
			value: value

		return @

module.exports = View

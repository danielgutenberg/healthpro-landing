Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	className: 'search_results--toolbar--filters'
	tagName: 'dl'

	events:
		'click .filters_list--item--clear': 'removeAllFilters'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common )

		@setOptions(options)
		@subViews = []
		@bind()
		@setDataToModel()

	bind: ->
		@listenTo @, 'rendered', =>
			@cacheDom()
			@checkLength()
			@renderAllFilters()

		@listenTo @filtersCollection, 'update', =>
			@checkLength()
			@renderAllFilters()
			@setDataToModel()

	cacheDom: ->
		@$el.$list = @$('.filters_list')
		@$el.$clear = @$('.filters_list--item.m-clear')
		@$el.$empty = @$('.filters_list--item.m-empty')

	checkLength: ->
		if @filtersCollection.length
			@$el.$clear.removeClass('m-hide')
			@$el.$empty.addClass('m-hide')
		else
			@$el.$clear.addClass('m-hide')
			@$el.$empty.removeClass('m-hide')

	removeAllFilters: ->
		_.forEach @subViews, (view) =>
			view.close()

	removeAllFiltersViews: ->
		_.forEach @subViews, (view) =>
			view.$el.remove()

	renderAllFilters: ->
		@removeAllFiltersViews()
		if @filtersCollection.length
			@filtersCollection.forEach (model) =>
				@subViews.push filter = new SearchResults.Views.Filter model: model
				$( filter.render().el ).insertBefore( @$el.$clear )

	render: ->
		@$el.html SearchResults.Templates.Filters()

		@trigger 'rendered'

		return @

	setDataToModel: ->
		radius = @filtersCollection.where({ name: 'radius' })
		if radius.length > 0
			@model.set('radius', radius[0].get('value'))

module.exports = View

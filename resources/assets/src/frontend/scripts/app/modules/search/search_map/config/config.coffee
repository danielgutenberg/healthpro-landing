module.exports =
	icon:
		url: '/assets/frontend/images/content/search/marker.png'
		size: new GMaps.gmaps.Size('19', '26')
		scaledSize: new GMaps.gmaps.Size(19, 26)
		origin: new GMaps.gmaps.Point(0,0)
		anchor: new GMaps.gmaps.Point(9, 37)

	icon_highlighted:
		url: '/assets/frontend/images/content/search/marker_bright.png'
		size: new GMaps.gmaps.Size('19', '26')
		scaledSize: new GMaps.gmaps.Size(19, 26)
		origin: new GMaps.gmaps.Point(0,0)
		anchor: new GMaps.gmaps.Point(9, 37)

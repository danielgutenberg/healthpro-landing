
Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'

# models
BaseModel = require './models/base'

# collections
MarkersCollection = require './collections/markers'

# templates
Infowindow = require 'text!./templates/infowindow.html'

#config
Config = require './config/config'

# list of all instances
window.SearchMap = SearchMap =
	Views:
		Base: BaseView
	Models:
		Base: BaseModel
	Collections:
		Markers: MarkersCollection
	Templates:
		InfoWindow: Handlebars.compile Infowindow
	Config: Config

# events bus
_.extend SearchMap, Backbone.Events

class SearchMap.App
	constructor: ->
		@model = new SearchMap.Models.Base()
		@view = new SearchMap.Views.Base
			model: @model

		return {
			model: @model
			view: @view
			markersCollection: @view.markersCollection
		}

module.exports = SearchMap

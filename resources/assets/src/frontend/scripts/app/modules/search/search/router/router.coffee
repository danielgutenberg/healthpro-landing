Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class Router extends Backbone.Router

	routes:
		'search?:query': 'search'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Common )
		@previousUrl = options.url
		@isFirstTimeLoad = true

		@setOptions(options)
		@addListeners()

		Search.trigger 'router:ready'

	addListeners: ->
		@listenTo Search, 'filters:updated', @navigateTo

	navigateTo: (url) ->
		if Backbone.History.started
			@navigate('search?' + url, {trigger: true})
			if @previousUrl isnt url then SearchResults.trigger('reset')

			@isFirstTimeLoad = false
			@previousUrl = url
		else
			@navigateTo(url)


	search: (query) ->
		Search.trigger 'url:changed', query

module.exports = Router

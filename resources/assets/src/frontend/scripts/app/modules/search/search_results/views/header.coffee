Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common )

		@setOptions(options)
		@addListeners()

	addListeners: ->
		@listenTo @baseModel, 'change:results_count', => @render()
		@listenTo @searchFiltersModel, 'change:q.search_by', @toggleHeader
		@listenTo @filtersCollection, 'update', => @render()

	render: ->
		@$el.html SearchResults.Templates.Header
			results_count: @baseModel.get('results_count')
			results_label: if @baseModel.get('results_count') == 1 then 'Result' else 'Results'
			professionals_label: if @baseModel.get('results_count') == 1 then 'Professional' else 'Professionals'
			location: if @searchFiltersModel.get('q.location_type_id') != '2' then @searchFiltersModel.get('q.location')
			radius: @searchFiltersModel.get('q.radius')
			is_zip: $.isNumeric(@searchFiltersModel.get('q.location'))

		return @

	toggleHeader: (model) ->
		if model.get('q.search_by') == 'name' then @$el.addClass 'm-hide' else @$el.removeClass 'm-hide'

module.exports = View

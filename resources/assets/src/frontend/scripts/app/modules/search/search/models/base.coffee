Backbone = require 'backbone'
URI = require 'URIjs/URI'
getApiRoute = require 'hp.api'
Qs = require 'qs'

class Model extends Backbone.Model

	defaults:
		isFirstTimeLoad: true
		# location
		# type
		# radius
		# query
		# from
		# duration
		# gap
		# page
		# per_page

	initialize: ->
		super

		@addListeners()

	addListeners: ->
		@listenTo Search, 'parts:inited', => @startup()
		@listenTo Search, 'url:changed', => @startup()
		@listenTo @, 'change:search_api_url', =>
			SearchResults.trigger 'fetch', @get('search_api_url')

	startup: ->
		url = new URI().href()

		if @checkUrl(url)
			if @get('isFirstTimeLoad')
				@passDataToFilters( @getQueryJson( @parseQuery(url) ) )
				@set('isFirstTimeLoad', false)

			@set
				current_url: url
				search_api_url: @prepareUrl(url)

		else
			# trigger Search Filters to build default query
			SearchFilters.trigger 'startup'

	prepareUrl: (url) ->
		result = @parseUrl(url)
		if result
			return @buildUrl(result.type, result.query)
		else
			return false

	parseUrl: (url) ->
		query = @parseQuery(url)
		queryJSON = @getQueryJson(query) # get query's json
		type = queryJSON.type ? 'single'
		delete queryJSON.type if queryJSON.type?

		# Cleaning query from unnecessary params
		if queryJSON.search_by == 'service'
			delete queryJSON.provider_id if queryJSON.provider_id?

		else if queryJSON.search_by == 'name'
			delete queryJSON.location if queryJSON.location?
			delete queryJSON.service if queryJSON.service?

		delete queryJSON.search_by if queryJSON.search_by?
		delete queryJSON.name if queryJSON.name?

		query = Qs.stringify( queryJSON )

		return {
			type: type
			query: query
		}

	parseQuery: (url) ->
		query = URI(url).search()
		query = query.slice(1) if query.charAt(0) is '?' # remove question

	getQueryJson: (query) ->
		return Qs.parse(query) # get query's json


	buildUrl: (type, query) ->
		switch type
			when 'single' then getApiRoute('providers-search', {query: query})
			when 'class' then getApiRoute('classes-search', {query: query})

	# check if url query isn't empty
	checkUrl: (url) ->
		return URI(url).search().length

	passDataToFilters: (data) ->
		SearchFilters.trigger 'fetch:query', data

module.exports = Model

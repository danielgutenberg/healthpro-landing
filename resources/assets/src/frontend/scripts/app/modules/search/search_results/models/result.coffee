Backbone = require 'backbone'

class Model extends Backbone.Model

	initialize: (attrs) ->
		@prepareData(attrs)

	prepareData: (attrs) ->
		@set
			'locations_ids': @prepareLocations( @get('locations') )

	prepareLocations: (locations) ->
		ids = []
		_.forEach locations, (value, key) ->
			ids.push value.id

		return ids.join()

module.exports = Model

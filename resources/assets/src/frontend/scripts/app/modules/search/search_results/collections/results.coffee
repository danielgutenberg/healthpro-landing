Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
searchUrl = require 'utils/search_url'
Qs = require 'qs'
serviceFormatter = require 'utils/formatters/service'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		Cocktail.mixin( @, Mixins.Common )

		@setOptions(options)

		@canLoadMore = false
		@currentPage = 1

		@addListeners()

	addListeners: ->
		@listenTo SearchResults, 'fetch', @fetch
		@listenTo SearchResults, 'results:load_more', @checkLoadMoreAbility
		@listenTo SearchResults, 'reset', @reset
		@listenTo @, 'results:loading', => @canLoadMore = false
		@listenTo @, 'results:ready', => @canLoadMore = true

	reset: ->
		@currentPage = 1
		super

	fetch: (url) ->
		if url
			SearchResults.trigger 'loading', @canLoadMore, @currentPage
			@trigger 'results:loading'
			$.ajax
				url: url
				method: 'get'
				success: (res) =>
					if res.data?.results?
						@baseModel.set('results_count', res.data.count)
						if _.isEmpty( res.data?.results )
							@trigger 'results:empty'
						else
							@addData(res.data.results)
					else
						@baseModel.set('results_count', 0)
						@trigger 'results:empty'

					parsedUrl = searchUrl.parseUrl( searchUrl.getCurrentUrl() )

					@trigger 'results:ready'
					SearchResults.trigger 'results:ready', searchUrl.getQueryJson parsedUrl.query
					SearchResults.trigger 'ready'

				error: (err) =>
					@trigger 'results:error'
					SearchResults.trigger 'ready'

		else
			Alerts.addNew('error', 'enter params in search')

	addData: (results) ->
		_.forEach results, (result) =>
			unless _.isEmpty result.services
				@add @resultsDataImprovements(result)

	# we set service locations to only be location of that specific result

	# do not add results with only monthly packages
	# should be refactored later
	resultsDataImprovements: (result) ->
		result.services.forEach (service) =>
			service.detailed_name = serviceFormatter.getServiceName service
			service.locations = result.locations
		result

	checkLoadMoreAbility: ->
		if @length >= @baseModel.get('results_count')
			@canLoadMore = false
		@loadMore()

	loadMore: ->
		if @canLoadMore
			parsedUrl = searchUrl.parseUrl( searchUrl.getCurrentUrl() )
			queryJSON = searchUrl.getQueryJson parsedUrl.query
			queryJSON.page = ++@currentPage
			parsedUrl.query = Qs.stringify queryJSON
			@fetch( searchUrl.buildUrl(parsedUrl.type ,parsedUrl.query) )

			@canLoadMore = false

module.exports = Collection

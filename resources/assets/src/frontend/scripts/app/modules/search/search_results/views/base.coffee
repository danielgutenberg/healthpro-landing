Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Stickit = require 'backbone.stickit'

class View extends Backbone.View

	el: $('.search_results')

	# events:

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common )

		@subViews = []

		@setOptions(options)
		@addListeners()
		@bootstrap()

	bootstrap: ->
		@cacheDom()
		@initParts()
		@stickit()

	addListeners: ->
		@listenTo SearchResults, 'loading', @showLoading
		@listenTo SearchResults, 'ready', @hideLoading
		@listenTo Search, 'filters:updated', @scrollResults

	cacheDom: ->
		@$el.$header = @$('.search_results--header')
		@$el.$toolbar = @$('.search_results--toolbar')
		@$el.$content = @$('.search_results--content')
		@$el.$loading = @$('.search_results--loading')
		@$el.$loading_more = @$('.search_results--loading_more')

	initParts: ->
		@initHeader()
		@initToolbar()
		@initResults()

	initToolbar: ->
		@subViews.push @toolbar = new SearchResults.Views.Toolbar
			filtersCollection: @filtersCollection
			searchFiltersModel: @searchFiltersModel
			model: @model
			el: @$el.$toolbar

	initResults: ->
		@subViews.push @results = new SearchResults.Views.Results
			el: @$el.$content
			baseView: @
			resultsCollection: @model.get('resultsCollection')
			markersCollection: @markersCollection

	initHeader: ->
		@subViews.push @header = new SearchResults.Views.Header
			el: @$el.$header
			baseView: @
			baseModel: @model
			filtersCollection: @filtersCollection
			searchFiltersModel: @searchFiltersModel

		@header.render()

	showLoading: (canLoadMore, currentPage) ->
		if canLoadMore && currentPage > 1
			@$el.$loading_more.removeClass('m-hide')
		else
			@$el.$loading.removeClass('m-hide')

		return @

	hideLoading: ->
		@$el.$loading.addClass('m-hide')
		@$el.$loading_more.addClass('m-hide')

		return @

	scrollResults: ->
		$('.search--results').scrollTop(0)

module.exports = View

# init perfect scrollbar
require('perfect-scrollbar')($)
require 'perfect-scrollbar/dist/css/perfect-scrollbar.min.css'

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

SearchFilters = require 'app/modules/search/search_filters'
SearchResults = require 'app/modules/search/search_results'
SearchMap = require 'app/modules/search/search_map'

class View extends Backbone.View

	el: $('.search')
	advancedOpened: true

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins

		@setOptions(options)
		@subViews = []
		@mobileBreakpoint = 680

		@initParts()
		@cacheDom()
		@cacheSizes()
		@addListeners()
		@bindActions()

		@setMainHeight()
		@hideAdvanced()
		@initPerfectScroll()
		@stickMap()
		@stickFilters()
		@filtersState()

	addListeners: ->
		@listenTo SearchFilters, 'advanced_filters:opened', =>
			@advancedOpened = true
			@showAdvanced()
		@listenTo SearchFilters, 'advanced_filters:closed', =>
			@advancedOpened = false
			@hideAdvanced()

	initParts: ->
		@initSearchFilters
			type: 'search'
		@initSearchMap()
		@initSearchResults()

		Search.trigger 'parts:inited'

	initSearchFilters: ->
		@subViews.push @searchFilters = new SearchFilters.App
			type: 'search'

		@searchFilters.view.render()

	initSearchMap: ->
		@subViews.push @searchMap = new SearchMap.App()

	initSearchResults: ->
		@subViews.push @searchResults = new SearchResults.App
			filtersCollection: @searchFilters.model.get('filtersCollection')
			markersCollection: @searchMap.markersCollection
			searchFiltersModel: @searchFilters.model

	cacheDom: ->
		# search layout nodes
		@$el.$map = @$('[data-search-node="map"]')
		@$el.$mapWidth = @$el.$map.width()
		@$el.$map.$opener = $('[data-search-map-opener]', @$el.$map)
		@$el.$searchResults = $('[data-search-node="results"]')
		@$el.$searchMain = $('[data-search-node="main"]')

		# other blocks
		@$searchResults = @$('[data-search-results]')
		@$searchMap = @$('[data-search-map]')

		# filters
		@$searchFilters = @$('[data-search-filters]')
		@$searchFilters.$fieldset_container = $('[data-search-filters-node="fieldset_container"]', @$searchFilters)
		@$searchFilters.$fieldset = $('[data-search-filters-node="fieldset"]', @$searchFilters)
		@$searchFilters.$submit = $('[data-search-filters-node="submit"]', @$searchFilters)
		@$searchFilters.$submit.$button = $('button[type="submit"]', @$searchFilters.$submit)
		@$searchFilters.$advanced_btn = $('[data-search-filters-node="advanced_btn"]', @$searchFilters)
		@$searchFilters.$buttons = $('[data-search-filters-node="buttons"]', @$searchFilters)
		@$searchFilters.$advanced = $('.search_filters--form_advanced', @$searchFilters)
		@$searchFilters.$form = $('.search_filters--form', @$searchFilters)

		@$el.$searchFilters = @$('[data-search-node="filters"]')
		@$el.$searchFilters.$nav = $('[data-search-filters-node="nav"]', @$el.$searchFilters)
		@$el.$searchFilters.$nav.$item = $('[data-search-by]', @$el.$searchFilters)
		@$el.$searchFilters.$nav.$item.$input = $('[name="search_by_radio"]', @$el.$searchFilters.$nav.$item)
		@$el.$searchFilters.$content = $('[data-search-filters-node="content"]', @$el.$searchFilters)
		@$el.$searchFilters.$opener = $('[data-search-filters-node="filters_opener"]', @$el.$searchFilters)

		# global
		@$window = $(window)
		@$document = $(document)
		@$layout = $('.layout')
		@$layout.$header = $('.layout--header')
		@$layout.$footer = $('.layout--footer')

	# recalculate variables sizes
	cacheSizes: ->
		@$window._height = @$window.height()
		@$document._height = @$document.height()
		@$layout.$header._height = @$layout.$header.outerHeight(true)
		@$el.$map._width = @$el.$map.width()
		@$searchFilters.$buttons._height = @$searchFilters.$buttons.outerHeight(true)
		@$searchFilters.$buttons._offset = @$searchFilters.$fieldset_container.outerHeight(true) + @$el.$searchFilters.$nav.outerHeight(true)

	bindActions: ->
		@$el.$map.$opener.on 'click', =>
			@toggleMap()

		@$el.$searchFilters.$opener.on 'click', (e) =>
			@toggleFilters()

		@$searchFilters.$submit.$button.on 'click', (e) =>
			@closeFiltersOnSubmit()

		@$el.$searchFilters.$nav.$item.$input.on 'change', (e) =>
			@cacheSizes()

		@$el.$searchResults.on 'scroll', (e) =>
			if @$el.$searchResults.scrollTop() >= @$searchResults.height() - @$el.$searchResults.height() - 300

				SearchResults.trigger 'results:load_more'

		@$window.on 'scroll', =>
			@stickMap()
			@stickFilters()

		@$window.on 'resize', =>
			@setMainHeight()
			@initPerfectScroll()
			@stickMap()
			@cacheSizes()
			@setFiltersOpened()

	# toggle map
	toggleMap: ->
		if @$el.$map.hasClass 'm-opened'
			@closeMap()

		else
			@openMap()

		@searchMap.view.resize()
		@updateMapWidth()

	openMap: ->
		@$el.$map.addClass('m-opened')
		@$el.addClass('m-opened_map')
		@$el.$map.$opener.addClass('m-opened')
		@$searchResults.addClass('m-compact')

	closeMap: ->
		@$el.$map.removeClass 'm-opened'
		@$el.removeClass('m-opened_map')
		@$el.$map.$opener.removeClass('m-opened')
		@$searchResults.removeClass('m-compact')


	stickMap: ->
		if @$window.width() < 1200 && @$window.width() > 980
			mapCalcHeight = @$window.height() - @$layout.$header._height - @$searchFilters.$buttons._height
			@$searchMap.height(mapCalcHeight)
			@$el.$map.$opener.css
				top: mapCalcHeight / 2

			if @$searchFilters.hasClass('m-advanced_opened') && !@$searchFilters.hasClass('m-opened')
				if @$window.scrollTop() > 0
					@$searchMap.addClass 'm-fixed'
					@updateMapWidth()
					@$el.$map.$opener.css
						top: mapCalcHeight / 2 + @$layout.$header._height + @$searchFilters.$buttons._height

				else
					@$searchMap.removeClass 'm-fixed'
			else
				if @$window.scrollTop() >= @$searchFilters.$buttons._offset
					@$searchMap.addClass 'm-fixed'
					@updateMapWidth()
					@$el.$map.$opener.css
						top: mapCalcHeight / 2 + @$layout.$header._height + @$searchFilters.$buttons._height

				else
					@$searchMap.removeClass 'm-fixed'

			# When reach footer
			if @$window.scrollTop() >= @$document.outerHeight(true) - @$window.outerHeight(true) - @$layout.$footer.outerHeight(true)
				if @$searchMap.hasClass 'm-fixed'
					footerScrolled = @$window.scrollTop() + @$window.outerHeight(true) - @$layout.$footer.offset().top
					@$searchMap.css
						top: -footerScrolled

					@$el.$map.$opener.css
						'margin-top': -footerScrolled

			else
				@$searchMap.css
					top: 0

		else if @$window.width() < 980
			if @$window.scrollTop() > @$el.$map.offset().top
				@$searchMap.addClass 'm-fixed'
				@updateMapWidth()

			else
				@$searchMap.removeClass 'm-fixed'

			# When reach footer
			if @$window.scrollTop() >= @$document.outerHeight(true) - @$window.outerHeight(true) - @$layout.$footer.outerHeight(true)
				if @$searchMap.hasClass 'm-fixed'
					footerScrolled = @$window.scrollTop() + @$window.outerHeight(true) - @$layout.$footer.offset().top
					@$searchMap.css
						top: -footerScrolled

					@$el.$map.$opener.css
						'margin-top': -footerScrolled

		else
			@$searchMap
				.height('100%')
				.removeClass('m-fixed')
				.css
					top: 0

			@updateMapWidth()

	updateMapWidth: ->
		@$searchMap.width(@$el.$map.width())
		@searchMap.view.resize()

	setMainHeight: ->
		if @$window.width() > @mobileBreakpoint
			mainHeight = @$window.outerHeight()
			@$el.$searchMain.height(mainHeight)
		else
			@$el.$searchMain.height('auto')


	initPerfectScroll: ->
		if @$window.width() > @mobileBreakpoint
			@$el.$searchResults.perfectScrollbar
				suppressScrollX: true
				wheelPropagation: true
				minScrollbarLength: 20

		else
			if @$el.$searchResults.hasClass 'ps-container'
				@$el.$searchResults.perfectScrollbar 'destroy'

	toggleFilters: ->
		if @$searchFilters.hasClass('m-opened')
			@closeFilters()
		else
			@openFilters()

	closeFilters: ->
		@$searchFilters.$buttons._offset = @$searchFilters.$fieldset_container.outerHeight(true) + @$el.$searchFilters.$nav.outerHeight(true)

		@$searchFilters
			.animate
				# leaving visible just opener buttons
				'margin-top': -@$searchFilters.$buttons._offset
			.removeClass('m-opened')

	openFilters: (animate = true) ->
		if animate
			@$searchFilters
				.animate
					'margin-top': 0
		else
			@$searchFilters.css
				'margin-top': 0

		@$searchFilters.addClass('m-opened')

	closeFiltersOnSubmit: ->
		if @$window.width() < @mobileBreakpoint then @closeFilters()

	filtersState: ->
		# Closing filters if it's not First time load
		if @$window.width() < @mobileBreakpoint
			if !@model.get('isFirstTimeLoad')
				@closeFilters()

	setFiltersOpened: ->
		if @$window.width() > @mobileBreakpoint
			@openFilters(false)

	showAdvanced: ->
		@$searchFilters.addClass('m-advanced_opened')

	hideAdvanced: ->
		@$searchFilters.removeClass('m-advanced_opened')
		@advancedOpened = false

	stickFilters: ->

	unstickFilters: ->

module.exports = View

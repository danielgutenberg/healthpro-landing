Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'
FiltersView = require './views/filters'
FilterView = require './views/filter'
ResultsView = require './views/results'
ResultView = require './views/result'
ResultListedView = require './views/result_listed'
HeaderView = require './views/header'
ToolbarView = require './views/toolbar'

# models
BaseModel = require './models/base'
ResultModel = require './models/result'

# collections
ResultsCollection = require './collections/results'

# templates
BaseTmpl = require 'text!./templates/base.html'
FiltersTmpl = require 'text!./templates/filters.html'
FilterTmpl = require 'text!./templates/filter.html'
ResultTmpl = require 'text!./templates/result.html'
ResultListedTmpl = require 'text!./templates/result_listed.html'
HeaderTmpl = require 'text!./templates/header.html'
ToolbarTmpl = require 'text!./templates/toolbar.html'

# list of all instances
window.SearchResults = SearchResults =
	Views:
		Base: BaseView
		Filters: FiltersView
		Filter: FilterView
		Results: ResultsView
		Result: ResultView
		ResultListed: ResultListedView
		Header: HeaderView
		Toolbar: ToolbarView
	Models:
		Base: BaseModel
		Result: ResultModel
	Collections:
		Results: ResultsCollection
	Templates:
		Base: Handlebars.compile BaseTmpl
		Filters: Handlebars.compile FiltersTmpl
		Filter: Handlebars.compile FilterTmpl
		Result: Handlebars.compile ResultTmpl
		ResultListed: Handlebars.compile ResultListedTmpl
		Header: Handlebars.compile HeaderTmpl
		Toolbar: Handlebars.compile ToolbarTmpl

# events bus
_.extend SearchResults, Backbone.Events

class SearchResults.App
	constructor: (options) ->
		@setOptions(options)

		@model = new SearchResults.Models.Base
			init_url: @initUrl

		@view = new SearchResults.Views.Base
			model: @model
			filtersCollection: @filtersCollection
			markersCollection: @markersCollection
			searchFiltersModel: @searchFiltersModel

		return {
			model: @model
			view: @view
		}

	setOptions: (options) ->
		@filtersCollection = options?.filtersCollection ? null
		@markersCollection = options?.markersCollection ? null
		@searchFiltersModel = options?.searchFiltersModel ? null

module.exports = SearchResults

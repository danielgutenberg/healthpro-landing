Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Moment = require 'moment'
searchUrl = require 'utils/search_url'
collectionUtils = require 'utils/collection_utils'

class View extends Backbone.View

	events:
		'click .search_results--empty--show_next': 'triggerShowNext'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common )

		@subViews = []

		@setOptions(options)

		@cacheDom()
		@addListeners()

	cacheDom: ->
		@$el.$empty = @$('.search_results--empty')

	addListeners: ->
		@listenTo @resultsCollection, 'add', (model) =>
			@addResult(model)

		@listenTo @resultsCollection, 'reset', => @reset()

		@listenTo @resultsCollection, 'results:loading', =>
			@hideEmpty()

		@listenTo @resultsCollection, 'results:empty', =>
			@showEmpty()

	addResult: (model) ->
		model.set('availabilitiesSearchUrl', @generateAvailabilitiesSearchUrl(model))
		@subViews.push result = new SearchResults.Views.Result
			model: model
			baseView: @baseView

		@$el.append( result.render().el )

		@addResultLocations(model)

		return @

	addResultLocations: (model) ->
		locations = model.get('locations')
		provider = model.get('profile')
		services = model.get('services')

		date = null
		switch @baseView.searchFiltersModel.get('q').from
			when 'today' then date = Moment( new Date() ).format('YYYY-MM-DD')
			when 'tomorrow' then date = Moment( new Date() ).add(1, 'day').format('YYYY-MM-DD')
			else date = Moment( @baseView.searchFiltersModel.get('q').from ).format('YYYY-MM-DD')

		servicesArr = collectionUtils.convertCollectionToArray(services)

		locations.forEach (location) =>
			@markersCollection.add
				coordinates:
					lat: location.address.latitude
					lng: location.address.longitude
				provider:
					id: provider.id
					photo: provider.avatar
					name: provider.full_name
					title: provider.title
					location: location.name
					address: location.address.address
					slug: provider.slug
					services: provider.services
					rating: provider.ratings.overall
					selected: false
				location: location
				services: servicesArr
				booking_data:
					bookedFrom: 'search'
					resetToFirstStep: 'recalculate'
					provider_id: provider.id
					from: Moment( new Date( @baseView.searchFiltersModel.get('date') ) ).format('YYYY-MM-DD')
					date: Moment( new Date( @baseView.searchFiltersModel.get('date') ) ).format('YYYY-MM-DD')
					services: servicesArr
					locations: locations
					availabilitiesSearchUrl: model.get('availabilitiesSearchUrl')

		return @

	reset: ->
		@subViews.forEach (view) =>
			view.close?()
			view.remove()

		@subViews = []

		@markersCollection.reset()

	showEmpty: ->
		unless @resultsCollection.length
			@$el.$empty.removeClass('m-hide')

	hideEmpty: ->
		@$el.$empty.addClass('m-hide')

	triggerShowNext: ->
		SearchResults.trigger 'show_next'

	generateAvailabilitiesSearchUrl: (model, date) ->
		date = date ? Moment( new Date() ).format('YYYY-MM-DD')

		currentUrl = searchUrl.getCurrentUrl()
		parsedUrl = searchUrl.parseUrl( currentUrl )
		query = parsedUrl.query
		type = parsedUrl.type
		queryJSON = searchUrl.getQueryJson query
		queryJSON.provider_id = model.get('profile').id
		queryJSON.per_page = undefined
		queryJSON.from = date
		queryJSON.until = {} if queryJSON.until?

		newUrl = searchUrl.buildUrl( type, searchUrl.buildQuery(queryJSON) )

		return newUrl

module.exports = View

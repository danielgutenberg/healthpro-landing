Backbone = require 'backbone'
InfoBubble = require 'framework/infobubble'

class View extends Backbone.View

	initialize: ->
		@map = @initMap(40.7142700, -74.0059700)
		@geocoder = new GMaps.gmaps.Geocoder()

		@addListeners()
		@bindActions()

		@markersCollection = new SearchMap.Collections.Markers
			gmap: @map
			infoWindow: @infoWindow

	addListeners: ->
		@listenTo SearchResults, 'results:ready', @onResultsReady

		GMaps.gmaps.event.addListener @map, 'click', =>
			if @infoWindow? then @infoWindow.close()

		@listenTo SearchResults, 'highlight_results', @showProvidersPins
		@listenTo SearchMap, 'check_provider', @checkProvider

	initMap: (lat, lng) ->
		myLatlng = new GMaps.gmaps.LatLng(lat, lng)
		mapOptions =
			zoom: 11
			center: myLatlng
		map = new GMaps.gmaps.Map($('.search_map')[0], mapOptions)

		@initInfoWindow()

		return map

	initInfoWindow: ->
		@infoWindow = new InfoBubble
			map: @map
			hideCloseButton: true
			disableAnimation: true
			arrowPosition: 50
			shadowStyle: 0
			borderWidth: 0
			padding: 0
			borderRadius: 2
			minHeight: 153
			maxHeight: 500
			minWidth: 220
			maxWidth: 220
			backgroundClassName: 'search_map--bubble'

	bindActions: ->
		$(window).on 'resize', =>
			if $('.search_map').width() < 250
				@infoWindow.setMaxWidth(190)
				@infoWindow.setMinWidth(190)
			else
				@infoWindow.setMaxWidth(220)
				@infoWindow.setMinWidth(220)

	onResultsReady: (query) ->
		if @markersCollection.instances.length
			@centerMap()
		else
			@codeLocation(query.location) unless query.location_type_id is '3'

	codeLocation: (address) ->
		if address?
			@geocoder.geocode { 'address': address }, (results, status) =>
				if status == GMaps.gmaps.GeocoderStatus.OK
					@centerMapByLocation(results[0].geometry.location)
				else
					Alerts.addNew('error', 'Geocode was not successful for the following reason: ' + status)

	centerMap: ->
		bounds = new GMaps.gmaps.LatLngBounds()
		@markersCollection.instances.forEach (marker, index) =>
			if marker instanceof GMaps.gmaps.Marker
				bounds.extend marker.getPosition()
			else if marker instanceof GMaps.gmaps.Circle
				bounds.union marker.getBounds()

		@map.setCenter bounds.getCenter()
		@map.fitBounds bounds
		GMaps.gmaps.event.addListenerOnce @map, 'bounds_changed', -> @setZoom(15) if @getZoom() > 15

	centerMapByLocation: (codedLocation) -> @map.setCenter(codedLocation)

	resize: -> GMaps.gmaps.event.trigger @map, 'resize'

	showProvidersPins: (id) -> @openFirstProviderPin @markersCollection.getPinsByProvider(id)

	showSiblingsPins: (id) -> @highlightPins @markersCollection.getPinsByProvider(id)

	highlightPins: (markers) ->
		@markersCollection.clearSelected()
		markers.forEach (marker) ->
			marker.set 'selected', true
			if marker.type is 'marker'
				marker.setIcon SearchMap.Config.icon_highlighted
			else if marker.type is 'circle'
				marker.setOptions
					strokeOpacity: 0.8
					fillOpacity: 0.35

	openFirstProviderPin: (markers) ->
		marker = _.first markers
		return unless marker
		if marker.type is 'marker'
			GMaps.gmaps.event.trigger marker, 'click'
		else if marker.type is 'circle'
			GMaps.gmaps.event.trigger marker, 'click',
				vertex: 0
				edge: 0
				path: 0

	checkProvider: (id) ->
		if !_.some(@markersCollection.getSelected(), 'provider_id', id)
			@markersCollection.clearSelected()

		@showSiblingsPins(id)

module.exports = View

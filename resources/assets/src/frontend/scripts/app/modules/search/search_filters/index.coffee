Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'

# models
BaseModel = require './models/base'

# collections
FiltersCollection = require './collections/filters'

# templates
BaseSearchTmpl = require 'text!./templates/base_search.html'
BaseLandingTmpl = require 'text!./templates/base_landing.html'

# list of all instances
window.SearchFilters = SearchFilters =
	Config:
		DatepickerFormat: 'mm/dd/yy'
		DatepickerMomentFormat: 'MM/DD/YYYY'
		DateFormat: 'YYYY-MM-DD'
		DatetimeFormat: 'YYYY-MM-DDTHH:mm'
	Views:
		Base: BaseView
	Models:
		Base: BaseModel
	Collections:
		Filters: FiltersCollection
	Templates:
		BaseSearch: Handlebars.compile BaseSearchTmpl
		BaseLanding: Handlebars.compile BaseLandingTmpl

# events bus
_.extend SearchFilters, Backbone.Events

class SearchFilters.App
	constructor: (options) ->
		@model = new SearchFilters.Models.Base
			type: options.type
		@view = new SearchFilters.Views.Base
			model: @model
			type: options.type

		return {
			model: @model
			view: @view
		}

module.exports = SearchFilters

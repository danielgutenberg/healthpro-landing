DeepModel = require 'backbone.deepmodel'
Moment = require 'moment'

class Model extends DeepModel

	defaults:
		q:
			type: 'single'
			from: 'today'
			service: undefined
			name: undefined
			provider_id: undefined
			location_type_id: '0'
			# split_by_location: true
			radius: 10
			query:
				tag: {}
			per_page: 10
			search_by: 'service'
			# page: 1

		date:
			from: Moment().format('MM/DD/YYYY')
			until: Moment().endOf('day').format('MM/DD/YYYY')
		time: Moment().format('HH:mm')

	validation:
		'q.location':
			required: ->
				if @get('q.search_by') == 'service'
					# not required for virtual locations
					return @get('q.location_type_id') != '3'
				else return false
			msg: 'Enter a location'

		'q.service':
			required: ->
				@get('q.search_by') == 'service'
			msg: 'Enter a service'

		'q.name':
			required: ->
				@get('q.search_by') == 'name'
			msg: 'Enter a name'

		'date.from':
			required: ->
				@get('q.search_by') == 'service'
			msg: 'Date is required'

		'date.until': (val) ->
			if @get('q.search_by') == 'service'
				unless val?
					return 'Date is required'

				if Moment(val).isBefore @get('date.from')
					return 'Should be greater than from date'

		'q.clients_address':
			required: ->
				@get('q.search_by') == 'service' and @get('q.location_type_id') == '2' # field is required if searching by service and for home visits
			msg: 'Your address is required'

	initialize: (options) ->

		@type = options?.type
		@addListeners()
		@initCollections()

		@startup() if options?.type is 'landing'

	startup: ->
		# init google maps

		@setLocation()
		@setFrom()
		@setUntil()

	addListeners: ->
		@listenTo SearchFilters, 'startup', => @startup()
		@listenTo SearchFilters, 'fetch:query', @fetch
		@listenTo @, 'change:q.search_by', @updateDates
		@listenTo @, 'change:date.from change:time', @setFrom
		@listenTo @, 'change:date.until', => @setUntil()
		@listenTo @, 'change:q.from', =>
			unless @get('q.from')?
				@set('date', Moment().format('MMMM DD, YYYY'))
			@setUntil()
		@listenTo @, 'change:q.until', => @setUntil(true)

	initCollections: ->
		@set 'filtersCollection', new SearchFilters.Collections.Filters 0, {
			baseModel: @
		}

	fetch: (data) ->
		@set('q', data)
		@setConvertedFrom(data)

		@trigger 'query:fetched'

	setConvertedFrom: (data) ->
		switch data.from
			when 'today'
				@set
					date: from: Moment().format(SearchFilters.Config.DatepickerMomentFormat)
					time: '00:00'
			when 'tomorrow'
				@set
					date: from: Moment().add(1, 'days').format(SearchFilters.Config.DatepickerMomentFormat)
					time: '00:00'
			else
				@set
					date: from: Moment( data.from ).format(SearchFilters.Config.DatepickerMomentFormat)
					time: Moment( data.from ).format('HH:mm')

		@setUntil()

	setFrom: ->
		date = Moment( new Date( @get('date.from') ) ).format(SearchFilters.Config.DateFormat)
		time = @returnValidTime( @get('time'), date )
		@set('q.from', date + 'T' + time)

	setUntil: (force) ->
		unless @get('q.until')?
			@set 'q.until', Moment(@get('date.until')).endOf('day').format(SearchFilters.Config.DatetimeFormat)
		else
			initialUntilMoment = Moment(@get('q.until'))
			if initialUntilMoment.isBefore(@get('q.from'))
				updatedUntilMoment = Moment(@get('q.from')).endOf('day')
				@set 'q.until', updatedUntilMoment.format(SearchFilters.Config.DatetimeFormat)
				@set 'date.until', updatedUntilMoment.format(SearchFilters.Config.DatepickerMomentFormat)
			else if force
				@set 'date.until', initialUntilMoment.format(SearchFilters.Config.DatepickerMomentFormat)

			@set 'q.until', Moment( @get('date.until') ).endOf('day').format(SearchFilters.Config.DatetimeFormat)

	# if selected time is less than current user's time, return current time
	# if not today's date, set time as 00:00
	returnValidTime: (time, date) ->
		return '00:00'

	setLocation: ->
		# we need to make we load gmaps before fetching the location.
		# since webpack doesn't allow to load external script we
		# wait for ScriptJS to load the dependencies for hp.gmaps and
		# then fetching the location.
		GMaps.load().ready => @fetchLocation()

	updateDates: ->
		# if searching by name we set a week period
		# if searching by service we remove until param
		if @get('q.search_by') == 'name'
			@set 'q.from', 'today'
			@set 'q.until', Moment().add(7, 'days').endOf('day').format(SearchFilters.Config.DatetimeFormat)
		else if @get('q.search_by') == 'service'
			@unset 'q.until'

	setCoords: (place) ->
		# only for home visits
		if @get('q.location_type_id') == '2'
			@set 'q.location', place.lat() + ' ' + place.lng()
			@set 'coordinates', place.lat() + ' ' + place.lng() # to catch coordinates changing

	resetCoords: ->
		@set 'q.location', null
		@set 'coordinates', null

	resetClientsAddress: ->
		@set 'q.clients_address', null

	fetchLocation: ->
		unless @get('q').location
			navigator.geolocation.getCurrentPosition (pos) =>
				latitude = pos.coords.latitude
				longitude = pos.coords.longitude
				location =  latitude + ' ' + longitude
				latlng = new GMaps.gmaps.LatLng(latitude, longitude)
				geocoder = new GMaps.gmaps.Geocoder()
				geocoder.geocode { 'latLng': latlng}, (results) =>
					result = results[0]
					unless result?
						return
					i = 0
					len = result.address_components.length
					while i < len
						ac = result.address_components[i]
						if ac.types.indexOf('locality') >= 0
							@set('q.location', ac.long_name)
						i++

				SearchFilters.trigger 'submit' if @type is 'search'
		else
			SearchFilters.trigger 'submit' if @type is 'search'

module.exports = Model

Backbone = require 'backbone'
Handlebars = require 'handlebars'
URI = require 'URIjs/URI'

# views
BaseView = require './views/base'

# models
BaseModel = require './models/base'

# collections

# router
Router = require './router/router'

# templates
BaseTmpl = require 'text!./templates/base.html'

# list of all instances
window.Search = Search =
	Views:
		Base: BaseView
	Models:
		Base: BaseModel
	Collections: {}
	Router: Router
	Templates:
		Base: Handlebars.compile BaseTmpl

# events bus
_.extend Search, Backbone.Events

class Search.App
	constructor: (opts) ->
		Search.on 'router:ready', => @start()

		@router = new Search.Router
			url: opts.url

		@model = new Search.Models.Base()
		@view = new Search.Views.Base
			model: @model

		return {
			model: @model
			view: @view
		}

	start: ->
		# start after data inited
		unless Backbone.History.started
			Backbone.history.start(pushState: true)
			Search.trigger 'history:started'

if $('.search').length
	query = new URI().search()
	new Search.App
		url: if query.length and query.charAt(0) is '?' then query.slice(1) else null

module.exports = Search

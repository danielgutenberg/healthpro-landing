Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Moment = require 'moment'
getRoute = require 'hp.url'

class View extends Backbone.View

	className: 'search_results--item_listed content_block'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common )

		@setOptions(options)

		@subViews = []

		@addListeners()

	addListeners: ->
		@listenTo @, 'rendered', =>
			@cacheDom()

	render: ->
		data = @prepareData()

		@$el.html SearchResults.Templates.ResultListed(data)

		@trigger 'rendered'

		return @

	prepareData: ->
		modelJSON = @model.toJSON()

		# convert rating number to string and make useful for class
		if modelJSON.profile.ratings?
			modelJSON.profile.ratings.overall = modelJSON.profile.ratings?.overall?.toString().replace('.', '_') ? null
			modelJSON.profile.ratings.count = modelJSON.profile.ratings?.details?[0]?.rating_counts ? null
		else
			modelJSON.profile.ratings = {overall: 0}

		@formatLocations(modelJSON)

		modelJSON.conversations_link = getRoute('dashboard-conversation', {profileId: @model.get('profile').id})

		return modelJSON

	formatLocations: (modelJSON) ->
		modelJSON.locations_office = []
		_.each modelJSON.locations, (location) ->
			switch location.location_type
				when 'virtual'
					modelJSON.locations_virtual = 1
				when 'office'
					modelJSON.locations_office.push location.address?.city + ', ' + location.address?.province
				when 'home-visit'
					modelJSON.locations_home = 1
		modelJSON.locations_office = _.uniq(modelJSON.locations_office)

	cacheDom: ->
		@$el.$booking = @$('.search_results--item--booking')

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Moment = require 'moment'
Numeral = require 'numeral'
Formats = require 'config/formats'
LocationFormatter = require 'utils/formatters/location'
HighlightOnHover = require 'framework/highlight_on_hover'

BookAppointment = require 'app/modules/book_appointment' # move to package later
WizardAuth = require 'helpers/wizard_auth'

class View extends Backbone.View

	className: 'search_results--item content_block'

	events:
		'click .profile_card--book_now': 'openWizard'
		'click .profile_card--book_recurring': 'openWizardRecurring'
		'click .profile_card--view_on_map': 'triggerViewOnMap'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common )

		@setOptions(options)

		@subViews = []

		@addListeners()

		new HighlightOnHover()

	addListeners: ->
		@listenTo @, 'rendered', =>
			@cacheDom()
			@initBookAppointment()

	render: ->
		data = @prepareData()

		@$el.html SearchResults.Templates.Result(data)

		@trigger 'rendered'

		return @

	prepareData: ->
		modelJSON = @model.toJSON()

		_.forEach(modelJSON.profile.tags, (value, tag) ->
			unless value.length
				delete modelJSON.profile.tags[tag]
			)

		# convert rating number to string and make useful for class
		if modelJSON.profile.ratings?
			modelJSON.profile.ratings.overall = modelJSON.profile.ratings?.overall?.toString().replace('.', '_') ? null
			modelJSON.profile.ratings.count = modelJSON.profile.ratings?.details?[0]?.rating_counts ? null
		else
			modelJSON.profile.ratings = {overall: 0}

		# calculate the lowest price session for each professional
		prices = []
		_.each modelJSON.services, (service) ->
			_.each service.sessions, (session) ->
				prices.push Numeral(session.price).value() if session.active

		# if we couldn't find prices it means that professional has only monthly packages
		modelJSON.hasAppointments = prices.length > 0

		modelJSON.hasMonthlyPackage = do ->
			hasMonthlyPackage = false
			_.each modelJSON.services, (service) ->
				_.each service.sessions, (session) ->
					_.each session.packages, (pkg) ->
						if pkg.rules.length
							hasMonthlyPackage = true
			hasMonthlyPackage

		# calculate lowest price for the package if professional has only monthly packages
		unless prices.length
			_.each modelJSON.services, (service) ->
				_.each service.sessions, (session) ->
					_.each session.packages, (pkg) ->
						prices.push Numeral(pkg.price).value()

		@formatLocations(modelJSON)

		modelJSON.packagesAvailable = @hasPackagesAvailble()

		availabilities = @model.get('availabilities')
		nextAvailability = Moment( availabilities[Object.keys(availabilities)[0]][0].from, 'YYYY-MM-DDTHH:mm:ss-Z' ).calendar null,
			sameDay: '[Today], hh:mma',
			nextDay: '[Tomorrow], hh:mma',
			sameElse: 'ddd, MMMM ddd, hh:mma'

		modelJSON.nextAvailability = nextAvailability

		return modelJSON

	formatLocations: (modelJSON) ->
		modelJSON.locations_office = []
		_.each modelJSON.locations, (location) ->
			switch location.location_type
				when 'virtual'
					modelJSON.locations_virtual = 1
				when 'office'
					modelJSON.locations_office.push location.address?.city + ', ' + location.address?.province
				when 'home-visit'
					modelJSON.locations_home = 1
		modelJSON.locations_office = _.uniq(modelJSON.locations_office)

	cacheDom: ->
		@$el.$booking = @$('.search_results--item--booking')

	initBookAppointment: ->
		momentFrom = Moment( @baseView.searchFiltersModel.get('date.from'), 'MM/DD/YYYY' )

		@subViews.push @bookAppointment = new BookAppointment.App
			type: 'search'
			presetData:
				from: momentFrom.format('YYYY-MM-DD')
				date: momentFrom.format('YYYY-MM-DD')
				provider_id: @model.get('profile').id
				location_id: @model.get('locations_ids')
				availabilities: @model.get('availabilities')
				services: @model.get('services')
				timezone: @model.get('locations')[0].timezone

		@$el.$booking.append @bookAppointment.view.render().el

	openWizard: ->
		bookingData = @getBookingData()

		openWizard = ->
			window.bookingHelper.openWizard
				bookingData: bookingData

		WizardAuth openWizard

		false

	openWizardRecurring: ->
		bookingData = @getBookingData()
		openWizard = ->
			window.bookingHelper.openWizardRecurring
				bookingData: bookingData
				forcePage: true

		WizardAuth openWizard,
			msg: 'Please log in to book a monthly package'

		false

	getBookingData: ->
		date = null
		switch @baseView.searchFiltersModel.get('q').from
			when 'today' then date = Moment().format('YYYY-MM-DD')
			when 'tomorrow' then date = Moment().add(1, 'day').format('YYYY-MM-DD')
			else date = Moment( @baseView.searchFiltersModel.get('q').from ).format('YYYY-MM-DD')

		service = _.first @model.get('services')
		session = _.findWhere service.sessions, {active: 1}

		# if we have only one location - highlight it in the wizard
		location = if service.locations.length is 1 then _.first(service.locations) else null

		data =
			bookedFrom: 'search'
			resetToFirstStep: 'recalculate'
			provider_id: @model.get('profile').id
			from: date
			date: date
			selected_time:
				service_id: service?.service_id
				session_id: session?.session_id
				location_id: location?.id

		data

	triggerViewOnMap: ->
		SearchResults.trigger 'highlight_results', @model.get('profile').id

	hasPackagesAvailble: ->
		packagesAvailable = false
		_.each @model.get('services'), (service) ->
			_.each service.sessions, (session) ->
				if session.packages?.length
					packagesAvailable = true

		packagesAvailable

module.exports = View

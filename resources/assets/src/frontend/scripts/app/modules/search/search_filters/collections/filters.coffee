Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		Cocktail.mixin( @, Mixins.Common )

		@setOptions(options)

		@addListeners()

	addListeners: ->
		@listenTo @baseModel, 'query:fetched', => @fetch( @baseModel.get('q') )
		if Search? then @listenTo Search, 'filters:updated', => @fetch( @baseModel.get('q') )
		@listenTo @, 'remove', @clearKey

	fetch: (filters) ->
		@reset()

		filters = _.pick filters, 'radius'
		_.forEach filters, (value, key) =>
			if value?
				@push
					name: key
					value: value

	clearKey: (model) ->
		key = model.attributes.name
		@baseModel.set( 'q.'+key+'', undefined)

		if key.indexOf(['location', 'from']) isnt -1
			SearchFilters.trigger 'submit'

module.exports = Collection

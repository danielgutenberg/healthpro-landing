require 'jquery-ui/autocomplete'
require 'jquery-ui/datepicker'

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
URI = require 'URIjs/URI'
Qs = require 'qs'
getApiRoute = require 'hp.api'
Moment = require 'moment'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	el: $('.js-search_filters')

	events:
		'submit': 'submit'
		'click .search_filters--form--advanced_opener': 'toggleAdvancedFilters'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@advancedOpened = false

		@addStickitBindings()
		@addValidation
			selector: 'data-selector'
		@addListeners()
		@cacheDom()


		return @

	cacheDom: ->
		@$el.$advancedFilters = @$('.search_filters--form_advanced')
		@$el.$advancedFiltersOpener = @$('.search_filters--form--advanced_opener')

	addListeners: ->
		@listenTo SearchFilters, 'submit', => @$el.submit()

		if @type == 'search'
			@listenTo SearchResults, 'show_next', @searchNextDays

	addStickitBindings: ->
		@bindings =
			'[name="date_from"]':
				observe: 'date.from'
				initialize: @initDate
				onGet: @onGetDate

			'[name="date_until"]':
				observe: 'date.until'
				initialize: @initDate
				onGet: @onGetDate

			'[name="time"]':
				observe: 'time'

			'[name="type"]': 'q.type'

			'[name="radius"]': 'q.radius'

			'[name="location_type_id"]':
				observe: 'q.location_type_id'

				update: ($el, val, model, options) ->
					if $el.length > 1
						val or (val = [])
						$el.each (i, el) ->
							checkbox = Backbone.$(el)
							checked = _.indexOf(val, checkbox.val()) > -1
							checkbox.prop 'checked', checked
					else
						checked = if _.isBoolean(val) then val else val == $el.val()
						$el.prop 'checked', checked

				getVal: ($el) ->
					if $el.val()?
						 # reset location if previous value were coords or searching for virtual
						if @model.get('q.location_type_id') is '2' or $el.val() is '3'
							@model.resetCoords()
							@model.resetClientsAddress()

					if $el.length > 1
						val = _.reduce($el, ((memo, el) ->
							checkbox = Backbone.$(el)
							if checkbox.prop('checked')
								memo.push checkbox.val()
							memo
						), [])
					else
						val = $el.prop('checked')
						boxval = $el.val()
						if boxval != 'on' and boxval != null
							val = if val then $el.val() else null
					val.toString()

				onGet: (val) ->
					if val?
						if val != '2' or val != '3'
							@model.resetCoords()
							@model.resetClientsAddress()
					val


			'.search_filters--nav--item[data-type="single"]':
				classes:
					'm-active':
						observe: 'q.type'
						onGet: (type) ->
							return type is 'single'

			'.search_filters--nav--item[data-type="class"]':
				classes:
					'm-active':
						observe: 'q.type'
						onGet: (type) ->
							return type is 'class'

			'.search_filters--nav--item[data-search-by="service"]':
				classes:
					'm-active':
						observe: 'q.search_by'
						onGet: (type) ->
							return type is 'service'

			'.search_filters--nav--item[data-search-by="name"]':
				classes:
					'm-active':
						observe: 'q.search_by'
						onGet: (type) ->
							return type is 'name'

			'[name="location"]':
				observe: 'q.location'
				initialize: @initLocations

			'[name="service"]':
				observe: 'q.service'
				initialize: @initServices
				onSet: @onSetServices

			'[name="name"]':
				observe: 'q.name'
				initialize: @initNames
				onSet: @onSetNames

			'.home-search--form--inner':
				classes:
					by_name:
						observe: 'q.search_by'
						onGet: (type) ->
							return type == 'name'
					by_service:
						observe: 'q.search_by'
						onGet: (type) ->
							return type != 'name'

			'.search_filters--form':
				classes:
					by_name:
						observe: 'q.search_by'
						onGet: (type) ->
							return type == 'name'
					by_service:
						observe: 'q.search_by'
						onGet: (type) ->
							return type != 'name'

			'[name="search_by"]':
				observe: 'q.search_by'
				onGet: (type) ->
						return type == 'name'
				onSet: (type) ->
					if type? then return type else return 'service'

			'[name="search_by_radio"]':
				observe: 'q.search_by'

			'.search_filters--clients_address':
				classes:
					'm-hide':
						observe: 'q.location_type_id'
						onGet: (type) ->
							type != '2'

			'.search_filters--location':
				classes:
					'm-hide':
						observe: ['q.location_type_id']
						onGet: (val) ->
							return val[0] == '2' or val[0] == '3'

			'[name="clients_address"]':
				observe: 'q.clients_address'
				initialize: ($el, model, view) =>
					@autocomplete = new GMaps.gmaps.places.Autocomplete $el[0], {
						types: ['address']
						componentRestrictions:
							country: 'us'
					}
					GMaps.gmaps.event.addListener @autocomplete, 'place_changed', =>
						@model.setCoords @autocomplete.getPlace().geometry.location
						@model.set 'q.clients_address', @$('input[name="clients_address"]').val()

					@$('input[name="clients_address"]').on 'input', (e) =>
						val = @$('input[name="clients_address"]').val()
						newVal = val.replace(/[a-zA-Z]\d/g, (match) ->  match[0] + ' ' + match[1])
						unless val == newVal
							@$('input[name="clients_address"]').val(newVal)
							GMaps.gmaps.event.trigger $el[0], 'focus', {}
				onSet: (val) =>
					# to make sure that coords are updated after input changes
					@model.resetCoords()
					val

	render: ->
		@stickit()
		@trigger 'rendered'
		return @

	submit: (e) ->
		unless @model.validate()
			if @model.get('q.search_by') == 'service'
				switch @type
					when 'landing' then @openSearch()
					when 'search'
						@navigateTo(@generateQuery())
			else
				# Don't allow to search without provider_id
				if @model.get('q.provider_id')?
					url = @getBaseUrl() + '/p/' + @model.get('q.provider_id')
					location.href = url
				else
					# Attach provider_id error to name field
					$('[data-selector="q[name]"]').parents('.field').find('.field--error').html('To search by professional name, start typing the name and select from the dropdown.')
					$('[data-selector="q[name]"]').parents('.field').addClass('m-error')

		if e? then e.preventDefault()

	openSearch: ->
		location.href = @generateUrl()

	navigateTo: (url) ->
		Search.trigger 'filters:updated', url

	getBaseUrl: ->
		uri = new URI()
		parts =
			protocol: uri.protocol()
			hostname: uri.hostname()
			port: uri.port()
		URI.build(parts)

	generateUrl: ->
		url = @getBaseUrl() + '/search?' + @generateQuery()

		return url

	generateQuery: ->
		query = Qs.stringify @model.get('q')
		return query

	initLocations: ($el) ->
		$autocomplete = $el.parent().find('.field--autocomplete')

		@bindAutocompleteLoading $el

		$el.autocomplete(
			appendTo: $autocomplete
			source: (request, response) ->
				data = []
				if isNaN(request.term[0])
					$.ajax
						url: getApiRoute('api-core-locations-search')
						method: 'get'
						data:
							q: request.term

						success: (res) ->
							_.forEach res.data, (item) ->
								data.push
									label: item.city + ', ' + item.province

							response data
							return

			select: (e, ui) =>
				$el.val( ui.item.label ).trigger('change')
				return false
		)


	initServices:  ($el) ->
		$autocomplete = $el.parent().find('.field--autocomplete')

		@bindAutocompleteLoading $el

		$el.autocomplete(
			appendTo: $autocomplete
			source: (request, response) =>
				$.ajax
					url: getApiRoute('api-core-services-search')
					method: 'get'
					data:
						q: request.term
						type: 'appointment'

					success: (res) =>
						response @formatServicesForAutocomplete(res.data)
						return

			select: (e, ui) ->
				e.preventDefault()
				$el.data('searchVal', ui.item.value)
				$el.val(ui.item.label).trigger('change')
				return false

			create: ->
				$(@).data('ui-autocomplete')._renderItem = (ul, item) ->
					$('<li>')
						.attr('id', "ui-id-#{item.value}")
						.attr('class', "ui-menu-item m-level m-level-#{item.level}")
						.append($( "<a>").text(item.label))
						.appendTo(ul)

		)

	bindAutocompleteLoading: ($el) ->
		$el.on 'autocompleteresponse', (e) ->
			$(e.currentTarget).closest('.field').removeClass 'm-loading'
		$el.on 'autocompletesearch', (e) ->
			$(e.currentTarget).closest('.field').addClass 'm-loading'

	onSetServices: ->
		if @type == 'landing'
			if $('[name="service"]').val().toLowerCase().indexOf('class') >= 0
				@model.set 'q.type', 'class'
			else
				@model.set 'q.type', 'single'
		$('[name="service"]').val()

	initNames:  ($el) ->
		$autocomplete = $el.parent().find('.field--autocomplete')

		@bindAutocompleteLoading $el

		$el.autocomplete(
			appendTo: $autocomplete
			source: (request, response) =>
				data = []
				$.ajax
					url: getApiRoute('professionals-names-search')
					method: 'get'
					data:
						query: request.term

					success: (res) =>
						response @formatNames(res.data)
						return

			focus: (e, ui) ->
				e.preventDefault()
				$el.val ui.item.label

			select: (e, ui) ->
				$el.data('searchSlug', ui.item.slug)
				$el.data('searchVal', ui.item.value)
				$el.val(ui.item.label).trigger('change')
				e.preventDefault()

			create: ->
				$(@).data('ui-autocomplete')._renderItem = (ul, item) ->
					$('<li>')
						.attr('id', "ui-id-#{item.value}")
						.attr('class', "ui-menu-item m-level m-level-#{item.level}")
						.attr('data-slug', item.slug)
						.append($( "<a>").text(item.label))
						.appendTo(ul)

			response: (e, ui) =>
				if ui.content.length
					unless ui.content[0].value == @model.get('q.provider_id')
						@model.unset('q.provider_id')
						$el.removeData('searchVal')
				else
					@model.unset('q.provider_id')
					$el.removeData('searchVal')

			# selecting the first autocomplete option if there is only one
			# open: (e, ui) ->
			# 	$children = $(@).data('ui-autocomplete').menu.element.children()
			# 	if $children.size() == 1
			# 		$children.children('a').mouseenter().click()

		)
		$el.on 'focus', ->
			$(@).parents('.field').removeClass 'm-error'

	onSetNames: ->
		id = $('[name="name"]').data('searchVal')
		slug = $('[name="name"]').data('searchSlug')
		if id?
			url = @getBaseUrl() + '/p/' + slug
			location.href = url

	initDate: ($el) ->
		$el.datepicker({
			minDate: 0
			dateFormat: SearchFilters.Config.DatepickerFormat
		})

	onGetDate: (val, options) ->
		$el = @$(options.selector)
		switch val
			when 'today'
				$el.datepicker('setDate', Moment().format('MMMM DD, YYYY') )
				return Moment().format('MMMM DD, YYYY')
			when 'tomorrow'
				$el.datepicker('setDate', Moment().add(1, 'days').format('MMMM DD, YYYY'))
				return Moment().add(1, 'days').format('MMMM DD, YYYY')
			else return val

	toggleAdvancedFilters: ->
		if @advancedOpened
			SearchFilters.trigger 'advanced_filters:closed'
			@advancedOpened = false
		else
			SearchFilters.trigger 'advanced_filters:opened'
			@advancedOpened = true

	searchNextDays: ->
		if @model.get('q.until')
			initialDate = new Date(@model.get('q.until'))
		else
			initialDate = new Date(@model.get('q.from'))

		initialDate.setDate(initialDate.getDate() + 3)
		@model.set('q.until', Moment(initialDate).format('YYYY-MM-DDTHH:mm'))
		@navigateTo(@generateQuery())

	formatServicesForAutocomplete: (data) ->
		@formatServices @getServicesTree(data)

	getServicesTree: (data, parentId = null) ->
		tree = _.sortBy _.filter(data, {parent_id: parentId}), 'name'
		# if search doesn't return parent service then return original results
		return data unless tree.length > 0 or parentId != null
		_.each tree, (group) =>
			group.children = @getServicesTree data, group.tag_id
		tree

	formatServices: (tree, results = [], level = 0) ->
		_.each tree, (item) =>
			results.push
				label: item.name
				value: item.tag_name
				level: level
			if item.children and item.children.length
				results = @formatServices item.children, results, ++level
				level = level - 1
		results

	formatNames: (data, results = []) ->
			_.each data, (item) =>
				results.push
					label: item.full_name
					value: item.id
					slug: item.slug
			results

	isLocationTypeSelected: ->
		return @model.get('q.location_type_id')?

module.exports = View

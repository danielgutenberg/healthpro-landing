Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class Model extends Backbone.Model

	defaults:
		results_count: 0

	initialize: (attrs, options) ->
		Cocktail.mixin( @, Mixins.Common )

		@setOptions(options)

		@initCollections()

	initCollections: ->
		resultsCollection = new SearchResults.Collections.Results 0,
			baseModel: @
			model: SearchResults.Models.Result

		@set('resultsCollection', resultsCollection)

module.exports = Model

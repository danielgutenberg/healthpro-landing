Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'

require 'backbone.stickit'

class View extends Backbone.View

	el: $('[data-billing-info]')

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@addStickit()
		@addValidation()

		@cacheDom()

	addListeners: ->
		@listenTo @model, 'data:ready', @render

	addStickit: ->
		@bindings =
			'[name="bill_to_name"]': 'bill_to_name'
			'[name="bill_to_address"]': 'bill_to_address'

	render: ->
		@$el.html Billing.Templates.BillingInfo
		@stickit()
		@cacheDom()
		@hideLoading()

	hideLoading: -> @$el.$loading.addClass('m-hide')
	showLoading: -> @$el.$loading.removeClass('m-hide')

	cacheDom: ->
		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	revert: ->
		@model.resetData()

	save: ->
		errors = @model.validate()
		if !errors
			@baseView.showLoading()
			$.when( @model.save() )
			.then(
				(res) =>
					@baseView.hideLoading()
					vexDialog.buttons.YES.text = 'Ok'
					vexDialog.alert
						message: Billing.Config.messages.updateSuccess
						contentClassName: 'vex-content-custom vex-content-centered m-success'
						overlayClassName: 'vex-overlay-light'

				, (err) =>
					@baseView.hideLoading()
					vexDialog.buttons.YES.text = 'Ok'
					vexDialog.alert
						message: Billing.Config.messages.updateError
						contentClassName: 'vex-content-custom vex-content-centered m-error'
						overlayClassName: 'vex-overlay-light'
			)

module.exports = View

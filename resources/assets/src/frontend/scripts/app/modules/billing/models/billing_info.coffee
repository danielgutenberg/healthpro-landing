Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class Model extends Backbone.Model

	defaults:
		bill_to_name: ''
		bill_to_address: ''

	validation:
		bill_to_name:
			required: true
			msg: 'Please enter name to be shown on invoice'

		bill_to_address:
			required: true
			msg: 'Please enter your address'

	initialize: (options) ->
		super
		Cocktail.mixin @, Mixins.Models.Validation
		@initialData = {}
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-billing-details', { profile_id: window.GLOBALS._PID })
			method: 'get'
			success: (res) =>
				if res.data?
					@setData(res.data)
				@trigger 'data:ready'
			error: (err) =>
				@trigger 'data:ready'

	setData: (data) ->
		@initialData =
			'bill_to_name': data.bill_to_name
			'bill_to_address': data.bill_to_address

		@set @initialData

	resetData: ->
		@set @initialData

	save: ->
		$.ajax
			url: getApiRoute('ajax-billing-details', { profile_id: window.GLOBALS._PID })
			method: 'put'
			data:
				bill_to_name: @get('bill_to_name')
				bill_to_address: @get('bill_to_address')
				_token: window.GLOBALS._TOKEN

module.exports = Model

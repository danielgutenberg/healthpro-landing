Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'

require 'backbone.stickit'

class View extends Backbone.View

	attributes:
		'class': 'popup billing_info change_plan'
		'data-popup': 'billing_info'
		'id' : 'popup_billing'

	events:
		'click [data-method-edit]': 'editPayment'
		'click [data-choose-cancel]': 'cancelChange'
		'click [data-choose-upgrade]': 'submit'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@addStickit()
		@addValidation()

		@cacheDom()

	addListeners: ->
		@listenTo @model, 'data:ready', @render
		@listenTo @model, 'change:card_id', @toggleUpgradeBtn

	addStickit: ->
		@bindings =
			'[data-plan-total]':
				observe: 'product_id'
				updateMethod: 'html'
				onGet: (val) ->
					if @model.is_product_valid
						product = @model.getProduct(val)

						switch product.billing_cycles
							when 1 then cycle = 'month'
							when 12 then cycle = 'year'

						"<b>$#{product.price.integer}</b>.#{product.price.decimal} <span> / #{cycle}</span>"

			'[name="product_id"]':
				observe: 'product_id'

	render: ->
		@$el.html Billing.Templates.PopupChangePlan @getTemplateData()

		@stickit()
		@cacheDom()
		@toggleUpgradeBtn()
		@hideLoading()
		@

	hideLoading: -> @$el.$loading.addClass('m-hide')
	showLoading: -> @$el.$loading.removeClass('m-hide')

	cacheDom: ->
		@$el.upgradeBtn = $('[data-choose-upgrade]', @$el)
		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	editPayment: ->
		@baseView.billingPayment?.editPayment(null, 'change_plan')

	getTemplateData: ->
		selected_plan = @model.get('product_id')

		data = {
			changing_cycle: @model.get 'changing_cycle'
			plan: @model.getPlan(selected_plan)

			card: if @model.get('card_id')?
				type: @model.get('type_formatted')
				holder: @model.get('card_holder')
				number: @model.get('card_number_last')
		}

		data

	cancelChange: ->
		@baseView.removePopup('choose_upgrade')

	toggleUpgradeBtn: ->
		if @model.get('card_id')?
			@$el.upgradeBtn.removeAttr('disabled')
		else
			@$el.upgradeBtn.prop('disabled', true)

	submit: ->
		@showLoading()
		$.when( @model.save() )
		.then(
			(res) =>
				@hideLoading()
				vexDialog.buttons.YES.text = 'Next'
				vexDialog.alert
					message: Billing.Config.messages.paymentSuccess
					contentClassName: 'vex-content-custom vex-content-centered m-success'
					overlayClassName: 'vex-overlay-light'
				@baseView.removePopup()

			, (err) =>
				@hideLoading()
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: Billing.Config.messages.updateError
					contentClassName: 'vex-content-custom vex-content-centered m-error'
					overlayClassName: 'vex-overlay-light'
		)

module.exports = View

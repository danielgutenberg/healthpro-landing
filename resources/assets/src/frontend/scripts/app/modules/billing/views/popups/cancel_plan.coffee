Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'
Moment = require 'moment'

require 'backbone.stickit'

class View extends Backbone.View

	attributes:
		'class': 'popup billing_info cancel_plan'
		'data-popup': 'billing_info'
		'id' : 'popup_billing'

	events:
		'click [name="cancel_plan"]': 'cancelPlan'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@addStickit()
		@addValidation()

		@cacheDom()

	addListeners: ->
		@listenTo @model, 'data:ready', @render

	addStickit: ->
		@bindings =
			'[data-cycle-ends]':
				observe: 'plan.end_date'
				onGet: (val) ->
					return Moment(val).format('MMMM Do, YYYY')

	render: ->
		@$el.html Billing.Templates.PopupCancelPlan
		@stickit()
		@cacheDom()
		@hideLoading()
		@

	hideLoading: -> @$el.$loading.addClass('m-hide')
	showLoading: -> @$el.$loading.removeClass('m-hide')

	cacheDom: ->
		@$el.conainer = $('[data-cancel-container]', @$el)
		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	cancelPlan: ->
		@showLoading()
		$.when( @model.cancelPlan() )
		.then(
			(res) =>
				@hideLoading()
				@baseView.removePopup()
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: Billing.Config.messages.cancelSuccess
					contentClassName: 'vex-content-custom vex-content-centered m-success'
					overlayClassName: 'vex-overlay-light'

			, (err) =>
				@hideLoading()
				@baseView.removePopup()
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: Billing.Config.messages.updateError
					contentClassName: 'vex-content-custom vex-content-centered m-error'
					overlayClassName: 'vex-overlay-light'

		)

module.exports = View

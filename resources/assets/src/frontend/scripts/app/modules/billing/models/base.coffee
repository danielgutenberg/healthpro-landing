Backbone = require 'backbone'
DeepModel = require 'backbone.deepmodel'
getApiRoute = require 'hp.api'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class Model extends DeepModel

	defaults:
		is_subscribed: true
		changing_cycle: false

	initialize: (options) ->
		super
		Cocktail.mixin @, Mixins.Models.Validation

		@addListeners()
		@getData()

	addListeners: ->
		@listenTo @, 'change:product_id', () =>
			@tryAltCycle() if @isProductValid()

	isProductValid: (id) ->
		id = @get('product_id') unless id
		@is_product_valid = _.some  @get('available_products'), (membership) =>
			_.some membership.products, (product) =>
				product.id is parseInt(id)

	tryAltCycle: ->
		# detecting changing current cycle
		plan = @getPlan()
		product = @getProduct()

		# check for pending product first
		if @get('plan.next_product')?
			changingCycleFromPending = _.findWhere(plan.products, { id: @get('plan.next_product.id') })
			@set 'changing_cycle', changingCycleFromPending?
		else
			@set('changing_cycle', plan.action is 'SWITCH')

		# switch product_id to the alternative cycle id
		unless product.allowed
			alt_plan = _.findWhere plan.products, { allowed: true }
			@set 'product_id', alt_plan.id

	getData: ->
		$.ajax
			url: getApiRoute('ajax-provider-membership-details', { providerId: window.GLOBALS._PID })
			method: 'get'
			show_alerts: false
			success: (res) =>
				if res?
					@setData(res)
				@trigger 'data:ready'
			error: (err) =>
				@set 'is_subscribed', false
				@trigger 'data:ready'

	setData: (data) ->
		# TODO: update only required fields if it's not the first request
		@set 'plan.id', data.plan.product.id
		@set 'plan.price', data.plan.product.name
		@set 'plan.end_date', data.plan.end_date.date
		if data.plan.next_product?
			@set 'plan.next_product.name', data.plan.next_product.membership.name
			@set 'plan.next_product.price', data.plan.next_product.name
			@set 'plan.next_product.id', data.plan.next_product.id
		@set 'plan.expiration_action', data.plan.expiration_action
		@set 'bookings', data.bookings
		@set 'membership.name', data.plan.product.membership.name
		@set 'next_billing_estimate.amount', data.next_billing_estimate.amount
		@set 'next_billing_estimate.date', data.next_billing_estimate.date.date
		@set 'next_billing_estimate.items', [] # reset to be cler before update
		estimate_items = []
		_.forEach data.next_billing_estimate.items, (item) =>
			formattedItem =
				subtotal: item.subtotal
			switch item.type
				when 'provider_membership'
					formattedItem.name = @get('membership.name') + 'Plan'
				when 'membership_upgrade'
					formattedItem.name = 'Upgrade'
					formattedItem.description = item.description
				when 'billing_cycle_fees'
					formattedItem.name = item.description
					formattedItem.qty = item.qty
					formattedItem.overbooking = true
				else
					formattedItem.name = item.description

			estimate_items.push formattedItem

		@set 'next_billing_estimate.items', estimate_items
		@set 'cancellation', data.pending_action is 'cancellation'

		@setProducts(data.available_product_changes)

	setProducts: (data) ->
		available_products = {}
		_.each data, (plan) =>
			planFormatted =
				action: plan.action
				membership:
					bookings_limit: plan.membership.bookings_limit
					description: plan.membership.description.replace(/(?:\r\n|\r|\n)/g, '<br>')
					name: plan.membership.name
				products: {}

			# dirty, but templates needs this
			if plan.action is 'DOWNGRADE' then planFormatted.is_downgrade = true
			if plan.membership.slug is 'part_timer'
				planFormatted.membership.is_part_timer = true
				planFormatted.membership.color = 'green'

			if plan.membership.slug is 'healthpro'
				planFormatted.membership.is_healthpro = true
				planFormatted.membership.color = 'red'

			if plan.membership.slug is 'healthpro_plus'
				planFormatted.membership.is_healthpro_plus = true
				planFormatted.membership.color = 'blue'

			_.each plan.products, (product_option) =>
				switch product_option.billing_cycles
					when 1 then cycle = 'monthly'
					when 12 then cycle = 'annual'

				price_parts = product_option.price.toString().split('.')

				planFormatted.products[cycle] =
					id: product_option.id
					allowed: product_option.allowed
					is_current: product_option.id is @get 'plan.id'
					billing_cycles: product_option.billing_cycles
					price:
						amount: product_option.price
						integer: price_parts[0]
						decimal: if price_parts[1] then price_parts[1] else '00'

			available_products[plan.membership.slug] = planFormatted

		@set 'available_products', available_products

	getPlan: (id) ->
		unless id? then id = @get('product_id')
		id = parseInt(id)
		_.find @get('available_products'), (plan) =>
			_.findWhere plan.products, { id: id }

	getProduct: (id) ->
		unless id? then id = @get('product_id')
		id = parseInt(id)
		product = null
		_.find @get('available_products'), (plan) =>
			product = _.findWhere plan.products, { id: id }
		return product

	save: ->
		$.ajax
			url: getApiRoute('ajax-provider-membership-change', { providerId: window.GLOBALS._PID })
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				productId: @get 'product_id'
				_token: window.GLOBALS._TOKEN
			success: (res) =>
				if res?
					@setData(res)
					@trigger 'data:ready'
			error: (err) =>
				console.log err

	cancelPlan: ->
		$.ajax
			url: getApiRoute('ajax-provider-membership-cancel', { providerId: window.GLOBALS._PID })
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: (res) =>
				@set 'cancellation', true
				@getData() # would be better to to update data from response
			error: (err) =>
				console.log err

	revivePlan: ->
		$.ajax
			url: getApiRoute('ajax-provider-membership-revive', { providerId: window.GLOBALS._PID })
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: (res) =>
				@set 'cancellation', false
				@getData() # would be better to to update data from response
				# @trigger 'data:ready'
			error: (err) =>
				console.log err


module.exports = Model

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Tooltip = require 'framework/tooltip'

require 'backbone.stickit'

class View extends Backbone.View

	attributes:
		'class': 'popup billing_info choose_upgrade'
		'data-popup': 'billing_info'
		'id' : 'popup_billing'

	events:
		'click [data-pricing-btn]': 'changePlan'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@addStickit()
		@addValidation()

		@cacheDom()

	addListeners: ->
		@listenTo @model, 'data:ready', @render

	addStickit: ->

	render: ->
		@$el.html Billing.Templates.PopupChooseUpgrade @getTemplateData()
		@stickit()
		@cacheDom()
		@initTooltips()
		@hideLoading()
		@

	getTemplateData: ->
		data =
			is_subscribed: @model.get 'is_subscribed'
			plans: @model.get 'available_products'
		data

	hideLoading: -> @$el.$loading.addClass('m-hide')
	showLoading: -> @$el.$loading.removeClass('m-hide')

	initTooltips: ->
		_.each @$el.$tooltip, (tooltip) ->
			new Tooltip($(tooltip))

	cacheDom: ->
		@$el.$tooltip = $('[data-tooltip]', @$el)
		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	changePlan: (e) ->
		@model.set 'product_id', $(e.currentTarget).data('product-id')
		@baseView.appendPopup new Billing.Views.ChangePlanPopup
			eventBus: @eventBus
			model: @model
			parentView: @
			baseView: @baseView

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'
Formats = require 'config/formats'
Numeral = require 'numeral'
Moment = require 'moment'

require 'backbone.stickit'

class View extends Backbone.View

	el: $('[data-billing-plan]')

	events:
		'click [data-choose-upgrade]': 'chooseUpgrade'
		'click [data-cancel-plan]': 'cancelPlan'
		'click [data-reactivate-plan]': 'revivePlan'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@addStickit()
		@addValidation()

		@cacheDom()

	addListeners: ->
		@listenTo @model, 'data:ready', @render

	addStickit: ->

	render: ->
		@$el.html Billing.Templates.BillingPlan @getTemplateData()
		@stickit()
		@cacheDom()
		@hideLoading()

	hideLoading: -> @$el.$loading.addClass('m-hide')
	showLoading: -> @$el.$loading.removeClass('m-hide')

	cacheDom: ->
		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	getTemplateData: ->
		estimate_upgrade = null
		estimate_price = null
		if @model.get('next_billing_estimate.upgrade.amount')?
			estimate_upgrade = Numeral(@model.get('next_billing_estimate.upgrade.amount')).format(Formats.Price.Simple)
		if @model.get('next_billing_estimate.plan.amount')?
			estimate_price = Numeral(@model.get('next_billing_estimate.plan.amount')).format(Formats.Price.Simple)

		_.each @model.get('next_billing_estimate.items'), (item) =>
			item.subtotal = Numeral(item.subtotal).format(Formats.Price.Simple)

		data =
			is_subscribed: @model.get 'is_subscribed'
			plan:
				name: @model.get 'membership.name'
				price: @model.get 'plan.price'
				end_date: Moment(@model.get('plan.end_date')).format('MMMM Do, YYYY')
			next_product:
				name: @model.get 'plan.next_product.name'
				price: @model.get 'plan.next_product.price'
				is_renewal: @model.get('plan.expiration_action') is 'renewal'
			bookings:
				used: @model.get 'bookings.used'
				limit: @model.get 'bookings.limit'
				price: Numeral(@model.get('bookings.price')).format(Formats.Price.Simple)
			estimate:
				visible: @model.get('next_billing_estimate.amount') isnt 0
				total: Numeral(@model.get('next_billing_estimate.amount')).format(Formats.Price.Simple)
				date: Moment(@model.get('next_billing_estimate.date')).format('MMMM Do, YYYY')
				items: @model.get('next_billing_estimate.items')
			overbooking:
				number: @model.get 'bookings.over'
			cancellation: @model.get 'cancellation'

		data

	chooseUpgrade: ->
		@baseView.appendPopup new Billing.Views.ChooseUpgradePopup
			eventBus: @eventBus
			model: @model
			parentView: @
			baseView: @baseView

	cancelPlan: ->
		@baseView.appendPopup new Billing.Views.CancelPlanPopup
			eventBus: @eventBus
			model: @model
			parentView: @
			baseView: @baseView

	revivePlan: ->
		@showLoading()
		$.when( @model.revivePlan() )
		.then(
			(res) =>
				@hideLoading()
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: Billing.Config.messages.reviveSuccess
					contentClassName: 'vex-content-custom vex-content-centered m-success'
					overlayClassName: 'vex-overlay-light'

			, (err) =>
				@hideLoading()
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: Billing.Config.messages.updateError
					contentClassName: 'vex-content-custom vex-content-centered m-error'
					overlayClassName: 'vex-overlay-light'

		)

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Capitalize = require 'utils/capitalize'

require 'backbone.stickit'

class View extends Backbone.View

	el: $('[data-billing-payment]')

	events:
		'click [data-method-edit]': 'editPayment'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@addStickit()
		@addValidation()

		@cacheDom()

	addListeners: ->
		@listenTo @model, 'data:ready', @render

	addStickit: ->
		# @bindings =

	render: ->
		@$el.html Billing.Templates.BillingPayment
			id: @baseModel.get('card_id')
			type_formatted: @baseModel.get('type_formatted')
			card_holder: @baseModel.get('card_holder')
			card_number_last: @baseModel.get('card_number_last')
			email: @model.get('email')

		@stickit(@baseModel)
		@cacheDom()
		@hideLoading()

	hideLoading: -> @$el.$loading.addClass('m-hide')
	showLoading: -> @$el.$loading.removeClass('m-hide')

	cacheDom: ->
		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	editPayment: (e, return_to) ->
		@baseView.appendPopup new Billing.Views.EditPaymentPopup
			eventBus: @eventBus
			model: @model
			parentView: @
			baseView: @baseView
			return_to: if return_to? then return_to else ''

module.exports = View

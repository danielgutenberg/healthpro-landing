Backbone = require 'backbone'
Validation = require 'backbone.validation'
getApiRoute = require 'hp.api'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Capitalize = require 'utils/capitalize'

class Model extends Backbone.Model

	defaults:
		card_holder: ''
		card_number: ''
		exp_month: ''
		exp_year: ''
		card_cvv: ''
		payment_type: 'card'

	validation:
		card_holder:
			required: ->
				@get('payment_type') == 'card'
			msg: 'Please enter a name'

		card_number: (val, field, model) ->
			if @get('payment_type') == 'card'
				return 'Please enter a credit card number' unless val
				return 'Min length is 12' if val.length < 12

		card_exp: (val, field, model) ->
			if @get('payment_type') == 'card'
				return 'Please enter expiration date' unless val

		card_cvv: [
			{
				required: ->
					@get('payment_type') == 'card'
				msg: 'Please enter a cvv'
			}
			{
				pattern: 'digits'
				msg: 'Only digits'
			}
			{
				maxLength: 4
				msg: 'Max length is 4'
			}
			{
				minLength: 3
				msg: 'Min length is 3'
			}
		]

	initialize: (options) ->
		super
		Cocktail.mixin @, Mixins.Models.Validation
		@initialData = {}
		@baseModel = options.baseModel
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-credit-cards')
			method: 'get'
			success: (res) =>
				if res.data?
					@setData(res.data)
				@trigger 'data:ready'
			error: (err) =>
				console.log err
				@trigger 'data:ready'

	setData: (data) ->
		# data[0] - supposed that only one card is available, using the first one
		if data[0]? then data = data[0]
		card_exp = ''
		if data.exp_month < 10 then card_exp = '0'
		card_exp += data.exp_month + ' / ' + data.exp_year

		if data?
			@initialData = {
				'id': data.id if data.id?
				'funding': data.funding
				'type': data.object
				'type_formatted':
					switch data.object
						when 'card'
							Capitalize(data.funding) + ' ' + data.object
						else
							Capitalize(data.object)
				'card_holder': ''
				'card_number': ''
				'card_number_last': data.card_number
				'exp_month': data.exp_month
				'exp_year': data.exp_year
				'card_exp': ''
				'email': data.email if data.email?
			}
		else
			@initialData = {
				'id': false
				'funding': ''
				'type': ''
				'type_formatted': ''
				'card_holder': ''
				'card_number_last': ''
				'card_exp': ''
				'email': ''
			}

		@set @initialData
		@baseModel.set
			'card_id': if @get('id')? then @get('id')
			'type_formatted': @get('type_formatted')
			'card_holder': data.card_holder
			'card_number_last': @get('card_number_last')

		@trigger 'data:ready'

	resetData: ->
		@set @initialData

	save: ->
		route = getApiRoute('ajax-credit-cards')
		method = 'post'
		$.ajax
			url: route
			method: method
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				name: @get('card_holder')
				number: @get('card_number').replace(/\s+/g, '')
				expiryMonth: @get('exp_month')
				expiryYear: @get('exp_year')
				cvv: @get('card_cvv')
				type: 'credit'
				replace_card_id: @get('id')
				_token: window.GLOBALS._TOKEN
			success: (res) =>
				if res.data?
					@setData(res.data)

module.exports = Model

Backbone = require 'backbone'
Handlebars = require 'handlebars'

require '../../../../styles/modules/billing.styl'

# Views
BaseView = require './views/base'
BillingPlanView = require './views/billing_plan'
BillingPaymentView = require './views/billing_payment'
BillingInfoView = require './views/billing_info'

ChooseUpgradePopupView = require './views/popups/choose_upgrade'
ChangePlanPopupView = require './views/popups/change_plan'
EditPaymentPopupView = require './views/popups/edit_payment'
CancelPlanPopupView = require './views/popups/cancel_plan'

# Models
BaseModel = require './models/base'
BillingInfoModel = require './models/billing_info'
BillingPaymentModel = require './models/billing_payment'

# Templates
BaseTmpl = require 'text!./templates/base.html'
BillingPlanTmpl = require 'text!./templates/billing_plan.html'
BillingPaymentTmpl = require 'text!./templates/billing_payment.html'
BillingInfoTmpl = require 'text!./templates/billing_info.html'

PopupChooseUpgradeTmpl = require 'text!./templates/popups/choose_upgrade.html'
PopupChangePlanTmpl = require 'text!./templates/popups/change_plan.html'
PopupEditPaymentTmpl = require 'text!./templates/popups/edit_payment.html'
PopupCancelPlanTmpl = require 'text!./templates/popups/cancel_plan.html'

# list of all instances
window.Billing =
	Config:
		messages:
			hasChanges: "All changes will be lost. Continue?"
			updateError: 'Something went wrong. Please reload the page and try again'
			paymentSuccess: '''
				<h2>Payment Successful</h2>
				<p>Your payment has been processed. Thank you!</p>
			'''
			updateSuccess: '''
				<h2>Success</h2>
				<p>Your changes have been saved.</p>
			'''
			cancelSuccess: '''
				<h2>Plan Cancelled</h2>
				<p>Your subscription to HealthPRO will be canceled at the end of this billing cycle.</p>
			'''
			reviveSuccess: '''
				<h2>Plan Reactivated</h2>
				<p>Your subscription to HealthPRO was successfully reactivated.</p>
			'''
			cardSuccess: '''
				<h2>Success</h2>
				<p>Your new card details has been successfully updated.</p>
			'''

	Views:
		Base: BaseView
		BillingPlan: BillingPlanView
		BillingPayment: BillingPaymentView
		BillingInfo: BillingInfoView
		ChooseUpgradePopup: ChooseUpgradePopupView
		ChangePlanPopup: ChangePlanPopupView
		EditPaymentPopup: EditPaymentPopupView
		CancelPlanPopup: CancelPlanPopupView
	Models:
		Base: BaseModel
		BillingInfo: BillingInfoModel
		BillingPayment: BillingPaymentModel
	Templates:
		Base: Handlebars.compile BaseTmpl
		BillingPlan: Handlebars.compile BillingPlanTmpl
		BillingPayment: Handlebars.compile BillingPaymentTmpl
		BillingInfo: Handlebars.compile BillingInfoTmpl
		PopupChooseUpgrade: Handlebars.compile PopupChooseUpgradeTmpl
		PopupChangePlan: Handlebars.compile PopupChangePlanTmpl
		PopupEditPayment: Handlebars.compile PopupEditPaymentTmpl
		PopupCancelPlan: Handlebars.compile PopupCancelPlanTmpl

# events bus
_.extend Billing, Backbone.Events

class Billing.App
	constructor: ->
		@eventBus = _.extend {}, Backbone.Events

		@baseModel = new Billing.Models.Base
		@models =
			payment: new Billing.Models.BillingPayment
				baseModel: @baseModel

			info: new Billing.Models.BillingInfo
				baseModel: @baseModel

		@view = new Billing.Views.Base
			baseModel: @baseModel
			models: @models
			eventBus: @eventBus

Billing.app = new Billing.App()

module.exports = Billing

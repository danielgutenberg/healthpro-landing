Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
parseHash = require 'utils/parse_hash'
vexDialog = require 'vexDialog'

require 'backbone.stickit'

class View extends Backbone.View

	el: $('[data-billing]')

	events:
		'click [data-billing-submit]': 'submitBillingInfo'
		'click [data-billing-cancel]': 'revertBillingInfo'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@popupView = null
		@hash = parseHash true

		@addListeners()
		@render()

	addListeners: ->
		@listenTo @baseModel, 'data:ready', () =>
			@hideLoading()
			@maybeChooseUpgrade()
			@maybeChangePlan()

	render: ->
		@$el.html Billing.Templates.Base
		@cacheDom()
		@initParts()

	hideLoading: -> @$el.$loading.addClass('m-hide')
	showLoading: -> @$el.$loading.removeClass('m-hide')

	cacheDom: ->
		@$billingPlan = $('[data-billing-plan]', @$el)
		@$BillingPayment = $('[data-billing-payment]', @$el)
		@$billingInfo = $('[data-billing-info]', @$el)
		@$el.$loading = $('<span class="loading_overlay m-light m-hide"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	initParts: ->
		@initBillingPlan()
		@initBillingPayment()
		@initBillingInfo()

	initBillingPlan: ->
		@billingPlan = new Billing.Views.BillingPlan
			baseView: @
			model: @baseModel
			eventBus: @eventBus
			el: @$billingPlan

	initBillingPayment: ->
		@billingPayment = new Billing.Views.BillingPayment
			baseView: @
			model: @models.payment
			baseModel: @baseModel
			eventBus: @eventBus
			el: @$BillingPayment

	initBillingInfo: ->
		@billingInfo = new Billing.Views.BillingInfo
			baseView: @
			model: @models.info
			eventBus: @eventBus
			el: @$billingInfo

	submitBillingInfo: -> if @billingInfo? then @billingInfo.save()
	revertBillingInfo: -> if @billingInfo? then @billingInfo.revert()

	removePopup: (return_to) ->
		if @popupView
			window.popupsManager.closePopup 'billing_info'
			@popupView.remove()
			@popupView = null
			window.popupsManager.popups = []

		if return_to is 'change_plan'
			@appendPopup new Billing.Views.ChangePlanPopup
				eventBus: @eventBus
				model: @baseModel
				baseView: @

		if return_to is 'choose_upgrade'
			if @billingPlan?
				@billingPlan.chooseUpgrade()

	appendPopup: (view) ->
		@removePopup()
		@popupView = view
		$('body').append @popupView.render().$el
		window.popupsManager.addPopup @popupView.$el
		window.popupsManager.openPopup 'billing_info'

	maybeChangePlan: ->
		if @hash.hasOwnProperty('change_plan') and @hash.change_plan?
			if @baseModel.isProductValid(@hash.change_plan)
				@baseModel.set 'product_id', @hash.change_plan
				@appendPopup new Billing.Views.ChangePlanPopup
					eventBus: @eventBus
					model: @baseModel
					parentView: @
					baseView: @
			else
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: 'Invalid Product ID'

	maybeChooseUpgrade: ->
		if @hash.hasOwnProperty('choose_upgrade')
			if @billingPlan?
				@billingPlan.chooseUpgrade()

module.exports = View

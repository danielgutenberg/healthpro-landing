Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'

require 'backbone.stickit'
require 'payment'

class View extends Backbone.View

	attributes:
		'class': 'popup billing_info edit_payment'
		'data-popup': 'billing_info'
		'id' : 'popup_billing'
		'data-options' : '{"closeOnEsc": false}'

	events:
		'click [data-payment-cancel]': 'revert'
		'click [data-payment-save]': 'save'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@cardNumberChanged = false

		@addListeners()
		@addStickit()
		@addValidation()

		@cacheDom()

	addListeners: ->
		@listenTo @model, 'data:ready', @render
		@listenTo @model, 'change:type', @switchMethod

	addStickit: ->
		@bindings =
			'[name="payment_type"]': 'type'
			'[name="card_holder"]':
				observe: 'card_holder'
				onGet: () -> return

			'[name="card_number"]':
				observe: 'card_number'
				onGet: () -> return

				initialize: ($el) ->
					$el.on 'focus', (e) =>
						if not @cardNumberChanged
							$el.val('')
							@cardNumberChanged = true

			'[name="card_exp"]':
				observe: 'card_exp'
				onGet: () -> return

				onSet: (val) ->
					if val?.length
						exp_arr = val.split(' / ')
						@model.set('exp_month', exp_arr[0])
						@model.set('exp_year', exp_arr[1])

			'[name="card_cvv"]':
				observe: 'card_cvv'
				onGet: () -> return

	render: ->
		@$el.html Billing.Templates.PopupEditPayment
			card_id: @model.get('id')
		@stickit()
		@cacheDom()
		@initCardCheck()
		@switchMethod()
		@hideLoading()
		@

	hideLoading: -> @$el.$loading.addClass('m-hide')
	showLoading: -> @$el.$loading.removeClass('m-hide')

	cacheDom: ->
		@$el.$cardName = @$el.find('[name="card_holder"]')
		@$el.$cardNumber = @$el.find('[name="card_number"]')
		@$el.$cardCvc = @$el.find('[name="card_cvv"]')
		@$el.$cardExp = @$el.find('[name="card_exp"]')

		@$el.$paymentBlock = @$el.find('[data-payment]')
		@$el.$paymentBlock.paypal = @$el.find('[data-payment="paypal"]')
		@$el.$paymentBlock.card = @$el.find('[data-payment="card"]')

		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading


	initCardCheck: ->
		@$el.$cardNumber.payment('formatCardNumber')
		@$el.$cardCvc.payment('formatCardCVC')
		@$el.$cardExp.payment('formatCardExpiry')

		# don't allow numbers in card holders name
		@$el.$cardName.on 'keypress', (e) ->
			return false if e.charCode > 47 and e.charCode < 58

	revert: ->
		@baseView.removePopup(@return_to)
		@model.resetData()

	switchMethod: ->
		@$el.$paymentBlock.removeClass('m-active')
		@$el.$paymentBlock[@model.get('type')].addClass('m-active')

	save: (e) ->
		e.preventDefault() if e?
		errors = @model.validate()
		if !errors
			@showLoading()
			$.when( @model.save() )
			.then(
				(response) =>
					switch response.message
						when 'success'
							if response.data.paypal_link?
								window.location.href = response.data.paypal_link.replace('https://', 'http://')
								return
							else
								vexDialog.buttons.YES.text = 'Ok'
								vexDialog.alert
									message: Billing.Config.messages.cardSuccess
									contentClassName: 'vex-content-custom vex-content-centered m-success'
									overlayClassName: 'vex-overlay-light'

							@baseView.removePopup(@return_to)
							@hideLoading()

						when 'error' then @setErrors(response.errors.messages)

				, (err) =>
					@hideLoading()
					if err?
						@setErrors err.responseJSON.errors
			)

	setErrors: (errors) ->
		popupErrors = []
		_.forIn errors, (value, key) =>
			if key is 'cvv' then key = 'card_cvv'
			if key is 'number' then key = 'card_number'
			if key is 'error'
				_.each value.messages, (msg) =>
					popupErrors.push(msg)
			$field = $('input[name='+key+']', @$el)
			@showFieldError($field, value.messages[0])

		@showErrorPopup(popupErrors) if popupErrors.length

	unsetErrors: ->
		@$el.find('.field--error').html('')
		@$el.find('.field.m-error, .form--group.m-error').removeClass('m-error')

	showFieldError: ($field, errors) ->
		$field
			.parent().addClass('m-error').end()
			.parents('.form--group').addClass('m-error')

		if errors?
			$errorContainer = $('<span class="field--error test" />').appendTo( $field.parent() )
			errors = [errors] unless $.isArray(errors)
			errors.forEach (error) =>
				html = $errorContainer.html()
				html += error + '<br>' + html
				$errorContainer.html( html )

			@hideLoading()

	showErrorPopup: (msg) ->
		vexDialog.buttons.YES.text = 'Ok'
		vexDialog.alert
			message: if msg then msg else Billing.Config.messages.updateError
			contentClassName: 'vex-content-custom vex-content-centered m-error'
			overlayClassName: 'vex-overlay-light'

module.exports = View

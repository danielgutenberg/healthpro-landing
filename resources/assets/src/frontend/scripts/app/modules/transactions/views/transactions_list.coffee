Backbone = require 'backbone'
Stickit = require 'backbone.stickit'

class View extends Backbone.View

	subViews: []

	initialize: (@options) ->
		super

		@baseView = @options.baseView
		@collections = @options.collections
		@transactions = []

		@bind()
		@render()
		@cacheDom()
		@

	bind: ->
		@listenTo @collections.transactions, 'add', @renderTransaction
		@listenTo @collections.transactions, 'update', @toggleEmptyMessage
		@listenTo @collections.transactions, 'data:ready', @toggleEmptyMessage
		@listenTo @collections.transactions, 'reset', @reset

	cacheDom: ->
		@$el.$listing = $('[data-listing]', @$el)
		@$el.$header = $('[data-header]', @$el)
		@$el.$empty = $('<div class="transactions--empty">' + Transactions.Config.Messages.empty + '</div>')

	render: ->
		@$el.html Transactions.Templates.TransactionsList
			type_provider: @baseView.profileType == 'provider'

	renderTransaction: (model) ->
		@transactions.push new Transactions.Views.Transaction
			parentView: @
			baseView: @baseView
			model: model
			collections: @collections

	toggleEmptyMessage: ->
		if @collections.transactions.length
			@$el.$empty.remove()
		else
			@$el.append @$el.$empty

	reset: ->
		@removeTransactions()
		@collections.transactions.reset() if @collections.transactions.length
		if @$el.$listing? and @$el.$listing.length
			@$el.$listing.html('')

	removeTransactions: ->
		if @transactions.length
			_.forEach @transactions, (transaction) =>
				transaction.remove()
		@transactions = []

module.exports = View

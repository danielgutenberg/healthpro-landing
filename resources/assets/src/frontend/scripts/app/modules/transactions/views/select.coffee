Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
ToggleEl = require 'utils/toggle_el'

Datepicker = require 'framework/datepicker'
Moment = require 'moment'

require 'backbone.stickit'

class View extends Backbone.View

	className: 'transactions--preset_select'

	events:
		'click [data-select-preset]' : 'selectPreset'
		'click [data-select-label]'  : 'labelClick'
		'click [data-select-cancel]' : 'closeDropdown'
		'click [data-select-view]'   : 'viewDates'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		# setting first preset from collection as default option. it triggers first collection load in @baseModel
		preset = Transactions.Config.Presets[@baseModel.get('type')]()[0]
		@currentPreset = preset.value
		@baseModel.set 'preset_dates', @currentPreset

		@bind()
		@render()
		@cacheDom()
		@addListeners()
		@initDatepicker()

		@updateLabel(preset.label)
		@$el.$preset.first().addClass 'm-selected'


	bind: ->
		@bindings =
			'[name="date_from"]' :
				observe		: 'date_from'
				onGet		: (val) -> if val then Moment(val, "MM/DD/YYYY").format(Transactions.Config.datepickerFormat) else ''
				onSet		: (val) -> Moment(val, "MM/DD/YYYY")
				afterUpdate	: (el)  -> el.trigger 'change'
			'[name="date_to"]' :
				observe		: 'date_to'
				onGet		: (val) -> if val then Moment(val, "MM/DD/YYYY").format(Transactions.Config.datepickerFormat) else ''
				onSet		: (val) -> Moment(val, "MM/DD/YYYY")
				afterUpdate	: (el)  -> el.trigger 'change'

	addListeners: ->
		$(window).on 'click', (e) =>
			$target = $(e.target)
			@toggleDropdown(false) if !($target.closest(@$el.$dropdown).length or $target.closest(@$el.$label).length)

	cacheDom: ->
		@$el.$dropdown = @$el.find('[data-select-dropdown]')
		@$el.$datepicker = @$el.find('[data-datepicker]')
		@$el.$date_from = @$el.find('[data-datepicker="date_from"]')
		@$el.$date_to = @$el.find('[data-datepicker="date_to"]')
		@$el.$preset = @$el.find('[data-select-preset]')
		@$el.$label = @$el.find('[data-select-label]')
		@$el.$dates = @$el.find('[data-select-dates]')
		@

	initDatepicker: ->
		@datepickerFrom = new Datepicker @$el.$date_from,
			dateFormat: Transactions.Config.datepickerPluginFormatInner
			selectOtherMonths: true
		@datepickerTo = new Datepicker @$el.$date_to,
			dateFormat: Transactions.Config.datepickerPluginFormatInner
			selectOtherMonths: true
		@

	selectPreset: (e) ->
		$el = $(e.currentTarget)
		@$el.$preset.removeClass('m-selected')
		$el.addClass('m-selected')
		@currentPreset = $el.data('select-preset')
		if @currentPreset is 'custom'
			@toggleDates true
			@toggleDropdown true
		else
			@updateLabel $el.text()
			@toggleDates false
			@toggleDropdown false
			@baseModel.set 'preset_dates', @currentPreset
			@baseModel.updateDates()

	render: ->
		@$el.html Transactions.Templates.Select
			presets: Transactions.Config.Presets[@baseModel.get('type')]
		@stickit @baseModel
		@

	viewDates: ->
		@baseModel.set 'preset_dates', @currentPreset
		if @baseModel.validateDates()
			@updateLabel 'Date Range'
			@baseModel.updateDates()
			@toggleDropdown false
			@$el.$datepicker.find('.field').removeClass 'm-error'
		else
			@$el.$datepicker.find('.field').addClass 'm-error'
		@

	toggleDates: (display = null) ->
		ToggleEl @$el.$dates, display

	toggleDropdown: (display = null) ->
		ToggleEl @$el.$dropdown, display

	updateLabel: (label = null) ->
		@currentLabel = label unless label is null
		@$el.$label.text @currentLabel
		@

	labelClick: -> @toggleDropdown()

	closeDropdown: -> @toggleDropdown false

module.exports = View

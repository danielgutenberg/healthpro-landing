Backbone = require 'backbone'
Moment = require 'moment'

class Model extends Backbone.Model

	FILTER_THIS_MONTH_CAPTURED: 'this_month_captured'
	FILTER_20_TRANSACTIONS: '20_transactions'
	FILTER_THIS_MONTH_AUTHORIZED: 'this_month_authorized'
	FILTER_NEXT_30_DAYS: 'next_30_days'
	FILTER_LAST_30_DAYS: 'last_30_days'

	defaults:
		type: ''
		date_from: ''
		date_to: ''
		preset_dates: ''

	initialize: (options) ->
		super
		@collections = options.collections
		@bind()

	bind: ->
		@listenTo @, 'change:preset_dates', @setPresetDates

	fetchCollection: ->
		if @validateDates()
			@collections.transactions.getData(@)

	updateDates: ->
		# if user sets date range from datepickers
		@unset('search_by') # it prevents searching by record numbers
		@fetchCollection()

	setPresetDates: ->
		if @get('preset_dates')
			@set('search_by', null)
			switch @get('preset_dates')
				when @FILTER_THIS_MONTH_CAPTURED
					@set('date_from', Moment().startOf('month'))
					@set('date_to', Moment())
				when @FILTER_20_TRANSACTIONS
					@set('record_from', '0')
					@set('record_to', '20')
					@set('search_by', 'numbers')
				when @FILTER_THIS_MONTH_AUTHORIZED
					@set('date_from', Moment())
					@set('date_to', Moment().endOf('month'))
				when @FILTER_NEXT_30_DAYS
					@set('date_from', Moment())
					@set('date_to', Moment().add(30, 'days'))
				when @FILTER_LAST_30_DAYS
					@set('date_from', Moment().subtract(30, 'days'))
					@set('date_to', Moment())

		@fetchCollection()

	validateDates: ->
		if Moment(@get('date_from')) > Moment(@get('date_to'))
			return false
		true

module.exports = Model

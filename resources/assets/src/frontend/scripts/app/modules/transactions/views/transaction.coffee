Backbone = require 'backbone'
vexDialog = require 'vexDialog'
Numeral = require 'numeral'
Formats = require 'config/formats'

class View extends Backbone.View

	tagName: 'div'
	className: 'transaction'

	events:
		'click .transaction--main': 'toggleDetails'

	initialize: (options) ->
		@model = options.model
		@collections = options.collections
		@baseView = options.baseView
		@parentView = options.parentView
		@typeProvider = @baseView.profileType is 'provider'

		@render()

	render: ->
		if @model.get('discount_service_compensation') > 0
			@credit = Numeral(@model.get('discount_service_compensation')).format(Formats.Price.Simple)

		@$el.html Transactions.Templates.Transaction
			type_provider: @typeProvider
			date: @model.get('date').format(Transactions.Config.dateFormat)
			name: @model.get('name')
			service: @model.getService()
			amount: Numeral(@model.get('amount')).format(Formats.Price.Simple)
			gateway_fee: Numeral(@model.get('gateway_fee')).format(Formats.Price.Simple)
			fee: Numeral(@model.get('healthpro_fee')).format(Formats.Price.Simple)
			paid_out: Numeral(@model.get('paid_out') + @model.get('discount_service_compensation')).format(Formats.Price.Simple)
			transaction_token: @model.get('transaction_token')
			provider_amount: Numeral(@model.calculateServiceCost()).format(Formats.Price.Simple)
			hp_credit: @credit

		@parentView.$el.$listing.append @$el

	toggleDetails: (e) ->
		return unless @typeProvider
		$(e.currentTarget).parents('.transaction').toggleClass 'm-opened'

module.exports = View

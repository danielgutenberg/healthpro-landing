Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Moment = require 'moment'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		super
		@baseModel = options.baseModel
		@bind()

	getData: (baseModel) ->
		@reset() # clear collection before each search

		# we check how user initiated the search. switching between date range and records numbers methods
		data =
			'begin':
				if baseModel.get('search_by')? then baseModel.get('record_from')
				else Moment(baseModel.get('date_from')).format(Transactions.Config.dateTimeFormat)
			'end':
				if baseModel.get('search_by')? then baseModel.get('record_to')
				else Moment(baseModel.get('date_to')).format(Transactions.Config.dateTimeFormat)
			'type': baseModel.get('type')

		$.ajax
			url: getApiRoute('ajax-transactions')
			method: 'get'
			data: data
			success: (res) =>
				if res.data?
					_.forEach res.data.transactions, (transaction) => @add @format(transaction)
				@trigger 'data:ready'
			error: (err) =>
				@trigger 'data:ready'

	format: (transaction) ->
		date: Moment(transaction.date)
		name: transaction.name
		product: transaction.product
		amount: transaction.amount
		paid_out: transaction.payout
		gateway_fee: transaction.gateway_fee
		healthpro_fee: transaction.healthpro_fee
		transaction_token: transaction.transaction_token
		discount_service_compensation: transaction.discount_service_compensation

module.exports = Collection

Backbone = require 'backbone'
HumanTime = require 'utils/human_time'

class Model extends Backbone.Model

	defaults:
		date: ''
		name: ''
		product: null
		amount: ''
		gateway_fee: ''
		paid_out: ''
		healthro_fee: ''
		payout: ''

	initialize: ->
		super

	getService: ->
		product = @get('product')
		return '' unless product?.type

		duration = HumanTime.minutes product.duration,
			hourLabel: 'hour'
			hoursLabel: 'hours'
			minuteLabel: 'minute'
			minutesLabel: 'minutes'

		switch product.type
			when 'package'
				if product.number_of_entities is -1
					"Monthly Package of #{duration} #{product.service_name} sessions"
				else
					"#{product.number_of_entities} #{product.service_name} sessions of #{duration}"
			when 'appointment'
				"Single #{duration} #{product.service_name} session"
			when 'gift_certificate'
				switch window.GLOBALS._PTYPE
					when 'client'
						"<strong>Gift Certificate / For: #{product.client_name}</strong> - Single #{duration} #{product.service_name} session"
					else
						"<strong>Gift Certificate / From: #{product.sender_name}</strong> - Single #{duration} #{product.service_name} session"
			else
				''

	calculateServiceCost: ->
		totalFee = @get('healthpro_fee') + @get('gateway_fee')
		amountPaidByClient = @get('amount')
		if totalFee > amountPaidByClient
			return totalFee

		amountPaidByClient


module.exports = Model

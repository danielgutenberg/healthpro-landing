Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'backbone.stickit'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@render()
		@cacheDom()
		@initSelect()

	initSelect: ->
		@selectView = new Transactions.Views.Select
			parentView: @
			baseView: @baseView
			baseModel: @baseModel
			collections: @collections

		@$el.$select.append @selectView.$el
		@

	cacheDom: ->
		@$el.$select = @$el.find('[data-header-select]')
		@

	render: ->
		@$el.html Transactions.Templates.Header()

module.exports = View

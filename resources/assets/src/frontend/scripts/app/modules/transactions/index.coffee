Backbone = require 'backbone'
Handlebars = require 'handlebars'

require '../../../../styles/modules/transactions.styl'

# list of all instances
window.Transactions =
	Config:
		datepickerPluginFormatInner: "mm/dd/yy" # used for datepicker plugin
		datepickerFormat: "MM/DD/YYYY" # used for datepicker formatting by momentjs
		dateTimeFormat: 'YYYY-MM-DD HH:mm:ss' # used for sending to backend
		dateFormat: 'DD MMM YYYY' # used for display
		Presets:
			captured: ->
				return [
					{ value: '20_transactions', label: 'Last 20 transactions' }
					{ value: 'this_month_captured', label: 'This month' }
					{ value: 'last_30_days', label: 'Last 30 days' }
				]
			authorized: ->
				return [
					{ value: '20_transactions', label: 'Next 20 transactions' }
					{ value: 'this_month_authorized', label: 'This month' }
					{ value: 'next_30_days', label: 'Next 30 days' }
				]
		Messages:
			error: 'Something went wrong. Please reload the page and try again.'
			datesError: 'The end date of the date range should not be before the start date. Please reselect your date range'
			empty: "There are no transactions for the selected date range"
	Views:
		Base: require('./views/base')
		Header: require('./views/header')
		TransactionsList: require('./views/transactions_list')
		Transaction: require('./views/transaction')
		Select: require('./views/select')

	Models:
		Base: require('./models/base')
		Transaction: require('./models/transaction')
	Collections:
		Transactions: require('./collections/transactions')
	Templates:
		Header: Handlebars.compile require('text!./templates/header.html')
		Select: Handlebars.compile require('text!./templates/select.html')
		TransactionsList: Handlebars.compile require('text!./templates/transactions_list.html')
		Transaction: Handlebars.compile require('text!./templates/transaction.html')

# events bus
_.extend Transactions, Backbone.Events

class Transactions.App
	constructor: ->
		@collections =
			transactions: new Transactions.Collections.Transactions [],
				model: Transactions.Models.Transaction
				baseModel: @baseModel

		@baseModel = new Transactions.Models.Base
			collections: @collections

		@view = new Transactions.Views.Base
			profileType: window.GLOBALS._PTYPE
			baseModel: @baseModel
			collections: @collections

Transactions.app = new Transactions.App()

module.exports = Transactions

Backbone = require 'backbone'
Print = require 'framework/print'
Moment = require 'moment'

class View extends Backbone.View

	el: $('[data-transactions]')

	events:
		'click .transactions--actions--print': -> @printTransactions()

	instances:
		header: null
		transactionsList: null
		popup: null

	initialize: (options) ->
		@collections = options.collections
		@profileType = options.profileType
		@baseModel = options.baseModel
		@transactionsType = @$el.data('transactions')

		@setTransactionType()
		@bind()
		@cacheDom()
		@render()

	bind: ->
		@listenTo @collections.transactions, 'data:ready', ->
			if(@baseModel.FILTER_20_TRANSACTIONS == @baseModel.attributes.preset_dates)

        		# make default 30 days before in case that was no transactions at all.
				if @collections.transactions and @collections.transactions.length > 0
					dateFrom = @collections.transactions.last().get('date').format(Transactions.Config.datepickerFormat)
				else
					dateFrom = Moment().subtract(30, 'days')

				@baseModel.set
					date_from : dateFrom
					date_to   : Moment()

			@hideLoading()
		@listenTo @collections.transactions, 'reset', @showLoading

	render: ->
		@instances.transactionsList = new Transactions.Views.TransactionsList
			collections: @collections
			baseView: @
		@$el.$app.append @instances.transactionsList.el

		@instances.header = new Transactions.Views.Header
			collections: @collections
			baseView: @
			el: @$el.$header
			baseModel: @baseModel

	setTransactionType: ->
		@baseModel.set('type', @transactionsType) # setting transaction type from the template to model

	hideLoading: -> @$el.$loading.addClass('m-hide')
	showLoading: -> @$el.$loading.removeClass('m-hide')

	cacheDom: ->
		@$el.$app = @$el.find('.transactions--app')
		@$el.$header = @$el.find('.transactions--actions')
		@$printPopup = $('[data-popup="print_transaction"]')

		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	printTransactions: ->
		unless @print
			@print = new Print @$printPopup, @transactionsType
		@print.openPopup()

module.exports = View

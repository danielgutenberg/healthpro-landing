Backbone = require 'backbone'
vexDialog = require 'vexDialog'

Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

BackboneTimer = require 'utils/backbone_timer'
vexDialog = require 'vexDialog'

require 'backbone.stickit'
require 'backbone.validation'

# init perfect scrollbar
require('perfect-scrollbar')($)
require 'perfect-scrollbar/dist/css/perfect-scrollbar.min.css'

class View extends Backbone.View

	updateTimer: null

	events:
		'click .header--menu_toggler': 'openSettingsDropdown'
		'click .link--remove': 'removeConversation'
		'click .form--send_btn': 'postMessage'
		'keyup textarea': 'validateMessage'
		'keydown textarea': 'postMessageWithReturn'
		'click .header--add_btn': 'addToMyProfiles'

	initialize: (options) ->
		super
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@render()
		@cacheDom()
		@initScroll()
		@loadConversation()

	initValidation: ->
		Backbone.Validation.bind @,
			valid: (view, attr) ->
				field = view.$el.find('[name="' + attr + '"]')
				field.parents('.field').removeClass 'm-error'
			invalid: (view, attr, error) ->
				field = view.$el.find('[name="' + attr + '"]')
				field.parents('.field').addClass 'm-error'
				field.prev('.field--error').html(error)

	cacheDom: ->
		@$el.$loading = @$el.find('.loading_overlay')
		@$el.$wrap = @$el.find('.conversation--wrap')
		@$el.$body = @$el.find('.conversation--body')
		@$el.$messages = @$el.find('.conversation--messages')
		@$el.$button = @$el.find('.form--send_btn')

		@$el.$dropdown = @$el.find('.header--menu_dropdown')
		@$el.$dropdownToggler = @$el.find('.header--menu_toggler')

		@$el.$textarea = @$el.find('.form--message textarea')

		@initSettingsDropdown()

		@

	showWrap: -> @$el.$wrap.removeClass('m-hide')
	hideWrap: -> @$el.$wrap.addClass('m-hide')

	render: ->
		@$el.html Conversations.Templates.Conversation
			isProvider: Conversations.Config.IsProvider
		@

	loadConversation: ->
		if @model
			@afterLoad()
			return

		@beforeLoad()

		@model = new Conversations.Models.SingleConversation
			id: @conversationId

		$.when(@model.load()).then(
			=>
				@afterLoad()
			=>
				console.log 'error', arguments
		)

	beforeLoad: ->
		@showLoading()
		@hideWrap()

	afterLoad: ->
		@eventBus.trigger 'loaded'

		@appendMessages()
		@hideLoading()
		@showWrap()
		@bind()
		@stickit()

		@startConversationIntervalUpdater()

	afterUpdate: ->
		@appendMessages()

	bind: ->
		@bindings =
			'.header--profile_name':
				observe: ['participant', 'invite']
				updateMethod: 'html'
				onGet: (values) ->
					if values[0] and values[0].full_name
						if values[0].type is "provider"
							return "<a href='#{values[0].public_url}' target='_blank'>#{values[0].full_name}<i class='tooltip'>Book now</i></a>"
						else if values[0].type is "client"
							if (values[1].status == 'approved')
								return "<a href='#{values[0].public_url}' target='_blank'>#{values[0].full_name}<i class='tooltip'>Client notes</i></a>"
					values[0]?.full_name

			'.header--userpic':
				observe: 'participant'
				updateMethod: 'html'
				onGet: (val) -> '<img src="' + val.avatar + '">' if val and val.avatar

			'.header--add':
				classes:
					'm-hide':
						observe: 'invite'
						onGet: (val) =>
							return true unless Conversations.Config.IsProvider
							!val.can_invite

			'.header--add_btn':
				observe: 'participant'
				onGet: (val) ->
					return '' unless val?.type
					if val.type is 'client' then 'Add to My Clients' else 'Add to My Professionals'


	appendMessages: ->
		_.each @model.get('messages').models, (message) =>
			# the message is already there
			return if _.find @subViews, (subView) -> subView.model.get('id') is message.id
			@appendMessage(message)

		# update scrollbar
		@$el.$messages.scrollTop @$el.$messages.prop("scrollHeight")
		@$el.$messages.perfectScrollbar('update')


	clearMessages: ->
		@removeSubViews []

	appendMessage: (message) ->
		@subViews.push message = new Conversations.Views.Message
			model: message
			currentProfileId: @model.get('current_profile_id')

		@$el.$messages.append message.render().el

	validateMessage: ->
		@$el.$button.prop('disabled', true)
		@inputText = @$el.$textarea.val()

		if @inputText != '' and @inputText.replace(/\s/g, '').length != 0
			@$el.$button.prop('disabled', false)
		!@$el.$button.prop('disabled')

	postMessage: ->
		@showLoading()
		# stop interval updater when posting a message
		@stopConversationIntervalUpdater()
		$.when(@model.postMessage @$el.$textarea.val()).then(
			(response) =>
				@hideLoading()
				message = @model.get('messages').addMessage response.data
				@$el.$textarea.val('')
				@appendMessage message
				# update message and date in sidebar
				@conversations.trigger 'conversation:updated', @model.get('id'), message
				# start updater after posting a message
				@startConversationIntervalUpdater()

			=> @hideLoading()
		)

	initSettingsDropdown: ->
		$(window).on 'click', (e) =>
			$target = $(e.target)
			@closeSettingsDropdown() if !($target.closest( @$el.$dropdown ).length or $target.closest( @$el.$dropdownToggler ).length)

	openSettingsDropdown: -> @$el.$dropdown.addClass('m-show')
	closeSettingsDropdown: -> @$el.$dropdown.removeClass('m-show')

	removeConversation: ->
		vexDialog.confirm
			message: 'Are you sure you would like to delete this conversation?'
			callback: (value) =>
				return unless value
				@conversations.trigger 'data:loading'
				@showLoading()
				$.when(@model.removeConversation()).then(
					=>
						@hideLoading()
						@conversations.trigger 'data:ready'
						@conversations.trigger 'conversation:remove', @model.get('id')
						@eventBus.trigger 'conversation:new'
					=>
						@hideLoading()
						@conversations.trigger 'data:ready'
				)

	startConversationIntervalUpdater: ->
		if !@updateTimer
			@updateTimer = BackboneTimer.get @model,
				delay: 10000 # every 10 seconds
				fetchMethod: 'update'
			@updateTimer.on 'success', => @afterUpdate()

		@updateTimer.start()

	stopConversationIntervalUpdater: ->
		@updateTimer.stop() if @updateTimer

	postMessageWithReturn: (e) ->
		@postMessage() if (e.metaKey || e.ctrlKey) and e.keyCode == 13 and @validateMessage()

	initScroll: ->
		@$el.$messages.perfectScrollbar
			suppressScrollX: true
			includePadding: true
			minScrollbarLength: 20

	addToMyProfiles: ->
		return unless Conversations.Config.IsProvider

		invite = @model.get('invite')
		return unless invite.can_invite

		@showLoading()
		@conversations.trigger 'data:loading'

		$.when(@model.inviteClient()).then(
			=>
				@hideLoading()
				@conversations.trigger 'data:ready'
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert "You successfully invited client"
			(err) =>
				@hideLoading()
				@conversations.trigger 'data:ready'
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert 'Something went wrong. Please reload the page and try again.'
		)

module.exports = View

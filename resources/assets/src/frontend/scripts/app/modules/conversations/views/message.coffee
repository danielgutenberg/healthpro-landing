Backbone = require 'backbone'
Stickit = require 'backbone.stickit'
Moment = require 'moment'

class View extends Backbone.View

	className: 'item--message'

	initialize: (@options) ->
		@model = @options.model
		@currentProfileId = parseInt(@options.currentProfileId, 10)

	render: ->
		@$el.html Conversations.Templates.Message
		@bind()
		@stickit()
		@

	bind: ->
		@bindings =
			'.item--message_content':
				observe: 'content'
				updateMethod: 'html'
				escape: true
				onGet: (val) ->
					val = val.replace /[\r\n]{2,}/g, '\n'
					val = val.replace /\n/g, '<br><br>'
					val
				classes:
					'm-me':
						observe: 'sender_id'
						onGet: (val) => @currentProfileId == val
			'.item--message_date':
				'observe': 'date'
				'onGet': (val) -> Moment(val * 1000).fromNow()

module.exports = View

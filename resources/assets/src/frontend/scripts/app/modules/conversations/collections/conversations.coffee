Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: (models, @options)->
		super

		@eventBus = @options.eventBus

		@getData()
		@bind()

	bind: ->
		@listenTo @, 'conversation:added', @addConversation
		@listenTo @, 'conversation:updated', @updateConversation
		@listenTo @eventBus, 'conversation:load', @activateConversation
		@listenTo @eventBus, 'conversation:new', @deactivateConversations

	getData: ->
		@trigger 'data:loading'
		$.ajax
			url: getApiRoute('ajax-conversations')
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.forEach response.data, (item) =>
					@add item
				@trigger 'data:ready'

	addConversation: (item) ->
		@push @formatNewConversation(item), merge: true
		@eventBus.trigger 'conversation:load', item.id

	search: (pattern) ->
		a = _.filter @.models, (model) ->
			model.set('filtered', false)
			regex = new RegExp pattern , 'i'
			unless regex.test model.get('participant.full_name')
				model.set('filtered', true)

	activateConversation: (conversationId) ->
		model = @get(conversationId)
		return unless model
		_.each @models, (model) ->
			model.set 'is_active', model.get('id') == conversationId
		model.markAsRead()

	deactivateConversations: ->
		_.each @models, (model) -> model.set 'is_active', false

	formatNewConversation: (data) ->
		conversationData =
			current_profile_id: data.current_profile_id
			id: data.id
			profiles: data.profiles
			last_message: data.messages[0]
		conversationData

	updateConversation: (conversationId, conversation) ->
		model = @get(conversationId)
		model.set 'last_message', conversation.attributes if model
		model.format()
		@trigger 'up', conversationId


	findByProfileId: (profileId) ->
		_.find @models, (model) ->
			_.findWhere model.get('profiles'), {id: profileId}


module.exports = Collection

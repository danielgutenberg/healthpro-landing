Backbone = require 'backbone'

class View extends Backbone.View

	el: $('[data-conversations]')

	initialize: (@options) ->
		super

		@eventBus = @options.eventBus
		@conversations = @options.conversations
#		@clients = @options.clients
		@currentConversationView = null

		@parseHash()
		@cacheDom()
		@render()
		@bind()

	cacheDom: ->
		@$el.$sidebar = @$el.find('[data-conversations-sidebar]')
		@$el.$conversation = @$el.find('[data-conversations-conversation]')

	render: ->
		new Conversations.Views.Sidebar
			el: @$el.$sidebar
			conversations: @conversations
			eventBus: @eventBus
		@newConversation()
		@beforeLoad()

	bind: ->
		@listenTo @eventBus, 'conversation:load', @loadConversation
		@listenTo @eventBus, 'conversation:new', @newConversation
		@listenTo @eventBus, 'loading', @loading
		@listenTo @eventBus, 'loaded', @loaded
		@listenTo @conversations, 'data:ready', @afterLoad

	loadConversation: (conversationId) ->
		@resetConversation()
		@currentConversationView = new Conversations.Views.Conversation
			eventBus: @eventBus
			baseView: @
			conversationId: conversationId
			conversations: @conversations
#			clients: @clients
		@$el.$conversation.html @currentConversationView.el

	newConversation:  ->
		return if @currentConversationView and @currentConversationView.constructor.name is 'NewConversationView'
		@resetConversation()
		@currentConversationView = new Conversations.Views.NewConversation
			eventBus: @eventBus
			baseView: @
			model: new Conversations.Models.NewConversation
			conversations: @conversations

		@$el.$conversation.html @currentConversationView.el

	resetConversation: ->
		if @currentConversationView
			@currentConversationView.close()

	loading: ->
		@eventBus.isLoading = true

	loaded: ->
		@eventBus.isLoading = false


	parseHash: ->
		@hash = _.object _.compact _.map window.location.hash.replace(/^#/, '').split('&'), (item) -> if item then item.split ':'
		@hash.to = parseInt @hash.to, 10 if @hash.to
		window.location.hash = ''

	beforeLoad: ->
		@currentConversationView.showLoading() if @hash.to

	afterLoad: ->
		return unless @hash.to
		# we have an old conversaation. let's load it
		if conversation = @conversations.findByProfileId @hash.to
			@eventBus.trigger 'conversation:load', conversation.get('id')
			return

		# we don't have any conversations with the profile yet. let's load profile data and start one
		@currentConversationView.startConversation @hash.to


module.exports = View



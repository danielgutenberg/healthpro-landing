Backbone = require 'backbone'
Stickit = require 'backbone.stickit'
Moment = require 'moment'

class View extends Backbone.View

	events:
		'click .list--item_wrap': 'loadConversation'

	initialize: (@options) ->
		@model = @options.model
		@eventBus = @options.eventBus
		@conversations = @options.conversations

		@bind()
		@addStickit()

	render: ->
		@$el.html Conversations.Templates.SidebarConversation
		@cacheDom()
		@stickit()
		@openFirtUnread()
		@

	cacheDom: ->
		@$el.$item = @$el.find('.list--item')
		@unread = $('.m-message i')

	bind: ->
		@listenTo @conversations, 'conversation:remove', @removeConversation

	addStickit: ->
		@readChanged = false
		@bindings =
			'.list--item_msg_content': 'last_message.content'
			'.list--item_more_date':
				observe: 'last_message.date'
				onGet: (val) ->
					date = Moment(val * 1000)
					if Moment().diff(date, 'days') then date.format("Do MMM") else 'today'

			'.list--item_msg_name': 'participant.full_name'
			'.list--item_userpic':
				observe: 'participant'
				updateMethod: 'html'
				onGet: (val) => '<img src="' + val.avatar + '">' if val and val.avatar
			'.list--item_status':
				classes:
					'm-unread':
						observe: 'read'
						onGet: (val) ->
							unless @readChanged == false
								if @unread? and (num = parseInt @unread.first().text()) > 1
									@unread.text(num - 1)
								else
									@unread.remove()
							@readChanged = true
							!val
			'.list--item':
				observe: 'id'
				update: ($el, val) -> $el.attr 'data-id', val
				classes:
					'm-current':
						observe: 'is_active'
					'm-hide': 'filtered'

	loadConversation: (e) ->
		return if $(e.target).is('a')
		return if @$el.$item.hasClass('m-current') or @eventBus.isLoading
		@eventBus.trigger 'loading'
		@eventBus.trigger 'conversation:load', @model.get('id')

	removeConversation: (conversationId) ->
		@conversations.remove conversationId
		if @model.get('id') == conversationId then @remove() # otherwise all the views will be removed. TODO: redo this

	openFirtUnread: ->
		if @$el.$item? and !@model.get('read')
			@$el.$item.find('.list--item_wrap').trigger 'click'

module.exports = View

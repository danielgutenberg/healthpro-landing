Backbone = require 'backbone'

class View extends Backbone.View

	sidebarConversations: []

	events:
		'click .search--new_btn': 'newConversation'
		'keyup .search--input': 'filter'

	initialize: (@options) ->
		super
		@conversations = @options.conversations
		@eventBus = @options.eventBus

		@render()
		@cacheDom()
		@bind()

	bind: ->
		@listenTo @conversations, 'add', @addSidebarConversation
		@listenTo @conversations, 'data:ready', @hideLoading
		@listenTo @conversations, 'data:loading', @showLoading
		@listenTo @conversations, 'up', @up

	addSidebarConversation: (model) ->
		@sidebarConversations.push sidebarConversation = new Conversations.Views.SidebarConversation
			model: model
			eventBus: @eventBus
			conversations: @conversations
		@$el.$list.append sidebarConversation.render().el
		@

	cacheDom: ->
		@$el.$list = @$el.find('.sidebar--list')
		@$el.$new = @$el.find('.search--new_btn')
		@$el.$search = @$el.find('.search--input input')
		@$el.$loading = @$el.find('.loading_overlay')
		@

	showLoading: ->
		@$el.$loading.removeClass('m-hide')
		@

	hideLoading: ->
		@$el.$loading.addClass('m-hide')
		@

	render: ->
		@$el.html Conversations.Templates.Sidebar()

	newConversation: ->
		@$el.$list.find('.list--item.m-current').removeClass('m-current')
		@eventBus.trigger 'conversation:new'

	filter: =>
		@conversations.search(@$el.$search.val())

	up: (conversationId)  ->
		@$el.$list.find('[data-id="' + conversationId + '"]').parent().prependTo @$el.$list


module.exports = View

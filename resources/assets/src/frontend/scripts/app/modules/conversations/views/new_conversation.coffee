require 'jquery-ui/autocomplete'

Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	events:
		'click .form--send_btn': 'saveConversation'
		'keyup textarea': 'validateMessage'
		'keydown textarea': 'postMessageWithReturn'

	initialize: (@options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common )

		@model = @options.model
		@conversations = @options.conversations
		@eventBus = @options.eventBus
		@baseView = @options.baseView

		@render()
		@cacheDom()
		@bind()
		@addValidation()
		@stickit()

	validateData: ->
		@errors = []
		errors = @model.validate()
		@errors.push errors if errors?

	bind: ->
		@bindings =
			'[name="message"]' : 'message'
			'[name="to"]':
				observe: 'name'
				initialize: ($el) =>
					@$el.$autocomplete = $el.closest('[data-autocomplete]')
					$el.autocomplete(
						type: 'jsonp'
						appendTo: @$el.$autocomplete.find('[data-autocomplete-results]')
						change: (e, ui) =>
							@model.set 'to', null unless ui.item
						select: (e, ui) =>
							@model.set 'to', ui.item.value
							@model.set 'name', ui.item.label
							false
						focus: (e, ui) =>
							e.preventDefault()
							$el.val(ui.item.label) # make keyboard navigation works
							@value = ui.item.label

						source: (request, response) =>
							@showSpinner()
							$.ajax
								type: 'get'
								url: getApiRoute('ajax-core-conversations-receivers')
								data:
									query: request.term
								success: (data) =>
									@hideSpinner()
									receivers = []
									_.each data.data.receivers, (el) ->
										receivers.push
											label: el.full_name
											value: el.id
											avatar: el.avatar
									response receivers
						).autocomplete().data('ui-autocomplete')._renderItem = (ul, item) ->
							li = $('<li>')
							li.append('<a>' + item.label + '<i></i></a>')
							if item.avatar then li.find('i').append('<img src="' + item.avatar + '">')
							li.appendTo ul

	cacheDom: ->
		@$el.$loading = @$el.find('.loading_overlay')
		@$el.$spinner = @$el.find('.field .spin')
		@$el.$button = @$el.find('.form--send_btn')
		@hideLoading()
		@

	showLoading: ->
		@$el.$loading.removeClass('m-hide')
		@

	hideLoading: ->
		@$el.$loading.addClass('m-hide')
		@

	showSpinner: -> @$el.$spinner.addClass('m-show')
	hideSpinner: -> @$el.$spinner.removeClass('m-show')

	render: ->
		@$el.html Conversations.Templates.NewConversation()
		@

	validateMessage: ->
		@$el.$button.prop('disabled', true)
		@inputText = $('.form--message textarea').val()

		if @inputText != '' and @inputText.replace(/\s/g, '').length != 0
			@$el.$button.prop('disabled', false)
		!@$el.$button.prop('disabled')

	saveConversation: ->
		@validateData()

		return if @errors.length

		@showLoading()
		@eventBus.trigger 'loading'

		$.when(@model.save()).then(
			(response) =>
				@hideLoading()
				@eventBus.trigger 'loaded'
				@conversations.trigger 'conversation:added', response.data

			(error) =>
				@hideLoading()
				@eventBus.trigger 'loaded'
		)


	postMessageWithReturn: (e) ->
		@saveConversation() if (e.metaKey || e.ctrlKey) and e.keyCode == 13 and @validateMessage()


	startConversation: (profileId) ->
		# ideally we need to check if we can send a message to this profile.
		# we will create a custom endpoint in the conversations api later.
		$.ajax
			type: 'get'
			url: getApiRoute('ajax-profile', {id: profileId})
			success: (response) =>
				@model.set 'to', response.data.id
				@model.set 'name', response.data.full_name
				@hideLoading()
			error: (error) ->
				@hideLoading()

module.exports = View
#
#window.inputClickMessageTitle = (@el) ->
#	inputValue = $(el).val();
#	id = $('.ui-helper-hidden-accessible').text();
#	if (inputValue)
#		window.location = '/p/'+id
#	false

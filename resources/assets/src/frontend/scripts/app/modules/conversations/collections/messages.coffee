Backbone = require 'backbone'
class Collection extends Backbone.Collection

	addMessage: (item) ->
		@push @formatMessage(item)

	formatMessage: (item) ->
		item

module.exports = Collection

Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		participant: {}
		messages: []
		invite: {}

	loadCanInviteClient: ->
		return true unless Conversations.Config.IsProvider
		$.ajax
			url: getApiRoute('ajax-provider-can-invite', {providerId: 'me', clientId: @get('participant').id})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				@set 'invite', response.data

	loadConversation: ->
		$.ajax
			url: getApiRoute('ajax-conversation-messages', {conversationId: @get('id')})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				# set data without ID
				@set _.omit(response.data, 'id')
				@convertMessages()
				@getParticipant()

	load: ->
		$.when(
			@loadConversation()
		).then(
			=> @loadCanInviteClient()
		)

	postMessage: (content) ->
		$.ajax
			url: getApiRoute('ajax-conversation-messages', {conversationId: @get('id')})
			method: 'post'
			data:
				content: content
				_token: window.GLOBALS._TOKEN

	convertMessages: ->
		@set 'messages', new Conversations.Collections.Messages(@get('messages'))

	getParticipant: ->
		participant = null
		currentProfileId = parseInt(@get('current_profile_id'))
		_.each @get('profiles'), (p) =>
			if currentProfileId != p.id
				@set('participant', p)

	removeConversation: ->
		$.ajax
			url: getApiRoute('ajax-conversation-messages', {conversationId: @get('id')})
			method: 'delete'
			data:
				_token: window.GLOBALS._TOKEN

	update: (options) ->
		$.when(@loadConversation()).then(
			=> options.success() if _.isFunction(options.success)
			=> options.error() if _.isFunction(options.error)
		)

	canInviteClient: -> @get('invite').can_invite

	inviteClient: ->
		$.ajax
			url: getApiRoute('ajax-invite-client', {clientId:  @get('participant').id})
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			show_alerts: false
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: (response) =>
				@set 'invite',
					can_invite: false
					status: 'pending'

module.exports = Model

Backbone = require 'backbone'
getApiRoute = require 'hp.api'
require 'backbone.validation'

class Model extends Backbone.Model
	defaults:
		message: ''
		to: ''
		name: ''

	validation:
		to: (value) ->
			unless value
				return 'Please select a name from the list'
		message: (value) ->
			unless value
				return 'Please add the message'
	save: ->
		data =
			recipient_id: @get('to')
			content: @get('message')
			_token: window.GLOBALS._TOKEN
		$.ajax
			url: getApiRoute('ajax-conversations')
			method: 'post'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify data

module.exports = Model

Backbone = require 'backbone'
Handlebars = require 'handlebars'

require '../../../../styles/modules/conversations.styl'

window.Conversations =
	Config:
		IsProvider: window.GLOBALS._PTYPE is 'provider'
	Views:
		Base: require('./views/base')
		Sidebar: require('./views/sidebar')
		Conversation: require('./views/conversation')
		NewConversation: require('./views/new_conversation')
		SidebarConversation: require('./views/sidebar_conversation')
		Message: require('./views/message')
	Models:
		Conversation: require('./models/conversation')
		NewConversation: require('./models/new_conversation')
		SingleConversation: require('./models/single_conversation')
	Collections:
		Conversations: require('./collections/conversations')
		Messages: require('./collections/messages')
	Templates:
		Sidebar: Handlebars.compile require('text!./templates/sidebar.html')
		Conversation: Handlebars.compile require('text!./templates/conversation.html')
		NewConversation: Handlebars.compile require('text!./templates/new_conversation.html')
		SidebarConversation: Handlebars.compile require('text!./templates/sidebar_conversation.html')
		Message:  Handlebars.compile require('text!./templates/message.html')

_.extend Conversations, Backbone.Events

class Conversations.App
	constructor: ->
		@eventBus = _.extend {
			isLoading: false
		}, Backbone.Events

		@collections =
			conversations: new Conversations.Collections.Conversations [],
				eventBus: @eventBus
				model: Conversations.Models.Conversation

		@views =
			base: new Conversations.Views.Base
				eventBus: @eventBus
				conversations: @collections.conversations

new Conversations.App() if $('[data-conversations]').length

module.exports = Conversations

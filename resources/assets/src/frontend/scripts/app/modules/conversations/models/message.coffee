Backbone = require 'backbone'

class Model extends Backbone.Model
	defaults:
		message: ''
		to: ''

	validation:
		to: (value) ->
			unless value
				return 'Please add the to'
		message: (value) ->
			unless value
				return 'Please add the message'

module.exports = Model

getApiRoute = require 'hp.api'
DeepModel = require 'backbone.deepmodel'

class Model extends DeepModel

	defaults:
		is_active: false
		read: true
		participant: {}

	initialize: ->
		super
		@getParticipant()
		@format()
		@

	markAsRead: ->
		@set 'read', true
		$.ajax
			url: getApiRoute('ajax-conversation-messages', {
				conversationId: @get('id')
			})
			method: 'put'
			type  : 'json'
			data:
				_token: window.GLOBALS._TOKEN

	removeConversation: ->
		$.ajax
			url: getApiRoute('ajax-conversation-messages', {
				conversationId: @get('id')
			})
			method: 'delete'
			data:
				_token: window.GLOBALS._TOKEN

	getParticipant: ->
		participant = null
		currentProfileId = parseInt(@get('current_profile_id'), 10)
		_.each @get('profiles'), (p) =>
			if currentProfileId != p.id
				@set('participant', p)
		@get('participant')

	trim: (field, maxLength = 20) ->
		message = @get(field)
		if message.length > maxLength
			message = message.substr(0, maxLength - 1) + '...'
			@set(field, message)

	format: ->
		@trim('last_message.content', 20)
		@trim('participant.full_name', 15)

module.exports = Model

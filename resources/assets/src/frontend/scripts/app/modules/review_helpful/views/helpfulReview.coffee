Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class View extends Backbone.View

	events:
		'click a[data-helpful]': 'markHelpful'

	initialize: (opts) ->
		@model = opts.model
		@listenTo @model, 'change:rated', @render
		@cacheDom()
		@render()

	render: ->
		@helpful.html('')
		if @model.get('helpful') > 0
			p = if parseInt(@model.get('helpful')) == 1 then 'person' else 'people'
			@helpful.append '<p>' + @model.get('helpful') + ' ' + p + ' found this review helpful.</p>'
		if @canMarkHelpful(@model)
			@helpful.append '<p>Was this review helpful to you?</p>'
			@helpful.append '<a data-helpful="">Yes</a>'

	cacheDom: ->
		@helpful = @$('.reviews--list--item--helpful--inner')
		@$el.$loading = @$('.loading_overlay')

		return @

	canMarkHelpful: (model) ->
		reviewedId = parseInt(model.get('reviewedBy'))
		currentId = parseInt(window.currentProfile)
		model.get('rated') == '' and currentId > 0 and reviewedId != currentId and currentId != window.GLOBALS.PROVIDER_ID

	showLoading: -> @$el.$loading.removeClass('m-hide')

	hideLoading: -> @$el.$loading.addClass('m-hide')

	markHelpful: =>
		@showLoading()
		$.ajax
			url: getApiRoute('provider-review-helpful-set', {
					providerId: @model.get('provider_id') ? window.GLOBALS.PROVIDER_ID
					reviewId: @model.get('id')
				}
			)
			method: 'post'
			data:
				_token: window.GLOBALS._TOKEN
			success: () =>
				@model.set
					rated: "yes"
					helpful: parseInt(@model.get('helpful')) + parseInt(1)
				@hideLoading()


module.exports = View

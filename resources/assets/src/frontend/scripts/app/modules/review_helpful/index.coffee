Backbone = require 'backbone'
Handlebars = require 'handlebars'
getApiRoute = require 'hp.api'

# views
HelpfulReviewView = require './views/helpfulReview'

# models
ReviewsCollection = require './collections/reviews'

# models
HelpfulReviewModel = require './models/review'

window.HelpfulReview = HelpfulReview =
	View: HelpfulReviewView
	Model: HelpfulReviewModel
	Collection: ReviewsCollection

_.extend HelpfulReview, Backbone.Events

class HelpfulReview.App
	constructor: ->
		@collection = new HelpfulReview.Collection window.reviews
		@index = 0
		@collection.forEach (model) =>
			new HelpfulReview.View
				model: model
				el: $('.reviews--list--item').get(@index)
			@index++

if $('.reviews--list').length
	new HelpfulReview.App

module.exports = HelpfulReview

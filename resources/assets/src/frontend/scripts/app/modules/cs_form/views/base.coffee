Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
getRoute = require 'hp.url'
Phone = require 'framework/phone'
Maxlength = require 'framework/maxlength'
Config = require '../config/config'
vexDialog = require 'vexDialog'

# require 'caret'
# require 'phonenumber'

require 'backbone.stickit'

class View extends Backbone.View

	events:
		'click [data-form-submit]': 'submitForm'

	initialize:(options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions options
		@setFormOptions()

		@addStickit()
		@addValidation()
		@render()

	setFormOptions: ->
		@formName = @$el.data 'cs-form'
		@model.set 'form_name', @formName
		@popupName = @$el.parents('[data-popup]').data('popup') if @$el.parents('[data-popup]').length

	cacheDom: ->
		@$el.$loading = $('.loading_overlay', @$el)
		unless @$el.$loading.length
			@$el.$loading = $('.loading_overlay', @$el.parent())

	addStickit: ->
		@bindings =
			'[name="first_name"]': 'first_name'
			'[name="last_name"]': 'last_name'
			'[name="email"]': 'email'
			'[name="phone"]':
				observe: 'phone'
				initialize: ($el) ->
					new Phone $el
				onSet: (val) ->
					'1 ' + val
			'[name="website"]': 'website'
			'[name="number_of_members"]': 'number_of_members'
			'[name="company_name"]': 'company_name'
			'[name="comment"]':
				observe: 'comment'
				onGet: (val, options) ->
					$el = @$el.find(options.selector)
					$parent = $el.parent()
					unless $parent.attr('data-maxlength') is 'inited'
						$el.val val
						new Maxlength $parent
					val
			'[name="country"]': 'country'
			'[name="interested_in"]': 'interested_in'
			'[name="time_to_call"]': 'time_to_call'
			'[name="question"]': 'question'
			'[name="title"]': 'title'

	bind: ->

	render: ->
		@$el.html CsForm.Templates[Config.Options[@formName].Template]

		$(document).trigger 'popup:center'
		@cacheDom()
		@stickit()
		@bind()

		@

	closePopup: (target) ->
		window.popupsManager.closePopup target
		@

	openPopup: (target) ->
		window.popupsManager.openPopup target
		@

	submitForm: (e) ->
		e.preventDefault() if e?
		errors = @model.validate()

		unless errors
			@showLoading()
			$.when( @model.submit(@route) )
			.then(
				(response) =>
					switch response.message
						when 'success'
							@hideLoading()
							@closePopup(@popupName)
							vexDialog.alert
								buttons: [
									$.extend {}, vexDialog.buttons.YES, {text: 'Ok'}
								]
								message: 'Your request has been sent'
						when 'error'
							@hideLoading()

				, (err) =>
					@hideLoading()
			)

	showLoading: -> @$el.$loading.removeClass('m-hide')
	hideLoading: -> @$el.$loading.addClass('m-hide')

module.exports = View

Backbone = require 'backbone'
Handlebars = require 'handlebars'

window.CsForm =
	Views:
		Base: require('./views/base')
	Models:
		Base: require('./models/base')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')
		DemoRequest: Handlebars.compile require('text!./templates/demo_request.html')
		PartnerContact: Handlebars.compile require('text!./templates/partner_contact.html')
		AffiliateRequest: Handlebars.compile require('text!./templates/affiliate_request.html')
		ContactUs: Handlebars.compile require('text!./templates/contact_us.html')
		Newsletter: Handlebars.compile require('text!./templates/newsletter.html')
		SubmitQuestion: Handlebars.compile require('text!./templates/submit_question.html')

class CsForm.App
	constructor: ($el) ->
		@eventBus = _.extend {}, Backbone.Events

		@model = new CsForm.Models.Base()

		@views =
			base: new CsForm.Views.Base
				eventBus: @eventBus
				model: @model
				el: $el

_.each $('[data-cs-form]'), ($el) ->
	new CsForm.App($el)

module.exports = CsForm

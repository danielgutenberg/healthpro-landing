module.exports =
	Messages:
		error: 'Something went wrong. Please reload the page and try again.'
	Options:
		demo_request:
			Template: 'DemoRequest'
			Route: 'ajax-cs-demo-request'
			Fields: ['first_name', 'last_name', 'email', 'phone']
			Required: ['first_name', 'last_name', 'email', 'phone']
		partner_contact:
			Template: 'PartnerContact'
			Route: 'ajax-cs-partner-request'
			Required: ['first_name', 'last_name', 'email', 'website', 'phone', 'interested_in', 'comment', 'company_name', 'number_of_members', 'country']
			Fields: ['first_name', 'last_name', 'email', 'website', 'phone', 'interested_in', 'comment', 'company_name', 'number_of_members', 'country']
		affiliate_program:
			Template: 'AffiliateRequest'
			Route: 'ajax-cs-affiliate-request'
			Required: ['first_name', 'last_name', 'email', 'website', 'phone', 'comment', 'company_name', 'title']
			Fields: ['first_name', 'last_name', 'email', 'website', 'phone', 'comment', 'company_name', 'title']
		contact_us:
			Template: 'ContactUs'
			Route: 'ajax-cs-contact'
			Required: ['first_name', 'last_name', 'email', 'phone', 'time_to_call', 'comment']
			Fields: ['first_name', 'last_name', 'email', 'phone', 'time_to_call', 'comment']
		newsletter:
			Template: 'Newsletter'
			Route: 'ajax-cs-newsletter'
			Required: ['email']
			Fields: ['email']
		submit_question:
			Template: 'SubmitQuestion'
			Route: 'ajax-cs-newsletter'
			Required: ['email', 'question']
			Fields: ['email', 'question']

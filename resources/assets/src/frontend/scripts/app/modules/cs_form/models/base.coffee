Backbone = require 'backbone'
Validation = require 'backbone.validation'
getApiRoute = require 'hp.api'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
DeepModel = require 'backbone.deepmodel'
Config = require '../config/config'

class Model extends DeepModel

	defaults:
		first_name: ''
		last_name: ''
		email: ''
		phone: ''

	initialize: ->
		super
		Cocktail.mixin @, Mixins.Models.Validation
		@addListeners()

	addListeners: ->
		@listenTo @, 'change:form_name', @initValidation

	initValidation: ->
		requredFields = Config.Options[@get('form_name')].Required
		@validation =
			first_name: [
				{
					required: requredFields.indexOf('first_name') != -1
					msg: 'Please enter your first name'
				}
				{
					maxLength: 26
					msg: 'Max length is 26'
				}
				{
					pattern: /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/g
					msg: 'Only letters allowed'
				}
			]

			last_name: [
				{
					required: requredFields.indexOf('last_name') != -1
					msg: 'Please enter your last name'
				}
				{
					maxLength: 26
					msg: 'Max length is 26'
				}
				{
					pattern: /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/g
					msg: 'Only letters allowed'
				}
			]

			email:
				required: requredFields.indexOf('email') != -1
				pattern: 'email'
				msg: 'Please enter a valid email'

			phone:
				required: requredFields.indexOf('phone') != -1
				pattern: /^\s*1?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/
				msg: 'Please enter phone number with the following format <br> +1 (XXX) XXX-XXXX.'

			website:
				required: requredFields.indexOf('website') != -1
				msg: 'Please enter your website.'

			interested:
				required: requredFields.indexOf('interested') != -1
				msg: 'Please select at least one option.'

			number_of_members: [
				{
					required: requredFields.indexOf('number_of_members') != -1
					msg: 'Please select at least one option.'
				}
				{
					pattern: 'digits'
					msg: 'Please enter a valid number (digits only)'
				}
			]

			comment: [
				{
					required: requredFields.indexOf('comment') != -1
					msg: 'Please enter your comment'
				}
				{
					maxLength: 400
					msg: 'Max length is 400 symbols'
				}
			]

			question: [
				{
					required: requredFields.indexOf('question') != -1
					msg: 'Please enter your question'
				}
			]

			title: [
				{
					required: requredFields.indexOf('title') != -1
					msg: 'Please enter title'
				}
			]

	submit: ->
		$.ajax
			url: getApiRoute(Config.Options[@get('form_name')].Route)
			type: 'post'
			data: @prepareData()
			# show_alerts: false

	prepareData: ->
		data = {}
		_.each Config.Options[@get('form_name')].Required, (field) =>
			data[field]= @get(field)

		data._token = window.GLOBALS._TOKEN
		data

module.exports = Model

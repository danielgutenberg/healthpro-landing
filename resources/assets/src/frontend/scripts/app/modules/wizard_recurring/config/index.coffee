BookingConfig = require 'app/modules/wizard_booking/config'

Config = _.extend {}, BookingConfig

module.exports = Config

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

Select = require 'framework/select'

class View extends Backbone.View

	className: 'wizard_booking--service_select'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@bind()
		@render()

	bind: ->
		@bindings =
			'[name="service"]':
				observe: 'current_service_id'
				selectOptions:
					collection: @baseModel.getServicesWithReccuringPackages()
					labelPath: 'name'
					valuePath: 'service_id'
				afterUpdate: (el) -> el.change() # forcing select2 to update selected option
				initialize: (el) ->
					new Select el

		@

	render: ->
		@$el.html WizardRecurringPackageSelect.Templates.Services()
		@stickit @baseModel
		@

module.exports = View

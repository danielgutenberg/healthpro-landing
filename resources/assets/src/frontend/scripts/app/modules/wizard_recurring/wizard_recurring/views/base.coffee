Abstract = require 'app/modules/wizard/views/main_view'
RecurringInfo = require 'app/modules/wizard_recurring/wizard_recurring_sidebar'
isMobile = require 'isMobile'

Handlebars = require 'handlebars'
TemplateLayout = Handlebars.compile require('text!../templates/layout.html')
ToggleEl = require 'utils/toggle_el'

vexConfirm = require 'app/modules/wizard_booking/utils/confirm'

getRoute = require 'hp.url'
getApiRoute = require 'hp.api'

class View extends Abstract

	events :
		'click [data-wizard-close]'   : 'closeWizard'
		'click [data-wizard-proceed]' : 'proceed'
		'click [data-wizard-back]'    : 'back'

	startup : ->
		super
		@canExit = true
		@

	bind : ->
		super
		@listenTo Wizard, 'initial_data:ready', => @initRecurringInfo()
		@listenTo Wizard, 'stack:loading', => @allowExit(false)
		@listenTo Wizard, 'stack:ready', => @allowExit(true)
		@


	addStickit : ->
		@bindings =
			'[data-wizard-title]' :
				observe : 'title'
			':el'                 :
				classes :
					'm-success' : 'isSuccessStep'

			'[data-wizard-proceed]' :
				observe    : 'proceedLabel'
				classes    :
					'm-green' : 'isPaymentStep'
				attributes : [
					{
						name    : 'disabled'
						observe : 'canGoAhead'
						onGet   : (val) -> if val? and val then !val else true
					}
				]

			'[data-wizard-close]' :
				classes :
					'm-hide' : 'isSuccessStep'

			'[data-wizard-back]' :
				observe : 'backLabel'
				classes :
					'm-hide' :
						observe : ['previous_step', 'disableBack']
						onGet   : (val) ->
							return true if val[1] is true
							return if val[0]? then false else true


	renderLayout : ->
		@$el.html TemplateLayout()
		@cacheDom()
		@centerPopup()

	cacheDom : ->
		super
		@$el.$close = @$el.find('[data-wizard-close]')
		@$el.$back = @$el.find('[data-wizard-back]')
		@$el.$proceed = @$el.find('[data-wizard-proceed]')
		@$el.$footer = @$el.find('[data-wizard-footer]')
		@

	centerPopup : ->
		if @place is 'popup'
			$(document).trigger 'popup:center'

	showLoading : ->
		@$el.$loading.removeClass 'm-hide'
		@

	hideLoading : ->
		@$el.$loading.addClass 'm-hide'
		@

	initRecurringInfo : ->
		@subViews.push @recurringInfo = new RecurringInfo.App
			$container     : @$el.$sidebar
			recurringModel : @model
		@recurringInfo.view.render()
		@

	closeWizard : ->
		return unless @canExit
		if @place is 'popup'
			@closeWithCallback =>
				Wizard.loading()
				Wizard.close()
				window.popupsManager.closePopup('wizard_booking_popup')
				window.popupsManager.removePopup('wizard_booking_popup')
				if !window.GLOBALS._PID? and Wizard.model.get('profileId')?
					location.reload()
			return

		if @place is 'page'
			@closeWithCallback =>
				location.href = @model.get('additional.professional.public_url') ? getRoute('dashboard-appointments')
			return

	closeWithCallback : (callback) ->
		vexConfirm 'Exiting the wizard will cancel your booking <br>Are you sure you want to exit?', callback, 'Yes', 'Cancel'
		return

	allowExit : (allow = true) ->
		@$el.$close.prop 'disabled', !allow
		@canExit = allow
		@

	appendExitButton : -> @

	removeExitButton : -> @

	proceed : ->
		Wizard.trigger 'proceed', @model
		@

	back : ->
		Wizard.trigger 'back', @model
		@

	updateProceed : (label = 'proceed', classes = '') ->
		@$el.$proceed.html(label)
		if oldClasses = @$el.$proceed.data('classes')
			@$el.$proceed.removeClass oldClasses
		if classes
			@$el.$proceed.data('classes', classes)
			@$el.$proceed.addClass classes
		@

	updateBack : (label = 'proceed', classes = '') ->
		@$el.$back.html(label)
		if oldClasses = @$el.$back.data('classes')
			@$el.$back.removeClass oldClasses
		if classes
			@$el.$back.data('classes', classes)
			@$el.$back.addClass classes
		@
	#
	#	cancelAppointment: (appointmentId) ->
	#		appointmentId =  @model.get('data.appointment_id')
	#		if appointmentId? and window.GLOBALS._PID
	#			$.ajax
	#				url         : getApiRoute('ajax-appointment', {appointmentId: appointmentId})
	#				method      : 'delete'
	#				type        : 'json'
	#				contentType : 'application/json; charset=utf-8'
	#				data        : JSON.stringify
	#					_token : window.GLOBALS._TOKEN
	#		else
	#			true

	toggleFooter : (display = null) -> ToggleEl @$el.$footer, display

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'change input[name="rule_id"]': 'changedRule'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@render()

	addListeners: ->
		@baseModel.on 'change:current_service_id', => @render()

	cacheDom: ->
		@$el.$rules = @$el.find('input[name="rule_id"]')
		@

	changedRule: (e) =>
		ruleId = $(e.currentTarget).val() * 1
		@baseModel.set 'current_session_package_rule_id', ruleId
		return

	maybeChangeRule: ->
		currentRuleId = @baseModel.get('current_session_package_rule_id')
		serviceSessionPackageRuleId = _.pluck @baseModel.getCurrentServicePackages(), 'rule_id'

		if currentRuleId and (serviceSessionPackageRuleId.indexOf(currentRuleId) > -1)
			@$el.$rules.filter("[value='#{currentRuleId}']").trigger('change').trigger('click')
		else
			@$el.$rules.first().trigger('change').trigger('click')

	render: ->
		@$el.html WizardRecurringPackageSelect.Templates.Packages
			packages: @baseModel.getFormattedServiceReccuringPackages @baseModel.getCurrentService()

		@cacheDom()
		@maybeChangeRule()

		@

module.exports = View

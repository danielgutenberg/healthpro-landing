Handlebars = require 'handlebars'

# list of all instances
window.WizardRecurringIntroductorySession = WizardRecurringIntroductorySession =
	Config:
		IntroductorySessionSlug: 'introductory-session'
	Views:
		Base: require('./views/base')
	Models:
		Base: require('./models/base')
	Templates:
		Base:  Handlebars.compile require('text!./templates/base.html')

class WizardRecurringIntroductorySession.App
	constructor: (options = {}) ->
		if options.presetData
			presetData = options.presetData
		else if Wizard? and Wizard.inited
			presetData = Wizard.model.getPresetData()
		else
			presetData = null

		_.extend options,
			model: new WizardRecurringIntroductorySession.Models.Base {},
				presetData: presetData

		@view = new WizardRecurringIntroductorySession.Views.Base options

		{
			view: @view
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = WizardRecurringIntroductorySession

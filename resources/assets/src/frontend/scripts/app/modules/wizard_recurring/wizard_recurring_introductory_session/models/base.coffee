Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults : ->
		{
			service  : null
			session  : null
			services : []
		}

	initialize : (attrs, options) ->
		@setInitPresetData options.presetData
		@fetch()

	fetch : ->
		Wizard.loading()
		$.when(
			@fetchServices()
		).then(
			-> Wizard.ready()
		).fail(
			-> Wizard.ready()
		)

	fetchServices : ->
		@xhr = $.ajax
			url     : getApiRoute 'ajax-provider-services',
				providerId : @getProviderId()
			method  : 'get'
			success : (response) =>
				@setIntroductoryServiceAndSessionFromResponse response.data
				@trigger 'services:ready'
				@trigger 'ready'
		@

	setIntroductoryServiceAndSessionFromResponse : (data) ->
		_.each data, (serv) =>
			_.each serv.sub_services, (subServ) =>
				if subServ.slug is WizardRecurringIntroductorySession.Config.IntroductorySessionSlug
					session = serv.sessions[0]
					@set 'service',
						service_id : serv.service_id
						tag_id     : subServ.id
						slug       : subServ.slug
						name       : session.title
					@set 'session',
						session_id : session.session_id
						duration   : session.duration
						price      : session.price
						title      : session.title
					@set 'locations', serv.locations
					# used when the back button is pressed from the payment step
					# we can't return the client to introductory session step
					# and instead we pass this service as the only one to pick_time step
					@set 'services', [
						{
							service_id    : serv.service_id
							name          : session.title
							detailed_name : session.title
							sessions      : serv.sessions
							locations     : serv.locations
						}
					]
		@

	getProviderId : -> @get('provider_id') || window.GLOBALS.PROVIDER_ID

	setInitPresetData : (data) ->
		if data?
			@set
				resetToFirstStep : 'recalculate'
				provider_id      : data.provider_id
		@

module.exports = Model

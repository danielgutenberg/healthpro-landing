Backbone = require 'backbone'
getApiRoute = require 'hp.api'
collectionUtils = require 'utils/collection_utils'
Moment = require 'moment'
Numeral = require 'numeral'
Formats = require 'config/formats'
HumanTime = require 'utils/human_time'
LocationFormatter = require 'utils/formatters/location'

LocationIcon = require 'app/modules/wizard_booking/utils/location_icon'

class Model extends Backbone.Model

	defaults : ->
		starting_week                   : do => @getFirstStartingWeekday()
		auto_renew                      : 1
		resetToFirstStep                : 'recalculate'
		bookedFrom                      : null
		provider_id                     : null
		current_location_id             : null
		current_service_id              : null
		current_session_package_rule_id : null
		services                        : []
		service_confirmed               : false
		package_confirmed               : false
		time_confirmed                  : false

	validation :
		current_service_id :
			required : true
			msg      : 'Select service'

	initialize: (attrs, options) ->
		@type = options.type ? null
		# if presetData was passed to the model init it
		@setInitPresetData options.presetData

		@addListeners()
		@fetch()

	addListeners: ->
		@listenToOnce @, 'services:ready', => @setInitData()

		@listenTo @, 'change:current_service_id change:current_location_id', =>
			@set
				service_confirmed : false
				package_confirmed : false
				time_confirmed    : false

		@listenTo @, 'change:current_session_package_rule_id', =>
			@set
				package_confirmed : false
				time_confirmed    : false

	setInitPresetData: (data) ->
		if data?
			@set
				resetToFirstStep                : data.resetToFirstStep ? 'recalculate'
				bookedFrom                      : data.bookedFrom
				services                        : collectionUtils.convertCollectionToArray data.services
				provider_id                     : data.provider_id
				client_id                       : data.client_id || +window.GLOBALS._PID
				current_location_id             : data.location_id
				current_service_id              : data.service_id
				current_session_package_rule_id : data.rule_id


			if data.rule_id
				@set
					service_confirmed : true
					package_confirmed : true
					time_confirmed    : true

		@

	fetch: ->
		@fetchServices()

	# get provider services
	fetchServices: ->
		unless @shouldFetchServices()
			@trigger 'services:ready'
			return true

		@trigger 'loading'
		@xhr = $.ajax
			url: getApiRoute('ajax-provider-services', {providerId: @get('provider_id') || window.GLOBALS?.PROVIDER_ID})
			method: 'get'
			success: (response) =>
				@set 'services', @formatServices(response.data)
				@trigger 'services:ready'
				@trigger 'ready'

	formatServices: (services) ->
		formatted = []
		_.forEach services, (service) =>
			formatted.push
				service_id: service.service_id
				name: service.name
				sessions: service.sessions
				locations: service.locations

		return formatted

	# set data that was passed to module
	setInitData: ->
		@setInitServices()
		@trigger 'data:ready'

		@

	setInitServices: ->

		serviceId = @get('current_service_id')
		servicesWithPackages = @getServicesWithReccuringPackages()

		if _.pluck(servicesWithPackages, 'service_id').indexOf(serviceId) > -1
			return @

		@set 'current_service_id', _.first(servicesWithPackages).service_id

		@

	getService: (serviceId) -> _.findWhere @get('services'), service_id: serviceId

	getSession: (sessionId) ->
		sessionObject = null
		_.each @get('services'), (service) =>
			_.each service.sessions, (session) =>
				if session.session_id is sessionId
					sessionObject = session
		sessionObject

	getCurrentService: ->
		currentServiceId = @get('current_service_id')
		if currentServiceId
			return @getService currentServiceId
		null

	getCurrentLocation: ->
		currentLocationId = @get('current_location_id')
		if currentLocationId
			return _.findWhere @getCurrentService().locations, id: currentLocationId
		null

	getCurrentServicePackage: ->
		currentRuleId = @get('current_session_package_rule_id')
		if currentRuleId
			return _.findWhere @getCurrentServicePackages(), rule_id: currentRuleId
		null

	shouldFetchServices: ->
		# in wizard we have the full list of services only when we book from the profile page.
		shouldLoadServices = true
		if @type is 'wizard'
			shouldLoadServices = false if @get('bookedFrom') is 'profile'
		else
			shouldLoadServices = false if @get('services').length

		shouldLoadServices


	getServicesWithReccuringPackages: ->
		_.filter @get('services'), (service) =>
			@getServiceSessionsWithReccuringPackages(service).length > 0

	getServiceSessionsWithReccuringPackages: (service) ->
		_.filter service.sessions, (session) =>
			@getServiceSessionReccuringPackages(session).length > 0

	getServiceSessionReccuringPackages: (session) ->
		_.filter session.packages, (pkg) ->
			return false unless pkg.rules.length
			true

	getFormattedServiceReccuringPackages: (service) ->
		packages = []
		_.each service.sessions, (session) =>
			_.each session.packages, (pkg) =>
				_.each pkg.rules, (rule) =>
					return unless rule.rule is 'max_weekly'
					packages.push @formatPackage(rule, pkg, session, service)
					return

		packages

	getFormattedServiceLocations: (service) ->
		service.locations.map (location) ->
			{
				id      : location.id
				name    : LocationFormatter.getLocationName location
				address : if location.location_type is 'virtual' then null else LocationFormatter.getLocationAddressLabel(location)
				icon    : LocationIcon(location, 'small')
				type    : location.location_type
			}

	getCurrentServicePackages: ->
		@getFormattedServiceReccuringPackages @getCurrentService()

	formatPackage: (rule, pkg, session, service) ->
		price = (pkg.price).replace(',', '') * 1
		{
			rule                   : rule.rule
			max_visits             : rule.value
			max_visits_label       : do ->
				return 'unlimited sessions a week' if rule.value is 0
				"#{rule.value} session" + (if rule.value > 1 then "s" else "") + " a week"
			price                  : Numeral(price).format(Formats.Price.Simple)
			price_label            : Numeral(price).format(Formats.Price.Simple) + " / month"
			duration               : session.duration
			duration_label         : HumanTime.minutes(session.duration)
			number_of_months       : pkg.number_of_months
			number_of_months_label : "#{pkg.number_of_months} month" + (if pkg.number_of_months > 1 then "s" else "")
			service_name           : service.name
			rule_id                : rule.id
			package_id             : pkg.package_id
			session_id             : session.session_id
			service_id             : service.service_id
		}

	getBookingData: ->
		pkg = @getCurrentServicePackage()
		location = @getCurrentLocation()

		{
			resetToFirstStep : @get('resetToFirstStep')
			provider_id      : @get('provider_id')
			client_id        : @get('client_id') || +window.GLOBALS._PID
			services         : @getServicesWithReccuringPackages()
			selected_time    :
				service_id               : pkg.service_id
				service_name             : pkg.service_name
				location_id              : location.id
				location_name            : LocationFormatter.getLocationName location
				location_address         : LocationFormatter.getLocationAddressLabel location
				professional_location_id : null
				rule_id                  : pkg.rule_id
				package_id               : pkg.package_id
				session_id               : pkg.session_id
				duration                 : pkg.duration
				duration_label           : pkg.duration_label
				max_visits               : pkg.max_visits
				max_visits_label         : pkg.max_visits_label
				number_of_months         : pkg.number_of_months
				number_of_months_label   : pkg.number_of_months_label
				price                    : pkg.price
				price_label              : pkg.price_label
				auto_renew               : @get('auto_renew')
				availabilities           : @collections.times.getFormattedForWizard()
		}

	isValid: -> @get('current_location_id') and @get('current_service_id') and @get('current_session_package_rule_id')

	getFirstStartingWeekday : -> Moment().weekday(0)

	getStartingWeeks : ->
		startingWeekday = @getFirstStartingWeekday()
		weekLabels = {
			0 : 'Starting This Week',
			1 : 'Starting Next Week'
			2 : 'Starting In Two Weeks'
			3 : 'Starting In Three Weeks'
		}

		weeks = []
		_.each weekLabels, (label, k) ->
			date = startingWeekday.clone().add(k, 'week')
			weeks.push {
				label : label
				value : date.format('YYYY-MM-DD')
				date  : date
			}
		weeks

	validateAvailabilities : (availabilities) ->
		$.ajax
			url         : getApiRoute 'ajax-package-can-purchase',
				sessionId : Wizard.model.get('data.selected_time.session_id')
			method      : 'post'
			show_alerts : false
			data        :
				availabilities : availabilities
				_token         : window.GLOBALS._TOKEN



module.exports = Model

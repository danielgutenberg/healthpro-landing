Handlebars = require 'handlebars'

# list of all instances
window.WizardRecurringPackageSelect = WizardRecurringPackageSelect =
	Config      :
		DisplaySelectedWeekdays : false
	Views       :
		Base           : require('./views/base')
		Services       : require('./views/services')
		Locations      : require('./views/locations')
		Packages       : require('./views/packages')
		Availabilities : require('./views/availabilities')
		Availability   : require('./views/availability')
		Datepicker     : require('./views/datepicker')
		Time           : require('./views/time')
	Models      :
		Base         : require('./models/base')
		Availability : require('./models/availability')
		Time         : require('./models/time')
	Collections :
		Availabilities : require('./collections/availabilities')
		Times          : require('./collections/times')
	Templates   :
		Base           : Handlebars.compile require('text!./templates/base.html')
		Services       : Handlebars.compile require('text!./templates/services.html')
		Locations      : Handlebars.compile require('text!./templates/locations.html')
		Packages       : Handlebars.compile require('text!./templates/packages.html')
		Datepicker     : Handlebars.compile require('text!./templates/datepicker.html')
		Availabilities : Handlebars.compile require('text!./templates/availabilities.html')
		Time           : Handlebars.compile require('text!./templates/time.html')

class WizardRecurringPackageSelect.App
	constructor : (options) ->
		# preset data set when booking wizard is opened from search results
		if options.presetData
			presetData = options.presetData
		else if Wizard? and Wizard.inited
			presetData = Wizard.model.getPresetData()
		else
			presetData = null

		@model = new WizardRecurringPackageSelect.Models.Base {},
			type       : options.type
			presetData : presetData

		@collections =
			times          : new WizardRecurringPackageSelect.Collections.Times [],
				model : WizardRecurringPackageSelect.Models.Time
			availabilities : new WizardRecurringPackageSelect.Collections.Availabilities [],
				model     : WizardRecurringPackageSelect.Models.Availability
				baseModel : @model

		@model.collections = @collections

		@view = new WizardRecurringPackageSelect.Views.Base
			model       : @model
			collections : @collections

		return {
			view  : @view
			model : @model
			close : @close
		}

	close : ->
		@view.close?()
		@view.remove?()

module.exports = WizardRecurringPackageSelect

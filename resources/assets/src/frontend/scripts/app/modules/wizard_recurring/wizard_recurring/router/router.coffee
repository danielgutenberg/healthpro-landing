debugLog = require 'utils/debug_log'

Abstract = require 'app/modules/wizard/router/router'

PackageSelect = require 'app/modules/wizard_recurring/wizard_recurring_package_select'
Payment = require 'app/modules/wizard_booking/wizard_booking_payment'
BookingSuccess = require 'app/modules/wizard_booking/wizard_booking_success'

CreateProfile = require 'app/modules/wizard_booking/wizard_booking_create_profile'
SwitchProfile = require 'app/modules/wizard_booking/wizard_booking_switch_profile'

IntroductorySession = require 'app/modules/wizard_recurring/wizard_recurring_introductory_session'
LocationSelect = require 'app/modules/wizard_booking/wizard_booking_location_select'

isMobile = require 'isMobile'

class Router extends Abstract

	routes :
		'wizard/recurring/introductory_session'  : 'introductorySessionStep'
		'wizard/recurring/package_select'        : 'packageSelectStep'
		'wizard/recurring/profile_client_create' : 'clientCreateStep'
		'wizard/recurring/profile_client_choice' : 'clientChoiceStep'
		'wizard/recurring/payment'               : 'paymentStep'
		'wizard/recurring/location_select'       : 'locationSelectStep'
		'wizard/recurring/success'               : 'successStep'

	initialize : (options) ->
		super

		@wizardModel = options.model
		@wizardView = options.view

		@$dom =
			container : @wizardView.$el.$container
			content   : @wizardView.$el.$content
			sidebar   : @wizardView.$el.$sidebar

		# array for all inited instances
		@instances = []

		@bind()
		@start()

	updateScrollBar  : -> @
	destroyScrollBar : -> @
	initScrollBar    : -> @

	start : ->
		@openStep '' if @wizardView.place is 'page' # dirty hack to start app on page
		@openStep @wizardModel.get('current_step.slug')

	hideSidebar : ->
		@$dom.sidebar.addClass 'm-hide'

	showSidebar : ->
		@$dom.sidebar.removeClass 'm-hide'

	openStep : (step) ->
		debugLog('open step ' + step, 'wizard_booking.router')
		@navigate "wizard/recurring/#{step}", {trigger : true, replace : true}

		# temporapy, we should open page version of wizard on mobile
		if @wizardView.place is 'popup' and isMobile.any
			window.location.reload()

	introductorySessionStep : ->
		@hideSidebar()

		@instances.push introductorySession = new IntroductorySession.App()

		@$dom.content.html introductorySession.view.render().el
		Wizard.trigger 'step:changed', 'introductory_session'

	## Step: 1
	packageSelectStep : ->
		@hideSidebar()

		@instances.push packageSelect = new PackageSelect.App
			type : 'wizard'

		@$dom.content.html packageSelect.view.render().el
		Wizard.trigger 'step:changed', 'package_select'

	paymentStep : ->
		@showSidebar()

		@wizardModel.set 'canGoAhead', true
		@instances.push payment = new Payment.App
			booking_type : 'recurring'

		@$dom.content.html payment.view.render().el
		Wizard.trigger 'step:changed', 'payment'

	successStep : ->
		@hideSidebar()
		@wizardModel.set 'canGoAhead', true
		@wizardModel.set 'isSuccessStep', true
		@instances.push success = BookingSuccess.App
			booking_type : 'recurring'

		@$dom.content.html success.view.render().el
		Wizard.trigger 'step:changed', 'success'

	clientCreateStep : ->
		@hideSidebar()

		@wizardModel.set 'canGoAhead', true
		@instances.push createProfile = new CreateProfile.App
			booking_type : 'recurring'

		@$dom.content.html createProfile.view.render().el
		Wizard.trigger 'step:changed', 'client_create'

	clientChoiceStep : ->
		@hideSidebar()

		@wizardModel.set 'canGoAhead', false
		@instances.push switchProfile = new SwitchProfile.App
			booking_type : 'recurring'

		@$dom.content.html switchProfile.view.render().el
		Wizard.trigger 'step:changed', 'client_choice'

	locationSelectStep : ->
		@wizardModel.set 'canGoAhead', false
		@instances.push locationSelect = new LocationSelect.App
			type   : 'wizard'
			wizard : 'recurring'

		@$dom.content.html locationSelect.view.render().el
		Wizard.trigger 'step:changed', 'location_select'

module.exports = Router

Backbone = require 'backbone'
Moment = require 'moment'

class Collection extends Backbone.Collection

	getNextAvailableDay : (weekdays) ->
		weekdays = [0..6] unless weekdays?

		usedWeekdays = @map (model) -> model.get('from').weekday()
		availableWeekdays = _.without.apply(_, [weekdays].concat(usedWeekdays))
		return null unless availableWeekdays.length

		# get closest date to today
		currentDayIndex = _.indexOf availableWeekdays, Moment().weekday()

		if currentDayIndex > -1 and availableWeekdays.length > (currentDayIndex + 1)
			return availableWeekdays[currentDayIndex + 1]

		return availableWeekdays[0]

	comparator : (a, b) -> if a.get('from').isBefore(b.get('from')) then -1 else 1

	getFormattedForInfo : ->
		@map (model) ->
			{
				availability_id : model.get('availability_id')
				weekday         : model.get('from').format('dddd')
				date            : model.get('from').format('MMMM D, YYYY')
				time            : model.get('from').format('hh:mm a')
			}

	getFormattedForWizard : ->
		@map (model) ->
			{
				availability_id : model.get('availability_id')
				from            : model.get('from').format('YYYY-MM-DDTHH:mm:ss')
				until           : model.get('until').format('YYYY-MM-DDTHH:mm:ss')
			}

module.exports = Collection

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	className : 'wizard_booking--pick_times--list--item'

	events    :
		'click [data-time-remove]' : 'removeTime'

	initialize : (options) ->
		Cocktail.mixin @, Mixins.Common
		@setOptions options

	render : ->
		@$el.html WizardRecurringPackageSelect.Templates.Time
			weekday : @model.get('from').format('dddd')
			date    : @model.get('from').format('MMMM D, YYYY')
			time    : @model.get('from').format('hh:mm a')
		@

	removeTime : ->
		_.each @parentView.instances, (instance) =>
			return unless instance and instance.model is @model
			@parentView.instances.splice @parentView.instances.indexOf(instance), 1
			return
		@baseView.collections.times.remove @model
		@remove()
		@

module.exports = View

Backbone = require 'backbone'

class Model extends Backbone.Model

	remove: ->
		@destroy()

	destroy: ->
		@set 'id', null
		super

module.exports = Model

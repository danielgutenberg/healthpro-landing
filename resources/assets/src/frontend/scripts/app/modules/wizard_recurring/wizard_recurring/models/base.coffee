Abstract = require 'app/modules/wizard/models/base'
getApiRoute = require 'hp.api'

class Model extends Abstract

	initialize : ->
		@urls =
			initial : 'api-wizard-recurring-initial'
			step    : 'api-wizard-recurring-step'

		@bind()

		# when booking data is passed save it first and then initialize wizard.
		$.when(
		).then(
			=> @saveWizardData()
		).then(
			=> @initWizard()
		)

	# rewrite wizard's default getInitialWizardData
	getInitialWizardData : ->
		Wizard.loading()
		Wizard.trigger 'initial_data:loading'

		@xhr = $.ajax
			method  : 'get'
			url     : getApiRoute(@urls.initial)
			data    :
				resetToFirstStep : 'recalculate'
			success : (response) =>
				@setData response
				Wizard.trigger 'initial_data:ready'
				@set 'isFirstLoad', false
				Wizard.ready()
				return

	getAdditionalWizardData : ->
		# don't load additional data in case we need to do a redirect
		return if Wizard.shouldRedirectToPage()

		$.when(
			@loadProfessionalProfile()
		).then(
			-> Wizard.trigger 'additional_data:ready'
		)

	loadProfessionalProfile : ->
		$.ajax
			url     : getApiRoute('ajax-provider', {providerId : @get('data.provider_id')})
			method  : 'get'
			success : (res) =>
				@set Wizard.formatter.formatBookingProfessionalFromResponse(res)


	# rewrite wizard's default resetWizardData
	# gets called on the confirmation step to make sure
	# if the user refreshes the page no current appointment data
	# will be passed to the first step
	resetWizardData : ->
		@set
			'data.resetToFirstStep' : 'recalculate'
			'data.selected_time'    : {}
			'data.availability_id'  : null
			'data.appointment_id'   : null

		@saveWizardData()

	# rewrite wizard's default getCurrentStepData
	getCurrentStepData : ->
		Wizard.loading()
		@xhr = $.ajax
			method  : 'get'
			url     : getApiRoute(@urls.step, {step : @get('current_step.slug')})
			success : (response) =>
				@setData response
				Wizard.trigger 'booking_data:ready'
				Wizard.ready()
				return

	getPresetData : ->
		data =
			resetToFirstStep : @get('data.resetToFirstStep')
			provider_id      : @get('data.provider_id')
			client_id        : @get('data.client_id') || +window.GLOBALS._PID
			service_id       : @get('data.selected_time.service_id') ? null
			session_id       : @get('data.selected_time.session_id') ? null
			package_id       : @get('data.selected_time.package_id') ? null
			rule_id          : @get('data.selected_time.rule_id') ? null
			location_id      : @get('data.selected_time.location_id') ? null
			services         : @get('data.services')
			bookedFrom       : @get('data.bookedFrom')

		data


module.exports = Model

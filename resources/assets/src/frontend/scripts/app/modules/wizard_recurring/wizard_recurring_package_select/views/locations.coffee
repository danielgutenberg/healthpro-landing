Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'change input[name="location_id"]': 'changedLocation'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@render()

	addListeners: ->
		@baseModel.on 'change:current_service_id', => @render()

	cacheDom: ->
		@$el.$locations = @$el.find('input[name="location_id"]')
		@

	changedLocation: (e) =>
		locationId = $(e.currentTarget).val() * 1
		@baseModel.set 'current_location_id', locationId
		return

	maybeChangeLocation: ->
		currentLocationId = @baseModel.get('current_location_id')
		serviceLocationIds = _.pluck @baseModel.getCurrentService().locations, 'id'
		if currentLocationId and (serviceLocationIds.indexOf(currentLocationId) > -1)
			$elm = @$el.$locations.filter("[value='#{currentLocationId}']").trigger('change').trigger('click')
		else
			$elm = @$el.$locations.first().trigger('change').trigger('click')

	render: ->
		@$el.html WizardRecurringPackageSelect.Templates.Locations
			locations: @baseModel.getFormattedServiceLocations @baseModel.getCurrentService()

		@cacheDom()
		@maybeChangeLocation()

		@

module.exports = View

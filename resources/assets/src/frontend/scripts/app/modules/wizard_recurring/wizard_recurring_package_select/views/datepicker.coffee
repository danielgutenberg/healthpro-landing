Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
ToggleEl = require 'utils/toggle_el'

class View extends Backbone.View

	events :
		'click [data-weekday-select]' : 'activateWeekday'

	initialize : (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@instances = []

		@displayAvailabilities = 6

		@selectedAvailability = null
		@selectedDate = null

		@weekdays = @collections.availabilities.getWeekdays @baseModel.get('starting_week'), @collections.times, WizardRecurringPackageSelect.Config.DisplaySelectedWeekdays

	render : ->
		@$el.html WizardRecurringPackageSelect.Templates.Datepicker
			weekdays : @weekdays
		@cacheDom()
		@afterRender()
		@

	cacheDom : ->
		@$el.$availabilities = @$el.find('[data-availabilities-list]')
		@$el.$availabilitiesEmpty = @$el.find('[data-availabilities-empty]')
		@

	afterRender : ->
		@selectAvailableWeekday()
		@toggleNotEnoughDaysWarning()
		@

	activateWeekday : (e) ->
		$weekday = $(e.currentTarget).parent()
		return if $weekday.hasClass('m-inactive') or $weekday.hasClass('m-active')

		@setSelectedAvailability null

		$weekday.addClass('m-active').siblings('.m-active').removeClass('m-active')
		@selectedDate = $weekday.data('date')

		# do not render times that were already selected
		ignoreTimes = @collections.times.map (model) -> model.get('from')
		availabilities = @collections.availabilities.getForDate @selectedDate, ignoreTimes

		@renderAvailabilities availabilities

		@toggleDateSelectedWarning()
		@

	renderAvailabilities : (availabilities) ->
		@resetAvailabilities()
		@toggleAvailabilitiesEmpty availabilities
		availabilities.forEach (availability, k) => @renderAvailability availability, k < @displayAvailabilities
		@

	renderAvailability : (availability, display = false) ->
		availabilityView = new WizardRecurringPackageSelect.Views.Availability
			model      : availability
			parentView : @
			baseView   : @baseView
			baseModel  : @baseModel

		@instances.push availabilityView
		@$el.$availabilities.append availabilityView.render().el
		@

	resetAvailabilities : ->
		_.each @instances, (instance) -> instance.remove()
		@instances = []
		@$el.$availabilities.html ''
		@

	toggleAvailabilitiesEmpty : (availabilities) ->
		if availabilities.length
			ToggleEl @$el.$availabilities, true
			ToggleEl @$el.$availabilitiesEmpty, false
		else
			ToggleEl @$el.$availabilities, false
			ToggleEl @$el.$availabilitiesEmpty, true
		@

	selectAvailableWeekday : ->
		$nextAvailableDaysSelector = @$el.find("[data-date]").not('.m-inactive')
		$nextAvailableDayEl = null
		if @collections.times.length
			days = _.uniq @collections.times.map (model) -> model.get('from').format('YYYY-MM-DD')
			$days = $nextAvailableDaysSelector.filter -> days.indexOf($(@).data('date')) < 0
			$nextAvailableDayEl = $days.first() if $days.length

		if $nextAvailableDayEl is null
			$nextAvailableDayEl = $nextAvailableDaysSelector.first()

		$nextAvailableDayEl.find('[data-weekday-select]').trigger 'click'
		@

	setSelectedAvailability : (availability = null) ->
		@selectedAvailability = availability
		@

	selectAvailability : ->
		return unless @selectedAvailability
		@collections.times.add @selectedAvailability
		@

	toggleNotEnoughDaysWarning : ->
		# times length is added because we do not show the selected weekdays
		allWeekdaysCount = do =>
			return @weekdays.length if WizardRecurringPackageSelect.Config.DisplaySelectedWeekdays
			@weekdays.length + @collections.times.length

		if @collections.times.length and (Wizard.model.get('data.selected_time.max_visits') > allWeekdaysCount)
			@parentView.raiseGenericError "Professional has only one day available. Please try selecting a different starting week above or contact professional directly.", 'warning'
			return

	toggleDateSelectedWarning : ->
		@parentView.raiseGenericError (do =>
			selected = @collections.times.findWhere (model) => model.get('from').format('YYYY-MM-DD') is @selectedDate
			if selected
				"You already selected this day"
			else
				null
		), 'warning'
		@

module.exports = View

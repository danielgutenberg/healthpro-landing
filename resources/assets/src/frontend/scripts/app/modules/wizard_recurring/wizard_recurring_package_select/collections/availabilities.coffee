Backbone = require 'backbone'
Moment = require 'moment'

datetimeParsers = require 'utils/datetime_parsers'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize : (models, options) ->
		@fetchedDate = null
		@baseModel = options.baseModel

	fetchByDate : (fromDate, untilDate = null) ->
		@reset()
		@fetch fromDate, untilDate
		@xhr

	fetch : (fromDate = null, untilDate = null) ->
		if fromDate is null
			fromDate = Moment()

		if untilDate is null
			untilDate = fromDate.clone().add 1, 'month'

		@xhr.abort() if @xhr # abort previous call
		@trigger 'data:loading'

		@xhr = $.ajax
			url     : getApiRoute 'ajax-provider-availabilities',
				providerId : Wizard.model.get('data.provider_id')
			method  : 'get'
			data    :
				from        : "#{fromDate.format('YYYY-MM-DD')}T00:00"
				until       : "#{untilDate.format('YYYY-MM-DD')}T23:59"
				length      : Wizard.model.get('data.selected_time.duration')
				location_id : Wizard.model.get('data.selected_time.location_id')
				recurring   : true
			success : (res) =>
				@trigger 'data:ready'
				@fetchedDate = fromDate
				result = res.data?.results[0] ? null

				@setData result if result
				@trigger 'availabilities:ready'

			error : => @trigger 'data:ready'

	# set data from collection's ajax call
	setData : (result) ->
		selectedPackage = @baseModel.getCurrentServicePackage()
		_.each result.availabilities, (availabilities, date) =>
			_.each availabilities, (availability) =>
				# do not add times that don't have current service
				return unless service = availability.services[selectedPackage.service_id]
				return unless service.indexOf(selectedPackage.session_id) > -1

				fromMoment = datetimeParsers.parseToMoment availability.from

				formatted =
					availability_id : availability.id
					from            : fromMoment
					until           : datetimeParsers.parseToMoment availability.until
					date            : date
					weekday         : fromMoment.weekday()
					label           : fromMoment.format('hh:mm a')

				@push formatted

	getForDate : (momentDate, ignoreTimes = []) ->
		if _.isString momentDate
			date = momentDate
		else
			date = momentDate.format('YYYY-MM-DD')

		duration = Wizard.model.get('data.selected_time.duration')

		@filter (model) ->
			return false unless model.get('date') is date
			return true unless ignoreTimes.length

			timeFrom = model.get('from')
			timeUntil = model.get('until')

			overlaps = ignoreTimes.filter (ignoreFrom) ->
				# support a string date
				unless Moment.isMoment ignoreFrom
					ignoreFrom = datetimeParsers.parseToMoment ignoreFrom

				ignoreUntil = ignoreFrom.clone().add duration, 'minutes'

				# check for overlaps
				timeFrom.unix() < ignoreUntil.unix() and timeUntil.unix() > ignoreFrom.unix()

			overlaps.length is 0

	getForWeek : (startOfWeek) ->
		endOfWeek = startOfWeek.clone().endOf('week')
		@filter (model) ->
			from = model.get('from')
			from.isAfter(startOfWeek) and from.isBefore(endOfWeek)

	hasTimesForWeek : (startOfWeek) ->
		startOfWeek = @fetchedDate.clone() unless startOfWeek
		@getForWeek(startOfWeek).length > 0

	getWeekdays : (startingWeek, timesCollection, includeSelectedWeekdays = true) ->
		if timesCollection.length
			startingDay = timesCollection.first().get('from')
		else
			startingDay = startingWeek.startOf('week')

		selectedWeekdays = timesCollection.map (model) -> model.get('from').weekday()

		days = []
		for weekday in [0..6]
			date = startingDay.clone().add(weekday, 'day')
			if includeSelectedWeekdays or (selectedWeekdays.indexOf(date.weekday()) is -1)
				if @getForDate(date).length
					days.push
						moment     : date
						weekday    : date.format('dddd')
						date       : date.format('YYYY-MM-DD')
						date_label : date.format('MMMM, D')
		days

module.exports = Collection

Backbone = require 'backbone'

class Model extends Backbone.Model

	defaults:
		id: ''
		from: ''
		until: ''
		label: ''

	remove: ->
		@destroy()

	destroy: ->
		@set 'id', null
		super

module.exports = Model

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Cocktail = require 'backbone.cocktail'
datetimeParsers = require 'utils/datetime_parsers'

require 'backbone.stickit'

class View extends Backbone.View

	className : 'wizard_booking--sidebar_info'

	initialize : (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@bind()
		@addStickit()
		@render()

		@maybeSetAdditionalInfo()

	bind : ->
		@listenTo Wizard, 'next_step', @maybeSetAdditionalInfo

	addStickit : ->
		@bindings =
			'[data-professional-name]' : 'additional.professional.name'

			'[data-professional-avatar]' :
				observe      : 'additional.professional.avatar'
				updateMethod : 'html'
				onGet        : (val) ->
					if val
						val = "<img src='#{val}' alt=''>"
					val

			'[data-appointment-service-name]' : 'additional.service_name'

			'[data-appointment-duration]' :
				observe : ['additional.duration', 'additional.max_visits', 'additional.number_of_months']
				onGet   : (values) ->
					"#{values[0]} x #{values[1]} for #{values[2]}"


			'[data-appointment-type]' :
				observe      : ['additional.location', 'additional.registration_location']
				updateMethod : 'html'
				onGet        : (values) ->
					'' if values[0] and values[1] and values[0].id is values[1].id
					"<span>home visit</span>"

				classes :
					# registration location equals location meaning
					# that we don't have a client location set
					'm-hide' :
						observe : ['additional.location.id', 'additional.registration_location.id']
						onGet   : (values) -> !values[0] or !values[1] or values[0] is values[1]

			'[data-appointment-location]' :
				observe      : ['additional.location.name', 'additional.location.address']
				updateMethod : 'html'
				onGet        : (values) ->
					return "Select Location" unless values[0] and values[1]
					html = "<strong>#{values[0]}</strong>"
					if values[0] isnt "Virtual"
						html += " #{values[1]}"
					html

			'[data-appointment-price]' :
				updateMethod : 'html'
				observe      : 'additional.price'
				onGet        : (val) -> "<strong>#{val}</strong>"

			'[data-appointment-times]' :
				observe      : 'additional.recurring_times'
				updateMethod : 'html'
				onGet        : (value) ->
					return if !(value? and value.length)
					html = ''
					value.forEach (time) ->
						html += """<p><strong>#{time.weekday}, #{time.time} (Weekly)</strong><br>
							First #{time.weekday} appointment: <span>#{time.date}</span></p>"""
					html

			'[data-payment-info]' :
				classes :
					'm-hide' :
						observe : 'additional.payment_info'
						onGet   : (val) -> if val then false else true

			'[data-payment-info-title]' :
				updateMethod : 'html'
				observe      : 'additional.payment_info.title'

			'[data-payment-info-content]' :
				updateMethod : 'html'
				observe      : 'additional.payment_info.content'

	render : ->
		@$container.find('.wizard_booking--sidebar_info').remove()
		@$el.html RecurringInfo.Templates.Base()
		@$el.appendTo @$container

		@delegateEvents()
		@stickit @recurringModel
		@

	# set additional information
	maybeSetAdditionalInfo : ->
		Wizard.model.set
			additional :
				duration         : Wizard.model.get('data.selected_time.duration_label')
				price            : Wizard.model.get('data.selected_time.price_label')
				service_name     : Wizard.model.get('data.selected_time.service_name')
				max_visits       : Wizard.model.get('data.selected_time.max_visits_label')
				number_of_months : Wizard.model.get('data.selected_time.number_of_months_label')
				location         :
					id      : Wizard.model.get('data.selected_time.location_id')
					name    : Wizard.model.get('data.selected_time.location_name')
					address : Wizard.model.get('data.selected_time.location_address')
				client_location  :
					id      : Wizard.model.get('data.selected_time.client_location_id')
					name    : Wizard.model.get('data.selected_time.client_location_name')
					address : Wizard.model.get('data.selected_time.client_location_address')
				recurring_times  : do ->
					availabilities = Wizard.model.get('data.selected_time.availabilities')
					return [] unless availabilities
					availabilities.map (availability) ->
						from = datetimeParsers.parseToMoment availability.from
						{
							availability_id : availability.availability_id
							weekday         : from.format('dddd')
							date            : from.format('MMMM D, YYYY')
							time            : from.format('hh:mm a')
						}

module.exports = View

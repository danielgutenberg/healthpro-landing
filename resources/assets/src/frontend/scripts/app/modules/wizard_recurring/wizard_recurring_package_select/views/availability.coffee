Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	tagName : 'li'

	events  :
		'click' : 'selectAvailability'

	initialize : (options) ->
		Cocktail.mixin @, Mixins.Common
		@setOptions options

	render : ->
		@$el.html "<button>#{@model.get('label')}</button>"
		@

	selectAvailability : ->
		@parentView.setSelectedAvailability
			availability_id : @model.get('availability_id')
			from            : @model.get('from')
			until           : @model.get('until')

		@$el.addClass('m-active').siblings('.m-active').removeClass('m-active')
		@

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
datetimeParsers = require 'utils/datetime_parsers'
Select = require 'framework/select'
vexDialog = require 'vexDialog'
ToggleEl = require 'utils/toggle_el'
ScrollToEl = require 'app/modules/wizard_booking/utils/scroll_to_element'

require 'backbone.stickit'

class View extends Backbone.View

	initialize : (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@instances = []
		@addListeners()
		@bind()
		@render()

	fetchAvailabilities : ->
		Wizard.loading()
		$.when(
			@collections.availabilities.fetchByDate @baseModel.get('starting_week')
		).then(
			=>
				Wizard.ready()
				ScrollToEl @baseView.$el.$blockTimes
		).fail(
			-> Wizard.ready()
		)
		@

	afterAvailabilitiesReady : ->
		@maybeShowNoAvailabilitiesNotice()
		@initDatepicker()
		@timesCollectionChanged()
		@

	addListeners : ->
		@listenTo @collections.availabilities, 'availabilities:ready', @afterAvailabilitiesReady

		@listenTo @collections.times, 'add', (model) =>
			@renderTimeRow model
			@timesCollectionChanged()

		@listenTo @collections.times, 'remove', =>
			@timesCollectionChanged()

		@listenTo @baseModel, 'change:starting_week', (model, week, options) =>
			return if options?.noFetch
			@reset()

		# load availabilities when package is confirmed
		@listenTo @baseModel, 'change:package_confirmed', (model, val) ->
			@fetchAvailabilities() if val

		@listenTo @, 'rendered', =>
			@cacheDom()
			@setSelectedTimes()
		@

	bind : ->
		@bindings =
			'[name="starting_week"]' :
				selectOptions :
					collection : @baseModel.getStartingWeeks()
					labelPath  : 'label'
					valuePath  : 'value'
				observe       : 'starting_week'
				afterUpdate   : (el) -> el.change()
				initialize    : (el) -> new Select el
				onGet         : (val) -> val?.format('YYYY-MM-DD')
				onSet         : (val) -> datetimeParsers.parseToMoment val
			'[name="auto_renew"]'    :
				observe : 'auto_renew'
				onGet   : (val) ->
					Wizard.model.set 'auto_renew', val
					!!val
				onSet   : (val) ->
					Wizard.model.set 'auto_renew', +val
					+val
		@

	cacheDom : ->
		@$el.$times = @$el.find('[data-availabilities-times]')
		@$el.$datepicker = @$el.find('[data-availabilities-datepicker]')
		@$el.$error = @$el.find('[data-availabilities-error]')
		@$el.$startingWeek = @$el.find('[data-availabilities-starting-week]')
		@$el.$availabilitiesEmpty = @$el.find('[data-availabilities-empty]')
		@

	render : ->
		pkg = @baseModel.getCurrentServicePackage()
		@$el.html WizardRecurringPackageSelect.Templates.Availabilities
			duration_label   : pkg.duration_label
			max_visits_label : pkg.max_visits_label
		@stickit @baseModel
		@trigger 'rendered'
		@

	raiseGenericError : (message = null, type = 'error') ->
		ToggleEl @$el.$error, if message then true else false
		@$el.$error.html if message then  "<div class='alert m-#{type} m-show'>#{message}</div>" else ''
		@

	canAddTime : ->
		return false unless @collections.availabilities.hasTimesForWeek @baseModel.get('starting_week')

		pkg = @baseModel.getCurrentServicePackage()

		if pkg.max_visits is 0
			weekdays = @collections.availabilities.getWeekdays(
				@baseModel.get('starting_week'),
				@collections.times,
				WizardRecurringPackageSelect.Config.DisplaySelectedWeekdays
			).length
			return weekdays isnt 0

		pkg.max_visits > @collections.times.length

	initDatepicker : ->
		@resetDatepicker()
		return unless @canAddTime()

		@datepicker = new WizardRecurringPackageSelect.Views.Datepicker
			parentView  : @
			baseView    : @baseView
			baseModel   : @baseModel
			collections : @collections

		@$el.$datepicker.html @datepicker.render().el
		@

	resetDatepicker : ->
		@raiseGenericError null
		if @datepicker
			@datepicker.remove?()
			@datepicker = null
			@$el.$datepicker.html ''
		@

	renderTimeRow : (time) ->
		@instances.push timeRowView = new WizardRecurringPackageSelect.Views.Time
			parentView : @
			baseView   : @baseView
			baseModel  : @baseModel
			model      : time
		@$el.$times.append timeRowView.render().el
		@

	selectAvailability : ->
		@datepicker.selectAvailability()

	setSelectedTimes : ->
		selectedTime = Wizard.model.get('data.selected_time')
		return unless selectedTime.availabilities?
		return unless _.isArray selectedTime.availabilities

		# set the start date for the base model
		from = datetimeParsers.parseToMoment selectedTime.availabilities[0].from
		@baseModel.set 'starting_week', from.startOf('week'),
			noFetch : true
		selectedTime.availabilities.forEach (availability) =>
			time =
				availability_id : availability.availability_id
				from            : datetimeParsers.parseToMoment availability.from
				until           : datetimeParsers.parseToMoment availability.until

			@collections.times.add time

	timesCollectionChanged : ->
		@toggleStartingWeek()
		@initDatepicker()

		@baseModel.set 'time_confirmed', !@canAddTime()

	maybeShowNoAvailabilitiesNotice : ->
		ToggleEl @$el.$availabilitiesEmpty, !@collections.availabilities.hasTimesForWeek(@baseModel.get('starting_week'))

	reset : (fetchAvailabilities = true) ->
		@instances.forEach (instance) -> instance?.remove()
		@collections.times.reset()
		@fetchAvailabilities() if fetchAvailabilities

	toggleStartingWeek : ->
		ToggleEl @$el.$startingWeek, @collections.times.length is 0
		@

module.exports = View

Handlebars = require 'handlebars'

window.RecurringInfo = RecurringInfo =
	Views:
		Base: require('./views/base')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

class RecurringInfo.App
	constructor: (options) ->
		@view = new RecurringInfo.Views.Base
			recurringModel: options.recurringModel
			$container: options.$container

module.exports =  RecurringInfo

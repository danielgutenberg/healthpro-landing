Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
ScrollToEl = require 'app/modules/wizard_booking/utils/scroll_to_element'

require 'backbone.validation'
require 'backbone.stickit'

require 'tooltipster'

class View extends Backbone.View

	className : 'wizard_booking--step m-package_select'

	initialize : (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@instances = []
		@$wizardSubFooter = null
		@prepareStep()

	prepareStep : ->
		Wizard.view.toggleFooter true
		@updateWizardFooterButtons()
		@centerPopup()

	updateWizardFooterButtons: ->
		return unless Wizard?.model?
		Wizard.model.set 'isPaymentStep', false
		Wizard.model.set 'proceedLabel', do =>
			if @model.get('package_confirmed') && @model.get('service_confirmed') && @model.get('time_confirmed')
				return 'Proceed to Payment'
			if @model.get('package_confirmed') && @model.get('service_confirmed')
				return 'Select Time'
			'Continue'

		@removeWizardSubFooter()
		if !@model.get('time_confirmed') && @collections.times.length
			@appendWizardSubFooter()

	addListeners : ->
		@listenTo Wizard, 'proceed', @proceed

		# update wizard buttons, center popup, etc
		@listenTo @model, 'change:service_confirmed change:package_confirmed change:time_confirmed', @prepareStep
		@listenTo @collections.times, 'add remove', @prepareStep

		# if user changes service, location or package - reset times collection
		@listenTo @model, 'change:current_service_id change:current_location_id change:current_session_package_rule_id', =>
			@availabilitiesView.reset(false) if @availabilitiesView?
			@prepareStep()

		@listenTo @, 'rendered', =>
			if @model.shouldFetchServices()
				@listenTo @model, 'services:ready', => @doWhenServicesReady()
			else
				@doWhenServicesReady()

	bind: ->
		@bindings =
			'[data-recurring-package-view]' :
				classes :
					'm-hide' :
						observe : 'service_confirmed'
						onGet   : (val) -> !val
			'[data-recurring-time-view]' :
				classes :
					'm-hide' :
						observe : ['service_confirmed', 'package_confirmed']
						onGet   : (val) -> !val[0] or !val[1]


	cacheDom : ->
		@$el.$services = @$el.find('[data-recurring-service]')
		@$el.$locations = @$el.find('[data-recurring-location]')
		@$el.$packages = @$el.find('[data-recurring-package]')
		@$el.$time = @$el.find('[data-recurring-time]')
		@$el.$blockPackages = @$el.find('[data-recurring-package-view]')
		@$el.$blockTimes = @$el.find('[data-recurring-time-view]')
		@

	render : ->
		@$el.html WizardRecurringPackageSelect.Templates.Base()
		@bind()
		@stickit()

		@delegateEvents()
		@cacheDom()
		@addListeners()

		@trigger 'rendered'
		@

	initTooltips: ->
		@$el.find('[data-recurring-tooltip]').tooltipster
			animation      : 'fade'
			theme          : 'wizard_booking--tooltip'
			arrow          : true
			trigger        : 'click'
			hideOnClick    : true
			position       : 'bottom'
			contentCloning : false
			content        : @$el.find('[data-recurring-tooltip-content]')
		@

	doWhenServicesReady : ->
		@appendParts()

	appendParts : ->
		@appendServices()
		@appendLocations()
		@appendPackages()
		@initTimes()
		@initTooltips()
		@centerPopup()

	appendServices : ->
		@instances.push @servicesView = new WizardRecurringPackageSelect.Views.Services
			el          : @$el.$services
			baseView    : @
			baseModel   : @model
			collections : @collections

	appendLocations : ->
		@instances.push @locationsView = new WizardRecurringPackageSelect.Views.Locations
			el          : @$el.$locations
			baseView    : @
			baseModel   : @model
			collections : @collections

	appendPackages : ->
		@instances.push @packagesView = new WizardRecurringPackageSelect.Views.Packages
			el          : @$el.$packages
			baseView    : @
			baseModel   : @model
			collections : @collections

	initTimes : ->
		@instances.push @availabilitiesView = new WizardRecurringPackageSelect.Views.Availabilities
			el          : @$el.$time
			baseView    : @
			baseModel   : @model
			collections : @collections

	removeWizardSubFooter: ->
		return unless @$wizardSubFooter
		@$wizardSubFooter.remove()
		@$wizardSubFooter = null

	appendWizardSubFooter: ->
		return if @$wizardSubFooter

		@$wizardSubFooter = $('<div class="wizard_booking--footer--sub"><p>or <button>Proceed to Booking and Add Another Time Later</button></p></div>')
		Wizard.view.$el.$footer.append @$wizardSubFooter
		@$wizardSubFooter.find('button').on 'click', => @forceProceed()

	proceed : ->
		# confirm location before submitting
		unless @model.get('service_confirmed')
			@model.set 'service_confirmed', true
			setTimeout( =>
				ScrollToEl @$el.$blockPackages
			, 50)
			return

		# confirm location before submitting
		unless @model.get 'package_confirmed'
			@model.set('package_confirmed', true)
			setTimeout( =>
				ScrollToEl @$el.$blockTimes
			, 50)
			return

		unless @model.get 'time_confirmed'
			@availabilitiesView.selectAvailability()
			return

		@forceProceed()

	forceProceed: ->
		return unless @model.get('service_confirmed') and @model.get('package_confirmed') and @collections.times.length > 0

		# force reset data
		Wizard.model.set 'data', null
		Wizard.model.set 'data', @model.getBookingData()

		location = @model.getCurrentLocation()
		Wizard.model.set 'next_step', if location.location_type is 'home-visit' then 'location_select' else 'payment'

		@removeWizardSubFooter()

		Wizard.trigger 'next_step'

	centerPopup: ->
		setTimeout( ->
			Wizard.view.centerPopup()
		, 50)


module.exports = View

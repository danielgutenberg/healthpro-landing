Abstract = require 'app/modules/wizard/abstract'

RecurringWizardView = require('./views/base')
RecurringWizardModel = require('./models/base')
RecurringWizardRouter = require('./router/router')

###
  RECURRING WIZARD PRIVATE CLASS
  AVAILABLE THROUGH THE SINGLETON PATTERN
###
class RecurringWizard extends Abstract

	inited: false

	defaults: ->
		{
			bookingData : null
			el          : $('[data-wizard="recurring"]')
			forcePage   : false
		}

	init: (options = null) ->
		if @inited
			console.error 'RecurringWizard has already been inited'
			return @

		@setOptions options if options

		@addListeners()

		@model = new RecurringWizardModel
			data: @options.bookingData
			wizard_name: 'recurring'

		@view = new RecurringWizardView
			model: @model
			el: @options.el

		@inited = true
		@

	initRouter: ->
		super
		@router = new RecurringWizardRouter
			model: @model
			view: @view
		@

class RecurringWizardSingleton
	instance = null

	@get: (options = {}) ->
		instance ?= new RecurringWizard options

	@destroy: ->
		instance = null

if $('[data-wizard="recurring"]').length
	window.Wizard = RecurringWizardSingleton.get()
	window.Wizard.init()

module.exports = RecurringWizardSingleton

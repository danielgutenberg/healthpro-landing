Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
getRoute = require 'hp.url'
getApiRoute = require 'hp.api'
BookingData = require 'app/modules/wizard_booking/utils/booking_data'

class View extends Backbone.View

	className : 'wizard_booking--step m-introductory_session'

	events :
		'click [data-send-message]' : 'sendMessage'

	initialize : (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@prepareStep()

	prepareStep: ->
		Wizard?.model?.set 'isPaymentStep', true
		Wizard?.model?.set 'proceedLabel', 'Book Now'

	addListeners : ->
		@listenTo Wizard, 'proceed', @proceed
		@

	render : ->
		@$el.html WizardRecurringIntroductorySession.Templates.Base()
		@

	sendMessage : ->
		Wizard.loading()
		Wizard.redirect getRoute('dashboard-conversation', {profileId : @model.getProviderId()})
		@

	# create new introductory session booking
	proceed : ->
		Wizard.loading()
		$.ajax
			url         : getApiRoute('api-wizard-booking-initial')
			method      : 'post'
			type        : 'json'
			contentType : 'application/json; charset=utf-8'
			data        : JSON.stringify
				data   : JSON.stringify BookingData.getIntroductoryData(@model) # data needs to be a string
				_token : window.GLOBALS._TOKEN
			success     : ->
				Wizard.redirect '/wizard/booking/introductory_session'

module.exports = View

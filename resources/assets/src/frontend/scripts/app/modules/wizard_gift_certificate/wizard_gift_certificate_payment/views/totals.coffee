Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Numeral = require 'numeral'
Formats = require 'config/formats'
HumanTime = require 'utils/human_time'
Device = require 'utils/device'
Config = require 'app/modules/wizard_gift_certificate/config'

class View extends Backbone.View

	className: 'wizard_booking--payment_totals'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()

	addListeners: ->
		@listenTo @, 'updated', @updateAdditionalData

	render: ->
		@updateTotals()
		@$el.html WizardGiftCertificatePayment.Templates.Totals @totals
		@trigger 'updated'
		@

	updateTotals: ->
		@totals =
			items: []

		@certificateItem(@totals)
		@discountItem(@totals)
		@calculateTotals(@totals)

		if Device.isMobile()
			@totals.info =
				title   : Config.Payment.SidebarInfo.Title
				content : Config.Payment.SidebarInfo.Content

		@totals

	certificateItem: (data) ->
		selectedCertificate = Wizard.model.get('data.certificate')
		item = {
			label : "#{selectedCertificate.service_name} - " + HumanTime.minutes(selectedCertificate.duration)
			value : Numeral(selectedCertificate.price).format(Formats.Price.Default)
			price : Numeral(selectedCertificate.price).value()
		}
		data.items.push item
		return

	discountItem: (data) ->
		coupon = @baseModel.get('coupon')
		unless coupon?.applied_coupons?.length
			return

		applied = coupon.applied_coupons[0]

		amount = Numeral(applied.amount)
		item = {
			label: "Coupon #{coupon.entered_coupon?.toUpperCase()}"
			value : amount.format(Formats.Price.Default)
			price : amount.value()
		}
		data.items.push item
		return

	calculateTotals: (data) ->
		total = _.reduce data.items, (t, item) ->
			t += item.price
		, 0
		data.total = Numeral(_.max([total, 0])).format(Formats.Price.Default)
		return


	updateAdditionalData: ->
		Wizard.model.set 'data.selected_time.total_price', @totals.total

module.exports = View

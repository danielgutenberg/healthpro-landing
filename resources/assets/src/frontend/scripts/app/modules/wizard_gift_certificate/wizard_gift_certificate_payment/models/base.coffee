Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Formats = require 'config/formats'
Numeral = require 'numeral'
DeepModel = require 'backbone.deepmodel'

class Model extends DeepModel

	defaults :
		payment_method : ''
		credit_card_id : null
		coupon         :
			applied_coupons : []
			entered_coupon  : null
			display_form    : false

	initialize: ->
		@getDiscountData()

	getDiscountData: ->
		if Wizard.model.get 'data.order.line_items.coupons'
			@set 'coupon.applied_coupons', Wizard.model.get('data.order.line_items.coupons').map (coupon) ->
				{
					id              : coupon.id
					amount          : coupon.amount
					type            : coupon.payload.type
					description     : coupon.payload.description
				}

		else
			@unset 'coupons'

	createCertificate: ->
		data = @getCertificateData()
		data._token = window.GLOBALS._TOKEN
		$.ajax
			url: getApiRoute('ajax-gift-certificates')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify(data)

	createOrder: ->
		data = @getOrderData()
		data._token = window.GLOBALS._TOKEN
		$.ajax
			url: getApiRoute('api-orders')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify(data)

	save: ->
		Wizard.loading()
		$.when(
			@createCertificate()
		).then(
			(res) =>
				Wizard.model.set 'data.certificate.id', res.data.id
				@createOrder()
		).then(
			(res) =>
				Wizard.ready()
				Wizard.model.set 'data.order', res.data # save the order
				Wizard.trigger 'next_step'
		).fail(
			=>
				Wizard.ready()
				console.log 'failed', arguments
		)
		@

	getCertificateData: ->
		recipientNameParts = Wizard.model.get('data.recipient_name').split ' '
		lastName = recipientNameParts.pop()
		firstName = recipientNameParts.join(' ')
		data =
			provider_id: Wizard.model.get('data.provider_id')
			email: Wizard.model.get('data.recipient_email')
			last_name: lastName
			first_name: firstName
			send_date: Wizard.model.get('data.delivery_date')
			message: Wizard.model.get('data.message')
			entity_id: Wizard.model.get('data.certificate.session_id')
			entity_type: 'service_session'

		data

	getOrderData: ->
		data =
			profile_id: GLOBALS._PID
			card_id:  @get('credit_card_id') # [wip] will be changed later
			items: []

		data.items.push
			id: Wizard.model.get('data.certificate.id')
			name: 'gift_certificate'

		if @get 'coupons'
			_.each @get('coupons'), (coupon) ->
				data.items.push
					id: coupon.id
					name: 'coupon'

		data

	# getting coupon ID by name
	addCoupon: (coupon_name) ->
		unless coupon_name
			coupon_name = @get('coupon.entered_coupon')
		@.trigger 'coupon:loading'
		$.ajax
			url: getApiRoute('ajax-coupon-get', { coupon: coupon_name })
			data: { types: ['order'], for: 'client' }
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			success: (res) =>
				# checking for applied coupons
				applied = null
				_.each @get('coupons'), (coupon) ->
					if res.data.id is coupon.id then applied = true

				# prevent multiple coupons usage
				unless applied
					@applyCoupon(res.data.id)
				else
					message = 'Coupon code <b>' + coupon_name + '</b> already applied.'
					vexDialog.buttons.YES.text = 'Ok'
					vexDialog.alert
						message: message

					@.trigger 'coupon:ready'

			error: (res) =>
				message = 'An error occurred. Please reload the page and try again.'
				if res.responseJSON?.errors?.coupon?
					if res.responseJSON.errors.coupon.messages[0] == 'Coupon is not exists'
						message = 'Coupon code <b>' + coupon_name + '</b> is not valid.'
					else
						message = res.responseJSON.errors.coupon.messages[0]

				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: message

				@.trigger 'coupon:ready'

	# applying coupon to the order
	applyCoupon: (id) ->
		if Wizard.model.get 'data.order'
			data =
				items: []

			data.items.push
				id: id
				name: 'coupon'
			data._token = window.GLOBALS._TOKEN

			$.ajax
				url: getApiRoute('api-order', {orderId: Wizard.model.get 'data.order.id'})
				method: 'put'
				type: 'json'
				contentType: 'application/json; charset=utf-8'
				data: JSON.stringify(data)
				success: (res) =>
					Wizard.model.set 'data.order', res.data # save updated order
					Wizard.trigger 'save_step' # saving wizard data on server (in case when user refreshes the page on current step)
					@setCoupons(res.data.line_items.coupons) # save coupons in current model
					@set 'price.total', res.data.total
					@.trigger 'coupon:ready'
					@.trigger 'coupon:added'

		else
			data = @getOrderData()
			data.items.push
				id: id
				name: 'coupon'
			data._token = window.GLOBALS._TOKEN

			# creating pre-order to check validity and store the response
			$.ajax
				url: getApiRoute('api-orders')
				method: 'post'
				type: 'json'
				contentType: 'application/json; charset=utf-8'
				data: JSON.stringify(data)
				success: (res) =>
					Wizard.model.set 'data.order', res.data # save the order
					@setCoupons(res.data.line_items.coupons) # save coupons in current model
					@set 'price.total', res.data.total
					@.trigger 'coupon:ready'

				error: (err) =>
					vexDialog.buttons.YES.text = 'Ok'
					vexDialog.alert
						message: 'This coupon code cannot be used with selected service'
					@.trigger 'coupon:ready'

	setCoupons: (coupons) ->
		couponsArr = []
		_.each coupons, (couponItem) =>
			coupon =
				id: couponItem.orderItemEntity.entity_id
				amount: couponItem.orderItemEntity.amount
				type: couponItem.payload.type
				formattedAmount: @formatDiscount(couponItem.amount, couponItem.payload.type)
				description: couponItem.payload.description

			couponsArr.push coupon

		@set 'coupons', couponsArr

	formatDiscount: (amount, type) ->
		if type == 'fixed'
			return '- ' + Numeral(amount).format(Formats.Price.Default)
		else if type == 'percent'
			return '- ' + amount + '%'
		else
			return amount

	removeCoupon: ->
		@set 'coupons', null # clear all coupons
		Wizard.model.set 'data.order', null # to create new order with the next coupon
		@trigger 'coupon:removed'

module.exports = Model

Handlebars = require 'handlebars'

# list of all instances
window.WizardGiftCertificateSelect = WizardGiftCertificateSelect =
	Config:
		DatepickerFormat: 'MM/DD/YYYY' # used for MomentJS
		DateFormat: 'YYYY-MM-DD'
	Views:
		Base: require('./views/base')
	Models:
		Base: require('./models/base')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

class WizardGiftCertificateSelect.App
	constructor: (options = {}) ->
		# preset data set when wizard is opened from search results
		if options.presetData
			presetData = options.presetData
		else if Wizard? and Wizard.inited
			presetData = Wizard.model.getPresetData()
		else
			presetData = null

		@model = new WizardGiftCertificateSelect.Models.Base {},
			type: options.type
			presetData: presetData

		@view = new WizardGiftCertificateSelect.Views.Base
			model: @model

		return {
			view: @view
			model: @model
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = WizardGiftCertificateSelect

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Select = require 'framework/select'
Datepicker = require 'framework/datepicker'
Moment = require 'moment'
Maxlength = require 'framework/maxlength'
HumanTime = require 'utils/human_time'
Formats = require 'config/formats'
Numeral = require 'numeral'
WizardDataFormatter = require 'utils/wizard_data_formatter'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	className: 'certificate_select'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@instances = []

		Wizard?.model?.set 'proceedLabel', 'Continue'
		Wizard?.model?.set 'backLabel', 'Cancel'

		@addListeners()
		@bind()
		@canGoAhead()

		@addValidation
			selector: 'data-selector'


	addListeners: ->
		@listenTo Wizard, 'proceed', @proceed

		@listenTo @model, 'loading', @showLoading
		@listenTo @model, 'ready', @hideLoading
		@listenTo @model, 'change:delivery_date_type', @toggleDatepicker
		@listenTo @model, 'change:current_service_id change:current_session_id', =>
			@updateWizardAdditionalData()
			@canGoAhead()

		@listenTo @, 'rendered', =>
			if @model.get('services').length
				@doWhenServicesReady()
			else
				@listenTo @model, 'services:ready', => @doWhenServicesReady()

	bind: ->
		@bindings =
			'[name="service"]':
				observe: 'current_service_id'
				selectOptions:
					collection: => @model.generateServiceSelect()
					labelPath: 'detailed_name'
					valuePath: 'service_id'
				getVal: ($el) =>
					val = $el.val()
					newService = @model.getService +val
					if newService?
						@model.set 'current_service_id', newService.service_id
						if _.where(newService.sessions, {session_id: @model.get('current_session_id')}).length
							@model.trigger 'change:current_session_id'
						else
							@model.set 'current_session_id', _.first(newService.sessions).session_id
					else
						@model.set
							current_service_id: null
							current_session_id: null
					val
				updateModel: false
				afterUpdate: (el) -> el.change() # forcing select2 to update selected option
				initialize: (el) ->
					new Select el

			'[name="session"]':
				observe: 'current_session_id'
				selectOptions:
					collection: => @model.generateSessionSelect()
					labelPath: 'duration_text'
					valuePath: 'session_id'
				getVal: ($el) =>
					val = $el.val()
					@model.set 'current_session_id', @model.getSession(+val)?.session_id
					val
				updateModel: false
				afterUpdate: (el) -> el.change() # forcing select2 to update selected option
				initialize: (el) ->
					new Select el,
						dropdownClass: 'select_session--dropdown'
						templateSelection: (data) ->
							# Customizing selecte option to show the price
							price = $(data.element).parent().attr('label')
							if price
								priceBlock = $('<b>').html(price)
								$('<span>').html(data.text).append(priceBlock)
							else
								$('<span>').html(data.text)

			'[name="recipient_name"]': 'recipient_name'
			'[name="recipient_email"]': 'recipient_email'
			'[name="message"]':
				observe: 'message'
				onGet: (val, options) ->
					$el = @$el.find(options.selector)
					$parent = $el.parent()
					unless $parent.attr('data-maxlength') is 'inited'
						$el.val val
						new Maxlength $parent
					val

			'[name="delivery_date_type"]': "delivery_date_type"
			'[name="delivery_date"]':
				observe: 'delivery_date'
				onGet: (val) => val.format(WizardGiftCertificateSelect.Config.DatepickerFormat)
				onSet: (val) => Moment(val, WizardGiftCertificateSelect.Config.DatepickerFormat)
				initialize: ($el) =>
					@$el.$datepicker = $el.closest('[data-datepicker]')
					@datepicker = new Datepicker @$el.$datepicker,
						minDate: 0
						dateFormat: "mm/dd/yy"

	render: ->
		@$el.html WizardGiftCertificateSelect.Templates.Base()

		@cacheDom()
		@stickit()

		@trigger 'rendered'
		@

	cacheDom: ->
		@$el.$error = @$('[data-error]')

	doWhenServicesReady: ->
		@hideLoading()
		@centerPopup()
		@toggleDatepicker()
		@updateWizardAdditionalData()

	canGoAhead: -> Wizard.model.set 'canGoAhead', @model.getCurrentService() and @model.getCurrentSession()

	centerPopup: ->
		setTimeout( ->
			Wizard.view.centerPopup()
		, 50)

	proceed: ->
		@raiseGenericError null

		return if @model.validate()

		# Wizard.model.set WizardDataFormatter.formatAdditionalData model.toJSON()
		Wizard.model.set 'data', null
		Wizard.model.set 'data', @model.getBookingData()

		# force payment step
		Wizard.model.set 'next_step', 'payment'

		Wizard.trigger 'next_step'

	raiseGenericError: (message = null, type = 'error') ->
		if message
			@$el.$error.html("<div class='alert m-#{type} m-show'>#{message}</div>").removeClass 'm-hide'
		else
			@$el.$error.html('').addClass 'm-hide'

	toggleDatepicker: ->
		if @model.get('delivery_date_type') is 'now'
			@$el.$datepicker.addClass 'm-hide'
		else
			@$el.$datepicker.removeClass 'm-hide'
		@

	updateWizardAdditionalData: ->
		currentService = @model.getCurrentService()
		currentSession = @model.getCurrentSession()

		Wizard.model.set 'additional', null
		Wizard.model.set 'additional',
			service_name: if currentService then currentService.detailed_name else 'Select Service'
			duration: if currentSession then currentSession.duration else 'Select Session'
			price: if currentSession then Numeral(currentSession.price.replace(',', '')).format(Formats.Price.Default) else '-'
		@

module.exports = View

Backbone = require 'backbone'
HumanTime = 'utils/human_time'
Formats = require 'config/formats'
HumanTime = require 'utils/human_time'
Numeral = require 'numeral'
dateTimeParsers = require 'utils/datetime_parsers'
Moment = require 'moment'

class View extends Backbone.View

	className: 'booking_success m-certificate'

	initialize: ->
		Wizard?.model?.set 'proceedLabel', 'Ok'
		Wizard?.model?.set 'backLabel', ''
		Wizard?.model?.set 'disableBack', true
		Wizard?.view?.toggleFooter false

	render: ->
		@$el.html WizardGiftCertificateSuccess.Templates.Base @getTemplateData()
		Wizard.model.resetWizardData()
		Wizard.view.centerPopup()
		@

	getTemplateData: ->
		{
			notificationTime: do ->
				deliveryDate = dateTimeParsers.parseToMoment Wizard.model.get('data.delivery_date')
				if deliveryDate.isSame(Moment(), 'day')
					return 'immediately'
				'on ' + deliveryDate.format('MMMM D, YYYY')
			duration: HumanTime.minutes Wizard.model.get('data.certificate.duration')
			serviceName: Wizard.model.get('data.certificate.service_name')
			price: Numeral(Wizard.model.get('data.order.total')).format(Formats.Price.Default)
		}

module.exports = View

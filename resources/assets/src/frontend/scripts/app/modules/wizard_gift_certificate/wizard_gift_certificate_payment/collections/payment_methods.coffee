Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Config = require 'app/modules/wizard_gift_certificate/config'

class Collection extends Backbone.Collection

	fetch : ->
		url = do ->
			if Wizard.model.get('data.appointment_id')
				getApiRoute 'ajax-appointment-payment-options',
					appointmentId : Wizard.model.get('data.appointment_id')
			else
				getApiRoute 'ajax-provider-payment-options',
					providerId: Wizard.model.get('data.provider_id')
		$.ajax
			url     : url
			method  : 'get'
			success : (res) =>
				_.each res.data, (enabled, type) =>
					unless type == 'in_person'
						@push {
							type    : type
							name    : Config.Payment.PaymentMethods.Methods[type]
							icon    : Config.Payment.PaymentMethods.Icons[type]
							enabled : enabled
						}

module.exports = Collection

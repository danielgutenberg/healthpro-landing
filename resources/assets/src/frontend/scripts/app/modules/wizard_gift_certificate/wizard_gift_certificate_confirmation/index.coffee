Backbone = require 'backbone'
Handlebars = require 'handlebars'

# list of all instances
window.WizardGiftCertificateConfirmation = WizardGiftCertificateConfirmation =
	Views:
		Base: require('./views/base')
	Models:
		Base: require('./models/base')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

# events bus
_.extend WizardGiftCertificateConfirmation, Backbone.Events

class WizardGiftCertificateConfirmation.App
	constructor: ->
		@model = new WizardGiftCertificateConfirmation.Models.Base()
		@view = new WizardGiftCertificateConfirmation.Views.Base
			model: @model
		{
			model: @model
			view: @view
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = WizardGiftCertificateConfirmation

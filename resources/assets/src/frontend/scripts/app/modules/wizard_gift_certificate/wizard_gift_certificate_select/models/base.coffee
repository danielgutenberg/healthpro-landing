Backbone = require 'backbone'
getApiRoute = require 'hp.api'
HumanTime = require 'utils/human_time'
Formats = require 'config/formats'
Numeral = require 'numeral'
Moment = require 'moment'
datetimeParsers = require 'utils/datetime_parsers'
serviceFormatter = require 'utils/formatters/service'

class Model extends Backbone.Model
	defaults: ->
		resetToFirstStep: 'recalculate'
		provider_id: null
		services: []
		current_service_id: null
		current_session_id: null

		recipient_name: ''
		recipient_email: ''

		message: ''
		delivery_date: Moment()
		delivery_date_type: 'now'

	validation:
		current_service_id:
			required: true
			msg: 'Select service'
		current_session_id:
			required: true
			msg: 'Select session'
		recipient_name:
			required: true
			msg: 'Please enter recipient name'
		recipient_email:
			required: true
			pattern: 'email'
			msg: 'Please enter recipient email'
		message: [
			{
				required: true
				msg: 'Please write your message'
			}
			{
				maxLength: 1000
				msg: 'Your message is too long, please enter a maximum of 1000 characters'
			}
		]

	initialize: (attrs, options) ->
		@addListeners()

		@setInitPresetData options.presetData
		@fetch()

	addListeners: ->
		@listenTo @, 'services:ready', @afterFetch

	setInitPresetData: (data) ->
		if data?

			if data.delivery_date
				deliveryDate = datetimeParsers.parseToMoment data.delivery_date
				deliveryDateType = if deliveryDate.isAfter(Moment(), 'day') then 'date' else 'now'
			else
				deliveryDate = Moment()
				deliveryDateType = 'now'

			@set
				resetToFirstStep: data.resetToFirstStep ? 'recalculate'
				provider_id: data.provider_id
				client_id: data.client_id

				current_service_id: data.service_id
				current_session_id: data.session_id

				recipient_name: data.recipient_name ? ''
				recipient_email: data.recipient_email ? ''

				message: data.message ? ''

				delivery_date_type: deliveryDateType
				delivery_date: deliveryDate
		@

	fetch: ->
		@fetchServices()
		# fetch personal details

	# get provider services
	fetchServices: ->
		@trigger 'loading'
		@xhr = $.ajax
			url: getApiRoute 'ajax-provider-services',
				providerId: @get('provider_id')
			method: 'get'
			success: (response) =>
				@set 'services', @formatServices(response.data)
				@trigger 'services:ready'
				@trigger 'ready'

	formatServices: (services) ->
		formatted = []
		_.each services, (service) ->
			# we care only about services with gift certificates
			if service.certificates
				sessions = _.where service.sessions, {active: 1}
				return if sessions.length is 0
				formatted.push
					service_id: service.service_id
					name: service.name
					sessions: sessions
					locations: service.locations
		serviceFormatter.detailedServicesName formatted
		formatted

	getService: (serviceId) -> _.findWhere @get('services'), service_id: serviceId

	getSession: (sessionId) ->
		sessionObject = null
		_.each @get('services'), (service) =>
			_.each service.sessions, (session) =>
				if session.session_id is sessionId
					sessionObject = session
		sessionObject

	getCurrentService: -> @getService @get('current_service_id')

	getCurrentSession: -> @getSession @get('current_session_id')

	afterFetch: ->
		if @getCurrentService()
			unless @getCurrentSession()
				@set 'current_session_id', _.first(service.sessions).session_id
			@trigger 'change:current_service_id'
			return
		service = _.first @get('services')
		@set 'current_service_id', service.service_id
		@set 'current_session_id', _.first(service.sessions).session_id
		@

	generateServiceSelect: -> @get('services')

	generateSessionSelect: ->
		currentService = @getCurrentService()

		opt_labels = []
		sessionWithOptions = {}

		if currentService
			_.each currentService.sessions, (item) ->
				price = Numeral(item.price.replace(',', '')).format(Formats.Price.Simple)
				opt_labels.push(price)
				sessionWithOptions['opt_labels'] = opt_labels
				sessionWithOptions[price] = [
					{
						duration: item.duration
						duration_text: HumanTime.minutes item.duration
						session_id: item.session_id
					}
				]
		sessionWithOptions

	getBookingData: ->
		currentService = @getCurrentService()
		currentSession = @getCurrentSession()
		{
			resetToFirstStep: @get('resetToFirstStep')
			provider_id: @get('provider_id')
			client_id: @get('client_id')
			services: @get('services')
			certificate:
				id: null
				service_id: currentService.service_id
				service_name: currentService.detailed_name
				session_id: currentSession.session_id
				duration: currentSession.duration
				price: currentSession.price
			recipient_name: @get('recipient_name')
			recipient_email: @get('recipient_email')
			message: @getMessage()
			order: null
			delivery_date: do =>
				if @get('delivery_date_type') is 'now'
					Moment().format('YYYY-MM-DD')
				else
					@get('delivery_date').format('YYYY-MM-DD')
		}

	getMessage: -> $("<div/>").html(@get('message')).text()

module.exports = Model

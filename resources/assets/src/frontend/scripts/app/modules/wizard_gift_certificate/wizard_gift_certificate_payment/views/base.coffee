Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Config = require 'app/modules/wizard_gift_certificate/config'
ToggleEl = require 'utils/toggle_el'

require 'backbone.stickit'

class View extends Backbone.View

	className: 'wizard_booking--payment'

	bindings: []

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@subViews = []

		Wizard?.model?.set 'proceedLabel', 'Confirm Purchase'
		Wizard?.model?.set 'backLabel', 'Change Your Order Details'
		Wizard?.model?.set 'isPaymentStep', true
		Wizard.view.toggleFooter true

		@updateSidebarData()
		@addListeners()

	preload: ->
		Wizard.loading()
		Wizard.view.centerPopup()
		$.when(
			@collections.paymentMethods.fetch()
		).then(
			=>
				@initTotals()
				@initPaymentMethods()
				@initPaymentDetails()
				@initCoupons()
				Wizard.view.centerPopup()
		).then(
			=>
				Wizard.ready()
		)

	updateSidebarData: ->
		Wizard.model.set 'additional.payment_info',
			title   : Config.Payment.SidebarInfo.Title
			content : Config.Payment.SidebarInfo.Content
		@

	addListeners: ->
		@listenTo Wizard, 'proceed', @proceed
		@listenTo Wizard, 'back', =>
			@removePaypalButtonFromFooter()
			Wizard.trigger 'prev_step'

		@listenTo @model, 'coupon:added coupon:removed', =>
			if @totalsView
				@$el.$totals.html @totalsView.render().el
		@

	render: ->
		@$el.html WizardGiftCertificatePayment.Templates.Base()
		@stickit()
		@cacheDom()
		@raiseGenericError null

		@preload()
		@

	initTotals: ->
		unless @totalsView
			@instances.push @totalsView = new WizardGiftCertificatePayment.Views.Totals
				baseView  : @
				baseModel : @model
		@$el.$totals.html @totalsView.render().el
		@

	initPaymentMethods: ->
		# set payment method after fetch
		if Wizard.model.get('data.payment_method')
			@model.set 'payment_method', Wizard.model.get('data.payment_method')
		else
			@model.set 'payment_method', @collections.paymentMethods.findWhere({enabled: true}).get('type')

		@instances.push @paymentMethodsView = new WizardGiftCertificatePayment.Views.PaymentMethods
			baseView  : @
			baseModel : @model
		@$el.$paymentMethods.html @paymentMethodsView.el
		@

	initPaymentDetails: ->
		@instances.push @paymentDetailsView = new WizardGiftCertificatePayment.Views.PaymentDetails
			baseView  : @
			baseModel : @model
		@$el.$paymentDetails.html @paymentDetailsView.el
		@

	initCoupons: ->
		@subViews.push @couponView = new WizardGiftCertificatePayment.Views.Coupon
			baseView  : @
			baseModel : @model
		@$el.$coupons.append @couponView.el

	proceed: ->
		$.when(
			@paymentDetailsView.processPaymentMethod()
		).then(
			(processed) =>
				return false if processed is false
				@book()
		).fail(
			(err) -> console.log err # do something on fail.
		)

	book: ->
		Wizard.loading()
		$.when(
			@model.createCertificate()
		).then(
			(res) =>
				Wizard.model.set 'data.certificate.id', res.data.id
				@model.createOrder()
		).then(
			(res) =>
				Wizard.ready()
				Wizard.model.set 'data.order', res?.data # save the order
				Wizard.model.set 'next_step', 'success' # force success step
				Wizard.trigger 'next_step'
		).fail(
			(err) =>
				Wizard.ready()
				@errorHandler(err)
		)

	cacheDom: ->
		@$el.$totals = @$('[data-payment-totals]')
		@$el.$paymentMethods = @$('[data-payment-methods]')
		@$el.$paymentDetails = @$('[data-payment-details]')
		@$el.$coupons = @$('[data-payment-coupons]')
		@$el.$error = @$el.find('[data-error]')
		@

	raiseGenericError: (message = null) ->
		if message
			@$el.$error.removeClass 'm-hide'
			@$el.$error.html "<p>#{message}</p>"
		else
			@$el.$error.addClass 'm-hide'
			@$el.$error.html ''
		@


	errorHandler: (err) ->
		if err.responseJSON?.errors?.error? and err.responseJSON.errors.error.messages[0] == "Provider not specified his CC"
			vexDialog.alert
				message: "Please be in touch with your professional to complete this booking."


	removePaypalButtonFromFooter: ->
		ToggleEl Wizard.view.$el.$proceed, true
		Wizard.view.$el.$footer.find("##{Config.Payment.PaymentMethods.ButtonId}").remove()
		@

	displayPaypalButtonInFooter: ->
		@removePaypalButtonFromFooter()

		ToggleEl Wizard.view.$el.$proceed, false
		@$paypalBtn = $('<div id="' + Config.Payment.PaymentMethods.ButtonId + '"></div>')
		Wizard.view.$el.$footer.append(@$paypalBtn)
		@



module.exports = View
#
#
# Backbone = require 'backbone'
# Cocktail = require 'backbone.cocktail'
# Mixins = require 'utils/backbone_mixins'
# CardsManager = require 'app/modules/cards_manager/cards_manager'
# Formats = require 'config/formats'
# Numeral = require 'numeral'
# Moment = require 'moment'
# HumanTime = require 'utils/human_time'
#
# require 'backbone.stickit'
#
# class View extends Backbone.View
#
# 	className: 'booking_payment wizard--block'
#
# 	bindings:
#
# 		'.booking_payment--price_total':
# 			observe: 'coupons'
# 			onGet: (val) ->
# 				total = Numeral(Wizard.model.get('data.certificate.price')).value()
# 				_.each val, (coupon) ->
# 					if coupon.type is 'percent'
# 						total = _.max [total - total * coupon.amount / 100, 0]
# 					else
# 						total = _.max [total - coupon.amount, 0]
#
# 				Numeral(total).format(Formats.Price.Default)
#
# 		'.booking_payment--invoice--subtotals':
# 			observe: ['price.hour']
# 			updateMethod: 'html',
# 			onGet: (val) ->
# 				"<b>#{Wizard.model.get('data.selected_time.service_name')} - #{Wizard.model.get('data.selected_time.duration')} min</b> #{Numeral(val[0]).format(Formats.Price.Default)}"
#
# 	initialize: ->
# 		Cocktail.mixin @, Mixins.Views.Base
# 		@subViews = []
#
# 		@listenTo Wizard, 'check_step', =>
# 			Wizard.model.set('next_step', 'confirmation')
# 			if @cardsManager.view.isFormOpened
# 				@cardsManager.view.cardForm.submitForm()
# 			else if @validateExistingCard()
# 				@model.save()
#
# 		@listenTo window.CardsManager, 'card:added', => @model.save()
# 		@listenTo @model, 'coupon:loading', => @showInvoiceLoading()
# 		@listenTo @model, 'coupon:ready', =>
# 			@renderDiscounts()
# 			@removeCouponInput()
# 			@hideInvoiceLoading()
#
# 		@listenTo @model, 'coupon:removed', =>
# 			@renderDiscounts()
# 			@initCoupon()
#
# 	render: ->
# 		@$el.html WizardGiftCertificatePayment.Templates.Base
# 			certificate:
# 				duration: HumanTime.minutes Wizard.model.get('data.certificate.duration')
# 				service_name: Wizard.model.get('data.certificate.service_name')
# 				price: Numeral(Wizard.model.get('data.certificate.price')).format(Formats.Price.Default)
#
# 		@stickit()
# 		@cacheDom()
# 		@initCards()
# 		@initCoupon()
# 		@renderDiscounts()
# 		@raiseGenericError null
#
# 		return @
#
# 	validateExistingCard: ->
# 		card = @cardsManager.view.collection.get @model.get('credit_card_id')
# 		return false unless card
#
# 		year = +card.get('exp_year')
# 		month = +card.get('exp_month') + 1
#
# 		date = Moment.utc([year, month, 1, 0, 0, 0])
# 		now = Moment.utc()
#
# 		return true if now.isSameOrBefore date
#
# 		@raiseGenericError 'Selected card has expired'
# 		false
#
# 	cacheDom: ->
# 		@$el.$cards = @$('.booking_payment--col.m-credit_card .wizard--block_container')
# 		@$el.$items = @$('.booking_payment--invoice--items')
# 		@$el.$subtotals = @$('.booking_payment--invoice--subtotals')
# 		@$el.$total = @$('.booking_payment--invoice--total')
# 		@$el.$discount = @$('.booking_payment--discount', @$el)
# 		@$el.$invoice = @$('.booking_payment--invoice', @$el)
# 		@$el.$invoice.$loading = @$('.loading_overlay', @$el.$invoice.$loading)
# 		@$el.$error = @$el.find('[data-error]')
#
# 		return @
#
# 	initCards: ->
# 		@subViews.push @cardsManager = new CardsManager.App()
#
# 		@$el.$cards.append @cardsManager.view.render().el
#
# 		@addCardsBindings()
#
# 		return @
#
# 	addCardsBindings: ->
# 		@listenTo @cardsManager.model, 'change:card_id', (model, val) =>
# 			@model.set('credit_card_id', val)
# 			Wizard.model.set('credit_card_id', val)
# 			@raiseGenericError null
#
# 		@listenTo CardsManager, 'card:edit', =>
# 			@raiseGenericError null
# 			@
#
# 	checkCards: (collection) ->
# 		if collection.length
# 			Wizard.model.set('canGoAhead', true)
# 		else
# 			Wizard.model.set('canGoAhead', false)
#
# 	initCoupon: ->
# 		unless @model.get('coupons')?.length >= 2 or @coupon?
# 			@subViews.push @coupon = new WizardGiftCertificatePayment.Views.CouponForm
# 				baseView: @
# 				model: @model
#
# 			@$el.$subtotals.before @coupon.render().el
#
# 	renderDiscounts: ->
# 		coupons = @model.get('coupons')
# 		if coupons and coupons.length
# 			@$el.$discount.removeClass('m-hide').html WizardGiftCertificatePayment.Templates.Discount
# 				coupons: coupons
# 				multiple: coupons.length > 1
#
# 			$removeCoupon = @$('.booking_payment--discount--remove').removeClass 'm-hide'
# 			$removeCoupon.on 'click', (e) =>
# 				e.preventDefault()
# 				@model.removeCoupon()
#
# 			@$el.$subtotals.removeClass('m-hide')
#
# 		else
# 			@$el.$discount.html('').addClass('m-hide')
# 			@$el.$subtotals.addClass('m-hide')
# 		@
#
#
# 	showInvoiceLoading: ->
# 		@$el.$invoice.$loading.removeClass 'm-hide'
#
# 	hideInvoiceLoading: ->
# 		@$el.$invoice.$loading.addClass 'm-hide'
#
# 	removeCouponInput: (force) ->
# 		if @model.get('coupons')?.length >= 2 or force
# 			@coupon.close()
# 			@coupon = null
#
# 		@initCoupon() if force
#
# 	raiseGenericError: (message = null) ->
# 		if message
# 			@$el.$error.removeClass 'm-hide'
# 			@$el.$error.html "<p>#{message}</p>"
# 		else
# 			@$el.$error.addClass 'm-hide'
# 			@$el.$error.html ''
# 		@
#
#
# module.exports = View

Backbone = require 'backbone'
getApiRoute = require 'hp.api'
vexDialog = require 'vexDialog'

class Model extends Backbone.Model

	save: ->
		Wizard.loading()
		$.ajax
			url: getApiRoute('api-order-book', {
				orderId: Wizard.model.get('data.order.id')
			})
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: (res) =>
				Wizard.ready()
				Wizard.trigger 'next_step'
			error: (err) ->
				Wizard.ready()
				if err.responseJSON?.errors?.error? and err.responseJSON.errors.error.messages[0] == "Provider not specified his CC"
					vexDialog.alert
						message: "Please be in touch with your professional to complete this booking."
		return @

module.exports = Model

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Cocktail = require 'backbone.cocktail'
Formats = require 'config/formats'
Numeral = require 'numeral'
HumanTime = require 'utils/human_time'


require 'backbone.stickit'

class View extends Backbone.View

	className: 'wizard_gift_certificate_info'

	events:
		'click .wizard_gift_certificate_info--fake_step': 'passFakeStep'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@hideOnMobile @$el
		@bind()
		@addStickit()
		@toggleVisibility()
		@maybeSetAdditionalInfo()

		@render()

	bind: ->
		@listenTo Wizard, 'next_step', @maybeSetAdditionalInfo
		@listenTo Wizard, 'step:changed', @toggleVisibility

	addStickit: ->
		@bindings =
			'[data-service-name]': 'additional.service_name'
			'[data-duration]': 'additional.duration'
			'[data-price]': 'additional.price'

	render: ->
		@$container.find('.wizard_gift_certificate_info').remove()
		@$el.html GiftCertificateInfo.Templates.Base()
		@$el.prependTo @$container

		@cacheDom()
		@delegateEvents()
		@stickit()
		@

	cacheDom: ->
		@$wizardContent = $('.wizard--content')
		@$fakeStepButton = $('.wizard_gift_certificate_info--fake_step', @$el)

	hideOnMobile: ($el) -> $el.addClass('m-mobile_hide').removeClass('m-mobile_show')
	showOnMobile: ($el) -> $el.removeClass('m-mobile_hide').addClass('m-mobile_show')

	passFakeStep: ->
		$(window).scrollTop(0) # going to the top like after page reload
		@hideOnMobile(@$el)
		@hideOnMobile(@$fakeStepButton)
		@showOnMobile(@$wizardContent)

	# set additional information
	maybeSetAdditionalInfo: ->
		Wizard.model.set
			additional:
				duration: HumanTime.minutes Wizard.model.get('data.certificate.duration')
				price: Numeral(Wizard.model.get('data.certificate.price')).format(Formats.Price.Default)
				service_name: Wizard.model.get('data.certificate.service_name')

	toggleVisibility: (step) ->
		if step?
			if step is 'certificate_select'
				@$container.addClass('m-hide')
			else
				@$container.removeClass('m-hide')

module.exports = View

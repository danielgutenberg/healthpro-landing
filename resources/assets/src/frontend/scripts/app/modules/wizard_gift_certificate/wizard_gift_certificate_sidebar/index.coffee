Handlebars = require 'handlebars'

window.GiftCertificateInfo = GiftCertificateInfo =
	Views:
		Base: require('./views/base')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

class GiftCertificateInfo.App
	constructor: (options) ->
		@view = new GiftCertificateInfo.Views.Base
			model: options.model
			$container: options.$container

module.exports =  GiftCertificateInfo

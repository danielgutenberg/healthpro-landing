debugLog = require 'utils/debug_log'

Abstract = require 'app/modules/wizard/router/router'

GiftCertificateSelect = require 'app/modules/wizard_gift_certificate/wizard_gift_certificate_select'
GiftCertificatePayment = require 'app/modules/wizard_gift_certificate/wizard_gift_certificate_payment'
GiftCertificateSuccess = require 'app/modules/wizard_gift_certificate/wizard_gift_certificate_success'
CreateProfile = require 'app/modules/wizard_booking/wizard_booking_create_profile'
SwitchProfile = require 'app/modules/wizard_booking/wizard_booking_switch_profile'

isMobile = require 'isMobile'

class Router extends Abstract

	routes :
		'wizard/gift_certificate/profile_client_create'       : 'clientCreateStep'
		'wizard/gift_certificate/profile_client_choice'       : 'clientChoiceStep'
		'wizard/gift_certificate/certificate_select'          : 'selectGiftCertificateStep'
		'wizard/gift_certificate/payment'                     : 'paymentStep'
		'wizard/gift_certificate/success'                     : 'successStep'

	initialize: (options) ->
		super

		@wizardModel = options.model
		@wizardView = options.view

		@$dom =
			container : @wizardView.$el.$container
			content   : @wizardView.$el.$content
			sidebar   : @wizardView.$el.$sidebar

		# array for all inited instances
		@instances = []

		@bind()
		@start()

	updateScrollBar  : -> @
	destroyScrollBar : -> @
	initScrollBar    : -> @

	hideSidebar: ->
		@$dom.sidebar.addClass 'm-hide'

	showSidebar: ->
		@$dom.sidebar.removeClass 'm-hide'

	start: ->
		@openStep '' if @wizardView.place is 'page' # dirty hack to start app on page
		@openStep @wizardModel.get('current_step.slug')

	openStep: (step) ->
		debugLog('open step ' + step, 'gift_certificate.router')
		@navigate "wizard/gift_certificate/#{step}", {trigger: true, replace: true}

		# temporapy, we should open page version of wizard on mobile
		if @wizardView.place is 'popup' and isMobile.any
			window.location.reload()

	selectGiftCertificateStep: ->
		@hideSidebar()
		@addGaTag 'wizard.gift_certificate.select'

		@wizardModel.set 'canGoAhead', false
		@instances.push certificateSelect = new GiftCertificateSelect.App()

		@$dom.content.html certificateSelect.view.render().el
		Wizard.trigger 'step:changed', 'certificate_select'

	paymentStep: ->
		@showSidebar()
		@addGaTag 'wizard.gift_certificate.payment'

		@wizardModel.set 'canGoAhead', true
		@instances.push payment = new GiftCertificatePayment.App()

		@$dom.content.html payment.view.render().el
		Wizard.trigger 'step:changed', 'payment'

	successStep: ->
		@hideSidebar()
		@addGaTag 'wizard.gift_certificate.success'

		@wizardModel.set 'canGoAhead', true
		@wizardModel.set 'isSuccessStep', true
		@instances.push success = GiftCertificateSuccess.App()

		@$dom.content.html success.view.render().el
		Wizard.trigger 'step:changed', 'success'

	clientCreateStep: ->
		@hideSidebar()
		@wizardModel.set 'canGoAhead', true
		@instances.push createProfile = new CreateProfile.App()

		@$dom.content.html createProfile.view.render().el
		Wizard.trigger 'step:changed', 'client_create'

	clientChoiceStep: ->
		@hideSidebar()
		@wizardModel.set 'canGoAhead', false
		@instances.push switchProfile = new SwitchProfile.App()

		@$dom.content.html switchProfile.view.render().el
		Wizard.trigger 'step:changed', 'client_choice'

module.exports = Router

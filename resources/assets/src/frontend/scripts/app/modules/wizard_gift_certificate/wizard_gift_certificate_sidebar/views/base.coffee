Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Formats = require 'config/formats'
Numeral = require 'numeral'
Moment = require 'moment'

require 'backbone.stickit'

class View extends Backbone.View

	className: 'wizard_booking--sidebar_info'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addStickit()
		@render()

	addStickit: ->
		@bindings =
			'[data-appointment-service-name]':
				observe: 'data.certificate.service_name'

			'[data-appointment-duration]':
				observe: 'data.certificate.duration'
				onGet: (val) ->
					return '' unless val
					val

			'[data-appointment-price]':
				updateMethod : 'html'
				observe      : 'data.selected_time'
				onGet: (val) ->
					if val?
						html = "<strong>#{Numeral(val.total_price).format(Formats.Price.Default)}</strong>"
						if val.package
							html +=" (#{val.package.number_of_visits} sessions x #{val.package.formatted_duration})"
						html

			'[data-appointment-recipient-name]': 'data.recipient_name'
			'[data-appointment-recipient-email]': 'data.recipient_email'

			'[data-appointment-message]': 'data.message'

			'[data-appointment-delivery-date]':
				observe: ['data.delivery_date']
				updateMethod: 'html'
				onGet: (val) =>
					unless val[0]
						return "Select Time"
					else
						date = Moment(val[0])
						if Moment().diff(date, 'days')
							dateFormatted = date.format('MMMM D, YYYY')
						else
							dateFormatted = 'Now'
						"<strong>#{dateFormatted}</strong>"

			'[data-payment-info]':
				classes:
					'm-hide':
						observe: 'additional.payment_info'
						onGet: (val) -> if val then false else true

			'[data-payment-info-title]':
				updateMethod: 'html'
				observe: 'additional.payment_info.title'

			'[data-payment-info-content]':
				updateMethod: 'html'
				observe: 'additional.payment_info.content'
		@

	render: ->
		@$container.find('.wizard_booking--sidebar_info').remove()
		@$el.html GiftCertificateInfo.Templates.Base()
		@$el.appendTo @$container

		@delegateEvents()
		@stickit()
		@

module.exports = View

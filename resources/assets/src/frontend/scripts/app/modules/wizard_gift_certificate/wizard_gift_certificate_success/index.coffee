Handlebars = require 'handlebars'

# list of all instances
window.WizardGiftCertificateSuccess = WizardGiftCertificateSuccess =
	Views:
		Base: require('./views/base')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

class WizardGiftCertificateSuccess.App
	constructor: ->
		@view = new WizardGiftCertificateSuccess.Views.Base {}
		return {
			view: @view
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports =  WizardGiftCertificateSuccess

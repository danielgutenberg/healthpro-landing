Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Numeral = require 'numeral'
Formats = require 'config/formats'
HumanTime = require 'utils/human_time'
Nl2Br = require 'utils/nl2br'
Moment = require 'moment'

class View extends Backbone.View

	className: 'booking_confirm booking--block'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@listenTo Wizard, 'submit_wizard', => @model.save()

	render: ->
		@$el.html WizardGiftCertificateConfirmation.Templates.Base @getTemplateData()
		@

	getTemplateData: ->
		discounts = null
		if Wizard.model.get('data.order.line_items.coupons')
			discounts = Wizard.model.get('data.order.line_items.coupons').map (coupon) => {
				amount: @formatDiscount(coupon.amount, coupon.payload.type)
				description: coupon.payload.description
			}

		deliveryDate = Moment(Wizard.model.get('data.delivery_date'), 'YYYY-MM-DD')
		data =
			discounts: discounts
			duration: HumanTime.minutes Wizard.model.get('data.certificate.duration')
			service_name: Wizard.model.get('data.certificate.service_name')
			price: Numeral(Wizard.model.get('data.certificate.price')).format(Formats.Price.Default)
			total: Numeral(Wizard.model.get('data.order.total')).format(Formats.Price.Default)
			recipient_name: Wizard.model.get('data.recipient_name')
			recipient_email: Wizard.model.get('data.recipient_email')
			message: Nl2Br Wizard.model.get('data.message')
			delivery_now: deliveryDate.isSame(Moment(), 'day')
			delivery_date: deliveryDate.format('MMMM DD, YYYY')

		data

	formatDiscount: (amount, type) ->
		switch type
			when 'fixed'
				'- ' + Numeral(amount).format(Formats.Price.Default)
			when 'percent'
				"- #{amount}%"
			else
				amount

module.exports = View

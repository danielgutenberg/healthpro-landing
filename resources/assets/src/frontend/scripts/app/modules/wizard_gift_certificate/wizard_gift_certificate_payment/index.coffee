Backbone = require 'backbone'
Handlebars = require 'handlebars'

window.WizardGiftCertificatePayment = WizardGiftCertificatePayment =
	Views:
		Base           : require('./views/base')
		Coupon         : require('./views/coupon')
		Totals         : require('./views/totals')
		PaymentMethods : require('./views/payment_methods')
		PaymentDetails : require('./views/payment_details')
	Models:
		Base: require('./models/base')
	Collections :
		PaymentMethods : require('./collections/payment_methods')
	Templates:
		Base           : Handlebars.compile require('text!./templates/base.html')
		Coupon         : Handlebars.compile require('text!./templates/coupon.html')
		Totals         : Handlebars.compile require('text!./templates/totals.html')
		PaymentMethods : Handlebars.compile require('text!./templates/payment_methods.html')
		PaymentDetails : Handlebars.compile require('text!./templates/payment_details.html')

# events bus
_.extend WizardGiftCertificatePayment, Backbone.Events

class WizardGiftCertificatePayment.App
	constructor: ->
		@model = new WizardGiftCertificatePayment.Models.Base()
		@collections =
			paymentMethods : new WizardGiftCertificatePayment.Collections.PaymentMethods()
		@view = new WizardGiftCertificatePayment.Views.Base
			model: @model
			collections : @collections

		{
			model: @model
			view: @view
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = WizardGiftCertificatePayment

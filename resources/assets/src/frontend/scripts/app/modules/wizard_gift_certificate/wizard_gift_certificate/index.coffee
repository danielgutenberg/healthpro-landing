Abstract = require 'app/modules/wizard/abstract'

GiftCertificateWizardView = require('./views/base')
GiftCertificateWizardModel = require('./models/base')
GiftCertificateWizardRouter = require('./router/router')

###
  GIFT CERTIFICATE WIZARD PRIVATE CLASS
  AVAILABLE THROUGH THE SINGLETON PATTERN
###
class GiftCertificateWizard extends Abstract

	inited: false

	defaults: ->
		{
			bookingData: null
			el: $('[data-wizard="gift_certificate"]')
			forcePage: false
		}

	init: (options = null) ->
		if @inited
			console.error 'GiftCertificateWizard has already been inited'
			return @

		@setOptions options if options

		@addListeners()

		@model = new GiftCertificateWizardModel
			data: @options.bookingData
			wizard_name: 'gift_certificate'

		@view = new GiftCertificateWizardView
			model: @model
			el: @options.el

		@inited = true
		@

	initRouter: ->
		super
		@router = new GiftCertificateWizardRouter
			model: @model
			view: @view
		@

class GiftCertificateWizardSingleton
	instance = null

	@get: (options = {}) ->
		instance ?= new GiftCertificateWizard options

	@destroy: ->
		instance = null

# save the singleton instance to the wizard object
#window.Wizard = GiftCertificateWizardSingleton.get()

if $('.wizard_booking.m-page[data-wizard="gift_certificate"]').length
	window.Wizard = GiftCertificateWizardSingleton.get()
	window.Wizard.init()

module.exports = GiftCertificateWizardSingleton

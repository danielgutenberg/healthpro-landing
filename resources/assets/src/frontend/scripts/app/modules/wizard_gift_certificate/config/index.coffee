Config = require 'app/modules/wizard_booking/config'

Config.Payment.SidebarInfo =
		Title: 'Payment Details'
		Content: '''
			<p>Your credit card will be charged at time of purchase.</p>
			<p>The recipient of the gift certificate may book an appointment at any time.</p>
			<p>Please note that if the recipient cancels the appointment without providing 24 hours notice, no refund will be provided.</p>
		'''

module.exports = Config

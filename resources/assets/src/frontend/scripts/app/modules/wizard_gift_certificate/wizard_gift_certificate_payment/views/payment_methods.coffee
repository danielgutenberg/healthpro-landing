Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Config = require 'app/modules/wizard_gift_certificate/config'

require 'backbone.stickit'

class View extends Backbone.View

	className: 'wizard_booking--payment_methods'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@bind()
		@render()

	render: ->
		@$el.html WizardGiftCertificatePayment.Templates.PaymentMethods
			paymentMethods: @baseView.collections.paymentMethods.toJSON()
		@stickit @baseModel
		@

	bind: ->
		@bindings =
			'[name="payment_method"]': 'payment_method'
		@

module.exports = View

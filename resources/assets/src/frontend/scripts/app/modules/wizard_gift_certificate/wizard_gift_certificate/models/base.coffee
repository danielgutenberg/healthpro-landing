Abstract = require 'app/modules/wizard/models/base'
getApiRoute = require 'hp.api'

class Model extends Abstract

	defaults: ->
		defaults = super
		defaults.proceedLabel = 'Continue'
		defaults.backLabel = 'Back'

	initialize: ->
		@urls =
			initial: 'api-wizard-gift-certificate-initial'
			step: 'api-wizard-gift-certificate-step'

		@bind()

		# when booking data is passed save it first and then initialize wizard.
		$.when(
		).then(
			=> @saveWizardData()
		).then(
			=> @initWizard()
		)

	# rewrite wizard's default getInitialWizardData
	getInitialWizardData: ->
		data = do =>
			if @get('data.resetToFirstStep')?
				'resetToFirstStep=' + @get('data.resetToFirstStep')
			else {}

		Wizard.loading()
		Wizard.trigger 'initial_data:loading'

		@xhr = $.ajax
			method: 'get'
			url: getApiRoute(@urls.initial)
			data: data
			success: (response) =>
				@setData response
				Wizard.trigger 'initial_data:ready'
				@set 'isFirstLoad', false
				Wizard.ready()
				return

	getAdditionalWizardData: ->
		$.ajax
			url: getApiRoute('ajax-provider', {providerId: @get('data.provider_id')})
			method: 'get'
			success: (res) =>
				@set Wizard.formatter.formatBookingProfessionalFromResponse(res)
				@setTitle()
				Wizard.trigger 'additional_data:ready'

	# rewrite wizard's default resetWizardData
	# gets called on the confirmation step to make sure
	# if the user refreshes the page no current appointment data
	# will be passed to the first step
	resetWizardData: ->
		@set
			'data.resetToFirstStep': 'recalculate'
			'data.certificate': {}
			'data.recipient_name': ''
			'data.recipient_email': ''
			'data.message': ''
			'data.delivery_date': ''

		@saveWizardData()

	# rewrite wizard's default getCurrentStepData
	getCurrentStepData: ->
		Wizard.loading()
		@xhr = $.ajax
			method: 'get'
			url: getApiRoute(@urls.step, { step: @get('current_step.slug') })
			success: (response) =>
				@setData response
				if @get('data.resetToFirstStep')? and !@get('isFirstLoad')
					@set 'data.resetToFirstStep', null
				Wizard.trigger 'booking_data:ready'
				Wizard.ready()
				return

	getPresetData: ->
		data =
			resetToFirstStep: @get('data.resetToFirstStep')
			provider_id: @get('data.provider_id')
			client_id: @get('data.client_id')
			services: @get('data.services')
			service_id: @get('data.certificate.service_id') ? null
			session_id: @get('data.certificate.session_id') ? null
#			sender_name: @get('data.sender_name') ? ''
#			sender_email: @get('data.sender_email') ? ''
			recipient_name: @get('data.recipient_name') ? ''
			recipient_email: @get('data.recipient_email') ? ''
			message: @get('data.message') ? ''
			delivery_date: @get('data.delivery_date') ? null
		data

	setTitle: ->
		@set 'title_formatted', @get('title') + ' from ' + @get('additional.professional.name')

module.exports = Model

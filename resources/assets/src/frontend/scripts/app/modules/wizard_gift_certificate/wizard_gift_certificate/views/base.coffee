Abstract = require 'app/modules/wizard/views/main_view'
GiftCertificateInfo = require 'app/modules/wizard_gift_certificate/wizard_gift_certificate_sidebar'
isMobile = require 'isMobile'

Handlebars = require 'handlebars'
TemplateLayout = Handlebars.compile require('text!../templates/layout.html')

ToggleEl = require 'utils/toggle_el'
getRoute = require 'hp.url'

class View extends Abstract

	events:
		'click [data-wizard-close]': 'closeWizard'
		'click [data-wizard-proceed]': 'proceed'
		'click [data-wizard-back]': 'back'
		'keypress input' : 'detectKey'

	startup: ->
		super
		@canExit = true
		@

	bind: ->
		super
		@listenTo Wizard, 'initial_data:ready', => @initGiftCertificateInfo()
		@listenTo Wizard, 'stack:loading', => @allowExit(false)
		@listenTo Wizard, 'stack:ready', => @allowExit(true)
		@listenTo Wizard, 'additional_data:ready', => @setExitWizardParams()

		$(window).on 'keyup.wizard_booking', (e) =>
			if e.keyCode is 27 || e.which is 27
				@closeWizard()
		@

	addStickit: ->
		@bindings =
			'[data-wizard-title]':
				observe: 'title_formatted'

			':el':
				classes:
					'm-success': 'isSuccessStep'

			'[data-wizard-proceed]':
				observe: 'proceedLabel'
				classes:
					'm-green': 'isPaymentStep'
				attributes: [{
					name: 'disabled'
					observe: 'canGoAhead'
					onGet: (val) -> if val? and val then !val else true
				}]

			'[data-wizard-back]':
				observe: 'backLabel'
				classes:
					'm-hide':
						observe: ['previous_step', 'disableBack']
						onGet: (val) ->
							return true if val[1] is true
							return if val[0]? then false else true

	renderLayout: ->
		@$el.html TemplateLayout()
		@cacheDom()
		@centerPopup()

	cacheDom: ->
		super
		@$el.$close = @$el.find('[data-wizard-close]')
		@$el.$back = @$el.find('[data-wizard-back]')
		@$el.$proceed = @$el.find('[data-wizard-proceed]')
		@$el.$footer = @$el.find('[data-wizard-footer]')
		@$el.$sidebar = @$el.find('[data-wizard-sidebar]')
		@

	initGiftCertificateInfo: ->
		@subViews.push @giftCertificateInfo = new GiftCertificateInfo.App
			$container: @$el.$sidebar
			model: @model
		@giftCertificateInfo.view.render()
		@

	setExitWizardParams: ->
		return unless @place is 'page' and @exitView
		super

		url = @model.get('additional.professional.public_url')
		url = getRoute('dashboard-appointments') unless url

		@exitView.exitUrl = url
		@exitView.exitMsg = 'Are you sure you want to cancel booking?'
		@

	toggleFooter: (display = null) -> ToggleEl @$el.$footer, display

	allowExit: (allow = true) ->
		@$el.$close.prop 'disabled', !allow
		@canExit = allow
		@

	closeWizard: ->
		return unless @canExit
		$(window).off('keyup.wizard_gift_certificate')
		if @place is 'popup'
			@closeWizardPopup()
			return

		if @place is 'page'
			Wizard.loading()
			location.href = @model.get('additional.professional.public_url') ? getRoute('dashboard-appointments')
			return

	closeWizardPopup: ->
		Wizard.close()
		window.popupsManager.closePopup('popup_wizard')
		window.popupsManager.removePopup('popup_wizard')
		if !window.GLOBALS._PID? and Wizard.model.get('profileId')?
			location.reload()

	proceed: ->
		Wizard.trigger 'proceed', @model
		@

	back: ->
		Wizard.trigger 'prev_step'
		@

	detectKey: (e) ->
		# enter key
		if e.keyCode is 13 then @proceed()

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
parseHash = require 'utils/parse_hash'

require 'backbone.stickit'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@hash = parseHash false
		@instances = []

		@bind()
		@addListeners()
		@render()
		@cacheDom()
		@appendCertificates()

	addListeners: ->
		@listenTo @collections.certificates, 'data:loading', @showLoading
		@listenTo @collections.certificates, 'data:ready', @afterDataLoaded
		@

	afterDataLoaded: ->
		@hideLoading()
		@model.set
			'stat_sold'		: @collections.certificates.length
			'stat_redeemed'	: @collections.certificates.where({active: false}).length
			'stat_active'	: @collections.certificates.where({active: true}).length
		@

	bind: ->
		@bindings =
			'[data-stat-sold]': 'stat_sold'
			'[data-stat-redeemed]': 'stat_redeemed'
			'[data-stat-active]': 'stat_active'
		@

	render: ->
		@$el.html ProfessionalGiftCertificates.Templates.Base()
		@$container.html @$el
		@stickit()
		@

	appendCertificates: ->
		@instances.push @certificatesView = new ProfessionalGiftCertificates.Views.Certificates
			collections: @collections
			baseView: @

		@$el.$certificates.append @certificatesView.el
		@

	cacheDom: ->
		@$body = $('body')
		@$el.$certificates = @$el.find('[data-gift-certificates]')
		@$el.$filter = @$el.find('[data-gift-certificates-filter]')
		@$el.$loading = @$el.find('.loading_overlay')
		@

module.exports = View

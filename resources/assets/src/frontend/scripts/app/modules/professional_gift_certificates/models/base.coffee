Backbone = require 'backbone'

class Model extends Backbone.Model

	defaults:
		stat_sold: 0
		stat_redeemed: 0
		stat_active: 0

module.exports = Model

Handlebars = require 'handlebars'
require 'app/modules/booking_helper'

require '../../../../styles/modules/professional_gift_certificates.styl'

window.ProfessionalGiftCertificates = ProfessionalGiftCertificates =
	Config:
		DisplaySelectedWeekdays: false
		DateFormat: 'dddd, MMMM Do, YYYY'
		TimeFormat: 'HH:mm'

	Views:
		Base: require('./views/base')
		Certificates: require('./views/certificates')
		Certificate: require('./views/certificate')

	Models:
		Base: require('./models/base')
		Certificate: require('./models/certificate')

	Collections:
		Certificates: require('./collections/certificates')

	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')
		Certificates: Handlebars.compile require('text!./templates/certificates.html')
		Certificate: Handlebars.compile require('text!./templates/certificate.html')

class ProfessionalGiftCertificates.App
	constructor: (options) ->
		@collections =
			certificates: new ProfessionalGiftCertificates.Collections.Certificates [],
				model: ProfessionalGiftCertificates.Models.Certificate

		@model = new ProfessionalGiftCertificates.Models.Base()

		_.extend options,
			collections: @collections
			model: @model

		@view = new ProfessionalGiftCertificates.Views.Base options

	close: ->
		@view.close?()
		@view.remove?()

if $('[data-professional-gift-certificates]').length
	new ProfessionalGiftCertificates.App
		type: 'dashboard'
		$container: $('[data-professional-gift-certificates]')

module.exports = ProfessionalGiftCertificates

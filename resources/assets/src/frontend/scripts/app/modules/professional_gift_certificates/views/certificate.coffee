Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
HumanTime = require 'utils/human_time'
getRoute = require 'hp.url'

class View extends Backbone.View

	tagName: 'tr'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@render()

	render: ->
		console.log @getTemplateData()

		@$el.html ProfessionalGiftCertificates.Templates.Certificate @getTemplateData()
		@trigger 'rendered'
		@

	getTemplateData: ->
		{
			serviceName: @model.get('service').name
			duration: HumanTime.minutes @model.get('session').duration

			isReceiverClient: @model.get('client').is_provider_client
			receiverName: @model.get('client').full_name
			receiverAvatar: @model.get('client').avatar
			receiverUrl: getRoute('client-public-profile-url', {slug: @model.get('client').id})
			receiverMessageUrl: getRoute('dashboard-conversation', {profileId: @model.get('client').id})

			isSenderClient: @model.get('sender').is_provider_client
			senderName: @model.get('sender').full_name
			senderAvatar: @model.get('sender').avatar
			senderUrl: getRoute('client-public-profile-url', {slug: @model.get('sender').id})

			receivedDate: @model.get('purchased_date').format('MMMM D, YYYY')
			receivedTime: @model.get('purchased_date').format('hh:mm a')

			redeemedDate: @model.get('redeemed_date')?.format('MMMM D, YYYY')
			redeemedTime: @model.get('redeemed_date')?.format('hh:mm a')
		}

module.exports = View

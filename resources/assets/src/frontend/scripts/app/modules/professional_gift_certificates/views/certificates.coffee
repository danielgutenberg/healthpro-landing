Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
require 'datatables'
require 'tooltipster'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@perPage = 8
		@instances = []
		@dataTables = {}

		@addListeners()
		@render()
		@cacheDom()

	addListeners: ->
		@listenTo @collections.certificates, 'data:ready', @afterDataReady
		@

	render: ->
		@$el.html ProfessionalGiftCertificates.Templates.Certificates()
		@

	afterDataReady: ->
		@renderCertificates()
		@initDataTables()
		@initTooltips()
		@

	renderCertificates: ->
		@collections.certificates.forEach (model) => @renderCertificate model
		@

	renderCertificate: (model) ->
		@instances.push view = new ProfessionalGiftCertificates.Views.Certificate
			baseView: @baseView
			parentView: @
			model: model
			collections: @collections

		if model.get('active')
			@$el.$tableActive.$tbody.append view.el
		else
			@$el.$tableRedeemed.$tbody.append view.el
		@

	cacheDom: ->
		@$el.$tableActive = @$el.find('[data-table="active"]')
		@$el.$tableRedeemed = @$el.find('[data-table="redeemed"]')

		@$el.$tableActive.$tbody = @$el.$tableActive.find('tbody')
		@$el.$tableRedeemed.$tbody = @$el.$tableRedeemed.find('tbody')
		@

	initDataTables: ->
		@initDataTable @$el.$tableActive, true
		@initDataTable @$el.$tableRedeemed, false
		@

	initTooltips: ->
		@$el.find('[data-tooltip]').tooltipster
			theme: 'tooltipster-dark'
		@

	initDataTable: ($tableContainer, active = false) ->
		@dataTables[$tableContainer.data('table')] = $tableContainer.find('table').dataTable
			paging: true
			pagingType: "simple_numbers"
			searching: false
			lengthChange: false
			info: false
			iDisplayLength: @perPage
			destroy: true
			oLanguage:
				sEmptyTable: 'No certificates'
			aoColumnDefs: [
				bSortable: false
				aTargets: 'nosort'
			]
			fnDrawCallback: =>
				if @collections.certificates.where({active: active}).length <= @perPage
					$tableContainer.addClass 'm-no-paging'
				else
					$tableContainer.removeClass 'm-no-paging'
		@

module.exports = View

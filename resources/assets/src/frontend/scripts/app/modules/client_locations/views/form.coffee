Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	events:
		'submit': 'saveLocation'
		'click [data-location-cancel]': 'cancelForm'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@errors = []

		@modelDefaults = @model.toJSON()
		@isNew = if @model.get('id') then false else true

		@addListeners()
		@initValidation()
		@bind()
		@render()

		@cacheDom()

	addListeners: ->
		@listenTo @model, 'error:postal_code', => @raiseError 'postal_code', 'The location cannot be saved because the address is not supported'
		@listenTo @model, 'error:address', => @raiseError 'address', 'Address already exists'
		@listenTo @model, 'error:generic', (errorMessage) => @raiseGenericError errorMessage

	bind: ->
		@bindings =
			'input[name="name"]': 'name'
			'input[name="address"]':
				observe: 'full_address'
				initialize: ($el, model) =>
					pacSelectFirst($el[0])

					@autocomplete = new GMaps.gmaps.places.Autocomplete $el[0], {
						types: ['address']
						componentRestrictions:
							country: 'us'
					}
					GMaps.gmaps.event.addListener @autocomplete, 'place_changed', => @parseAutocomplete()

					$el.on 'input', (e) =>
						val = $el.val()
						newVal = val.replace(/[a-zA-Z]\d/g, (match) ->  match[0] + ' ' + match[1])
						unless val == newVal
							$el.val(newVal)
							GMaps.gmaps.event.trigger $el[0], 'focus', {}
				onSet: (val) =>
					# when we're typing the address we need to make sure it gets parsed
					# we can parse the address only firing 'place_changed' event on autocomplete
					@model.resetAddressFields()
					val

	parseAutocomplete: -> @model.parseAddressFields @autocomplete.getPlace()



	pacSelectFirst = (input) ->
		# store the original event binding function
		_addEventListener = if input.addEventListener then input.addEventListener else input.attachEvent

		addEventListenerWrapper = (type, listener) ->
			# Simulate a 'down arrow' keypress when no pac suggestion is selected,
			# and then trigger the original listener
			if type == 'keydown'
				orig_listener = listener

				listener = (event) ->
					suggestion_selected = $('.pac-item-selected').length > 0
					if event.which == 13 and !suggestion_selected
						simulated_downarrow = $.Event('keydown',
							keyCode: 40
							which: 40)
						orig_listener.apply input, [ simulated_downarrow ]
					orig_listener.apply input, [ event ]

			# add the modified listener
			_addEventListener.apply input, [
				type
				listener
			]

		if input.addEventListener
			input.addEventListener = addEventListenerWrapper
		else if input.attachEvent
			input.attachEvent = addEventListenerWrapper

	saveLocation: (e) ->
		e?.preventDefault()
		return if @model.validate()
		updateName = true
		if @autocomplete.getPlace()

			# if address was edited, but not selected
			# using $('.pac-item') because .getPlace returns short names, while in the input we have long
			inputAddressClean = @$el.$locationAddressLine.val().trim().replace(/\s+/g, '')
			googleAddressClean = $('.pac-item').first().text().trim().replace(/\s+/g, '')
			if inputAddressClean == googleAddressClean
				GMaps.gmaps.event.trigger @autocomplete, 'place_changed'
			@doSaveLocation()
			updateName = false
		else
			# first predicted location
			service = new (GMaps.gmaps.places.AutocompleteService)
			service.getQueryPredictions { input: @$el.$locationAddressLine.val() }, (predictions, status) =>
				if status is GMaps.gmaps.places.PlacesServiceStatus.OK
					placeservice = new (GMaps.gmaps.places.PlacesService)(document.createElement('div'))
					placeservice.getDetails { placeId: predictions[0].place_id }, (place, status) =>
						if status is GMaps.gmaps.places.PlacesServiceStatus.OK
							if predictions[0].description.trim() == @$el.$locationAddressLine.val().trim() # only if prediction and input are the same
								@model.parseAddressFields place
								@doSaveLocation()
								updateName = false

							else @model.validate()
						else @model.validate()
				else @model.validate()

		if @model.get('initialName') != @model.get('name') and updateName
			@doSaveLocation()

	doSaveLocation: ->
		@raiseGenericError null

		@showLoading()
		$.when(
			@model.save()
		).then(
			=>
				@collections.locations.push @model if @isNew
				@hideLoading()
				@baseView.removePopup()
		).fail(
			=> @hideLoading()
		)

	cancelForm: ->
		@model.set @modelDefaults
		@baseView.removePopup()

	cacheDom: ->
		@$el.$error = @$el.find('[data-generic-error]')
		@$el.$loading = @$el.find('.loading_overlay')

		@$el.$locationAddressLine = @$el.find('input[name="address"]')

		@

	render: ->
		@$el.html ClientLocations.Templates.Form()
		@stickit()
		@

	initValidation: ->
		Backbone.Validation.bind @,
			valid: (view, attr) => @raiseError attr, null
			invalid: (view, attr, error) => @raiseError attr, error
		@

	raiseError: (attr, error) ->
		# show all address errors on the address field
		if @model.addressFields.indexOf(attr) isnt -1
			field = @$el.find('[name="address"]')
		else
			field = @$el.find('[name="' + attr + '"]')
		if error?
			field.parents('.field').addClass 'm-error'
			field.prev('.field--error').html error
			field.on 'focus.validation', ->
				field.parents('.field').removeClass('m-error')
				field.off('focus.validation')
		else
			field.parents('.field').removeClass 'm-error'
			field.prev('.field--error').html ''

	raiseGenericError: (message = null, type = 'error') ->
		if message
			@$el.$error.removeClass 'm-hide'
			@$el.$error.html "<div class='alert m-#{type} m-show'>#{message}</div>"
		else
			@$el.$error.addClass 'm-hide'
			@$el.$error.html ''


module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	className: 'popup client_locations_popup'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base
		@popupView = options.popupView

		if options.className
			@$el.addClass options.className

		if options.popupName
			@$el.attr 'data-popup', options.popupName
			@$el.attr 'id', "popup_#{options.popupName}"

		@render()
		@cacheDom()
		@appendView()

	render: ->
		@$el.html ClientLocations.Templates.Popup()

	cacheDom: ->
		@$el.$content = @$el.find('[data-popup-content]')

	appendView: ->
		@$el.$content.append @popupView.el

module.exports = View

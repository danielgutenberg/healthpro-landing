Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'
require 'backbone.stickit'

class View extends Backbone.View

	tagName: 'tr'

	events:
		'click [data-location-edit]': 'editLocation'
		'click [data-location-remove]': 'removeLocation'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@bind()
		@render()

	bind: ->
		@bindings =
			'[data-location-name]': 'name'
			'[data-location-address]': 'full_address'

	render: ->
		@$el.html ClientLocations.Templates.Location()
		@parentView.$el.$listing.append @$el
		@stickit()

	editLocation: ->
		@baseView.initEditLocation @model

	removeLocation: ->
		vexDialog.confirm
			message: ClientLocations.Config.Messages.remove
			callback: (value) =>
				return unless value
				@baseView.showLoading()
				$.when(
					@model.remove()
				).then(
					=>
						@baseView.hideLoading()
						@collections.locations.remove @model
						@parentView.subViews.splice @parentView.subViews.indexOf(@), 1
						@close()
				).fail(
					=> @baseView.hideLoading()
				)
		@

	showError: ->
		alert ClientLocations.Config.Messages.error

module.exports = View

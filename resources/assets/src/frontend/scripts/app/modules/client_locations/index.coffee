Handlebars = require 'handlebars'

require '../../../../styles/modules/client_locations.styl'

window.ClientLocations =
	Config:
		Messages:
			error: 'Something went wrong. Please reload the page and try again.'
			remove: 'Are you sure you want to remove this location?'
	Views:
		Base: require('./views/base')
		Locations: require('./views/locations')
		Location: require('./views/location')
		Form: require('./views/form')
		Popup: require('./views/popup')

	Collections:
		Locations: require('./collections/locations')
	Models:
		Location: require('./models/location')
	Templates:
		Base:  Handlebars.compile require('text!./templates/base.html')
		Locations:  Handlebars.compile require('text!./templates/locations.html')
		Location:  Handlebars.compile require('text!./templates/location.html')
		Form:  Handlebars.compile require('text!./templates/form.html')
		Popup:  Handlebars.compile require('text!./templates/popup.html')


class ClientLocations.App
	constructor: (options) ->
		GMaps.load().ready =>
			@collections =
				locations: new ClientLocations.Collections.Locations [],
					model: ClientLocations.Models.Location

			# assign collection to the model. used in validation
			ClientLocations.Models.Location.prototype.collection = @collections.locations

			_.extend options,
				collections: @collections

			@view = new ClientLocations.Views.Base options

	close: ->
		@view.close?()
		@view.remove?()

if $('[data-client-locations]').length
	new ClientLocations.App
		type: 'dashboard'
		$container: $('[data-client-locations]')

module.exports = ClientLocations

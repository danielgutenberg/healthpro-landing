Backbone = require 'backbone'
getApiRoute = require 'hp.api'
require 'backbone.validation'

class Model extends Backbone.Model

	addressFields: ['address', 'postal_code', 'country_code', 'city', 'province']

	defaults:
		id: null
		name: ''
		full_address: ''
		address: null
		postal_code: null
		country_code: null
		city: null
		province: null

	initialize: ->
		@initValidation()
		@formatFullAddress()

	initValidation: ->
		@validation =
			name:
				fn: (value, attr, computedState) =>
					return 'Please enter a name' unless value
					return 'Please enter a valid location address' unless @collection.isUnique computedState.id, attr, value
					return

			address:
				fn: (value, attr, computedState) ->
					address = computedState.address
					postal_code = computedState.postal_code
					country_code = computedState.country_code
					city = computedState.city

					if address? and postal_code? and country_code? and city?
						return false
					else
						return 'Please enter a valid location address'

	formatFullAddress: ->
		params = @toJSON()
		full_addresss = ''
		full_addresss += params.address if params.address
		full_addresss += ", #{params.city}" if params.city
		full_addresss += ", #{params.province}" if params.province
		full_addresss += " #{params.postal_code}" if params.postal_code
		full_addresss += ", #{params.country_code}" if params.country_code
		@set 'full_address', full_addresss
		@

	save: ->
		locationExists = if @get('id') then true else false

		# make sure we have the right full address when we're saving the model
		@formatFullAddress() if @get('full_address')

		params =
			name: @get 'name'
			address: @get 'address'
			postal_code: @get 'postal_code'
			country_code: @get 'country_code'
			_token: window.GLOBALS._TOKEN

		if locationExists
			method = 'put'
			route = getApiRoute('ajax-client-location', {locationId: @get('id'), clientId: 'me'})
		else
			method = 'post'
			route = getApiRoute('ajax-client-locations', {clientId: 'me'})

		$.ajax
			url: route
			method: method
			type: 'json'
			show_alerts: false
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify params
			success: (response) =>
				@set 'id', response.data.id unless locationExists
			error: (jqXHR) =>
				resp = jqXHR.responseJSON
				@trigger 'error:postal_code' if resp.errors?.postalCode? or resp.errors?.postal_code?
				@trigger 'error:address' if resp.errors?.address?
				@trigger 'error:generic', resp.errors.error.messages[0] if resp.errors?.error?.messages

	remove: ->
		$.ajax
			url: getApiRoute('ajax-client-location', {locationId: @get('id'), clientId: 'me'})
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: =>
				@set 'id', null
				@destroy()
			error: (error) =>
				@trigger 'ajax:error', error.responseJSON.errors?.error?.messages?[0]

	parseAddressFields: (googleAddressObj) ->
		@resetAddressFields()
		if  googleAddressObj?
			@set 'address', googleAddressObj.name
			for k, v of googleAddressObj.address_components
				switch v.types[0]
					when 'country'
						@set 'country_code', v.short_name.toUpperCase()
					when 'postal_code'
						@set 'postal_code', v.short_name.toUpperCase() if v.short_name?
					when 'administrative_area_level_1'
						@set 'province', v.short_name if v.short_name?
					when 'locality', 'sublocality_level_1'
						@set 'city', v.short_name if v.short_name?

	resetAddressFields: ->
		@set
			address: null
			postal_code: null
			country_code: null
			city: null
			province: null

	getFormattedAddressHtml: -> "<p>#{@get('address')}<br>#{@get('city')}, #{@get('postal_code')}<br>#{@get('country_code')}</p>"

module.exports = Model

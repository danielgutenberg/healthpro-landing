Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@bind()
		@render()
		@cacheDom()

	bind: ->
		@listenTo @collections.locations, 'add', @renderLocation
		@listenTo @collections.locations, 'update', @toggleEmptyMessage
		@listenTo @collections.locations, 'data:ready', @toggleEmptyMessage

	render: ->
		@$el.html ClientLocations.Templates.Locations()

	cacheDom: ->
		@$el.$listing = @$el.find('[data-listing]')
		@$el.$empty = $('<tr><td class="cards_list--table--empty" colspan="3"><p>Please add your location</p></td></tr>')

	renderLocation: (model) ->
		@subViews.push new ClientLocations.Views.Location
			parentView: @
			baseView: @baseView
			model: model
			collections: @collections

	toggleEmptyMessage: ->

		if @collections.locations.length
			@$el.$empty.remove()
		else
			@$el.$listing.append @$el.$empty

module.exports = View

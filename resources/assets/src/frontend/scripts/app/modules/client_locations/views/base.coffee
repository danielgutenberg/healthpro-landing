Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'click [data-location-add]': 'addLocation'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@instances = {}

		@bind()
		@render()
		@cacheDom()
		@appendLocations()

	bind: ->
		@listenTo @collections.locations, 'data:ready', @hideLoading

	render: ->
		@$container.html @$el.html(
			ClientLocations.Templates.Base()
		)

	appendLocations: ->
		@instances.locations = new ClientLocations.Views.Locations
			collections: @collections
			baseView: @

		@$el.$locations.append @instances.locations.el

	cacheDom: ->
		@$body = $('body')
		@$el.$locations = @$el.find('[data-locations]')
		@$el.$loading = @$el.find('.loading_overlay')

	addLocation: (e) ->
		e?.preventDefault()
		@initEditLocation new ClientLocations.Models.Location()

	removePopup: ->
		if @instances.popup
			_.each window.popupsManager.popups, (item) -> item.closePopup() if item.opened
			@instances.popup?.remove()
			@instances.popup = null
			window.popupsManager.popups = []

	appendPopup: (options) ->
		@instances.popup = new ClientLocations.Views.Popup options
		@$body.append @instances.popup.$el
		window.popupsManager.addPopup @instances.popup.$el
		window.popupsManager.openPopup options.popupName
		@instances.popup.$el.on 'popup:closed', => @removePopup()

	initEditLocation: (model) ->
		@removePopup()
		@appendPopup
			popupName: 'client_locations_popup'
			popupView: new ClientLocations.Views.Form
				collections: @collections
				baseView: @
				model: model

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
getUrl = require 'hp.url'
Maxlength = require 'framework/maxlength'

require 'backbone.validation'
require 'backbone.stickit'

class FormView extends Backbone.View

	className: 'professional_setup_wizard--form'

	initialize: (options)->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@bind()
		@initValidation()
		@render()
		@cacheDom()
		@toggleBusinessNameDisplay @model, @model.get('business_name')

	cacheDom: ->
		@$el.$businessNameDisplay = @$el.find('[name="business_name_display"]')
		@

	initValidation: ->
		Backbone.Validation.bind @,
			valid: (view, attr) => @raiseError attr, null
			invalid: (view, attr, error) => @raiseError attr, error

	raiseError: (attr, error) ->
		field = @$el.find("[name='#{attr}']")

		return unless field.length

		fieldParent = field.closest('.field, [data-userpic]')
		fieldError  = fieldParent.find('.field--error')

		if error
			fieldParent.addClass 'm-error'
			fieldError.html error
			field.on 'focus.validation', ->
				fieldParent.removeClass('m-error')
				field.off('focus.validation')
		else
			fieldParent.removeClass 'm-error'
			fieldError.html ''

	addListeners: ->
		if Wizard.model.get('isLastStep')
			@listenTo Wizard, 'submit_wizard', @saveProfile
		else
			@listenTo Wizard, 'check_step', @saveProfile
		@

		@listenTo @model, 'change:business_name', @toggleBusinessNameDisplay


	bind: ->
		@bindings =
			'[name="business_name"]': 'business_name'
			'[name="business_name_display"]':
				observe: 'business_name_display'
				onGet: (val) -> Boolean val
				onSet: (val) -> Boolean val
			'[name="about_self_background"]':
				observe: 'about_self_background'
				initialize:($el) ->
					@backgroundMaxLength = new Maxlength $el.parent()
					@backgroundMaxLength.checkLeftLength()

	render: ->
		@$el.html WizardProfessionalInformation.Templates.Profile()
		@baseView.$el.$main.append @$el
		@stickit()
		@

	saveProfile: ->
		return if @model.validate()

		@baseView.showLoading()
		$.when(
			@model.save()
		).then(
			=>
				$.when(
					@model.sendVerifyEmail()
				).then(
					=>
						if Wizard.model.get('isLastStep')
							window.location = getUrl('dashboard')
						else
							@baseView.hideLoading()
							Wizard.trigger 'next_step'
				).fail(
					=> @baseView.hideLoading()
				)
		).fail(
			=> @baseView.hideLoading()
		)

	toggleBusinessNameDisplay: (model, val) ->
		if val?.trim()
			@$el.$businessNameDisplay.attr 'disabled', false
		else
			@$el.$businessNameDisplay.attr('disabled', true).prop('checked', false)
		@


module.exports = FormView

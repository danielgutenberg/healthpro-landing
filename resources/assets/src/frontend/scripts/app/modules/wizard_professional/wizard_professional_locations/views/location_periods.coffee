Backbone = require 'backbone'

class View extends Backbone.View

	className: 'professional_setup_wizard--form--row'

	events:
		'click [data-periods-add]': 'addPeriodModel'

	initialize: (@options) ->
		@collection = @options.collection
		@parentModel = @model = @options.parentModel

		@subViews = []

		@bind()

	bind: ->
		@listenTo @collection, 'add', (model) => @addPeriodView model
		@listenTo @collection, 'remove', => @checkIfEmpty()
		@listenTo @, 'rendered', =>
			@cacheDom()
			@appendPeriods()

	render: ->

		@$el.html WizardProfessionalLocations.Templates.LocationPeriods()
		@trigger 'rendered'
		@

	cacheDom: ->
		@$el.$list = @$el.find('[data-periods-list]')

	checkIfEmpty: ->
		@addPeriodModel() unless @collection.length

	appendPeriods: ->
		@collection.forEach (model) => @addPeriodView model
		@checkIfEmpty()

	# create model in collection to trigger add event, to call render View
	addPeriodModel: ->
		@collection.push
			from: '09:00'
			until: '17:00'

	addPeriodView: (model) ->
		if model?.attributes?
			@subViews.push period = new WizardProfessionalLocations.Views.LocationPeriod
				model: model
				collection: @collection
			@$el.$list.append period.render().el

module.exports = View

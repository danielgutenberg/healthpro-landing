Handlebars = require 'handlebars'

ServiceTypesCollection 	= require('app/modules/professional_setup/setup_services/collections/service_types')
ServicesCollection		= require('app/modules/professional_setup/setup_services/collections/services')

window.WizardProfessionalServices =

	Views:
		Base: require('./views/base')
		AddService: require('./views/add_service')
		ServiceForm: require('./views/service_form')
		Sessions: require('./views/sessions')
		Session: require('./views/session')
		Packages: require('./views/packages')
		Package: require('./views/package')
		PackagesReccuring: require('./views/packages_reccuring')
		PackageReccuring: require('./views/package_reccuring')

	Models:
		Service: require('./models/service')

	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')
		AddService: Handlebars.compile require('text!./templates/add_service.html')
		Sessions: Handlebars.compile require('text!./templates/sessions.html')
		Session: Handlebars.compile require('text!./templates/session.html')
		Packages: Handlebars.compile require('text!./templates/packages.html')
		Package: Handlebars.compile require('text!./templates/package.html')
		PackagesReccuring: Handlebars.compile require('text!./templates/packages_reccuring.html')
		PackageReccuring: Handlebars.compile require('text!./templates/package_reccuring.html')

class WizardProfessionalServices.App
	constructor: (options) ->
		@collections =
			services: new ServicesCollection [],
				model: WizardProfessionalServices.Models.Service
			serviceTypes: new ServiceTypesCollection

		@view = new WizardProfessionalServices.Views.Base
			collections: @collections
			$container: options.$container
			profileId: options.profileId

	close: ->
		@view.close?()
		@view.remove?()

module.exports = WizardProfessionalServices

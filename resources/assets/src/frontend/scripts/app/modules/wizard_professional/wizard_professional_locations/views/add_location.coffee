Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

Select = require 'framework/select'
LocationsMixins = require 'app/modules/professional_setup/mixins/location_view'
ScrollTo = require 'utils/scroll_to_element'
NumericInput = require 'framework/numeric_input'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	className: 'professional_setup_wizard--location'

	events:
		'click .professional_setup--location_name input': 'selectInputContent'
		'click .professional_setup--location_address input': 'maybeSelectInputContent'
		'click input[name="timezone_current"]': 'toggleTimezone'
		'click [data-change-location-type-btn]': 'resetLocationType'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common, LocationsMixins
		@setOptions options

		@errors = []

		@model = new WizardProfessionalLocations.Models.Location()

		@bind()
		@addListeners()
		@render()
		@cacheDom()
		@appendPeriods()
		@toggleTimezone() # set initial users timezone

		# method from mixin
		@initValidation()

	addListeners: ->
		# show the right fields for different location types
		@listenTo @model, 'change:location_type_id', (model, val) =>

			@displayLocationFields()
			@maybeFillAddressField()
			@setLocationTypeName()
			@resetErrors()
			@baseView.toggleGoAhead if val then true else false
			@toggleForm if val then true else false

			@

		@listenTo @model, 'error:postal_code', => @raiseError 'postal_code', 'The area of the address you have entered is not supported'
		@listenTo @model, 'error:address', => @raiseError 'address_line', 'Address already exists'
		@listenTo @model, 'error:generic', @raiseGenericError

		@listenTo Wizard, 'check_step', @saveLocation

		@

	toggleForm: (display = true) ->
		if display
			@$el.addClass('m-location_type_set')
			@$el.$form.removeClass('m-hide')
			Wizard.view.$el.find('[data-wizard-footer]').addClass('m-location_type_set')
		else
			@$el.removeClass('m-location_type_set')
			@$el.$form.addClass('m-hide')
			@$el.find('input[type=radio]').prop('checked', false);
			Wizard.view.$el.find('[data-wizard-footer]').removeClass('m-location_type_set')
		@

	bind: ->
		@bindings =
			'input[name="location_name"]': 'name'
			'textarea[name="note"]':
				observe: 'note'
				onGet: (val, options) =>
					@initTextarea @$el.find(options.selector).parent(), val
					val

			'input[name="service_area"]':
				observe: 'service_area'
				onSet: (val) -> @area.parseValue(val) * 1
				initialize: ($el) ->
					@area = new NumericInput $el,
						numberOfDecimals: 0
						initialParse: false
						parseOnBlur: false


			'select[name="timezone"]':
				observe: 'timezone'
				selectOptions:
					collection: @baseView.tz.timezones()
					labelPath: 'full_name'
					valuePath: 'name'
				initialize: ($el) =>
					@timezoneSelect = new Select $el,
						minimumResultsForSearch: 0

			'input[name="address_line"]':
				observe: 'full_address'
				initialize: ($el, model, view) =>
					@pacSelectFirst($el[0])

					# removing old instances
					if @autocomplete?
						GMaps.gmaps.event.clearInstanceListeners @autocomplete
						$(".pac-container").remove();

					@autocomplete = new GMaps.gmaps.places.Autocomplete $el[0], {
						types: ['address']
						componentRestrictions:
							country: 'us'
					}
					GMaps.gmaps.event.addListener @autocomplete, 'place_changed', => @parseAutocomplete()

					@$('input[name="address_line"]').on 'input', (e) =>
						val = @$('input[name="address_line"]').val()
						newVal = val.replace(/[a-zA-Z]\d/g, (match) ->  match[0] + ' ' + match[1])
						unless val == newVal
							@$('input[name="address_line"]').val(newVal)
							GMaps.gmaps.event.trigger $el[0], 'focus', {}

				onSet: (val) =>
					# when we're typing the address we need to make sure it gets parsed
					# we can parse the address only firing 'place_changed' event on autocomplete
					@model.resetAddressFields()
					val

			'input[name="address_line_two"]': 'address_line_two'
			'input[name="address_label"]': 'address_label'

			'input[name="location_type"]':
				observe: 'location_type_id'
				onSet: (val) -> val * 1 # force integer

		@

	render: ->
		@$el.html WizardProfessionalLocations.Templates.AddLocation
			locationTypes: @collections.locationsTypes.toJSON()
		@stickit()
		@delegateEvents()

		@baseView.$el.$main.append @$el

	cacheDom: ->
		@$el.$form = @$el.find('[data-fields]')
		@$el.$fields = @$el.find('[data-field]')
		@$el.$availabilities = @$el.find('[data-edit-availabilities]')

		@$el.$locationAddressLine = @$el.find('input[name="address_line"]')
		@$el.$locationAddress = @$el.find('input[name="address"]')
		@$el.$locationCountryCode = @$el.find('input[name="country_code"]')
		@$el.$locationPostalCode = @$el.find('input[name="postal_code"]')
		@$el.$locationProvince = @$el.find('input[name="province"]')
		@$el.$locationCity = @$el.find('input[name="city"]')

		@$el.$error = @$el.find('[data-generic-error]')

		@$el.$timezoneSelect = @$el.find('[data-field-timezone]')

	appendPeriods: ->
		@periods = new WizardProfessionalLocations.Views.LocationPeriods
			collection: @model.get('availabilities')
			parentModel: @model
		@periods.render().$el.appendTo @$el.$availabilities

	saveLocation: ->
		if !@model.get('address')? # only if address was updated
			if @autocomplete.getPlace()
				# if address was edited, but not selected
				# using $('.pac-item') because .getPlace returns short names, while in the input we have long
				inputAddressClean = @$el.$locationAddressLine.val().trim().replace(/\s+/g, '')
				googleAddressClean = $('.pac-item').first().text().trim().replace(/\s+/g, '')
				if inputAddressClean == googleAddressClean
					GMaps.gmaps.event.trigger @autocomplete, 'place_changed'
				@doSaveLocation()
			else
				$.when(@getFirstPrediction(@$el.$locationAddressLine.val()) ).then( (status)=>
					unless status?
						@doSaveLocation()
					else
						# @raiseGenericError status

						# address format is valid but without GMaps prediction
						@raiseError('address_line', 'Please enter a valid location address')
				)
		else
			@doSaveLocation()

	doSaveLocation: ->
		@raiseGenericError null
		@validateData()

		if @errors.length
			@scrollToError()
			return

		@baseView.showLoading()
		$.when(
			@model.save()
		).then(
			=> @afterSave()
		).fail(
			=> @baseView.hideLoading()
		)

	afterSave: ->
		@baseView.hideLoading()
		Wizard.view.$el.find('[data-wizard-footer]').removeClass('m-location_type_set')
		Wizard.trigger 'next_step'

	validateData: ->
		@errors = []
		errors = @model.validate()
		@errors.push errors if errors?
		@model.get('availabilities').each (item) =>
			if !item.isEmpty()
				errors = item.validate()
				if errors?
					@errors.push errors

		@validatePeriodsOverlaps() unless @errors.length

	validatePeriodsOverlaps: ->
		if @model.get('availabilities').hasOverlaps()
			@errors.push 'overlap'
			@raiseGenericError "Oops! Looks like there's a scheduling conflict. The times you've selected overlap."
		else
			@raiseGenericError null

	setLocationTypeName: ->
		locationType = @model.getLocationType()
		switch locationType
			when "virtual"
				@model.set 'name', WizardProfessionalLocations.Config.locationNames.virtual
			when "home-visit"
				@model.set 'name', WizardProfessionalLocations.Config.locationNames.homeVisit
			else
				@model.set 'name', ''
		@

	resetLocationType: ->
		@model.set 'location_type_id', null
		@

	scrollToError: ->
		ScrollTo @$el.find('.field.m-error:first'), 30

module.exports = View

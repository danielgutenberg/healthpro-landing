Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: ->
		super
		@fetchLocations()

		@listenTo Backbone, 'professional_setup_service:location_added', @addLocationFromServices

	fetchLocations: ->
		$.ajax
			url: getApiRoute('ajax-location-get', { providerId: 'me' })
			method: 'get'
			type: 'json'
			data:
				service_area_unit: WizardProfessionalLocations.Config.serviceAreaUnit
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				response.data.forEach (item) => @push @formatLocation(item)
				@trigger 'data:ready'

	formatLocation: (item) ->
		location =
			id: item.id
			name: item.name
			availabilities: item.availabilities
			location_type_id: item.location_type_id
			service_area: item.service_area

		# for virtual location type the address is the note
		if item.address.address_id
			location.address = item.address.address
			location.address_id = item.address.address_id
			location.postal_code = item.address.postal_code
			location.country_code = item.address.country
			location.city = item.address.city
			location.province = item.address.province
			location.address_label = item.address.address_label
			location.timezone = item.timezone
			location.address_line_two = item.address.address_line_two
		else
			location.note = item.address.address
			location.timezone = item.timezone

		location

	addLocationFromServices: (item) ->
		@push item



	isUnique: (modelId, field, value) ->
		models = _.reject @toJSON(), {id: modelId}
		duplicate = _.where models,
			"#{field}": value
		duplicate.length is 0

module.exports = Collection

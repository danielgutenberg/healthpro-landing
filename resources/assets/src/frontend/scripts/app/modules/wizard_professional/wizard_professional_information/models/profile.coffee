Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		phone					: ''
		phone_verified			: false
		phone_verify_method 	: ''
		phone_verification_code : ''
		business_name			: ''
		business_name_display   : false
		canFinish               : false

	initialize: ->
		@initValidation()

	initValidation: ->
		@validation =
			avatar:
				required: true
				msg: 'Uploading a photo is required'

			first_name: [
				{
					required: true
					msg: 'Please enter First Name'
				},
				{
					pattern: /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/
					msg: 'Please enter a valid First Name'
				},
				{
					maxLength: 26
				}
			]
			last_name: [
				{
					required: true
					msg: 'Please enter Last Name'
				},
				{
					pattern: /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/
					msg: 'Please enter a valid Last Name'
				},
				{
					maxLength: 26
				}
			]

			phone: [
				{
					required: true
				}
				{
					pattern: /^\s*1?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/
					msg: 'Please enter phone number with the following format +1 (XXX) XXX-XXXX.'
				},
				{
					minLength: 10
					maxLength: 11
					msg: 'Please enter phone number with the following format +1 (XXX) XXX-XXXX.'
				}
			]

			about_self_background: [
				{
					minLength: 20
					msg: 'Your text is too short, please enter a minimum of 20 characters'
				}
				{
					maxLength: 1000
					msg: 'Your text is too long, please enter a maximum of 1000 characters'
				}
			]

	setData: (data) ->
		@set
			first_name: data.first_name
			last_name: data.last_name
			business_name: data.business_name
			business_name_display: data.business_name_display
			phone: data.phone ? ''
			about_self_background: data.about_self_background ? ''
			avatar: data.avatar
			avatar_original: data.avatar_original

	dataToSave: ->
		{
			first_name: @get('first_name')
			last_name: @get('last_name')
			business_name: @get('business_name')
			business_name_display: @get('business_name_display')
			phones: [@get('phone')]
			about_self_background: @get('about_self_background')
		}

	fetch: ->
		$.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				data = response.data
				# force + in the beginning. used to format phone nicely
				if data.phone?.number
					@set 'phone_id', data.phone.id
					@set 'phone_verified', data.phone.verified
					data.phone = data.phone.number.replace('+1 ', '')
				else
					data.phone = ''
				@setData data

	save: ->
		$.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: @getAjaxData @dataToSave()

	sendCode: ->
		if @get 'phone_id'
			return $.ajax
				url: getApiRoute('ajax-profile-edit-phone', {phoneId: @get('phone_id')})
				method: 'put'
				type: 'json'
				contentType: 'application/json; charset=utf-8'
				show_alerts: false
				data: @getAjaxData({number: @get('phone'), phone_verify_method: @get('phone_verify_method')})
		$.ajax
			url: getApiRoute('ajax-profile-add-phone')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			show_alerts: false
			data: @getAjaxData({phone: @get('phone'), phone_verify_method: @get('phone_verify_method')})
			success: (response) =>
			  @set 'phone_id', response.data.id

	resendCode: ->
		$.ajax
			url: getApiRoute('ajax-profile-resend-verify-phone', {phoneId: @get('phone_id')})
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			show_alerts: false
			data: @getAjaxData({phone_verify_method: @get('phone_verify_method')})

	verifyCode: ->
		$.ajax
			url: getApiRoute('ajax-profile-verify-phone', {phoneId: @get('phone_id')})
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			show_alerts: false
			data: @getAjaxData({code: @get('phone_verification_code')})

	sendVerifyEmail: ->
		return true if Wizard.model.get('email_verified')
		$.ajax
			url: getApiRoute('ajax-auth-send-activation')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			show_alerts: false
			data: @getAjaxData()

	getAjaxData: (data = {}) -> JSON.stringify $.extend {}, {_token: window.GLOBALS._TOKEN}, data

module.exports = Model

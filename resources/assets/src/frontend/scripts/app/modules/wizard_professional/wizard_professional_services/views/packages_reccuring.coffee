Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'click [data-packages-add]': 'createPackage'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@packages = []

		@bind()

	bind: ->
		@listenTo @parentView.model, 'change', => @toggleMaxWeeklyInfo()
		@listenTo @collection, 'add remove',   => @toggleMaxWeeklyInfo()

		@listenTo @collection, 'add', @renderPackage
		@listenTo @, 'rendered', =>
			@cacheDom()
			@renderAllPackages()

	renderAllPackages: ->
		@collection.models.forEach (model) => @renderPackage model

	cacheDom: ->
		@$el.$list = @$el.find('[data-packages-list]')
		@$el.$error = @$el.find('[data-packages-error]')

	render: ->
		@$el.html WizardProfessionalServices.Templates.PackagesReccuring()
		@trigger 'rendered'
		@

	createPackage: ->
		@collection.createPackage()
		$(document).trigger 'popup:center'

	renderPackage: (model) ->
		@parentView.raiseSessionsError null

		@packages.push item = new WizardProfessionalServices.Views.PackageReccuring
			model			: model
			collection		: @collection
			baseView		: @baseView
			parentView		: @

		item.render().$el.appendTo @$el.$list

	raiseError: (message, type = 'error') ->
		@parentView.raiseEditError message, @$el.$error, type

	toggleMaxWeeklyInfo: ->
		minAvailabilitiesPerWeek = _.reduce @parentView.model.get('location_ids'), (result, locationId) =>
			location = @baseView.collections.locations.get(locationId)
			if location
				min = _.min _.flatten(location.get('availabilities').pluck('days'))
				result = min if result is null or min < result
			result
		, null

		# no locations selected yet
		return if minAvailabilitiesPerWeek is null

		maxSessionsPerWeek = _.reduce @collection.models, (result, model) ->
			max = model.get('rules.max_weekly.value')
			result = max if max > result
			result
		, 0

		# unlimited locations
		return if maxSessionsPerWeek is 0

		if maxSessionsPerWeek > minAvailabilitiesPerWeek
			@raiseError 'Some locations have less available days then package visits per week', 'notice'
		else
			@raiseError null

module.exports = View

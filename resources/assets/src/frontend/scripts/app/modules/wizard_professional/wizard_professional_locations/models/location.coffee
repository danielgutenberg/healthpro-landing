Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	addressFields: ['address', 'postal_code', 'country_code', 'city', 'province']

	defaults:
		id: null
		name: ''
		full_address: ''
		address_label: ''
		address_line_two: ''
		address: null
		postal_code: null
		country_code: null
		city: null
		province: null
		location_type_id: null
		availabilities: null
		service_area: 15
		note: ''
		prepopulated_address: false

	initialize: (attributes = {}) ->
		super
		@initValidation()
		@initAvailabilities()
		@formatFullAddress()

	initValidation: ->
		@validation =
			address:
				fn: (value, attr, computedState) ->

					# for virtual office we don't need the address
					return false if @getLocationType() is 'virtual'

					address = computedState.address
					postal_code = computedState.postal_code
					country_code = computedState.country_code
					city = computedState.city

					if address and postal_code and country_code and city
						return false
					else
						return 'Please enter a valid location address'

			note:
				fn: (value) ->
					return false if @getLocationType() isnt 'virtual'
					return 'Your text is too short, please enter at least 10 characters' if value.length < 10
					return 'Your text is too long, please enter a maximum of 200 characters' if value.length >= 200
					return false

			service_area:
				fn: (value) ->
					return false if @getLocationType() isnt 'home-visit'
					return 'Please enter a number' unless _.isNumber value
					return false if value
					return 'Please enter radius'

	initAvailabilities: ->
		return if @get('availabilities') instanceof WizardProfessionalLocations.Collections.LocationPeriods
		@set 'availabilities', new WizardProfessionalLocations.Collections.LocationPeriods null,
			model: WizardProfessionalLocations.Models.LocationPeriod
			parentModel: @
			data: @get('availabilities')

		@listenTo @get('availabilities'), 'change', => @trigger 'change'

		@

	save: ->
		locationExists = if @get('id') then true else false
		$.when(@saveLocation(locationExists)).then(
			=> @saveAvailabilities(locationExists)
		)

	remove: ->
		$.ajax
			url: getApiRoute('ajax-location-remove',
				providerId: 'me'
				locationId: @get('id')
				)
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: =>
				@set('id', null)
				@destroy()

			error: (error) =>
				@trigger 'ajax:error', error.responseJSON.errors?.error?.messages?[0]

	saveLocation: (locationExists = false) ->

		# make sure we have the right full address when we're saving the model
		@formatFullAddress() if @get('full_address')
		@maybeFillAddressLabel() if @getLocationType() is 'home-visit'

		# we have different params for different location types
		params = @getLocationParamsForSave locationExists
		# add token
		params._token = window.GLOBALS._TOKEN

		if locationExists
			url = getApiRoute('ajax-location-update', {locationId: @get('id'), providerId: 'me'})
			method = 'put'
		else
			url = getApiRoute('ajax-location-add', {providerId: 'me'})
			method = 'post'

		$.ajax
			url: url
			method: method
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify params
			success: (response) =>
				@set 'id', response.data.id unless locationExists
				@set 'timezone', response.data.timezone
			error: (jqXHR) =>
				resp = jqXHR.responseJSON
				@trigger 'error:postal_code' if resp.errors?.postalCode? or resp.errors?.postal_code?
				@trigger 'error:address' if resp.errors?.address?
				@trigger 'error:generic', resp.errors.error.messages[0] if resp.errors?.error?.messages

	formatFullAddress: ->
		params = @toJSON()
		full_addresss = ''
		full_addresss += params.address if params.address
		full_addresss += ", #{params.city}" if params.city
		full_addresss += ", #{params.province}" if params.province
		full_addresss += " #{params.postal_code}" if params.postal_code
		full_addresss += ", #{params.country_code}" if params.country_code
		@set 'full_address', full_addresss
		@

	saveAvailabilities: (locationExists = false) ->
		url = getApiRoute('ajax-location-availabilities-update', {
			providerId: 'me'
			locationId: @get('id')
		})
		params =
			availabilities: @getAvailabilities()
			_token: window.GLOBALS._TOKEN
		if locationExists
			method = 'put'
		else
			method = 'post'

		$.ajax
			url: url
			method: method
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify params
			success: =>
				json = @toJSON()
				json.location_type = @getLocationType()
			error: (jqXHR) =>
				resp = jqXHR.responseJSON
				@trigger 'error:generic', resp.errors.error.messages[0] if resp.errors?.error?.messages

	getAvailabilities: ->
		returns = []
		@get('availabilities').each (model) =>
			if model.get('days')? and !model.isEmpty()
				_.each model.get('days'), (day) ->
					returns.push
						id: model.get('id')
						day: day
						from: model.get('from')
						until: model.get('until')
		returns

	parseAddressFields: (googleAddressObj) ->
		@resetAddressFields()
		if  googleAddressObj?
			@set 'address', googleAddressObj.name
			for k, v of googleAddressObj.address_components
				switch v.types[0]
					when 'country'
						@set 'country_code', v.short_name.toUpperCase()
					when 'postal_code'
						@set 'postal_code', v.short_name.toUpperCase() if v.short_name?
					when 'administrative_area_level_1'
						@set 'province', v.short_name if v.short_name?
					when 'locality', 'sublocality_level_1'
						@set 'city', v.short_name if v.short_name?

	resetAddressFields: ->
		@set
			address: null
			postal_code: null
			country_code: null
			city: null
			province: null

	# make sure we return a proper JSON object
	toJSON: ->
		attributes = _.clone @attributes
		if attributes.availabilities instanceof WizardProfessionalLocations.Collections.LocationPeriods
			attributes.availabilities = @attributes.availabilities.toJSON()
		attributes

	# custom clone method
	deepClone: -> new @constructor @toJSON()

	updateFromEdit: (args) ->
		@set args
		@initAvailabilities()

	# get editable fields for the view
	getLocationEditFields: ->
		switch @getLocationType()
			when "home-visit" then ['name', 'address_line', 'service_area', 'address_label']
			when "virtual" then ['name', 'note', 'timezone']
			# office is the default value
			else ['name', 'address_line', 'address_line_two']

	getLocationType: ->
		locationTypeId = @get('location_type_id')
		locationTypeId = 1 unless locationTypeId
		locationType = @locationsTypes.get(locationTypeId)
		return if locationType then locationType.get('type') else 'office'

	getLocationParamsForSave: (locationExists) ->
		locationType = @getLocationType()
		params =
			name: @get('name').trim()
			primary_location: 1

		if params.name is '' and locationType is 'office'
			params.name = WizardProfessionalLocations.Config.locationNames.office

		switch locationType
			when "virtual"
				params.address = @get 'note'
				params.timezone = @get 'timezone'
			else
				params.address = @get 'address'
				params.postal_code = @get 'postal_code'
				params.country_code = @get 'country_code'
				params.service_area = @get 'service_area'
				params.service_area_unit = WizardProfessionalLocations.Config.serviceAreaUnit
				params.address_line_two = @get 'address_line_two'

		if locationType == 'home-visit'
			params.address_label = @get 'address_label'

		# check if we're able to save it for existing locations
		unless locationExists
			params.location_type_id = @get 'location_type_id'

		params

	getFormattedAddressHtml: ->
		locationType = @getLocationType()
		html = ''
		switch locationType
			when "virtual"
				html += "<p>#{@get('note')}</p>"
			else
				html += "<p><strong>#{@get('address_label')}</strong></p>" if locationType is 'home-visit' and @get('address_label')
				html += "<p>#{@get('address')}"
				html += ", #{@get('address_line_two')}" if @get('address_line_two')
				html += "<br>#{@get('city')}, #{@get('postal_code')}<br>#{@get('country_code')}</p>"

		html

	isPrepopulatedAddress: -> @get('prepopulated_address')

	maybeFillAddressLabel: ->
		if @get('address_label')?.trim() is ''
			@set 'address_label', "The area of #{@get('postal_code')}"
		@

	getDefaultLocationTypePadding: ->
		switch @getLocationType()
			when 'home-visit' then 15
			else 0


module.exports = Model

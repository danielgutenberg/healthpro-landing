Backbone = require 'backbone'

class View extends Backbone.View

	className: 'professional_setup_wizard--panel'

	events:
		'click [data-hubspot-back]': 'back'

	initialize: (options) ->
		@baseView = options.baseView

		@render()
		@cacheDom()
		@initForm()

	render: ->
		html = """
			<div class="professional_setup_wizard--hubspot">
				<h3>Can't find a service you provide?</h3>
				<div id="service_form_container"></div>
				<button class='professional_setup_wizard--hubspot--back btn m-link m-hide' data-hubspot-back>Back</button>
			</div>
		"""
		@$el.append(html).appendTo @baseView.$el.$main

	cacheDom: ->
		@$el.$back = @$el.find('[data-hubspot-back]')

	initForm: ->
		require.ensure [], =>
			Hubspot = require 'framework/hubspot'
			Hubspot.initForm
				portalId: '496304'
				formId: 'ca58fd83-bb7f-4ac3-8bd5-04cef1a01bed'
				target: '#service_form_container'
				submitButtonClass: 'btn'
				inlineMessage: 'Thank you for submitting the service, your request has been sent for approval'
				onFormReady: =>
					@$el.$back.removeClass 'm-hide'

	back: ->
		@baseView.initAddService()

module.exports = View

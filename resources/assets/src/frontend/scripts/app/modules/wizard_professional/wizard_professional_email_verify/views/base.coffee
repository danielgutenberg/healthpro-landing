Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
getUrl = require 'hp.url'

require 'backbone.stickit'

class View extends Backbone.View

	className: 'professional_setup_wizard--inner'

	events:
		'click [data-resend-email]'	: 'resendEmail'
		'click [data-change-email]'	: 'changeEmail'

	initialize: (options)->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@model = new EmailVerify.Models.Base()
		@preload()

	preload: ->
		$.when(
			@model.fetch()
		).then(
			=>
				@bind()
				@render()
				@cacheDom()
				@initPopup()
		).fail(
			=> @hideLoading()
		)

	cacheDom: ->
		@$body = $('body')
		@$el.$loading = @$el.find('.loading_overlay')
		@$el.$popup = @$el.find('[data-popup]')
		@$el.$popup.$content = @$el.$popup.find('[data-popup-content]')

	render: ->
		@$el.html(EmailVerify.Templates.Base()).appendTo @$container
		@stickit()

		# remove footer from the wizard
		Wizard.view.$el.find('[data-wizard-footer]').remove()
		Wizard.view.$el.$containerWrap.addClass('m-success')

		@

	bind: ->
		@bindings =
			'[data-email]': 'email'
		@

	resendEmail: (e) ->
		e?.preventDefault()
		@showLoading()
		$.when(
			@model.resendEmail()
		).then(
			=>
				@hideLoading()
				@openEmailChangedPopup()
		).fail(
			(response) =>
				msg = response.responseJSON.errors?.error?.messages?[0]
				if msg is 'This email has already been verified'
					window.location = getUrl('dashboard')
				else
					@errorAlertPopup response.responseJSON.errors?.error?.messages?[0]
					@hideLoading()
		)

	changeEmail: (e) ->
		e?.preventDefault()
		@openEmailChangePopup()

	initPopup: ->
		@popupName = 'professional_wizard_success'

		$('body').append @$el.$popup
		window.popupsManager.addPopup @$el.$popup

		popup = window.popupsManager.getPopup(@popupName)
		popup.setCloseOnEsc false

	openPopup: ->
		window.popupsManager.openPopup @popupName
		@bindPopupEvents()

	closePopup: ->
		_.each @subViews, (view) -> view.remove?()
		@subViews = []
		window.popupsManager.closePopup @popupName
		@$el.$popup.$content.html ''
		@

	openEmailChangedPopup: ->
		@closePopup()
		@$el.$popup.$content.html EmailVerify.Templates.ChangedEmail()
		@openPopup()

	openEmailChangePopup: ->
		@closePopup()
		@subViews.push new EmailVerify.Views.ChangeEmail
			model: @model
			parentView: @
			baseView: @baseView
		@openPopup()


	errorAlertPopup: (msg, title = 'Error') ->
		@closePopup()
		html = EmailVerify.Templates.Error
			title: title
			msg: msg
		@$el.$popup.$content.html html
		@openPopup()

	bindPopupEvents: ->
		@$el.$popup.find('[data-close]').on 'click', => @closePopup()

	hideLoading: -> Wizard.ready()
	showLoading: -> Wizard.loading()

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'click [data-packages-add]': 'createPackage'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@packages = []

		@bind()

	bind: ->
		@listenTo @collection, 'add', @renderPackage
		@listenTo @, 'rendered', =>
			@cacheDom()
			@renderAllPackages()

	renderAllPackages: ->
		@collection.models.forEach (model) => @renderPackage model

	cacheDom: ->
		@$el.$list = @$el.find('[data-packages-list]')

	render: ->
		@$el.html WizardProfessionalServices.Templates.Packages()
		@trigger 'rendered'
		@

	createPackage: ->
		@collection.createPackage()
		$(document).trigger 'popup:center'

	renderPackage: (model) ->
		@packages.push item = new WizardProfessionalServices.Views.Package
			model: model
			collection: @collection
			baseView: @baseView

		item.render().$el.appendTo @$el.$list

module.exports = View

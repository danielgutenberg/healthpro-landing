Backbone = require 'backbone'
Moment = require 'moment'

class Collection extends Backbone.Collection

	initialize: ->
		@_timeOptions = []

	weekdayOptions: [
		{value: 1, label: 'Sunday', short: 'Sun', letter: 'S'}
		{value: 2, label: 'Monday', short: 'Mon', letter: 'M'}
		{value: 3, label: 'Tuesday', short: 'Tue', letter: 'T'}
		{value: 4, label: 'Wednesday', short: 'Wed', letter: 'W'}
		{value: 5, label: 'Thursday', short: 'Thu', letter: 'T'}
		{value: 6, label: 'Friday', short: 'Fri', letter: 'F'}
		{value: 7, label: 'Saturday', short: 'Sat', letter: 'S'}
	]

	timeOptions: ->
		return @_timeOptions if @_timeOptions.length

		m = Moment().startOf('day')
		offset = 30
		current = 0

		while current < 24 * 60

			@_timeOptions.push {
				value : m.format("HH:mm")
				label : m.format("hh:mm a")
			}

			m.add(offset, 'minute')
			current += offset
		@_timeOptions


	hasOverlaps: ->
		return false if @length < 2

		hasOverlaps = false

		@each (model) =>
			return if hasOverlaps
			days = model.get('days')
			@each (compare) =>
				return if hasOverlaps
				return if compare.cid is model.cid
				intersection = _.intersection days, compare.get('days')
				# all weekdays are different
				return if intersection.length is 0

				_.each intersection, (intDay) ->
					modelFrom = Moment(model.get('from'), 'HH:mm').weekday(intDay - 1)
					modelUntil = Moment(model.get('until'), 'HH:mm').weekday(intDay - 1)

					compareFrom = Moment(compare.get('from'), 'HH:mm').weekday(intDay - 1)
					compareUntil = Moment(compare.get('until'), 'HH:mm').weekday(intDay - 1)

					hasOverlaps = modelFrom.unix() <= compareUntil.unix() and modelUntil.unix() >= compareFrom.unix()

		hasOverlaps

module.exports = Collection

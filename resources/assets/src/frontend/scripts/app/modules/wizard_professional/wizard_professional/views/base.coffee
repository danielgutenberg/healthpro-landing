Handlebars = require 'handlebars'
Abstract = require 'app/modules/wizard/views/main_view'
TemplateProgress = Handlebars.compile require('text!../templates/progress.html')
TemplateFooter = Handlebars.compile require('text!../templates/footer.html')

class View extends Abstract

	bind: ->
		super
		@listenTo Wizard, 'stack:ready', => @renderProgress()

	addStickit: ->
		super
		@bindings['.wizard--prev_step'] =
			classes:
				'm-hide':
					observe: 'previous_step'
					onGet: (val) ->
						return if val? then false else true
			attributes: [{
				name: 'disabled'
				observe: 'canGoBack'
				onGet: (val) ->
					return if val? and val then !val else true
			}]

	appendFooter: ->
		@$el.$contentWrap.append TemplateFooter()

	renderProgress: ->
		steps = _.clone @model.get('steps') # remove the success step
		return unless steps and steps.length

		currentStep = @model.get('current_step').number ? 1

		_.each steps, (step, k) ->
			step.classes = ''
			step.classes += ' m-active' if (k+1 <= currentStep)

		renderHtml = TemplateProgress
			steps: steps
			total_steps: @model.get('total_steps')
			current_step: @model.get('current_step.number')
			completedPercent: 100 / @model.get('total_steps') * @model.get('current_step.number')
			title: @model.get('title')

		@$el.find('.wizard--progress').remove() # remove
		@$el.find('.wizard--header_in').append renderHtml

	appendExitButton: -> return
	removeExitButton: -> return

module.exports = View

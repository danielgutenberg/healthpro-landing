Abstract = require 'app/modules/wizard/models/base'
getApiRoute = require 'hp.api'

class Model extends Abstract

	initialize: ->
		super

		@urls =
			initial: 'api-wizard-profile-initial'
			step: 'api-wizard-profile-step'

		@bind()
		# when booking data is passed save it first and then initialize wizard.
		$.when(
			@loadUser(), @loadProfile()
		).then(
			=> @initWizard()
		)

	loadUser: ->
		$.ajax
			method: 'get'
			url: getApiRoute 'user-account'
			success: (response) =>
				@set 'email_verified', response.data.email_verified
				@

	loadProfile: ->
		$.ajax
			method: 'get'
			url: getApiRoute 'ajax-profiles-self'
			success: (response) =>
				@set 'profile', response.data
				@



module.exports = Model

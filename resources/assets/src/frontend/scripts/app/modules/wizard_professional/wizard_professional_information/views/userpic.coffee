Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

Userpic = require 'framework/userpic'
getApiRoute = require 'hp.api'
Mixins = require 'utils/backbone_mixins'
EditImages = require 'app/modules/profile_edit/edit_image'

class View extends Backbone.View

	events:
		'click .js-edit': (e) -> @initImageEdit(e)

	attributes:
		'class'			: 'js-userpic'
		'data-options'	: JSON.stringify
			"_token"		: window.GLOBALS._TOKEN
			"hasUpload"		: true
			"uploadUrl"		: getApiRoute('ajax-profiles-upload-file', {'id': window.GLOBALS._PID})

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions(options)

		@addListeners()
		@render()

	addListeners: ->
		$(document).on 'image:uploaded', =>
			@model.set
				avatar: @userpic.$block.$image.attr('src')
				avatar_original: @userpic.$block.$image.data('original')
			@showUserpicEdit()

		@listenTo @, 'rendered', =>
			@$('.userpic img').attr('src', @model.get('avatar')).data('original', @model.get('avatar_original'))
			@userpic = new Userpic @$el
			@showUserpicEdit()

	render: ->
		@$el.html WizardProfessionalInformation.Templates.Userpic
			_token: window.GLOBALS._TOKEN
			uploadUrl: getApiRoute('ajax-profiles-upload-file', {'id': @baseView.profileId})

		@baseView.$el.find('[data-userpic]').append @$el

		@trigger 'rendered'
		@

	showUserpicEdit: ->
		if @$el.find('img').attr('src') != '' && @$el.find('img').attr('src') != undefined
			@$el.find('.userpic--edit').removeClass 'm-hide'

	initImageEdit: ->
		@originalImageUrl = @$el.find('.userpic--inner img').data('original')
		@editor = new EditImages(@originalImageUrl, {aspectRatio: 1 / 1}, @saveEdited)

	saveEdited: (blob) =>
		@userpic.uploadEdited(blob)

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Phone = require 'framework/phone'

require 'backbone.validation'
require 'backbone.stickit'

class FormView extends Backbone.View

	events:
		'click [data-send-code]'		: 'sendCode'
		'click [data-edit-number]' 		: 'editNumber'
		'click [data-send-code-sms]' 	: 'sendSMS'
		'click [data-send-code-call]' 	: 'sendCall'
		'click [data-verify-code]'		: 'verifyCode'
		'click [data-resend-code]'		: 'resendCode'

	initialize: (options)->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@bind()
		@render()
		@cacheDom()
		if @model.get 'phone_verified'
			@displayMsg 'success'
		else
			if @model.get 'phone'
				@displayBlock 'code', 'phone'

	render: ->
		@$el.html WizardProfessionalInformation.Templates.PhoneVerify()
		@baseView.$el.find('[data-phone-verify]').append @$el
		@stickit()
		@trigger 'rendered'
		@

	cacheDom: ->
		@$el.$blocks = @$el.find('[data-block]')
		@$el.$msg 	= @$el.find('[data-block="msg"]')
		@$el.$edit = @$el.find('[data-block="edit"]')
		@$el.$phone = @$el.find('[data-block="phone"]')
		@$el.$type 	= @$el.find('[data-block="type"]')
		@$el.$code 	= @$el.find('[data-block="code"]')
		@

	bind: ->
		@bindings =
			'[name="phone"]':
				observe: 'phone'
				onSet: (val) => if val then @phone.formattedVal(true) else ''
				onGet: (val) -> if val then $.formatMobilePhoneNumber(val).replace('+1 ', '') else ''
				afterUpdate: ($el) -> $el.change()
				initialize: ($el) ->
					@phone = new Phone $el
			'[data-phone-label]':
				observe: 'phone'
				onGet: => "+1 " + @phone.$node.val()
			'[name="verification_code"]': 'phone_verification_code'

			'[data-edit-number]':
				classes:
					'm-hide':
						observe: 'phone_verified'

			'[data-verify-code]':
				attributes: [
					{
						name: 'disabled'
						observe: ['phone_verification_code']
						onGet: (val) -> "#{val}".length < 4
					}
				]
		@

	displayBlock: ->
		@$el.$blocks.addClass 'm-hide'
		_.each _.flatten(arguments), (block) => @$el["$#{block}"].removeClass 'm-hide'
		@

	displayMsg: (msgType = 'success', msg = null, displayBlocks = null) ->
		if msg is null
			msg = WizardProfessionalInformation.Config.messages.phone[msgType]

		@$el.$msg.html "<p>#{msg}</p>"
		@$el.$msg.removeClass('m-success m-error').addClass("m-#{msgType}")

		@displayBlock displayBlocks ? ['msg', 'phone']

	sendCode: ->
		return unless @model.isValid('phone')
		@displayBlock 'phone', 'type'

	editNumber: ->
		@model.set
			phone_verified			: false
			phone_verification_code	: ''
			phone_verify_method		: ''
		@displayBlock 'edit'

	sendSMS: -> @sendVerificationCode('sms')

	sendCall: -> @sendVerificationCode('call')

	handleErrors: (responseJSON, defaultError = '') ->
		error 			= null
		displayBlocks 	= null

		if responseJSON.errors?.error?.messages?.length
			if responseJSON.errors.error.messages[0].match /Invalid Code/i
				error = "You've entered an invalid code"
				displayBlocks = ['msg', 'code', 'phone']
			else if responseJSON.errors.error.messages[0].match /Invalid phone/i
				error = "You've entered an invalid phone number"
				displayBlocks = ['msg', 'phone']

		@displayMsg 'error', error ? defaultError, displayBlocks
		@

	sendVerificationCode: (method) ->
		@baseView.showLoading()
		@model.set 'phone_verify_method', method

		$.when(
			@model.sendCode()
		).then(
			=>
				@baseView.hideLoading()
				@displayBlock 'code', 'phone'
				@
		).fail(
			(response) =>
				@baseView.hideLoading()
				@handleErrors response.responseJSON, 'Could not send the code. Please verify your phone number.'
		)

	resendCode: ->
		@baseView.showLoading()
		$.when(
			@model.resendCode()
		).then(
			=>
				@baseView.hideLoading()
				@displayBlock 'code', 'phone'
				@
		).fail(
			(response) =>
				@baseView.hideLoading()

				@handleErrors response.responseJSON, 'Could not resend the code. Please reload the page and try again'
		)

	verifyCode: ->
		@baseView.showLoading()
		$.when(
			@model.verifyCode()
		).then(
			=>
				@baseView.hideLoading()
				@displayMsg 'success'
				@model.set 'phone_verified', true
				@
		).fail(
			(response) =>
				@model.set 'phone_verification_code', ''
				@baseView.hideLoading()
				@handleErrors response.responseJSON, 'Could not verify the code. Please reload the page and try again'
		)

module.exports = FormView

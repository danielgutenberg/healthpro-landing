Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'

# models
BaseModel = require './models/base'

# templates
BaseTmpl = require 'text!./templates/base.html'

# list of all instances
window.EmailVerify = ProfileSuccess =
	Views:
		Base: BaseView
		ChangeEmail: require('./views/change_email')
	Models:
		Base: BaseModel
	Templates:
		Base: Handlebars.compile BaseTmpl
		ChangeEmail: Handlebars.compile require('text!./templates/change_email.html')
		ChangedEmail: Handlebars.compile require('text!./templates/changed_email.html')
		Error: Handlebars.compile require('text!./templates/error.html')


# events bus
_.extend ProfileSuccess, Backbone.Events

class ProfileSuccess.App
	constructor: (options) ->
		@model = new EmailVerify.Models.Base()

		@view = new EmailVerify.Views.Base
			model: @model
			$container: options.$container

		return {
			model: @model
			view: @view

			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports =  EmailVerify

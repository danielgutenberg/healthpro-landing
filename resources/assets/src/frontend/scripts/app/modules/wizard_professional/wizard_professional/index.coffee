Backbone = require 'backbone'
Abstract = require 'app/modules/wizard/abstract'

ProfessionalWizardView = require('./views/base')
ProfessionalWizardModel = require('./models/base')
ProfessionalWizardRouter = require('./router/router')

require '../../../../../styles/modules/wizard_professional.styl'

###
  PROFESSIONAL WIZARD PRIVATE CLASS
  AVAILABLE THROUGH THE SINGLETON PATTERN
###
class ProfessionalWizard extends Abstract

	inited: false

	defaults: ->
		{
			el: $('.wizard.m-profile')
		}


	init: (options = null) ->

		if @inited
			console.error 'ProfessionalWizard has already been inited'
			return @

		@setOptions options if options

		# start router when init data is loaded
		@listenToOnce Wizard, 'stack:ready', =>
			@initRouter()

		@model = new ProfessionalWizardModel
			wizard_name: 'profile'

		@view = new ProfessionalWizardView
			model: @model
			el: @options.el

		@inited = true

		@

	initRouter: ->
		# start after data inited
		Backbone.history.start(pushState: true) unless Backbone.History.started

		@router = new ProfessionalWizardRouter
			model: @model
			view: @view
		@



class ProfessionalWizardSingleton
	instance = null

	@get: (options = {}) ->
		instance ?= new ProfessionalWizard options

	@destroy: ->
		instance = null

# save the singleton instance to the wizard object
window.Wizard = ProfessionalWizardSingleton.get()

if window.GLOBALS._PTYPE is 'provider' and $('.wizard.m-page[data-wizard="profile"]').length
	ProfessionalWizardSingleton.get().init()

module.exports = ProfessionalWizardSingleton

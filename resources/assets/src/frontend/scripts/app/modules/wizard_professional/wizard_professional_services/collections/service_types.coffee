Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Tree = require 'utils/tree'
Constants = require 'config/constants'

class Collection extends Backbone.Collection

	initialize: ->
		@tree = new Tree [],
			prefix: '—'

	getServiceTypes: ->
		$.ajax
			url: getApiRoute('ajax-provider-service-types') + '?sort=name'
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.each response.data, (model) =>
					@push model if model.slug isnt Constants.IntroductorySessionSlug

				@tree.setData @toJSON()
				@trigger 'data:ready'

	getTree: -> @tree.getTree()

	getFlatTree: -> @tree.getFlatTree()

module.exports = Collection

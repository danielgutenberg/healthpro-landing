Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		email: ''
		new_email: ''

	validation:
		new_email: (val, name, props) ->
			return 'Enter a valid email address' unless @isEmail(val)
			return 'Please enter a new email address' if props.email is val

	fetch: ->
		$.ajax
			url: getApiRoute('user-account')
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				@set 'email', response.data.user.email
				@

	resendEmail: ->
		$.ajax
			url: getApiRoute('ajax-auth-send-activation')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

	updateEmail: ->
		$.ajax
			url: getApiRoute('user-account')
			method: 'put'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				old_email			: @get('email')
				email				: @get('new_email')
				current_password	: '-'
				_token				: window.GLOBALS._TOKEN
			success: =>
				@set 'email', @get('new_email') # update email
				@set 'new_email', ''
				@

	isEmail: (value) ->
		value and value.match(Backbone.Validation.patterns.email)

module.exports = Model

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

getRoute = require 'config/url.coffee'

class View extends Backbone.View

	className: 'professional_setup_wizard--inner'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@render()
		@cacheDom()
		@preload()

	addListeners: ->
		@listenTo @model, 'change:phone_verified', => @toggleGoAhead @model.get('phone_verified')

	cacheDom: ->
		@$body = $('body')
		@$el.$main = @$el.find('[data-view-main]')
		@$el.$title = @$el.find('[data-view-title]')


	render: ->
		@$el.html(WizardProfessionalInformation.Templates.Base()).appendTo @$container

	preload: ->
		@toggleGoAhead false
		@showLoading()
		$.when(@model.fetch()).then(
			=>
				@initProfile()
				@initUserpic()
				@initPhoneVerify()
				@hideLoading()
				@toggleGoAhead @model.get('phone_verified')

		)

	hideLoading: -> Wizard.ready()
	showLoading: -> Wizard.loading()

	initProfile: ->
		@subViews.push @profileView = new WizardProfessionalInformation.Views.Profile
			model: @model
			baseView: @
		@

	initUserpic: ->
		@subViews.push @userpic = new WizardProfessionalInformation.Views.Userpic
			model: @model
			baseView: @
		@

	initPhoneVerify: ->
		@subViews.push @phoneVerify = new WizardProfessionalInformation.Views.PhoneVerify
			model: @model
			baseView: @
		@

	toggleGoAhead: (enable = false) ->
		Wizard.model.set 'canGoAhead', enable
		Wizard.model.set 'canFinish', enable
		@

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Select = require 'framework/select'
vexDialog = require 'vexDialog'
ScrollTo = require 'utils/scroll_to_element'

require 'backbone.stickit'
require 'backbone.validation'

require 'tooltipster'

class View extends Backbone.View

	events:
		'click [data-sub-toggle]': 'subToggle'
		'click [data-open-service-form]': 'openServiceForm'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@model = new WizardProfessionalServices.Models.Service()

		@errors = []

		@addListeners()
		@initValidation()
		@bind()
		@render()

	initValidation: ->
		Backbone.Validation.bind @,
			valid	: (view, attr)			=> @raiseError attr, null
			invalid	: (view, attr, error) 	=> @raiseError attr, error

	addListeners: ->
		@listenTo @model, 'error:generic', (errorMessage) => @raiseGenericError errorMessage
		@listenTo @model, 'error:alert', (errorMessage) => @raiseAlertError errorMessage, callback
		@listenTo Wizard, 'check_step', @saveService

		# disable service in sub-services if it is selected as a main service
		@listenTo @model, 'change:service_type_id', @onServiceTypeChanged

	bind: ->
		@bindings =
			'[name="service_type_ids"]':
				observe: 'service_type_ids'
				selectOptions:
					collection: => @collections.serviceTypes.getFlatTree()
					labelPath: 'label'
					valuePath: 'value'
				initialize: ($el) =>
					@selectSubServices = new Select $el,
						minimumResultsForSearch: 0
						templateResult: (service) ->
							return service.text if service.loading or !service.id
							return service.text if service.text.match '—'
							return "<strong>#{service.text}</strong>"
						templateSelection: (service) ->
							_.trunc service.text.replace(/—/g, ''),
								length: 30
						escapeMarkup: (markup) -> markup
						dropdownClass: 'm-show_disabled'
					$el.on 'select2:select', -> $el.trigger 'focus.validation'
				getVal: @onSetSubServices

			'[name="service_type_id"]':
				observe: 'service_type_id'
				selectOptions:
					collection: @collections.serviceTypes.getFlatTree()
					labelPath: 'label'
					valuePath: 'value'
					defaultOption:
						label: 'Select Service'
						value: null
				initialize: ($el) =>
					@selectService = new Select $el,
						minimumResultsForSearch: 0
						templateResult: (service) ->
							return service.text if service.loading or !service.id
							if service.text.match '—'
								return service.text
							return "<strong>#{service.text}</strong>"
						templateSelection: (service) ->
							_.trunc service.text.replace(/—/g, ''),
								length: 40
						escapeMarkup: (markup) -> markup
					$el.on 'select2:select', -> $el.trigger 'focus.validation'
				getVal: @onSetService

			'[name="certificates"]':
				observe: 'certificates'
				onSet: (val) -> +val
				onGet: (val) -> +val
				initialize: ($el, model) ->
					$el.prop 'checked', model.get('certificates')

			'[name="description"]': 'description'

	cacheDom: ->
		@$el.$service = @$el.find('[data-edit-service]')
		@$el.$serviceTypes = @$el.find('[data-edit-service-types]')
		@$el.$subServices = @$el.find('[data-edit-sub-services]')
		@$el.$sessions = @$el.find('[data-edit-sessions]')
		@$el.$packagesReccuring = @$el.find('[data-edit-packages-reccuring]')
		@$el.$error = @$el.find('[data-generic-error]')
		@$el.$sessionsError = @$el.find('[data-sessions-error]')

	onSetService: ($el) =>
		val = parseInt $el.val()
		@model.set 'name', if val then @collections.serviceTypes.get(val).get('name') else ''
		val

	onSetSubServices: ($el) =>
		values = $el.val()
		return [] unless values

		values = values.map (val) -> parseInt val

		# check for duplicate services
		duplicate = @getDuplicateServices values, true
		duplicateServiceIds = _.flatten duplicate.map (model) -> model.getServiceTypeIds()

		# update select and remove duplicate ids
		if duplicateServiceIds.length
			values = _.difference(values, duplicateServiceIds)
			$el.val values
			$el.trigger 'change'

		values

	onServiceTypeChanged: ->
		# enable all disabled options
		@selectSubServices.$node.find('option').prop('disabled', false)

		# disable selected service type option
		@selectSubServices.$node.find('option[value=' + @model.get('service_type_id') + ']').prop('disabled', true);

		# remove selected service from service type ids
		@model.set 'service_type_ids', _.without(@model.get('service_type_ids'), @model.get('service_type_id'))

		# reinit
		@selectSubServices.reInit()

	getDuplicateServices: (values, duplicateAlert = true) ->
		duplicate = @collections.services.findDuplicateServices values, @model
		@duplicateAlert() if duplicate.length and duplicateAlert
		duplicate

	render: ->
		@$el.html WizardProfessionalServices.Templates.AddService
			# gift_certificates: @hasFeature('gift_certificates')
			gift_certificates: false # don't allow certs before setting online payments
			monthly_packages: @hasFeature('monthly_packages')

		@baseView.$el.$main.append @$el

		@afterRender()
		@

	afterRender: ->
		@stickit()
		@cacheDom()
		@appendSessions()
		@appendPackagesReccuring()

	subToggle: (e) ->
		$btn = $(e.currentTarget)
		$btn.addClass 'm-hide'
		@$el.find('[data-sub="' + $btn.data('sub-toggle') +  '"]').removeClass 'm-hide'

	saveService: ->
		@raiseGenericError null
		@validateData()

		if @errors.length
			@scrollToError()
			return

		@baseView.showLoading()
		$.when(@model.save()).then(
			=>
				@baseView.hideLoading()
				Wizard.trigger 'next_step'
		).fail(
			=> @baseView.hideLoading()
		)



	raiseError: (attr, error) ->
		field = @$el.find('[name="' + attr + '"]')
		fieldParent = field.parents('.field, .select')
		fieldError  = fieldParent.find('.field--error')
		if error
			fieldParent.addClass 'm-error'
			fieldError.html error
			field.on 'focus.validation', ->
				fieldParent.removeClass 'm-error'
				field.off('focus.validation')
		else
			fieldParent.removeClass 'm-error'
			fieldError.html ''

	raiseEditError: (message, $errorEl, type = 'error') ->
		if message
			$errorEl.removeClass 'm-hide'
			$errorEl.html "<div class='err m-#{type}'>#{message}</div>"
		else
			$errorEl.addClass 'm-hide'
			$errorEl.html ''
		@

	raiseGenericError: (message = null, type = 'error') ->
		@raiseEditError message, @$el.$error, type
		@

	raiseSessionsError: (message = null, type = 'error') ->
		@raiseEditError message, @$el.$sessionsError, type
		@

	raiseAlertError: (message, callback = null) ->
		vexDialog.alert
			buttons: [$.extend({}, vexDialog.buttons.YES, text: 'OK')]
			message: message
			callback: -> callback() if callback

	openServiceForm: (e) ->
		e?.preventDefault()
		@baseView.initServiceForm()

	validateData: ->
		@raiseSessionsError null
		@errors = []
		errors = @model.validate()
		@errors.push errors if errors?

		# check if no active sessiona and recurring packages available
		# display a message

		activeSessions = @model.getActiveSessions()
		recurringPackages = @model.get('packages_reccuring').models

		if !activeSessions.length and !recurringPackages.length
			msg = 'Please add at least one standard rate or monthly package'
			@raiseSessionsError msg
			@errors.push msg

		# we need to validate only active sessions here
		# reccuring packages should be validated separately
		_.each activeSessions, (sessionModel) =>
			return if sessionModel.isEmpty()
			errors = sessionModel.validate()
			@errors.push errors if errors?
			_.each sessionModel.get('packages').models, (packageModel) =>
				return if packageModel.isEmpty()
				errors = packageModel.validate()
				@errors.push errors if errors?
				return
			return

		# validate reccuring packages
		_.each recurringPackages, (packageModel) =>
			return if packageModel.isEmpty()
			errors = packageModel.validate()
			@errors.push errors if errors?
			return

		@errors


	appendSessions: ->
		$sessions = new WizardProfessionalServices.Views.Sessions
			collection: @model.get('sessions')
			parentModel: @model
			baseView: @baseView
			parentView: @

		$sessions.render().$el.appendTo @$el.$sessions

	appendPackagesReccuring: ->
		$packagesReccuring = new WizardProfessionalServices.Views.PackagesReccuring
			collection: @model.get('packages_reccuring')
			parentModel: @model
			baseView: @baseView
			parentView: @

		$packagesReccuring.render().$el.appendTo @$el.$packagesReccuring

	scrollToError: ->
		ScrollTo @$el.find('.field.m-error:first'), 30

	hasFeature: (feature) ->
		features = Wizard.model.get('profile.features')
		return true unless _.isArray(features)
		_.indexOf(features, feature) > -1

module.exports = View

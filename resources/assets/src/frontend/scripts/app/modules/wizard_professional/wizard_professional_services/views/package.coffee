Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'
NumericInput = require 'framework/numeric_input'
Config = require 'app/modules/professional_setup/config/config'

require 'backbone.stickit'

class View extends Backbone.View

	className: 'professional_setup_wizard--sessions--item_package'

	events:
		'click [data-package-remove]': 'confirmRemove'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addValidation()
		@bind()

	bind: ->
		@bindings =
			'[name="price"]':
				observe: 'price'
				initialize: ($el) ->
					@price = new NumericInput $el,
						decimal: '.'
						leadingZeroCheck: true
						initialParse: false
						parseOnBlur: false

			'[name="number_of_visits"]':
				observe: 'number_of_visits'
				onSet: @onSetNumber
				selectOptions:
					collection: Config.NumberOfVisitsOptions

	render: ->
		@$el.html WizardProfessionalServices.Templates.Package()
		@stickit()
		@trigger 'rendered'
		@

	onSetNumber: (val) =>
		packages = @model.collection
		sameNumber = packages.findWhere
			number_of_visits: val
		if sameNumber instanceof Backbone.Model
			@duplicateAlert()
		val

	duplicateAlert: ->
		vexDialog.alert
			message: 'You already have a package with that number of entries, please select a different number of entries'
			callback: =>
				@model.set 'number_of_visits', @model.previous('number_of_visits')

	confirmRemove: ->
		if @model.get('price') or @model.get('id')
			vexDialog.confirm
				message: 'Are you sure you want to remove this package?'
				callback: (status) =>
					if status
						@remove()
		else
			@remove()

	remove: ->
		@baseView.showLoading()

		if @baseView.instances.edit_service
			@baseView.instances.edit_service.raiseGenericError null

		$.when(@model.remove()).then(
			=>
				@baseView.hideLoading()
				super
		,
			(response) =>
				@baseView.hideLoading()
				if @baseView.instances.edit_service
					msg = response.responseJSON.errors?.error?.messages?[0]
					msg = "Can't delete the package." unless msg
					@baseView.instances.edit_service.raiseGenericError msg
		)

module.exports = View

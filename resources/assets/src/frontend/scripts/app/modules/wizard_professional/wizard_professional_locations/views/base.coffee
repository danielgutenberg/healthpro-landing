Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

Timezones = require 'framework/timezones'

require 'tooltipster'

class View extends Backbone.View

	className: 'professional_setup_wizard--inner'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@tz = new Timezones()
		@instances = {}

		@toggleGoAhead false

		@bind()
		@render()
		@initTooltips()
		@cacheDom()
		@preload()

	cacheDom: ->
		@$body = $('body')
		@$el.$main = @$el.find('[data-view-main]')
		@$el.$error = @$el.find('[data-base-error]')

	render: ->
		@$el.html(WizardProfessionalLocations.Templates.Base()).appendTo @$container

	preload: ->
		@showLoading()
		unloadedCollections = _.size @collections
		_.forEach @collections, ($collection) =>
			@listenTo $collection, 'data:ready', =>
				unloadedCollections--
				unless unloadedCollections
					@trigger 'data:loaded'

	hideLoading: -> Wizard.ready()
	showLoading: -> Wizard.loading()

	bind: ->
		@listenToOnce @, 'data:loaded', =>
			@initAddLocation()
			@hideLoading()

	raiseError: (message = null, type = 'error') ->
		if message
			@$el.$error.removeClass 'm-hide'
			@$el.$error.html "<div class='alert m-#{type} m-show'>#{message}</div>"
		else
			@$el.$error.addClass 'm-hide'
			@$el.$error.html ''

	initAddLocation: ->
		@subViews.push @addLocation = new WizardProfessionalLocations.Views.AddLocation
			collections: @collections
			baseView: @

	toggleGoAhead: (enable = false) ->
		Wizard.model.set 'canGoAhead', enable

	initTooltips: ->
		@$el.find('[data-tooltip]').tooltipster
			theme: 'tooltipster-dark'
			maxWidth: 200

module.exports = View

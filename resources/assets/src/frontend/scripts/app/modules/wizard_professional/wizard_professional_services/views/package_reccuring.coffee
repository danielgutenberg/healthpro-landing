Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'
NumericInput = require 'framework/numeric_input'
Config = require 'app/modules/professional_setup/config/config'
require 'backbone.stickit'

class View extends Backbone.View

	className: 'professional_setup_wizard--recurring--item'

	events:
		'click [data-package-remove]': 'confirmRemove'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addValidation()
		@bind()

	bind: ->
		@bindings =
			'[name="price"]':
				observe: 'price'
				initialize: ($el) ->
					@price = new NumericInput $el,
						decimal: '.'
						leadingZeroCheck: true
						initialParse: false
						parseOnBlur: false

			'[name="rules[max_weekly]"]':
				observe: 'rules.max_weekly.value'
				onSet: (val) =>
					@checkForDuplicates 'rules.max_weekly.value', val, @model.get('rules.max_weekly.value')
					@parentView.toggleMaxWeeklyInfo()
					val
				selectOptions:
					collection: Config.NumberOfVisitsPerWeekOptions
					defaultOption:
						label: ' '
						value: null

			'[name="duration"]':
				observe: 'duration'
				onSet: (val) =>
					@checkForDuplicates 'duration', val
					val
				selectOptions:
					collection: Config.SessionDurationOptions
					defaultOption:
						label: 'Select duration'
						value: null

			'[name="number_of_months"]':
				observe: 'number_of_months'
				onSet: (val) =>
					@checkForDuplicates 'number_of_months', val, @model.get('number_of_months')
					val
				selectOptions:
					collection: Config.NumberOfMonthsOptions
					defaultOption:
						label: ' '
						value: null

	checkForDuplicates: (field, value, previousValue) ->
		packages = @model.collection.without(@model)
		condition =
			duration: @model.get('duration')
			rules: @model.get('rules')
			number_of_months: @model.get('number_of_months')

		# replace current field condition
		_.set condition, field, value

		# if one of the conditions is null/undefined to not check for duplicates
		return unless condition.duration and condition.rules and condition.number_of_months

		duplicate = _.filter packages, (pkg) ->
			return false unless pkg.get('duration') is condition.duration
			return false unless pkg.get('rules.max_weekly.value') is condition.rules.max_weekly.value
			return false unless pkg.get('number_of_months') is condition.number_of_months
			true

		if duplicate.length
			vexDialog.alert
				buttons: [
					$.extend {}, vexDialog.buttons.YES, {text: 'OK'}
					$.extend {}, vexDialog.buttons.NO, {text: 'Cancel'}
				]
				message: 'Duration / Visits per Week / Number of Months should be unique'
				callback: =>
					@model.set field, previousValue ? @model.previous(field)

	cacheDom: ->
		@$el.$maxWeeklyNotice = @$el.find('[data-]')

	render: ->
		@$el.html WizardProfessionalServices.Templates.PackageReccuring()
		@stickit()
		@cacheDom()
		@trigger 'rendered'
		@

	confirmRemove: ->
		if @model.get('price') or @model.get('id')
			vexDialog.confirm
				buttons: [
					$.extend {}, vexDialog.buttons.YES, {text: 'YES'}
					$.extend {}, vexDialog.buttons.NO, {text: 'Cancel'}
				]
				message: "Are you sure you’d like to delete this package?<br><span class='m-error'>If yes, please note: Packages that have ALREADY been purchased by clients will NOT be cancelled.</span>"
				callback: (status) =>
					if status
						@remove()
		else
			@remove()

	remove: ->
		@baseView.showLoading()

		if @baseView.instances.edit_service
			@baseView.instances.edit_service.raiseGenericError null

		$.when(
			@model.remove()
		).then(
			=>
				@baseView.hideLoading()
				super
		).fail(
			(response) =>
				@baseView.hideLoading()
				return unless @baseView.instances.edit_service
				msg = response.responseJSON.errors?.error?.messages?[0]
				msg = "Can't delete the package." unless msg
				@baseView.instances.edit_service.raiseGenericError msg
		)

module.exports = View

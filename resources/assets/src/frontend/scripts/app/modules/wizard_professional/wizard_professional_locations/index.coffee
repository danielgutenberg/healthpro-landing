Backbone = require 'backbone'
Handlebars = require 'handlebars'

window.WizardProfessionalLocations =
	Config:
		serviceAreaUnit: 'mile'
		locationNames:
			office: 'Business Location'
			homeVisit: 'Home Visits'
			virtual: 'Virtual'
			introductory: 'Introductory Location'
		locationDescriptions:
			office: 'The session occurs at your place of business.'
			homeVisit: 'The session takes place at the client’s home'
			virtual: 'The session takes place online or at an alternative location'
		locationIcons:
			office: '''
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
					<path d="M446.812 493.966l-67.499-142.781c-1.347-2.849-3.681-5.032-6.48-6.223l-33.58-14.949 58.185-97.518c.139-.234.27-.471.395-.713 11.568-22.579 17.434-46.978 17.434-72.515 0-42.959-16.846-83.233-47.435-113.402C337.248 15.703 296.73-.588 253.745.016c-41.748.579-81.056 17.348-110.685 47.22-29.626 29.87-46.078 69.313-46.326 111.066-.152 25.515 5.877 50.923 17.431 73.479.124.241.255.479.394.713l58.184 97.517-33.774 15.031c-2.763 1.229-4.993 3.408-6.285 6.142L65.187 493.966c-2.259 4.775-1.306 10.453 2.388 14.23 3.693 3.777 9.345 4.859 14.172 2.711l84.558-37.646 84.558 37.646c3.271 1.455 7.006 1.455 10.277 0l84.558-37.646 84.558 37.646c1.652.735 3.401 1.093 5.135 1.093 3.331 0 6.608-1.318 9.037-3.803 3.691-3.778 4.643-9.454 2.384-14.231zm-310.339-274.06c-9.73-19.132-14.599-39.805-14.47-61.453.428-72.429 59.686-132.17 132.094-133.173 36.166-.486 70.263 13.199 95.993 38.576 25.738 25.383 39.911 59.267 39.911 95.412 0 21.359-4.869 41.757-14.473 60.638L266.85 402.054c-3.318 5.56-8.692 6.16-10.849 6.16-2.158 0-7.532-.6-10.849-6.16L136.473 219.906zm214.361 227.985c-3.271-1.455-7.006-1.455-10.277 0l-84.558 37.646-84.558-37.646c-3.271-1.455-7.006-1.455-10.277 0l-58.578 26.08 50.938-107.749 32.258-14.356 37.668 63.133c6.904 11.572 19.072 18.481 32.547 18.481 13.475 0 25.643-6.909 32.547-18.48l37.668-63.133 32.261 14.361 50.935 107.744-58.574-26.081z"/>
					<path d="M256.004 101.607c-31.794 0-57.659 25.865-57.659 57.658s25.865 57.658 57.659 57.658c31.793.001 57.658-25.865 57.658-57.658s-25.865-57.658-57.658-57.658zm0 90.05c-17.861.001-32.393-14.529-32.393-32.392 0-17.861 14.531-32.392 32.393-32.392 17.861 0 32.392 14.531 32.392 32.392s-14.531 32.392-32.392 32.392z"/>
				</svg>
			'''
			homeVisit: '''
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
					<path d="M506.555 208.064L263.859 30.367c-4.68-3.426-11.038-3.426-15.716 0L5.445 208.064c-5.928 4.341-7.216 12.665-2.875 18.593s12.666 7.214 18.593 2.875L256 57.588l234.837 171.943c2.368 1.735 5.12 2.57 7.848 2.57 4.096 0 8.138-1.885 10.744-5.445 4.342-5.927 3.054-14.251-2.874-18.592zM442.246 232.543c-7.346 0-13.303 5.956-13.303 13.303v211.749H322.521V342.009c0-36.68-29.842-66.52-66.52-66.52s-66.52 29.842-66.52 66.52v115.587H83.058V245.847c0-7.347-5.957-13.303-13.303-13.303s-13.303 5.956-13.303 13.303V470.9c0 7.347 5.957 13.303 13.303 13.303h133.029c6.996 0 12.721-5.405 13.251-12.267.032-.311.052-.651.052-1.036V342.01c0-22.009 17.905-39.914 39.914-39.914s39.914 17.906 39.914 39.914V470.9c0 .383.02.717.052 1.024.524 6.867 6.251 12.279 13.251 12.279h133.029c7.347 0 13.303-5.956 13.303-13.303V245.847c-.001-7.348-5.957-13.304-13.304-13.304z"/>
				</svg>
			'''
			virtual: '''
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 511.999 511.999">
					<path d="M481.091 27.937H30.909C13.866 27.937 0 41.803 0 58.846v310.819c0 17.043 13.866 30.909 30.909 30.909h154.26v22.49c0 20.617-16.774 37.391-37.391 37.391h-33.997c-6.518 0-11.803 5.284-11.803 11.803 0 6.519 5.285 11.803 11.803 11.803h284.436c6.518 0 11.803-5.284 11.803-11.803 0-6.519-5.285-11.803-11.803-11.803h-33.998c-20.617 0-37.391-16.774-37.391-37.391v-22.489h154.26c17.043 0 30.91-13.866 30.91-30.909V58.846c.002-17.043-13.864-30.909-30.907-30.909zM195.92 460.457c8.046-10.336 12.857-23.308 12.857-37.391v-22.49h94.447v22.49c0 14.083 4.811 27.056 12.857 37.391H195.92zm292.474-90.791c0 4.027-3.276 7.304-7.304 7.304H30.909c-4.027 0-7.304-3.276-7.304-7.304v-62.033h464.789v62.033zm0-85.64H23.606V58.846c0-4.027 3.276-7.304 7.304-7.304h450.18c4.027 0 7.305 3.276 7.305 7.304v225.18z"/>
					<circle cx="256.003" cy="342.305" r="12.738"/>
					<path d="M276.238 109.254c-4.61-4.609-12.081-4.609-16.693 0l-83.414 83.414c-4.609 4.609-4.609 12.083 0 16.693 2.306 2.305 5.325 3.457 8.347 3.457 3.022 0 6.041-1.152 8.346-3.457l83.414-83.414c4.609-4.609 4.609-12.083 0-16.693zM325.678 157.593c-4.608-4.609-12.079-4.609-16.692-.001l-33.23 33.228c-4.609 4.61-4.609 12.084 0 16.693 2.305 2.305 5.325 3.457 8.346 3.457 3.02 0 6.041-1.152 8.346-3.457l33.23-33.228c4.609-4.609 4.609-12.083 0-16.692z"/>
				</svg>
			'''

		messages:
			error: 'Something went wrong. Please reload the page and try again'
	Views:
		Base: require('./views/base')
		AddLocation: require('./views/add_location')
		LocationPeriods: require('./views/location_periods')
		LocationPeriod: require('./views/location_period')

	Models:
		Location: require('./models/location')
		LocationPeriod: require('./models/location_period')
	Collections:
		Locations: require('./collections/locations')
		LocationsTypes: require('./collections/locations_types')
		LocationPeriods: require('./collections/location_periods')

	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')
		AddLocation: Handlebars.compile require('text!./templates/add_location.html')
		LocationPeriods: Handlebars.compile require('text!./templates/location_periods.html')
		LocationPeriod: Handlebars.compile require('text!./templates/location_period.html')

_.extend WizardProfessionalLocations, Backbone.Events

class WizardProfessionalLocations.App
	constructor: (options) ->
		GMaps.load().ready =>
			@collections =
				locations: new WizardProfessionalLocations.Collections.Locations 0, {model: WizardProfessionalLocations.Models.Location}
				locationsTypes: new WizardProfessionalLocations.Collections.LocationsTypes()

			WizardProfessionalLocations.Models.Location.prototype.collection = @collections.locations
			WizardProfessionalLocations.Models.Location.prototype.locationsTypes = @collections.locationsTypes

			@view = new WizardProfessionalLocations.Views.Base
				collections: @collections
				$container: options.$container
				profileId: options.profileId

	close: ->
		@view.close?()
		@view.remove?()

module.exports = WizardProfessionalLocations

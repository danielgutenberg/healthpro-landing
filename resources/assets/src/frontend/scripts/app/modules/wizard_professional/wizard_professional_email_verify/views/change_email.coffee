Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'backbone.validation'
require 'backbone.stickit'

class FormView extends Backbone.View

	events:
		'click [data-send-email]': 'sendEmail'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addValidation()
		@bind()
		@render()
		@cacheDom()

	bind: ->
		@bindings =
			'[name="new_email"]': 'new_email'
		@

	render: ->
		@$el.html EmailVerify.Templates.ChangeEmail()
		@parentView.$el.$popup.$content.append @$el

		@stickit()
		@trigger 'rendered'
		@

	cacheDom: ->
		@$el.$blocks = @$el.find('[data-block]')
		@$el.$msg 	= @$el.find('[data-block="msg"]')
		@$el.$edit = @$el.find('[data-block="edit"]')
		@$el.$phone = @$el.find('[data-block="phone"]')
		@$el.$type 	= @$el.find('[data-block="type"]')
		@$el.$code 	= @$el.find('[data-block="code"]')
		@

	sendEmail: (e) ->
		e?.preventDefault()
		return if @model.validate()

		@parentView.showLoading()
		$.when(
			@model.updateEmail()
		).then(
			=>
				@parentView.hideLoading()
				@parentView.openEmailChangedPopup()
		).fail(
			(err) =>
				if err?.responseJSON?.errors?
					_.forEach err.responseJSON.errors, (err, field) =>
						field = 'new_email' if field is 'email'
						@raiseErr(field, err.messages)
				@parentView.hideLoading()
		)

	raiseErr: (field, messages) ->
		field = @$el.find('[name="' + field + '"]')
		if messages?
			msgHtml = ''
			_.each messages, (msg) =>
				msgHtml += "<p>#{msg}</p>"

			field.parents('.field')
				.addClass 'm-error'
				.find('.field--error').html msgHtml

			field.on 'focus.validation', ->
				field.parents('.field').removeClass('m-error')
				field.off('focus.validation')
		else
			field.parents('.field').removeClass 'm-error'
			field.prev('.field--error').html ''

module.exports = FormView

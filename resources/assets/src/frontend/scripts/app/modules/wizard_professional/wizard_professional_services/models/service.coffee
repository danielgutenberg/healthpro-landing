Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
getApiRoute = require 'hp.api'

ServiceMixins = require 'app/modules/professional_setup/mixins/service_model'

class Model extends Backbone.Model

	defaults:
		id: ''
		name: ''
		location_ids: []
		sessions: []
		packages_reccuring: []
		service_type_id: null 	# primary service
		service_type_ids: [] 	# secondary services
		certificates: 0
		description: ''

	validation:
		service_type_id: (value, field, computedState) ->
			return if computedState.id
			'Please select a service' unless value
		description:
			maxLength: 500
			msg: 'Your description is too long, please enter a maximum of 500 characters'

	initialize: ->
		Cocktail.mixin @, ServiceMixins
		@initSessions()

	save: ->
		method = 'post'
		url = getApiRoute('ajax-provider-service-add', {providerId: 'me'})

		params =
			name: @get('name')
			service_type_ids: @getServiceTypeIds()
			location_ids: @get('location_ids')
			sessions: @getSessionsJson()
			gift_certificates: @get('certificates')
			description: @get('description')
			_token: window.GLOBALS._TOKEN

		$.ajax
			url: url
			method: method
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify params
			show_alerts: false
			error: (jqXHR) =>
				@raiseAlertErrorFromResponse jqXHR.responseJSON

module.exports = Model

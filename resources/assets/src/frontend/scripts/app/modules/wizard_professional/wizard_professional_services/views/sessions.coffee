Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'click [data-sessions-add]': 'createSession'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@sessions = []

		@setOptions options
		@bind()

	bind: ->
		@listenToOnce @, 'rendered', =>
			@cacheDom()
			@renderAllSessions()
			@listenTo @collection, 'add', @addSession

	renderAllSessions: ->
		# render only active sessions
		@collection.models.forEach (model) => @addSession model if model.get('active')

	cacheDom: ->
		@$el.$list = @$el.find('[data-sessions]')

	render: ->
		@$el.html WizardProfessionalServices.Templates.Sessions()
		@trigger 'rendered'
		@

	createSession: ->
		@collection.add {}

	addSession: (model) ->
		@parentView.raiseSessionsError null

		@sessions.push session = new WizardProfessionalServices.Views.Session
			model: model
			collection: @collection
			parentView: @
			baseView: @baseView

		session.render().$el.appendTo @$el.$list

module.exports = View

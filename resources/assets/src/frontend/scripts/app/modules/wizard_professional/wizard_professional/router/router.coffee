Abstract = require 'app/modules/wizard/router/router'

PersonalDetailsWizard = require 'app/modules/wizard_professional/wizard_professional_information'
LocationsModule = require 'app/modules/wizard_professional/wizard_professional_locations'
ServicesModule = require 'app/modules/wizard_professional/wizard_professional_services'
EmailVerifyWizard = require 'app/modules/wizard_professional/wizard_professional_email_verify'

class Router extends Abstract

	routes:
		'wizard/profile_setup/schedule': 'locationsStep'
		'wizard/profile_setup/services': 'servicesStep'
		'wizard/profile_setup/personal_details': 'personalDetailsStep'
		'wizard/profile_setup/verify_email': 'emailVerifyStep'

	initialize: (options) ->
		super
		@wizardModel = options.model
		@wizardView = options.view

		@$dom =
			container: $('.wizard--container', @wizardView.$el)
			content: $('.wizard--content', @wizardView.$el)
			contentScroll: $('.wizard--content_scroll', @wizardView.$el)
			sidebar: $('.wizard--sidebar', @wizardView.$el)

		# array for all inited instances
		@instances = []

		@bind()
		@start()

	start: ->
		if @wizardView.place is 'page' then @openStep '' # dirty hack to start app on page
		@openStep @wizardModel.get('current_step.slug')

	openStep: (step) -> @navigate "wizard/profile_setup/#{step}", {trigger: true, replace: true}

	addStepClasses: ->
		@$dom.container.newClass = 'professional_setup_wizard m-no_sidebar'
		@$dom.container.addClass @$dom.container.newClass

		@$dom.sidebar.newClass = 'm-hide'
		@$dom.sidebar.addClass @$dom.sidebar.newClass


	## Step: 1
	servicesStep: ->
		@addGaTag 'wizard.professional.services'
		@addStepClasses()
		@wizardModel.set 'canGoAhead', false

		@instances.push new ServicesModule.App
			$container: @$dom.content
			profileId: @wizardModel.get('profileId')


  ## Step: 2
	locationsStep: ->
		@addGaTag 'wizard.professional.locations'
		@addStepClasses()
		@wizardModel.set 'canGoAhead', false

		@instances.push new LocationsModule.App
			$container: @$dom.content
			profileId: @wizardModel.get('profileId')


	## Step: 3
	personalDetailsStep: ->
		@addGaTag 'wizard.professional.personal_details'
		@addStepClasses()
		@wizardModel.set 'canGoAhead', false

		if @wizardModel.get('total_steps') is 3
			@wizardModel.set 'isLastStep', true

		@instances.push @personalDetails = new PersonalDetailsWizard.App
			$container: @$dom.content
			profileId: @wizardModel.get('profileId')

	## Step: 4
	emailVerifyStep: ->
		@addGaTag 'wizard.professional.email_verify'
		@addStepClasses()

		@instances.push @personalDetails = new EmailVerifyWizard.App
			$container: @$dom.content
			profileId: @wizardModel.get('profileId')

module.exports = Router

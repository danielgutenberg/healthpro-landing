Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	getServices: ->
		$.ajax
			url: getApiRoute('ajax-profile-provider-services', {providerId: 'me'})
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.forEach response.data, (item) => @addService item
				@trigger 'data:ready'

	addService: (item) ->
		# temp solution for introductory session in this branch
		return if item.name is ""

		primaryServiceId = _.findWhere(item.sub_services, {name: item.name}).id
		subServiceIds = _.without(_.pluck(item.sub_services, 'id'), primaryServiceId)

		@add
			id: item.service_id
			name: item.name
			sessions: item.sessions
			introductory: primaryServiceId is WizardProfessionalServices.Config.IntroductoryService.id
			location_ids: []
			service_type_id: primaryServiceId
			service_type_ids: subServiceIds

	findDuplicateServices: (serviceIds, serviceModel) ->
		return [] unless serviceIds

		serviceIds = [serviceIds] unless Array.isArray(serviceIds)

		@filter (model) ->
			return false if model.id is serviceModel.id
			_.intersection(serviceIds, model.getServiceTypeIds()).length > 0


module.exports = Collection

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'tooltipster'

class View extends Backbone.View

	className: 'professional_setup_wizard--inner'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@toggleGoAhead false
		@bind()
		@render()
		@cacheDom()
		@preload()

	cacheDom: ->
		@$body = $('body')
		@$el.$main = @$el.find('[data-view-main]')

	render: ->
		@$el.html(WizardProfessionalServices.Templates.Base()).appendTo @$container

	preload: ->
		@showLoading()
		$.when(
			@collections.serviceTypes.getServiceTypes(),
		).then(
			=> @collections.services.getServices()
		).then(
			=> @trigger 'data:loaded'
		)

	hideLoading: -> Wizard.ready()
	showLoading: -> Wizard.loading()

	bind: ->
		@listenToOnce @, 'data:loaded', =>
			@initAddService()
			@initTooltips()
			@toggleGoAhead true
			@hideLoading()

	initAddService: ->
		@addService.remove() if @addService
		@subViews.push @addService = new WizardProfessionalServices.Views.AddService
			collections: @collections
			baseView: @


	initServiceForm: ->
		@addService.remove() if @addService
		@addService = new WizardProfessionalServices.Views.ServiceForm
			$container: @$el.$main
			baseView: @


	initTooltips: ->
		@$el.find('[data-tooltip]').tooltipster
			theme: 'tooltipster-dark'
			maxWidth: 200


	toggleGoAhead: (enable = false) ->
		Wizard.model.set 'canGoAhead', enable

module.exports = View

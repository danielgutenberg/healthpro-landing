Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: ->
		super
		@fetchLocationsTypes()

	fetchLocationsTypes: ->
		$.ajax
			url: getApiRoute('ajax-location-types-get')
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				response.data.forEach (item) =>
					switch item.type
						when 'office'
							item.description = WizardProfessionalLocations.Config.locationDescriptions.office
							item.icon = WizardProfessionalLocations.Config.locationIcons.office
						when 'home-visit'
							item.description = WizardProfessionalLocations.Config.locationDescriptions.homeVisit
							item.icon = WizardProfessionalLocations.Config.locationIcons.homeVisit
						when 'virtual'
							item.description = WizardProfessionalLocations.Config.locationDescriptions.virtual
							item.icon = WizardProfessionalLocations.Config.locationIcons.virtual
					@push item
				@trigger 'data:ready'

module.exports = Collection

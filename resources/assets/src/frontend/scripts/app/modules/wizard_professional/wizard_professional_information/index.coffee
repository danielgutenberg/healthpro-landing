Backbone = require 'backbone'
Handlebars = require 'handlebars'

window.WizardProfessionalInformation =
	Config:
		messages:
			error: 'Something went wrong. Please reload the page and try again'
			phone:
				success: 'Your phone number has been verified'
				error: 'Your phone cannot be verified'
	Views:
		Base: require('./views/base')
		Profile: require('./views/profile')
		Userpic: require('./views/userpic')
		PhoneVerify: require('./views/phone_verify')
	Models:
		Profile: require('./models/profile')
		User: require('./models/user')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')
		Profile: Handlebars.compile require('text!./templates/profile.html')
		Userpic: Handlebars.compile require('text!./templates/userpic.html')
		PhoneVerify: Handlebars.compile require('text!./templates/phone_verify.html')

_.extend WizardProfessionalInformation, Backbone.Events

class WizardProfessionalInformation.App
	constructor: (options) ->
		@view = new WizardProfessionalInformation.Views.Base
			$container: options.$container
			profileId: options.profileId
			model: new WizardProfessionalInformation.Models.Profile()

	close: ->
		@view.close?()
		@view.remove?()

module.exports = WizardProfessionalInformation

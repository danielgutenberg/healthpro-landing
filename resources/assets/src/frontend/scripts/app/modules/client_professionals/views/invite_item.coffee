Backbone = require 'backbone'
Stickit = require 'backbone.stickit'
Moment = require 'moment'
Phone = require 'framework/phone'
MomentTimezone = require 'moment-timezone'

vexDialog = require 'vexDialog'
Tooltipster = require 'tooltipster'

class View extends Backbone.View

	className: 'profiles_list--table--row'
	tagName: 'tr'

	events:
		'click [data-invite-confirm]': 'confirmInvite'
		'click [data-invite-decline]': 'declineInvite'

	initialize: (@options) ->
		super

		@eventBus = @options.eventBus
		@model = @options.model
		@collections = @options.collections
		@parentView = @options.parentView

		@bind()
		@render()

	bind: ->
		@listenTo @model, 'remove', @destroy

		@bindings =
			'[data-name]': 'full_name'
			'[data-avatar]':
				observe: 'avatar'
				updateMethod: 'html'
				onGet: (val) => "<img src='#{val}' alt=''>" if val

			'[data-profile-link]':
				attributes: [
					name: 'href'
					observe: 'public_url'
				]

	render: ->
		@$el.html ClientProviders.Templates.InviteItem
		@parentView.$el.$listing.append @el
		@stickit()

		@$el.find('[data-tooltip]').tooltipster
			theme: 'tooltipster-dark'

		@trigger 'rendered'

	destroy: -> @$el.fadeOut 200, => @remove()

	confirmInvite: (e) ->
		e?.preventDefault()
		@parentView.showLoading()
		$.when(@model.confirmInvite()).then(
			(response) =>
				@collections.invites.remove(@model)
				@collections.profiles.trigger 'invite:confirmed' # updating the list of professionals
				return unless response.data is true
			(error) =>
				@parentView.hideLoading()
				alert ClientProviders.Config.Messages.error
		)

	declineInvite: (e) ->
		e?.preventDefault()
		vexDialog.confirm
			message: ClientProviders.Config.Messages.declineInvite
			callback: (value) =>
				return unless value
				@parentView.showLoading()
				$.when(@model.declineInvite()).then(
					(response) =>
						@collections.invites.remove(@model)
						return unless response.data is true
						@parentView.hideLoading()
					(error) =>
						@parentView.hideLoading()
						alert ClientProviders.Config.Messages.error
				)

module.exports = View

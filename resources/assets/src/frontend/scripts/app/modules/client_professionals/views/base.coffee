Backbone = require 'backbone'

ReviewProfessional = require 'app/modules/review_professional'

parseHash = require 'utils/parse_hash'
isNumeric = require 'utils/is_numeric'
vexDialog = require 'vexDialog'

getRoute = require 'hp.url'

class View extends Backbone.View

	el: $('[data-client-providers]')

	instances: {}

	initialize: (@options) ->
		super

		@eventBus = @options.eventBus
		@collections = @options.collections

		@cacheDom()
		@render()
		@bind()

	cacheDom: ->
		@$notification_link = $('[data-review-notification-link]') # link to open review popup from notification sidebar
		@$el.$app = @$el.find('.profiles_list--app')
		@$el.$header = @$el.find('.profiles_list--header')
		@$el.$invites = @$el.find('.profiles_list--invites')
		@$el.$invites.$app = @$el.find('.profiles_list--invites--app', @$el.$invites)

		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-cog"></span></span>')
		@$el.append @$el.$loading

	render: ->
		@instances.header = new ClientProviders.Views.Header
			eventBus: @eventBus
			collections: @collections
			el: @$el.$header

		@instances.profilesList = new ClientProviders.Views.ProfilesList
			eventBus: @eventBus
			collections: @collections
			baseView: @
		@$el.$app.append @instances.profilesList.el

		@instances.invitesList = new ClientProviders.Views.InvitesList
			eventBus: @eventBus
			collections: @collections
			baseView: @
			container: @$el.$invites
		@$el.$invites.$app.append @instances.invitesList.el

		@trigger 'rendered'

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

	showLoading: ->
		@$el.$loading.removeClass('m-hide')

	bind: ->
		@listenTo @collections.profiles, 'data:ready', @afterDataLoaded

		$(window).on 'hashchange', (e) =>
			@maybeInitReview()

	afterDataLoaded: ->
		@hideLoading()
		@maybeInitReview()

	# open popup with review
	reviewProfessional: (options, callback = null) ->

		# close another review if it is open for some reason
		@review.close?() if @review

		defaults =
			profileId: null
			professionalDetails: null
			showCloseButton: true
			redirectUrl: null
			isPopup: true

		@review = new ReviewProfessional.App _.merge(defaults, options)

		if _.isFunction callback
			@listenToOnce ReviewProfessional, 'review:posted', callback


	maybeInitReview: (event) ->

		hash = parseHash true
		return unless hash.review

		redirectUrl = null
		profile = null

		# we have the ID
		if isNumeric(hash.review)
			profile = @collections.profiles.findWhere
				id: parseInt(hash.review)
			if !profile
				profile = @collections.invites.findWhere
					id: parseInt(hash.review)

				profile.set('can_review', true) if profile

		else
			profile = @collections.profiles.findWhere
				slug: _.trim(hash.review)
			if !profile
				profile = @collections.invites.findWhere
					slug: _.trim(hash.review)

				profile.set('can_review', true) if profile


		# no profile found or the found profile cannot be reviewed
		if !profile or !profile.get('can_review')
			@raiseError 'You are not permitted to review this professional'
			return

		# Do not redirect user after completing a review
		# if slug = profile.get('slug')
		# 	redirectUrl = getRoute('professional-public-profile-url', {slug: slug})
		# else
		# 	redirectUrl = getRoute('professional-public-profile-url', {slug: profile.get('id')})

		@reviewProfessional
			profileId: profile.get('id')
			professionalDetails: profile.toJSON()
			showCloseButton: false
			redirectUrl: redirectUrl

	raiseError: (msg) ->
		vexDialog.alert
			message: msg


module.exports = View

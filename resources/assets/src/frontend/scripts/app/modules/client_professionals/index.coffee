Handlebars = require 'handlebars'
Backbone = require 'backbone'

# views
BaseView = require './views/base'
HeaderView = require './views/header'
ProfilesListView = require './views/profiles_list'
ProfileItemView = require './views/profile_item'
InvitesListView = require './views/invites_list'
InviteItemView = require './views/invite_item'

# models
ProfileModel = require './models/profile'
InviteModel = require './models/invite'

# collections
ProfilesCollection = require './collections/profiles'
InvitesCollection = require './collections/invites'

#templates
HeaderTemplate = require 'text!./templates/header.html'
ProfilesListTemplate = require 'text!./templates/profiles_list.html'
ProfileItemTemplate = require 'text!./templates/profile_item.html'
InvitesListTemplate = require 'text!./templates/invites_list.html'
InviteItemTemplate = require 'text!./templates/invite_item.html'


window.ClientProviders =
	Config:
		CancellationPeriod: 24 # hours
		Messages:
			error: 'Something went wrong. Please reload the page and try again.'
			cancelAppointment: 'Are you sure you want to cancel this appointment?'
			cancelAppointmentNoRefund: '<span class="m-error">This appointment is in less then 24 hours. Cancelling this appointment will not give you a refund.</span> Do you want to cancel anyway?'
			cancelAppointmentNoRefundInPerson: 'As the appointment you are canceling is less than 24 hours from now, you will incur charges from your professional according to his or her terms of service. Do you want to cancel anyway?'
			confirmAppointment: 'You are about to confirm an appointment. Click OK to confirm.'
			declineInvite: 'Are you sure you want to decline professional\'s invitation? <p>You will still be able to book appointments with this professional in the future.</p>'

	Views:
		Base: BaseView
		Header: HeaderView
		ProfilesList: ProfilesListView
		ProfileItem: ProfileItemView
		InvitesList: InvitesListView
		InviteItem: InviteItemView
	Collections:
		Profiles: ProfilesCollection
		Invites: InvitesCollection
	Models:
		Profile: ProfileModel
		Invite: InviteModel
	Templates:
		Header:  Handlebars.compile HeaderTemplate
		ProfilesList:  Handlebars.compile ProfilesListTemplate
		ProfileItem:  Handlebars.compile ProfileItemTemplate
		InvitesList:  Handlebars.compile InvitesListTemplate
		InviteItem:  Handlebars.compile InviteItemTemplate

_.extend ClientProviders, Backbone.Events

class ClientProviders.App
	constructor: ->

		@eventBus = _.extend {}, Backbone.Events

		@collections =
			profiles: new ClientProviders.Collections.Profiles [],
				model: ClientProviders.Models.Profile
				eventBus: @eventBus
			invites: new ClientProviders.Collections.Invites [],
				model: ClientProviders.Models.Invite
				eventBus: @eventBus

		@views =
			base: new ClientProviders.Views.Base
				eventBus: @eventBus
				collections: @collections

ClientProviders.app = new ClientProviders.App()

module.exports = ClientProviders

Backbone = require 'backbone'

class View extends Backbone.View

	events:
		'keyup [data-profiles-search]': 'search'
		'change [data-profiles-sort]': 'sort'

	initialize: (@options) ->
		super

		@eventBus = @options.eventBus
		@collections = @options.collections

		@render()
		@cacheDom()
		@

	render: ->
		@$el.html ClientProviders.Templates.Header

	cacheDom: ->
		@$el.$search = $('[data-profiles-search]')
		@$el.$sort = $('[data-profiles-sort]')

		@fillSelect()

	sort: (e) ->
		@$el.$search.val ''
		@collections.profiles.order e.target.value

	search: (e) ->
		@collections.profiles.filter e.target.value

	fillSelect: ->
		_.each @collections.profiles.sortOptions, (label, value) =>
			@$el.$sort.append "<option value='#{value}'>#{label}</option>"

module.exports = View

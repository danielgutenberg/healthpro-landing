DeepModel = require 'backbone.deepmodel'
getApiRoute = require 'hp.api'
getUrl = require 'hp.url'
Moment = require 'moment'

class ProfileModel extends DeepModel

	attributes:
		is_hidden: false
		appointment_id: null
		is_searchable: true

	initialize: ->
		super
		@bind()

	bind: ->
		@listenTo @, 'load:availability', @loadAvailability

	reviewUrl: -> getUrl 'professional-review', {slug: @get('slug')}

	messageUrl: -> getUrl 'dashboard-conversation', {profileId: @get('id')}

	cancelAppointment: ->
		appointment = @get('next_appointment')
		return unless appointment
		$.ajax
			url: getApiRoute('ajax-appointment', appointmentId: appointment.id)
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

	confirmAppointment: ->
		appointment = @get('next_appointment')
		return unless appointment
		$.ajax
			url: getApiRoute('ajax-appointment-confirm', {
				appointmentId: appointment.id
			})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: (response) =>
				@set 'next_appointment.status', 'confirmed' if response.data

	reloadData: ->
		$.ajax
			url: getApiRoute('ajax-client-provider', {clientId: 'me', providerId: @get('id')})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data:
				return_fields: 'all'
			success: (response) =>
				return unless response.data
				# we don't want to change the ID
				delete response.data.id
				@set response.data

	loadAvailability: ->
		return if @get('next_appointment')
		lastAppointment = @get('last_appointment')
		services = @get('services')

		locationId = do ->
			return '' unless lastAppointment?
			if lastAppointment.location.status == 'deleted'
				null
			else
				if lastAppointment.location.location_type is 'home-visit'
					lastAppointment.registration.location_id
				else
					lastAppointment.location_id

		data =
			from: Moment().format('YYYY-MM-DD')
			until: Moment().add(7, 'days').format('YYYY-MM-DD')

		if locationId
			data['location_id'] = locationId

		return unless @.get('is_searchable')

		$.ajax
			url: getApiRoute('ajax-provider-availabilities', {providerId: @get('id')})
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: data
			success: (response) =>
				return unless response.data?.results?
				return if _.isEmpty response.data.results[0].availabilities
				@setNextAvailability response.data.results[0]

	setNextAvailability: (nextAvailabilities) ->
		day = _.min _.keys(nextAvailabilities.availabilities), (key) =>
			Moment(key)
		nextAvailability = nextAvailabilities.availabilities[day][0]

		_.each nextAvailabilities.availabilities, (availabilities) ->
			return if nextAvailability
			_.each availabilities, (availability) ->
				return if nextAvailability
				nextAvailability = availability

		if nextAvailability
			nextAvailability.from = Moment.parseZone(nextAvailability.from)
			nextAvailability.until = Moment.parseZone(nextAvailability.until)
			nextAvailability.location = _.findWhere nextAvailabilities.locations, {id: nextAvailability.location_id}

		@set 'next_availability', nextAvailability
		@

	formatAppointmentForBookingConfirmation: ->
		appointment = @get('next_appointment')
		return unless appointment
		return if appointment.status is 'confirmed'

		sessions =  _.findWhere(@get('services'), service_id: appointment.service.service_id).sessions
		session = _.findWhere(sessions, duration: appointment.duration)

		data =
			appointment_id: appointment.id
			date: Moment(appointment.date).format('YYYY-MM-DD')
			from: Moment(appointment.from).format()
			resetToFirstStep: 'recalculate'
			bookedFrom: 'widget'
			provider_id: @get('id')
			appointment_from_dashboard: true
			services: @get('services')
			selected_time:
				from: appointment.from
				until: appointment.until
				location_id: appointment.location.id
				location_name: appointment.location.name
				service_name: appointment.service.name
				service_id: appointment.service.service_id
				session_id: if session then session.session_id else null
				duration: appointment.duration
				price: appointment.price
				appointment_id: appointment.id

		data

	bookAppointment: ->
		nextAvailability = @get('next_availability')
		return null unless nextAvailability?
		serviceData = @findSessionAndService(nextAvailability)

		$.ajax
			url: getApiRoute('ajax-appointments')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				session_id: serviceData.session.session_id
				from: nextAvailability.from.format()
				availability_id: nextAvailability.id
				type: 'client'
				_token: window.GLOBALS._TOKEN
			success: (response) =>
				@set('next_availability.appointment_id', response.data.id)


	getBookingData: ->
		nextAvailability = @get('next_availability')
		return null unless nextAvailability

		serviceData = @findSessionAndService(nextAvailability)

		bookingData =
			bookedFrom: 'widget'
			resetToFirstStep: 'recalculate'
			provider_id: @get('id')
			availability_id: nextAvailability.id
			session_id: serviceData.session.session_id
			services: @get('services')
			from: nextAvailability.from.format()
			date: nextAvailability.from.format('YYYY-MM-DD')
			selected_time:
				availability_id: nextAvailability.id
				from: nextAvailability.from.format()
				until: nextAvailability.until.format()
				location_id: nextAvailability.location_id
				location_name: nextAvailability.location.name
				service_id: serviceData.service.service_id
				service_name: serviceData.service.name
				session_id: serviceData.session.session_id
				duration: serviceData.session.duration
				price: serviceData.session.price

		bookingData

	findSessionAndService: ->
		nextAvailability = @get('next_availability')
		return null unless nextAvailability

		lastAppointment = @get('last_appointment')

		sessionId = null
		serviceId = null


		if lastAppointment?.service_id and _.findWhere(@get('services'), service_id: lastAppointment.service_id)?
			serviceId = lastAppointment.service_id

		_.each nextAvailability.services, (sessions, service_id) ->
			return if sessionId
			return if serviceId != null and service_id != serviceId
			if sessions and sessions.length
				sessionId = _.first sessions
				serviceId = service_id
		return null unless sessionId

		service = _.findWhere @get('services'), service_id: serviceId
		session = _.findWhere service.sessions, session_id: sessionId

		service: service
		session: session

	removeProfessional: ->
		$.ajax
			url: getApiRoute('ajax-client-provider-decline', { 'clientId': 'me', 'providerId': @.get 'id' })
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

module.exports = ProfileModel

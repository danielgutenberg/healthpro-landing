Backbone = require 'backbone'

class View extends Backbone.View

	initialize: (@options) ->
		super

		@eventBus = @options.eventBus
		@collections = @options.collections
		@baseView = @options.baseView

		@subViews = []

		@bind()
		@render()
		@cacheDom()
		@

	bind: ->
		@listenTo @collections.profiles, 'sort', @renderProfiles

	cacheDom: ->
		@$el.$listing = $('[data-listing]', @$el)

	render: ->
		@$el.html ClientProviders.Templates.ProfilesList

	renderProfile: (model) ->
		@subViews.push new ClientProviders.Views.ProfileItem
			parentView: @
			baseView: @baseView
			model: model
			collections: @collections
			eventBus: @eventBus

	renderProfiles: ->
		# we should update this method later to re-sort without re-render
		if @subViews.length
			_.each @subViews, (view) ->
				view.remove()
			@subViews = []
		@collections.profiles.each @renderProfile, @

module.exports = View

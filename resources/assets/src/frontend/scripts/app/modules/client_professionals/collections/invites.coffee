Backbone = require 'backbone'

getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: ->
		super
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-clients-invites-get', {clientId: 'me'})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.each response.data, (item) =>
					@push item
				@trigger 'data:ready'

module.exports = Collection

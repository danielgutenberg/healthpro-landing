Backbone = require 'backbone'
DeepModel = require 'backbone.deepmodel'
getApiRoute = require 'hp.api'

Moment = require 'moment'

class InviteModel extends DeepModel

	initialize: ->
		super

	confirmInvite: ->
		$.ajax
			url: getApiRoute('ajax-client-provider-confirm', { 'clientId': 'me', 'providerId': @.get 'id' })
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

	declineInvite: ->
		$.ajax
			url: getApiRoute('ajax-client-provider-decline', { 'clientId': 'me', 'providerId': @.get 'id' })
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

module.exports = InviteModel

Backbone = require 'backbone'
Moment = require 'moment'
vexDialog = require 'vexDialog'
LocationFormatter = require 'utils/formatters/location'
ServiceFormatter = require 'utils/formatters/service'

require 'backbone.stickit'
require 'moment-timezone'
require 'tooltipster'

class View extends Backbone.View

	review: null

	className: 'profiles_list--listing--item'

	events:
		'click [data-cancel-link]': 'cancelAppointment'
		'click [data-confirm-link]': 'confirmAppointment'
		'click [data-remove-link]': 'removeProfessional'
		# 'click [data-review-link]': 'reviewProfessional'


	initialize: (@options) ->
		super

		@eventBus = @options.eventBus
		@model = @options.model
		@collections = @options.collections
		@parentView = @options.parentView
		@baseView = @options.baseView

		@bind()
		@render()
		@loadAvailability()

	bind: ->
		@listenTo @, 'loading:show', @showLoading
		@listenTo @, 'loading:hide', @hideLoading

		@listenTo @model, 'remove', @destroy
		@listenTo @model, 'change:is_hidden', @toggleHidden

		@bindings =

			'[data-reviews-rating]':
				attributes: [
					name: 'class'
					observe: 'ratings.overall'
					onGet: (val) =>
						val = (Math.round(parseInt(val, 10) * 2) / 2).toString().replace('.', '_')
						"star_rating m-s m-#{val}"

				]

			'[data-reviews-count]':
				observe: 'reviews'
				onGet: (val) ->
					total = if Array.isArray(val) then val.length else 'no'
					"(#{total} review" + (if total == 1 then "" else "s") + ")"

			'[data-next-availability]':
				observe: 'next_availability'
				visible: @isVisible

			'[data-next-availability-date]':
				observe: 'next_availability.from'
				onGet: (val) ->
					return '' unless val
					val.format('MMM D, YYYY')

			'[data-next-availability-time]':
				observe: ['next_availability.from', 'next_availability.location.address.postal_code.timezone', 'next_availability.location.virtual_address.timezone']
				onGet: (val) =>
					return '' unless val[0]
					timezone = if val[1] then val[1] else val[2]
					return '' unless timezone
					val[0].format('hh:mm A') + ' ' + Moment().tz(timezone).format('z')

			'[data-next-availability-location]': 'next_availability.location.name'
			'[data-next-availability-address]':
				updateMethod: 'html'
				observe: 'next_availability.location.address'
				onGet: (val) =>
					return '' unless val
					"<p>" + LocationFormatter.getLocationAddressLabel(@model.get('next_availability.location')) + "</p>"

			'[data-last-appointment]':
				observe: 'last_appointment'
				visible: @isVisible

			'[data-last-appointment-time]':
				observe: 'last_appointment.date'
				onGet: @onGetDate

			'[data-next-appointment-date]':
				observe: 'next_appointment.date'
				updateMethod: 'html'
				onGet: @onGetDate

			'[data-next-appointment]':
				observe: 'next_appointment'
				visible: @isVisible

			'[data-cancel-appointment]':
				observe: 'next_appointment'
				visible: (val) -> val?.status is 'confirmed'

			'[data-not-available]':
				observe: 'is_searchable'
				onGet: (val) ->
					return 'This Professional is Currently Not Bookable' if !val
					''

			'[data-confirm-appointment]':
				observe: 'next_appointment'
				visible: (val) ->
					return false unless val
					return true if val.status is 'pending' and val.initiated_by is 'provider'

			'[data-waiting-appointment-confirmation]':
				observe: 'next_appointment'
				visible: (val) ->
					return false unless val
					return true if val.status is 'pending' and val.initiated_by is 'client'

			'[data-next-appointment-time]':
				observe: 'next_appointment'
				updateMethod: 'html'
				onGet: (val) ->
					return '' unless val
					Moment(val.from).format('hh:mm A') + ' - ' + Moment(val.until).format('hh:mm A') + ' ' + val.location.address.timezone.replace(/_/g, ' ')

			'[data-next-appointment-service]': 'next_appointment.service.detailed_name'
			'[data-next-appointment-location]': 'next_appointment.location.name'

			'[data-next-appointment-address]':
				updateMethod: 'html'
				observe: 'next_appointment.location.address'
				onGet: (val) =>
					return '' unless val
					"<p>" + LocationFormatter.getLocationAddressLabel(@model.get('next_appointment.location')) + "</p>"

			'[data-next-appointment-home-visit]':
				observe: 'next_appointment.location.location_type'
				updateMethod: 'html'
				onGet: (val) ->
					return '' unless val is 'home-visit'
					"<span>home visit</span>"
				classes:
					"m-hide":
						observe: 'next_appointment.location.location_type'
						onGet: (val) -> val isnt 'home-visit'

			'[data-packages]':
				classes:
					'm-hide':
						observe: 'available_packages'
						onGet: (packages) ->
							return true if packages.length is 0
							pkgs = _.filter packages, (pkg) -> (pkg.entities_left > 0) and (pkg.number_of_entities > -1)
							pkgs.length is 0
				updateMethod: 'html'
				observe: 'available_packages'
				onGet: @formatAvailablePackages

			'[data-review-link]':
				classes:
					'm-hide':
						observe: 'can_review'
						onGet: (val) ->
							!val

	render: ->
		@$el.html ClientProviders.Templates.ProfileItem @getTemplateData()
		@parentView.$el.$listing.append @el
		@stickit()
		@$el.find('[data-tooltip]').tooltipster
			theme: 'tooltipster-dark'

		@$el.$loading = $('<span class="loading_overlay m-light m-hide"><span class="spin m-cog"></span></span>')
		@$el.append @$el.$loading

		@trigger 'rendered'

	getTemplateData: ->
		{
			profileUrl: @model.get('public_url')
			fullName: @model.get('full_name')
			avatar: @model.get('avatar')
			isSearchable: @model.get('is_searchable')
			messageUrl: @model.messageUrl()
			reviewUrl: @model.reviewUrl()

			canReview: @model.get('can_review')
		}

	onGetDate: (val) ->
		return '' unless val
		Moment(val).format('MMM D, YYYY')

	isVisible: (val) -> val

	cancelAppointment: (e) ->
		e.preventDefault() if e
		vexDialog.buttons.YES.text = 'Yes'
		vexDialog.buttons.NO.text = 'No'
		vexDialog.confirm
			message: @getCancellationMessage()
			callback: (value) =>
				return unless value
				@trigger 'loading:show'
				$.when(@model.cancelAppointment()).then(
					(response) =>
						@trigger 'loading:hide'
						return unless response.data is true

						@model.set 'next_appointment', null
						@stickit() # we need to call stickit method again because it won't update with Backbone.DeepModel

						@reloadModelData()
				,
					(error) =>
						@trigger 'loading:hide'
						alert ClientProviders.Config.Messages.error
				)

	confirmAppointment: (e) ->
		e?.preventDefault()
		# temporarily redriect to profile page to open wizard to solve bug
		window.location = @model.get('next_appointment').confirmLink
#		window.bookingHelper.openWizard
#			bookingData: @model.formatAppointmentForBookingConfirmation()

#	bookAppointment: ->
#		window.bookingHelper.openWizard
#			bookingData: @model.getBookingData()

	destroy: -> @$el.fadeOut 200, => @remove()

	toggleHidden: ->
		if @model.get('is_hidden')
			@$el.addClass('m-hide')
		else
			@$el.removeClass('m-hide')

	reloadModelData: ->
		@trigger 'loading:show'
		$.when(@model.reloadData()).then(
			() =>
				@trigger 'loading:hide'
				@stickit() # we need to call stickit method again because it won't update with Backbone.DeepModel
				@loadAvailability() # load availability if needed
		,
			(error) =>
				@trigger 'loading:hide'
		)

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

	showLoading: ->
		@$el.$loading.removeClass('m-hide')

	loadAvailability: ->
		@trigger 'loading:show'
		$.when(@model.loadAvailability()).then(
			=>
				@trigger 'loading:hide'
		, (error) =>
			@trigger 'loading:hide'
		)

	reviewProfessional: (e) ->
		e?.preventDefault()
		return unless @model.get('can_review')

		@baseView.reviewProfessional
			profileId: @model.get('id')
			professionalDetails: @model.toJSON()
		, =>
			@afterReviewPosted()

	afterReviewPosted: ->
		# hide review button
		@model.set 'can_review', false

		# reload data to update reviews
		@reloadModelData()

	removeProfessional: (e) ->
		e?.preventDefault()
		vexDialog.buttons.YES.text = 'Yes'
		vexDialog.buttons.NO.text = 'No'
		vexDialog.confirm
			message: 'Are you sure you want to remove this professional?'
			callback: (value) =>
				return unless value
				@trigger 'loading:show'
				$.when(@model.removeProfessional()).then(
					(response) =>
						@collections.profiles.remove(@model)
						return unless response.data is true
				,
					(error) =>
						@trigger 'loading:hide'
						alert ClientProviders.Config.Messages.error
				)

	getCancellationMessage: ->
		message = ClientProviders.Config.Messages.cancelAppointment
		if appointment = @model.get('next_appointment')
			difference = Moment(appointment.from).diff(Moment(), 'hours')
			if difference < ClientProviders.Config.CancellationPeriod
				if appointment.payInPerson?
					message = ClientProviders.Config.Messages.cancelAppointmentNoRefundInPerson
				else
					message = ClientProviders.Config.Messages.cancelAppointmentNoRefund

		message

	formatAvailablePackages: (packages) ->
		html = ''
		_.each packages, (pkg) =>
			return if pkg.entities_left is 0
			return if pkg.number_of_entities < 0

			service = _.find @model.get('services'), (s) =>
				sessionIds = _.pluck s.sessions, 'session_id'
				_.indexOf(sessionIds, pkg.entity_id) > -1

			serviceName = if service then service.name.toLowerCase() else 'session'

			html += '<p>'
			html += "You have #{pkg.entities_left} out of #{pkg.number_of_entities} #{serviceName} credits"
			html += '</p>'
		html

module.exports = View

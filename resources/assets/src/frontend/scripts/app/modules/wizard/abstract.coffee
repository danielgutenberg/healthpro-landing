Backbone = require 'backbone'

LoadingStack = require 'utils/loading_stack'
DataFormatter = require 'utils/wizard_data_formatter'

getRoute = require 'hp.url'

class WizardAbstract

	defaults : -> {}

	constructor : (options = null) ->
		_.extend @, Backbone.Events
		@setOptions options if options

		@loadingStack = new LoadingStack @

		@formatter = DataFormatter

	init : -> @

	setOptions : (options = {}) ->
		_.defaults options, @defaults()
		@options = options
		@

	isPopup : -> !@isPage()

	isPage : -> @options.el.hasClass('m-page')

	close : ->
		@inited = false
		@options = null

		@model?.destroy?()
		@model = null

		@view?.close?()
		@view = null

		@router = null

		@off()

		Backbone.history.stop()
		Backbone.history.start(pushState : true)
		@

	addListeners : ->
		if @shouldRedirectToPage()
			@listenToOnce @, 'initial_data:ready', =>
				@redirect getRoute('wizard', {
					wizard : @model.get('wizard_name'),
					step   : @model.get('current_step.slug')
				})
		else
			@listenToOnce @, 'booking_data:ready', => @initRouter()
		@

	shouldRedirectToPage : -> @options.forcePage and !@isPage()

	initRouter : ->
		Backbone.history.start(pushState : true) unless Backbone.History.started
		@

	redirect : (url) ->
		window.location.href = url
		@

	showWizard : ->
		@view.showWizard()
		@

	# helper methods
	loading : (args...) -> @loadingStack.loading args
	ready   : (args...) -> @loadingStack.ready args
	reset   : (args...) -> @loadingStack.reset args

module.exports = WizardAbstract

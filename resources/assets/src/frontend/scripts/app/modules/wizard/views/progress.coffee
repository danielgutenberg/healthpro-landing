Backbone = require 'backbone'

Handlebars = require 'handlebars'
TemplateProgress = Handlebars.compile require('text!../templates/progress.html')

require 'backbone.stickit'

class Progress extends Backbone.View

	className: 'wizard--progress progress m-on_blue'

	initialize: (options) ->
		@model = options.model
		@addStickit()
		@bind()

	bind: ->
		@listenToOnce @, 'rendered', => @stickit()

	addStickit: ->
		@bindings =
			'[data-progress-value]':
				attributes: [
					{
						name: 'style'
						observe: ['total_steps', 'current_step.number']
						onGet: (values) =>
							completed = 100 / values[0] * values[1]
							"width: #{completed}%"

					}
				]

	render: ->
		@$el.append TemplateProgress()
		@trigger 'rendered'
		@

module.exports = Progress

Backbone = require 'backbone'

isMobile = require 'isMobile'
vexDialog = require 'vexDialog'

Handlebars = require 'handlebars'
TemplateLayout = Handlebars.compile require('text!../templates/layout.html')

ExitView = require('./exit')
ProgressView = require('./progress')

require 'backbone.stickit'

class MainView extends Backbone.View

	events:
		'click .wizard--next_step': 'checkStep'
		'click .wizard--prev_step': 'prevStep'
		'click .wizard--save_data': 'saveData'
		'click .wizard--finish': 'submitWizard'
		'click .wizard--back': 'triggerBackEvent'

	initialize: (options) ->
		@model = options.model
		@subViews = []

		# where is popup located. popup or page
		@place = null

		# set name attr
		@$el.attr 'data-wizard', @model.get('wizard_name')

		@startup()

	startup: ->
		@bind()
		@addStickit()
		@detectPlace()
		@renderLayout()
		@appendExitButton()
		@stickit()

	addStickit: ->
		@bindings =
			'[data-wizard-title]':
				observe: 'title'
			':el':
				classes:
					'm-success': 'isSuccessStep'
			'.wizard--finish':
				classes:
					'm-hide':
						observe: 'isLastStep'
						onGet: (val) -> return if val? then !val else val
				attributes: [{
					name: 'disabled'
					observe: 'canFinish'
					onGet: (val) -> if val? and val then !val else true
				}]

			'.wizard--next_step':
				classes:
					'm-hide': 'isLastStep'
				attributes: [{
					name: 'disabled'
					observe: 'canGoAhead'
					onGet: (val) -> if val? and val then !val else true
				}]
			'.wizard--prev_step':
				classes:
					'm-hide':
						observe: ['previous_step', 'disableBack']
						onGet: (val) ->
							return true if val[1] is true
							return if val[0]? then false else true

	bind: ->
		@listenTo Wizard, 'stack:loading', => @showLoading()
		@listenTo Wizard, 'stack:ready', => @hideLoading()
		@listenTo Wizard, 'step:changed', (step) =>
			# remove exit button on the success step
			@removeExitButton() if step is 'success'

	renderLayout: ->

		@$el.html TemplateLayout()

		@cacheDom()
		@renderProgress()

		@appendFooter() if _.isFunction @appendFooter

		@centerPopup()

	centerPopup: ->
		if @place is 'popup'
			$(document).trigger 'popup:center'

	cacheDom: ->
		@$el.$container = $('[data-wizard-container]', @$el)
		@$el.$containerWrap = $('[data-wizard-container-wrap]', @$el)
		@$el.$contentWrap = $('[data-wizard-content-wrap]', @$el)
		@$el.$contentScroll = $('[data-wizard-content-scroll]', @$el)
		@$el.$content = $('[data-wizard-content]', @$el)
		@$el.$header = $('[ data-wizard-header]', @$el)
		@$el.$sidebar = $('[data-wizard-sidebar]', @$el)
		@$el.$loading = $('[data-wizard-loading]', @$el)

	renderProgress: ->
		@subViews.push @progressView = new ProgressView
			model: @model

		@$el.$header.append @progressView.render().el

	showWizard: ->
		if @place is 'popup' and !isMobile.any # don't show popup on mobile
			@setCurrentUrl()
			window.popupsManager.openPopup @$el.parents('.popup').attr('id')

	# detect where is wizard. popup or page
	detectPlace: ->
		if @$el.closest('.popup').length
			@$el.addClass('m-popup')
			@place = 'popup'
		else
			@$el.addClass('m-page')
			@place = 'page'

	checkStep: (e) ->
		e.preventDefault()
		Wizard.trigger 'check_step', @model

	nextStep: (e) ->
		e.preventDefault()
		@resetScroll()
		Wizard.trigger 'next_step', @model

	prevStep: (e) ->
		e.preventDefault()
		@resetScroll()
		Wizard.trigger 'prev_step', @model

	saveData: (e) ->
		e.preventDefault()
		Wizard.trigger 'save_data', @model

	submitWizard: (e) ->
		e.preventDefault()
		Wizard.trigger 'submit_wizard', @model

	showLoading: ->
		@$el.$loading.show()
		@$el.$containerWrap.addClass 'm-loading_show'

	hideLoading: ->
		@$el.$loading.hide()
		@$el.$containerWrap.removeClass 'm-loading_show'

	setCurrentUrl: ->
		@previousUrl = window.location.href

	returnPreviousUrl: ->
		window.history.pushState {}, '', @previousUrl

	resetScroll: ->
		# while we don't reload the page we should go to the top when changing step
		$(window).scrollTop 0

	removeEvents: ->
		@stopListening()
		@undelegateEvents()

	removeSubViews: ->
		if @subViews?.length
			_.forEach @subViews, (view) ->
				if view?
					view.close?()
					view.remove?()
					view = null

		@subViews = []

	show: ->
		@delegateEvents()
		return @ unless @subViews?.length
		_.forEach @subViews, (view) -> view.delegateEvents?() if view?
		@

	close: ->
		@returnPreviousUrl() if @previousUrl?
		@removeSubViews()
		@removeEvents()
		@$el.empty().removeData().unbind()

		@redirect()
		@remove()

	redirect: ->
		if @model.get('isSuccessStep') and @model.get('wizard_name') is 'profile'
			location.href = window.location.origin + '/dashboard'

		if @model.get('isSuccessStep') and @model.get('wizard_name') is 'booking'
			# if wizard opened up from link in email then reload homepage otherwise, reload the current page
			if window.location.pathname.indexOf('/appointment') == 0
				location.href = window.location.origin
			else
				window.location.reload(true)

		if @model.get('isSuccessStep') and @model.get('wizard_name') is 'gift_certificate'
			location.href = window.location.origin + '/dashboard'

		@

	triggerBackEvent: (e) ->
		# this action triggers a helper event for mobile devices.
		# currenty used in locations_wizard and services_wizard modules.
		return unless $('body').hasClass 'm-wizard_edit'

		Wizard.trigger 'current_step_back'

	appendExitButton: ->
		return unless @place is 'page'
		@subViews.push @exitView = new ExitView
			baseView: @
		@setExitWizardParams()
		@

	removeExitButton: ->
		return unless @place is 'page'
		@subViews.splice @subViews.indexOf(@exitView), 1
		@exitView.remove()
		@exitView = null
		@

	setExitWizardParams: -> @

module.exports = MainView

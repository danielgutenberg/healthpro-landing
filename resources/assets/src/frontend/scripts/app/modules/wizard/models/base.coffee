DeepModel = require 'backbone.deepmodel'
getApiRoute = require 'hp.api'

class Model extends DeepModel

	defaults: ->
		return {
			wizard_name: ''
			title: ''
			current_step:
				number: 1
				slug: null
			total_steps: 1
			steps: null
			data: null

			# can user go to the next step without adding information
			canGoAhead: true
			canFinish: true
			isLastStep: false
			isSuccessStep: false
			isFirstLoad: true
			isSwitchedAppointment: false

			# can user go to the previous step without adding information
			canGoBack: true
		}

	bind: ->
		###
  			triggers on sign up / login step to request next wizard step
		###
		@listenTo Wizard, 'request_next_step', =>
			$.when(
				@saveWizardData()
			).then(
				=> @requestNextStep()
			)

		###
			get next wizard step
		###
		@listenTo Wizard, 'save_step', => @saveWizardData()

		###
			save wizard data without changing the step
		###
		@listenTo Wizard, 'next_step', =>
			$.when(
				@saveWizardData()
			).then(
				=> @getNextStep()
			)

		###
  			get previous wizard step
		###
		@listenTo Wizard, 'prev_step', => @getPreviousStep()

	initWizard: ->
		$.when(
			Wizard.loading()
		).then(
			=> @getInitialWizardData()
		).then(
			=> @getCurrentStepData()
		).then(
			=> @getAdditionalWizardData()
		).then(
			=> Wizard.ready()
		)

	# load initial data
	getInitialWizardData: ->
		Wizard.trigger 'initial_data:loading'
		Wizard.loading()
		@xhr = $.ajax
			method: 'get'
			url: getApiRoute(@urls.initial)
			success: (response) =>
				@setData response
				@set 'isFirstLoad', false
				Wizard.trigger 'initial_data:ready'
				Wizard.ready()
				return

	# sends wizard data to the server
	saveWizardData: ->
		# do nothing if no data present
		return unless @has('data')
		Wizard.loading()
		@xhr = $.ajax
			url: getApiRoute @urls.initial
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				data: JSON.stringify @get('data') # data needs to be a string
				_token: window.GLOBALS._TOKEN
			success: =>
				Wizard.ready()
				return

	resetWizardData: ->
		@set 'data', {}
		@saveWizardData()

	# get current step data
	getCurrentStepData: ->
		Wizard.loading()
		@xhr = $.ajax
			method: 'get'
			url: getApiRoute @urls.step, step: @get('current_step.slug')
			success: (response) =>
				@setData response
				Wizard.ready()
				return

	# get next step data
	getNextStep: ->
		Wizard.loading()
		@xhr = $.ajax
			method: 'get'
			url: getApiRoute @urls.step, step: @get('next_step')
			success: (response) =>
				@setData response
				Wizard.ready()
				return

	# method is used on the login/sign up steps of the booking wizard
	requestNextStep: ->
		$.when(@getInitialWizardData()).then(
			=>
				if @get('next_step')
					Wizard.trigger 'next_step'
				else
					$.when(@getCurrentStepData()).then(
						-> Wizard.trigger 'current_step'
					)
			,
			(error) ->
				console.log 'error in requestNextStep', error
		)

	# get previous step data
	getPreviousStep: ->
		Wizard.loading()
		@xhr = $.ajax
			method: 'get'
			url: getApiRoute(@urls.step, { step: @get('previous_step') })
			success: (response) =>
				@setData response
				Wizard.ready()
				return

	detectCurrentStep: (data) ->
		current_step_slug = ''
		if data.current_step?
			if _.isNumber(data.current_step)
				current_step_slug = @get('steps')[data.current_step-1].slug
			else
				if data.current_step.slug?
					current_step_slug = data.current_step.slug
				else
					current_step_slug = data.current_step
		else
			current_step_slug = data.steps[0].slug

		return current_step_slug

	setData: (response) ->
		@set Wizard.formatter.formatFromResponse(response, @)
		@

	# this method is used to prefetch needed data before wizard initialization
	# should be overwritten
	getAdditionalWizardData: -> true

module.exports = Model

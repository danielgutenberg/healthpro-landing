Backbone = require 'backbone'
isMobile = require 'isMobile'
debugLog = require 'utils/debug_log'
gaTag    = require 'utils/ga_tag'

require 'backbone.routefilter'
require('perfect-scrollbar')($)
require 'perfect-scrollbar/dist/css/perfect-scrollbar.min.css'

class Router extends Backbone.Router

	routes: {}

	before:
		'*any': 'prepareStep'

	after:
		'*any': 'afterPageLoad'

	initialize: () ->
		super
		@instances = []

	bind: ->
		@listenTo Wizard, 'next_step', => @nextStep()
		@listenTo Wizard, 'prev_step', => @prevStep()
		@listenTo Wizard, 'current_step', => @currentStep()
		@listenTo Wizard, 'content_scroll:update', => @updateScrollBar()

	nextStep: ->
		debugLog('next step', 'wizard.router')
		@openStep( @wizardModel.get('next_step') )

	prevStep: ->
		debugLog('prev step', 'wizard.router')
		@openStep( @wizardModel.get('previous_step') )

	currentStep: ->
		debugLog('current step', 'wizard.router')
		steps = @wizardModel.get('steps')
		currentStep = +@wizardModel.get('current_step.number') - 1
		@openStep( steps[currentStep].slug )

	prepareStep: ->
		@wizardModel.set
			'isSuccessStep': false
			'isLastStep': false

		@destroyScrollBar()
		@clear()

	close: ->
		@clear()
		@stopListening()

	clear: ->
		@clearInstances()
		@clearLayout()
		@clearElClasses()

	###*
	 * clear all inited modules in step
	###
	clearInstances: ->
		_.forEach @instances, (instance) ->
			if instance?
				instance.close?()
				instance.remove?()
				instance = null

		@instances = []

	clearLayout: (clearSidebar = false) ->
		if clearSidebar and @$dom.sidebar? and @$dom.sidebar.length
			@$dom.sidebar.html('')

		if @$dom.content? and @$dom.content.length
			@$dom.content.html('')

	clearElClasses: ->
		if @$dom.container?.newClass?
			@$dom.container.removeClass( @$dom.container.newClass )

		if @$dom.sidebar?.newClass?
			@$dom.sidebar.removeClass( @$dom.sidebar.newClass )

	afterPageLoad: ->
		@initScrollBar()

	updateScrollBar: ->
		if @$dom.contentScroll.hasClass('ps-container')
			@$dom.contentScroll.perfectScrollbar('update')

	destroyScrollBar: ->
		if @$dom.contentScroll.hasClass('ps-container')
			@$dom.contentScroll.perfectScrollbar('destroy')

	initScrollBar: ->
		if @wizardView.place is 'popup'
			if !isMobile.any
				@$dom.contentScroll.perfectScrollbar
					suppressScrollX: true
					includePadding: true
					minScrollbarLength: 20

	addGaTag: (event, additionalParams = {}) -> gaTag event, additionalParams

module.exports = Router

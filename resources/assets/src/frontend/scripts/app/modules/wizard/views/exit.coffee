Backbone = require 'backbone'

class View extends Backbone.View

	el: $('.header--wizard')

	events:
		'click .header--wizard_exit': 'exitWizard'

	initialize: (options) ->
		@baseView = options.baseView
		@canExit = true

		@exitUrl = window.location.origin + '/dashboard'
		@exitMsg = 'Are you sure you want to exit wizard?'
		@exitMsgButtons =
			Yes: 'Yes'
			No: 'Cancel'

		@cacheDom()
		@deactivate()
		@bind()

	cacheDom: ->
		@$el.$btn = @$el.find('.header--wizard_exit')
		@

	bind: ->
		@listenTo Wizard, 'stack:loading', @deactivate
		@listenTo Wizard, 'stack:ready', @activate
		@

	exitWizard: (e) ->
		e?.preventDefault()
		return unless @canExit

		return @baseView.close() if @baseView.model.get('isSuccessStep')

		vexDialog.confirm
			buttons: [
				$.extend {}, vexDialog.buttons.YES, {text: @exitMsgButtons.Yes}
				$.extend {}, vexDialog.buttons.NO, {text: @exitMsgButtons.No}
			]
			message: @exitMsg
			callback: (value) =>
				return unless value
				location.href = @exitUrl
		@

	activate: ->
		@$el.$btn.prop 'disabled', false
		@canExit = true
		@

	deactivate: ->
		@$el.$btn.prop 'disabled', true
		@canExit = false
		@

	setExitUrl: (@exitUrl) -> @

	setExitConfirmationMessage: (@exitMsg) -> @

module.exports = View

Moment = require 'moment'
Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		@baseModel = options.baseModel
		@eventBus = options.eventBus
		super
		@getData()
		@

	getData: ->
		@trigger 'data:loading'
		$.ajax
			url: getApiRoute('ajax-provider-client-packages', {
				clientId: window.GLOBALS.PROFILE_ID
			})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				@fillData(response.data)
				@trigger 'data:ready'

	fillData: (data) ->
		_.each data, (item) =>
			if item.service?
				activeSession = _.findWhere item.service.sessions, active: 1

			dows = []
			_.each item.appointments, (appointment) ->
				dows.push
					day: Moment(appointment.registration.date).format 'e'
					name: Moment(appointment.registration.date).format 'ddd'

			dows = _.uniq(dows, (item, key, a) -> item.day)
			dows = _.sortBy(dows, (item) -> item.day)

			@push
				id: item.id
				start_time: Moment(item.start_time)
				expires_on: Moment(item.expires_on)
				number_of_entities: item.number_of_entities
				entities_left: item.entities_left
				price: item.price
				service: item.service?.name
				duration: if item.number_of_entities isnt -1 then activeSession.duration else item.appointments?[0].duration # all the durations are equal in each day
				locations: item.service?.locations
				appointments_total: item.appointments?.length
				dows: _.uniq(dows)
				rule: item.rules[0] if item.rules?.length

module.exports = Collection

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

require 'backbone.stickit'

Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	el: $('.profile_packages')

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@render()
		@cacheDom()
		@addListeners()

	addListeners: ->
		@listenTo @collection, 'data:loading', @showLoading
		@listenTo @collection, 'data:ready', =>
			@hideLoading()
			if @collection.length
				@toggle true
				@renderItems()
			else
				@toggle false

	cacheDom: ->
		@$el.$loading = @$('.loading_overlay')
		@$el.$packages_list = @$('.profile_packages--list')

	render: ->
		@$el.html ClientsProfile.Templates.Packages()
		@

	renderItems: ->
		@collection.each (model) =>
			@subViews.push new ClientsProfile.Views.PackagesItem
				parentView: @
				model: model
				container: @$el.$packages_list

	toggle: (display = true) ->
		if display
			@$el.removeClass 'm-hide'
		else
			@$el.addClass 'm-hide'

module.exports = View

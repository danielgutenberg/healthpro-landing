Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	el: $('.profile_history')

	events:
		'click [data-client-note-add]': 'addNotePopup'
		'keyup [data-notes-search]': 'search'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions(options)
		@subViews = []
		@addNoteView = null
		@model = new ClientsProfile.Models.History

		@addListeners()

	cacheDom: ->
		@$el.$loading = $('.loading_overlay', @$el)
		@$el.$upcoming = $('[data-section-upcoming]', @$el)
		@$el.$upcoming.$list = $('[data-list-upcoming]', @$el)
		@$el.$history = $('[data-section-history]', @$el)
		@$el.$history.$list = $('[data-list-history]', @$el)
		@$el.$search = $('.profile_history--search', @$el)

	addListeners: ->
		@listenTo @eventBus, 'loading:show', ->
			@showLoading()

		@listenTo @eventBus, 'loading:hide', ->
			@hideLoading()

		@listenTo @collection, 'data:loading', ->
			@showLoading()

		@listenTo @collection, 'data:ready', ->
			@render()
			@hideLoading()

	render: ->
		@$el.html ClientsProfile.Templates.History
			clientId: window.GLOBALS.PROFILE_ID
		@cacheDom()
		@appendParts()
		@

	appendParts: ->
		@appendUpcoming()
		@appendHistory()

	appendUpcoming: ->
		@$el.$upcoming.$list.html('')
		upcoming = @collection.where
			'status': 'confirmed'
		if upcoming.length
			_.each upcoming, (model) =>
				@renderItem(model, @$el.$upcoming.$list)
		else
			@$el.$upcoming.$list.append "<li class='profile_history--item m-empty'>#{ClientsProfile.Config.Messages.emptyUpcoming}</li>"

	appendHistory: ->
		@$el.$history.$list.html('')
		# cleaning array of future appointments models
		history = _.reject @collection.models, (model) ->
			model.get('status') is 'pending'
		history = _.reject history, (model) ->
			model.get('status') is 'confirmed'
		if history.length
			_.each history, (model) =>
				@renderItem(model, @$el.$history.$list)
			@showSearchField()
		else
			@$el.$history.$list.append "<li class='profile_history--item m-empty'>#{ClientsProfile.Config.Messages.emptyHistory}</li>"

	renderItem: (model, container) ->
		@subViews.push new ClientsProfile.Views.HistoryItem
			parentView: @
			model: model
			baseModel: @baseModel
			container: container
			collection: @collection
			eventBus: @eventBus

	addNotePopup: (e, viewOptions) ->
		e?.preventDefault()
		@eventBus.trigger 'loading:show'

		@removePopup()
		@model.resetNotes() # it's a new note, remove old from model

		unless @addNoteView
			unless viewOptions
				viewOptions =
					historyView: @
					model: @model
					collection: @collection
					eventBus: @eventBus
					elmType: 'profile'
					elmId: window.GLOBALS.PROFILE_ID

			@addNoteView = new ClientsProfile.Views.ClientsNote viewOptions

		@eventBus.trigger 'loading:hide'

		# append popup
		$('body').append @addNoteView.render().$el
		window.popupsManager.addPopup $('#popup_add_note')
		window.popupsManager.openPopup 'popup_add_note'

	removePopup: ->
		window.popupsManager.closePopup 'popup_add_note'
		@addNoteView?.remove()
		@addNoteView = null
		window.popupsManager.popups = []

	showSearchField: ->
		@$el.$search.removeClass 'm-hide'

	search: (e) ->
		if e.target.value
			@collection.search e.target.value
		else
			@resetSearch()

	resetSearch: (e) ->
		@collection.resetSearch()

	showLoading: ->
		@$el.$loading.removeClass 'm-hide'

	hideLoading: ->
		@$el.$loading.addClass 'm-hide'

module.exports = View

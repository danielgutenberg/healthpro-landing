Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	el: $('.profile_card')

	# events:
	# 	'click [data-client-note-add]': 'addNotePopup'
	# 	'keyup [data-notes-search]': 'search'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		# @setOptions(options)
		# @model = new ClientsProfile.Models.Profile
		#
		# @cacheDom()
		# @addListeners()

	cacheDom: ->
		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	addListeners: ->
		@listenTo @model, 'data:loading', ->
			@showLoading()

		@listenTo @model, 'data:ready', ->
			@render()
			@hideLoading()

	render: ->
		@$el.html ClientsProfile.Templates.Card @model.toJSON()
		@


	showLoading: ->
		@$el.$loading.removeClass 'm-hide'

	hideLoading: ->
		@$el.$loading.addClass 'm-hide'

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Moment = require 'moment'

require 'backbone.stickit'

Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	tagName: 'li'
	className: 'profile_packages--item'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions(options)
		@addListeners()
		@bind()
		@render()

	addListeners: ->
		@listenTo @, 'rendered', =>
			@cacheDom()

	bind: ->

	render: ->
		months = Moment(@model.get('expires_on')).diff(Moment(@model.get('start_time')), 'months')
		months += ' ' + if months > 1 then 'months' else 'month'

		@$el.html ClientsProfile.Templates.PackagesItem
			expires: if Moment(@model.get('expires_on')).isValid() then @model.get('expires_on').format('Do MMMM YYYY') else ''
			purchased: if Moment(@model.get('start_time')).isValid() then @model.get('start_time').format('Do MMMM YYYY') else ''
			service: @model.get 'service'
			duration: @model.get 'duration'
			number_of_entities: @model.get 'number_of_entities'
			entities_left: @model.get 'entities_left'
			price: @model.get 'price'
			locations: @model.prepareLocations()
			is_monthly: parseInt(@model.get('number_of_entities')) is -1
			appointments_total: @model.get 'appointments_total'
			dows: _.pluck(@model.get('dows'), 'name').join(', ')
			months: months
			max_per_week: =>
				if @model.get('rule')?
					if @model.get('rule').value is 0 then 'unlimited' else @model.get('rule').value

		@container.append @$el
		@stickit()
		@trigger 'rendered'
		@

	cacheDom: ->

module.exports = View

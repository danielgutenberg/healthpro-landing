Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@instances = {}

		@setOptions(options)

		@cacheDom()
		@addListeners()
		@appendParts()

	cacheDom: ->
		@$el.toggleMetaBtn = @$('[data-toggle-meta]')
		@$el.$metaLoading = @$('[data-meta-loading]')

	addListeners: ->
		@listenTo @eventBus, 'updated:mark', ->
			@model.getMissedAppointments(@collection)

		@$el.toggleMetaBtn.on 'click', (e) =>
			e.preventDefault()
			@toggleMeta $(e.currentTarget)


		@listenTo @model, 'meta:loading', @showMetaLoading
		@listenTo @model, 'meta:ready', @hideMetaLoading

	render: ->
		@cacheDom()
		@

	appendParts: ->
		@appendCard()
		@appendHistory()
		@appendStats()
		@appendPackages()

	appendCard: ->
		@instances.card = new ClientsProfile.Views.Card
			baseModel: @model
			eventBus: @eventBus

	appendHistory: ->
		@instances.history = new ClientsProfile.Views.History
			collection: @collections.history
			baseModel: @model
			eventBus: @eventBus

	appendStats: ->
		@instances.history = new ClientsProfile.Views.Stats
			collection: @collections.history
			model: @model

	appendPackages: ->
		@instances.packages = new ClientsProfile.Views.Packages
			collection: @collections.packages
			model: new ClientsProfile.Models.Packages

	toggleMeta: ($metaBtn) ->
		meta = $metaBtn.data('meta')
		return unless meta

		@model.trigger 'meta:loading'
		$.when(
			@model.updateMeta(meta)
		).then(
			(response) =>
				@model.set response.data?.meta_key, response.data?.meta_value

				if response.data?.meta_value
					$metaBtn.addClass 'm-active'
				else
					$metaBtn.removeClass 'm-active'
				@model.trigger 'meta:ready'
		).fail(
			=>
				@model.trigger 'meta:ready'
				alert ClientsProfile.Config.Messages.error
		)

	showMetaLoading: ->
		@$el.$metaLoading.removeClass 'm-hide'

	hideMetaLoading: ->
		@$el.$metaLoading.addClass 'm-hide'

module.exports = View

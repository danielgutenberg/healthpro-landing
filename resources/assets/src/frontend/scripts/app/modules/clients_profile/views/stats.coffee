Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

require 'backbone.stickit'

Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	el: $('.profile_stats')

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions(options)

		@bind()
		@addListeners()

	bind: ->
		@bindings =
			'[data-last-booking]':
				observe: 'last_booking'
				onGet: (val) ->
					val.format('MMMM D YYYY') if val?

			'[data-total-bookings]': 'total_bookings'
			'[data-no-shows]': 'no_shows'
			'[data-cancellations]': 'cancellations'

	addListeners: ->
		@listenTo @collection, 'data:loading', ->
			@showLoading()

		@listenTo @collection, 'data:ready', ->
			@render()
			@hideLoading()

	cacheDom: ->
		@$el.$loading = $('.loading_overlay', @$el)

	render: ->
		@$el.html ClientsProfile.Templates.Stats
		@stickit()
		@cacheDom()
		@

	showLoading: ->
		@$el.$loading.removeClass 'm-hide'

	hideLoading: ->
		@$el.$loading.addClass 'm-hide'

module.exports = View

Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Moment = require 'moment'

require 'backbone.validation'

class Model extends Backbone.Model
	defaults:
		id: null
		date: Moment()
		time_from: null
		price: null
		duration: null
		mark: null
		service: null
		notes: null
		status: null

	attributes:
		is_hidden: false

	initialize: (options) ->
		super
		@initValidation()

	initValidation: ->
		@validation =
			notes: (val, field, model) ->
				return 'Please write some note' unless val.text
				return

	attended: -> @mark 'attended'
	missed: -> @mark 'missed'

	getFullAddress: ->
		address = @get('location').address.address
		if @get('location').location_type_id is 2 # for home visits only
			address += ', ' + @get('location').address.city if @get('location').address.city?
			address += ' ' + @get('location').address.postal_code if @get('location').address.postal_code?

		address

	resetNotes: ->
		@set 'notes', []

	mark: (mark) ->
		return if @get('mark') is mark
		$.ajax
			url: getApiRoute('ajax-appointment-mark', {
				appointmentId: @get('id')
			})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				mark: mark
				_token: window.GLOBALS._TOKEN
			success: =>
				@set 'mark', mark

	save: (elmType, elmId, noteId) ->
		if noteId
			method = 'put'
			options =
				profileId: 'me'
				elmType: elmType
				elmId: elmId
				noteId: noteId
			data =
				text: @get('notes')?.text
				date: @get('date').format(ClientsProfile.Config.dateTimeFormat)
				_token: window.GLOBALS._TOKEN
			url = getApiRoute('ajax-profile-notes-update', options)
		else
			method = 'post'
			options =
				profileId: 'me'
				elmType: elmType
				elmId: elmId
			data =
				text: @get('notes')?.text
				date: @get('date').format(ClientsProfile.Config.dateTimeFormat)
				_token: window.GLOBALS._TOKEN
			url = getApiRoute('ajax-profile-notes', options)

		$.ajax
			url: url
			method: method
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify data
			success: (response) =>
				@trigger 'note:saved'
			error: (response) =>
				console.log response

	remove: (elmType, elmId, noteId) ->
		$.ajax
			url: getApiRoute('ajax-profile-notes-delete', {
				profileId: 'me'
				elmType: elmType
				elmId: elmId
				noteId: noteId
				})
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: (response) =>
				@set('notes', [])
				@trigger 'note:removed'
			error: (response) =>
				console.log response

module.exports = Model

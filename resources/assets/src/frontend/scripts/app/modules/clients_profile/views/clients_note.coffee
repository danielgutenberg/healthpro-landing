Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Moment = require 'moment'
Datepicker = require 'framework/datepicker'

require 'backbone.stickit'
require 'backbone.validation'

Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'click [data-note-save]': 'saveNote'
		'click [data-note-remove]': 'removeNote'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions(options)
		@addValidation
			valid: (view, attr) ->
				$field = view.$el.find("[name='#{attr}']")
				$field.parents('.field, .select').removeClass('m-error')
				.find('.field--error').html('')

			invalid: (view, attr, error) ->
				$field = view.$el.find("[name='#{attr}']")
				$field.parents('.field, .select').addClass('m-error')
				.find('.field--error').html(error)
				$field.on('focus.validation', ->
					$field.parents('.field, .select').removeClass('m-error')
					$field.off('focus.validation')
				)

		@render()
		@bind()

	bind: ->
		@listenTo @, 'rendered', =>
			@cacheDom()

		@bindings =
			'[name="date"]':
				observe: 'date'
				onGet: @onDateGet
				onSet: @onDateSet
				initialize: ($el) =>
					unless @model.get('type') is 'appointment'
						@$el.$datepicker = $el.closest('[data-datepicker]')
						@datepicker = new Datepicker @$el.$datepicker,
							dateFormat: ClientsProfile.Config.datepickerPluginFormat
							selectOtherMonths: true

			'[name="notes"]':
				observe: 'notes'
				onGet: (val) ->
					if val? then val.text else ''
				onSet: (val) ->
					if @model.get('notes')
						@model.get('notes').text = val
						return @model.get('notes')
					else
						noteObj =
							text: val
						@model.set 'notes', noteObj
						return @model.get('notes')

	onDateGet: (val) ->
		if val then val.format(ClientsProfile.Config.datepickerFormat)

	onDateSet: (val) -> Moment(val, ClientsProfile.Config.datepickerFormat)

	render: ->
		@$el.html ClientsProfile.Templates.ClientsNote
			editMode: @editMode
		@stickit()
		@trigger 'rendered'
		@

	cacheDom: ->
		@$el.$loading = $('.loading_overlay', @$el)

	saveNote: ->
		unless @model.validate()
			@showLoading()
			$.when( @model.save(@elmType, @elmId, @noteId) ).then(
				(success) =>
					@eventBus.trigger 'loading:show'
					@collection.getData()
					@historyView.removePopup()
				(error) =>

			)

	removeNote: ->
		@showLoading()
		$.when( @model.remove(@elmType, @elmId, @noteId) ).then(
			(success) =>
				@historyView.removePopup()
			(error) =>

		)

	hideLoading: ->
		@$el.$loading.addClass 'm-hide'

	showLoading: ->
		@$el.$loading.removeClass 'm-hide'

module.exports = View

Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Moment = require 'moment'

require 'backbone.validation'

class Model extends Backbone.Model

	initialize: (options) ->
		super

	prepareLocations: ->
		locations = []
		_.each @get('locations'), (location) ->
			locations.push
				name: location.name
				location_type: location.location_type
				address: if location.address.address_label? and location.address.address_label isnt '' then location.address.address_label else location.address.address
				address_line_two: location.address.address_line_two if location.address.address_line_two?
				city: location.address.city if location.address.city? and location.location_type_id isnt 3 #not for virtual
				postal_code: location.address.postal_code if location.address.postal_code?

		locations

module.exports = Model

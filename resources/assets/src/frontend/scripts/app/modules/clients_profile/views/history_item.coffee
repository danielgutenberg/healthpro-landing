Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Moment = require 'moment'

require 'backbone.stickit'

Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	tagName: 'li'
	className: 'profile_history--item'

	events:
		'click [data-mark-attended]': 'attendedAppointment'
		'click [data-mark-missed]': 'missedAppointment'
		'click [data-note-add]': 'addNotePopup'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions(options)
		@addListeners()
		@bind()
		@render()

	addListeners: ->
		@listenTo @, 'rendered', =>
			@cacheDom()

		@listenTo @model, 'change:mark', =>
			@baseModel.getMissedAppointments(@collection)

		@listenTo @model, 'change:is_hidden', @toggleHidden

		@listenTo @model, 'note:removed', ->
			if @model.get('type') is 'note'
				@remove()
			else
				@eventBus.trigger 'loading:show'
				@collection.getData()

	bind: ->
		@bindings =
			'[data-mark-attended]':
				classes:
					'm-yes':
						observe: 'mark'
						onGet: (val) -> val is 'attended'

			'[data-mark-missed]':
				classes:
					'm-no':
						observe: 'mark'
						onGet: (val) -> val is 'missed'

	render: ->
		if @model.get('type') is 'note'
			@$el.html ClientsProfile.Templates.HistoryItem
				date: @model.get('date').format('MMM D, YYYY')
				notes: @model.get('notes')
				status: @model.get 'type'

		else
			@$el.html ClientsProfile.Templates.HistoryItem @getAppointmentData()

		@container.append @$el
		@stickit()
		@trigger 'rendered'
		@

	getAppointmentData: ->
		if @model.get('updated')?
			updated=
				date: @model.get('updated').format('DD/MM/YYYY')
				time: @model.get('updated').format('h.mma')

		data =
			date: @model.get('time_from').format('MMM D, YYYY')
			time: @model.get('time_from').format('h.mma')
			service: @model.get 'service'
			status: @model.get 'status'
			notes: if @model.get('notes') then @model.get('notes') else ''
			cancelled_on: if @model.get('status') is 'cancelled' then updated
			is_previous: @model.get('status') is 'previous'
			price: @model.get('price')
			duration: @model.get('duration')
			location_name: @model.get('location').name
			location_address: @model.getFullAddress()
			location_type: @model.get('location').location_type

		return data

	cacheDom: ->
		@$el.$attended = @$el.find('.profile_history--item--attended')

	attendedAppointment: (e) ->
		e.preventDefault()
		return if @isLoading()
		@showLoading()
		$.when( @model.attended() ).then(
			(success) =>
				@hideLoading()
			(error) =>
				@hideLoading()
		)

	missedAppointment: (e) ->
		e.preventDefault()
		return if @isLoading()
		@showLoading()
		$.when( @model.missed() ).then(
			(success) =>
				@hideLoading()
			(error) =>
				@hideLoading()
		)

	showLoading: -> @$el.$attended.addClass('m-loading')
	hideLoading: -> @$el.$attended.removeClass('m-loading')
	isLoading: -> @$el.$attended.hasClass('m-loading')

	addNotePopup: (e) ->
		e?.preventDefault()

		viewOptions =
			historyView: @parentView
			model: @model
			collection: @collection
			eventBus: @eventBus
			editMode: $(e.currentTarget).data('note-edit')
			elmType: if @model.get('type') is 'note' then 'profile' else 'appointment'
			elmId: if @model.get('id') then @model.get('id') else window.GLOBALS.PROFILE_ID
			noteId: if @model.get('notes') then @model.get('notes').id

		@parentView.addNotePopup(null, viewOptions)

	toggleHidden: ->
		if @model.get('is_hidden')
			@$el.addClass('m-hide')
		else
			@$el.removeClass('m-hide')

module.exports = View

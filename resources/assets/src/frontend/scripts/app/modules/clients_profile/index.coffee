Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'
CardView = require './views/card'
HistoryView = require './views/history'
HistoryItemView = require './views/history_item'
ClientsNoteView = require './views/clients_note'
StatsView = require './views/stats'
PackagesView = require './views/packages'
PackagesItemView = require './views/packages_item'

# models
BaseModel = require './models/base'
ProfileModel = require './models/profile'
HistoryModel = require './models/history'
PackagesModel = require './models/packages'

# collections
HistoryCollection = require './collections/history'
PackagesCollection = require './collections/packages'

# templates
BaseTmpl = require 'text!./templates/base.html'
Card = require 'text!./templates/card.html'
HistoryTmpl = require 'text!./templates/history.html'
HistoryItemTmpl = require 'text!./templates/history_item.html'
ClientsNoteTmpl = require 'text!./templates/clients_note.html'
StatsTmpl = require 'text!./templates/stats.html'
PackagesTmpl = require 'text!./templates/packages.html'
PackagesItemTmpl = require 'text!./templates/packages_item.html'


window.ClientsProfile =
	Config:
		datepickerFormat: 'MM/DD/YYYY' # used for MomentJS
		datepickerPluginFormat: "mm/dd/yy" # used for datepicker plugin
		dateTimeFormat: 'YYYY-MM-DD HH:mm:ss'
		Messages:
			error: 'Something went wrong. Please reload the page and try again.'
			emptyUpcoming: 'No upcoming appointments'
			emptyHistory: 'No previous appointments'
		Tooltips:
			allow: 'Allow client to book a home visit outside of your range.'
			limit: 'Limit the client to book a home visit inside your range.'

	Models:
		Base: BaseModel
		Profile: ProfileModel
		History: HistoryModel
		Packages: PackagesModel
	Collections:
		History: HistoryCollection
		Packages: PackagesCollection
	Views:
		Base: BaseView
		Card: CardView
		History: HistoryView
		HistoryItem: HistoryItemView
		ClientsNote: ClientsNoteView
		Stats: StatsView
		Packages: PackagesView
		PackagesItem: PackagesItemView
	Templates:
		Base: Handlebars.compile BaseTmpl
		Card: Handlebars.compile Card
		History: Handlebars.compile HistoryTmpl
		HistoryItem: Handlebars.compile HistoryItemTmpl
		ClientsNote: Handlebars.compile ClientsNoteTmpl
		Stats: Handlebars.compile StatsTmpl
		Packages: Handlebars.compile PackagesTmpl
		PackagesItem: Handlebars.compile PackagesItemTmpl

_.extend ClientsProfile, Backbone.Events

class ClientsProfile.App
	constructor: (@$block) ->
		@eventBus = _.extend {}, Backbone.Events

		@models =
			base: new ClientsProfile.Models.Base
			history: new ClientsProfile.Models.History
				eventBus: @eventBus
		@collections =
			history: new ClientsProfile.Collections.History [],
				model: ClientsProfile.Models.History
				baseModel: @models.base
				eventBus: @eventBus
			packages: new ClientsProfile.Collections.Packages [],
				model: ClientsProfile.Models.Packages
				baseModel: @models.base
				eventBus: @eventBus
		@view = new ClientsProfile.Views.Base
			el: @$block
			model: @models.base
			collections: @collections
			eventBus: @eventBus

		return {
			view: @view
			model: @models.base
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

if $('[data-clients-profile]').length
	new ClientsProfile.App $('[data-clients-profile]')

module.exports = ClientsProfile

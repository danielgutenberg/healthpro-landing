Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		last_booking: null
		total_bookings: null
		no_shows: null
		cancellations: null

	initialize: ->
#		@getMeta()
		super
#
#	getMeta: ->
#		@trigger 'meta:loading'
#		$.ajax
#			url: getApiRoute('ajax-provider-client-meta-get', {
#				providerId: 'me'
#				clientId: window.GLOBALS.PROFILE_ID
#				})
#			method: 'get'
#			type  : 'json'
#			contentType: 'application/json; charset=utf-8'
#			success: (response) =>
#				@set 'range_exemption', response.data.range_exemption
#				@trigger 'meta:ready'
#			error: (error) =>
#				@trigger 'meta:ready'
#				if error.responseJSON?.errors
#					_.each error.responseJSON.errors, (err) =>
#						if err.messages?.length
#							window.Alerts.addNew('error', 'Error:' + err.messages[0])

	updateMeta: (field) ->
		$.ajax
			url: getApiRoute('ajax-provider-client-meta-toggle', {
				providerId: 'me'
				clientId: window.GLOBALS.PROFILE_ID
			})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
				meta_key: field

	fillStats: (collection) ->
		@getTotalBookings(collection)
		@getCancelledAppointments(collection)
		@getLastAppointment(collection)
		@getMissedAppointments(collection)

		@trigger 'data:ready'

	getLastAppointment: (collection) ->
		last_appointment = collection.findWhere
			'status': 'previous'
		@set 'last_booking', last_appointment.get('date') if last_appointment

	getMissedAppointments: (collection) ->
		missed_appointments = collection.where
			'mark': 'missed'
		@set 'no_shows', missed_appointments.length if missed_appointments?

	getCancelledAppointments: (collection) ->
		cancelled_appointments = collection.where
			'status': 'cancelled'
		@set 'cancellations', cancelled_appointments.length if cancelled_appointments?

	getTotalBookings: (collection) ->
		total_bookings = collection.where
			'status': 'previous'
		@set 'total_bookings', total_bookings.length if total_bookings?

module.exports = Model

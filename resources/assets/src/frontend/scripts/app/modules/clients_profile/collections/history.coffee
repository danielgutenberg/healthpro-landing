Moment = require 'moment'
Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	comparator: (model) ->
		return model.get('time_from')?.valueOf()

	initialize: (models, options) ->
		@baseModel = options.baseModel
		@eventBus = options.eventBus
		super
		@getData()
		@

	getData: ->
		@trigger 'data:loading'
		@reset()

		$.when( @getAppointments(), @getNotes() ).then(
			(success) =>
				@trigger 'data:ready'

			(error) =>

		)

	getAppointments: ->
		$.ajax
			url: getApiRoute('ajax-provider-client-history', {
				providerId: 'me'
				clientId: window.GLOBALS.PROFILE_ID
			})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				@fillData(response.data)

	getNotes: ->
		$.ajax
			url: getApiRoute('ajax-profile-notes', {
				profileId: 'me'
				elmType: 'profile'
				elmId: window.GLOBALS.PROFILE_ID
			})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				@fillData(response.data, 'notes')

	fillData: (data, type) ->
		if type is 'notes'
			_.each data, (item) =>
				@push
					notes: item
					date: Moment(item.note_date)
					type: 'note'

		else
			_.each data, (items, status) =>
				_.each items, (item) =>
					@push
						id: item.id
						date: Moment(item.date)
						time_from: Moment(item.from)
						price: item.price
						duration: item.duration
						mark: item.mark
						service: item.service?.name
						notes: item.notes[0]
						status: status
						updated: Moment(item.updated_at.date)
						type: 'appointment'
						location: item.location

			@baseModel.fillStats(@)

		@sort()

	search: (value) ->
		value = value.toLowerCase()
		_.each @models, (model) ->
			if model.get('notes')?
				is_hidden = value and model.get('notes').text.toLowerCase().indexOf(value) is -1
			else is_hidden = true
			model.set 'is_hidden', is_hidden

	resetSearch: (value) ->
		_.each @models, (model) ->
			model.set 'is_hidden', false

module.exports = Collection

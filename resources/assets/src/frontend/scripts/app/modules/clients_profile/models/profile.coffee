Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	initialize: ->
		super
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-profile', {id: window.GLOBALS.PROFILE_ID})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				@formatData response.data
				@trigger 'data:ready'

	formatData: (data) ->
		@set data

module.exports = Model

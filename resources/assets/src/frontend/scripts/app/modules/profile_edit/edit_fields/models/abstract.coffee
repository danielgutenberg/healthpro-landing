Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		id: null

	initialize: (options)->
		@type = options.type
		@config = options.config

		@set options.presetData

		@initValidation()

	initValidation: ->
		@

	getSelectOptions: (fieldName) ->
		switch fieldName
			when 'languages'
				options =
					collection: @get('language_options')
					labelPath: 'text'
					valuePath: 'value'
			when 'experience'
				options =
					collection: @get('experience_tags')
					labelPath: 'text'
					valuePath: 'value'
					defaultOption:
						label: 'Select Your Experience'
						value: null
			when 'title'
				options =
					collection: ['Dr.', 'Mr.', 'Ms.']
					defaultOption:
						label: 'Your Title'
						value: null
			when 'gender'
				options =
					collection: [
						{value: 'male', label: 'Male'}
						{value: 'female', label: 'Female'}
					]
					defaultOption:
						label: 'Select Gender'
						value: null

			else
				options = []

		options

	save: ->
		data = @getSaveData()
		return false unless data
		data._token = window.GLOBALS._TOKEN

		@trigger 'data:saving'

		@xhr = $.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify data
			success: => @trigger 'data:saved'
			error: => @trigger 'data:error'

	getSaveData: ->
		console.error 'please add `getSaveData` method in your model'

module.exports = Model

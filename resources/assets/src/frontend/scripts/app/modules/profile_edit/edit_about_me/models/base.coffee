Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		'experience_tags': []

	url: getApiRoute('ajax-profiles-self')

	initialize: ->
		@fetch()
		@setValidation()


	setValidation: ->
		@validation =
			"tags.experience":
				required: false
				msg: 'Enter years of experience'

			"about_self_background": [
				{
					required: true
					msg:'Enter your background'
				}
				{
					maxLength: 1000
					msg: 'Your text is too long, please enter a maximum of 1000 characters'
				}
			]

			'about_what_i_do':
				maxLength: 500
				msg: 'Your text is too long, please enter a maximum of 500 characters'

			'about_services_benefit':
				maxLength: 500
				msg: 'Your text is too long, please enter a maximum of 500 characters'

			'about_self_specialities':
				maxLength: 500
				msg: 'Your text is too long, please enter a maximum of 500 characters'


	fetch: ->
		window.Dashboard.trigger 'content:loading'
		$.when(
			@fetchProfile(),
			@fetchLanguagesTags(),
			@fetchExperienceTags()
		).then(
			=>
				@initialData = @toJSON()
				@trigger 'data:ready'
				window.Dashboard.trigger 'content:ready'
		)

	fetchProfile: ->
		@xhr = $.ajax
			url: @url
			method: 'get'
			success: (res) =>
				@set
					'business_name': res.data.business_name
					'about_self_background': res.data.about_self_background ? ''
					'about_self_specialities': res.data.about_self_specialities ? ''
					'about_services_benefit': res.data.about_services_benefit ? ''
					'about_what_i_do': res.data.about_what_i_do ? ''
					'tags.languages': @parseArray(res.data.tags.languages.tags)
					'tags.experience': do (res) ->
						if res.data.tags?.experience?.tags?.length
							+res.data.tags.experience.tags[0]['id'].toString()
						else
							null

	fetchLanguagesTags: ->
		$.ajax
			'url': getApiRoute('ajax-taxonomy-tags', {taxonomySlug: 'language'})
			method: 'get'
			success: (response) =>
				languages = []
				_.each response.data, (item) =>
					languages.push
						value: item.id
						text: item.name

				@set('language_options', languages)

	fetchExperienceTags: ->
		$.ajax
			'url': getApiRoute('ajax-taxonomy-tags', {taxonomySlug: 'experience'})
			method: 'get'
			success: (response) =>
				experienceList = _.sortBy response.data, 'id'
				experienceTags = []
				experienceList.forEach (item) =>
					experienceTags.push
						value: item.id
						text: item.name

				@set('experience_tags', experienceTags)

	parseArray: (array) ->
		result = []
		array.forEach (item) =>
			result.push(item.id)
		result

	resetData: ->
		@set @initialData

	save: ->
		window.Dashboard.trigger 'content:loading'

		@xhr = $.ajax
			url: @url
			method: 'put'
			dataType: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				business_name: @get('business_name')
				about_self_background: @get('about_self_background')
				about_self_specialities: @get('about_self_specialities')
				about_services_benefit: @get('about_services_benefit')
				about_what_i_do: @get('about_what_i_do')
				tags:
					languages: @get('tags.languages')
					experience: [@get('tags.experience')]
				_token: window.GLOBALS._TOKEN

			success: (res) =>
				@trigger 'ajax:success'
				window.Dashboard.trigger 'content:ready'

			error: (err) =>
				@trigger 'ajax:error', err.responseJSON.errors
				window.Dashboard.trigger 'content:ready'

module.exports = Model

Abstract = require './abstract/model'

PhoneValidator = require 'utils/validators/phone'
getApiRoute = require 'hp.api'
require 'framework/phone'

class Model extends Abstract

	cachedField: 'number'

	defaults:
		verification_method	: ''
		verification_code	: ''
		needs_verification	: false

	addValidationRules: ->
		super
		@validation.number = (val, attr, computed) ->
			return if computed.needs_verification
			return 'Please enter phone number with the following format +1 (XXX) XXX-XXXX' unless @isPhone(val)
			return 'Please enter a different phone number' if @cachedFieldValue is val

		@validation.verification_code = (val, attr, computed) ->
			return unless computed.needs_verification
			return 'Please enter your verification code' unless val.trim()

	update: ->
		$.ajax
			url: getApiRoute('ajax-profile-edit-phone', {phoneId: @get('id')})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			show_alerts: false
			data: @getAjaxData({number: @get('number'), phone_verify_method: @get('verification_method')})
			success: =>
				@set 'verified', false
				@set 'verification_code', ''
				@set 'needs_verification', true

				@cacheField()
				@resetFields()

	isPhone: (value) -> return PhoneValidator(value)

	formattedPhone: ->
		val = @get('number').replace('+1', '')
		$.formatMobilePhoneNumber "+1#{val}"

	resendCode: ->
		$.ajax
			url: getApiRoute('ajax-profile-resend-verify-phone', {phoneId: @get('id')})
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			show_alerts: false
			data: @getAjaxData
				phone_verify_method: @get('verification_method')

	verifyCode: ->
		$.ajax
			url: getApiRoute('ajax-profile-verify-phone', {phoneId: @get('id')})
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			show_alerts: false
			data: @getAjaxData
				code: @get('verification_code')
			success: =>
				@set 'verification_code', ''
				@set 'needs_verification', ''
				@set 'verified', true

module.exports = Model

Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Abstract = require './abstract/model'

class Model extends Abstract

	cachedField: 'email'

	addValidationRules: ->
		super

		@validation.current_password =
			required: true
			msg: 'Please enter your current password to update your email address'

		@validation.email = (value) =>
			return 'Please enter a valid email address' unless @isEmail(value)
			return 'Please enter a different email address' if @cachedFieldValue is value

	update: ->
		$.ajax
			url: getApiRoute('user-account')
			method: 'put'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data: @getAjaxData
				email: @get('email')
				current_password: @get('current_password')

	isEmail: (value) -> value and value.match(Backbone.Validation.patterns.email)

	resendEmail: ->
		$.ajax
			url: getApiRoute('ajax-auth-send-activation')
			method: 'post'
			type: 'json'
			show_alerts: false
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

module.exports = Model

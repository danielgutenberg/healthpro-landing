Backbone = require 'backbone'
Handlebars = require 'handlebars'

require '../../../../../styles/modules/receive_payments.styl'

# views
BaseView = require './views/base'
LearnMoreView = require './views/learn_more'

# models
BaseModel = require './models/base'

# templates
BaseTmpl = require 'text!./templates/base.html'
LearnMoreTmpl = require 'text!./templates/learn_more.html'

# list of all instances
window.ReceivePayments =
	Config:
		Messages:
			updateSuccess: 'Your changes have been saved.'
			updateError: 'Something went wrong. Please reload the page and try again.'
			connectSystem: 'Please select at least one payment method.'
			changesNotSaved: 'Your changes will not be saved.'
			confirmGiftCertificatesDisable: '<b>Please note:</b> Disabling online payments will make the Gift Certificates feature unavailable to your clients.'

	Views:
		Base: BaseView
		LearnMore: LearnMoreView
	Models:
		Base: BaseModel
	Templates:
		Base: Handlebars.compile BaseTmpl
		LearnMore: Handlebars.compile LearnMoreTmpl

# events bus
_.extend ReceivePayments, Backbone.Events

class ReceivePayments.App
	constructor: ->
		@view = new ReceivePayments.Views.Base
			model: new ReceivePayments.Models.Base

ReceivePayments.app = new ReceivePayments.App()

module.exports = ReceivePayments

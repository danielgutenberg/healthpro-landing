Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		super
		Cocktail.mixin( @, Mixins.Common ) # add Mixins

		@setOptions(options)

		_.forEach @phonesData, (model) =>
			@add
				'id': model.id
				'number': model.number
				'prefix': model.prefix
				'count': model.index
				'activated': model.activated
				'verified': model.verified

module.exports = Collection

Handlebars = require 'handlebars'
Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

EditAssets = require 'app/modules/profile_edit/edit_assets'

Template = Handlebars.compile require('text!../templates/base.html')

class MediaEdit extends Backbone.View

	events:
		'click .profile_edit_inline--btn': 'closePopup'


	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

	render: ->
		@$el.html Template()

		@subViews.push @editAssets = new EditAssets.App
			presetData: @baseView.aboutModel.toJSON()
			el: @$el.find('.profile_edit_inline--container')

		@bind()
		@

	bind: ->
		@editAssets.view.editVideos.view.on 'form:toggle', =>
			@baseView.popup.centerPopup()

		@baseView.popup.popup.$el.on 'popup:closed', => @baseView.reloader.reloadWithMessage()

	closePopup: ->
		@baseView.popup.closePopup()

module.exports = MediaEdit

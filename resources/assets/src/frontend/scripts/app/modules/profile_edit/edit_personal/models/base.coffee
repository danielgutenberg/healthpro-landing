Backbone = require 'backbone'
getApiRoute = require 'hp.api'
vexDialog = require 'vexDialog'

class Model extends Backbone.Model

	initialize: ->
		@bind()

	bind: ->
		Dashboard.trigger 'content:loading'
		@listenTo Dashboard, 'profile:loaded', @setData

	setData: (model) ->
		@set
			first_name			: model.get('profile.first_name')
			last_name			: model.get('profile.last_name')
			slug				: model.get('profile.slug')
			title				: model.get('profile.title') ? ''
			gender				: model.get('profile.gender')
			social_pages		: model.get('profile.social_pages')
			avatar				: model.get('profile.avatar')
			avatar_original		: model.get('profile.avatar_original')
			phone				: model.get('profile.phone.number') ? ''
			occupation			: model.get('profile.occupation') ? ''
			birthday			: model.get('profile.birthday') ? ''

			profile				: model.get('profile')

		Dashboard.trigger 'content:ready'
		@trigger 'data:fetched'

	save: ->
		Dashboard.trigger 'content:loading'

		@xhr = $.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				first_name			: @get('first_name')
				last_name			: @get('last_name')
				title				: @get('title') ? ''
				gender				: @get('gender')
				social_pages		: @get('social_pages')
				phone				: @get('phone') ? ''
				birthday			: @get('birthday') if @get('birthday')
				occupation			: @get('occupation') ? ''
				_token: window.GLOBALS._TOKEN

			success: =>
				@trigger 'data:saved'
				@trigger 'ajax:success'
				Dashboard.trigger 'content:ready'

			error: (response) =>
				@trigger 'ajax:error', response.responseJSON.errors
				@trigger 'data:saved'
				Dashboard.trigger 'content:ready'

	hasNameUpdated: ->
		profile = @get('profile')
		!(profile.first_name is @get('first_name') and profile.last_name is @get('last_name'))

	updateSlug: ->
		Dashboard.trigger 'content:loading'
		@xhr = $.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				slug: @getSlug()
				_token: window.GLOBALS._TOKEN

			success: =>
				# reload page after updating the slug
				window.location.reload()

			error: (response) =>
				@trigger 'ajax:error', response.responseJSON.errors
				@trigger 'data:saved'
				Dashboard.trigger 'content:ready'

	getCurrentUrl: -> @get('profile').public_url.replace /https?\:\/\//, ''

	getSlug: ->
		newSlug = @get('first_name') + ' ' + @get('last_name')
		newSlug = newSlug
			.toLowerCase()
			.trim()
			.replace(/[^a-z0-9-]/gi, '-')
			.replace(/-+/g, '-')
			.replace(/^-|-$/g, '')
		newSlug

	getUpdateUrl: -> @getCurrentUrl().replace '/p/' + @get('slug'), '/p/' + @getSlug()


module.exports = Model

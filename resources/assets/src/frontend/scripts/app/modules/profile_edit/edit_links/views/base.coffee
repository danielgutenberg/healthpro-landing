Backbone = require 'backbone'
Handlebars = require 'handlebars'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

template = require 'text!../templates/item.html'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins

		@instances = []
		@bind()

	bind: ->
		@listenTo @collection, 'data:ready', =>
			@appendLinks()
			@hideLoading()
		@listenTo @, 'rendered', =>
			@cacheDom()
			@appendLinks()
			@hideLoading()

	render: ->
		@$el.html EditLinks.Templates.Base()
		@trigger 'rendered'

		@

	cacheDom: ->
		@$el.$list = @$('.edit_links--list')
		@$el.$loading = @$('.loading_overlay')

	appendLinks: ->
		if @collection.length
			@$el.$list.html('')
			@collection.forEach (model) =>
				@instances.push link = new EditLinks.Views.Link
					model: model

				@$el.$list.append link.render().el

		return @

	showLoading: -> @$el.$loading.show()
	hideLoading: -> @$el.$loading.hide()

module.exports = View

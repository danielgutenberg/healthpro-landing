Backbone = require 'backbone'
getApiRoute = require 'hp.api'
vexDialog = require 'vexDialog'

class Model extends Backbone.Model

	defaults:
		id: ''
		url: ''
		title: ''
		description: ''
		type: 'video'
		isNew: false

	validation:
		title:
			required: true

	initialize: (attrs, options) ->

	upload: ->
		url = @get('url')
		unless url.match '/^https?:\/\//'
			url = "https://#{url}"

		@trigger('loading')
		$.ajax
			url: getApiRoute('ajax-profile-assets', {
				profileId: window.GLOBALS._PID
				assetsType: 'videos'
				})
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				url: url
				title: @get('title')
				type: @get('type')
				description: @get('description')
				_token: window.GLOBALS._TOKEN
			success: (res) =>
				@trigger('done:loading')
				@trigger('video:added')
				@set('id', res.data.asset.id)
			error: =>
				@trigger('done:loading')
				vexDialog.alert 'Their was an error uploading your video.'

	save: ->
		$.ajax
			url: getApiRoute('ajax-profile-asset', {
				profileId: window.GLOBALS._PID
				assetType: 'videos'
				assetId: @get('id')
				})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				title: @get('title')
				_token: window.GLOBALS._TOKEN

	remove: ->
		$.ajax
			url: getApiRoute('ajax-profile-asset', {
				profileId: window.GLOBALS._PID
				assetType: 'videos'
				assetId: @get('id')
				})
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: =>
				@set('id', null)
				@destroy()

module.exports = Model

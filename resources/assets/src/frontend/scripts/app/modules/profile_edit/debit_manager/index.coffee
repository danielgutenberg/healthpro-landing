Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'
HeaderView = require './views/header'
CardsListView = require './views/cards_list'
CardView = require './views/card'
CardFormView = require './views/card_form'
BankAccountsListView = require './views/bank_accounts_list'
BankAccountView = require './views/bank_account'
BankAccountFormView = require './views/bank_account_form'

# models
CardModel = require './models/card'
BankAccountModel = require './models/bank_account'

# collections
CardsCollection = require './collections/cards'
BankAccountsCollection = require './collections/bank_accounts'

# templates
HeaderTmpl = require 'text!./templates/header.html'
CardsListTmpl = require 'text!./templates/cards_list.html'
CardTmpl = require 'text!./templates/card.html'
CardFormTmpl = require 'text!./templates/card_form.html'
BankAccountListTmpl = require 'text!./templates/bank_account_list.html'
BankAccountTmpl = require 'text!./templates/bank_account.html'
BankAccountFormTmpl = require 'text!./templates/bank_account_form.html'

# list of all instances
window.DebitManager =
	Config:
		Messages:
			removeCard: 'Are you sure you want to remove this card? Your clients will not be able to book appointments until you add a new card.'
			removeBankAccount: 'Are you sure you want to remove bank account? Your clients will not be able to book appointments until you add new payment information.'
			error: 'Something went wrong. Please reload the page and try again.'
		MaxCards:
			provider: 1
			client: 10
		Fields:
			provider:
				card_type :
					label: 'Card Type'
					class: 'cards_list--table--type'
				card_number :
					label: 'Card Number'
				card_holder :
					label: 'Name on Card'
				card_exp :
					label: 'Expiry Date'
					class: 'cards_list--table--exp'
				card_funding :
					label: 'Funding'
					class: 'cards_list--table--funding'
				card_withdraw :
					label: 'Withdraw'
					class: 'cards_list--table--withdraw'
				actions :
					label: '&nbsp;'
			client:
				card_type :
					label: 'Card Type'
					class: 'cards_list--table--type'
				card_number :
					label: 'Card Number'
				card_holder :
					label: 'Name on Card'
				card_exp :
					label: 'Expiry Date'
					class: 'cards_list--table--exp'
				card_funding :
					label: 'Funding'
					class: 'cards_list--table--funding'
				actions :
					label: '&nbsp;'
			bank_account:
				account_number:
					label: 'Account Number'
					class: 'cards_list--table--account_number'
				routing_number:
					label: 'Routing Number'
				actions :
					label: '&nbsp;'

	Views:
		Base: BaseView
		Header: HeaderView
		CardsList: CardsListView
		Card: CardView
		CardForm: CardFormView
		BankAccountsList: BankAccountsListView
		BankAccount: BankAccountView
		BankAccountForm: BankAccountFormView
	Models:
		Card: CardModel
		BankAccount: BankAccountModel
	Collections:
		Cards: CardsCollection
		BankAccounts: BankAccountsCollection
	Templates:
		Header: Handlebars.compile HeaderTmpl
		CardsList: Handlebars.compile CardsListTmpl
		Card: Handlebars.compile CardTmpl
		CardForm: Handlebars.compile CardFormTmpl
		BankAccountList: Handlebars.compile BankAccountListTmpl
		BankAccount: Handlebars.compile BankAccountTmpl
		BankAccountForm: Handlebars.compile BankAccountFormTmpl

# events bus
_.extend DebitManager, Backbone.Events

class DebitManager.App
	constructor: ->
		@view = new DebitManager.Views.Base
			profileType: window.GLOBALS._PTYPE
			collections:
				cards: new DebitManager.Collections.Cards null, {model: DebitManager.Models.Card}
				bank_accounts: new DebitManager.Collections.BankAccounts null, {model: DebitManager.Models.BankAccount}

DebitManager.app = new DebitManager.App()

module.exports = DebitManager

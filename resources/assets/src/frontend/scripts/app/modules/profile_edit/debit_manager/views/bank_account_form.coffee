Backbone = require 'backbone'
jqPayment = require 'payment'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
datetimepicker = require 'datetimepicker'

class View extends Backbone.View
	attributes:
		'class': 'popup popup_bank_account_form bank_account_form'
		'data-popup': 'bank_account_form'
		'id' : 'popup_bank_account_form'

	events:
		'click .card_form--add_bank_account': 'submitForm'
		'submit': 'submitForm'
		'click .card_form--cancel': 'cancelForm'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base

		@collections = options.collections
		@baseView = options.baseView
		@model = options.model
		@profileType = options.profileType

		@isNew = if @model.get('id') then false else true

		@bind()
		@addValidation
			valid: (view, attr) ->
				$field = view.$el.find("[name='#{attr}']")
				$field.parents('.field, .select').removeClass('m-error')
					.find('.field--error').html('')

			invalid: (view, attr, error) ->
				$field = view.$el.find( "[name='#{attr}']")
				$field.parents('.field, .select').addClass('m-error')
					.find('.field--error').html(error)
				$field.on('focus.validation', ->
					$field.parents('.field, .select').removeClass('m-error')
					$field.off('focus.validation')
				)

		@render()
		@cacheDom()

		@stickit()
		@initFieldsCheck()

	bind: ->
		@bindings =
			'[name="full_name"]': 'full_name'
			'[name="account_number"]': 'account_number'
			'[name="routing_number"]': 'routing_number'
			'[name="dob_month"]':
				observe: 'dob_month'
				selectOptions:
					collection: @monthOptions()
					defaultOption:
						label: 'Month'
						value: ''
			'[name="dob_day"]':
				observe: 'dob_day'
				onSet: (val) ->
					if val < 10
						return '0' + val
					val
				selectOptions:
					collection: @dayOptions()
					defaultOption:
						label: 'Day'
						value: ''
			'[name="dob_year"]':
				observe: 'dob_year'
				selectOptions:
					collection: @yearOptions()
					defaultOption:
						label: 'Year'
						value: ''
			'[name="account_type"]': 'account_type'

			'.bank_account_form--number--label':
				classes:
					'm-hide':
						observe: 'id'
						onGet: (val) -> if val then false else true

			'.bank_account_form--number--field':
				classes:
					'm-hide':
						observe: 'id'
						onGet: (val) -> if val then true else false

			'.bank_account_form--account_number':
				observe: 'account_number'
				updateMethod: 'html'
				onGet: (val) -> '<i>&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;</i>' + val

	initFieldsCheck: ->
		# don't allow numbers in name
		@$el.$accountFullName.on 'keypress', (e) =>
			code = e.charCode
			if code > 47 and code < 58
				return false

	submitForm: (e) ->
		e.preventDefault() if e?
		@hideError()
		errors = @model.validate()
		if !errors?
			@showLoading()
			$.when( @model.save() )
				.then(
					(res) =>
						@hideLoading()
						if @isNew
							data = res.data
							@collections.bank_accounts.push data
						@baseView.hideNotificationAlert()
						@baseView.removePopup()
					, (request) =>
						@hideLoading()
						if request.responseJSON?.errors
							_.each request.responseJSON.errors, (err) =>
								@showError err.messages[0] if err.messages?.length
				)
		@

	cancelForm: -> 
		@baseView.removePopup()

	# base functions
	cacheDom: ->
		@$el.$accountFullName = @$el.find('[name="full_name"]')

		@$el.$buttonCancel = @$el.find('.card_form--cancel')
		@$el.$buttonDelete = @$el.find('.card_form--delete')

		@$el.$errorsContainer = @$el.find('.card_form--errors')

		@

	render: ->
		@$el.html DebitManager.Templates.BankAccountForm()

		@$el.$loading = $('<span class="loading_overlay m-light m-hide"><span class="spin m-cog"></span></span>')
		@$el.find('.popup--container').append @$el.$loading

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

	showLoading: ->
		@$el.$loading.removeClass('m-hide')


	showError: (error) ->
		@$el.$errorsContainer.html("<span>#{error}</span>").removeClass('m-hide')

	hideError: ->
		@$el.$errorsContainer.html('').addClass('m-hide')

	dayOptions: ->
		days = []
		start = 1

		days.push(start++) while start <= 31

		return days

	yearOptions: ->
		years = []
		start = 1920
		current = new Date().getFullYear() - 10

		years.push(current--) while start <= current

		return years

	monthOptions: ->
		return [
			{
				value: "01"
				label: "January"
			}
			{
				value: "02"
				label: "February"
			}
			{
				value: "03"
				label: "March"
			}
			{
				value: "04"
				label: "April"
			}
			{
				value: "05"
				label: "May"
			}
			{
				value: "06"
				label: "June"
			}
			{
				value: "07"
				label: "July"
			}
			{
				value: "08"
				label: "August"
			}
			{
				value: "09"
				label: "September"
			}
			{
				value: "10"
				label: "October"
			}
			{
				value: "11"
				label: "November"
			}
			{
				value: "12"
				label: "December"
			}
		]

module.exports = View

Backbone = require 'backbone'
Stickit = require 'backbone.stickit'

class View extends Backbone.View

	subViews: []

	attributes:
		'class': 'cards_list--app--container cards_list--app--cards'

	initialize: (@options) ->
		super

		@baseView = @options.baseView
		@collections = @options.collections

		@model = new Backbone.Model
			fields: DebitManager.Config.Fields[@baseView.profileType]

		@bind()
		@render()
		@cacheDom()
		@stickit()
		@

	bind: ->
		@listenTo @collections.cards, 'add', @renderCard
		@listenTo @collections.cards, 'update', @toggleEmptyMessage
		@listenTo @collections.cards, 'data:ready', @toggleEmptyMessage

		@bindings =
			'[data-header]':
				observe: 'fields'
				updateMethod: 'html'
				onGet: (val, options) ->
					html = ''
					_.each val, (opts) ->
						html += "<th" + (if opts.class then " class='#{opts.class}'" else "") + ">#{opts.label}</th>"
					html

	cacheDom: ->
		@$el.$listing = $('[data-listing]', @$el)
		@$el.$header = $('[data-header]', @$el)
		@$el.$empty = $('<tr><td class="cards_list--table--empty" colspan="' + _.size(@model.get('fields')) + '"><p>Please add your first card</p></td></tr>')

	render: ->
		@$el.html DebitManager.Templates.CardsList

	renderCard: (model) ->
		@subViews.push new DebitManager.Views.Card
			parentView: @
			baseView: @baseView
			model: model
			collections: @collections

	toggleEmptyMessage: ->
		if @collections.cards.length
			@$el.$empty.remove()
		else
			@$el.$listing.append @$el.$empty

module.exports = View

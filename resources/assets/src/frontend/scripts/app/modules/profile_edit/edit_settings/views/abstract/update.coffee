Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'

require 'backbone.validation'
require 'backbone.stickit'

class View extends Backbone.View

	events:
		'click [data-update]': 'maybeUpdate'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()
		@addValidation()
		@addStickit()
		@render()
		@cacheDom()
		@stickit()

		@delegateEvents()

	addListeners: -> @

	cacheDom: -> @

	render: ->
		switch @type
			when 'phone'
				el = UpdateAccount.Templates.Phone()
			when 'email'
				el = UpdateAccount.Templates.Email()
		@$el.html el
		@

	addStickit: ->
		@bindings =
			'[name="current_password"]'	: 'current_password'
		@

	initPopup: ->
		@popupView = new UpdateAccount.Views.VerificationPopup
			parentView	: @
			type		: @type
		@

	closePopup: ->
		if @reload
			window.location.reload()
			return

		return unless @popupView
		@popupView.remove()
		@popupView = null
		@

	update: ->
		return if @model.validate()
		@popupView.showLoading()
		$.when(
			@model.update()
		).then(
			=> @afterUpdate()
		).fail(
			(response) => @handleUpdateErrors response.responseJSON
		)

	maybeUpdate: ->
		return if @model.validate()
		@initPopup()

	afterUpdate: ->
		@popupView.setStep 'success'
		@popupView.hideLoading()

	displayMsg: (msg, cb = null) ->
		vexDialog.alert
			message: msg
			buttons: [$.extend({}, vexDialog.buttons.YES, text: 'OK')]
			callback: -> cb() if cb

	confirmUpdate: -> @update()

	cancelUpdate: ->
		@model.resetFields()
		@closePopup()
		@

	handleUpdateErrors: (responseJSON) ->
		if @popupView
			@popupView.hideLoading()
			@closePopup()
		msg = 'An error has occured, please try again'
		if responseJSON.errors.currentPassword
			msg = responseJSON.errors.currentPassword.messages[0]
		else if responseJSON.errors.email
			msg = responseJSON.errors.email.messages[0]
		else if responseJSON.errors.phone
			msg = responseJSON.errors.phone.messages[0]
		@displayMsg msg

	showLoading: -> @baseView.showLoading()

	hideLoading: -> @baseView.hideLoading()

module.exports = View

Backbone = require 'backbone'
Validation = require 'backbone.validation'
getApiRoute = require 'hp.api'
Moment = require 'moment'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class Model extends Backbone.Model

	defaults:
		id: ''
		full_name: ''
		account_number: ''
		routing_number: ''
		account_type: ''
		country: 'us'
		currency: 'usd'
		dob: ''

	initialize: ->
		super
		Cocktail.mixin @, Mixins.Models.Validation

	validation:

		full_name: (val, field, model) =>
			val = val.trim() # trim the whitespace
			return if val.match(/(.*)\s(.*)/i)
			return 'Please enter first and last name'

		account_type:
			required: true
			msg: 'Please select an account type'

		account_number: (val, field, model) ->
			# we don't need to check anything if we have the model ID
			return if model.id
			return 'Please enter your account number' unless val
			# return 'Min length is 12' if val.length < 12

		dob_day: (val, field, model) =>
			if !model.dob_year or !model.dob_month
				return
			return 'Please enter a valid day' unless Moment([model.dob_year, parseInt(model.dob_month) - 1, model.dob_day]).isValid()

		dob_month:
			required: true
			msg: 'Please select a month'

		dob_year:
			required: true
			msg: 'Please select a year'

	save: ->
		legalEntity =
			first_name: name.first_name
			last_name: name.last_name
			dob:
				day: @get('dob_day')
				month: @get('dob_month')
				year: @get('dob_year')
			type: @get('account_type')
		if @get('id')
			method = 'put'
			route = getApiRoute('ajax-bank-account', {bankAccountId: @get('id')})
		else
			method = 'post'
			route = getApiRoute('ajax-bank-accounts')

		$.ajax
			url: route
			method: method
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				account_number: @get('account_number')
				routing_number: @get('routing_number')
				country: @get('country')
				currency: @get('currency')
				legal_entity: legalEntity
				tos_accepted: true
				_token: window.GLOBALS._TOKEN

			error: (error) => @handleErrors(error)

	remove: ->
		$.ajax
			url: getApiRoute('ajax-bank-account', {bankAccountId: @get('id')})
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

			error: (error) => @handleErrors(error)

module.exports = Model

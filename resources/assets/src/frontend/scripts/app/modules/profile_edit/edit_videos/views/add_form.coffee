Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Stickit = require 'backbone.stickit'
Validation = require 'backbone.validation'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	tagName: 'form'
	className: 'edit_media--add_video form'

	events:
		'submit': 'submit'
		'click .js-edit_videos--show_form': 'toggleForm'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common )

		@setOptions(options)
		@isOpened = false
		@addStickit()
		@addValidation()
		@addListeners()

	addListeners: ->
		@listenTo @, 'rendered', =>
			@cacheDOM()
			@stickit()

	addStickit: ->
		@bindings =
			'[name="url"]': 'url'
			'[name="title"]': 'title'
			'.edit_media--submit_button':
				attributes:[{
					name: 'disabled'
					observe: ['url', 'title']
					onGet: @onGetSubmit
					}]

	onGetSubmit: (val) ->
		return !(val[0].trim().length and val[1].trim().length)

	render: ->
		@$el.empty().html EditVideos.Templates.AddForm()
		@trigger 'rendered'

		return @

	cacheDOM: ->
		@$el.$fields = @$('.js-edit_videos--fields')
		@$el.$showFormButton = @$('.js-edit_videos--show_form')

		return @

	submit: (e) ->
		e.preventDefault() if e?

		if !@model.validate()
			@addVideo()
			@clearForm()
			@toggleForm()

	addVideo: ->
		@videosCollection.push
			url: @model.get('url')
			title: @model.get('title')
			isNew: true

	clearForm: ->
		@model.set
			url: ''
			title: ''

	toggleForm: ->
		if @isOpened then @hide() else @show()
		@baseView.trigger 'form:toggle', @isOpened

		return @

	show: ->
		@$el.$fields.show()
		@$el.$showFormButton.hide()
		@isOpened = true

		return @

	hide: ->
		@$el.$fields.hide()
		@$el.$showFormButton.show()
		@isOpened = false

		return @

module.exports = View

Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'
JobView = require './views/job'

# models
JobModel = require './models/job'

# collections
JobsCollection = require './collections/jobs'

# templates
BaseTmpl = require 'text!./templates/base.html'
ItemTmpl = require 'text!./templates/item.html'


# list of all instances
window.EditJobs = EditJobs =
	Views:
		Base: BaseView
		Job: JobView
	Models:
		Job: JobModel
	Collections:
		Jobs: JobsCollection
	Templates:
		Base: Handlebars.compile BaseTmpl
		Item: Handlebars.compile ItemTmpl

# events bus
_.extend EditJobs, Backbone.Events

class EditJobs.App
	constructor: (options) ->
		jobs = if options?.jobs? then options.jobs else []

		@collection = new EditJobs.Collections.Jobs 0,
			model: EditJobs.Models.Job
			jobsData: options?.jobs ? []

		@view = new EditJobs.Views.Base
			el: options.$el ? options.el
			collection: @collection

		return {
			collection: @collection
			view: @view

			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = EditJobs

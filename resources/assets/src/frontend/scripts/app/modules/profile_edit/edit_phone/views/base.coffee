Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'click .edit_phones--list--add_item': 'addNewPhoneModel'

	initialize: (options) ->
		super
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins

		@instances = []
		@index = 0

		@setOptions(options)

		@bind()

	bind: ->
		@listenTo @collection, 'add', @addNewPhoneView
		@listenTo @collection, 'update', @ifEmpty
		@listenTo @, 'rendered', =>
			@cacheDom()
			@init()

	init: ->
		if @collection.length
			@collection.forEach (model) =>
				@addPhoneView(model)

		else
			@addNewPhoneModel()

	ifEmpty: ->
		unless @collection.length
			@addNewPhoneModel()

	# Show existing phones
	addPhoneModel: () ->
		@collection.add( new EditPhones.Models.Phone() )

	addPhoneView: (model) ->
		@instances.push phone = new EditPhones.Views.Phone
			model: model
			index: @index

		@$el.$list.append phone.render().el

		@index++

		return @

	# Add new phone
	addNewPhoneModel: () ->
		@collection.add( new EditPhones.Models.NewPhone() )

	addNewPhoneView: (model) ->
		@instances.push phone = new EditPhones.Views.NewPhone
			model: model
			index: @index

		@$el.$list.append phone.render().el

		@index++

		return @

	cacheDom: ->
		@$el.$list = @$('.edit_phones--list--items')

		return @

	render: ->
		@$el.html EditPhones.Templates.Base()

		@trigger 'rendered'

		return @

module.exports = View

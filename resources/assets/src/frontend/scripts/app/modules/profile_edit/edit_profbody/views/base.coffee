Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'click [data-add]': 'addItemModel'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins

		@setOptions(options)
		@initEventsListeners()

	initEventsListeners: ->
		@listenTo @, 'rendered', =>
			@cacheDOM()
			@afterRender()

		@listenTo @collection, 'add', (model) =>
			@addItemView(model)

		@listenTo @collection, 'remove', => @checkIfEmpty()

	cacheDOM: ->
		@$el.$list = @$el.find('.edit_set--list')

	afterRender: ->
		if @collection.length
			@collection.forEach (model) =>
				@addItemView(model)

		else
			@collection.add({})

	addItemModel: ->
		if @collection.length
			unless @validate(true)
				@collection.add({})
		else
			@collection.add({})

	addItemView: (model) ->
		@subViews.push view = new EditProfBody.Views.Item
			model: model

		$( view.render().el ).appendTo( @$el.$list )

	# beforeNewItem, pass it true if you're adding new model.
	validate: (beforeNewItem = false)->
		@errors = []

		if beforeNewItem
			@collection.forEach (model) =>
				errors = model.validate()
				if errors?
					@errors.push errors
		else
			@collection.forEach (model) =>
				if !model.isEmpty()
					errors = model.validate()
					if errors?
						@errors.push errors

		return @errors.length


	render: ->
		@$el.html( EditProfBody.Templates.Base() )
		@trigger 'rendered'

		return @

	removeEmpty: ->
		@subViews.forEach (view) =>
			if view.model.isEmpty()
				view.close()

	checkIfEmpty: ->
		unless @collection.length
			@collection.add({})

module.exports = View

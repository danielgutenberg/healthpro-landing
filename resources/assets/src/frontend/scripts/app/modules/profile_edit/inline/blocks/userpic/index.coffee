Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

Userpic = require 'framework/userpic'
EditImages = require '../../../edit_image'
getApiRoute = require 'hp.api'

class BlockUserpic extends Backbone.View

	attributes:
		'data-options': JSON.stringify
			"_token": window.GLOBALS._TOKEN
			"hasUpload": true
			"uploadUrl": getApiRoute('ajax-profiles-upload-file', {'id': window.GLOBALS._PID})

	events:
		'click .js-edit': (e) ->
			@initImageEdit(e)

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@cacheDom()
		@addUpload()
		@initUserpic()

		@addEditImage()

	cacheDom: ->
		@$el.$figure = @$el.find('figure')
		@$el.$imageWrap = @$el.find('.userpic--inner')
		@$el.$image = @$el.find('img')

	addUpload: ->
		@$el.attr @attributes

		@$el.addClass 'js-userpic'
		@$el.$figure.addClass 'm-has_upload'

		unless @$el.$image.length
			@$el.image = document.createElement('img')
			@$el.$image = $(@$el.image)
			@$el.$imageWrap.append @$el.$image

		@$el.$upload = '<span class="js-userpic--upload_btn userpic--upload_btn" type="button" data-tooltip="dark tooltipster-upload_userpic" title="Upload Photo"><input class="userpic--files_input js-userpic--files_input" type="file" name="avatar"></span>'
		@$el.$imageWrap.append @$el.$upload

	addEditImage: ->
		@$el.$edit = '<span class="userpic--edit js-edit"></span>'
		@$el.$imageWrap.append @$el.$edit

	initUserpic: ->
		@userpic = new Userpic @$el

	initImageEdit: (e) ->
		@originalImageUrl = @$el.find('img').data 'original'

		@editor = new EditImages(@originalImageUrl, {aspectRatio: 1 / 1}, @saveEdited)

	saveEdited: (blob) =>
		@userpic.uploadEdited( blob )


module.exports = BlockUserpic

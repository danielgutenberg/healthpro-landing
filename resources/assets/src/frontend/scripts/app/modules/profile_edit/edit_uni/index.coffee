Backbone = require 'backbone'
Handlebars = require 'handlebars'
BaseView = require './views/base'
EducationView = require './views/education'
EducationModel = require './models/education'
EducationsCollection = require './collections/educations'
ItemTmpl = require 'text!./templates/item.html'
BaseTmpl = require 'text!./templates/base.html'


# list of all instances
window.EditUni = EditUni =
	Views:
		Base: BaseView
		Education: EducationView
	Models:
		Education: EducationModel
	Collections:
		Educations: EducationsCollection
	Templates:
		Item: Handlebars.compile ItemTmpl
		Base: Handlebars.compile BaseTmpl

# events bus
_.extend EditUni, Backbone.Events

class EditUni.App
	constructor: (options) ->
		@collection = new EditUni.Collections.Educations 0,
			model: EditUni.Models.Education
			educationsData: options?.educations ? []

		@view = new EditUni.Views.Base
			el: options.$el ? options.el
			collection: @collection

		return {
			collection: @collection
			view: @view

			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = EditUni

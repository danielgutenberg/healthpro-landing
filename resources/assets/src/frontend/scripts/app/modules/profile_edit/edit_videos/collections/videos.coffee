Backbone = require 'backbone'

class Collection extends Backbone.Collection

	initialize: (models, options) ->

		@fetch(options.data)

	fetch: (data) ->
		if data?
			data.forEach (video) =>
				@add
					id: video.id
					title: video.title
					url: video.url

		return @

module.exports = Collection

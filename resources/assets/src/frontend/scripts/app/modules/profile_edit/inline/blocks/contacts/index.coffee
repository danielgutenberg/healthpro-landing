AbstractBlock = require '../abstract'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

EditView = require './views/edit'

class BasicInfo extends AbstractBlock

	headingTitle: 'Edit Social Links'

	editButtonHolderSelector: 'dd'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@cacheDom()
		@appendEditButton()

	edit: ->
		@subViews.push editView = new EditView
			template: @template
			baseView: @

		@popup.openPopup editView

module.exports = BasicInfo

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
DirtyForms = require 'framework/dirty_forms'

Conditions = require 'app/modules/professional_setup_suitability_conditions'

class ConditionsEdit extends Backbone.View

	events:
		'submit .form': 'submit'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

	render: ->
		@$el.html @template()

		@subViews.push @conditions = new Conditions.App
			presetData: @baseView.aboutModel.toJSON()
			el: @$el.find('.profile_edit_inline--container')
			displayFooter: false
			displayLoading: false
			dirtyForms: false
			groups: [
				'concerns-conditions'
			]

		@initDirtyForms()
		@bind()

		@

	submit: (e) ->
		@conditions.view.save()
		if e? then e.preventDefault()

	bind: ->
		@baseView.popup.showLoading()
		@conditions.view.on 'data:loaded', =>
			@baseView.popup.centerPopup()
			@baseView.popup.hideLoading()

		@conditions.model.on 'data:saved', => @baseView.reloader.reloadWithMessage()
		@conditions.model.on 'ajax:loading', => @baseView.popup.showLoading()
		@conditions.model.on 'ajax:error', => @baseView.popup.hideLoading() # ajax:success

	initDirtyForms: ->
		model = DirtyForms.addForm
			$el: @$el

		@listenTo @conditions.model, 'change', -> model.set 'isDirty', true
		@listenTo @conditions.model, 'data:saved', -> model.set 'isDirty', false

module.exports = ConditionsEdit

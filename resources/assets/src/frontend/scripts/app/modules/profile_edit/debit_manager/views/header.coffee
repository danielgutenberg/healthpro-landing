Backbone = require 'backbone'

class View extends Backbone.View

	events:
		'click [data-method]': 'chooseMethod'

	initialize: (options) ->
		super
		@collections = options.collections
		@baseView = options.baseView
		@profileType = options.profileType
		@render()
		@

	render: ->
		@$el.html DebitManager.Templates.Header
			provider: @profileType == 'provider'

	chooseMethod: (e) ->
		method = $(e.currentTarget).data('method')
		switch method
			when 'add_bank_info' then @openBankAccountPopup()
			when 'add_debit_card' then @openAddCardPopup()

	openAddCardPopup: ->
		@baseView.openAddCardPopup new DebitManager.Models.Card()

	openBankAccountPopup: ->
		@baseView.openBankAccountPopup new DebitManager.Models.BankAccount()

module.exports = View

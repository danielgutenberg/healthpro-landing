Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'
AddFormView = require './views/add_form'
VideosView = require './views/videos'
VideoView = require './views/video'

# models
BaseModel = require './models/base'
VideoModel = require './models/video'
AddFormModel = require './models/add_form'

# collections
VideosCollection = require './collections/videos'
# templates
BaseTmpl = require 'text!./templates/base.html'
VideoTmpl = require 'text!./templates/video.html'
AddFormTmpl = require 'text!./templates/add_form.html'

# list of all instances
window.EditVideos =
	Views:
		Base: BaseView
		AddForm: AddFormView
		Videos: VideosView
		Video: VideoView
	Models:
		Base: BaseModel
		Video: VideoModel
		AddForm: AddFormModel
	Collections:
		Videos: VideosCollection
	Templates:
		Base: Handlebars.compile BaseTmpl
		Video: Handlebars.compile VideoTmpl
		AddForm: Handlebars.compile AddFormTmpl

# events bus
_.extend EditVideos, Backbone.Events

class EditVideos.App
	constructor: (options) ->
		@collection = new EditVideos.Collections.Videos 0,
			model: EditVideos.Models.Video
			data: options?.data

		@view = new EditVideos.Views.Base
			collection: @collection
			hasPresetedData: !!options?.data
			el: options.el ? $('.js-edit_videos')

		return {
			# data
			collection: @collection
			view: @view

			# methods
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = EditVideos

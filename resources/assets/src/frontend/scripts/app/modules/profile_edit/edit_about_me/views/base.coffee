Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

Mixins = require 'utils/backbone_mixins'
dirtyForms = require 'framework/dirty_forms'

require 'backbone.stickit'
require 'backbone.validation'

Maxlength = require 'framework/maxlength'
Select = require 'framework/select'

class View extends Backbone.View

	events:
		'submit .form': 'submit'
		'click .js-edit_about_me--cancel': 'revertChanges'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addValidation()
		@addListeners()

	addListeners: ->
		@listenTo @model, 'data:ready', =>
			@addStickit()
			@render()
			@cacheDom()
			@initDirtyForms()

		@listenTo @model, 'ajax:success', => @clearErrors()

		@listenTo @model, 'ajax:error', (errors) =>
			@clearErrors()
			@showErrors(errors)

	addStickit: ->
		@bindings =
			'[name="business_name"]': 'business_name'
			'[name="tags[experience]"]':
				observe: 'tags.experience'
				selectOptions:
					collection: @model.get('experience_tags') # @presetData.experienceTags
					labelPath: 'text'
					valuePath: 'value'
					defaultOption:
						label: 'Select'
						value: null

			'[name="languages"]':
				observe: 'tags.languages'
				initialize: ($el) ->
					new Select $el
					return @
				selectOptions:
					collection: @model.get('language_options')
					labelPath: 'text'
					valuePath: 'value'

			'[name="about_self_background"]':
				observe: 'about_self_background'
				onGet: @onGetTextarea

			'[name="about_what_i_do"]':
				observe: 'about_what_i_do'
				onGet: @onGetTextarea

			'[name="about_services_benefit"]':
				observe: 'about_services_benefit'
				onGet: @onGetTextarea

			'[name="about_self_specialities"]':
				observe: 'about_self_specialities'
				onGet: @onGetTextarea


	onGetTextarea: (val, options) ->
		@initTextarea( $(options.selector).parent(), val )
		return val

	initTextarea: ($el, val) ->
		unless $el.attr('data-maxlength') is 'inited'
			$('textarea', $el).val(val)
			new Maxlength($el)

	render: ->
		@$el.html EditAboutMe.Templates.Base()
		@stickit()
		@trigger 'rendered'
		@

	cacheDom: -> @

	submit: (e) ->
		e.preventDefault() if e?
		@save()

	save: ->
		return if @model.validate()

		$.when(
			@model.save()
		).then(
			=>
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: EditAboutMe.Config.messages.updateSuccess
		).fail(
			=>
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: EditAboutMe.Config.messages.updateError
		)

	initDirtyForms: ->
		model = dirtyForms.addForm( {$el: @$el} )

		@listenTo @model, 'change', -> model.set('isDirty', true)
		@listenTo @model, 'ajax:success', -> model.set('isDirty', false)

	revertChanges: (e) ->
		e?.preventDefault()
		@model.resetData()


		@$el.find('select, textarea').trigger 'change'

module.exports = View

Handlebars = require 'handlebars'

window.ProfileInlineEdit =
	Popup: require './popup'
	Reloader: require './reloader'

	Models:
		About: require './models/about'

	Blocks:
		about: require './blocks/about'
		basic_info: require './blocks/basic_info'
		conditions: require './blocks/conditions'
		contacts: require './blocks/contacts'
		locations: require './blocks/locations'
		media: require './blocks/media'
		services: require './blocks/services'
		suitability: require './blocks/suitability'
		userpic: require './blocks/userpic'
		qualifications: require './blocks/qualifications'

	Templates:
		Form: Handlebars.compile require('text!./templates/form.html')

class ProfileInlineEdit.App

	editBlocks: {}

	constructor: (@$el) ->
		@initPopup()
		@initReloader()
		@initModels()
		@bind()


	initPopup: ->
		@popup = new ProfileInlineEdit.Popup.App()

	initReloader: ->
		@reloader = new ProfileInlineEdit.Reloader()

	initEditBlocks: ->
		@$el.find('[data-edit]').each (k, block) => @initEditBlock $(block)

	initEditBlock: ($el) ->
		editBlock = $el.data('edit')
		@editBlocks[editBlock] = new ProfileInlineEdit.Blocks[editBlock]
			el: $el
			popup: @popup
			reloader: @reloader
			aboutModel: @aboutModel
			template: ProfileInlineEdit.Templates.Form

	initModels: ->
		@aboutModel = new ProfileInlineEdit.Models.About()

	bind: ->
		@aboutModel.on 'ready', => @initEditBlocks()


if $('[data-profile-edit]').length and window.GLOBALS.PROVIDER_ID
	new ProfileInlineEdit.App $('[data-profile-edit]')

module.exports = ProfileInlineEdit

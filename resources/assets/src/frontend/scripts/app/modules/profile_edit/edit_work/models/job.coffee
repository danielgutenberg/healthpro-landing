Backbone = require 'backbone'

class Model extends Backbone.Model

	initialize: (attrs, options) ->
		@addValidationRules()

	addValidationRules: ->
		@validation =
			company_name:
				required: true

			job_name:
				required: true

			'from.month':
				required: true
				msg: 'Please enter month'

			'from.year':
				required: true
				msg: 'Please enter year'

			'to.month':
				fn: @timeValidation

			'to.year':
				fn: @timeValidation

	timeValidation: (value, attr, computedState) ->
		if !value and !computedState['current']
			if attr is 'to.month'
				return 'Please enter month or click checkbox on right'
			else
				return 'Please enter year or click checkbox on right'
		fromMonth = if +computedState['from.month'] then +computedState['from.month'].trim() else null
		fromYear = if +computedState['from.year'] then +computedState['from.year'].trim() else null
		toMonth = if +computedState['to.month'] then +computedState['to.month'].trim() else null
		toYear = if +computedState['to.year'] then +computedState['to.year'].trim() else null

		dependency = if attr is 'to.month' then toYear else toMonth

		if fromMonth and fromYear and dependency
			if attr is 'to.month'
				if fromYear is toYear and toMonth < fromMonth
					return 'Please enter valid "to" month'

			else
				if fromYear > toYear
					return 'Please enter valid "to" year'

	isEmpty: ->
		empty = true
		_.each @attributes, (item, index) =>
			if item and index != 'current'
				empty = false
				return
		return empty

module.exports = Model

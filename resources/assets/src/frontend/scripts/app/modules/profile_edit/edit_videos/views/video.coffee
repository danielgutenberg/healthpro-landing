Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

Mixins = require 'utils/backbone_mixins'

vexDialog = require 'vexDialog'

class View extends Backbone.View

	className: 'edit_media--item js-edit_video'
	tagName: 'li'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common )

		@addStickit()
		@addListeners()
		@isEditOpen = false

		@options =
			editClass: 'm-edit'

	addListeners: ->
		@listenTo @model, 'loading', => @showLoading()
		@listenTo @model, 'ready', => @hideLoading()
		@listenTo @, 'rendered', =>
			@cacheDOM()
			@addDOMBindings()
			@showPreview()
			@stickit()

	addStickit: ->
		@bindings =
			'[name="title"]': 'title'
			'.edit_media--item_text': 'title'

	render: ->
		@$el.empty().html( EditVideos.Templates.Video( @model.toJSON() ) )

		@trigger 'rendered'

		return @

	cacheDOM: ->
		@$el.$container = @$('.edit_media--item_container')
		@$el.$editButton = @$('.edit_media--item_edit')
		@$el.$saveButton = @$('.edit_media--item_save')
		@$el.$cancelButton = @$('.edit_media--item_cancel')
		@$el.$removeButton = @$('.edit_media--item_remove')
		@$el.$hiddenButtons = @$('.m-hidden_buttons')
		@$el.$title = do =>
			if @$('.edit_media--item_text').length
				@$('.edit_media--item_text')
			else
				$('<p class="edit_media--item_text"></p>').appendTo(@$el.$container).hide()

		@$el.$titleFieldBlock = @$('.edit_media--item_text_field')
		@$el.$titleField = @$('.edit_media--item_text_input')

		@$el.$preview = @$('.edit_media--item_preview')
		@$el.$preview.$img = @$('.edit_media--item_preview img')

	addDOMBindings: ->
		@$el.$editButton.on 'click', => @showEdit()
		@$el.$saveButton.on 'click', => @save()
		@$el.$cancelButton.on 'click', => @hideEdit()
		@$el.$removeButton.on 'click', => @removeItem()

		# click outsite document
		$(document).on 'click', (e) =>
			if !$(e.target).closest(@$el).length && @isEditOpen
				@save()

	showEdit: ->
		@$el.addClass @options.editClass
		@$el.$title.hide()
		@$el.$editButton.hide()
		@$el.$saveButton.show()
		@$el.$hiddenButtons.show()
		@$el.$cancelButton.show()
		@$el.$titleFieldBlock.show()
		@$el.$titleField.focus()
		@isEditOpen = true

	save: ->
		@hideEdit()
		@model.save() # check if title was changed

	hideEdit: ->
		@$el.removeClass @options.editClass
		@$el.$titleFieldBlock.hide()
		@$el.$saveButton.hide()
		@$el.$hiddenButtons.hide()
		@$el.$cancelButton.hide()
		@$el.$title.show()
		@$el.$editButton.show()
		@isEditOpen = false

	showLoading: ->

	hideLoading: ->

	removeItem: ->
		vexDialog.confirm
			message: 'Are you sure you would like to delete this item?'
			callback: (value) =>
				if value
					@model.remove()
					@close()
				else return

	getVideoId: (url) ->
		regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/
		match = url.match(regExp)
		if match and match[2].length is 11
		  return match[2]
		else
			alert "That doesn't look like a youtube url."
			# todo do this with the normal error notifications and errors.

	showPreview: ->
		@$el.$preview.$img.attr('src', 'http://img.youtube.com/vi/' + @getVideoId(@model.get('url')) + '/0.jpg')

module.exports = View

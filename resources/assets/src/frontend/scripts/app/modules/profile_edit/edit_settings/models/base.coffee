Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	initialize: ->
		Dashboard.on 'profile:loaded', =>
			@fetch()

	fetch: ->
		@set 'phone', Dashboard.app.model.get('profile.phone')

		$.ajax
			url: getApiRoute('user-account')
			method: 'get'
			success: (response) =>
				@set 'email', response.data.user.email
				@set 'email_pending', response.data.user.pending_email
				@set 'email_verified', response.data.email_verified

				@trigger 'data:ready'
				Dashboard.trigger 'content:ready'
			error: =>
				Dashboard.trigger 'content:ready'


module.exports = Model

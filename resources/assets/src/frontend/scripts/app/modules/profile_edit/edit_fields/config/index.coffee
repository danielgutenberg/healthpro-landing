Config =
	basic:
		fields: [
			{
				name: 'title'
				label: 'Title'
				binding: '[name="title"]'
				type: 'select'
			}
			{
				name: 'first_name'
				label: 'First Name'
				binding: '[name="first_name"]'
				type: 'text'
			}
			{
				name: 'last_name'
				label: 'Last Name'
				binding: '[name="last_name"]'
				type: 'text'
			}
			{
				name: 'gender'
				label: 'Gender'
				binding: '[name="gender"]'
				type: 'select'
			}
			{
				name: 'business_name'
				label: 'Business Name'
				binding: '[name="business_name"]'
				type: 'text'
			}
			{
				name: 'phone'
				label: 'Phone'
				binding: '[name="phone"]'
				type: 'phone'
			}
			{
				name: 'experience'
				label: 'Years of Experience'
				binding: '[name="experience"]'
				type: 'select'
			}
			{
				name: 'languages'
				label: 'Languages <small>(Optional)</small>'
				binding: '[name="languages"]'
				type: 'select'
				multiple: true
			}
		]

	about:
		fields: [
			{
				name: 'about_self_background'
				label: 'Your Professional Background'
				binding: '[name="about_self_background"]'
				type: 'textarea'
				maxlength: 1000
			}
			{
				name: 'about_what_i_do'
				label: 'What I Do <small>(Optional)</small>'
				binding: '[name="about_what_i_do"]'
				type: 'textarea'
				maxlength: 500
			}
			{
				name: 'about_services_benefit'
				label: 'How My Services Will Benefit You <small>(Optional)</small>'
				binding: '[name="about_services_benefit"]'
				type: 'textarea'
				maxlength: 500
			}
			{
				name: 'about_self_specialities'
				label: 'What Are My Specialities <small>(Optional)</small>'
				binding: '[name="about_self_specialities"]'
				type: 'textarea'
				maxlength: 500
			}
		]

_.each Config, (group) ->
	_.each group.fields, (field) ->

		placeholder = if field.placeholder then "placeholder='#{field.placeholder}'" else ''

		switch field.type
			when 'text'
				field.input = """
					<div class='field m-wide'>
						<span class='field--error'></span>
						<input type='text' name='#{field.name}' #{placeholder}>
					</div>
				"""
			when 'textarea'
				className = if field.maxlength then 'js-maxlength' else ''
				maxlength = if field.maxlength then "data-maxlength='#{field.maxlength}' maxlength='#{field.maxlength}'" else ''
				field.input = """
					<div class='field m-wide #{className}'>
						<span class='field--error'></span>
						<textarea cols='30' rows='6' name='#{field.name}' placeholder='#{field.placeholder ? ""}' #{maxlength} #{placeholder}></textarea>
					</div>
				"""
			when 'select'
				multiple = if field.multiple then 'multiple' else ''
				field.input = """
					<div class='select m-wide'>
						<span class='field--error'></span>
						<select name='#{field.name}' #{multiple}></select>
					</div>
				"""
			when 'phone'
				field.input = """
					<div class='field m-wide'>
						<span class='field--error'></span>
						<input name='#{field.name}' type='tel' x-autocompletetype='tel' #{placeholder}>
					</div>
				"""

module.exports = Config

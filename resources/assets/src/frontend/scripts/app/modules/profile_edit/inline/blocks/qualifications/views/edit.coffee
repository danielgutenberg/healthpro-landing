Handlebars = require 'handlebars'
Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

Template = Handlebars.compile require('text!../templates/base.html')

EditCredentials = require 'app/modules/profile_edit/edit_credentials'

class QualificationsEdit extends Backbone.View

	events:
		'submit .form': 'submit'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

	render: ->
		@$el.html Template()

		@subViews.push @editCredentials = new EditCredentials.App
			presetData: @baseView.aboutModel.toJSON()
			el: @$el.find('.profile_edit_inline--container')
			dirtyForms: false

		@bind()

		@

	submit: (e) ->
		@editCredentials.view.submit()
		if e? then e.preventDefault()

	bind: ->
		@editCredentials.model.on 'data:saved', => @baseView.reloader.reloadWithMessage()
		@editCredentials.model.on 'data:loading', => @baseView.popup.showLoading()
		@editCredentials.model.on 'ajax:error data:ready', => @baseView.popup.hideLoading()

module.exports = QualificationsEdit

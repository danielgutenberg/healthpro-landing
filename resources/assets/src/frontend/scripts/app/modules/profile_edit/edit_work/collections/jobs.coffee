Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Model = require '../models/job'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		super
		Cocktail.mixin( @, Mixins.Common ) # add Mixins

		@setOptions(options)
		@fetch(@jobsData)

	fetch: (data) ->
		if data?
			data.forEach (model) =>
				@add
					'id': model.id ? null
					'company_name': model.company_name ? model.company
					'job_name': model.job_title ? model.job_title
					'from.month': model.from.month
					'from.year': model.from.year
					'to.month': if model.to then model.to.month else null
					'to.year': if model.to then model.to.year else null

	getPreparedData: ->
		returns = []
		@forEach (model) =>
			if !model.isEmpty() and !!model.get('company_name')
				job =
					"id": model.get('id')
					"company_name": model.get('company_name')
					"job_name": model.get('job_name')
					"from":
						"month": model.get('from.month')
						"year": model.get('from.year')

				# if [to] dates isn't empty, add them to job
				if !!model.get('to.month') and !!model.get('to.year')
					job.to =
						"month": model.get('to.month')
						"year": model.get('to.year')

				returns.push job

		return returns

module.exports = Collection

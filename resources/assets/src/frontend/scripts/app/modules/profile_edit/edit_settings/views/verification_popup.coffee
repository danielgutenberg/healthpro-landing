Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'click [data-cancel-update]' 	: 'cancelUpdate'
		'click [data-close]' 			: 'closePopup'
		'click [data-confirm-update]' 	: 'confirmUpdate'
		'click [data-method]' 			: 'verificationMethod'
		'click [data-verify-btn]'		: 'verifyPhone'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@step 			= 'confirmation' # there are two steps - confirmation and success
		@popupName 		= "verification_#{@type}"

		@render()
		@cacheDom()
		@bind()
		@toggleFooter()

	render: ->
		@$el.html UpdateAccount.Templates.VerificationPopup @getPopupData()

		@$el.addClass "popup popup_verification m-#{@type}"
		@$el.attr 'id', "popup_#{@popupName}"
		@$el.attr 'data-popup', @popupName

		$('body').append @$el

		window.popupsManager.addPopup @$el
		window.popupsManager.openPopup @popupName
		@

	getPopupData: ->
		{
			title          : @getTitle()
			content        : @getContent()
			svgBefore      : @getSVG 'before'
			svgAfter       : @getSVG 'after'
			updateBtnLabel : @getUpdateBtnLabel()
		}

	typeStep: -> "#{@type}_#{@step}"

	cancelUpdate: -> @parentView.cancelUpdate()

	confirmUpdate: -> @parentView.confirmUpdate()

	closePopup: ->
		@parentView.closePopup()

	verificationMethod: (e) -> @parentView.verificationMethod $(e.currentTarget).data('method')

	cacheDom: ->
		@$el.$content 	= @$el.find('[data-content]')
		@$el.$svgBefore = @$el.find('[data-svg="before"]')
		@$el.$svgAfter = @$el.find('[data-svg="after"]')
		@$el.$footer 	= @$el.find('[data-footer]')
		@$el.$loading   = @$el.find('.loading_overlay')
		@

	bind: ->
		$(window).on 'keyup.verification_popup', (e) =>
			if e.keyCode is 27 || e.which is 27
				@closePopup()

	remove: ->
		window.popupsManager.removePopup @popupName
		super

	setStep: (step) ->
		@step = step
		@updateContent()
		@updateSvg()
		@toggleFooter()
		@

	updateContent: ->
		@$el.$content.html @getContent()
		@

	updateSvg: ->
		@$el.$svgBefore.html @getSVG('before')
		@$el.$svgAfter.html @getSVG('after')
		@

	toggleFooter: ->
		if @step is 'confirmation'
			@$el.$footer.removeClass 'm-hide'
		else
			@$el.$footer.addClass 'm-hide'

		$(document).trigger 'popup:center'
		@

	getTitle: ->
		switch @type
			when 'email'
				'Email Confirmation'
			when 'phone'
				'Phone Number Verification'

	getContent: ->
		switch @typeStep()
			when 'email_confirmation'
				"
					<div class='popup_verification--content--block'>
						<p>Changing your email address requires a new email verification. Your current email will be used until the new one is verified.</p>
						<p>Do you wish to continue?</p>
					</div>
				"
			when 'email_success'
				"
					<div class='popup_verification--content--block'>
						<p>A verification email has been successfully sent to: <strong>#{@parentView.model.get('email')}</strong></p>
						<p>Follow the link in the email to verify. Your account will be updated once you confirm your new email address.</p>
					</div>
					<div class='popup_verification--content--block'>
						<button class='popup_verification--content--close_btn btn m-large m-green' type='button' data-close>OK</button>
					</div>
				"
			when 'email_success_resend'
				"
					<div class='popup_verification--content--block'>
						<p>A verification email has been successfully sent to: <strong>#{@parentView.model.get('email_pending')}</strong></p>
						<p>Follow the link in the email to verify. Your account will be updated once you confirm your new email address.</p>
					</div>
					<div class='popup_verification--content--block'>
						<button class='popup_verification--content--close_btn btn m-large m-green' type='button' data-close>OK</button>
					</div>
				"
			when 'phone_confirmation'
				"
					<div class='popup_verification--content--block'>
						<p>Changing your phone number requires a new phone verification. Your current phone number will be used until the new one is verified.</p>
						<p>Do you wish to continue?</p>
					</div>
				"
			when 'phone_verification'
				"
					<div class='popup_verification--content--block'>
						<p>Choose a verification method</p>
						<footer>
							<button type='button' class='popup_verification--content--choose_btn btn m-large m-light_blue' data-method='sms'>SMS</button>
							<button type='button' class='popup_verification--content--choose_btn btn m-large m-light_blue' data-method='call'>Call</button>
						</footer>
					</div>
				"

			when 'phone_success'
				"
					<div class='popup_verification--content--block'>
						<p>A verification code has been successfully sent to: <strong>#{@parentView.model.formattedPhone()}</strong></p>
					</div>
					<div class='popup_verification--content--block m-verify_form'>
						<div class='popup_verification--content--verify_field field'>
							<input type='text' data-verify-code placeholder='Enter Verification Code'>
							<div class='field--error'></div>
						</div><button class='popup_verification--content--verify_btn btn m-green' type='button' data-verify-btn>Verify</button>
					</div>
				"

			when 'phone_verified'
				"
					<div class='popup_verification--content--block'>
						<p>A phone number has been successfully updated to: <strong>#{@parentView.model.formattedPhone()}</strong></p>
					</div>
					<div class='popup_verification--content--block'>
						<button class='popup_verification--content--close_btn btn m-large m-green' type='button' data-close>OK</button>
					</div>
				"


	getSVG: (position = 'before') ->
		switch "#{position}_#{@typeStep()}"
			when 'after_email_confirmation'
				UpdateAccount.Config.Images.EmailConfirmation
			when 'before_phone_confirmation', 'before_phone_verification'
				UpdateAccount.Config.Images.PhoneConfirmation
			when 'after_email_success', 'after_email_success_resend', 'before_phone_verified'
				UpdateAccount.Config.Images.Success
			when 'after_phone_success'
				UpdateAccount.Config.Images.PhoneVerification
			else ''

	getUpdateBtnLabel: ->
		switch @type
			when 'email' then 'Update My Email Address'
			when 'phone' then 'Update My Phone Number'
			else 'OK'


	verifyPhone: ->
		$input = @$el.find('[data-verify-code]')
		$field = $input.parent()
		$error = $field.find('.field--error')

		$field.removeClass 'm-error'
		$error.text ''

		@parentView.model.set 'verification_code', @$el.find('[data-verify-code]').val().trim()

		unless @parentView.model.isValid('verification_code')
			$error.text('Please enter your verification code')
			$field.addClass 'm-error'
			return

		@showLoading()
		$.when(
			@parentView.model.verifyCode()
		).then(
			=>
				@hideLoading()
				@setStep 'verified'
				@
		).fail(
			(response) =>
				@hideLoading()
				@parentView.handleVerificationErrors response.responseJSON, 'Could not verify the code. Please reload the page and try again'
		)

module.exports = View

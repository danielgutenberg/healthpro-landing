Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
getApiRoute = require 'hp.api'

require 'backbone.validation'
require 'backbone.stickit'

class View extends Backbone.View

	events:
		'click [data-set-password]': 'savePassword'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@model = new UpdateAccount.Models.Password()

		@addValidation
			selector: "name"

		@addStickit()
		@bind()

	cacheDom: ->
		@$el.$loading = @$el.find('.loading_overlay')

	addStickit: ->
		@bindings =
			'[name="newPassword"]': 'newPassword'
			'[name="confirmNewPassword"]': 'confirmNewPassword'
		@

	savePassword: ->
		unless @model.validate()
			@showLoading()
			$.ajax
				url: getApiRoute('user-account')
				method: 'put'
				type  : 'json'
				contentType: 'application/json; charset=utf-8'
				data: JSON.stringify
					password: @model.get('newPassword')
					current_password: @model.get('currentPassword')
					confirmed_password: @model.get('confirmNewPassword')
					_token: window.GLOBALS._TOKEN
				success: (response) =>
					@hideLoading()
					@baseView.removePopup()
					if window.GLOBALS.PasswordRequired == '1'
						$('.m-warning').remove()
					@updateUser 'Your password has been successfully set'
				error: (error) =>
					@hideLoading()
					@baseView.removePopup()
					res = error.responseJSON
					if res.errors.currentPassword
						return @updateUser res.errors.currentPassword.messages[0]
					@updateUser 'An error has occured, please try again'

	updateUser: (msg) ->
		vexDialog.buttons.YES.text = 'Ok'
		vexDialog.alert
			message: msg

	render: ->
		@$el.html UpdateAccount.Templates.RequirePassword()

		@$el.addClass 'popup require_password'
		@$el.attr 'id', 'popup_require_password'
		@$el.attr 'data-popup', 'require_password'

		@stickit()
		@cacheDom()
		@delegateEvents()

		@

module.exports = View

Backbone = require 'backbone'
Stickit = require 'backbone.stickit'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	el: $('[data-settings-card-manager]')

	instances:
		header: null
		cardsList: null
		popup: null

	initialize: (options) ->
		@collections = options.collections
		@profileType = options.profileType

		@bind()
		@cacheDom()
		@render()

	bind: ->
		@listenTo @collections.cards, 'data:ready', @hideLoading
		@listenTo @collections.cards, 'add remove', @checkMaxCards

	render: ->
		@instances.cardsList = new CreditCardsManager.Views.CardsList
			collections: @collections
			baseView: @
		@$el.$app.append @instances.cardsList.el

		@instances.header = new CreditCardsManager.Views.Header
			collections: @collections
			baseView: @
			el: @$el.$header

	hideLoading: -> @$el.$loading.addClass('m-hide')
	showLoading: -> @$el.$loading.removeClass('m-hide')

	cacheDom: ->
		@$el.$app = @$el.find('.cards_list--app')
		@$el.$header = @$el.find('.cards_list--header')

		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	removePopup: ->
		# remove old popups
		$('#popup_card_form').remove()
		window.popupsManager.popups = []
		@$el.$popup = null

	openPopup: (model) ->
		@removePopup()
		@instances.popup = new CreditCardsManager.Views.CardForm
			collections: @collections
			model: model
			baseView: @
			profileType: @profileType

		$('body').append @instances.popup.$el

		@$el.$popup = $('#popup_card_form')
		window.popupsManager.addPopup @$el.$popup
		window.popupsManager.openPopup('card_form')

	checkMaxCards: ->
		maxCards = CreditCardsManager.Config.MaxCards[@profileType]
		if maxCards > @collections.cards.length
			@$el.$header.removeClass 'm-hide'
		else
			@$el.$header.addClass 'm-hide'

module.exports = View

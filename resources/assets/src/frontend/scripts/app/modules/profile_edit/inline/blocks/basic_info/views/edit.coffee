Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
DirtyForms = require 'framework/dirty_forms'

EditFields = require 'app/modules/profile_edit/edit_fields'

class BasicInfoEdit extends Backbone.View

	events:
		'submit .form': 'submit'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

	render: ->
		@$el.html @template()

		@subViews.push @inlineProfile = new EditFields.App
			type: 'basic'
			presetData: @baseView.aboutModel.toJSON()
			el: @$el.find('.profile_edit_inline--container')

		@initDirtyForms()
		@bind()

		@

	submit: (e) ->
		@inlineProfile.view.save()
		if e? then e.preventDefault()

	bind: ->
		@inlineProfile.model.on 'data:saved', => @baseView.reloader.reloadWithMessage()
		@inlineProfile.model.on 'data:saving', => @baseView.popup.showLoading()
		@inlineProfile.model.on 'data:error', => @baseView.popup.hideLoading()

	initDirtyForms: ->
		model = DirtyForms.addForm
			$el: @$el

		@listenTo @inlineProfile.model, 'change', -> model.set 'isDirty', true
		@listenTo @inlineProfile.model, 'data:saved', -> model.set 'isDirty', false

module.exports = BasicInfoEdit

Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	fetched: false

	initialize: (options = {}) ->
		@setPresetData options.presetData if options.presetData

	setPresetData: (data) ->
		@set 'educations', data.educations
		@set 'professional_body', data.professional_body
		@set 'jobs', data.jobs
		@set 'certificates' , data.assets.certificate

		@fetched = true

	fetch: ->
		if @fetched
			@trigger 'data:ready'
			return
		@trigger 'data:loading'
		$.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'get'
			success: (res) =>
				@set 'educations', res.data.educations
				@set 'professional_body', res.data.professional_body
				@set 'jobs', res.data.jobs
				@set 'certificates' , res.data.assets.certificate
				@trigger 'data:ready'

	save: ->
		@trigger 'data:loading'
		$.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'put'
			dataType: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				educations: @get('educations')
				professional_body: @get('professional_body')
				jobs: @get('jobs')
				_token: window.GLOBALS._TOKEN
			success: (res) =>
				@trigger 'data:saved'
				@trigger 'ajax:success'

			error: (res) =>
				@trigger 'ajax:error', res.responseJSON.errors
				@trigger 'data:saved'

module.exports = Model

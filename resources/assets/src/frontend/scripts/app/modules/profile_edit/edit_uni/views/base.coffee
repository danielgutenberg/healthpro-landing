Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'click [data-add]': 'addEducationModel'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins

		@subViews = []
		@index = 0

		@setOptions(options)

		@bind()

	bind: ->
		@listenTo @collection, 'add', @addEducationView
		@listenTo @collection, 'remove', @ifEmpty
		@listenTo @collection, 'data:ready', @hideLoading
		@listenTo @collection, 'data:loading', @showLoading
		@listenTo @, 'rendered', =>
			@cacheDom()
			@init()

	init: ->
		if @collection.length
			@collection.forEach (model) =>
				@addEducationView(model)
		else
			@addEducationModel()

		@hideLoading()

	ifEmpty: ->
		@addEducationModel() unless @collection.length

	removeEmpty: ->
		@subViews.forEach (view) =>
			if view.model.isEmpty()
				view.close()

	addEducationModel: ->
		if @collection.length
			@collection.add({}) unless @validate()
		else
			@collection.add({})

	validate: ->
		@errors = []

		@collection.forEach (model) =>
			errors = model.validate()
			if errors?
				@errors.push errors

		return @errors.length

	addEducationView: (model) ->
		@subViews.push job = new EditUni.Views.Education
			model: model
			index: @index

		@$el.$list.append job.render().el

		@index++

		return @

	cacheDom: ->
		@$el.$list = @$el.find('.edit_set--list')
		@$el.$loading = @$el.find('.loading_overlay')

		return @

	showLoading: ->
		@$el.$loading.removeClass('m-hide')

		return @

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

		return @


	render: ->
		@$el.html EditUni.Templates.Base()

		@trigger 'rendered'

		return @

module.exports = View

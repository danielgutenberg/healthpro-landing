class Reloader

	messageId: 'hp_inline_success_msg'

	messages:
		default: 'The changes have been saved'

	constructor: ->
		return unless sessionStorage
		@maybeDisplaySuccessMessage()

	reload: ->
		window.location.reload()

	reloadWithMessage: (msg = null) ->
		msg = @messages.default unless msg
		@addMessage msg
		@reload()

	addMessage: (msg) -> sessionStorage.setItem @messageId, msg
	removeMessage: -> sessionStorage.removeItem @messageId
	getMessage: -> sessionStorage.getItem @messageId
	hasMessage: -> sessionStorage.getItem(@messageId)?

	maybeDisplaySuccessMessage: ->
		if @hasMessage()
			@displayMessage()
			@removeMessage()

	displayMessage: ->
		msg = @getMessage()

		$msg = $("<div class='profile_edit_inline--message'><p>#{msg}</p></div>")
		$('body').append $msg

		close = ->
			$msg.fadeOut(200, ->
				$msg.remove()
			)

		$msg.on 'click', close
		setInterval close, 5000


module.exports = Reloader

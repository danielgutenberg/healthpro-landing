AbstractModel = require './abstract'

class Model extends AbstractModel

	initValidation: ->
		@validation =
			"about_self_background": [
				{
					required: true
					msg: 'Please enter your background'
				}
				{
					maxLength: 1000
					msg: 'Your text is too long, please enter a maximum of 1000 characters'
				}
			]
			"about_what_i_do":
				maxLength: 500
				msg: 'Your text is too long, please enter a maximum of 500 characters'

			"about_services_benefit":
				maxLength: 500
				msg: 'Your text is too long, please enter a maximum of 500 characters'

			"about_self_specialities":
				maxLength: 500
				msg: 'Your text is too long, please enter a maximum of 500 characters'


	getSaveData: ->
		{
			about_self_background: @get('about_self_background')
			about_what_i_do: @get('about_what_i_do')
			about_services_benefit: @get('about_services_benefit')
			about_self_specialities: @get('about_self_specialities')
		}


module.exports = Model

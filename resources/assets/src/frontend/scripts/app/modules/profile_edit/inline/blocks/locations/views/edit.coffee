Handlebars = require 'handlebars'
Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

getRoute = require 'hp.url'

Template = Handlebars.compile require('text!../templates/base.html')

class LocationsEdit extends Backbone.View

	events:
		'click .proceed': 'proceed'
		'click [data-edit-close]': 'closePopup'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

	render: ->
		@$el.html Template()
		@

	proceed: ->
		win = window.open getRoute('dashboard-edit-locations-services'), '_blank'
		win.focus()
		@closePopup()

	closePopup: ->
		@baseView.popup.removePopup()

module.exports = LocationsEdit

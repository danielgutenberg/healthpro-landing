AbstractBlock = require '../abstract'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

EditView = require './views/edit'

class Conditions extends AbstractBlock

	editButtonHolderSelector: '.profile--main--title'

	headingTitle: 'Edit Conditions &amp; Concerns'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@cacheDom()
		@appendEditButton()

	edit: ->
		@subViews.push editView = new EditView
			template: @template
			baseView: @

		@popup.openPopup editView

module.exports = Conditions

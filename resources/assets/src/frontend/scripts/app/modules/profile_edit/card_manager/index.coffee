Backbone = require 'backbone'
Handlebars = require 'handlebars'
BaseView = require './views/base'
HeaderView = require './views/header'
CardsListView = require './views/cards_list'
CardView = require './views/card'
CardFormView = require './views/card_form'
CardModel = require './models/card'
CardsCollection = require './collections/cards'
HeaderTmpl = require 'text!./templates/header.html'
CardsListTmpl = require 'text!./templates/cards_list.html'
CardTmpl = require 'text!./templates/card.html'
CardFormTmpl = require 'text!./templates/card_form.html'

# list of all instances
window.CreditCardsManager =
	Config:
		Messages:
			removeCard: 'Are you sure you want to remove this card? You will not be able to book appointments until you add a new card.'
			error: 'Something went wrong. Please reload the page and try again.'
		MaxCards:
			provider: 1
			client: 10
		Fields:
			provider:
				card_type :
					label: 'Card Type'
					class: 'cards_list--table--type'
				card_number :
					label: 'Card Number'
				card_holder :
					label: 'Name on Card'
				card_exp :
					label: 'Expiry Date'
					class: 'cards_list--table--exp'
				card_funding :
					label: 'Funding'
					class: 'cards_list--table--funding'
				card_withdraw :
					label: 'Withdraw'
					class: 'cards_list--table--withdraw'
				actions :
					label: '&nbsp;'
			client:
				card_type :
					label: 'Card Type'
					class: 'cards_list--table--type'
				card_number :
					label: 'Card Number'
				card_holder :
					label: 'Name on Card'
				card_exp :
					label: 'Expiry Date'
					class: 'cards_list--table--exp'
				card_funding :
					label: 'Funding'
					class: 'cards_list--table--funding'
				actions :
					label: '&nbsp;'
	Views:
		Base: BaseView
		Header: HeaderView
		CardsList: CardsListView
		Card: CardView
		CardForm: CardFormView
	Models:
		Card: CardModel
	Collections:
		Cards: CardsCollection
	Templates:
		Header: Handlebars.compile HeaderTmpl
		CardsList: Handlebars.compile CardsListTmpl
		Card: Handlebars.compile CardTmpl
		CardForm: Handlebars.compile CardFormTmpl

# events bus
_.extend CreditCardsManager, Backbone.Events

class CreditCardsManager.App
	constructor: ->
		@view = new CreditCardsManager.Views.Base
			profileType: window.GLOBALS._PTYPE
			collections:
				cards: new CreditCardsManager.Collections.Cards null, {model: CreditCardsManager.Models.Card}

CreditCardsManager.app = new CreditCardsManager.App()

module.exports = CreditCardsManager

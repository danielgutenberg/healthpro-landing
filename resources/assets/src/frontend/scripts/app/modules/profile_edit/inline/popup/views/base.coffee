Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	editView: null

	attributes:
		'class': 'profile_edit_inline popup'
		'data-popup': 'profile_edit_inline_popup'
		'id' : 'popup_profile_edit_inline_popup'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common # add Mixins
		@setOptions options

		@bind()

	bind: ->
		@listenTo @, 'rendered', =>
			@cacheDom()
			@appendView()

	cacheDom: ->
		@$el.$editContainer = @$el.find('[data-edit-container]')
		@$el.$loading = @$el.find('.loading_overlay')
		@$el.$close = @$el.find('[data-edit-close]')


	render: ->
		@$el.html @template
			title: @editView.baseView.getHeadingTitle()
		@trigger 'rendered'
		@

	appendView: ->
		@$el.$editContainer.append @editView.render().$el if @editView


	close: ->
		# cleanup before removing the view
		@editView.close() if @editView
		@remove()

module.exports = View

Handlebars = require 'handlebars'

InlineProfile =
	Config: require('./config')
	Views:
		Base: require('./views/base')
	Models:
		About: require('./models/about')
		Basic: require('./models/basic')

	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

class InlineProfile.App
	constructor: (options) ->

		model = if options.type then _.capitalize(options.type) else 'Basic'
		type = options.type ? 'basic'

		@model = new InlineProfile.Models[model]
			type: type
			config: InlineProfile.Config[type]
			presetData: options.presetData

		@view = new InlineProfile.Views.Base
			model: @model
			el: options.el
			template: InlineProfile.Templates.Base

		return {
			view: @view
			model: @model
			close: @close
		}

	close: ->
		@view.close()
		@view.remove()

module.exports = InlineProfile

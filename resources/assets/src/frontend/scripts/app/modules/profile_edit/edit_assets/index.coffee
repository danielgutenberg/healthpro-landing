Backbone = require 'backbone'
Handlebars = require 'handlebars'

window.EditAssets = EditAssets =
	Views:
		Base: require('./views/base')
	Models:
		Base: require('./models/base')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

# events bus
_.extend EditAssets, Backbone.Events

class EditAssets.App
	constructor: (options) ->
		@model = new EditAssets.Models.Base
			presetData: options.presetData
		@view = new EditAssets.Views.Base
			model: @model
			el: options.el

		return {
			# data
			model: @model
			view: @view

			# methods
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

if $('.js-edit_assets').length
	new EditAssets.App
		el: '.js-edit_assets'

module.exports = EditAssets

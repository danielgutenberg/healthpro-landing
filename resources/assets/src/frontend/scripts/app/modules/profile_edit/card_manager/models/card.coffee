Backbone = require 'backbone'
Validation = require 'backbone.validation'
getApiRoute = require 'hp.api'
Moment = require 'moment'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class Model extends Backbone.Model

	defaults:
		id: ''
		account_id: ''
		funding: ''
		application: ''
		card_holder: ''
		card_number: ''
		card_type: ''
		exp_month: ''
		exp_year: ''

	initialize: ->
		super
		Cocktail.mixin @, Mixins.Models.Validation

	validation:
		card_holder:
			required: true
			msg: 'Please enter card holder\'s name'

		card_number: (val, field, model) ->
			return if model.id # we don't need to check anything if we have the model ID
			'Please enter a valid card number' if !val or val.length < 12

		exp_month: (val) ->
			'Please enter a valid expiration date mm/yyyy' if _.indexOf([1..12], val * 1) < 0

		exp_year: (val) ->
			'Please enter a valid expiration date mm/yyyy' if Moment().year() > val * 1

		cvv: (val) ->
			val = val + ""
			'Please enter valid CVV' if val.length > 4 or val.length < 3

	save: ->
		if @get('id')
			method = 'put'
			route = getApiRoute('ajax-credit-card', {creditCardId: @get('id')})
		else
			method = 'post'
			route = getApiRoute('ajax-credit-cards')

		$.ajax
			url: route
			method: method
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				name: @get('card_holder')
				number: @get('card_number').replace(/\s+/g, '')
				expiryMonth: @get('exp_month')
				expiryYear: @get('exp_year')
				cvv: @get('cvv')
				type: 'credit'
				_token: window.GLOBALS._TOKEN

			error: (error) =>
				@handleErrors(error)

	remove: ->
		$.ajax
			url: getApiRoute('ajax-credit-card', {creditCardId: @get('id')})
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

module.exports = Model

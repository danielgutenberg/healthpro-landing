Backbone = require 'backbone'
Stickit = require 'backbone.stickit'
vexDialog = require 'vexDialog'

class View extends Backbone.View

	tagName: 'tr'

	events:
		'click [data-card-edit]': 'editCard'
		'click [data-card-remove]': 'removeCard'


	initialize: (options) ->

		@model = options.model
		@collections = options.collections
		@baseView = options.baseView
		@parentView = options.parentView

		@bind()
		@render()
		@stickit()

	bind: ->
		@bindings =
			'[data-card_number]':
				observe: 'card_number'
				updateMethod: 'html'
				onGet: (val) -> "<i>&bull;&bull;&bull;&bull;</i>&nbsp;<i>&bull;&bull;&bull;&bull;</i>&nbsp;<i>&bull;&bull;&bull;&bull;</i>&nbsp;<span>#{val}</span>"

			'[data-card_type]':
				observe: 'card_type'
				updateMethod: 'html'
				onGet: (val) -> "<img src='/assets/frontend/images/content/booking/cc-#{val}.png' alt=''>"

			'[data-card_holder]':
				observe: ['first_name', 'last_name']
				onGet: (val) -> val[0] + ' ' + val[1]

			'[data-card_withdraw]':
				observe: 'application'
				onGet: (val) -> if val is 'withdraw' then 'Yes' else 'No'

			'[data-card_funding]':
				observe: 'funding'

			'[data-card_exp]':
				observe: ['exp_month', 'exp_year']
				updateMethod: 'html'
				onGet: (val) -> "#{val[0]}&nbsp;/&nbsp;#{val[1]}"

	render: ->
		@$el.html DebitManager.Templates.Card()
		@renderFields()
		@parentView.$el.$listing.append @$el

	renderFields: ->
		fields = DebitManager.Config.Fields[@baseView.profileType]
		html = ''
		_.each fields, (header, field) ->
			html += "<td data-#{field}></td>" unless field is 'actions' # we don't want to render actions
		@$el.prepend html

	editCard: ->
		@baseView.openAddCardPopup @model

	removeCard: ->
		vexDialog.confirm
			message: DebitManager.Config.Messages.removeCard
			callback: (value) =>
				return unless value
				@baseView.showLoading()
				$.when(@model.remove()).then(
					(response) =>
						@baseView.hideLoading()
						if response.data is true
							@destroy()
						else
							@showError response
					, (error) =>
						@baseView.hideLoading()
						@showError error
				)

	destroy: ->
		@$el.fadeOut 200, =>
			@collections.cards.remove @model
			@remove()
			unless @collections.cards.length then @baseView.showNotificationAlert()
			@unbind()

	showError: (error) ->
		alert DebitManager.Config.Messages.error

module.exports = View

Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: ->
		super
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-credit-cards')
			method: 'get'
			success: (res) =>
				_.forEach res.data, (card) =>
					@add @format(card)
				@trigger 'data:ready'
			error: (err) =>
				@trigger 'data:ready'

	format: (card) ->
		id: card.id
		application: card.application
		funding: card.funding
		card_holder: card.card_holder # will be available later
		card_number: card.card_number
		card_type: card.card_type.toLowerCase()
		exp_month: card.exp_month
		exp_year: card.exp_year
		account_id: parseInt card.account_id, 10

module.exports = Collection

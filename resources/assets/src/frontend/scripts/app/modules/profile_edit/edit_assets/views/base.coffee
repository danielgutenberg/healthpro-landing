Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

Mixins = require 'utils/backbone_mixins'
getApiRoute = require 'hp.api'

EditPhotos = require 'app/modules/profile_edit/edit_media/edit_photos'
EditVideos = require 'app/modules/profile_edit/edit_videos'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions options

		@addListeners()

		@model.fetch()


	addListeners: ->
		@listenToOnce @model, 'data:fetched', => @render()
		@listenToOnce @, 'rendered', => @initParts()

	render: ->
		@$el.html EditAssets.Templates.Base()
		@trigger 'rendered'

	initParts: ->
		@initPhotos()
		@initVideos()

	initPhotos: ->

		@subViews.push @editPhotos = new EditPhotos @$el.find('.js-edit_photos'), {
			"type": "photo"
			"typeForUri" : "photos"
			"url": getApiRoute('ajax-profile-assets', {
				profileId: window.GLOBALS._PID
				assetsType: 'photos'
			})
			"_token": window.GLOBALS._TOKEN
			"profileId": window.GLOBALS._PID
			"assets": @model.get('assets.photo')
		}

	initVideos: ->
		@subViews.push @editVideos = new EditVideos.App
			el: @$el.find('.js-edit_videos')
			data: @model.get('assets.video')

		@editVideos.view.render()

module.exports = View

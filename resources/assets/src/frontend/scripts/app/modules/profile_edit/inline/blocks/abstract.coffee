Backbone = require 'backbone'

class AbstractBlock extends Backbone.View

	editButtonHolderSelector: null

	headingTitle: 'Edit'

	events:
		'click button[data-edit-open]': 'edit'

	cacheDom: ->
		if @editButtonHolderSelector
			@$editButtonHolder = @$el.find @editButtonHolderSelector
		else
			@$editButtonHolder = @$el

	appendEditButton: ->
		@$el.$edit = $('<button class="profile_edit_inline--edit_btn" data-edit-open></button>')
		@$editButtonHolder.append @$el.$edit

	getHeadingTitle: -> @headingTitle

module.exports = AbstractBlock

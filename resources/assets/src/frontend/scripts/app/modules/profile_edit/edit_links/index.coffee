Handlebars = require 'handlebars'

BaseView = require './views/base'
LinkView = require './views/link'
LinksCollection = require './collections/links'
LinkModel = require './models/link'

ItemTmpl = require 'text!./templates/item.html'
BaseTmpl = require 'text!./templates/base.html'

window.EditLinks =
	Models:
		Link: LinkModel
	Views:
		Base: BaseView
		Link: LinkView
	Collections:
		Links: LinksCollection
	Templates:
		Base: Handlebars.compile BaseTmpl
		Item: Handlebars.compile ItemTmpl

class EditLinks.App
	constructor: (options) ->
		@collection = new EditLinks.Collections.Links 0,
			model: EditLinks.Models.Link
			links: if options.links? then options.links else null

		@view = new EditLinks.Views.Base
			el: options.el
			collection: @collection
			container: options.container

		return {
			collection: @collection
			view: @view

			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = EditLinks

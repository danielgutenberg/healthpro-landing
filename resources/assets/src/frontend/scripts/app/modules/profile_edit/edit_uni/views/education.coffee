Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
getApiRoute = require 'hp.api'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'
Select = require 'framework/select'
Datepicker = require 'framework/datepicker'

require 'backbone.validation'
require 'backbone.stickit'

class View extends Backbone.View

	className: 'edit_set--item'
	tagName: 'li'

	events:
		'click .edit_set--item_remove': 'closeItem'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addValidation
			selector: "data-selector"

		@addStickit()
		@addListeners()

	addListeners: ->
		@listenTo @, 'rendered', =>
			@cacheDom()
			@initSelects()
			@stickit()
			@undelegateEvents()
			@delegateEvents()

			@listenTo @model, 'change:education_id', @updateEducationName

	cacheDom: ->
		@$el.$select = @$('[data-select]')

		return @

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

	render: ->
		university = EditUni.Templates.Item
			index: @index
			university_id: @model.get('education_id')
			university_name: @model.get('education_name')
			searchUniversitiesUrl: getApiRoute('ajax-core-education-search')
			months: @generateMonths()
			years: @generateYears()

		@$el.html university

		@trigger 'rendered'

		return @

	updateEducationName: ->
		name = @$('.select2-selection__rendered').text()
		@model.set
			education_name: name

		return @

	initSelects: ->
		_.each @$el.$select, (item) =>
			new Select $(item)

	addStickit: ->
		@bindings =
			'[name="education_id"]':
				"observe": "education_id",
				"onSet": () ->
					return @$('[name="education_id"]').val()
			'[name="degree"]': 'degree'
			'[name="from[month]"]': 'from.month'
			'[name="from[year]"]': 'from.year'
			'[name="to[month]"]': 'to.month'
			'[name="to[year]"]': 'to.year'
			'[name="current"]':
				'initialize': =>
					if @model.get('from.month') and !@model.get('to.month')
						@model.set('current', true)
						@disableTo()
				'observe': 'current'
				'onSet': (val) =>
					if val
						@disableTo()
					else
						@enableTo()
					return val

		return @

	closeItem: ->
		@close()

	disableTo: () ->
		@model.set('to.year', '')
		@model.set('to.month', '')
		@$('[name="to[month]"]').attr('disabled', 'disabled')
		@$('[name*="to[year]"]').attr('disabled', 'disabled')

	enableTo: () ->
		@$('[name="to[month]"]').attr('disabled', false)
		@$('[name="to[year]"]').attr('disabled', false)

	generateMonths: ->
# write auto generating json (http://stackoverflow.com/questions/8513032/less-than-10-add-0-to-number)
# monthsList = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

		return [
			{
				value: "01"
				text: "January"
			}
			{
				value: "02"
				text: "February"
			}
			{
				value: "03"
				text: "March"
			}
			{
				value: "04"
				text: "April"
			}
			{
				value: "05"
				text: "May"
			}
			{
				value: "06"
				text: "June"
			}
			{
				value: "07"
				text: "July"
			}
			{
				value: "08"
				text: "August"
			}
			{
				value: "09"
				text: "September"
			}
			{
				value: "10"
				text: "October"
			}
			{
				value: "11"
				text: "November"
			}
			{
				value: "12"
				text: "December"
			}
		]

	generateYears: ->
		years = []
		start = 1950
		current = new Date().getFullYear()

		createObj = (val) ->
			return {
			value: val
			text: val
			}

		years.push(createObj(current--)) while start <= current

		return years

module.exports = View

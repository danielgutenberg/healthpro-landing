Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: (models, @options) ->
		@model = @options.model

		if @options.links?
			@addFetchData(@options.links)
		else
			@getData()

	getData: ->
		$.ajax
			method: 'get'
			url: getApiRoute('ajax-profiles-self', {id: 'me'})
			success: (response) =>
				links = response.data.social_pages
				@addFetchData(links)
				@trigger 'data:ready'

	addFetchData: (links) ->
		for link, value of links
			@add
				link: link
				value: value

		@trigger 'data:ready'

module.exports = Collection

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions options
		@addListeners()

	addListeners: ->
		unless @hasPresetedData?
			@listenTo @collection, 'data:fetched', =>
				@render()
		else
			@render()

		@listenTo @, 'rendered', =>
			@cacheDOM()
			@initParts()

	initParts: ->
		@initVideos()
		@initAddForm()

	initVideos: ->
		@subViews.push @videos = new EditVideos.Views.Videos
			collection: @collection

		@$el.append @videos.el

		return @

	initAddForm: ->
		@subViews.push @addForm = new EditVideos.Views.AddForm
			videosCollection: @collection
			model: new EditVideos.Models.AddForm()
			baseView: @

		@$el.append @addForm.render().el

		return @

	cacheDOM: ->

	render: ->
		@trigger 'rendered'

		return @

module.exports = View

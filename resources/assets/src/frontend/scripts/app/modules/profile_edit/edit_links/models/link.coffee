Backbone = require 'backbone'

class Model extends Backbone.Model

	validation:
		value:
			required: false
			pattern: 'url'
			msg: 'Value must be a valid url starting with http'

	defaults:
		link: ''
		value: ''

module.exports = Model

Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'
ItemView = require './views/item'

# models
ItemModel = require './models/item'

# collections
ListCollection = require './collections/list'

# templates
BaseTmpl = require 'text!./templates/base.html'
ItemTmpl = require 'text!./templates/item.html'

# list of all instances
window.EditProfBody = EditProfBody =
	Views:
		Base: BaseView
		Item: ItemView
	Models:
		Item: ItemModel
	Collections:
		List: ListCollection
	Templates:
		Base: Handlebars.compile BaseTmpl
		Item: Handlebars.compile ItemTmpl

# events bus
_.extend EditProfBody, Backbone.Events

class EditProfBody.App
	constructor: (options) ->
		@collection = new EditProfBody.Collections.List 0,
			data: options.data
			model: EditProfBody.Models.Item
		@view = new EditProfBody.Views.Base
			el: options.el
			collection: @collection

		return {
			# data
			collection: @collection
			view: @view

			# methods
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = EditProfBody

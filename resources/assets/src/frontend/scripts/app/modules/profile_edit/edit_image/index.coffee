require 'cropperjs'
require 'tooltipster'

Handlebars = require 'handlebars'
editImageTemplate = require 'text!./templates/edit_image.html'

class EditImage
	constructor: (@imageUrl, @options, @callback) ->
		@init()

	init: ->
		@openPopup()

	initTooltips: ->
		@$popup.find('[data-tooltip]').tooltipster
			theme: 'tooltipster-dark'

	render: ->
		@editImageTemplate = Handlebars.compile editImageTemplate
		editImageHtml = @editImageTemplate
			image: @imageUrl

	removePopup: ->
		window.popupsManager.closePopup('edit_image')
		$('#popup_edit_image').remove()
		window.popupsManager.popups = []
		@$popup = null

		$('body').css
			'overflow-x': 'initial'

	openPopup: ->
		@removePopup()

		editImageHtml = @render()

		$('body').append $(editImageHtml)
		# fix page jump caused by tooltipster
		$('body').css
			'overflow-x': 'hidden'

		@$popup = $('#popup_edit_image')
		@$image = @$popup.find('.js-image')
		window.popupsManager.addPopup @$popup
		window.popupsManager.openPopup('edit_image')

		@cacheDom()
		@initTooltips()
		@initImageEdit(@$image)

	initImageEdit: ($image) ->
		options =
			scalable: false
			zoomable: false
			autoCrop: false
			autoCropArea: .8
			ready: =>
				@bindEditActions()

		_.extend options, @options

		@cropper = new Cropper $image[0], options

	cacheDom: ->
		@$editorAction = $('[data-editor-action]', @$popup)
		@$actionRotateLeft = $('[data-editor-action="rotate_left"]', @$popup)
		@$actionRotateRight = $('[data-editor-action="rotate_right"]', @$popup)
		@$actionCrop = $('[data-editor-action="crop"]', @$popup)
		@$actionApply = $('[data-editor-action="apply"]', @$popup)
		@$actionReset = $('[data-editor-action="reset"]', @$popup)
		@$save = $('[data-editor-save]', @$popup)

		@$preview = $('.edit_image--preview', @$popup)

	bindEditActions: ->
		@$editorAction.on 'click', =>
			@hidePreview()

		@$actionRotateLeft.on 'click', =>
			@cropper.clear()
			@cropper.rotate(-90)

		@$actionRotateRight.on 'click', =>
			@cropper.clear()
			@cropper.rotate(90)

		@$actionCrop.on 'click', =>
			@cropper.crop()

		@$actionApply.on 'click', =>
			@$preview.removeClass 'm-hide'
			@$preview.html @cropper.getCroppedCanvas()

		@$actionReset.on 'click', =>
			@cropper.clear()
			@cropper.reset()

		@$save.on 'click', =>
			@saveEdited()

	saveEdited: ->
		@removePopup()
		@cropper.getCroppedCanvas().toBlob (blob) =>
			@callback(blob)

	hidePreview: ->
		@$preview.html ''
		@$preview.addClass 'm-hide'

module.exports = EditImage

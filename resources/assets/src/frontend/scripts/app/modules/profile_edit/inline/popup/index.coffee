Handlebars = require 'handlebars'
vexDialog = require 'vexDialog'
DirtyForms = require 'framework/dirty_forms'


Popup =
	Views:
		Base: require './views/base'
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

class Popup.App

	popupName: 'profile_edit_inline_popup'

	popup: null

	bind: ->
		# make sure we remove popup when user closes it
		@popup.$el.on 'popup:closed', => @removePopup()

		@popup.$el.$close.on 'click', (e) =>
			e?.preventDefault()
			@closePopup()

	closePopup: ->
		if DirtyForms.hasUnsaved()
			vexDialog.confirm
				message: 'You have unsaved changes! Exiting this window will not save unsaved changes.'
				callback: (value) =>
					if value
						@forceClosePopup()
						DirtyForms.reset()
			return

		@forceClosePopup()

	forceClosePopup: ->
		window.popupsManager.closePopup @popupName
		@removePopup()

	removePopup: ->
		if @popup
			@popup.close()
			window.popupsManager.removePopup @popupName
			@popup = null
			@

	openPopup: (view) ->
		# make sure we don't have a popup
		@removePopup()

		@popup = new Popup.Views.Base
			editView: view
			baseView: @
			template: Popup.Templates.Base

		$('body').append @popup.render().$el

		window.popupsManager.addPopup @popup.$el
		window.popupsManager.openPopup @popupName

		popup = window.popupsManager.getPopup @popupName
		popup.setCloseOnEsc false if popup

		@bind()

	centerPopup: ->
		if @popup
			popup = window.popupsManager.getPopup @popupName
			popup.detectBigPopup() if popup

	showLoading: -> @popup.showLoading() if @popup
	hideLoading: -> @popup.hideLoading() if @popup

module.exports = Popup

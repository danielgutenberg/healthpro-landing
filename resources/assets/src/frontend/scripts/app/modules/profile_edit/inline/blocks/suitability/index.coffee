AbstractBlock = require '../abstract'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

EditView = require './views/edit'

class Suitability extends AbstractBlock

	headingTitle: 'Edit Suitability'

	editButtonHolderSelector: '.profile--block--header--title'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@cacheDom()
		@appendEditButton()

	edit: ->
		@subViews.push editView = new EditView
			template: @template
			baseView: @

		@popup.openPopup editView

module.exports = Suitability

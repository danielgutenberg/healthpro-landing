Backbone = require 'backbone'
Handlebars = require 'handlebars'

# list of all instances
window.EditAboutMe = EditAboutMe =
	Config:
		messages:
			updateSuccess: 'Your changes have been saved'
			updateError: 'Something went wrong. Please reload the page and try again'
	Views:
		Base: require('./views/base')
	Models:
		Base: require('./models/base')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

# events bus
_.extend EditAboutMe, Backbone.Events

class EditAboutMe.App
	constructor: (options) ->
		@view = new EditAboutMe.Views.Base
			model: new EditAboutMe.Models.Base()
			el: options.el

if $('.js-edit_about_me').length
	new EditAboutMe.App
		el: '.js-edit_about_me'

module.exports = EditAboutMe

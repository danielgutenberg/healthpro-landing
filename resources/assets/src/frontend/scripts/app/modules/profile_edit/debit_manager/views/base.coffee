Backbone = require 'backbone'
Stickit = require 'backbone.stickit'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	el: $('[data-settings-card-manager]')

	instances:
		header: null
		cardsList: null
		bankAccountList: null
		popup: null

	initialize: (options) ->
		@collections = options.collections
		@profileType = options.profileType

		@bind()
		@cacheDom()
		@render()

	bind: ->
		# @listenTo @collections.cards, 'add remove', @checkMaxCards
		@listenTo @collections.cards, 'data:ready', @hideLoading
		@listenTo @collections.bank_accounts, 'data:ready', @hideLoading
		@listenTo @collections.cards, 'add update remove', @checkCollections
		@listenTo @collections.bank_accounts, 'add update remove', @checkCollections

	render: ->
		@instances.header = new DebitManager.Views.Header
			collections: @collections
			baseView: @
			el: @$el.$header
			profileType: @profileType

		@instances.cardsList = new DebitManager.Views.CardsList
			collections: @collections
			baseView: @

		@instances.bankAccountList = new DebitManager.Views.BankAccountsList
			collections: @collections
			baseView: @

	checkCollections: ->
		if @collections.cards.length
			@hideHeader()
			@hideAccounts()
			@showCards()
			@$el.$app.append @instances.cardsList.el
		else if @collections.bank_accounts.length
			@hideHeader()
			@hideCards()
			@showAccounts()
			@$el.$app.append @instances.bankAccountList.el
		else
			@hideAccounts()
			@hideCards()
			@showHeader()

	hideLoading: -> @$el.$loading.addClass('m-hide')
	showLoading: -> @$el.$loading.removeClass('m-hide')

	hideHeader: -> @$el.$header.addClass('m-hide')
	showHeader: -> @$el.$header.removeClass('m-hide')

	hideAccounts: -> @$el.find('.cards_list--app--bank_accounts').addClass('m-hide')
	showAccounts: -> @$el.find('.cards_list--app--bank_accounts').removeClass('m-hide')

	hideCards: -> @$el.find('.cards_list--app--cards').addClass('m-hide')
	showCards: -> @$el.find('.cards_list--app--cards').removeClass('m-hide')

	cacheDom: ->
		@$el.$app = @$el.find('.cards_list--app')
		@$el.$header = @$el.find('.cards_list--header')

		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	removePopup: ->
		# remove old popups
		$('#popup_card_form').remove()
		$('#popup_bank_account_form').remove()
		window.popupsManager.popups = []
		@$el.$popup = null

	openAddCardPopup: (model) ->
		@removePopup()
		@instances.popup = new DebitManager.Views.CardForm
			collections: @collections
			model: model
			baseView: @
			profileType: @profileType

		$('body').append @instances.popup.$el

		@$el.$popup = $('#popup_card_form')
		window.popupsManager.addPopup @$el.$popup
		window.popupsManager.openPopup('card_form')

	openBankAccountPopup: (model) ->
		@removePopup()
		@instances.popup = new DebitManager.Views.BankAccountForm
			collections: @collections
			model: model
			baseView: @
			profileType: @profileType

		$('body').append @instances.popup.$el

		@$el.$popup = $('#popup_bank_account_form')
		window.popupsManager.addPopup @$el.$popup
		window.popupsManager.openPopup('bank_account_form')

	hideNotificationAlert: ->
		@notificationAlert = $('.alert.m-notification')
		if @notificationAlert? and @notificationAlert.length then @notificationAlert.removeClass 'm-show'

	showNotificationAlert: ->
		@notificationAlert = $('.alert.m-notification')
		if @notificationAlert? and @notificationAlert.length then @notificationAlert.addClass 'm-show'

	checkMaxCards: ->
		maxCards = DebitManager.Config.MaxCards[@profileType]
		if maxCards > @collections.cards.length
			@$el.$header.removeClass 'm-hide'
		else
			@$el.$header.addClass 'm-hide'

module.exports = View

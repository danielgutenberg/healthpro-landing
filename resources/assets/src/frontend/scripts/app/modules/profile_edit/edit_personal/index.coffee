Backbone = require 'backbone'
Handlebars = require 'handlebars'

Device = require 'utils/device'

# list of all instances
window.EditPersonal = EditPersonal =
	Config:
		messages:
			updateSuccess	: 'Your changes have been saved'
			updateError		: 'Something went wrong. Please reload the page and try again'
	Views:
		Base: require('./views/base')
	Models:
		Base: require('./models/base')
	Templates:
		Base: Handlebars.compile do ->
			if Device.isMobile()
				require('text!./templates/mobile/base.html')
			else
				require('text!./templates/desktop/base.html')
		UpdateSlug: Handlebars.compile require('text!./templates/update_slug.html')

# events bus
_.extend EditPersonal, Backbone.Events

class EditPersonal.App
	constructor: (options) ->
		@view = new EditPersonal.Views.Base
			model	: new EditPersonal.Models.Base()
			el		: options.el

if $('.js-edit_personal').length
	new EditPersonal.App
		el: '.js-edit_personal'

module.exports = EditPersonal

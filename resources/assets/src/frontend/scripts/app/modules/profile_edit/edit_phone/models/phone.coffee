Backbone = require 'backbone'

class Model extends Backbone.Model

	initialize: (args...) ->
		super


module.exports = Model

Backbone = require 'backbone'

class Model extends Backbone.Model

	defaults:
		title: ''
		'expiry_date.month': null
		'expiry_date.year': null

	validation:
		title:
			required: true
			msg: 'Please enter the name of the association'

		'expiry_date.month':
			fn: (value, attr, computedState) ->
				month = value
				year = computedState['expiry_date.year']
				currentMonth = (new Date()).getMonth() + 1
				currentYear = (new Date()).getFullYear()
				if month?
					if year?
						if year is currentYear
							if currentMonth > +month
								return 'Month should be before the current month'
				else
					if year? and year > '-1'
						return 'Select a month'

		'expiry_date.year':
			fn: (value, attr, computedState) ->
				year = value
				month = computedState['expiry_date.month']
				if month? and month > '-1'
					if !year? or year <= '-1'
						return 'Select a year'

	initialize: (attrs, options) ->

	isEmpty: ->
		empty = true
		_.each @attributes, (item, index) =>
			if !!item
				empty = false

		return empty

module.exports = Model

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'payment'

class View extends Backbone.View
	attributes:
		'class': 'popup popup_card_form card_form'
		'data-popup': 'card_form'
		'id' : 'popup_card_form'

	events:
		'click .card_form--add_card': 'submitForm'
		'submit': 'submitForm'
		'click .card_form--cancel': 'cancelForm'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base

		@collections = options.collections
		@baseView = options.baseView
		@model = options.model
		@profileType = options.profileType

		@modelDefaults = @model.toJSON()

		@isNew = if @model.get('id') then false else true

		@bind()
		@addValidation
			valid: (view, attr) ->
				$field = view.$el.find(if attr.match /^exp_/ then "[name='card_exp']" else "[name='#{attr}']")
				$field.parents('.field, .select').removeClass('m-error')
					.find('.field--error').html('')

			invalid: (view, attr, error) ->
				$field = view.$el.find(if attr.match /^exp_/ then "[name='card_exp']" else "[name='#{attr}']")
				$field.parents('.field, .select').addClass('m-error')
					.find('.field--error').html(error)
				$field.on('focus.validation', ->
					$field.parents('.field, .select').removeClass('m-error')
					$field.off('focus.validation')
				)

		@render()
		@cacheDom()
		@stickit()
		@initCardCheck()

	bind: ->
		@bindings =
			'[data-title]':
				observe: 'id'
				onGet: (val) =>
					switch @profileType
						when 'provider'
							if val then 'Edit Debit Card' else 'Add Debit Card'
						when 'client'
							if val then 'Edit Credit Card' else 'Add Credit Card'
			'[name="card_holder"]': 'card_holder'
			'[name="card_number"]': 'card_number'
			'[name="card_exp"]':
				observe: ['exp_month', 'exp_year']
				onGet: (val) -> val[0] + ' / ' + val[1] if val[0] and val[1]
				onSet: (val) ->
					exp = val.split '/'
					exp[0] = if exp[0] then exp[0] * 1 else 0
					exp[1] = if exp[1] then exp[1] * 1 else 0
					exp

			'[name="cvv"]':
				observe: 'cvv'
				classes:
					"m-edit":
						observe: 'id'
						onGet: (val) -> if val then true else false
			'.card_form--card_logo.m-visa':
				classes:
					"m-active":
						observe: 'card_type'
						onGet: (val) -> val is 'visa'
			'.card_form--card_logo.m-mastercard':
				classes:
					"m-active":
						observe: 'card_type'
						onGet: (val) -> _.indexOf(['mastercard', 'maestro'], val) > -1
			'.card_form--card_logo.m-amex':
				classes:
					"m-active":
						observe: 'card_type'
						onGet: (val) -> val is 'amex'
			'.card_form--form_number--label':
				classes:
					'm-hide':
						observe: 'id'
						onGet: (val) -> if val then false else true

			'.card_form--form_number--field':
				classes:
					'm-hide':
						observe: 'id'
						onGet: (val) -> if val then true else false

			'.card_form--card_number':
				observe: 'card_number'
				updateMethod: 'html'
				onGet: (val) -> '<i>&bull;&bull;&bull;&bull;</i><i>&bull;&bull;&bull;&bull;</i><i>&bull;&bull;&bull;&bull;</i>' + val

	initCardCheck: ->
		@$el.$cardNumber.payment('formatCardNumber')
		@$el.$cardCvc.payment('formatCardCVC')
		@$el.$cardExp.payment('formatCardExpiry')

		@$el.$cardNumber.on 'keyup', (e) =>
			@model.set 'card_type', $.payment.cardType e.target.value

#			don't allow numbers in card holders name
		@$el.$cardName.on 'keypress', (e) =>
			code = e.charCode
			if code > 47 and code < 58
				return false

	submitForm: (e) ->
		e.preventDefault() if e?
		@hideError()
		errors = @model.validate()
		if !errors?
			@showLoading()
			$.when( @model.save() )
				.then(
					(res) =>
						@hideLoading()
						if @isNew
							@collections.cards.push res.data
						else
							@model.set 'cvv', null
						@baseView.removePopup()
					, (error) =>
						@hideLoading()
						if error.responseJSON?.errors
							_.each error.responseJSON.errors, (err) =>
								@showError err.messages[0] if err.messages?.length
				)
		@

	cancelForm: ->
		# reset model values
		@model.set @modelDefaults
		@model.set 'cvv', null

		@baseView.removePopup()

	# base functions
	cacheDom: ->
		@$el.$cardNumber = @$el.find('[name="card_number"]')
		@$el.$cardExp = @$el.find('[name="card_exp"]')
		@$el.$cardCvc = @$el.find('[name="cvv"]')
		@$el.$cardName = @$el.find('[name="card_holder"]')
		@$el.$cardLogos = @$el.find('.card_form--card_logo')

		@$el.$buttonCancel = @$el.find('.card_form--cancel')
		@$el.$buttonDelete = @$el.find('.card_form--delete')

		@$el.$errorsContainer = @$el.find('.card_form--errors')

		@

	render: ->
		@$el.html CreditCardsManager.Templates.CardForm()

		@$el.$loading = $('<span class="loading_overlay m-light m-hide"><span class="spin m-cog"></span></span>')
		@$el.find('.popup--container').append @$el.$loading

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

	showLoading: ->
		@$el.$loading.removeClass('m-hide')


	showError: (error) ->
		@$el.$errorsContainer.html("<span>#{error}</span>").removeClass('m-hide')

	hideError: ->
		@$el.$errorsContainer.html('').addClass('m-hide')

module.exports = View

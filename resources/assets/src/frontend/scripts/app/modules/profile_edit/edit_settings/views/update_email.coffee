Abstract = require './abstract/update'

class View extends Abstract

	type: 'email'

	initialize: ->
		super
		@events['click [data-resend-email]'] = 'resendEmail'
		@events['click [data-form-notification-close]'] = 'closeNotification'

		@delegateEvents()

	addStickit: ->
		super
		@bindings['[name="email"]'] =
			observe: 'email'

		@bindings['[data-email]'] =
			observe: 'email_pending'
			onGet: (val) ->
				return val

		@bindings['[data-email-verified]'] =
			classes:
				'm-hide':
					observe: 'email_pending'
					onGet: (val) ->
						!val?
		@

	cacheDom: ->
		@$notification = $('[data-email-verified]', @$el)

	afterUpdate: ->
		super
		@reload = false
		@model.set('email_pending', @model.get('email'))
		@model.resetFields()

	resendEmail: ->
		e?.preventDefault()
		@showLoading()
		$.when(
			@model.resendEmail()
		).then(
			(response) =>
				@hideLoading()
				@initPopup()
				@popupView.setStep 'success_resend'
		).fail(
			(response) =>
				msg = response.responseJSON.errors?.error?.messages?[0]
				@hideLoading()
				if msg is 'This email has already been verified'
					@model.set 'email_pending', @model.get ''
					@displayMsg(msg)
				else
					@displayMsg(UpdateAccount.Config.Messages.updateError)
		)

	closeNotification: ->
		@$notification.addClass 'm-hide'

module.exports = View

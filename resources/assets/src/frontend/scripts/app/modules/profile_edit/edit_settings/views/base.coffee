Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Device = require 'utils/device'
Tabs = require 'framework/tabs'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@render()
		@cacheDom()
		@addListeners()

	addListeners: ->
		@showLoading()
		@listenToOnce @model, 'data:ready', =>
			@hideLoading()
			@initParts()

	render: ->
		@$el.html UpdateAccount.Templates.Base()

	initParts: ->
		if Device.isMobile()
			new Tabs @$el.find('[data-tabs]')

		@initEmail()
		@initPassword()
		@initPhone()
		@requirePassword()

	initPassword: ->
		@password = new UpdateAccount.Views.Password
			$el: @$el.$password
			model: new UpdateAccount.Models.Password
			baseView: @

	initEmail: ->
		@email = new UpdateAccount.Views.Email
			$el: @$el.$email
			model: new UpdateAccount.Models.Email
				email: @model.get('email')
				email_verified: @model.get('email_verified')
				email_pending: @model.get('email_pending')
			baseView: @

	initPhone: ->
		if @model.get('phone')?
			@phone = new UpdateAccount.Views.Phone
				$el: @$el.$phone
				model: new UpdateAccount.Models.Phone
					number	: @model.get('phone').number
					id		: @model.get('phone').id
					verified: @model.get('phone').verified
				baseView: @

	cacheDom: ->
		@$el.$password = @$el.find('[data-update-password]')
		@$el.$email = @$el.find('[data-update-email]')
		@$el.$phone = @$el.find('[data-update-phone]')

	requirePassword: ->
		return unless @manualPasswordRequired
		@popupView = new UpdateAccount.Views.RequirePassword
			$el: @$el.$password
			baseView: @

		$('body').append @popupView.render().$el
		window.popupsManager.addPopup @popupView.$el
		window.popupsManager.openPopup 'require_password'

	removePopup: ->
		window.popupsManager.closePopup 'require_password'
		if @popupView
			@popupView.close()
			window.popupsManager.removePopup 'require_password'
			@popupView = null
			@

	showLoading: -> Dashboard.trigger 'content:loading'

	hideLoading: -> Dashboard.trigger 'content:ready'

module.exports = View

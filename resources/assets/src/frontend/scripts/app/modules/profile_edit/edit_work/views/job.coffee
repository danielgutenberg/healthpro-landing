Backbone = require 'backbone'
Validation = require 'backbone.validation'
Stickit = require 'backbone.stickit'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'

class View extends Backbone.View

	className: 'edit_set--item'
	tagName: 'li'

	events:
		'click .edit_set--item_remove': 'closeItem'

	initialize: (options) ->
		super
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins

		@data =
			months: @generateMonths()
			years: @generateYears()

		@setOptions(options)
		@addValidation
			selector: "data-selector"

		@addStickit()

	addStickit: ->
		@bindings =
			'[name*="company_name"]': 'company_name'
			'[name*="job_title"]': 'job_name'
			'[name*="[from][month]"]': 'from.month'
			'[name*="[from][year]"]': 'from.year'
			'[name*="[to][month]"]': 'to.month'
			'[name*="[to][year]"]': 'to.year'
			'[name*="[current]"]':
				'initialize': =>
					if @model.get('from.month') and !@model.get('to.month')
						@model.set('current', true)
						@disableTo()
				'observe': 'current'
				'onSet': (val) =>
					if val
						@disableTo()
					else
						@enableTo()
					return val

		return @

	render: ->
		@$el.html EditJobs.Templates.Item
			index: @index
			months: @data.months
			years: @data.years
			profile_jobs_id: @model.get('id')

		@stickit()

		@delegateEvents()

		return @

	closeItem: ->
		@close()

	disableTo: () ->
		@model.set('to.year', '')
		@model.set('to.month', '')
		@.$('[name*="[to][month]"]').attr('disabled', 'disabled')
		@.$('[name*="[to][year]"]').attr('disabled', 'disabled')

	enableTo: () ->
		@.$('[name*="[to][month]"]').attr('disabled', false)
		@.$('[name*="[to][year]"]').attr('disabled', false)

	generateMonths: ->
# write auto generating json (http://stackoverflow.com/questions/8513032/less-than-10-add-0-to-number)
# monthsList = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

		return [
			{
				value: "01"
				text: "January"
			}
			{
				value: "02"
				text: "February"
			}
			{
				value: "03"
				text: "March"
			}
			{
				value: "04"
				text: "April"
			}
			{
				value: "05"
				text: "May"
			}
			{
				value: "06"
				text: "June"
			}
			{
				value: "07"
				text: "July"
			}
			{
				value: "08"
				text: "August"
			}
			{
				value: "09"
				text: "September"
			}
			{
				value: "10"
				text: "October"
			}
			{
				value: "11"
				text: "November"
			}
			{
				value: "12"
				text: "December"
			}
		]

	generateYears: ->
		years = []
		start = 1950
		current = new Date().getFullYear()

		createObj = (val) ->
			return {
			value: val
			text: val
			}

		years.push(createObj(current--)) while start <= current

		return years

module.exports = View

Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'
PhoneView = require './views/phone'
NewPhoneView = require './views/new_phone'

# models
PhoneModel = require './models/phone'
NewPhoneModel = require './models/new_phone'

# collections
PhonesCollection = require './collections/phones'

# templates
BaseTmpl = require 'text!./templates/base.html'
ItemTmpl = require 'text!./templates/item.html'
NewItemTmpl = require 'text!./templates/new_item.html'

# list of all instances
window.EditPhones = EditPhones =
	Views:
		Base: BaseView
		Phone: PhoneView
		NewPhone: NewPhoneView
	Models:
		Phone: PhoneModel
		NewPhone: NewPhoneModel
	Collections:
		Phones: PhonesCollection
	Templates:
		Base: Handlebars.compile BaseTmpl
		Item: Handlebars.compile ItemTmpl
		NewItem: Handlebars.compile NewItemTmpl

# events bus
_.extend EditPhones, Backbone.Events

class EditPhones.App
	constructor: (options) ->
		phones = if options?.phones? then options.phones else []
		@$el = if options?.$el? then options.$el

		@collection = new EditPhones.Collections.Phones 0,
			phonesData: phones

		@view = new EditPhones.Views.Base
			el: @$el
			collection: @collection

		return {
			collection: @collection
			view: @view

			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

if $('.edit_phones').length
	EditPhones = new EditPhones.App({
		$el: $('.edit_phones')
		phones: window.editPhone.phones
		})

	#for dashboard
	EditPhones.view.render()

module.exports = EditPhones

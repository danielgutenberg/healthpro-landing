Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
DirtyForms = require 'framework/dirty_forms'
getApiRoute = require 'hp.api'

EditLinks = require 'app/modules/profile_edit/edit_links'

class BasicInfoEdit extends Backbone.View

	events:
		'submit .form': 'submit'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

	render: ->
		@$el.html @template()

		@subViews.push @editLinks = new EditLinks.App
			el		: @$el.find('.profile_edit_inline--container')
			links	: @baseView.aboutModel.get('social_pages')

		@editLinks.view.render()

		@initDirtyForms()

		@

	submit: (e) ->
		@save() unless @validate()
		if e? then e.preventDefault()

	validate: ->
		@errors = []
		@editLinks.collection.forEach (model) =>
			errors = model.validate()
			@errors.push errors if errors?
		@errors.length

	save: ->
		@baseView.popup.showLoading()
		$.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify @getSaveData()
			success: =>
				@trigger 'data:saved'
				@baseView.reloader.reloadWithMessage()
			error: (response) =>
				@trigger 'ajax:error', response.responseJSON.errors
				@baseView.popup.hideLoading()

	getSaveData: ->
		{
			social_pages: do =>
				links = {}
				@editLinks.collection.forEach (model) => links[model.get('link')] = model.get('value') ? ''
				links
			_token: window.GLOBALS._TOKEN
		}

	initDirtyForms: ->
		model = DirtyForms.addForm
			$el: @$el

		@listenTo @editLinks.collection, 'change', -> model.set 'isDirty', true
		@listenTo @, 'data:saved', -> model.set 'isDirty', false


module.exports = BasicInfoEdit

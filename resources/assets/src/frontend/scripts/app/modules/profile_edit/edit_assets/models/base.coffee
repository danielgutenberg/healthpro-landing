Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	fetched: false

	initialize: (options = {}) ->
		@setPresetData options.presetData if options.presetData

	setPresetData: (data) ->
		@set 'assets.photo', data.assets.photo
		@set 'assets.video', data.assets.video

		@fetched = true

	fetch: ->
		if @fetched
			@trigger 'data:fetched'
			return

		window.Dashboard.trigger 'content:loading' if window.Dashboard
		$.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'get'
			success: (res) =>
				@set
					'assets.photo': res.data.assets.photo
					'assets.video': res.data.assets.video

				@trigger 'data:fetched'
				window.Dashboard.trigger 'content:ready' if window.Dashboard

module.exports = Model

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Select = require 'framework/select'
vexDialog = require 'vexDialog'

require 'backbone.stickit'
require 'backbone.validation'


class View extends Backbone.View

	el: '[data-receive-payments]'

	events:
		'click [data-cancel-btn]': 'revert'
		'click [data-update-btn]': 'update'
		'click [data-method-connect-btn]': 'redirectConnect'
		'click [data-method-disconnect-btn]': 'disconnectMethod'
		'click [data-learn-more]': 'learnMore'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@redirecting = false

		@addValidation()
		@addListeners()
		@bind()
		@addStickit()

		@showLoading()

	addListeners: ->
		@listenTo @model, 'data:ready', =>
			@render()

	bind: ->
		window.onbeforeunload = (e) =>
			if @model.initialAllowPayments isnt @model.get('allow_payments') and not @redirecting
				dialogText = ReceivePayments.Config.Messages.changesNotSaved
				e.returnValue = dialogText
				dialogText

	addStickit: ->
		@bindings =
			'[name="allow_payments"]':
				observe: 'allow_payments'
				initialize: ($el, model) ->
					$el.on 'click', (e) =>
						# handling bindings manually because of callback
						$el = $(e.currentTarget)
						val = $el.val()
						if val is 'in_person' and @model.get('has_gift_certificates')
							e.preventDefault()
							vexDialog.confirm
								buttons: [
									$.extend {}, vexDialog.buttons.NO, {text: 'Cancel'}
									$.extend {}, vexDialog.buttons.YES, {text: 'Confirm'}
								]
								message: ReceivePayments.Config.Messages.confirmGiftCertificatesDisable
								callback: (value) =>
									return unless value
									$el.prop 'checked', true
									model.set('allow_payments', val)

			'[data-method-settings-container]':
				classes:
					'm-hide':
						observe: 'allow_payments'
						onGet: (val) ->
							return val is 'in_person'

			'[data-methods-select]':
				classes:
					'm-hide':
						observe: 'allow_payments'
						onGet: (val) ->
							return val is 'in_person'

			'[data-method-error]':
				classes:
					'm-hide':
						observe: 'allow_payments'
						onGet: (val) ->
							paymentSet = @model.get('paypal.id')? or @model.get('stripe')?
							return paymentSet

			'[data-actions]':
				classes:
					'm-hide':
						observe: ['allow_payments', 'paypal.id', 'stripe']
						onGet: (val) ->
							changed = @model.initialAllowPayments isnt val[0]
							if val[0] is 'in_person'
								paymentSet = true
							else
								paymentSet = val[1]? or val[2]?

							return !paymentSet or !changed

			'[data-method-connect="paypal"]':
				classes:
					'm-hide':
						observe: 'paypal.email'
						onGet: (val) ->
							return val?

			'[data-method-disconnect="paypal"]':
				classes:
					'm-hide':
						observe: 'paypal.email'
						onGet: (val) ->
							return not val?

			'[data-method-email]': 'paypal.email'

			'[data-method-connect="stripe"]':
				classes:
					'm-hide':
						observe: 'stripe'
						onGet: (val) ->
							return val?

			'[data-method-disconnect="stripe"]':
				classes:
					'm-hide':
						observe: 'stripe'
						onGet: (val) ->
							return not val?

	render: ->
		@$el.html ReceivePayments.Templates.Base
			email: @model.get 'email'
			key: @model.get 'key'
		@stickit()
		@cacheDom()
		@hideLoading()

	cacheDom: ->
		@$el.methodSettings = $('[data-method-settings]', @$el)
		@$el.methodSettings.paypal = $('[data-method-settings="paypal"]', @$el)
		@$el.methodSettings.stripe = $('[data-method-settings="stripe"]', @$el)
		@$el.$error = $('[data-method-error]')

	redirectConnect: (e) ->
		if e?
			e.preventDefault()
			@showLoading()
			@redirecting = true
			$.when(
				@model.saveToSession()
			).then(
				=>
					switch $(e.currentTarget).data('method-connect-btn')
						when 'paypal' then window.location = GLOBALS.PAYPALREDIRECT
						when 'stripe' then window.location = GLOBALS.STRIPEREDIRECT

			).fail(
				=>
					@hideLoading()
					vexDialog.alert
						message: ReceivePayments.Config.Messages.updateError
			)

	disconnectMethod: (e) ->
		if e?
			e.preventDefault()
			method = $(e.currentTarget).data('method-disconnect-btn')
			@showLoading()
			$.when(
				@model.disconnectMethod(method)
			).then(
				=>
					@hideLoading()
					@showDisconnectSuccess(method)
					unless @model.get('paypal.email')? or @model.get('stripe')?
						@raiseMethodError(ReceivePayments.Config.Messages.connectSystem)

			).fail(
				=>
					@hideLoading()
					vexDialog.alert
						message: ReceivePayments.Config.Messages.updateError
			)

	update: ->
		vexDialog.buttons.YES.text = 'Ok'
		if @validate()
			@showLoading()
			$.when(
				@model.save()
			).then(
				=>
					@hideLoading()
					vexDialog.alert
						message: ReceivePayments.Config.Messages.updateSuccess
			).fail(
				=>
					@hideLoading()
					vexDialog.alert
						message: ReceivePayments.Config.Messages.updateError
			)
		else
			@raiseMethodError(ReceivePayments.Config.Messages.connectSystem)

	validate: ->
		isValid = true
		if @model.get('allow_payments') isnt 'in_person'
			unless @model.get('paypal.email')? or @model.get('stripe')?
				isValid = false

		return isValid

	showDisconnectSuccess: (method) ->
		switch method
			when 'paypal'
				message = '''
					<h2>Paypal Disconnected</h2>
					<p>You disconnected from your paypal account.</p>
				'''
			when 'stripe'
				message = '''
					<h2>Stripe Disconnected</h2>
					<p>You disconnected from your stripe account.</p>
				'''

		unless @model.get('paypal.email')? or @model.get('stripe')?
			message = '''
				<h2>Online Payment Options Disconnected</h2>
				<p>You disconnected from all your online payment options.</p>
				<p>You can now receive payments only in-person.</p>
			'''
			if @model.get('has_gift_certificates')
				message += '<p>Purchasing new Gift Certificates will not be available</p>'

		vexDialog.alert
			message: message
			className: 'vex-custom'
			contentClassName: 'vex-content-centered vex-content-custom'

	learnMore: (e) ->
		e?.preventDefault()
		type = $(e.currentTarget).data('learn-more')

		popupView = new ReceivePayments.Views.LearnMore

		$('body').append popupView.render().$el
		window.popupsManager.addPopup popupView.$el
		window.popupsManager.openPopup 'learn_more'

	showLoading: -> Dashboard.trigger 'content:loading'
	hideLoading: -> Dashboard.trigger 'content:ready'

	raiseMethodError: (msg) ->
		@$el.$error.removeClass 'm-hide'
		@$el.$error.html "<div class='alert m-warning m-show m-standalone'><span>#{msg}</span></div>"

	revert: ->
		@model.reset()

module.exports = View

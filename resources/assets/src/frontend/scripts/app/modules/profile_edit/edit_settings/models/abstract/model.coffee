Backbone = require 'backbone'

class Model extends Backbone.Model

	cachedField: null

	initialize: ->
		@addValidationRules()
		@cacheField()

	addValidationRules: ->
		@validation = {}

	cacheField: ->
		@cachedFieldValue = @get(@cachedField)
		@

	resetFields: ->
		@set 'current_password', ''
		@set @cachedField, @cachedFieldValue

	getAjaxData: (data = {}) -> JSON.stringify $.extend {}, {_token: window.GLOBALS._TOKEN}, data

module.exports = Model

Backbone = require 'backbone'

class Collection extends Backbone.Collection

	initialize: (models, options) ->

		@fetch(options.data)

	fetch: (data) ->
		if data?
			data.forEach (item) => @add({
				title: item.title
				'expiry_date.month': item.expiry_date.month
				'expiry_date.year': +item.expiry_date.year
				})

	getPreparedData: ->
		data = []
		@forEach (item) ->
			if !item.isEmpty()
				data.push({
					title: item.get('title')
					expiry_date:
						month: item.get('expiry_date.month')
						year: item.get('expiry_date.year')
					})

		return data


module.exports = Collection

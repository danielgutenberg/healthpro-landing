Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'
dateSelect = require 'utils/date_select'

require 'backbone.validation'
require 'backbone.stickit'

class View extends Backbone.View

	tagName: 'li'
	className: 'edit_set--item'

	events:
		'click .edit_set--item_remove': 'removeItem'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins

		@setOptions(options)
		@initEventsListeners()
		@initStickitBindings()
		@addValidation()

	initEventsListeners: ->
		@listenTo @, 'rendered', =>
			@stickit()

	initStickitBindings: ->
		@bindings =
			'[name="title"]': 'title'
			'[name="expiry_date[month]"]':
				observe: 'expiry_date.month'
				selectOptions:
					collection: dateSelect.getMonthsOptions()
					labelPath: 'label'
					defaultOption:
						label: 'Month'
						value: null
			'[name="expiry_date[year]"]':
				observe: 'expiry_date.year'
				selectOptions:
					collection: dateSelect.getYearsOptions( new Date().getFullYear(), 10 )
					defaultOption:
						label: 'Year'
						value: null

	removeItem: ->
		@close()

	render: ->
		@$el.html EditProfBody.Templates.Item()
		@trigger 'rendered'

		@


module.exports = View

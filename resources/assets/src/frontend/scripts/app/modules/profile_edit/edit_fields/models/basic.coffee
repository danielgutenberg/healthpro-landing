AbstractModel = require './abstract'

class Model extends AbstractModel

	initValidation: ->
		@validation =
			first_name: [
				{
					required: true
					msg: 'Please enter First Name'
				},
				{
					pattern: /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/
					msg: 'Please enter a valid First Name'
				},
				{
					maxLength: 26
				}
			]
			last_name:[
				{
					required: true
					msg: 'Please enter Last Name'
				},
				{
					pattern: /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/
					msg: 'Please enter a valid Last Name'
				},
				{
					maxLength: 26
				}
			]

			phone: [
				{
					required: false
				}
				{
					pattern: /^\s*1?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/
					msg: 'Please enter phone number with the following format +1 (XXX) XXX-XXXX.  The +1 is optional'
				},
				{
					maxLength: 20
				}
			]

	getSaveData: ->
		{
			title: @get('title')
			first_name: @get('first_name')
			last_name: @get('last_name')
			phone: @get('phone')
			gender: @get('gender')
			business_name: @get('business_name')
			tags:
				languages: @get('languages')
				experience: [@get('experience')]
		}


module.exports = Model

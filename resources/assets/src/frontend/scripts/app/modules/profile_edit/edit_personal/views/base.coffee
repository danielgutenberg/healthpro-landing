Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

Device = require 'utils/device'
Mixins = require 'utils/backbone_mixins'

Tabs = require 'framework/tabs'
dirtyForms = require 'framework/dirty_forms'

PersonalDetails = require 'app/modules/profile_personal_details'
EditLinks = require 'app/modules/profile_edit/edit_links'

class View extends Backbone.View

	events:
		'submit .form': 'submit'
		'click .js-edit_about_me--cancel': 'revertChanges'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()

	addListeners: ->
		@listenTo @, 'rendered', =>
			@cacheDom()
			@initParts()

		@listenTo @model, 'data:fetched', => @render()

		@listenTo @model, 'ajax:success', => @clearErrors()
		@listenTo @model, 'ajax:error', (errors) => @showErrors(errors)
		@listenTo @model, 'data:saved', => @updateUserName()
		@listenTo @, 'parts:inited', => @initDirtyForms()

		return @

	initDirtyForms: ->
		model = dirtyForms.addForm({$el: @$el})

		@listenTo @personalDetails.model, 'change', -> model.set('isDirty', true)
		@listenTo @editLinks.collection, 'change', -> model.set('isDirty', true)
		@listenTo @model, 'data:saved', => model.set('isDirty', false)

	render: ->
		@$el.html EditPersonal.Templates.Base()
		@trigger 'rendered'
		@

	cacheDom: ->
		@$el.$loading = @$('.loading_overlay')
		@$el.$userpic = @$('.edit_personal--userpic')
		@$el.$form = @$('.edit_personal--data')
		@$el.$info_pane = @$('.js-edit_personal--info')
		@$el.$social_pane = @$('.js-edit_personal--social')

		return @

	initParts: ->
		if Device.isMobile()
			new Tabs @$el.find('[data-tabs]')

		@initPersonalDetails()
		@initSocialLinks()

		@trigger 'parts:inited'

	initPersonalDetails: ->
		@subViews.push @personalDetails = new PersonalDetails.App
			type: 'profile'
			data: @model.toJSON()
			$userpic: @$el.$userpic
			$form: @$el.$form

		return @

	initSocialLinks: ->
		@subViews.push @editLinks = new EditLinks.App
			el: '.js-edit_links'
			"profileId": 'me'
			"links": @model.get('social_pages')

		@editLinks.view.render()

		return @

	submit: (e) ->
		@save() unless @validate()

		if e? then e.preventDefault()

	validate: ->
		@errors = []
		@validatePersonalDetails()
		@validateSocial()

		return @errors.length

	validatePersonalDetails: ->
		@$el.$info_pane.removeClass('m-error')
		errors = @personalDetails.model.validate()
		if errors?
			@errors.push errors
			@$el.$info_pane.addClass('m-error')

	validateSocial: ->
		@$el.$social_pane.removeClass('m-error')
		@editLinks.collection.forEach (model) =>
			errors = model.validate()
			if errors?
				@errors.push errors
				@$el.$social_pane.addClass('m-error')

	save: ->
		@prepareData()
		$.when(
			@model.save()
		).then(
			=> @afterSave()
		).fail(
			=> @failedToUpdateAlert()
		)

	prepareData: ->
		@model.set
			'first_name': @personalDetails.model.get('first_name')
			'last_name': @personalDetails.model.get('last_name')
			'title': @personalDetails.model.get('title') ? ''
			'gender': @personalDetails.model.get('gender')
			'phone': @personalDetails.model.get('phone')
			'occupation': @personalDetails.model.get('occupation') ? ''
			'birthday': @personalDetails.model.get('birthday') ? ''
			'accepted_terms_conditions': @personalDetails.model.get('accepted_terms_conditions')
			'social_pages': @getLinks()

	getLinks: ->
		links = {}
		@editLinks.collection.forEach (model) =>
			links[model.get('link')] = model.get('value') ? ''

		return links

	# update user name on the page (sidebar, header)
	updateUserName: ->
		fullName = @model.get('first_name') + ' ' + @model.get('last_name')

		#sidebar
		$('.sidebar_card--user_name').text(fullName)

		#user menu
		$('.user_menu--opener_text').text(fullName)
		$('.user_menu--user').eq(0).find('user_menu--user_name dt strong').text(fullName)

	revertChanges: (e) ->
		e?.preventDefault()
		cancelTarget = $(e.currentTarget).data 'cancel'
		switch cancelTarget
			when 'personal'
				@personalDetails.remove?()
				@initPersonalDetails()

			when 'social'
				@editLinks.remove?()
				@initSocialLinks()


	afterSave: ->
		if @model.hasNameUpdated()
			vexDialog.confirm
				message: EditPersonal.Templates.UpdateSlug
					current_url: @model.getCurrentUrl()
					update_url: @model.getUpdateUrl()
				buttons: [
					$.extend {}, vexDialog.buttons.YES, {text: 'NO'}
					$.extend {}, vexDialog.buttons.NO, {text: 'YES'}
				]
				callback: (status) => @updateSlug() unless status
		else
			@updatedAlert()

	failedToUpdateAlert: ->
		vexDialog.alert
			message: EditPersonal.Config.messages.updateError
			buttons: [
				$.extend({}, vexDialog.buttons.YES, text: 'OK')
			]

	updatedAlert: ->
		vexDialog.alert
			message: EditPersonal.Config.messages.updateSuccess
			buttons: [
				$.extend({}, vexDialog.buttons.YES, text: 'OK')
			]

	updateSlug: ->
		@model.updateSlug()
		$.when(
			@model.updateSlug()
		).then(
			=> @updatedAlert()
		).fail(
			=> @failedToUpdateAlert()
		)

module.exports = View

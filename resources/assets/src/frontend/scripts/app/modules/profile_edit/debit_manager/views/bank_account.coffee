Backbone = require 'backbone'
Stickit = require 'backbone.stickit'
vexDialog = require 'vexDialog'

class View extends Backbone.View

	tagName: 'tr'

	events:
		'click [data-card-edit]': 'editBankAccount'
		'click [data-card-remove]': 'removeBankAccount'


	initialize: (options) ->

		@model = options.model
		@collections = options.collections
		@baseView = options.baseView
		@parentView = options.parentView

		@bind()
		@render()
		@stickit()

	bind: ->
		@bindings =
			'[data-account_number]':
				observe: 'account_number'
				updateMethod: 'html'
				onGet: (val) -> "<i>&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;</i>&nbsp;<span>#{val}</span>"

			'[data-routing_number]':
				observe: 'routing_number'

	render: ->
		@$el.html DebitManager.Templates.BankAccount()
		@renderFields()
		@parentView.$el.$listing.append @$el

	renderFields: ->
		fields = DebitManager.Config.Fields['bank_account']
		html = ''
		_.each fields, (header, field) ->
			html += "<td data-#{field}></td>" unless field is 'actions' # we don't want to render actions
		@$el.prepend html

	editBankAccount: ->
		@baseView.openBankAccountPopup @model

	removeBankAccount: ->
		vexDialog.confirm
			message: DebitManager.Config.Messages.removeBankAccount
			callback: (value) =>
				return unless value
				@baseView.showLoading()
				$.when(@model.remove()).then(
					(response) =>
						@baseView.hideLoading()
						if response.data is true
							@destroy()
						else
							@showError response
					, (error) =>
						@baseView.hideLoading()
						@showError error
				)

	destroy: ->
		@$el.fadeOut 200, =>
			@collections.bank_accounts.remove @model
			@remove()
			unless @collections.bank_accounts.length then @baseView.showNotificationAlert()
			@unbind()

	showError: (error) ->
		alert DebitManager.Config.Messages.error

module.exports = View

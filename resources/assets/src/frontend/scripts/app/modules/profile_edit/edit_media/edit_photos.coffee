magnificPopup = require 'magnificPopup'
Handlebars = require 'handlebars'
getApiRoute = require 'hp.api'
vexDialog = require 'vexDialog'
template = require 'text!./templates/photo.html'

EditImages = require '../edit_image'

class EditPhoto
	constructor: (@$block, @collection, options) ->

		require.ensure [], =>
			require 'imports?define=>false!jquery.fileupload'
			require 'imports?define=>false!jquery.fileupload-process'

			@isEdit = false
			@defaultOptions =
				editClass: 'm-edit'

			@currentEl = null

			@options = @$block.data('options')
			_.merge @options, @defaultOptions

			@$block.container = $('.edit_media--item_container', @$block)
			@$block.editButton = $('.edit_media--item_edit', @$block)
			@$block.editImageButton = $('.edit_media--image_edit', @$block)
			@$block.saveButton = $('.edit_media--item_save', @$block)
			@$block.cancelButton = $('.edit_media--item_cancel', @$block)
			@$block.removeButton = $('.edit_media--item_remove', @$block)
			@$block.hiddenButtons = $('.m-hidden_buttons', @$block)
			@$block.title = if $('.edit_media--item_text', @$block).length then $('.edit_media--item_text', @$block) else $('<p class="edit_media--item_text"></p>').appendTo(@$block.container).hide()
			@$block.titleFieldBlock = $('.edit_media--item_text_field', @$block)
			@$block.titleField = $('.edit_media--item_text_input', @$block)
			@$block.imagePreview = $('.edit_media--item_preview', @$block)
			@$block.imagePreview.link = $('.edit_media--item_preview a', @$block)
			@$block.rotateLeft = $('.edit_media--rotate_left', @$block)
			@$block.rotateRight = $('.edit_media--rotate_right', @$block)

			@$block.imagePreview.link.magnificPopup
				type: 'image'

			@setHeight()
			@bind()

	bind: ->
		@$block.editButton.on 'click', =>
			@showEdit()

		@$block.saveButton.on 'click', =>
			@saveData()

		@$block.cancelButton.on 'click', =>
			@hideEdit()

		@$block.removeButton.on 'click', =>
			@removeItem()

		@$block.editImageButton.on 'click', (event) =>
			@initImageEdit(event)

		# click outsite document
		$(document).on 'click', (event) =>
			if !$(event.target).closest(@$block).length && @isEdit
				@saveData()

	setHeight: ->
		@$block.height( @$block.container.outerHeight(true) )

	showEdit: ->
		@$block.addClass @options.editClass
		@$block.title.hide()
		@$block.editButton.hide()
		@$block.saveButton.show()
		@$block.hiddenButtons.show()
		@$block.cancelButton.show()
		@$block.titleFieldBlock.show()
		@$block.titleField
			.val( @$block.title.text() )
			.val( @$block.imagePreview.link.attr('title') )
			.focus()
		@isEdit = true

	hideEdit: ->
		@$block.removeClass @options.editClass
		@$block.titleFieldBlock.hide()
		@$block.saveButton.hide()
		@$block.hiddenButtons.hide()
		@$block.cancelButton.hide()
		# @$block.title.show()
		@$block.editButton.show()
		@setHeight()
		@isEdit = false

	saveData: ->
		title = @$block.titleField.val()
		@$block.imagePreview.link.attr('title', title)
		@$block.title.text(title)
		@hideEdit()

		$.ajax
			url: @options.url
			type: 'put'
			dataType: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				title: title
				_token: @options._token

	removeItem: ->
		vexDialog.confirm
			message: 'Are you sure you would like to delete this item?'
			callback: (value) =>
				if value
					@removeItemRequest()

				else return

	removeItemRequest: ->
		$.ajax
			url: @options.url
			type: 'delete'
			dataType: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				id: @options.id
				_token: @options._token
			success: (response) =>
				@$block.remove()

	initImageEdit: (e) ->
		@$parentItem = $(e.currentTarget).parents('.js-edit_photo')
		@originalImageUrl = @$parentItem.find('.edit_media--item_preview_inner').attr 'href'

		@editor = new EditImages(@originalImageUrl, null, @saveEdited)

	saveEdited: (blob) =>
		editPhotosCollection = @collection
		editPhotosCollection.uploadEdited(blob, @$parentItem)

class EditPhotosCollection
	constructor: (@$block, options) ->
		@models = []
		@itemTemplate = Handlebars.compile template

		if @$block.data('options')?
			@options = @$block.data('options')
		else if options?
			@options = options

		@$block.list = $('.edit_media--list', @$block)
		@$block.fileInput = $('.js-edit_photos--add_photo', @$block)
		@$block.uploaderContainer = $('.edit_media--item_uploader', @$block)

		require.ensure [], =>
			require 'imports?define=>false!jquery.fileupload'
			require 'imports?define=>false!jquery.fileupload-process'

			@init()
			@uploadInit()

			if options?.assets?
				options.assets.forEach (item) =>
					@initViews item, options

	uploadEdited: (blob, el) ->
		@uploadInit()

		@rotationBusy = true
		@currentEl = el
		@currentElTitle = $('.edit_media--item_preview_inner', el).attr 'title'

		@$block.fileInput.fileupload('option', 'formData').file = blob
		@$block.fileInput.fileupload formData: [
			{
				name: '_token'
				value: @options._token
			}
			{
				name: 'type'
				value: @options.type
			}
			{
				name: 'title'
				value: if @currentElTitle? then @currentElTitle else ''
			}
			{
				name: 'description'
				value: ''
			}
		]
		@$block.fileInput.fileupload('add', files: [ blob ])


	uploadInit: ->
		@$block.fileInput.fileupload
			url: @options.url
			type: 'post'

			formData: [
				{
					name: '_token'
					value: @options._token
				}
				{
					name: 'type'
					value: @options.type
				}
				{
					name: 'title'
					value: ''
				}
				{
					name: 'description'
					value: ''
				}
			]

			maxFileSize: 10000000 # are only supported by the UI version
			add : (e, data) => @addPreview(e, data)
			done: (e, data) => @initItem(e, data)

	initViews: (data, options) ->
		itemHtml = @itemTemplate
			# + data.id hack
			url: getApiRoute('ajax-profile-asset', {
				profileId: @options.profileId
				assetType: @options.typeForUri
				assetId: data.id
				})
			_token: options._token
			image:
				id: data.id
				preview: data.attachment['215x140']
				original: data.attachment.original
			title: data.title

		$(itemHtml).insertBefore @$block.uploaderContainer
		@init()

	addPreview: (e, data) ->
		if data.files and data.files[0]
			acceptFileTypes = /^image\/(gif|jpe?g|png)$/i

			vex.defaultOptions.className = 'vex-theme-default'
			vexDialog.buttons.YES.text = 'Ok'

			if data.files[0]['type']? and !acceptFileTypes.test(data.files[0]['type'])
				vexDialog.alert 'The file you are attempting to upload does not contain an acceptable file type. Accepted types are GIF, JPG, JPEG, and PNG.'
				return

			if data.files[0]['size']? and data.files[0]['size'] > 6000000
				vexDialog.alert 'The file you are attempting to upload is too large.'
				return

			reader = new FileReader()
			reader.readAsDataURL data.files[0]

			reader.onload = (e) =>
				itemHtml = @itemTemplate
					image:
						preview: e.target.result
				unless @currentEl
					data.context = $(itemHtml).insertBefore @$block.uploaderContainer
					data.context.after(' ') # fix, add space after new element before upload button
				else
					$(itemHtml).insertBefore(@currentEl)
					@currentEl.hide()
				data.submit()

	initItem: (e, data) ->
		response = data.result.data
		itemHtml = @itemTemplate
			image:
				preview: response.asset['215x140']
				original: response.asset['original']
				id: response.asset['id']
			url: getApiRoute('ajax-profile-asset', {
				profileId: @options.profileId
				assetType: @options.typeForUri
				assetId: response.asset['id']
				})
			title: @currentElTitle
			_token: @options._token

		unless @currentEl
			$item = data.context.replaceWith $(itemHtml)
		else
			@currentEl.prev().replaceWith $(itemHtml)
			_.each @models, (model) =>
				if model.options.id == @currentEl.data('options').id then model.removeItemRequest()

			@currentEl.remove()
			@currentEl = null

		# add all uninited blocks
		@init()

	init: ->
		_.each $('.js-edit_photo'), (item) =>
			$item = $(item)
			if !$item.hasClass('m-inited')
				@models.push new EditPhoto $item, @, {}
				$item.addClass('m-inited')

module.exports = EditPhotosCollection

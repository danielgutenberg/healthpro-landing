Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'
getApiRoute = require 'hp.api'

class View extends Backbone.View

	events:
		'click [data-update-password]': 'updatePassword'
		'keyup [name="confirmNewPassword"]': 'confirmNewPassword'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addValidation
			selector: "name"

		@render()
		@addStickit()
		@stickit()

		@delegateEvents()

	render: ->
		@$el.html UpdateAccount.Templates.Password
			manualPasswordRequired: @baseView.manualPasswordRequired

	confirmNewPassword: ->
		@model.isValid 'confirmNewPassword'

	addStickit: ->
		@bindings =
			'[name="newPassword"]': 'newPassword'
			'[name="confirmNewPassword"]': 'confirmNewPassword'
		unless @baseView.manualPasswordRequired
			@bindings['[name="currentPassword"]'] = 'currentPassword'
		@

	updatePassword: ->
		return if @model.validate()

		@showLoading()
		$.ajax
			url: getApiRoute('user-account')
			method: 'put'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				password: @model.get('newPassword')
				current_password: @model.get('currentPassword')
				confirmed_password: @model.get('confirmNewPassword')
				_token: window.GLOBALS._TOKEN
			success: =>
				@hideLoading()
				@model.clear() # clear the form

				$('.m-warning').remove() if window.GLOBALS.PasswordRequired == '1'
				@displayMsg 'Your password has been successfully changed'

			error: (error) =>
				@hideLoading()
				res = error.responseJSON
				if res.errors.currentPassword
					return @displayMsg res.errors.currentPassword.messages[0]
				@displayMsg 'An error has occured, please try again'

	displayMsg: (msg, cb = null) ->
		vexDialog.alert
			message: msg
			buttons: [$.extend({}, vexDialog.buttons.YES, text: 'OK')]
			callback: -> cb() if cb

	showLoading: -> @baseView.showLoading()
	hideLoading: -> @baseView.hideLoading()

module.exports = View

Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	fetched: false

	defaults:
		experience_tags: []

	url: getApiRoute('ajax-profiles-self')

	initialize: ->
		@fetch()

	fetch: ->
		if @fetched
			@trigger 'ready'
			return
		$.when(@fetchProfile(), @fetchLanguagesTags(), @fetchExperienceTags()).then(
			(res) =>
				@fetched = true
				@trigger 'ready'
		)

	fetchProfile: ->
		@xhr = $.ajax
			url: @url
			method: 'get'
			success: (res) =>
				@set
					'first_name': res.data.first_name
					'last_name': res.data.last_name
					'phone': res.data.phone
					'title': res.data.title
					'gender': res.data.gender
					'business_name': res.data.business_name
					'about_self_background': res.data.about_self_background ? ''
					'about_self_specialities': res.data.about_self_specialities ? ''
					'about_services_benefit': res.data.about_services_benefit ? ''
					'about_what_i_do': res.data.about_what_i_do ? ''
					'social_pages': res.data.social_pages ? []
					'educations': res.data.educations
					'professional_body': res.data.professional_body
					'jobs': res.data.jobs
					'assets': res.data.assets
					'languages': @parseArray(res.data.tags.languages.tags)
					'concerns_conditions': @parseArray(res.data.tags.concerns_conditions.tags)
					'age_group': @parseArray(res.data.tags.age_group.tags)
					'gender_identities': @parseArray(res.data.tags.gender_identities.tags)
					'special_needs': @parseArray(res.data.tags.special_needs.tags)
					'experience': do (res) ->
						if res.data.tags?.experience?.tags?.length
							+res.data.tags.experience.tags[0]['id'].toString()
						else
							null

	fetchLanguagesTags: ->
		$.ajax
			'url': getApiRoute('ajax-taxonomy-tags', {taxonomySlug: 'language'})
			method: 'get'
			success: (response) =>
				languages = []
				_.each response.data, (item) =>
					languages.push
						value: item.id
						text: item.name

				@set('language_options', languages)

	fetchExperienceTags: ->
		$.ajax
			'url': getApiRoute('ajax-taxonomy-tags', {taxonomySlug: 'experience'})
			method: 'get'
			success: (response) =>
				experienceList = _.sortBy response.data, 'id'
				experienceTags = []
				experienceList.forEach (item) =>
					experienceTags.push
						value: item.id
						text: item.name

				@set('experience_tags', experienceTags)

#	parseArray: (array) ->
#		result = []
#		array.forEach (item) =>
#			result.push(item.id)
#		result

	parseArray: (array) ->
		array.map (item) -> item.id

module.exports = Model

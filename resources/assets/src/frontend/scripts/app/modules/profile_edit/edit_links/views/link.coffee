Backbone = require 'backbone'
Validation = require 'backbone.validation'
Stickit = require 'backbone.stickit'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	linksOptions:
		link:
			label: 'Your Website'
			placeholder: 'http://www.your_website.com'
			prefix: 'http://'
		twitter:
			label: 'Twitter'
			placeholder: 'http://www.twitter.com/username'
			prefix: 'http://'
		facebook:
			label: 'Facebook'
			placeholder: 'http://www.facebook.com/username'
			prefix: 'http://'
		googleplus:
			label: 'Google+'
			placeholder: 'http://plus.google.com/google_profile_id'
			prefix: 'http://'
		linkedin:
			label: 'LinkedIn'
			placeholder: 'http://www.linkedin.com/in/username'
			prefix: 'http://'
		pinterest:
			label: 'Pinterest'
			placeholder: 'http://www.pinterest.com/username'
			prefix: 'http://'
		instagram:
			label: 'Instagram'
			placeholder: 'http://www.instagram.com/username'
			prefix: 'http://'
		youtube:
			label: 'Youtube'
			placeholder: 'http://www.youtube.com/username'
			prefix: 'http://'
		ideafit:
			label: 'IDEAFit'
			placeholder: 'http://www.ideafit.com/profile/username'
			prefix: 'http://'
		amazon:
			label: 'Amazon'
			placeholder: 'http://www.amazon.com/shops/username'
			prefix: 'http://'

	tagName: 'li'
	className: 'edit_links--list--item'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins
		@model = options.model

		@addValidation
			valid: (view, attr, selector) =>
				field = @$el.find('input')
				field.parent().removeClass('m-error')
				field.parent().find('.field--error').html('')
			invalid: (view, attr, error, selector) =>
				field = @$el.find('input')
				field.parent().addClass('m-error')
				field.parent().find('.field--error').html(error)

		@addStickit()

	addStickit: ->
		@bindings =
			input:
				observe: 'value'
				initialize: ($el) =>
					$el.on 'blur', => @prefixInputValue $el

	getLabel: ->
		@linksOptions[@model.get('link')].label

	getPlaceholder: ->
		@linksOptions[@model.get('link')].placeholder

	getPrefix: ->
		@linksOptions[@model.get('link')].prefix

	render: ->
		@$el.html EditLinks.Templates.Item
			label: @getLabel()
			link: @model.get('link')
			value: @model.get('value')
			placeholder: @getPlaceholder()

		@stickit()

		return @

	prefixInputValue: ($input) ->
		# make sure we remove all the empty characters
		value = $input.val().replace /\s+/, ''
		$input.val value

		# don't prefix empty value or if it's already prefixed
		return if value == '' || value.match /^https?:\/\//

		$input.val @getPrefix() + value
		$input.trigger 'change'

module.exports = View

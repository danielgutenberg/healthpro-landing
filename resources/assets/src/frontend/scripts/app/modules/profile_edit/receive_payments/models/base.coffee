Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		'allow_payments': ''

	initialize: ->
		super
		@getData()

	getData: ->
		$.when( @getPaymentsAllowance(), @getPaymentMethods() ).then(
			=> @trigger 'data:ready'
		).fail(
			=> @trigger 'data:ready'
		)

	getPaymentsAllowance: ->
		$.ajax
			url: getApiRoute('ajax-provider-full-get', {providerId: 'me'})
			method: 'get'
			success: (res) =>
				@set 'allow_payments', res?.data?.profile?.accepted_payment_methods
				@set 'has_gift_certificates', res?.data?.hasGiftCertificates
				@initialAllowPayments = @get 'allow_payments'
			error: (err) =>
				console.log err

	getPaymentMethods: ->
		$.ajax
			url: getApiRoute('ajax-payment-methods')
			method: 'get'
			success: (res) =>
				if res?.data?
					@setMethods(res.data)
			error: (err) =>
				console.log err

	save: ->
		$.ajax
			url: getApiRoute('ajax-provider-accept-methods-update', {'providerId':'me'})
			method: 'put'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				method: @get('allow_payments')
				_token: window.GLOBALS._TOKEN
			success: (res) =>
				@initialAllowPayments = @get 'allow_payments'
				@trigger 'data:ready'
			error: (err) =>
				console.log err
				@trigger 'data:ready'

	saveToSession: ->
		$.ajax
			url: getApiRoute('ajax-payment-options')
			method: 'post'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				accepted_payment_methods: @get('allow_payments')
				_token: window.GLOBALS._TOKEN
			success: (res) =>
				# @trigger 'data:ready'
			error: (err) =>
				console.log err
				@trigger 'data:ready'

	setMethods: (data) ->
		if data?.paypal?
			@set 'paypal.email', data.paypal.email
			@set 'paypal.id', data.paypal.id

		if data?.stripe?
			@set 'stripe', data.stripe

	disconnectMethod: (method) ->
		switch method
			when 'paypal'
				url = getApiRoute('ajax-paypal-delete', {paypalId: @get('paypal.id')})
			when 'stripe'
				url = getApiRoute('ajax-stripe-delete')

		$.ajax
			url: url
			method: 'delete'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: (res) =>
				if res?.data?
					switch method
						when 'paypal'
							@unset 'paypal.email'
							@unset 'paypal.id'
						when 'stripe'
							@unset 'stripe'

					unless @get('paypal.email')? or @get('stripe')?
						@set 'allow_payments', 'in_person'
						@save()

	reset: ->
		@set 'allow_payments', @initialAllowPayments

module.exports = Model

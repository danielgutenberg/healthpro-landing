Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
require 'backbone.stickit'

class View extends Backbone.View

	attributes:
		'class': 'popup learn_more'
		'data-popup': 'learn_more'
		'id' : 'learn_more'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

	render: ->
		@$el.html ReceivePayments.Templates.LearnMore
		@

module.exports = View

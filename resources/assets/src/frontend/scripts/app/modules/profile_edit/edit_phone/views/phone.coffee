Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'backbone.stickit'

class View extends Backbone.View

	className: 'edit_phones--list--item'
	tagName: 'li'

	events:
		'click .edit_phones--list--item--remove': 'close'

	initialize: (options) ->
		super
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins


		@setOptions(options)
		@addStickit()

	addStickit: ->
		@bindings =
			'[name*="is_notified"]': 'id'
			'[name*="number"]': 'number'

		return @

	render: ->
		@$el.html EditPhones.Templates.Item
			index: @index
			prefix: @model.get('prefix')
			verified: @model.get('verified')
			id: @model.get('id')

		@stickit()

		@delegateEvents()

		return @

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'click [data-add]': 'addJobModel'

	initialize: (options) ->
		super
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common # add Mixins

		@instances = []
		@index = 0

		@setOptions(options)

		@bind()

	bind: ->
		@listenTo @collection, 'add', @addJobView
		@listenTo @collection, 'remove', @ifEmpty
		@listenTo @collection, 'data:ready', @hideLoading
		@listenTo @collection, 'data:loading', @showLoading
		@listenTo @, 'rendered', =>
			@cacheDom()
			@init()

	init: ->
		if @collection.length
			@collection.forEach (model) =>
				@addJobView(model)
		else
			@addJobModel()

		@hideLoading()

	ifEmpty: ->
		@addJobModel() unless @collection.length

	addJobModel: ->
		if @collection.length
			@collection.add({}) unless @validate()
		else
			@collection.add({})

	validate: ->
		@errors = []

		@collection.forEach (model) =>
			errors = model.validate()
			if errors?
				@errors.push errors

		return @errors.length

	addJobView: (model) ->
		@instances.push job = new EditJobs.Views.Job
			model: model
			index: @index

		@$el.$list.append job.render().el

		@index++

		return @

	cacheDom: ->
		@$el.$list = @$el.find('.edit_set--list')
		@$el.$loading = @$el.find('.loading_overlay')

		return @

	showLoading: ->
		@$el.$loading.removeClass('m-hide')

		return @

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

		return @

	render: ->
		@$el.html EditJobs.Templates.Base()
		@trigger 'rendered'

		return @

	removeEmpty: ->
		@subViews.forEach (view) =>
			if view.model.isEmpty()
				view.close()

module.exports = View

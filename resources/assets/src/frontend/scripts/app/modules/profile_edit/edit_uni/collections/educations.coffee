Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Model = require '../models/education'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		Cocktail.mixin( @, Mixins.Common ) # add Mixins

		@setOptions(options)
		@fetch(@educationsData)

	fetch: (data) ->
		if data?
			data.forEach (model) =>
				@add
					'id': model.id
					'education_id': model.education_id ? null
					'degree': model.degree ? model.degree
					'education_name': model.education_name
					'from.month': if model.from?.month? then model.from.month else null
					'from.year': if model.from?.year? then model.from.year else null
					'to.month': if model.to then model.to.month else null
					'to.year': if model.to then model.to.year else null

	getPreparedData: ->
		returns = []

		@forEach (model) =>
			education_name = model.get('education_id')?.toString() ? model.get('education_name')?.toString()
			unless model.isEmpty() || !education_name
				educationId = model.get('education_id')
				educationName = model.get('education_name')

				uniObject = {}
				uniObject.education_name = educationName
				unless Number(educationId) == -1
					uniObject.education_id = educationId

				uniObject.degree = model.get('degree')
				if !!model.get('from.month') and !!model.get('from.year')
					uniObject.from =
						"year": model.get('from.year')
						"month": model.get('from.month')

				# if [to] dates isn't empty, add them to job
				if !!model.get('to.month') and !!model.get('to.year')
					uniObject.to =
						"month": model.get('to.month')
						"year": model.get('to.year')

				returns.push uniObject

		return returns


module.exports = Collection

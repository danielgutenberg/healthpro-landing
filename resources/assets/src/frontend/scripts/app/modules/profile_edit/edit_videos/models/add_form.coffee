Backbone = require 'backbone'

class Model extends Backbone.Model

	validation:
		url:
			pattern: /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/
			msg: 'That doesn\'t look like a youtube url.'

	defaults:
		url: ''
		title: ''

module.exports = Model

Phone = require 'framework/phone'
Abstract = require './abstract/update'
vexDialog = require 'vexDialog'

class View extends Abstract

	type: 'phone'

	initialize: ->
		super

		@events['click [data-phone-number-edit]'] 	= 'editPhoneNumber'
		@events['click [data-code-verify]'] 		= 'verifyCode'
		@events['click [data-code-resend]'] 		= 'resendCode'

		@delegateEvents()

		@toggleBlocks()

		@$el.addClass 'edit_phone'

	cacheDom: ->
		super
		@$el.$blocks = @$el.find('[data-block]')
		@$el.$msg    = @$el.find('[data-msg]')
		@

	addListeners: ->
		@model.on 'change:verified change:verification_method', =>
			@toggleBlocks()

	addStickit: ->
		super
		@bindings['[name="number"]'] =
			observe: 'number'
			onSet: (val) => if val then @phone.formattedVal(true) else ''
			onGet: (val) -> if val then $.formatMobilePhoneNumber(val).replace('+1 ', '') else ''
			afterUpdate: ($el) -> $el.change()
			initialize: ($el) ->
				@phone = new Phone $el

		@bindings['[data-phone-number-label]'] =
			observe: 'number'
			onGet: (val) => @model.formattedPhone()

		@bindings['[name="verification_code"]'] = 'verification_code'

		@

	confirmUpdate: ->
		@popupView.setStep('verification')

	verificationMethod: (method) ->
		@model.set 'verification_method', method
		@update()

	afterUpdate: ->
		super
		@toggleBlocks()

	editPhoneNumber: ->
		@model.set 'needs_verification', false
		@displayBlock 'edit', 'button'

	verifyCode: ->
		return if @model.validate()
		@showLoading()
		$.when(
			@model.verifyCode()
		).then(
			=>
				@hideLoading()
				@displayMsg 'success'
				@
		).fail(
			(response) =>
				@hideLoading()
				@handleVerificationErrors response.responseJSON, 'Could not verify the code. Please reload the page and try again'
		)

	resendCode: ->
		@showLoading()
		$.when(
			@model.resendCode()
		).then(
			=>
				@hideLoading()
				@displayBlock 'verify', 'label'
				@
		).fail(
			(response) =>
				@hideLoading()
				@handleVerificationErrors response.responseJSON, 'Could not resend the code. Please reload the page and try again'
		)

	displayBlock: ->
		@$el.$blocks.addClass 'm-hide'
		_.each _.flatten(arguments), (block) =>
			@$el.$blocks.filter("[data-block='#{block}']").removeClass 'm-hide'
		@

	toggleBlocks: ->
		if @model.get('verified')
			@displayBlock 'edit', 'button'
			return

		@displayBlock 'verify', 'label'
		@

	handleVerificationErrors: (responseJSON, defaultError = '') ->
		error = null
		if responseJSON.errors?.error?.messages?.length
			if responseJSON.errors.error.messages[0].match /Invalid Code/i
				error = "You've entered an invalid code"
			else if responseJSON.errors.error.messages[0].match /Invalid phone/i
				error = "You've entered an invalid phone number"

		@displayMsg 'error', error ? defaultError
		@

	displayMsg: (msgType = 'success', msg = null) ->
		if msg is null
			msg = UpdateAccount.Config.Messages.Phone[msgType]

		vexDialog.alert
			buttons: [
				$.extend({}, vexDialog.buttons.YES, text: 'OK')
			]
			message: msg

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	className: 'edit_media--list'
	tagName: 'ul'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common )

		@setOptions options
		@startup()
		@addListeners()

	startup: ->
		if @collection.length
			@collection.forEach (model) =>
				@renderItem(model)

	addListeners: ->
		@listenTo @collection, 'add', (model) =>
			@renderItem(model)

		@listenTo @collection, 'loading', @loading
		@listenTo @collection, 'done:loading', @hideLoading

	renderItem: (model) ->
		unless model.get('isNew')
			@subViews.push video = new EditVideos.Views.Video
				model: model
			@$el.append video.render().el
			return @

		@listenToOnce @collection, 'video:added', =>
			@subViews.push video = new EditVideos.Views.Video
				model: model
			@$el.append video.render().el

		model.upload()

		return @

	loading: ->
		$('.loading_overlay').removeClass('m-hide')

	hideLoading: ->
		$('.loading_overlay').addClass('m-hide')




module.exports = View

Backbone = require 'backbone'
Handlebars = require 'handlebars'

# list of all instances
window.EditCredentials = EditCredentials =
	Config:
		messages:
			updateSuccess: 'Your changes have been saved'
			updateError: 'Something went wrong. Please reload the page and try again'
	Views:
		Base: require './views/base'
	Models:
		Base: require './models/base'
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

# events bus
_.extend EditCredentials, Backbone.Events

class EditCredentials.App
	constructor: (options) ->
		@model = new EditCredentials.Models.Base
			presetData: options.presetData

		@view = new EditCredentials.Views.Base
			model: @model
			el: options.el
			dirtyForms: options.dirtyForms ? true

		return {
			model: @model
			view: @view
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()


if $('.js-edit_credentials').length
	new EditCredentials.App
		el: '.js-edit_credentials'

module.exports = EditCredentials

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

Mixins = require 'utils/backbone_mixins'
getApiRoute = require 'hp.api'
dirtyForms = require 'framework/dirty_forms'

EditEducations = require 'app/modules/profile_edit/edit_uni'
EditWork = require 'app/modules/profile_edit/edit_work'
EditProfbody = require 'app/modules/profile_edit/edit_profbody'
EditCertificates = require 'app/modules/profile_edit/edit_media/edit_certificates'

class View extends Backbone.View

	events:
		'submit .form': 'submit'
		'click .js-edit_credentials--cancel': 'revertChanges'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addListeners()

		@model.fetch()

	addListeners: ->
		@listenTo @model, 'data:ready', => @render()
		@listenTo @model, 'data:loading', @onModelLoading
		@listenTo @model, 'data:ready data:saved', @onModelReady
		@listenToOnce @, 'rendered', @initParts

	initParts: ->
		@initEducations()
		@initWork()
		@initCertificates()
		@initProfbody()

		@initDirtyForms()

	initDirtyForms: ->
		return unless @dirtyForms

		model = dirtyForms.addForm({$el: @$el})

		@listenTo @educations.collection, 'change', -> model.set 'isDirty', true
		@listenTo @work.collection, 'change', -> model.set 'isDirty', true
		@listenTo @editProfbody.collection, 'change', -> model.set 'isDirty', true
		@listenTo @model, 'ajax:success', -> model.set 'isDirty', false

	initEducations: ->

		@subViews.push @educations = new EditEducations.App
			el: @$el.find('.js-edit_educations')
			educations: @model.get('educations')
		@educations.view.render()
		@

	initWork: ->
		@subViews.push @work = new EditWork.App
			el: @$el.find('.js-edit_work')
			jobs: @model.get('jobs')
		@work.view.render()
		@

	initCertificates: ->
		@subViews.push @editCertificates = new EditCertificates @$el.find('.js-edit_photos'), {
			type: "certificate"
			typeForUri: "certificates"
			url: getApiRoute('ajax-profile-assets', {
				profileId: window.GLOBALS._PID
				assetsType: 'certificates'
			})
			_token: window.GLOBALS._TOKEN
			profileId: window.GLOBALS._PID
			assets: @model.get('certificates')
		}

	initProfbody: ->
		@editProfbody = new EditProfbody.App
			el: @$el.find('.js-edit_profbody')
			data: @model.get('professional_body')

		@editProfbody.instance = 'editProfbody'

		@subViews.push @editProfbody

		@editProfbody.view.render()

	render: ->
		@$el.html EditCredentials.Templates.Base()
		@trigger 'rendered'
		@

	submit: (e) ->
		if e? then e.preventDefault()

		unless @validate()
			@setDataToModel()
			$.when(
				@model.save()
			).then(
				=>
					vexDialog.buttons.YES.text = 'Ok'
					vexDialog.alert
						message: EditCredentials.Config.messages.updateSuccess
			).fail(
				=>
					vexDialog.buttons.YES.text = 'Ok'
					vexDialog.alert
						message: EditCredentials.Config.messages.updateError
			)

	validate: ->
		@errors = []
		@validateEducations()
		@validateWork()
		@validateProfbody()

		@errors.length

	validateEducations: ->
		# remove empty models before
		@educations.view.removeEmpty()

		if @educations.collection.length and !@educations.collection.at(0).isEmpty()
			@educations.collection.forEach (model) =>
				errors = model.validate()
				if errors?
					@errors.push errors
		@

	validateWork: ->
		# remove empty models before
		@work.view.removeEmpty()

		if @work.collection.length and !@work.collection.at(0).isEmpty()
			@work.collection.forEach (model) =>
				errors = model.validate()
				if errors?
					@errors.push errors
		@

	validateProfbody: ->
		@editProfbody.view.removeEmpty()
		@errors.push ['error'] if @editProfbody.view.validate()
		@

	setDataToModel: ->
		@model.set
			'educations': @educations.collection.getPreparedData()
			'professional_body': @getProfBody()
			'jobs': @work.collection.getPreparedData()

	getProfBody: -> @editProfbody.collection.getPreparedData()

	onModelLoading: -> window.Dashboard.trigger 'content:loading'  if window.Dashboard
	onModelReady: -> window.Dashboard.trigger 'content:ready' if window.Dashboard

	revertChanges: (e) ->
		e?.preventDefault()
		cancelTarget = $(e.currentTarget).data 'cancel'
		switch cancelTarget
			when 'education'
				@educations.remove?()
				@initEducations()

			when 'work'
				@work.remove?()
				@initWork()

			when 'associations'
				@editProfbody.remove?()
				@initProfbody()

module.exports = View

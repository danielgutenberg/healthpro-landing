Backbone = require 'backbone'

class View extends Backbone.View

	events:
		'click button': 'openPopup'

	initialize: (options) ->
		super
		@collections = options.collections
		@baseView = options.baseView
		@render()
		@

	render: ->
		@$el.html CreditCardsManager.Templates.Header

	openPopup: ->
		@baseView.openPopup new CreditCardsManager.Models.Card()

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'backbone.stickit'
require 'backbone.validation'

Maxlength = require 'framework/maxlength'
Select = require 'framework/select'
Phone = require 'framework/phone'

class View extends Backbone.View

	bindings: {}

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addValidation()
		@render()
		@addBindings()
		@stickit()

	addBindings: ->

		_.each @model.config.fields, (field) =>
			binding =
				observe: field.name

			switch field.type
				when 'select'
					binding.initialize = ($el) ->
						new Select $el
						@
					binding.selectOptions = @model.getSelectOptions field.name
				when 'textarea'
					if field.maxlength
						binding.onGet = @onGetTextarea
				when 'phone'
					binding.onSet = -> if val then @phone.formattedVal(true) else ''
					binding.onGet = (val) -> if val then $.formatMobilePhoneNumber(val).replace('+1 ', '') else ''
					binding.afterUpdate = ($el) -> $el.change()
					binding.initialize = ($el) ->
						@phone = new Phone $el
						@

			@bindings[field.binding] = binding
		@

	onGetTextarea: (val, options) ->
		$textarea = @$el.find(options.selector)
		if $textarea.length
			@initTextarea $textarea.parent(), val
		val

	initTextarea: ($el, val) ->
		return if $el.attr('data-maxlength') is 'inited'
		$el.find('textarea').val(val)
		new Maxlength $el
		return

	save: -> @model.save() unless @model.validate()

	render: ->
		@$el.html @template
			fields: @model.config.fields
		@


module.exports = View

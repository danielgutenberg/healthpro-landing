Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: ->
		super
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-bank-accounts')
			method: 'get'
			success: (res) =>
				_.forEach res.data, (bank_account) =>
					@add @format(bank_account)
				@trigger 'data:ready'
			error: (err) =>
				@trigger 'data:ready'

	format: (bank_account) ->
		bank_account =
			id: bank_account.id
			account_number: bank_account.account_number
			routing_number: bank_account.routing_number
			currency: bank_account.currency
			account_id: parseInt bank_account.account_id, 10

		bank_account

module.exports = Collection

Handlebars = require 'handlebars'

# list of all instances
require '../../../../styles/modules/welcome_returning.styl'

window.WelcomeReturning = WelcomeReturning =
	Config:
		Messages:
			acceptTerms: 'Please agree to "Terms of Service" before registering'
	Views:
		Base: require('./views/base')

class WelcomeReturning.App
	constructor: ->
		@view = new WelcomeReturning.Views.Base()

new WelcomeReturning.App()

module.exports = WelcomeReturning

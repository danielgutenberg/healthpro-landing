Backbone = require 'backbone'
getRoute = require 'hp.url'

class View extends Backbone.View

	el: $('#welcome_returning')

	events:
		'change [data-accept-terms]': 'toggleAcceptBtnHref'
		'click [data-proceed-btn]': 'toggleAcceptBtnPrevent'

	initialize: ->
		window.popupsManager.openPopup 'welcome_returning'
		@cacheDom()
		@bind()

	cacheDom: ->
		@$acceptTerms = $('[data-accept-terms]', @$el)
		@$proceedBtn = $('[data-proceed-btn]', @$el)

	bind: ->

	toggleAcceptBtnHref: (e) ->
		if $(e.currentTarget).is(':checked')
			@$proceedBtn.attr 'href', getRoute('activate-old-professional')
			@unsetErrors()
		else
			@$proceedBtn.attr 'href', '#'

	toggleAcceptBtnPrevent: (e) ->
		unless @$acceptTerms.is(':checked')
			e.preventDefault()
			@raiseError('agree_terms', WelcomeReturning.Config.Messages.acceptTerms)

	raiseError: (field, error) ->
		$field = $('input[name='+field+']', @$el)
		$field
			.parent().addClass('m-error').end()
			.parents('.form--group').addClass('m-error')

		if error?
			$errorContainer = $field.parent().find('.field--error')
			unless $errorContainer.length
				$errorContainer = $('<span class="field--error" />').appendTo( $field.parent() )

			$errorContainer.html( error )

	unsetErrors: ->
		$fieldError = $('.field--error', @$el)
		$fieldError
			.html('')
		$fieldError
			.parent().removeClass('m-error')

module.exports = View

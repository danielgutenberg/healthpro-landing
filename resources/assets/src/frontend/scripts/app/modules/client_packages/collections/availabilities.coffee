Backbone = require 'backbone'
Moment = require 'moment'

datetimeParsers = require 'utils/datetime_parsers'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: (models, options) ->

		@packageModel = options.packageModel
		@scheduleModel = options.scheduleModel


	fetch: (fromDate = null, untilDate = null) ->
		if fromDate is null
			fromDate = @packageModel.get('start_time').min(Moment())

		if untilDate is null
			untilDate = fromDate.clone().add 1, 'month'

		@xhr.abort() if @xhr # abort previous call
		@trigger 'data:loading'

		@xhr = $.ajax
			url: getApiRoute 'ajax-provider-availabilities',
				providerId: @packageModel.get('provider.id')
			method: 'get'
			data:
				from: "#{fromDate.format('YYYY-MM-DD')}T00:00"
				until: "#{untilDate.format('YYYY-MM-DD')}T23:59"
				length: @packageModel.get('duration')
				recurring: true
			success: (res) =>
				@trigger 'data:ready'
				result = res.data?.results[0] ? null
				@setData result if result
				@trigger 'availabilities:ready'

			error: => @trigger 'data:ready'

	# set data from collection's ajax call
	setData: (result) ->
		selectedService = @packageModel.get('service').service_id
		selectedSession = @packageModel.get('session_id')

		_.each result.availabilities, (availabilities, date) =>
			_.each availabilities, (availability) =>
				# do not add times that don't have current service
				return unless service = availability.services[selectedService]
				return unless service.indexOf(selectedSession) > -1

				fromMoment = datetimeParsers.parseToMoment availability.from

				formatted =
					availability_id: availability.id
					from: fromMoment
					until: datetimeParsers.parseToMoment availability.until
					date: date
					weekday: fromMoment.weekday()
					label: fromMoment.format('hh:mm a')

				@push formatted

	getForDate: (momentDate, ignoreTimes = []) ->
		if _.isString momentDate
			date = momentDate
		else
			date = momentDate.format('YYYY-MM-DD')

		duration = @packageModel.get('duration')

		@filter (model) ->
			return false unless model.get('date') is date
			return true unless ignoreTimes.length

			timeFrom = model.get('from')
			timeUntil = model.get('until')

			overlaps = ignoreTimes.filter (ignoreFrom) ->
				# support a string date
				unless Moment.isMoment ignoreFrom
					ignoreFrom = datetimeParsers.parseToMoment ignoreFrom

				ignoreUntil = ignoreFrom.clone().add duration, 'minutes'

				# check for overlaps
				timeFrom.unix() < ignoreUntil.unix() and timeUntil.unix() > ignoreFrom.unix()

			overlaps.length is 0

	getForWeek: (startOfWeek) ->
		endOfWeek = startOfWeek.clone().endOf('week')

		@filter (model) ->
			from = model.get('from')
			from.isAfter(startOfWeek) and from.isBefore(endOfWeek)


	getWeekdays: (includeSelectedWeekdays = true) ->
		startingDay = @packageModel.get('start_time').min(Moment())
		selectedWeekdays = @packageModel.get('schedule').map (model) -> model.get('date').weekday()

		days = []
		for weekday in [0..6]
			date = startingDay.clone().add(weekday, 'day')
			if includeSelectedWeekdays or (selectedWeekdays.indexOf(date.weekday()) is -1)
				if @getForDate(date).length
					days.push
						moment: date
						weekday: date.format('dddd')
						date: date.format('YYYY-MM-DD')
						date_label: date.format('MMMM, D')
		days

module.exports = Collection

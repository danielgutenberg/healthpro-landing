Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
parseHash = require 'utils/parse_hash'

require 'backbone.stickit'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@hash = parseHash false
		@instances = []

		@bind()
		@addListeners()
		@render()
		@cacheDom()
		@appendPackages()

	addListeners: ->
		@listenTo @collections.packages, 'data:loading', @showLoading
		@listenTo @collections.packages, 'data:ready', @hideLoading
		@

	bind: ->
		@bindings =
			'[data-packages-filter]':
				observe: 'filter'
				selectOptions:
					collection: @collections.packages.getFilterOptions()
		@

	render: ->
		@$el.html ClientPackages.Templates.Base()
		@$container.html @$el
		@stickit()
		@

	appendPackages: ->
		@instances.push @packagesView = new ClientPackages.Views.Packages
			collections: @collections
			baseView: @

		@$el.$packages.append @packagesView.el
		@

	cacheDom: ->
		@$body = $('body')
		@$el.$packages = @$el.find('[data-packages]')
		@$el.$filter = @$el.find('[data-packages-filter]')
		@$el.$loading = @$el.find('.loading_overlay')
		@


	removePopup: ->
		if @popupView
			@popupView.remove()
			@popupView = null

		if window.popupsManager.getPopup('client_packages')
			window.popupsManager.removePopup('client_packages')

		@

	appendPopup: (options) ->
		@instances.push @popupView = new ClientPackages.Views.Popup options
		@$body.append @popupView.$el

		window.popupsManager.addPopup(@popupView.$el)

		popup = window.popupsManager.getPopup('client_packages')
		popup.setCloseOnEsc false
		popup.openPopup()

		@popupView.$el.on 'popup:closed', => @removePopup()


module.exports = View

Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	save: (fromTime, untilTime) ->
		packageChargeId = @get('price_package_charge_id')

		if @get('id')
			weekday = @get('date').weekday()
			scheduleId = @get('id')
			url = getApiRoute('ajax-package-schedule', {packageId: packageChargeId, scheduleId: scheduleId})
			method = 'put'
		else
			weekday = fromTime.weekday()
			url = getApiRoute('ajax-package-schedule-add', {packageId: packageChargeId})
			method = 'post'

		$.ajax
			url: url
			method: method
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
				schedule:
					day: weekday
					from: fromTime.format('HH:mm')
					until: untilTime.format('HH:mm')
					firstDate: fromTime.format('Y-MM-DD')

module.exports = Model

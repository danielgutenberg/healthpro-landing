Backbone = require 'backbone'
Moment = require 'moment'
getApiRoute = 'hp.api'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		@setData options.data

	setData: (items) ->
		_.each items, (item) => @push @formatScheduleRow(item)
		@

	formatScheduleRow: (item) ->
		moment = Moment()
		time = item.time.split(':')
		moment.set
			weekday: item.day
			h: time[0]
			m: time[1]

		{
			id: item.id
			date: moment # we're not using actual dates here - we only need time and weekday
			price_package_charge_id: item.price_package_charge_id
		}

	# for some reason we get a lot of schedule rows.
	# this is a quick way to filter them on the front-end
	# @TODO should be changed when backend is fixed
	getSchedule: ->
		models = _.uniq @models, (model) -> model.get('date').format('d HH:mm')
		_.sortBy models, (model) -> model.get('date').weekday()


	loadAvailabilities: (fromDate = null) ->
		fromDate = Moment() if fromDate is null
		untilDate = fromDate.clone().endOf('week')

		@trigger 'data:loading'

		$.ajax
			url: getApiRoute 'ajax-provider-availabilities',
				providerId: Wizard.model.get('data.provider_id')
			method: 'get'
			data:
				from: "#{fromDate.format('YYYY-MM-DD')}T00:00"
				until: "#{untilDate.format('YYYY-MM-DD')}T00:00"
				length: Wizard.model.get('data.selected_time.duration')
				location_id: Wizard.model.get('data.selected_time.location_id')
			success: (res) =>
				@trigger 'data:ready'
				@fetchedDate = fromDate
				result = res.data?.results[0] ? null
				@setData result if result
				@trigger 'availabilities:ready'

			error: => @trigger 'data:ready'




module.exports = Collection

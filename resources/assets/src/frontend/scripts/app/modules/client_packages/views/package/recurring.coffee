Abstract = require('./abstract')

Numeral = require 'numeral'
Formats = require 'config/formats'
HumanTime = require 'utils/human_time'
LocationFormatter = require 'utils/formatters/location'
vexDialog = require 'vexDialog'

class View extends Abstract

	className: 'client_package m-recurring'

	initialize: ->
		super

		@events['click [data-schedule-appointments-more]'] = 'toggleMoreScheduledAppointments'
		@events['click [data-schedule-appointment-add-time]'] = 'addAppointmentTime'
		@events['click [data-reschedule-appointment]'] = 'rescheduleAppointmentConfirm'
		@events['click [data-reschedule-time]'] = 'rescheduleTime'
		@events['click [data-schedule-toggle]'] = 'toggleScheduleView'

		@delegateEvents()

	addListeners: ->
		super
		@listenTo @model, 'change:auto_renew', @toggleAutoRenewal

	cacheDom: ->
		super
		@$el.$appointment = @$el.find('[data-schedule-appointment]')
		@$el.$schedule = @$el.find('[data-schedule]')
		@$el.$schedule.$toggle = @$el.find('[data-schedule-toggle]')
		@$el.$schedule.$add = @$el.find('[data-schedule-appointment-add-time]')

		@

	afterRender: ->
		super
		@displayScheduledAppointments(3)
		@displayScheduleView 'times'
		@toggleAddScheduleTime()
		@

	bind: ->
		super
		@bindings['[name="auto_renew"]'] =
			observe: 'auto_renew'
			onGet: (val) -> val * 1
			onSet: (val) -> val * 1
			initialize: ($el, model) ->
				$el.prop 'checked', model.get('auto_renew')
				return
		@

	toggleAutoRenewal: ->
		@baseView.showLoading()
		$.when(
			@model.toggleAutoRenewal()
		).then(
			=> @baseView.hideLoading()
		).fail(
			=> @baseView.hideLoading()
		)

	getTemplate: -> ClientPackages.Templates.Package.Recurring

	getTemplateData: ->
		data = super

		data.package =
			price: @model.get('price')
			price_label: Numeral(@model.get('price')).format(Formats.Price.Simple) + " / month"
			max_visits: @model.get('number_of_visits')
			max_visits_label: do =>
				numberOfVisits = @model.get('number_of_visits')
				return 'unlimited visits per week' unless numberOfVisits
				"#{numberOfVisits} visit" + (if numberOfVisits > 1 then "s" else "") + " per week"
			duration: @model.get('duration')
			duration_label: HumanTime.minutes(@model.get('duration')) + " session"
			number_of_months: @model.get('number_of_months')
			number_of_months_label: do =>
				numberOfMonths = @model.get('number_of_months')
				"#{numberOfMonths} month" + (if numberOfMonths > 1 then "s" else "")
			expires_on: do =>
				if !@model.get('active')
					@model.get('expires_on').format('MMMM D, YYYY')
				else
					@model.get('appointments').getLastAppointment()?.get('from').format('MMMM D, YYYY')
			cancelled: !@model.get('active')

		data.appointments = @model.get('appointments').map (model) ->
			{
				id: model.get('id')
				location_name: LocationFormatter.getLocationName model.get('location')
				location_type_label: LocationFormatter.getLocationTypeLabel model.get('location')
				location_type:  model.get('location').location_type
				weekday: model.get('date').format('dddd')
				date: model.get('date').format('MMMM D, YYYY')
				from: model.get('from').format('hh:mm a')
			}

		data.times = @model.get('schedule').getSchedule().map (model) ->
			{
				id: model.get('id')
				weekday: model.get('date').format('dddd')
				from: model.get('date').format('hh:mm a')
			}

		data

	toggleMoreScheduledAppointments: (e) ->
		e?.preventDefault()

		$btn = $(e.currentTarget)
		if $btn.data('more')
			@displayScheduledAppointments 3
			$btn.data('more', 0).text $btn.data('label-more')
		else
			@displayScheduledAppointments -1
			$btn.data('more', 1).text $btn.data('label-less')
		@

	displayScheduledAppointments: (amount = -1) ->
		if amount < 0
			@$el.$appointment.removeClass 'm-hide'
			return @

		@$el.$appointment.addClass 'm-hide'
		@$el.$appointment.slice(0, amount).removeClass 'm-hide'
		@

	toggleScheduleView: (e) ->
		e?.preventDefault()
		$btn = $(e.currentTarget)
		@displayScheduleView $btn.data('view') ? 'times'

	displayScheduleView: (view = 'times') ->
		return @ if @$el.$schedule.hasClass "m-#{view}"
		switch view
			when 'times'
				@$el.$schedule.removeClass('m-appointments').addClass('m-times')
				@$el.$schedule.$toggle.data('view', 'appointments').text @$el.$schedule.$toggle.data('label-times')
			when 'appointments'
				@$el.$schedule.removeClass('m-times').addClass('m-appointments')
				@$el.$schedule.$toggle.data('view', 'times').text @$el.$schedule.$toggle.data('label-appointments')
				@displayScheduledAppointments 3
		@

	toggleAddScheduleTime: ->
		if @model.canAddScheduleTime()
			@$el.$schedule.$add.removeClass 'm-hide'
		else
			@$el.$schedule.$add.addClass 'm-hide'
		@

	rescheduleAppointmentConfirm: (e) ->
		e?.preventDefault()

		appointmentId = $(e.currentTarget).data('reschedule-appointment')

		vexDialog.confirm
			buttons: [
				$.extend {}, vexDialog.buttons.YES, {text: 'Yes'}
				$.extend {}, vexDialog.buttons.NO, {text: 'Cancel'}
			]
			message: '<span class="m-error">WARNING</span> Are you sure you want to reschedule this appointment?'
			callback: (status) => @rescheduleAppointment(appointmentId) if  status
		@

	rescheduleAppointment: (appointmentId) ->
		console.log 'reschedule appointment'

	rescheduleTime: (e) ->
		e?.preventDefault()
		$btn = $(e.currentTarget)

		scheduleId = $btn.data('schedule_id')

		scheduleModel = @model.get('schedule').get(scheduleId)
		weekday = scheduleModel.get('date').format('dddd')
		time = scheduleModel.get('date').format('hh:mm a')

		@openSchedulePopup scheduleModel,
			popupTitle: 'Update Schedule Day/Time'
			popupDescription: "Current schedule time is <strong>#{weekday}, #{time}</strong>."
		@

	addAppointmentTime: (e) ->
		e?.preventDefault()
		return unless @model.canAddScheduleTime()

		scheduleModel = new ClientPackages.Models.ScheduleRow
			price_package_charge_id: @model.get('id')

		@openSchedulePopup scheduleModel,
			popupTitle: 'Add Appointment Day/Time'
			popupDescription: "Schedule your appointments"
		@


	openSchedulePopup: (scheduleModel, options = {}) ->
		_.extend {
			popupTitle: ''
			popupDescription: ''
			contentClass: 'm-schedule'
		}, options

		options.contentView = new ClientPackages.Views.ScheduleUpdate
			parentView: @
			baseView: @baseView
			collections: @collections

			scheduleModel: scheduleModel
			packageModel: @model

		@baseView.appendPopup options

		options.contentView.loadAvailabilities()
		@

	reload: ->
		@showLoading()
		$.when(
			@model.load()
		).then(
			=>
				@hideLoading()
				@reRender()
		).fail(
			=> @hideLoading()
		)
		@

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
scrollTo = require 'utils/scroll_to_element'

class View extends Backbone.View

	className: 'client_packages--list_wrap'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@instances = []

		@addListeners()
		@render()
		@cacheDom()

	addListeners: ->
		@listenTo @collections.packages, 'data:ready', @renderPackages
		@listenTo @collections.packages, 'reset', @resetPackages
		@

	render: ->
		@$el.html ClientPackages.Templates.Packages()
		@

	renderPackages: ->
		@toggleEmpty @collections.packages.length is 0
		@collections.packages.forEach (model) => @renderPackage model
		@baseView.model.trigger 'change:filter'
		@maybeActivatePackage()
		@

	renderPackage: (model) ->
		PackageView = do =>
			if model.get('type') is 'recurring'
				return ClientPackages.Views.Package.Recurring
			else
				ClientPackages.Views.Package.Standard

		@instances.push view = new PackageView
			baseView: @baseView
			parentView: @
			model: model
			collections: @collections
		@$el.$list.append view.el
		@

	resetPackages: ->
		_.each @instances, (instance) -> instance?.remove()
		@$el.$list.html ''
		@

	cacheDom: ->
		@$el.$list = @$el.find('[data-packages-list]')
		@$el.$empty = @$el.find('[data-packages-empty]')
		@

	maybeActivatePackage: ->
		return unless @baseView.hash?.package?
		packageView = _.findWhere @instances, (instance) -> instance.model.get('package_id') is +@baseView.hash.package
		return unless packageView

		# activate the right filter if the package is hidden
		if packageView.isHidden()
			filterType = packageView.model.get('filter_type')
			@baseView.$el.$filter.val(filterType).trigger 'change'

		packageView.toggleContent()
		scrollTo packageView.$el

		@

	toggleEmpty: (display = true) ->
		if display
			@$el.$empty.removeClass 'm-hide'
		else
			@$el.$empty.addClass 'm-hide'

module.exports = View

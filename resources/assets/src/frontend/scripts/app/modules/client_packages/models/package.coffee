DeepModel = require 'backbone.deepmodel'
Moment = require 'moment'
getApiRoute = require 'hp.api'
datetimeParsers = require 'utils/datetime_parsers'

class Model extends DeepModel

	defaults: -> {}

	initialize: ->
		@afterInit()
		@

	afterInit: ->
		@initAppointments()
		@initSchedule()
		@initServiceSession()
		@initFilterType()


	initAppointments: ->
		return if @get('appointments') instanceof ClientPackages.Collections.Appointments
		@set 'appointments', new ClientPackages.Collections.Appointments null,
			model: ClientPackages.Models.Appointment
			parentModel: @
			data: @get('appointments')
		@

	initSchedule: ->
		return if @get('schedule') instanceof ClientPackages.Collections.Schedule
		@set 'schedule', new ClientPackages.Collections.Schedule null,
			model: ClientPackages.Models.ScheduleRow
			parentModel: @
			data: @get('schedule')
		@

	toggleAutoRenewal: ->
		return if @get('type') unless 'recurring'

		$.ajax
			url: getApiRoute('ajax-package-status', {packageId: @get('id')})
			method: 'put'
			type  : 'json'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
				auto_renew: @get('auto_renew') * 1
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				@set 'auto_renew', response.data.auto_renew

	initServiceSession: ->
		currentPackageId = @get('package_id')

		_.each @get('service').sessions, (session) =>
			return unless session.packages.length
			_.each session.packages, (pkg) =>
				return unless pkg.package_id is currentPackageId

				@set
					session_id: session.session_id
					duration: session.duration

				# recurring package
				if pkg.number_of_visits > -1
					@set
						number_of_visits: pkg.number_of_visits
				else
					@set
						number_of_months: pkg.number_of_months

					rule = _.findWhere pkg.rules, rule: 'max_weekly'
					return unless rule

					@set
						rule_id: rule.id
						number_of_visits: rule.value
				@
			@
		@

	initFilterType: ->
		if @get('type') is 'standard'
			@set 'filter_type', if @get('entities_left') > 0 then 'active' else 'finished'
			return
		@set 'filter_type', if @get('expires_on').isAfter(Moment()) then 'active' else 'finished'
		@

	isActive: -> @get('filter_type') is 'active'

	getUpcomingAppointment: -> @get('appointments').getUpcomingAppointment()

	canAddScheduleTime: ->
		return false if @get('type') is 'standard'
		@get('schedule').length < @get('number_of_visits')

	load: ->
		$.ajax
			url: getApiRoute('ajax-package', {packageId: @get('id')})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				item = response.data

				# DeepModel :/
				@set 'schedule', []
				@set 'appointments', []

				@set
					provider: item.provider
					client: item.client
					appointments: item.appointments
					entities_left: item.entities_left
					expires_on: datetimeParsers.parseToMoment(item.expires_on)
					number_of_entities: item.number_of_entities
					package_id: item.package_id
					price: item.price
					schedule: item.schedule
					service: item.service
					auto_renew: item.auto_renew ? 0
					active: item.active ? 1

				@afterInit()

module.exports = Model

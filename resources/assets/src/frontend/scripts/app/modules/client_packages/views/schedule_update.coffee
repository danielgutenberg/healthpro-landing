Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Moment = require 'moment'

class View extends Backbone.View

	className: 'client_package_schedule'

	events:
		'click [data-cancel]': 'cancel'
		'click [data-save]': 'save'
		'click [data-weekday-select]': 'activateWeekday'
		'click [data-availability-select]': 'selectAvailability'


	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@selectedAvailability = null

		@collection = new ClientPackages.Collections.Availabilities [],
			packageModel: @packageModel
			scheduleModel: @scheduleModel

		@addListeners()

	addListeners: ->
		@listenToOnce @collection, 'data:loading', => @popupView.showLoading()
		@listenToOnce @collection, 'data:ready', => @popupView.hideLoading()
		@listenToOnce @collection, 'availabilities:ready', @afterAvailabilitiesReady
		@

	render: ->
		@$el.html ClientPackages.Templates.ScheduleUpdate @getTemplateData()
		@

	cacheDom: ->
		@$el.$availabilities = @$el.find('[data-availabilities]')
		@$el.$availabilitiesEmpty = @$el.find('[data-availabilities-empty]')
		@$el.$save = @$el.find('[data-save]')
		@

	loadAvailabilities: ->
		@collection.fetch()
		@

	afterAvailabilitiesReady: ->
		@render()
		@cacheDom()
		@disableWeekdaysWithoutAvailabilities()
		@selectAvailableWeekday()
		$(document).trigger 'popup:center'
		@

	getTemplateData: ->
		data =
			weekdays: @collection.getWeekdays(ClientPackages.Config.DisplaySelectedWeekdays)

		data

	disableWeekdaysWithoutAvailabilities: ->
		_.each @collection.getWeekdays(ClientPackages.Config.DisplaySelectedWeekdays), (weekday) =>
			$weekdayEl = @$el.find("[data-date='#{weekday.date}']")

			# deactivate weekdays without availabilities
			if @collection.getForDate(weekday.date).length > 0
				$weekdayEl.removeClass 'm-inactive'
			else
				$weekdayEl.addClass 'm-inactive'
		@

	selectAvailableWeekday: ->
		@$el.find("[data-date]").not('.m-inactive').first().find('[data-weekday-select]').trigger 'click'
		@

	activateWeekday: (e) ->
		$weekday = $(e.currentTarget).parent()
		return if $weekday.hasClass('m-inactive') or $weekday.hasClass('m-active')

		@setSelectedAvailability null

		$weekday.addClass('m-active').siblings('.m-active').removeClass('m-active')
		availabilities =  @collection.getForDate $weekday.data('date')

		@renderAvailabilities availabilities

		$(document).trigger 'popup:center'
		@

	setSelectedAvailability: (availability = null) ->
		@selectedAvailability = availability
		@toggleSave()
		@

	toggleSave: ->
		@$el.$save.prop 'disabled', if @selectedAvailability then false else true
		@

	toggleAvailabilitiesEmpty: (availabilities) ->
		if availabilities.length
			@$el.$availabilities.removeClass 'm-hide'
			@$el.$availabilitiesEmpty.addClass 'm-hide'
		else
			@$el.$availabilities.addClass 'm-hide'
			@$el.$availabilitiesEmpty.removeClass 'm-hide'
		@

	renderAvailabilities: (availabilities) ->
		@resetAvailabilities()
		@toggleAvailabilitiesEmpty availabilities

		availabilities.forEach (availability) => @renderAvailability availability
		@

	renderAvailability: (availability) ->
		@$el.$availabilities.append """
			<li data-cid='#{availability.cid}'>
				<button data-availability-select>#{availability.get('label')}</button>
			</li>
		"""
		@

	resetAvailabilities: ->
		@$el.$availabilities.html ''
		@

	selectAvailability: (e) ->
		$availability = $(e.currentTarget).parent()
		$availability.addClass('m-active').siblings('.m-active').removeClass('m-active')

		availability = @collection.get $availability.data('cid')

		@setSelectedAvailability
			availability_id: availability.get('availability_id')
			from: availability.get('from')
			until: availability.get('until')
		@

	cancel: ->
		@popupView.$el.find('[data-popup-close]').trigger 'click'
		@

	save: ->
		return unless @selectedAvailability

		@popupView.showLoading()

		$.when(
			@scheduleModel.save @selectedAvailability.from, @selectedAvailability.until
		).then(
			(res) =>
				rejectedAppointments = @findRejected(res)
				if rejectedAppointments.length
					@alertRejectedAppointments(rejectedAppointments)
				@popupView.hideLoading()
				@parentView.reload()
				@baseView.removePopup()
		).fail(
			=> @popupView.hideLoading()
		)

	alertRejectedAppointments: (datesLayout) ->
		vexDialog.alert
			buttons: [
				$.extend {}, vexDialog.buttons.YES, {text: 'Ok'}
			]
			message: '<span class="m-error">WARNING</span> Selections are limited to whatever weeks you haven\'t scheduled appointments. The appointments for the following dates weren\'t booked:' + '<ul>' + datesLayout.join('') + '</ul>'
			contentClassName: 'm-w550'

	findRejected: (res) ->
		purchasables = res?.data?.line_items?.purchasables
		if purchasables?
			rejected = []
			_.each purchasables, (appointment) ->
				if appointment.orderItemEntity.status is null
					date = Moment(appointment.sourceEntity.from_utc)
					time = Moment(appointment.sourceEntity.from.date)
					formattedDate = date.format(ClientPackages.Config.DateFormat)
					formattedTime = time.format(ClientPackages.Config.TimeFormat)

					rejected.push "<li>#{formattedDate} at #{formattedTime}</li>"
		rejected

module.exports = View

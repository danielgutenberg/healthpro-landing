Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		id: null

	read: ->
		$.ajax
			url: getApiRoute('ajax-notification-inbox-read', {
				profileId: 'me'
				notificationId: @get('id')
			})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				read: 1
				_token: window.GLOBALS._TOKEN

	request: (request_url) ->
		$.ajax
			url: request_url
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

module.exports = Model

Backbone = require 'backbone'
vexDialog = require 'vexDialog'

class View extends Backbone.View

	className: 'notification'
	tagName: 'li'

	events:
		'click [data-read]': 'read'
		'click [data-action]': 'action'

	initialize: (options) ->
		super

		@eventBus = options.eventBus
		@model = options.model
		@collections = options.collections
		@baseView = options.baseView

		@bind()
		@render()
		@cacheDom()

	bind: ->
		@listenTo @model, 'remove', @destroy

	render: ->
		@$el.html Notifications.Templates.Notification @model.toJSON()
		@baseView.$el.$listing.append @el

	destroy: -> @$el.fadeOut 200, => @remove()

	read: (e) ->
		e?.preventDefault()
		@showLoading()
		$.when(@model.read()).then(
			(response) =>
				@collections.notifications.remove @model
			(error) =>
				@hideLoading()
				alert 'Something went wrong. Please reload the page and try again.'
		)

	request: (request_url, e) ->
		e?.preventDefault()
		@showLoading()
		$.when(@model.request(request_url)).then(
			(response) =>
				@read()
			(error) =>
				@hideLoading()
				alert 'Something went wrong. Please reload the page and try again.'
		)


	action: (e) =>
		e?.preventDefault()

		$btn = $(e.currentTarget)

		action = $btn.data('action')
		url = $btn.data('action-url')
		redirect_url = $btn.data('action-redirect')

		is_put_request = $btn.data('action-put')
		confirm = $btn.data('action-confirm')

		if confirm
			confirmMessage = $btn.data('action-message')
			confirmMessage = 'Are you sure?' unless confirmMessage

			vexDialog.confirm
				message: confirmMessage
				callback: (value) =>
					return unless value
					@actionCallback url

		else if is_put_request
			@actionRequestCallback url, redirect_url
		else
			@actionCallback url


	actionCallback: (url) ->
		# read the notification
		$.when(@read()).then(
			->
				# right now we simply redirect to the URL
				window.location = url
		)

	actionRequestCallback: (request_url, redirect_url) ->
		# make request and read the notification. redirect if needed
		$.when(@request(request_url)).then(
			->
				if redirect_url then window.location = redirect_url
		)


	hideLoading: -> @$el.$loading.addClass('m-hide')
	showLoading: -> @$el.$loading.removeClass('m-hide')

	cacheDom: ->
		@$el.$loading = @$el.find('> .loading_overlay')

module.exports = View

Backbone = require 'backbone'
Handlebars = require 'handlebars'

require '../../../../styles/modules/notifications.styl'

window.Notifications =
	Config:
		Messages:
			readAll: 'Are you sure you want to delete all notifications?'
			error: 'Something went wrong. Please reload the page and try again.'
	Views:
		Base: require('./views/base')
		Notification: require('./views/notification')
	Collections:
		Notifications: require('./collections/notifications')
	Models:
		Notification: require('./models/notification')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')
		Notification: Handlebars.compile require('text!./templates/notification.html')

class Notifications.App
	constructor: ->
		@eventBus = _.extend {}, Backbone.Events

		@collections =
			notifications: new Notifications.Collections.Notifications [],
				model: Notifications.Models.Notification
				eventBus: @eventBus

		@views =
			base: new Notifications.Views.Base
				eventBus: @eventBus
				collections: @collections

Notifications.app = new Notifications.App()

module.exports = Notifications

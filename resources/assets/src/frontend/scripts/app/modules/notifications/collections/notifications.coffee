Backbone = require 'backbone'

Moment = require 'moment'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: ->
		super
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-notifications-inbox', {profileId: 'me'})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.each response.data, (item) => @push @formatNotification(item)
				@trigger 'data:ready'
			error: (error) => console.log 'error', error

	formatNotification: (item) ->
		id: item.id
		title: item.title
		created: item.created_at
		created_human_diff: Moment.utc(item.created_at).fromNow()
		description: item.description

	readAll: ->
		$.ajax
			url: getApiRoute('ajax-notifications-inbox-read', {profileId: 'me'})
			method: 'put'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				read: 1
				_token: window.GLOBALS._TOKEN
			success: => @reset()
			error: (error) => console.log 'error', error

module.exports = Collection

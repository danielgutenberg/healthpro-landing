Backbone = require 'backbone'
vexDialog = require 'vexDialog'

require 'backbone.stickit'

class View extends Backbone.View

	el: $('[data-notifications]')

	subViews: []

	events:
		'click [data-read-all]': 'readAll'
		'click [data-menu-open]': 'openMenu'

	initialize: (options) ->
		super

		@eventBus = options.eventBus
		@collections = options.collections

		# handle the count
		@model = new Backbone.Model
			count: 0

		@bind()
		@render()
		@cacheDom()

		vexDialog.buttons.YES.text = 'Yes'
		vexDialog.buttons.NO.text = 'No'

	cacheDom: ->
		@$el.$listing = @$el.find('[data-notifications]')
		@$el.$menu = @$el.find('[data-menu]')
		@$el.$menuToggler = @$el.find('[data-menu-toggler]')
		@$el.$loading = @$el.find('.loading_overlay')

	render: ->
		@$el.html Notifications.Templates.Base
		@stickit()
		@trigger 'rendered'

	renderNotification: (model) ->
		@subViews.push new Notifications.Views.Notification
			baseView: @
			model: model
			collections: @collections
			eventBus: @eventBus

	hide: -> @$el.addClass('m-hide')
	show: -> @$el.removeClass('m-hide')

	hideLoading: -> @$el.$loading.addClass('m-hide')
	showLoading: -> @$el.$loading.removeClass('m-hide')

	openMenu: ->
		@$el.$menu.addClass('m-show')
		setTimeout(
			=>
				$(window).on 'click.notificationsMenu', (e) =>
					return unless @$el.$menu.hasClass('m-show')
					$target = $(e.target)
					@closeMenu() if !($target.closest(@$el.$menu).length or $target.closest(@$el.$menuToggler).length)
			, 1
		)

	closeMenu: ->
		@$el.$menu.removeClass('m-show')
		$(window).off 'click.notificationsMenu'

	bind: ->
		@listenTo @eventBus, 'loading:show', @showLoading
		@listenTo @eventBus, 'loading:hide', @hideLoading
		@listenTo @collections.notifications, 'data:ready', @hideLoading
		@listenTo @collections.notifications, 'add', @renderNotification
		@listenTo @collections.notifications, 'update reset', @afterNotificationsUpdate

		@bindings =
			'[data-count]': 'count'

	afterNotificationsUpdate: ->
		@model.set 'count', @collections.notifications.length

		if @model.get('count') then @show() else @hide()

	readAll: (e) ->
		e.preventDefault() if e

		vex.defaultOptions.className = 'vex-theme-default'
		vexDialog.confirm
			message: Notifications.Config.Messages.readAll
			callback: (value) =>
				return unless value
				@showLoading()
				$.when(@collections.notifications.readAll()).then(
					(response) =>
						@hideLoading()
					(error) =>
						@hideLoading()
						alert Notifications.Config.Messages.error
				)

module.exports = View

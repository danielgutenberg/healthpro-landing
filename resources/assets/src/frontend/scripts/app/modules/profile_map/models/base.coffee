Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	initialize: ->
		@fetch()

	fetch: ->
		@trigger 'loading'

		$.when(
			@fetchProfile(),
			@fetchServices()
		).then(
			=> @trigger 'ready'
			,
			=> @trigger 'ready'
		)

	fetchProfile: ->
		$.ajax
			url: getApiRoute('ajax-provider', { providerId: @get('provider_id') } )
			method: 'get'
			data: 'fields=locations,ratings'
			success: (response) =>
				@set 'profile', response.data

	fetchServices: ->
		@xhr = $.ajax
			url: getApiRoute('ajax-provider-services', {
				providerId: @get('provider_id')
				})
			method: 'get'
			success: (response) =>
				@set 'services', @formatServices(response.data)

	formatServices: (services) ->
		formatted = []
		_.each services, (service) =>
			formatted.push
				service_id: service.service_id
				name: service.name
				sessions: service.sessions
				locations: service.locations

		return formatted

	hasMapLocations: -> @getMapLocations().length > 0
	getMapLocations: -> _.filter @get('profile').locations, (location) -> location.location_type isnt 'virtual'

module.exports = Model

module.exports =
	icon:
		url: '/assets/frontend/images/content/search/marker.png'
		size: new GMaps.gmaps.Size('19', '26')
		scaledSize: new GMaps.gmaps.Size(19, 26)
		origin: new GMaps.gmaps.Point(0,0)
		anchor: new GMaps.gmaps.Point(9, 37)

	icon_blue:
		url: '/assets/frontend/images/content/search/marker_blue.png'
		size: new GMaps.gmaps.Size('19', '26')
		scaledSize: new GMaps.gmaps.Size(19, 26)
		origin: new GMaps.gmaps.Point(0,0)
		anchor: new GMaps.gmaps.Point(9, 37)

	icon_orange:
		url: '/assets/frontend/images/content/search/marker_orange.png'
		size: new GMaps.gmaps.Size('19', '26')
		scaledSize: new GMaps.gmaps.Size(19, 26)
		origin: new GMaps.gmaps.Point(0,0)
		anchor: new GMaps.gmaps.Point(9, 37)

	icon_green:
		url: '/assets/frontend/images/content/search/marker_green.png'
		size: new GMaps.gmaps.Size('19', '26')
		scaledSize: new GMaps.gmaps.Size(19, 26)
		origin: new GMaps.gmaps.Point(0,0)
		anchor: new GMaps.gmaps.Point(9, 37)

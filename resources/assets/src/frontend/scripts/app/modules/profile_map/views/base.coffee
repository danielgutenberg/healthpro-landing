Backbone = require 'backbone'
InfoBubble = require 'framework/infobubble'

class View extends Backbone.View

	initialize: ->
		@cacheDom()
		@addListeners()

	addListeners: ->
		@listenTo @model, 'loading', @showLoading
		@listenTo @model, 'ready', =>
			@showMap()
			@initParts()
			@showLocations()
			@hideLoading()

			GMaps.gmaps.event.addListener @map, 'click', => @infoWindow?.close()

			@listenTo ProfileMap, 'open_location', @openLocation

	closeLocation: -> GMaps.gmaps.event.trigger @map, 'click'

	openLocation: (id) ->
		@closeLocation()
		marker = _.find @markersCollection.instances,
			location_id : id

		if marker instanceof GMaps.gmaps.Marker
			GMaps.gmaps.event.trigger marker, 'click'
		else if marker instanceof GMaps.gmaps.Circle
			GMaps.gmaps.event.trigger marker, 'click',
				vertex: 0
				edge: 0
				path: 0
		@

	setCurrentLocations: (locations) -> @model.set 'current_locations', locations

	showLocations: ->
		profile = @model.get('profile')
		services = @model.get('services')
		@model.getMapLocations().forEach (location) => @markersCollection.add(location, profile, services)

		if @markersCollection.instances.length
			@showMap()
			@centerMap()
		else
			@hideMap()

	cacheDom: ->
		@$el.$map = @$el.find('.map_widget--inner')
		@$el.$loading = @$el.find('.map_widget--loading')

	initParts: ->
		@initMap 40.7142700, -74.0059700
		@initInfoWindow()
		@initMarkersCollection()

	initMap: (lat, lng) ->
		@map = new GMaps.gmaps.Map @$el.$map[0],
			zoom: 15
			center: new GMaps.gmaps.LatLng(lat, lng)
		@

	initInfoWindow: ->
		@infoWindow = new InfoBubble
			map: @map
			hideCloseButton: true
			disableAnimation: true
			arrowPosition: 50
			shadowStyle: 0
			borderWidth: 0
			padding: 0
			borderRadius: 2
			minHeight: 153
			maxHeight: 500
			minWidth: 220
			maxWidth: 220
			backgroundClassName: 'search_map--bubble'
		@

	initMarkersCollection: ->
		@markersCollection = new ProfileMap.Views.Markers
			gmap: @map
			infoWindow: @infoWindow
		@

	centerMap: ->
		bounds = new GMaps.gmaps.LatLngBounds()
		@markersCollection.instances.forEach (marker) =>
			if marker instanceof GMaps.gmaps.Marker
				bounds.extend marker.getPosition()
			else if marker instanceof GMaps.gmaps.Circle
				bounds.union marker.getBounds()

		GMaps.gmaps.event.addListenerOnce @map, 'bounds_changed', -> @setZoom(15) if @getZoom() > 15

		@map.setCenter bounds.getCenter()
		@map.fitBounds bounds

	showLoading: -> @$el.$loading.removeClass('m-hide')
	hideLoading: -> @$el.$loading.addClass('m-hide')

	showMap: -> @$el.removeClass 'm-hide'
	hideMap: -> @$el.addClass 'm-hide'


module.exports = View

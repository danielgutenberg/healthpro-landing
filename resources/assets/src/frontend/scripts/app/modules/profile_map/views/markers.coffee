Moment = require 'moment'
LocationFormatter = require 'utils/formatters/location'
WizardAuth = require 'helpers/wizard_auth'

class View

	constructor: (options) ->
		@instances = []
		@map = options.gmap
		@infoWindow = options.infoWindow

	add: (location, profile, services) ->
		filtereServices = services.filter (service) -> _.indexOf(_.pluck(service.locations, 'id'), location.id) > -1
		return unless filtereServices.length

		lat = location.coordinates?.lat ? location.address.latitude
		lng = location.coordinates?.lng ? location.address.longitude
		return false unless lat and lng

		profile.ratings.overall = profile.ratings.overall.toString().replace('.', '_')

		date = Moment().format('YYYY-MM-DD')

		hasActiveSessions = false
		hasPackages = false
		_.each services, (service) ->
			_.each service.sessions, (session) ->
				hasActiveSessions = true if session.active
				if session.packages.length
					_.each session.packages, (pkg) ->
						hasPackages = true if pkg.number_of_visits < 0
						return
				return
			return


		content = ProfileMap.Templates.InfoWindow
			profile: profile
			location: location
			locationAddress: LocationFormatter.getLocationAddressLabel location, true
			locationName: LocationFormatter.getLocationName location
			can_book_appointment: hasActiveSessions
			can_book_recurring: hasPackages
			booking_data: JSON.stringify
				resetToFirstStep: 'recalculate'
				bookedFrom: 'profile'
				provider_id: profile.id
				from: date
				date: date
				services: services
				locations: profile.locations

		myLatlng = new GMaps.gmaps.LatLng(lat, lng)
		if location.location_type is 'home-visit'
			marker = @addCircle location, myLatlng, content
		else
			marker = @addMarker location, myLatlng, content

		GMaps.gmaps.event.addListener marker, 'click', => @openWindow marker, content
		GMaps.gmaps.event.addListener @infoWindow, 'domready', => @addBookingButton()

		@instances.push marker

	addMarker: (location, latLng) ->
		marker = new GMaps.gmaps.Marker
			position: latLng
			map: @map
			icon: ProfileMap.Config.icon
			optimized: false
			location_id: location.id
			type: 'marker'

		marker

	addCircle: (location, latLng) ->
		circle = new GMaps.gmaps.Circle
			strokeColor: '#3db7cb'
			strokeOpacity: 0.8
			strokeWeight: 2
			fillColor: '#3db7cb'
			fillOpacity: 0.35
			center: latLng
			map: @map
			location_id: location.id
			type: 'circle'
			radius: location.service_area * 1000 # radius in meters

		circle

	openWindow: (marker, content) ->
		# display the full circle on the map
		if marker instanceof GMaps.gmaps.Circle
			@map.setCenter marker.getCenter()
			@map.fitBounds marker.getBounds()
			GMaps.gmaps.event.addListenerOnce @map, 'bounds_changed', -> @setZoom(15) if @getZoom() > 15
		# zoom to the marker
		else
			@map.setZoom 15

		@infoWindow.setContent content
		@adjustInfowindowHeight @infoWindow
		@infoWindow.open @map, marker

	adjustInfowindowHeight: (infoWindow) ->
		# Dirty way to fix infowindow height calculation
		$('.search_map--dummy_bubble').remove()
		$('.search_map').append('<div class="search_map--bubble search_map--dummy_bubble" style="width:' + infoWindow.minWidth + '">' + infoWindow.getContent() + '</div>')
		calculatedHeight = $('.search_map--dummy_bubble').height()
		infoWindow.setMinHeight(calculatedHeight + 2)

	addBookingButton: ->
		$bookingButton = $('.search_map--bubble--booking', $(@infoWindow.content_))
		$bookingButton
			.off() # prevent attach event to all locations. dumb way
			.on 'click', (e) =>
				e.preventDefault()
				$btn = $(e.currentTarget)

				if $btn.data('type') is 'recurring'
					bookingData = $btn.data('booking-data')
					openWizard = ->
						window.bookingHelper.openWizardRecurring
							bookingData: bookingData
							forcePage: true

					WizardAuth openWizard,
						msg: 'Please log in to book a monthly package'

				else
					bookingData = $(e.target).data('booking-data')
					openWizard = ->
						window.bookingHelper.openWizard
							bookingData: bookingData
					WizardAuth openWizard
		@

	reset: ->
		@instances.forEach (marker) ->
			marker.setMap(null)
			marker = null
		@instances = []
		GMaps.gmaps.event.clearListeners(@map, 'bounds_changed')

module.exports = View


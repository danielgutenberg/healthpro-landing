Backbone = require 'backbone'
Handlebars = require 'handlebars'

# list of all instances
window.ProfileMap =
	Views:
		Base: require('./views/base')
		Markers: require('./views/markers')
	Models:
		Base: require('./models/base')
	Templates:
		InfoWindow: Handlebars.compile require('text!./templates/infowindow.html')

# events bus
_.extend ProfileMap, Backbone.Events

class ProfileMap.App
	constructor: (options) ->
		# load google maps
		GMaps.load().ready =>
			window.ProfileMap.Config = require './config'
			# load gmaps and then init the module
			@model = new ProfileMap.Models.Base
				provider_id: options.provider_id

			@view = new ProfileMap.Views.Base
				el: options.el
				model: @model
		@

	openLocation: (locationId) -> @view?.openLocation locationId

	setCurrentLocations: (locations) -> @view?.setCurrentLocations locations



if $('.profile .map_widget').length and window.GLOBALS.PROVIDER_ID
	window.profileMap = new ProfileMap.App
		provider_id: window.GLOBALS.PROVIDER_ID
		el: $('.profile .map_widget')

module.exports = ProfileMap

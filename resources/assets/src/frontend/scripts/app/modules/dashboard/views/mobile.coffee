Backbone = require 'backbone'

class View extends Backbone.View

	el: 'body'

	initialize:(options = {}) ->
		@options =
			def: 'default'

		_.extend @options, options


	initTabs: ->


module.exports = View

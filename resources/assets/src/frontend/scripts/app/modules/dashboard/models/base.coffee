DeepModel = require 'backbone.deepmodel'
getApiRoute = require 'hp.api'

class Model extends DeepModel

	defaults: ->
		profile: null

	initialize: ->
		@getProfile()

	getProfile: ->
		return @xhr if @xhr
		@xhr = $.when(
			$.ajax
				url: getApiRoute('ajax-profiles-self')
				method: 'get'
		).then(
			(response) =>
				@set 'profile', response.data
				Dashboard.trigger 'profile:loaded', @
				window.GLOBALS.profile = response.data
		)
		@xhr

module.exports = Model

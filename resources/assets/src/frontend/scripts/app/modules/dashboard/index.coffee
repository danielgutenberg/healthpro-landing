Backbone = require 'backbone'

# list of all instances
window.Dashboard = Dashboard =
	Views:
		Base: require('./views/base')
	Models:
		Base: require('./models/base')

# events bus
_.extend Dashboard, Backbone.Events

class Dashboard.App
	constructor: ->
		@model = new Dashboard.Models.Base()
		@view = new Dashboard.Views.Base
			model: @model

Dashboard.app = new Dashboard.App() if $('[data-dashboard]').length

module.exports = Dashboard

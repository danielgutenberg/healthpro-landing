Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	el: '[data-dashboard]'

	initialize:(options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@cacheDom()
		@addListeners()

	addListeners: ->
		# listen to loading
		@listenTo Dashboard, 'content:loading', => @showLoading('content')
		@listenTo Dashboard, 'content:ready', => @hideLoading('content')

	cacheDom: ->
		@$el.$content = @$el.find('.layout--content')
		@$el.$content.$loading = @$el.find('.layout--content_loading')

	showLoading: (part) -> @$el['$' + part].$loading.removeClass('m-hide')
	hideLoading: (part) -> @$el['$' + part].$loading.addClass('m-hide')

module.exports = View

Backbone = require 'backbone'
Handlebars = require 'handlebars'
Device = require 'utils/device'

window.ProfessionalSetupLocations =
	Views:
		Base: require('./views/base')
		AddLocation: require('./views/add_location')
		EditLocation: require('./views/edit_location')
		Locations: require('./views/locations')
		Location: require('./views/location')
		LocationPeriods: require('./views/location_periods')
		LocationPeriod: require('./views/location_period')
	Models:
		Location: require('./models/location')
		LocationPeriod: require('./models/location_period')
	Collections:
		Locations: require('./collections/locations')
		LocationsTypes: require('./collections/locations_types')
		LocationPeriods: require('./collections/location_periods')

	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')
		AddLocation: Handlebars.compile require('text!./templates/add_location.html')
		EditLocation: Handlebars.compile require('text!./templates/edit_location.html')
		Locations: Handlebars.compile do ->
			if Device.isMobile()
				require('text!./templates/mobile/locations.html')
			else
				require('text!./templates/desktop/locations.html')

		Location: Handlebars.compile require('text!./templates/location.html')
		LocationPeriods: Handlebars.compile require('text!./templates/location_periods.html')
		LocationPeriod: Handlebars.compile require('text!./templates/location_period.html')

_.extend ProfessionalSetupLocations, Backbone.Events

class ProfessionalSetupLocations.App
	constructor: (options) ->
		@rootApp = options.rootApp
		@$container = options.$container

	setup: ->
		@collections =
			locations: new ProfessionalSetupLocations.Collections.Locations 0, {model: ProfessionalSetupLocations.Models.Location}
			locationsTypes: new ProfessionalSetupLocations.Collections.LocationsTypes()

		# assign collection to the model. used in validation
		ProfessionalSetupLocations.Models.Location.prototype.collection = @collections.locations
		# cache location types
		ProfessionalSetupLocations.Models.Location.prototype.locationsTypes = @collections.locationsTypes

	init: ->
		@view = new ProfessionalSetupLocations.Views.Base
			collections	: @collections
			$container	: @$container
			app			: @

	close: ->
		@view.close?()
		@view.remove?()

module.exports = ProfessionalSetupLocations

Handlebars = require 'handlebars'
Backbone = require 'backbone'

window.ProfessionalSetupServices =
	Views:
		Base: require('./views/base')
		AddService: require('./views/add_service')
		EditService: require('./views/edit_service')
		AddIntroductorySession: require('./views/add_introductory')
		EditIntroductorySession: require('./views/edit_introductory')
		Services: require('./views/services')
		Service: require('./views/service')
		Sessions: require('./views/sessions')
		Session: require('./views/session')
		Packages: require('./views/packages')
		Package: require('./views/package')
		PackagesReccuring: require('./views/packages_reccuring')
		PackageReccuring: require('./views/package_reccuring')
		ServiceForm: require('./views/service_form')
		SessionIntroductory: require('./views/session_introductory')
		SelectServiceType: require('./views/select_service_type')

	Collections:
		Services: require('./collections/services')
		Sessions: require('./collections/sessions')
		Packages: require('./collections/packages')
		PackagesReccuring: require('./collections/packages_reccuring')
		Locations: require('./collections/locations')
		ServiceTypes: require('./collections/service_types')

	Models:
		Service: require('./models/service')
		Session: require('./models/session')
		Package: require('./models/package')
		PackageReccuring: require('./models/package_reccuring')
		ServiceForm: require('./models/service_form')

	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')
		AddService: Handlebars.compile require('text!./templates/add_service.html')
		EditService: Handlebars.compile require('text!./templates/edit_service.html')
		AddIntroductorySession: Handlebars.compile require('text!./templates/add_introductory.html')
		EditIntroductorySession: Handlebars.compile require('text!./templates/edit_introductory.html')
		Services: Handlebars.compile require('text!./templates/services.html')
		Service: Handlebars.compile require('text!./templates/service.html')
		Sessions: Handlebars.compile require('text!./templates/sessions.html')
		Session: Handlebars.compile require('text!./templates/session.html')
		Packages: Handlebars.compile require('text!./templates/packages.html')
		Package: Handlebars.compile require('text!./templates/package.html')
		PackagesReccuring: Handlebars.compile require('text!./templates/packages_reccuring.html')
		PackageReccuring: Handlebars.compile require('text!./templates/package_reccuring.html')
		SubServices: Handlebars.compile require('text!./templates/sub_services.html')
		SelectServiceType: Handlebars.compile require('text!./templates/select_service_type.html')
		ServiceForm: Handlebars.compile require('text!./templates/service_form.html')
		Partials:
			LocationsField: Handlebars.compile require('text!./templates/partials/locations_field.html')
			ServiceField: Handlebars.compile require('text!./templates/partials/service_field.html')
			ServiceLabel: Handlebars.compile require('text!./templates/partials/service_label.html')
			GiftCertificates: Handlebars.compile require('text!./templates/partials/gift_certificates_field.html')
			IntroductorySession: Handlebars.compile require('text!./templates/partials/introductory_session.html')

# extend module with events
_.extend ProfessionalSetupServices, Backbone.Events

class ProfessionalSetupServices.App
	constructor: (options) ->
		@rootApp = options.rootApp
		@$container = options.$container

	setup: ->
		@collections =
			services: new ProfessionalSetupServices.Collections.Services [],
				model: ProfessionalSetupServices.Models.Service
			serviceTypes: new ProfessionalSetupServices.Collections.ServiceTypes

		unless @rootApp
			@collections.locations = new ProfessionalSetupServices.Collections.Locations()
		@

	init: ->
		@view = new ProfessionalSetupServices.Views.Base
			collections	: @collections
			$container	: @$container
			app			: @
		@

	close: ->
		@view.close?()
		@view.remove?()

module.exports = ProfessionalSetupServices

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

Select = require 'framework/select'
NumericInput = require 'framework/numeric_input'
Timezones = require 'framework/timezones'

LocationsMixins = require 'app/modules/professional_setup/mixins/location_view'
Config = require 'app/modules/professional_setup/config/config'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	className: 'professional_setup--introductory--location'

	events:
		'click [data-cancel-location]': 'cancel'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common, LocationsMixins
		@setOptions options

		@model.set 'location_type_id', 1
		@model.set 'location_type', 'office'

		@tz = new Timezones()
		@hasChanges = false
		@errors = []

		@bind()
		@addListeners()
		@render()

	bind: ->
		@bindings =
			'input[name="location_type"]':
				observe: 'location_type_id'
				onSet: (val) -> val * 1 # force integer
			'input[name="padding_time"]':
				observe: 'padding_time'
				onSet: (val) ->
					@model.set 'padding_time_changed', true
					@paddingTime.parseValue(val) * 1
				initialize: ($el) ->
					@paddingTime = new NumericInput $el,
						numberOfDecimals: 0
						initialParse: false
						parseOnBlur: false
			'input[name="name"]': 'name'
			'textarea[name="note"]':
				observe: 'note'
				onGet: (val, options) =>
					@initTextarea @$el.find(options.selector).parent(), val
					val
			'input[name="service_area"]':
				observe: 'service_area'
				onSet: (val) -> @area.parseValue(val) * 1
				initialize: ($el) ->
					@area = new NumericInput $el,
						numberOfDecimals: 0
						initialParse: false
						parseOnBlur: false
			'input[name="padding_time"]':
				observe: 'padding_time'
				onSet: (val) -> @paddingTime.parseValue(val) * 1
				initialize: ($el) ->
					@paddingTime = new NumericInput $el,
						leadingZeroCheck: false
						initialParse: false
						parseOnBlur: false
			'select[name="timezone"]':
				observe: 'timezone'
				selectOptions:
					collection: @tz.timezones()
					labelPath: 'full_name'
					valuePath: 'name'
				initialize: ($el) =>
					@timezoneSelect = new Select $el,
						minimumResultsForSearch: 0

			'input[name="address_line"]':
				observe: 'full_address'
				initialize: ($el, model, view) =>
					@pacSelectFirst($el[0])

					# removing old instances
					if @autocomplete?
						GMaps.gmaps.event.clearInstanceListeners @autocomplete
						$(".pac-container").remove();

					@autocomplete = new GMaps.gmaps.places.Autocomplete $el[0], {
						types: ['address']
						componentRestrictions:
							country: 'us'
					}
					GMaps.gmaps.event.addListener @autocomplete, 'place_changed', => @parseAutocomplete()

					@$('input[name="address_line"]').on 'input', (e) =>
						val = @$('input[name="address_line"]').val()
						newVal = val.replace(/[a-zA-Z]\d/g, (match) ->  match[0] + ' ' + match[1])
						unless val == newVal
							@$('input[name="address_line"]').val(newVal)
							GMaps.gmaps.event.trigger $el[0], 'focus', {}

				onSet: (val) =>
					# when we're typing the address we need to make sure it gets parsed
					# we can parse the address only firing 'place_changed' event on autocomplete
					@model.resetAddressFields()
					val

			'input[name="address_line_two"]': 'address_line_two'

			'input[name="address_label"]': 'address_label'

	addListeners: ->
		@listenToOnce @, 'rendered', => @afterRender()

		# show the right fields for different location types
		@listenTo @model, 'change:location_type_id', =>
			@displayLocationFields()
#			@maybeFillAddressField()
			@setLocationTypeName()
			@resetErrors()
			$(document).trigger 'popup:center'
			@

		@listenTo @model, 'error:postal_code', =>
			@raiseError 'postal_code', 'The location cannot be saved because the address is not supported'
			@

		@listenTo @model, 'error:address', =>
			@raiseError 'address_line', 'Address already exists'
			@

		@listenTo @model, 'error:generic', (errorMessage) =>
			@raiseGenericError errorMessage
			@

		# track edit model changes
		@listenTo @model, 'change', =>
			@hasChanges = true
			@

		@

	render: ->
		@$el.html ProfessionalSetupIntroductoryLocation.Templates.Base
			locationTypes: @collections.locationsTypes.toJSON()

		@$el.appendTo @$container
		@stickit()

		@trigger 'rendered'

		@

	afterRender: ->
		ProfessionalSetupIntroductoryLocation.trigger 'loading:hide'

		@cacheDom()
		@appendPeriods()
		@toggleTimezone() # set initial users timezone

		# trigger location type change event to display the right fields
		@model.trigger 'change:location_type_id'

		# method from mixin
		@initValidation()


	cacheDom: ->
		@$el.$fields = @$el.find('[data-field]')
		@$el.$availabilities = @$el.find('[data-edit-availabilities]')

		@$el.$locationAddressLine = @$el.find('input[name="address_line"]')
		@$el.$locationAddress = @$el.find('input[name="address"]')
		@$el.$locationCountryCode = @$el.find('input[name="country_code"]')
		@$el.$locationPostalCode = @$el.find('input[name="postal_code"]')
		@$el.$locationProvince = @$el.find('input[name="province"]')
		@$el.$locationCity = @$el.find('input[name="city"]')

		@$el.$error = @$el.find('[data-generic-error]')

		@$el.$locationType = @$el.find('.professional_setup--location_type')
		@$el.$timezoneSelect = @$el.find('.professional_setup--timezone')

	appendPeriods: ->
		@periods = new ProfessionalSetupLocations.Views.LocationPeriods
			collection: @model.get('availabilities')
			parentModel: @model
		@periods.render().$el.appendTo @$el.$availabilities

	cancel: ->
		# since this module is dependent on other modules cancellation should be handled
		# by the parent module.
		ProfessionalSetupIntroductoryLocation.trigger 'cancel'
		@

	setLocationTypeName: ->
		switch @model.getLocationType()
			when "virtual"
				@model.set 'name', Config.LocationNames.virtual
			when "home-visit"
				@model.set 'name', Config.LocationNames.homeVisit
			else
				@model.set 'name', Config.LocationNames.introductory
		@

	validateData: ->
		@errors = []
		errors = @model.validate()
		@errors.push errors if errors?
		_.each @model.get('availabilities').models, (item) =>
			if !item.isEmpty()
				errors = item.validate()
				if errors?
					@errors.push errors

#		@validatePeriodsOverlaps() unless @errors.length

		return @errors

	saveLocation: ->
		if !@model.get('address')? # only if address was updated
			if @autocomplete.getPlace()
				# if address was edited, but not selected
				# using $('.pac-item') because .getPlace returns short names, while in the input we have long
				inputAddressClean = @$el.$locationAddressLine.val().trim().replace(/\s+/g, '')
				googleAddressClean = $('.pac-item').first().text().trim().replace(/\s+/g, '')
				if inputAddressClean == googleAddressClean
					GMaps.gmaps.event.trigger @autocomplete, 'place_changed'
				@doSaveLocation()
			else
				$.when( @getFirstPrediction(@$el.$locationAddressLine.val()) ).then( (status)=>
					unless status?
						@doSaveLocation()
					else
						raiseGenericError status
				)

		else
			@doSaveLocation()

	doSaveLocation: ->
		@raiseGenericError null
		@validateData()

		return if @errors.length
		$.when(
			@model.save()
		).then(
			=> @afterSave()
		)

	afterSave: ->
		# append location to the list of locations
		if @collections.locations?
			@collections.locations.add @model.toJSON()

module.exports = View

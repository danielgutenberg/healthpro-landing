Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Tree = require 'utils/tree'
Constants = require 'config/constants'
Config = require 'app/modules/professional_setup/config/config'

class Collection extends Backbone.Collection

	initialize: ->
		@tree = new Tree [],
			prefix: '—'

	getServiceTypes: ->
		$.ajax
			url: getApiRoute('ajax-provider-service-types') + '?sort=name'
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.each response.data, (model) =>
					if model.slug is Constants.IntroductorySessionSlug
						Config.IntroductoryService = model
					else
						@push model
				@tree.setData @toJSON()
				@trigger 'data:ready'

	getTree: -> @tree.getTree()
	getFlatTree: -> @tree.getFlatTree()

module.exports = Collection

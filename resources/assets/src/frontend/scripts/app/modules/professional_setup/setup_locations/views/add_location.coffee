BaseEditLocationView = require './abstract/abstract_edit_location'
NumericInput = require 'framework/numeric_input'
Config = require 'app/modules/professional_setup/config/config'
Device = require 'utils/device'

class View extends BaseEditLocationView

	initialize: (options) ->
		super(options)

		_.extend @events,
			'click .professional_setup--location_name input': 'selectInputContent'
			'click .professional_setup--location_address input': 'maybeSelectInputContent'
			'click input[name="timezone_current"]': 'toggleTimezone'

		@delegateEvents()

		@model = new ProfessionalSetupLocations.Models.Location()

		@bind()
		@addListeners()
		@render()
		@cacheDom()
		@appendPeriods()
		@toggleTimezone() # set initial users timezone

		# trigger location type change event to display the right fields
		if Device.isMobile()
			# for mobile we don't need to set the default location type id
			@model.trigger 'change:location_type_id'
		else
			@model.set 'location_type_id', @collections.locationsTypes.first().get('id')

		# method from mixin
		@initValidation()

	addListeners: ->
		super
		@listenTo @model, 'change:location_type_id', (model, val) =>

			@displayLocationFields()
			@maybeFillAddressField()
			@setLocationTypeName()
			@resetErrors()
			@toggleForm if val then true else false

	toggleForm: (display = true) ->

		if @baseView.hasPopup()
			popup = @baseView.getPopup()
			popup.toggleBack(display)
			if display
				popup.bindEvent 'back', =>
					@model.set 'location_type_id', null

		if display
			@$el.addClass('m-location_type_set')
			@$el.$form.removeClass('m-hide')

		else
			@$el.removeClass('m-location_type_set')
			@$el.$form.addClass('m-hide')
			@$el.find('input[type=radio]').prop('checked', false)
		@

	bind: ->
		super
		@bindings['input[name="location_type"]'] =
			observe: 'location_type_id'
			onSet: (val) -> val * 1 # force integer

		@bindings['input[name="padding_time"]'] =
			observe: 'padding_time'
			onSet: (val) ->
				@model.set 'padding_time_changed', true
				@paddingTime.parseValue(val) * 1

			initialize: ($el) ->
				@paddingTime = new NumericInput $el,
					numberOfDecimals: 0
					initialParse: false
					parseOnBlur: false

	render: ->
		@$el.html ProfessionalSetupLocations.Templates.AddLocation
			locationTypes: @collections.locationsTypes.toJSON()

		@$el.appendTo @$container

		@stickit()

		if @baseView.hasPopup()
			@afterRenderPopup()
		else
			@$container.removeClass 'm-hide'
			@baseView.scrollTo @$container
		@

	afterSave: ->
		super
		@collections.locations.push @model
		@baseView.cancelEditLocation true

	remove: ->
		unless @baseView.hasPopup()
			@$container.addClass 'm-hide'
		super

	setLocationTypeName: ->
		if @baseView.hasPopup()
			headerLabel = if @model.get('location_type_id') then @model.getLocationTypeLabel() else 'Add New Location'
			@baseView.getPopup().updateHeader headerLabel, false, true

		locationType = @model.getLocationType()
		switch locationType
			when "virtual"
				@model.set 'name', Config.LocationNames.virtual
			when "home-visit"
				@model.set 'name', Config.LocationNames.homeVisit
			else
				@model.set 'name', 'Location ' + (@collections.locations.length + 1)
		@

	afterRenderPopup: ->
		@baseView.scrollTo @baseView.$el
		popup = @baseView.getPopup()
		popup.openPopup()
		popup.updateHeader 'Add New Location', false, true
		popup.bindEvent 'cancel', =>
			# bind single close event
			@baseView.cancelEditLocation true



module.exports = View

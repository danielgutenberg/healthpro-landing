Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Device = require 'utils/device'

class View extends Backbone.View

	events:
		'click [data-services-add-btn]': 'addService'
		'click [data-introductory-add-btn]': 'addIntroductorySession'
		'click [data-services-service-type-btn]': 'selectServiceType'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@services = []

		@render()
		@bind()
		@cacheDom()

	bind: ->
		@listenTo @collections.services, 'add remove', @toggleIntroductoryBtn

		@listenToOnce @baseView, 'data:loaded', =>
			@collections.services.each (model) => @renderService model
			# after initial load listen to the add event
			@listenTo @collections.services, 'add', @renderService
		@

	render: ->
		@$el.html ProfessionalSetupServices.Templates.Services
			isMobile: Device.isMobile()
		@baseView.$el.$main.append @$el

	cacheDom: ->
		@$el.$list = @$el.find('[ data-services-list]')
		@$el.$addBtn = @$el.find('[data-services-add-btn]')
		@$el.$addIntroductoryBtn = @$el.find('[data-introductory-add-btn]')

		@$el.$selectServiceTypeBtn = @$el.find('[data-services-service-type-btn]')

		@

	renderService: (model) ->
		@services.push view = new ProfessionalSetupServices.Views.Service
			model: model
			collections: @collections
			parentView: @
			baseView: @baseView

		view.render().$el.appendTo @$el.$list
		@

	addService: (e) ->
		e?.preventDefault()
		return if @$el.$addBtn.hasClass('m-edit')

		@baseView.cancelEditServiceWithConfirmation => @forceAddService()
		@

	addIntroductorySession: (e) ->
		e?.preventDefault()
		return if @$el.$addBtn.hasClass('m-edit')

		@baseView.cancelEditServiceWithConfirmation => @forceAddIntroductorySession()
		@

	forceAddService: (fromModel = null) ->
		@baseView.initAddService(fromModel)
		@$el.$addBtn.addClass('m-edit')
		@$el.$addIntroductoryBtn.addClass('m-edit')
		@$el.$selectServiceTypeBtn.addClass('m-edit')
		@

	forceAddIntroductorySession: ->
		@baseView.initAddIntroductorySession()
		@$el.$addBtn.addClass('m-edit')
		@$el.$addIntroductoryBtn.addClass('m-edit')
		@$el.$selectServiceTypeBtn.addClass('m-edit')
		@

	cancelEditAddService: ->
		@$el.$addBtn.removeClass('m-edit')
		@$el.$addIntroductoryBtn.removeClass('m-edit')
		@$el.$selectServiceTypeBtn.removeClass('m-edit')
		@

	cancelEdit: ->
		@cancelEditAddService()
		_.each @services, (serviceView) ->
			serviceView.cancelEdit()
			return
		@

	toggleIntroductoryBtn: ->
		if @collections.services.where({introductory: true}).length > 0
			@$el.$addIntroductoryBtn.addClass 'm-hide'
		else
			@$el.$addIntroductoryBtn.removeClass 'm-hide'

	selectServiceType: (e) ->
		e?.preventDefault()

		return unless Device.isMobile()

		# we need to display service type only for mobile
		# and only when we don't have the introductory session set
		# if we already have introductory session this button acts like an add service button
		if @collections.services.where({introductory: true}).length > 0
			@addService()
			return

		@baseView.initSelectServiceType()
		@$el.$selectServiceTypeBtn.addClass('m-edit')

module.exports = View

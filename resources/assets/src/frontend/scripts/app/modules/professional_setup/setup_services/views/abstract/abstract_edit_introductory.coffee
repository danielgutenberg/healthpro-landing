Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
IntroductoryLocation = require 'app/modules/professional_setup/setup_introductory_location'
isMobile = require 'isMobile'
Config = require 'app/modules/professional_setup/config/config'

require 'backbone.stickit'
require 'backbone.validation'
require 'tooltipster'

class View extends Backbone.View

	events:
		'click [data-save-introductory]'   : 'saveIntroductorySession'
		'click [data-cancel-introductory]' : 'cancelEditIntroductorySession'
		'click [data-location-add]' : 'addIntroductorySessionLocation'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@hasChanges = false
		@subViews = []
		@errors = []

	afterInit: ->
		@bind()
		@addStickit()
		@render()
		@initValidation()

	bind: ->
		@listenToOnce @, 'rendered', =>
			@afterRender()
			@stickit()
			@

		@listenTo @model, 'error:generic', (errorMessage) =>
			@raiseGenericError errorMessage
			@

		# track edit model changes
		@listenTo @model, 'change', =>
			@hasChanges = true
			@

	afterRender: ->
		@appendIntroductorySession()
		@cacheDom()
		@appendLocations()
		@initSession() # introductory session has only one session

		@initTooltips()

	addStickit: ->
		@bindings =
			'[name="location_ids"]' :
				observe    : 'location_ids'
				getVal     : @onSetLocation
				initialize : ($el) => @model.get('location_ids').forEach (location) -> $el.filter('[value=' + location + ']').prop('checked', true)
			'[name="description"]'  : 'description'

	onSetLocation: ($el) =>
		values = []
		if $el.length > 1
			values = _.reduce($el, ((memo, el) ->
				checkbox = Backbone.$(el)
				if checkbox.prop('checked')
					memo.push parseInt(checkbox.val())
				memo
			), [])
		else
			val = $el.val()
			if val != 'on' and val != null and $el.prop('checked')
				values.push parseInt(val)
		values

	cacheDom: ->
		@$el.$locations = @$el.find('[data-edit-locations]')
		@$el.$error = @$el.find('[data-generic-error]')
		@$el.$sessionsError = @$el.find('[data-sessions-error]')
		@$el.$session = @$el.find('[data-session]')
		@

	appendIntroductorySession: ->
		@$el.$section = @$el.find('[data-introductory-session]')
		@$el.$section.html ProfessionalSetupServices.Templates.Partials.IntroductorySession()
		@

	appendLocations: (delegateEvents = false) ->
		locations = @collections.locations.toJSON().map (item) ->
			switch item.location_type
				when 'office'
					item.icon = Config.LocationIconsSmall.office
				when 'home-visit'
					item.icon = Config.LocationIconsSmall.homeVisit
				when 'virtual'
					item.icon = Config.LocationIconsSmall.virtual
			item

		@$el.$locations.html ProfessionalSetupServices.Templates.Partials.LocationsField
			locations: locations
			addNewEnabled: if @model.get('id') then false else true
			addNewLabel: 'Add a new location for introductory session'

		@$el.$addLocation = @$el.find('[data-location-add]')
		@$el.$locationsOffered = @$el.$locations.find('[data-locations-offered]')

		if delegateEvents
			@delegateEvents()
			@stickit()
		@

	initSession: ->
		@sessionView = new ProfessionalSetupServices.Views.SessionIntroductory
			model: @model.get('sessions').first() # we have only one session here
			parentModel: @model
			baseView: @baseView
			parentView: @
			el: @$el.$session
		@

	initTooltips: ->
		@$el.find('[data-tooltip-click]').tooltipster
			theme: 'tooltipster-dark'
			maxWidth: 200
			repositionOnScroll: true
			trigger: if isMobile.any then 'click' else 'hover'

		@$el.find('[data-tooltip]').tooltipster
			theme: 'tooltipster-dark'
			maxWidth: 200

	saveIntroductorySession: ->
		@raiseGenericError null
		@validateData()

		return if @errors.length

		@beforeSave()

		locationsDeferred = $.Deferred()

		$.when(
			locationsDeferred
		).then(
			=> @model.save()
		).then(
			=> @afterSave()
		).fail(
			=> @hideLoading()
		)

		# trigger save location
		@maybeSaveLocation locationsDeferred
		@

	maybeSaveLocation: (deferred) ->
		unless @addLocation
			deferred.resolve()
			return
		$.when(
			@addLocation.saveLocation()
		).then(
			=> @afterSaveLocation()
		).then(
			=> deferred.resolve()
		).fail(
			=> @hideLoading()
		)

	afterSaveLocation: ->
		locationModel = @addLocation.view.model

		# move to the template?
		$locationCheckbox = $("""
			<label class="field checkbox">
				<input type="checkbox" name="location_ids" value="#{locationModel.get('id')}">
				<span class="checkbox--i"></span>
				<span class="checkbox--label">#{locationModel.get('name')}</span>
				<i class="checkbox--icon m-#{locationModel.getLocationType()}"></i>
			</label>
		""")

		@$el.$locationsOffered.append $locationCheckbox

		# re-stickit
		@stickit()

		# trigger select location
		$locationCheckbox.find('input').trigger 'click'

		# close the add location view and remove the button
		# the professional can add only one location
		@cancelIntroductorySessionLocation()
		@$el.$addLocation.remove()

		true

	validateData: ->
		@raiseSessionsError null
		@errors = []
		errors = @model.validate()
		@errors.push errors if errors?

		if @sessionView
			errors = @sessionView.model.validate()
			@errors.push errors if errors?

		# check new location for errors
		if @addLocation
			errors = @addLocation.validate()
			@errors.push errors if errors.length

		# display a message
		activeSessions = @model.getActiveSessions()

		# we need to validate only active sessions here
		_.each activeSessions, (sessionModel) =>
			return if sessionModel.isEmpty()
			errors = sessionModel.validate()
			@errors.push errors if errors?
			return

		@errors

	initValidation: ->
		Backbone.Validation.bind @,
			valid: (view, attr) =>
				@raiseError attr, null
			invalid: (view, attr, error) =>
				@raiseError attr, error

	raiseError: (attr, error) ->
		field = @$el.find('[name="' + attr + '"]')
		field_group = field.parents('.field--group')
		if error
			if field_group.length
				field_group.addClass 'm-error'
				field_group.find('.field--error').html error
			else
				field.parents('.field, .select').addClass 'm-error'
				field.prev('.field--error').html error

			field.on 'focus.validation', ->
				if field_group.length
					field_group.removeClass 'm-error'
				field.parents('.field, .select').removeClass 'm-error'
				field.off('focus.validation')
		else
			if field_group.length
				field_group.removeClass 'm-error'
			else
				field.parents('.field, .select').removeClass 'm-error'
				field.prev('.field--error').html ''

	raiseEditError: (message, $errorEl, type = 'error') ->
		if message
			$errorEl.removeClass 'm-hide'
			$errorEl.html "<div class='alert m-#{type} m-show'>#{message}</div>"
		else
			$errorEl.addClass 'm-hide'
			$errorEl.html ''
		@

	raiseGenericError: (message = null, type = 'error') ->
		@raiseEditError message, @$el.$error, type
		@

	raiseSessionsError: (message = null, type = 'error') ->
		@raiseEditError message, @$el.$sessionsError, type
		@

	beforeSave: -> @showLoading()
	afterSave: -> @hideLoading()

	showLoading: (isSave = true) -> @baseView.showLoading(isSave)
	hideLoading: (isSave = true) -> @baseView.hideLoading(isSave)

	cancelEditIntroductorySession: (scrollTop = false) ->
		@baseView.cancelEditServiceWithConfirmation => @baseView.cancelEditService(scrollTop)
		@

	toggleAddLocation: (display = true) ->
		if display
			@$el.$addLocation.removeClass 'm-hide'
		else
			@$el.$addLocation.addClass 'm-hide'
		@

	addIntroductorySessionLocation: ->
		# create a subview
		return if @addLocation
		@toggleAddLocation false

		# the introductory location module is a submodule and this module should know when it gets closed.
		@listenToOnce ProfessionalSetupIntroductoryLocation, 'cancel', @cancelIntroductorySessionLocation

		# bind loading events
		@listenToOnce ProfessionalSetupIntroductoryLocation, 'loading:show', @showLoading
		@listenToOnce ProfessionalSetupIntroductoryLocation, 'loading:hide', @hideLoading

		@addLocation = new IntroductoryLocation.App
			base			: @baseView.app
			$container		: @$el.$locations.find('[data-add-location-container]')
		$(document).trigger 'popup:center'

		# remove locations error if exists
		@$el.$locations.find('.field--group.m-error').removeClass('m-error').find('.field--error').html ''


		@model.set 'introductory_location', @addLocation.view.model
		@

	cancelIntroductorySessionLocation: ->
		return @ unless @addLocation
		@addLocation.close()
		@addLocation = null
		@model.set 'introductory_location', null
		@toggleAddLocation true
		@


module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

Select = require 'framework/select'
NumericInput = require 'framework/numeric_input'

LocationsMixins = require 'app/modules/professional_setup/mixins/location_view'
Config = require 'app/modules/professional_setup/config/config'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	events:
		'click [data-save-location]': 'saveLocation'
		'click [data-cancel-location]': 'cancelEditLocation'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common, LocationsMixins
		@setOptions options

		@hasChanges = false
		@errors = []

	addListeners: ->
		# show the right fields for different location types
		@listenTo @model, 'change:location_type_id', =>
			@displayLocationFields()
#			$(document).trigger 'popup:center'
			@

		@listenTo @model, 'error:postal_code', =>
			@raiseError 'postal_code', 'The area of the address you have entered is not supported'
			@

		@listenTo @model, 'error:address', =>
			@raiseError 'address_line', 'Address already exists'
			@

		@listenTo @model, 'error:generic', (errorMessage) =>
			@raiseGenericError errorMessage
			@

		# track edit model changes
		@listenTo @model, 'change', =>
			@hasChanges = true
			@

		@

	bind: ->
		@bindings =
			'input[name="location_name"]': 'name'
			'textarea[name="note"]':
				observe: 'note'
				onGet: (val, options) =>
					@initTextarea @$el.find(options.selector).parent(), val
					val

			'input[name="service_area"]':
				observe: 'service_area'
				onSet: (val) -> @area.parseValue(val) * 1
				initialize: ($el) ->
					@area = new NumericInput $el,
						numberOfDecimals: 0
						initialParse: false
						parseOnBlur: false

			'input[name="padding_time"]':
				observe: 'padding_time'
				onSet: (val) -> @paddingTime.parseValue(val) * 1
				initialize: ($el) ->
					@paddingTime = new NumericInput $el,
						numberOfDecimals: 0
						initialParse: false
						parseOnBlur: false

			'select[name="timezone"]':
				observe: 'timezone'
				selectOptions:
					collection: @baseView.tz.timezones()
					labelPath: 'full_name'
					valuePath: 'name'
				initialize: ($el) =>
					# wait for 50ms before initing timezones select to make popup open faster
					setTimeout =>
						@timezoneSelect = new Select $el,
							minimumResultsForSearch: 0
					, 50

			'input[name="address_line"]':
				observe: 'full_address'
				initialize: ($el, model, view) =>
					@pacSelectFirst($el[0])

					# removing old instances
					if @autocomplete?
						GMaps.gmaps.event.clearInstanceListeners @autocomplete
						$(".pac-container").remove();

					@autocomplete = new GMaps.gmaps.places.Autocomplete $el[0], {
						types: ['address']
						componentRestrictions:
							country: 'us'
					}
					GMaps.gmaps.event.addListener @autocomplete, 'place_changed', => @parseAutocomplete()

					@$('input[name="address_line"]').on 'input', (e) =>
						val = @$('input[name="address_line"]').val()
						newVal = val.replace(/[a-zA-Z]\d/g, (match) ->  match[0] + ' ' + match[1])
						unless val == newVal
							@$('input[name="address_line"]').val(newVal)
							GMaps.gmaps.event.trigger $el[0], 'focus', {}

				onSet: (val) =>
					# when we're typing the address we need to make sure it gets parsed
					# we can parse the address only firing 'place_changed' event on autocomplete
					@model.resetAddressFields()
					val

			'input[name="address_line_two"]': 'address_line_two'
			'input[name="address_label"]': 'address_label'
		@

	cacheDom: ->
		@$el.$form = @$el.find('[data-fields]')
		@$el.$fields = @$el.find('[data-field]')

		@$el.$availabilities = @$el.find('[data-edit-availabilities]')
		@$el.$error = @$el.find('[data-generic-error]')

		@$el.$locationAddressLine = @$el.find('input[name="address_line"]')
		@$el.$locationAddress = @$el.find('input[name="address"]')
		@$el.$locationCountryCode = @$el.find('input[name="country_code"]')
		@$el.$locationPostalCode = @$el.find('input[name="postal_code"]')
		@$el.$locationProvince = @$el.find('input[name="province"]')
		@$el.$locationCity = @$el.find('input[name="city"]')

		@$el.$timezoneSelect = @$el.find('[data-field-timezone]')
		@

	appendPeriods: ->
		@periods = new ProfessionalSetupLocations.Views.LocationPeriods
			collection: @model.get('availabilities')
			parentModel: @model
		@periods.render().$el.appendTo @$el.$availabilities

	saveLocation: ->
		if !@model.get('address')? # only if address was updated
			if @autocomplete.getPlace()
				# if address was edited, but not selected
				# using $('.pac-item') because .getPlace returns short names, while in the input we have long
				inputAddressClean = @$el.$locationAddressLine.val().trim().replace(/\s+/g, '')
				googleAddressClean = $('.pac-item').first().text().trim().replace(/\s+/g, '')
				if inputAddressClean == googleAddressClean
					GMaps.gmaps.event.trigger @autocomplete, 'place_changed'
				@doSaveLocation()
			else
				$.when( @getFirstPrediction(@$el.$locationAddressLine.val()) ).then( (status)=>
					unless status?
						@doSaveLocation()
					else
						@raiseGenericError status
				)

		else
			@doSaveLocation()

	doSaveLocation: ->
		@raiseGenericError null
		@validateData()

		return if @errors.length

		@showLoading()
		$.when(@model.save()).then(
			(response) =>
				if @type? and @type is 'dashboard'
					vexDialog.buttons.YES.text = 'Ok'
					vexDialog.alert
						message: Config.Messages.updateSuccess

				@afterSave()
			, (error) =>
				if error.responseJSON?.errors?.error?.messages?[0]
					message = error.responseJSON?.errors?.error?.messages?[0]
				else
					message = Config.Messages.updateError
				console.log message
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: message

				@hideLoading(true)
		)

	validateData: ->
		@errors = []
		errors = @model.validate()
		@errors.push errors if errors?
		_.each @model.get('availabilities').models, (item) =>
			if !item.isEmpty()
				errors = item.validate()
				if errors?
					@errors.push errors

		@validatePeriodsOverlaps() unless @errors.length

	afterSave: -> @hideLoading()

	showLoading: (isSave = true) -> @baseView.showLoading(isSave)
	hideLoading: (isSave = true) -> @baseView.hideLoading(isSave)

	validatePeriodsOverlaps: ->
		if @model.get('availabilities').hasOverlaps()
			@errors.push 'overlap'
			@raiseGenericError "Oops! Looks like there's a scheduling conflict. The times you've selected overlap."
		else
			@raiseGenericError null

	cancelEditLocation: (scrollTop = false) ->
		@baseView.cancelEditLocationWithConfirmation => @baseView.cancelEditLocation(scrollTop)
		@

module.exports = View

BaseEditIntroductorySessionView  = require './abstract/abstract_edit_introductory'

class View extends BaseEditIntroductorySessionView

	className: "professional_setup--edit m-service"

	initialize: (options) ->
		super(options)

		# we don't want to update the models on the sidebar right away
		@model = @initialModel.deepClone()

		@afterInit()

	render: ->
		@$el.html ProfessionalSetupServices.Templates.EditIntroductorySession()
		@$el.appendTo @$container

		if @baseView.hasPopup()
			@afterRenderPopup()
		else
			@$container.removeClass 'm-hide'
			@baseView.scrollTo @$container

		@trigger 'rendered'
		@

	afterSave: ->
		super
		@initialModel.updateFromEdit @model.toJSON()
		@baseView.cancelEditService()

	afterRenderPopup: ->
#		@baseView.scrollTo @parentView.$el # scroll to the location view
		@baseView.scrollTo @baseView.$el
		popup = @baseView.getPopup()
		popup.openPopup()
		popup.updateHeader 'Edit Introductory Session', false, true
		popup.bindEvent 'cancel', =>
			# bind single close event
			@baseView.cancelEditService true

module.exports = View

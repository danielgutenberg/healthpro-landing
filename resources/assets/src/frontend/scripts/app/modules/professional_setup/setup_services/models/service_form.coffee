Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		email: ''
		service: ''

	validation:
		email:
			required: true
			pattern: 'email'
			msg: 'Please enter a valid email'
		service:
			required: true
			msg: 'Please enter a service name'

	save: ->
		$.ajax
			url: getApiRoute('ajax-cs-add-service-request')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				email: @get 'email'
				service: @get 'service'
				_token: window.GLOBALS._TOKEN


module.exports = Model

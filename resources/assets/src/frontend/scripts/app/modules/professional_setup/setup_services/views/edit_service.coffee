BaseEditServiceView  = require './abstract/abstract_edit_service'

class View extends BaseEditServiceView

	className: "professional_setup--edit m-service"

	initialize: (options) ->
		super(options)

		# we don't want to update the models on the sidebar right away
		@model = @initialModel.deepClone()

		@afterInit()

	render: ->
		@$el.html ProfessionalSetupServices.Templates.EditService
			gift_certificates: @hasFeature('gift_certificates')
			monthly_packages: @hasFeature('monthly_packages')

		@$el.appendTo @$container

		if @baseView.hasPopup()
			@afterRenderPopup()
		else
			@baseView.scrollTo @parentView.$el # scroll to the service view

		@trigger 'rendered'
		@

	afterRender: ->
		super
		@appendServiceField()

	appendServiceField: ->
		@$el.$service.html ProfessionalSetupServices.Templates.Partials.ServiceLabel()
		@

	afterSave: ->
		super
		@initialModel.updateFromEdit @model.toJSON()
		@baseView.cancelEditService()

	afterRenderPopup: ->
		@baseView.scrollTo @baseView.$el
		popup = @baseView.getPopup()
		popup.openPopup()
		popup.updateHeader @model.get('name'), false, true
		popup.bindEvent 'cancel', =>
			# bind single close event
			@baseView.cancelEditService true

module.exports = View

Maxlength = require 'framework/maxlength'
Moment = require 'moment'

module.exports =
	initValidation: ->
		Backbone.Validation.bind @,
			valid: (view, attr) => @raiseError attr, null
			invalid: (view, attr, error) => @raiseError attr, error

	raiseError: (attr, error) ->
		# show all address errors on the address field
		if @model.addressFields.indexOf(attr) isnt -1
			field = @$el.find('[name="address_line"]')
		else
			field = @$el.find('[name="' + attr + '"]')

		return unless field.length

		if error?
			field.parents('.field').addClass 'm-error'
			field.prev('.field--error').html error
			field.on 'focus.validation', ->
				field.parents('.field').removeClass('m-error')
				field.off('focus.validation')
		else
			field.parents('.field').removeClass 'm-error'
			field.prev('.field--error').html ''

	raiseGenericError: (message = null, type = 'error') ->
		if message
			@$el.$error.removeClass 'm-hide'
			@$el.$error.html "<div class='alert m-#{type} m-show'>#{message}</div>"
		else
			@$el.$error.addClass 'm-hide'
			@$el.$error.html ''

	resetErrors: ->
		@raiseGenericError null
		_.each @model.toJSON(), (value, attr) => @raiseError attr, null
		_.each @periods.subViews, (view) -> view.resetErrors()
		@

	initTextarea: ($el, val) ->
		unless $el.attr('data-maxlength') is 'inited'
			$('textarea', $el).val(val)
			new Maxlength $el
		@

	selectInputContent: (ev) -> ev.currentTarget.select()

	maybeSelectInputContent: (ev) -> @selectInputContent(ev) if @model.isPrepopulatedAddress()

	getFirstPrediction: (string) ->
		serviceDeffered = $.Deferred()
		if string.trim().length
			service = new (GMaps.gmaps.places.AutocompleteService)
			service.getQueryPredictions { input: string }, (predictions, status) =>
				if status is GMaps.gmaps.places.PlacesServiceStatus.OK
					placeservice = new (GMaps.gmaps.places.PlacesService)(document.createElement('div'))
					placeservice.getDetails { placeId: predictions[0].place_id }, (place, status) =>
						# only if prediction and input are the same
						if status is GMaps.gmaps.places.PlacesServiceStatus.OK and predictions[0].description.trim() == string.trim()
							@model.parseAddressFields place
						serviceDeffered.resolve()
				else serviceDeffered.resolve('Service erorr')
		else
			serviceDeffered.resolve()

		return serviceDeffered.promise()

	parseAutocomplete: -> @model.parseAddressFields @autocomplete.getPlace()


	pacSelectFirst: (input) ->
# store the original event binding function
		_addEventListener = if input.addEventListener then input.addEventListener else input.attachEvent

		addEventListenerWrapper = (type, listener) ->
# Simulate a 'down arrow' keypress when no pac suggestion is selected,
# and then trigger the original listener
			if type == 'keydown'
				orig_listener = listener

				listener = (event) ->
					suggestion_selected = $('.pac-item-selected').length > 0
					if event.which == 13 and !suggestion_selected
						simulated_downarrow = $.Event('keydown',
							keyCode: 40
							which: 40)
						orig_listener.apply input, [ simulated_downarrow ]
					orig_listener.apply input, [ event ]

			# add the modified listener
			_addEventListener.apply input, [
				type
				listener
			]

		if input.addEventListener
			input.addEventListener = addEventListenerWrapper
		else if input.attachEvent
			input.attachEvent = addEventListenerWrapper

	toggleTimezone: (e) ->
		if e?
			checked = $(e.currentTarget).is(':checked')
			if checked
				@$el.$timezoneSelect.addClass 'm-hide'
				@model.set 'timezone', Moment.tz.guess()
				@timezoneSelect.$node.trigger 'change'
			else
				@$el.$timezoneSelect.removeClass 'm-hide'
		else
			@model.set 'timezone', Moment.tz.guess()

	maybeFillAddressField: ->

		@model.set 'prepopulated_address', false

		locationType = @model.getLocationType()

		if locationType is 'home-visit'
# if we have a populated address do nothing
			return if @model.get('full_address')?.trim()

			officeLocationType = @collections.locationsTypes.findWhere
				type: 'office'

			officeModel = @collections.locations.findWhere
				location_type_id: officeLocationType.get('id')

			return unless officeModel

			# set all the address fields if we found a model with office location type
			@model.set 'full_address', officeModel.get('full_address')
			_.each @model.addressFields, (addressField) => @model.set addressField, officeModel.get(addressField)
			@model.set 'prepopulated_address', true

		# if we already have the address in the collection - clean up fields
		else if locationType is 'office'

			# do nothing if the address is empty
			return unless @model.get('full_address')?.trim()

			modelWithSameAddress = @collections.locations.findWhere
				full_address: @model.get('full_address')

			return unless modelWithSameAddress

			# cleanup the field if we found the model
			@model.set 'full_address', ''
			_.each @model.addressFields, (addressField) => @model.set addressField, ''

	displayLocationFields: ->
		@$el.$fields.addClass 'm-hide'

		_.each @model.getLocationEditFields(), (field) =>
			@$el.$fields.filter('[data-field="' + field + '"]').removeClass 'm-hide'

		addressDescription = @$el.$fields.filter('[data-field="address_line"]').find('.field--description')
		addressLabel = @$el.$fields.filter('[data-field="address_line"]').find('dt')
		if @model.getLocationType() is 'home-visit'
			addressLabel.addClass 'm-hide'
			addressDescription.removeClass 'm-hide'
		else
			addressLabel.removeClass 'm-hide'
			addressDescription.addClass 'm-hide'
		@

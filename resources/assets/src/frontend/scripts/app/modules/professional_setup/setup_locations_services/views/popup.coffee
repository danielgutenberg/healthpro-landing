Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'click [data-popup-back]'	: 'triggerBack'
		'click [data-popup-cancel]'	: 'triggerCancel'


	attributes:
		'class': 'popup professional_setup--popup'
		'data-popup': 'professional_setup'
		'id' : 'popup_professional_setup'

	initialize: (options = {}) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@initPopup()
		@cacheDom()

		@bindEvents =
			back: []
			cancel: []

	cacheDom: ->
		@$el.$container = @$el.find('[data-popup-content-container]')
		@$el.$back = @$el.find('[data-popup-back]')
		@$el.$cancel = @$el.find('[data-popup-cancel]')
		@$el.$title = @$el.find('[data-popup-title]')
		@$el.$loading = @$el.find('.loading_overlay')
		@

	initPopup: ->
		@$el.html """
			<div class="popup--container" data-popup-container>
				<div class="professional_setup--popup--header">
					<button class="professional_setup--popup--header--back m-hide" data-popup-back></button>
					<h3 class="professional_setup--popup--header--title" data-popup-title></h3>
					<button class="professional_setup--popup--header--close m-hide" data-popup-cancel></button>
				</div>
				<div class="professional_setup--popup--inner">
					<div class="professional_setup--popup--scroll">
						<div class="professional_setup--popup--content" data-popup-content-container></div>
					</div>
				</div>
				<span class="loading_overlay m-light m-hide"><span class="spin m-logo"></span></span>
			</div>
			<div class="popup--bg"></div>
		"""

		$('body').append @$el
		window.popupsManager.addPopup @$el
		@

	openPopup: ->
		window.popupsManager.openPopup 'professional_setup'
		@

	closePopup: (empty = true) ->
		window.popupsManager.closePopup 'professional_setup'
		if empty
			@emptyContainer()
		@

	emptyContainer: ->
		@$el.$container.html ''
		@

	getContainer: -> @$el.$container

	triggerBack: ->
		@bindEvents.back.forEach (fn) -> fn()
		@bindEvents.back = []
		@

	triggerCancel: ->
		@bindEvents.cancel.forEach (fn) -> fn()
		@bindEvents.cancel = []
		@

	toggleEl: ($el, display = null) ->
		switch display
			when true then $el.removeClass('m-hide')
			when false then $el.addClass('m-hide')
			else $el.toggleClass('m-hide')

	toggleBack: (display = null) -> @toggleEl @$el.$back, display

	toggleCancel: (display = null) -> @toggleEl @$el.$cancel, display

	updateHeader: (title = '', displayBack = false, displayCancel = false) ->
		@$el.$title.html title
		@toggleBack displayBack
		@toggleCancel displayCancel

	bindEvent: (btn, fn) -> @bindEvents[btn].push fn

module.exports = View

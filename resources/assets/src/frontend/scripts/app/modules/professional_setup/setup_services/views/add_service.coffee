BaseEditServiceView  = require './abstract/abstract_edit_service'
Select = require 'framework/select'

class View extends BaseEditServiceView

	initialize: (options) ->
		super(options)

		@model = new ProfessionalSetupServices.Models.Service {},
			fromModel: @fromModel # duplicate service from given model
		if @collections.locations.length is 1
			@model.set 'location_ids', @collections.locations.pluck('id')

		@afterInit()

	bind: ->
		super

		# disable service in sub-services if it is selected as a main service
		@listenTo @model, 'change:service_type_id', @onServiceTypeChanged


	addStickit: ->
		super

		@bindings['[name="service_type_id"]'] =
			observe: 'service_type_id'
			selectOptions:
				collection: @collections.serviceTypes.getFlatTree()
				labelPath: 'label'
				valuePath: 'value'
				defaultOption:
					label: 'Select Service'
					value: null
			initialize: ($el) =>
				@selectService = new Select $el,
					minimumResultsForSearch: 0
					templateResult: (service) ->
						return service.text if service.loading or !service.id
						if service.text.match '—'
							return service.text
						return "<strong>#{service.text}</strong>"
					templateSelection: (service) -> service.text
					escapeMarkup: (markup) -> markup
				$el.on 'select2:select', -> $el.trigger 'focus.validation'
			getVal: @onSetService

	onSetService: ($el) =>
		val = parseInt $el.val()
		@model.set 'name', if val then @collections.serviceTypes.get(val).get('name') else ''

		locationIds = @model.get('location_ids').map (val) -> parseInt(val)

		services = @collections.services.findDuplicateServices [val], @model
		double = _.reduce services, (result, service) ->
			_.intersection(service.get('location_ids'), locationIds).length > 0
		, false

		if double is true
			$el.val('').trigger 'change'
			@duplicateAlert()
			return null
		val

	render: ->
		@$el.html ProfessionalSetupServices.Templates.AddService
			gift_certificates: @hasFeature('gift_certificates')
			monthly_packages: @hasFeature('monthly_packages')

		@$el.appendTo @$container

		if @baseView.hasPopup()
			@afterRenderPopup()
		else
			@$container.removeClass 'm-hide'
			@baseView.scrollTo @$container

		@trigger 'rendered'
		@

	afterRender: ->
		super
		@appendServiceField()

	appendServiceField: ->
		@$el.$service.html ProfessionalSetupServices.Templates.Partials.ServiceField()

	afterSave: ->
		super
		@collections.services.push @model
		@baseView.cancelEditService true

	remove: ->
		unless @baseView.hasPopup()
			@$container.addClass 'm-hide'
		super

	onServiceTypeChanged: ->
		# enable all disabled options
		@selectSubServices.$node.find('option').prop('disabled', false)

		# disable selected service type option
		@selectSubServices.$node.find('option[value=' + @model.get('service_type_id') + ']').prop('disabled', true);

		# remove selected service from service type ids
		@model.set 'service_type_ids', _.without(@model.get('service_type_ids'), @model.get('service_type_id'))

		# reinit
		@selectSubServices.reInit()

	afterRenderPopup: ->
		@baseView.scrollTo @baseView.$el
		popup = @baseView.getPopup()
		popup.openPopup()
		popup.updateHeader 'Add New Service', false, true
		popup.bindEvent 'cancel', =>
			# bind single close event
			@baseView.cancelEditService true


module.exports = View

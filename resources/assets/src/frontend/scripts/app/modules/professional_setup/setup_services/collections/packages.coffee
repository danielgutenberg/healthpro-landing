Backbone = require 'backbone'
Numeral = require 'numeral'
Config = require 'app/modules/professional_setup/config/config'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		@parentModel = options.parentModel
		@getData()

	getData: ->
		packages = @parentModel.get('packages')
		return unless packages
		packages.forEach (model) =>
			# reccuring packages have rules - we don't want to add them here
			return if model.rules?.length
			@add
				id: model.package_id or model.id
				price: Numeral(model.price).value()
				number_of_visits: model.number_of_visits * 1

	createPackage: ->
		@add
			number_of_visits: _.difference(Config.NumberOfVisitsOptions, @pluck('number_of_visits'))[0]

module.exports = Collection

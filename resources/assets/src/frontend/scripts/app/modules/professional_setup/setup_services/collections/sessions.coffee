Backbone = require 'backbone'
Numeral = require 'numeral'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		@parentModel = options.parentModel
		@getData()

	getData: ->
		@parentModel.get('sessions').forEach (model) =>
			@add
				id: model.session_id or model.id
				price: Numeral(model.price).value()
				active: model.active ? 1
				duration: model.duration * 1
				packages: model.packages
				is_first_time: model.is_first_time
				title: model.title ? ''

	getSessionsWithPackages: -> @filter (session) -> session.get('packages').length > 0 && session.get('active') == 1

module.exports = Collection

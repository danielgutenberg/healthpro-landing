Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Select = require 'framework/select'
vexDialog = require 'vexDialog'
getUrl = require 'hp.url'
Config = require 'app/modules/professional_setup/config/config'

require 'backbone.stickit'
require 'backbone.validation'

require 'tooltipster'

class View extends Backbone.View

	events:
		'click [data-save-service]': 'saveService'
		'click [data-cancel-service]': 'cancelEditService'
		'click [data-open-service-form]': 'openServiceForm'
		'click [data-duplicate]': 'duplicate'
		'click [data-sub-toggle]': 'subToggle'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@hasChanges = false
		@errors = []

	afterInit: ->
		@bind()
		@addStickit()
		@render()
		@initValidation()

	bind: ->
		@listenToOnce @, 'rendered', =>
			@afterRender()
			@stickit()
			@

		@listenTo @model, 'error:generic', (errorMessage) =>
			@raiseGenericError errorMessage
			@

		@listenTo @model, 'error:alert', (errorMessage) =>

			# that's not the right way to check for the particular error message but
			# there's no other way to check if the error was raised because of monthly packages

			callback = null
			if errorMessage.indexOf('You cannot update the package details' > -1)
				callback = => @resetMonthlyPackagesSessions()

			if errorMessage.indexOf('You have already added this package' > -1)
				callback = => location.reload()

			@raiseAlertError errorMessage, callback
			@

		# track edit model changes
		@listenTo @model, 'change', =>
			@hasChanges = true
			@

		@listenTo @collections.locations, 'update location:updated', => @appendLocations(true)


	afterRender: ->
		@cacheDom()
		@appendLocations()
		@appendSubServices()
		@appendSessions()
		@appendPackagesReccuring()
		@appendGiftCertificates()
		@initTooltips()

	initTooltips: ->
		@$el.find('[data-tooltip]').tooltipster
			theme: 'tooltipster-dark'
			maxWidth: 200

	addStickit: ->
		@bindings =
			'[data-service-name]': 'name'
			'[name="location_ids"]':
				observe: 'location_ids'
				getVal: @onSetLocation
				initialize: ($el) =>
					@model.get('location_ids').forEach (location) -> $el.filter("[value='#{location}']").prop('checked', true)
			'[name="service_type_ids"]':
				observe: 'service_type_ids'
				selectOptions:
					collection: @collections.serviceTypes.getFlatTree()
					labelPath: 'label'
					valuePath: 'value'
				initialize: ($el) =>
					@selectSubServices = new Select $el,
						minimumResultsForSearch: 0
						templateResult: (service) ->
							return service.text if service.loading or !service.id
							return service.text if service.text.match '—'
							return "<strong>#{service.text}</strong>"
						templateSelection: (service) -> service.text
						escapeMarkup: (markup) -> markup
						dropdownClass: 'm-show_disabled'
					$el.on 'select2:select', -> $el.trigger 'focus.validation'
				getVal: @onSetSubServices
			'[name="certificates"]':
				observe: 'certificates'
				updateModel: false # set model value manually
				getVal: @onSetCertificates
				onGet: (val) -> +val
				initialize: ($el, model) ->
					$el.prop 'checked', model.get('certificates')
			'[name="description"]': 'description'

	onSetSubServices: ($el) =>
		values = $el.val()
		return [] unless values

		values = values.map (val) -> parseInt val

		# check for duplicate services
		duplicate = @getDuplicateServices values, true
		duplicateServiceIds = _.flatten duplicate.map (model) -> model.getServiceTypeIds()

		# update select and remove duplicate ids
		if duplicateServiceIds.length
			values = _.difference(values, duplicateServiceIds)
			$el.val values
			$el.trigger 'change'

		values

	getDuplicateServices: (values, duplicateAlert = true) ->
		duplicate = @collections.services.findDuplicateServices values, @model
		@duplicateAlert() if duplicate.length and duplicateAlert
		duplicate


	onSetCertificates: ($el, e) ->
		val = $el.prop('checked')
		boxval = $el.val()
		if boxval != 'on' && boxval != null
			val = if val then $el.val() else null

		if val?
			$el.prop 'checked', false
			if @collections.services.paymentsChecked
				@setCertificatesIfAllowed($el, val)
			else
				@showLoading()
				$.when(
					@collections.services.checkOnlinePayments()
				).then( =>
					@setCertificatesIfAllowed($el, val)
				)
		else
			+val

	setCertificatesIfAllowed: ($el, val) ->
		if @collections.services.hasOnlinePayments
			@model.set 'certificates', true
			$el.prop 'checked', true
			@hideLoading()
		else
			@model.set 'certificates', false
			$el.prop 'checked', false
			@showSetOnlinePaymentsPopup()

	showSetOnlinePaymentsPopup: ->
		vexDialog.confirm
			buttons: [
				$.extend {}, vexDialog.buttons.YES, {text: 'Set Up Online Payments'}
				$.extend {}, vexDialog.buttons.NO, {text: 'Cancel'}
			]
			message: 'Gift certificates can only be offered to your clients if you accept payments online.'
			callback: (value) =>
				unless value
					@hideLoading()
					return
				window.location = getUrl('dashboard-settings-payment')

	onSetLocation: ($el) =>
		values = []
		if $el.length > 1
			values = _.reduce($el, ((memo, el) ->
				checkbox = Backbone.$(el)
				if checkbox.prop('checked')
					memo.push parseInt(checkbox.val())
				memo
			), [])
		else
			value = $el.prop('checked')
			boxval = $el.val()
			if boxval != 'on' and boxval != null
				if value then values.push $el.val()

		id = @model.id
		duplicate = []
		double = false

		services = @collections.services.findDuplicateServices @model.getServiceTypeIds(), @model

		services.forEach (service) ->
			unless service.id == id
				duplicate = _.intersection(service.get('location_ids'), values)
				if duplicate.length > 0
					double = true

		if double is true
			duplicate.forEach (location) -> $el.filter('[value=' + location + ']').prop('checked', false)
			@duplicateAlert()
			return @model.get('location_ids')

		values

	duplicateAlert: ->
		vexDialog.alert
			message: 'You already offer that service in this location, please select a different service or location'
			buttons: [
				$.extend({}, vexDialog.buttons.YES, text: 'OK')
			]

	cacheDom: ->
		@$el.$service = @$el.find('[data-edit-service]')
		@$el.$locations = @$el.find('[data-edit-locations]')
		@$el.$sessions = @$el.find('[data-edit-sessions]')
		@$el.$packagesReccuring = @$el.find('[data-edit-packages-reccuring]')
		@$el.$giftCertificates = @$el.find('[data-edit-gift-certificates]')
		@$el.$serviceTypes = @$el.find('[data-edit-service-types]')
		@$el.$subServices = @$el.find('[data-edit-sub-services]')
		@$el.$error = @$el.find('[data-generic-error]')
		@$el.$sessionsError = @$el.find('[data-sessions-error]')

	appendSubServices: ->
		@$el.$subServices.html ProfessionalSetupServices.Templates.SubServices()

		if @model.get('service_type_ids').length > 0
			@$el.find('[data-sub-toggle="sub_services"]').trigger 'click'

		if @model.get('certificates')
			@$el.find('[data-sub-toggle="gift_certificates"]').trigger 'click'

		if @model.get('description')
			@$el.find('[data-sub-toggle="description"]').trigger 'click'

		@



	appendLocations: (delegateEvents = false) ->

		locations = @collections.locations.toJSON().map (item) ->
			switch item.location_type
				when 'office'
					item.icon = Config.LocationIconsSmall.office
				when 'home-visit'
					item.icon = Config.LocationIconsSmall.homeVisit
				when 'virtual'
					item.icon = Config.LocationIconsSmall.virtual
			item

		@$el.$locations.html ProfessionalSetupServices.Templates.Partials.LocationsField
			locations: locations

		if delegateEvents
			@delegateEvents()
			@stickit()

	appendSessions: ->
		$sessions = new ProfessionalSetupServices.Views.Sessions
			collection: @model.get('sessions')
			parentModel: @model
			baseView: @baseView
			parentView: @

		$sessions.render().$el.appendTo @$el.$sessions

	appendGiftCertificates: ->
		@$el.$giftCertificates.html ProfessionalSetupServices.Templates.Partials.GiftCertificates()

	appendPackagesReccuring: ->
		$packagesReccuring = new ProfessionalSetupServices.Views.PackagesReccuring
			collection: @model.get('packages_reccuring')
			parentModel: @model
			baseView: @baseView
			parentView: @

		$packagesReccuring.render().$el.appendTo @$el.$packagesReccuring

	saveService: ->
		@raiseGenericError null
		@validateData()

		return if @errors.length

		failed_locations = @checkDurations()
		if failed_locations.length
			@showDurationAlert(failed_locations)
		else
			@doSaveService()

	doSaveService: ->
		@showLoading()
		$.when(@model.save()).then(
			(response) =>
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: Config.Messages.updateSuccess
				@afterSave()
			, (error) =>
				@hideLoading()
		)

	validateData: ->
		@raiseSessionsError null
		@errors = []
		errors = @model.validate()
		@errors.push errors if errors?


		# check if no active sessiona and recurring packages available
		# display a message

		activeSessions = @model.getActiveSessions()
		recurringPackages = @model.get('packages_reccuring').models

		if !activeSessions.length and !recurringPackages.length
			msg = 'Please add at least one standard rate or monthly package'
			@raiseSessionsError 'Please add at least one standard rate or monthly package', 'warning'
			@errors.push msg

		# we need to validate only active sessions here
		# reccuring packages should be validated separately
		_.each activeSessions, (sessionModel) =>
			return if sessionModel.isEmpty()
			errors = sessionModel.validate()
			@errors.push errors if errors?
			_.each sessionModel.get('packages').models, (packageModel) =>
				return if packageModel.isEmpty()
				errors = packageModel.validate()
				@errors.push errors if errors?
				return
			return

		# validate reccuring packages
		_.each recurringPackages, (packageModel) =>
			return if packageModel.isEmpty()
			errors = packageModel.validate()
			@errors.push errors if errors?
			return

		@errors

	initValidation: ->
		Backbone.Validation.bind @,
			valid: (view, attr) =>
				@raiseError attr, null
			invalid: (view, attr, error) =>
				@raiseError attr, error

	raiseError: (attr, error) ->
		field = @$el.find('[name="' + attr + '"]')
		field_group = field.parents('.field--group')
		if error
			if field_group.length
				field_group.addClass 'm-error'
				field_group.find('.field--error').html error
			else
				field.parents('.field, .select').addClass 'm-error'
				field.prev('.field--error').html error

			field.on 'focus.validation', ->
				if field_group.length
					field_group.removeClass 'm-error'
				field.parents('.field, .select').removeClass 'm-error'
				field.off('focus.validation')
		else
			if field_group.length
				field_group.removeClass 'm-error'
			else
				field.parents('.field, .select').removeClass 'm-error'
				field.prev('.field--error').html ''

	raiseEditError: (message, $errorEl, type = 'error') ->
		if message
			$errorEl.removeClass 'm-hide'
			$errorEl.html "<div class='alert m-#{type} m-show m-standalone'><span>#{message}</span></div>"
		else
			$errorEl.addClass 'm-hide'
			$errorEl.html ''
		@

	raiseGenericError: (message = null, type = 'error') ->
		@raiseEditError message, @$el.$error, type
		@

	raiseSessionsError: (message = null, type = 'error') ->
		@raiseEditError message, @$el.$sessionsError, type
		@

	raiseAlertError: (message, callback = null) ->
		vexDialog.alert
			buttons: [
				$.extend({}, vexDialog.buttons.YES, text: 'OK')
			]
			message: message
			callback: -> callback() if callback

	beforeSave: ->
		@showLoading()

	checkDurations: ->
		locationsDurations = []
		_.each @model.get('location_ids'), (location_id) =>
			location = @collections.locations.find
				id: parseInt(location_id)

			locationsDurations.push
				max_duration: location.get('max_availabilities_duration')
				name: location.get('name')

		location_names = []
		_.each @model.get('sessions').models, (rateModel) =>
			_.each locationsDurations, (item) =>
				if rateModel.get('duration') > item.max_duration
					location_names.push item.name
			return false if location_names.length

		return location_names

	showDurationAlert: (location_names) ->
		message = '<p>Selected duration is longer than the availabilities set in following locations: <b>' + location_names.join(', ') + '</b></p>'
		message += '<p>You will not be searchable for this time slot.</p>'

		vexDialog.confirm
			buttons: [
				$.extend {}, vexDialog.buttons.YES, {text: 'Fix'}
				$.extend {}, vexDialog.buttons.NO, {text: 'Dismiss'}
			]
			message: message
			callback: (value) =>
				return if value
				@doSaveService()

	afterSave: ->
		@hideLoading()

	showLoading: (isSave = true) -> @baseView.showLoading(isSave)
	hideLoading: (isSave = true) -> @baseView.hideLoading(isSave)

	openServiceForm: (e) ->
		e?.preventDefault()
		@baseView.initServiceForm()

	cancelEditService: (scrollTop = false) ->
		@baseView.cancelEditServiceWithConfirmation => @baseView.cancelEditService(scrollTop)

	resetMonthlyPackagesSessions: ->
		return unless @initialModel
		initialPackages = @initialModel.get('packages_reccuring')
		@model.get('packages_reccuring').each (model) =>
			return unless model.get('id')
			model.set 'duration', initialPackages.get(model.get('id')).get('duration')
			model.set 'price', initialPackages.get(model.get('id')).get('price')
			model.set 'number_of_months', initialPackages.get(model.get('id')).get('number_of_months')
			model.set 'rules.max_weekly.value', initialPackages.get(model.get('id')).get('rules.max_weekly.value')

			return

	duplicate: ->
		@baseView.instances.services.forceAddService @initialModel
		@

	subToggle: (e) ->
		$btn = $(e.currentTarget)
		$btn.addClass 'm-hide'
		@$el.find('[data-sub="' + $btn.data('sub-toggle') +  '"]').removeClass 'm-hide'


	hasFeature: (feature) ->
		features = Dashboard.app.model.get('profile.features')
		return true unless _.isArray(features)
		_.indexOf(features, feature) > -1


module.exports = View

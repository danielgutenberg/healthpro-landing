Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'
require 'backbone.validation'
require 'backbone.stickit'

class View extends Backbone.View

	className: "professional_setup--listing--item m-service"

	events:
		'click [data-details-edit]': 'editService'
		'click [data-details-remove]': 'confirmRemoveService'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@bind()
		@initEventListeners()

	bind: ->
		@bindings =
			':el':
				classes:
					'm-introductory':
						observe: 'introductory'

			'[data-details-name]':
				observe: ['name', 'sessions', 'packages_reccuring', 'introductory']
				updateMethod: 'html'
				onGet: (values) ->
					sessionsInited = @model.get('sessions') instanceof ProfessionalSetupServices.Collections.Sessions
					if values[3] and sessionsInited
						html = values[1].first().get('title')
					else
						html = values[0]
					if sessionsInited
						if values[1].getSessionsWithPackages().length or values[2].length
							html += " <strong class='m-packages'>Packages Available</strong>"
					html
				classes:
					'm-certificates':
						observe: 'certificates'

			'[data-details-sub]':
				observe: 'service_type_ids'
				onGet: (val) =>
					names = val.map (id) => @collections.serviceTypes.get(id)?.get('name')

					names.join(', ')

			'[data-details-description]':
				observe: 'location_ids'
				onGet: (val) ->
					names = []
					_.each val, (locationId) =>
						location = @collections.locations.get(locationId)
						names.push location.get('name') if location
					names.join(', ')

	initEventListeners: ->
		@listenTo @collections.services, 'add remove', => @toggleRemoveButton()
		@

	render: ->
		@$el.html ProfessionalSetupServices.Templates.Service()
		@stickit()
		@cacheDom()
		@toggleRemoveButton()
		@

	cacheDom: ->
		@$el.$editContainer = @$el.find('[data-services-edit-container]')
		@$el.$removeBtn = @$el.find('[data-details-remove]')
		@

	cancelEdit: -> @$el.removeClass('m-edit')

	editService: ->
		@baseView.cancelEditServiceWithConfirmation => @forceEditService()
		@

	forceEditService: ->
		@baseView.initEditService @model, @
		@$el.addClass('m-edit')
		@

	confirmRemoveService: (e) ->
		e?.preventDefault()
		vexDialog.confirm
			buttons: [
				$.extend {}, vexDialog.buttons.YES, {text: 'Yes'}
				$.extend {}, vexDialog.buttons.NO, {text: 'Cancel'}
			]
			message: 'Are you sure you want to remove this service?'
			callback: (status) => @removeService() if status

	removeService: ->
		@baseView.showLoading()
		@baseView.cancelEditService()
		$.when(
			@model.remove()
		).then(
			=>
				@baseView.hideLoading()
				@parentView.services.splice @parentView.services.indexOf(@), 1
				@collections.services.trigger 'remove'
				@remove()
		).fail(
			=> @baseView.hideLoading()
		)

	toggleRemoveButton: ->
		if @collections.services.where({introductory: false}).length > 1 or @model.get('introductory')
			@$el.$removeBtn.removeClass 'm-hide'
		else
			@$el.$removeBtn.addClass 'm-hide'

module.exports = View

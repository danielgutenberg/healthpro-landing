Backbone = require 'backbone'
vexDialog = require 'vexDialog'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	tagName: 'dl'

	events:
		'click [data-period-remove]': 'removePeriod'

	initialize: (options) ->
		@bind()
		@initValidation()

		@model = options.model
		@collection = options.collection

	bind: ->
		@bindings =
			'[name="days"]':
				observe: 'days'
				onSet: (val) -> val.map (v) -> v * 1
				onGet: (val) -> val.map (v) -> "#{v}"

			'[name="from"]':
				selectOptions:
					collection: @collection.timeOptions()
					labelPath: 'label'
					valuePath: 'value'
				observe: 'from'

			'[name="until"]':
				selectOptions:
					collection: @collection.timeOptions()
					labelPath: 'label'
					valuePath: 'value'
				observe: 'until'

	render: ->
		@$el.html ProfessionalSetupLocations.Templates.LocationPeriod
			weekdays: @collection.weekdayOptions
		@delegateEvents()
		@stickit()
		@

	initValidation: ->
		Backbone.Validation.bind @,
			valid: (view, attr) => @raiseError attr, null
			invalid: (view, attr, error) => @raiseError attr, error

	raiseError: (attr, error) ->
		field = @$el.find("[name='#{attr}']")
		return unless field.length

		fieldParent = field.closest('.field, .weekdays, .select')
		fieldError  = fieldParent.find('.field--error')

		if error
			fieldParent.addClass 'm-error'
			fieldError.html error
			field.on 'focus.validation', ->
				fieldParent.removeClass('m-error')
				field.off('focus.validation')
		else
			fieldParent.removeClass 'm-error'
			fieldError.html ''

	resetErrors: ->
		_.each @model.toJSON(), (value, attr) => @raiseError attr, null

	removePeriod: (e) ->
		e?.preventDefault()
		if @model.get('days').length or @model.get('id')
			vexDialog.confirm
				message: 'Are you sure you would like to delete this period?'
				callback: (val) =>
					return unless val
					@close()
		else
			@close()

	close: ->
		@model.set('id', null).destroy()
		@remove()

module.exports = View

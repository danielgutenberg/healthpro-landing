Backbone = require 'backbone'
Numeral = require 'numeral'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		@parentModel = options.parentModel
		@getData()

	getData: ->
		# find all the packages with rules
		_.each @parentModel.get('unformatted_sessions'), (session) =>
			_.each session.packages, (model) =>
				return unless model.rules?.length
				@add
					id: model.package_id or model.id
					sessionId: session.session_id or session.id
					price: Numeral(model.price).value()
					rules: @formatRules model.rules
					duration: session.duration
					number_of_months: model.number_of_months ? 1
					is_saved: true
					# number_of_visits: -1

	createPackage: -> @add {}

	formatRules: (rules) ->
		formatted = {}
		_.each rules, (rule) -> formatted[rule.rule] =
			value: rule.value
			id: rule.id
		formatted

	getFormattedPackages: ->
		models = @filter (model) -> model.price isnt ''
		arr = []
		_.each models, (model) -> arr.push model.formatPackage()
		arr

module.exports = Collection

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

ServicesSetup = require 'app/modules/professional_setup/setup_services'
LocationsSetup = require 'app/modules/professional_setup/setup_locations'

Tabs = require 'framework/tabs'
Device = require 'utils/device'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@render()
		@cacheDom()
		@initParts()

	render: ->
		@$el.html ProfessionalSetupLocationsServices.Templates.Base()

	initParts: ->
		@setupServices()
		@setupLocations()

		# share locations and services collections between these two objects
		@services.collections.locations = @locations.collections.locations
		@locations.collections.services = @services.collections.services

		# init app
		@services.init()
		@locations.init()

		if Device.isMobile()
			new Tabs @$el.find('[data-tabs]')

	cacheDom: ->
		@$el.$locations = $('[data-professional-setup-locations]')
		@$el.$services  = $('[data-professional-setup-services]')

	setupServices: ->
		@locations = new LocationsSetup.App
			rootApp		: @
			$container	: @$el.$locations
		@locations.setup()

	setupLocations: ->
		@services = new ServicesSetup.App
			rootApp		: @
			$container	: @$el.$services
		@services.setup()

	hasPopup: -> Device.isMobile()

	getPopup: ->
		return @popupView if @popupView
		@popupView = new ProfessionalSetupLocationsServices.Views.Popup()


module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
NumericInput = require 'framework/numeric_input'
Config = require 'app/modules/professional_setup/config/config'

require 'backbone.validation'
require 'backbone.stickit'

class View extends Backbone.View

	className: 'professional_setup--sessions--item'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addValidation()
		@bind()
		@stickit()

	bind: ->
		@bindings =
			'[name="title"]':
				observe: 'title'
				onGet: (val) -> val.trim()
				onSet: (val) -> val.trim()
			'[name="is_first_time"]':
				observe: 'is_first_time'
				onGet: (val) -> !!val
				onSet: (val) -> val * 1
			'[name="price"]':
				observe: 'price'
				onGet: (val) -> val * 1
				onSet: (val) -> @price.parseValue val
				initialize: ($el) ->
					@price = new NumericInput $el,
						decimal: '.'
						leadingZeroCheck: false
						initialParse: false
						parseOnBlur: false
			'[name="duration"]':
				observe: 'duration'
				selectOptions:
					collection: Config.SessionDurationOptions
					defaultOption:
						label: 'Select duration'
						value: null

module.exports = View

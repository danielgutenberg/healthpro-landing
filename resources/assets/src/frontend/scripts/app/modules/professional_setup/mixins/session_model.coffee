getApiRoute						= require 'hp.api'

PackagesCollection 				= require('app/modules/professional_setup/setup_services/collections/packages')
PackageModel 					= require('app/modules/professional_setup/setup_services/models/package')

module.exports =

	initValidation: ->
		@validation =
			price: (value, field, computedState) ->
				# introductory sessions can be free of charge
				return if computedState.introductory
				value = +value
				return 'Please enter the price' if !value
				return 'Price must be $10 or more' if value < 10
			duration:
				pattern: 'number'
				msg: 'Please select duration'
				required: true
			title: (value, field, computedState) ->
				return unless computedState.introductory
				return 'Please enter a name for the introductory session' unless value

	initIntroductory: ->
		@set 'introductory', @collection.parentModel.get('introductory')

	initPackages: ->
		return if @get('packages') instanceof PackagesCollection
		@set 'packages', new PackagesCollection 0,
			model: PackageModel
			parentModel: @

	toJSON: ->
		attrs = _.clone @attributes
		if attrs.packages instanceof PackagesCollection
			attrs.packages = attrs.packages.toJSON()
		attrs.price = attrs.price * 1
		attrs

	remove: ->
		id = @get('id')
		unless id
			@destroy()
			return true

		$.ajax
			url: getApiRoute('ajax-provider-service-session-delete', {providerId: 'me', serviceSessionId: id})
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: =>
				@set 'id', null
				@destroy()


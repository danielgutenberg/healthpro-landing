Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

SessionMixins = require 'app/modules/professional_setup/mixins/session_model'

class Model extends Backbone.Model

	defaults:
		title: ''
		price: null
		duration: null
		active: 1
		is_first_time: 0
		packages: []

	initialize: ->
		Cocktail.mixin @, SessionMixins
		@initValidation()
		@initIntroductory()
		@initPackages()

module.exports = Model

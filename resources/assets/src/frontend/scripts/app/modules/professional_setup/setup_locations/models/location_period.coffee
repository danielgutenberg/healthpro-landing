Backbone = require 'backbone'
Moment = require 'moment'

class Model extends Backbone.Model

	defaults:
		days: []
		from: null
		until: null

	initialize: ->
		@addValidationRules()

	addValidationRules: ->
		@validation =
			days: @validateDays
			from: @validateFrom
			until: @validateUntil

	validateDays: (value) ->
		return 'Please select your work days' unless value.length

	validateFrom: (value, attr, computedState) ->
		return 'Set from time' unless value

		from = Moment(value, 'HH:mm')
		to   = Moment(computedState.until, 'HH:mm')

		return 'From time should start earlier' if from.isAfter(to) or from.isSame(to)

	validateUntil: (value) ->
		return 'Set until time' unless value

module.exports = Model

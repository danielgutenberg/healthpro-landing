Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Config = require 'app/modules/professional_setup/config/config'
ScrollTo = require 'utils/scroll_to_element'

class View extends Backbone.View

	className: 'professional_setup--inner m-dashboard'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@instances = {}

		@bind()
		@render()
		@cacheDom()
		@preload()
		@initServices()

	cacheDom: ->
		@$body = $('body')
		@$el.$main = @$el.find('[data-view-main]')
		@$el.$loading = @$el.find('.loading_overlay')

	render: -> @$el.html(ProfessionalSetupServices.Templates.Base()).appendTo @$container

	bind: ->
		@listenToOnce @, 'data:loaded', @hideLoading

	initServices: ->
		@instances.services = new ProfessionalSetupServices.Views.Services
			baseView: @
			collections: @collections

	preload: ->
		@showLoading()

		$.when(
			# wait for profile to load first.
			Dashboard.app.model.getProfile()
		).then(
			=>
				$.when(
					@collections.serviceTypes.getServiceTypes(),
					@collections.locations.getLocations()
				).then(
					=> @collections.services.getServices()
				).then(
					=> @trigger 'data:loaded'
				)
		)

	saveService: ->
		return unless @instances.edit_service
		@instances.edit_service.saveService()

	removeEditService: ->
		if @instances.edit_service
			@instances.edit_service.remove()
			@instances.edit_service = null
		@

	cancelEditService: (scrollTop = false) ->
		@removeEditService()
		@instances.services.cancelEdit()

		@scrollTo @$el if scrollTop

		if @hasPopup()
			@closePopup()

	cancelEditServiceWithConfirmation: (callback) ->
		if @instances.edit_service? and @instances.edit_service.hasChanges
			vexDialog.confirm
				message: Config.Messages.hasChanges
				callback: (value) =>
					callback() if value
					return
		else
			callback()
		@

	initEditService: (model, view) ->
		@cancelEditService()
		ViewClass = do ->
			if model.get('introductory')
				ProfessionalSetupServices.Views.EditIntroductorySession
			else
				ProfessionalSetupServices.Views.EditService

		@instances.edit_service = new ViewClass
			collections: @collections
			baseView: @
			initialModel: model
			parentView: view
			isPopup			: @hasPopup()
			$container		: do =>
				if @hasPopup()
					@getPopup().getContainer()
				else
					view.$el.$editContainer

	initAddService: (fromModel = null) ->
		@cancelEditService()
		@instances.edit_service = new ProfessionalSetupServices.Views.AddService
			collections: @collections
			baseView: @
			fromModel: fromModel
			isPopup			: @hasPopup()
			$container		: do =>
				if @hasPopup()
					@getPopup().getContainer()
				else
					@$el.find('[data-services-add-container]')

	initAddIntroductorySession: ->
		@cancelEditService()
		@instances.edit_service = new ProfessionalSetupServices.Views.AddIntroductorySession
			collections		: @collections
			baseView		: @
			isPopup			: @hasPopup()
			$container		: do =>
				if @hasPopup()
					@getPopup().getContainer()
				else
					@$el.find('[data-services-add-container]')

	initSelectServiceType: ->
		return unless @hasPopup()
		@cancelEditService()
		@instances.edit_service = new ProfessionalSetupServices.Views.SelectServiceType
			collections		: @collections
			baseView		: @
			isPopup			: true
			$container		: @getPopup().getContainer()

	initServiceForm: ->
		@serviceForm = new ProfessionalSetupServices.Views.ServiceForm
			baseView: @

	removeServiceForm: ->
		if @serviceForm
			@serviceForm.remove()
			@serviceForm = null

	scrollTo: ($el = null, offset = 100) ->
		ScrollTo (if $el is null then @$el else $el), offset
		@

	hasPopup: -> @app.rootApp?.hasPopup()

	closePopup: ->
		if @hasPopup()
			@getPopup().closePopup()
		return

	getPopup: -> @app.rootApp?.getPopup()

	showLoading: (isSave = false) ->
		if isSave and @hasPopup()
			@getPopup().showLoading()
		else
			@$el.$loading.removeClass('m-hide')

	hideLoading: (isSave = false) ->
		if isSave and @hasPopup()
			@getPopup().hideLoading()
		else
			@$el.$loading.addClass('m-hide')



module.exports = View

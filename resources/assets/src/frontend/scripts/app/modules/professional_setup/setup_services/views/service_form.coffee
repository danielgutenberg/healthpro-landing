Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Config = require 'app/modules/professional_setup/config/config'
vexDialog = require 'vexDialog'

class View extends Backbone.View

	attributes:
		'class': 'popup professional_setup--service_form'
		'id': 'service_form_popup'
		'data-popup' : 'service_form'

	events:
		'click [data-close]': 'removePopup'
		'click [data-form-submit]': 'submitForm'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@popupName = 'service_form'
		@model = new ProfessionalSetupServices.Models.ServiceForm

		@addValidation()
		@addStickit()
		@render()
		@cacheDom()
		@initPopup()

	addStickit: ->
		@bindings =
			'[name="email"]' : 'email'
			'[name="service"]'  : 'service'

	render: ->
		@$el.html ProfessionalSetupServices.Templates.ServiceForm
		$('body').append @$el
		@stickit()

	cacheDom: ->
		@$el.$loading = @$el.find('.loading_overlay')

	initPopup: ->
		window.popupsManager.addPopup @$el
		window.popupsManager.openPopup 'service_form'

	removePopup: ->
		console.log '1'
		window.popupsManager.removePopup 'service_form'
		@baseView.removeServiceForm()

	showLoading: -> @$el.$loading.removeClass 'm-hide'
	hideLoading: -> @$el.$loading.addClass 'm-hide'

	submitForm: (e) ->
		e.preventDefault() if e?

		return if @model.validate()?

		@showLoading()
		$.when( @model.save() )
			.then(
				(res) =>
					@removePopup()
					@hideLoading()
					vexDialog.alert
						message: Config.Messages.csRequestSuccess
						buttons: [
							$.extend({}, vexDialog.buttons.YES, text: 'OK')
						]

				, (res) =>
					@hideLoading()
			)

module.exports = View

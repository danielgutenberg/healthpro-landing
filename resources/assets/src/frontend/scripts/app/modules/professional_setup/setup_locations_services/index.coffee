Handlebars = require 'handlebars'
Device = require 'utils/device'

require '../../../../../styles/modules/professional_setup.styl'

window.ProfessionalSetupLocationsServices = ProfessionalSetupLocationsServices =
	Views:
		Base: require('./views/base')
		Popup: require('./views/popup')
	Templates:
		Base: Handlebars.compile do ->
			if Device.isMobile()
				require('text!./templates/mobile/base.html')
			else
				require('text!./templates/desktop/base.html')

class ProfessionalSetupLocationsServices.App
	constructor: (options) ->
		GMaps.load().ready =>
			@view = new ProfessionalSetupLocationsServices.Views.Base
				el: options.el

if $('[data-professional-setup-locations-services]').length
	new ProfessionalSetupLocationsServices.App
		el: '[data-professional-setup-locations-services]'

module.exports = ProfessionalSetupLocationsServices

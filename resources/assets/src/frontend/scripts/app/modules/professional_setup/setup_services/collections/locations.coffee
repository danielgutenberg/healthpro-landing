Backbone = require 'backbone'
getApiRoute = require 'hp.api'

Moment = require 'moment'

class Collection extends Backbone.Collection

	getLocations: ->
		$.ajax
			url: getApiRoute('ajax-location-get', {providerId: 'me'})
			method: 'get'
			type: 'json'
			data:
				service_area_unit: 'mile'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.each response.data, (model) => @addLocation model
				@trigger 'data:ready'

	addLocation: (item) =>
		availabilities_durations = []
		_.each item.availabilities, (availability) ->
			availabilities_durations.push Moment(availability.until, 'HH:mm').diff(Moment(availability.from, 'HH:mm'), 'minutes')

		@push
			id: item.id
			name: item.name
			location_type: item.location_type
			max_availabilities_duration: _.max availabilities_durations

module.exports = Collection

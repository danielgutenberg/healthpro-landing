Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'click [data-locations-add-btn]': 'addLocation'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@locations = []

		@render()
		@bind()
		@cacheDom()

	bind: ->
		@listenToOnce @baseView, 'data:loaded', =>
			@collections.locations.each (model) => @renderLocation model
			# after initial load listen to the add event
			@listenTo @collections.locations, 'add', @renderLocation
		@

	render: ->
		@$el.html(ProfessionalSetupLocations.Templates.Locations()).appendTo @baseView.$el.$main
		@

	cacheDom: ->
		@$el.list = @$el.find('[data-locations-list]')
		@$el.$addBtn = @$el.find('[data-locations-add-btn]')
		@

	renderLocation: (location) ->
		@locations.push view = new ProfessionalSetupLocations.Views.Location
			model: location
			collections: @collections
			baseView: @baseView
			parentView: @
		view.render().$el.appendTo @$el.list
		@

	addLocation: (e) ->
		e?.preventDefault()
		return if @$el.$addBtn.hasClass('m-edit')

		@baseView.cancelEditLocationWithConfirmation => @forceAddLocation()
		@

	forceAddLocation: ->
		@baseView.initAddLocation()
		@$el.$addBtn.addClass('m-edit')
		@

	cancelEditAddLocation: ->
		@$el.$addBtn.removeClass('m-edit')
		@

	cancelEdit: ->
		@cancelEditAddLocation()
		_.each @locations, (locationView) ->
			locationView.cancelEdit()
			return
		@

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
require 'backbone.validation'
require 'backbone.stickit'

class View extends Backbone.View

	className: "professional_setup--service_type"

	events:
		'click [data-service-type-services-add-btn]'		: 'addService'
		'click [data-service-type-introductory-add-btn]'	: 'addIntroductorySession'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@render()

	render: ->
		@$el.html ProfessionalSetupServices.Templates.SelectServiceType()
		@$el.appendTo @$container
		@afterRenderPopup()
		@trigger 'rendered'
		@

	afterRenderPopup: ->
		@baseView.scrollTo @baseView.$el
		popup = @baseView.getPopup()
		popup.openPopup()
		popup.updateHeader 'Select Service Type', false, true
		popup.bindEvent 'cancel', =>
			# bind single close event
			@baseView.cancelEditService true

	addService: (e) ->
		e?.preventDefault()
		@baseView.instances.services.addService()

	addIntroductorySession: ->
		e?.preventDefault()
		@baseView.instances.services.addIntroductorySession()

module.exports = View

BaseEditIntroductorySessionView  = require './abstract/abstract_edit_introductory'
Config = require 'app/modules/professional_setup/config/config'

class View extends BaseEditIntroductorySessionView

	initialize: (options) ->
		super(options)

		@model = new ProfessionalSetupServices.Models.Service
			service_type_id: Config.IntroductoryService.id
			name: Config.IntroductoryService.name
			introductory: true

		if @collections.locations.length is 1
			@model.set 'location_ids', @collections.locations.pluck('id')

		# add new introductory session
		@model.get('sessions').add
			title: 'Introductory Session'

		@afterInit()

	remove: ->
		unless @baseView.hasPopup()
			@$container.addClass 'm-hide'
		super

	render: ->
		@$el.html ProfessionalSetupServices.Templates.AddIntroductorySession()
		@$el.appendTo @$container

		if @baseView.hasPopup()
			@afterRenderPopup()
		else
			@$container.removeClass 'm-hide'
			@baseView.scrollTo @$container

		@trigger 'rendered'
		@

	afterSave: ->
		super
		@collections.services.push @model
		@baseView.cancelEditService true
		@

	afterRenderPopup: ->
		@baseView.scrollTo @baseView.$el
		popup = @baseView.getPopup()
		popup.openPopup()
		popup.updateHeader 'Add Introductory Session', false, true
		popup.bindEvent 'cancel', =>
			# bind single close event
			@baseView.cancelEditService true

module.exports = View

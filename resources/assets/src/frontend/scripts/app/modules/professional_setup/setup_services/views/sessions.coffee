Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	events:
		'click [data-sessions-add]': 'createSession'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@sessions = []

		@setOptions options
		@bind()

	bind: ->
		@listenTo @collection, 'add', @addSession
		@listenTo @collection, 'remove', @checkCollection
		@listenTo @, 'rendered', =>
			@cacheDom()
			@renderAllSessions()
			@checkCollection()

	renderAllSessions: ->
		# render only active sessions
		@collection.models.forEach (model) => @addSession model if model.get('active')

	checkCollection: ->
#		@createSession() unless @collection.length

	cacheDom: ->
		@$el.$list = @$el.find('[data-sessions]')

	render: ->
		@$el.html ProfessionalSetupServices.Templates.Sessions()
		@trigger 'rendered'
		@

	createSession: ->
		@collection.add {}
		$(document).trigger 'popup:center'

	addSession: (model) ->
		@parentView.raiseSessionsError null

		@sessions.push session = new ProfessionalSetupServices.Views.Session
			model: model
			serviceModel: @parentModel
			collection: @collection
			parentView: @
			baseView: @baseView

		session.render().$el.appendTo @$el.$list

module.exports = View

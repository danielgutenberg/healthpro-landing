SessionsCollection 				= require('app/modules/professional_setup/setup_services/collections/sessions')
SessionModel 					= require('app/modules/professional_setup/setup_services/models/session')

PackagesReccuringCollection 	= require('app/modules/professional_setup/setup_services/collections/packages_reccuring')
PackageRecurringModel			= require('app/modules/professional_setup/setup_services/models/package_reccuring')

module.exports =

	initSessions: ->
		return if @get('sessions') instanceof SessionsCollection

		# cache unformatted sessions. We'll grab the reccuring packages from this array
		@set 'unformatted_sessions', @get('sessions') unless @has('unformatted_sessions')

		@set 'sessions', new SessionsCollection 0,
			model: SessionModel
			parentModel: @

		# store reccuring packages in the service model
		# because we want to render them separately from the sessions
		# they will be combined with the sessions on save
		@set 'packages_reccuring', new PackagesReccuringCollection 0,
			model: PackageRecurringModel
			parentModel: @

		@listenTo @get('sessions')			 , 'change', => @trigger 'change'
		@listenTo @get('packages_reccuring') , 'change', => @trigger 'change'
		@

	# this method collects all the standard sessions for the service
	# and also all the sessions with reccuring packages
	# sometimes we can have an inactive session with reccuring packages
	getSessionsJson: ->
		sessions = _.reject @get('sessions').toJSON(), {price: ''}

		packagesReccuring = @get('packages_reccuring').getFormattedPackages()

		# for every reccuring package find a session with same duration
		# append package to the session if found or
		# create a new inactive session and fill it with the package
		_.each packagesReccuring, (packageReccuring) ->
			# we always keep the session id of the package even if the regular rate associated with it
			# changed the time.  So if the duration of the regular rate changes, we create it into a new
			# model without the id and the package remains with the old session id

			session = _.findWhere sessions,
				id: packageReccuring.sessionId

			if session?
				if session.duration != packageReccuring.duration
					session.id = null
					sessions.push
						id: packageReccuring.sessionId
						active: 0
						duration: packageReccuring.duration
						price: 0
						is_first_time: 0
						packages: [packageReccuring]
						title: ''

				else
					session.packages.push packageReccuring
			else
				session = _.findWhere sessions,
					duration: packageReccuring.duration
					is_first_time: 0

				if session?
					session.packages.push packageReccuring
				else
					sessions.push
						active: 0
						duration: packageReccuring.duration
						price: 0
						is_first_time: 0
						packages: [packageReccuring]
						title: ''

		# now we need to double check if we have sessions with same duration.
		# we can have them when we had an inactive session from the backend and
		# then we've added a new active session with the same duration.
		# if that happened - merge them
		@mergeDuplicateSessions sessions

		# remove the params we don't need for saving
		sessions.map (session) -> _.omit session, 'introductory'

	# a bit messy implementation
	# @todo refactor it later
	mergeDuplicateSessions: (sessions) ->

		# group sessions by duration
		grouped = _.groupBy sessions, (session) -> session.duration

		_.each grouped, (sess) ->
			return if sess.length < 2

			primarySession = _.remove sess, (s) -> s.id
			return unless primarySession.length and sess.length

			primarySession = primarySession[0]
			duplicatedSession = sess[0]

			sessions = _.without sessions, primarySession, duplicatedSession

			primarySession.price = duplicatedSession.price if duplicatedSession.price
			primarySession.active = duplicatedSession.active if duplicatedSession.active
			primarySession.packages = primarySession.packages.concat duplicatedSession.packages

			sessions.push primarySession

		# clean empty sessions after merge and return
		_.filter sessions, (session) -> !(session.active is 0 and session.packages.length is 0)

	getActiveSessions: -> @get('sessions').where active: 1

	getServiceTypeIds: -> [@get('service_type_id')].concat @get('service_type_ids')

	raiseAlertErrorFromResponse: (responseJson) ->
		@trigger 'error:alert', responseJson.errors.error.messages[0] if responseJson.errors?.error?.messages

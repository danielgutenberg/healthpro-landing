Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		price: ''
		number_of_visits: 2

	validation:
		price:
			pattern: 'number'
			min: 10
			required: true
		number_of_visits:
			pattern: 'number'
			min: 1
			max: 100

	remove: ->
		id = @get('id')
		unless id
			@destroy()
			return true

		$.ajax
			url: getApiRoute('ajax-provider-service-session-package-delete', {providerId: 'me', packageId: id})
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: =>
				@set 'id', null
				@destroy()


module.exports = Model

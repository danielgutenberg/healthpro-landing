Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Moment = require 'moment'
Config = require 'app/modules/professional_setup/config/config'

class Collection extends Backbone.Collection

	initialize: ->
		@xhr = null

	getLocations: ->
		unless @xhr
			@xhr = $.ajax
				url: getApiRoute('ajax-location-get', { providerId: 'me' })
				method: 'get'
				type: 'json'
				data:
					service_area_unit: Config.ServiceAreaUnit
				contentType: 'application/json; charset=utf-8'
				success: (response) =>
					response.data.forEach (item) => @push @formatLocation(item)
		@xhr

	formatLocation: (item) ->
		location =
			id: item.id
			name: item.name
			availabilities: @formatAvailabilities(item.availabilities)
			location_type_id: item.location_type_id
			location_type: item.location_type
			service_area: item.service_area
			padding_time: item.padding_time
			max_availabilities_duration: do ->
				availabilities_durations = item.availabilities.map (availability) ->
					Moment(availability.until, 'HH:mm').diff(Moment(availability.from, 'HH:mm'), 'minutes')
				_.max availabilities_durations

		# for virtual location type the address is the note
		if item.address.address_id
			location.address = item.address.address
			location.address_id = item.address.address_id
			location.postal_code = item.address.postal_code
			location.country_code = item.address.country
			location.city = item.address.city
			location.province = item.address.province
			location.address_label = item.address.address_label
			location.timezone = item.timezone
			location.address_line_two = item.address.address_line_two
		else
			location.note = item.address.address
			location.timezone = item.timezone

		location

	formatAvailabilities: (items) ->
		formatted = {}
		_.each items, (item) ->
			unless formatted["#{item.from}_#{item.until}"]?
				formatted["#{item.from}_#{item.until}"] = []
			formatted["#{item.from}_#{item.until}"].push item.day

		availabilities = []
		_.each formatted, (days, k) =>
			times = k.split('_')
			availabilities.push
				days: days
				from: times[0]
				until: times[1]

		availabilities

	isUnique: (modelId, field, value) ->
		models 		= _.reject @toJSON(), {id: modelId}
		duplicate 	= _.where models, "#{field}": value
		duplicate.length is 0

module.exports = Collection

DeepModel = require 'backbone.deepmodel'
getApiRoute = require 'hp.api'

class Model extends DeepModel

	defaults: ->
		price: ''
		rules:
			max_weekly:
				value: null
		duration: null
		number_of_visits: -1
		number_of_months: null

	validation:
		price:
			pattern: 'number'
			min: 10
			required: true
			msg: 'Price must be $10 or more'
		duration:
			required: true
			msg: 'Please select duration'
		number_of_months:
			min: 1
			max: 12
			msg: 'Please select number of months'
		'rules.max_weekly.value':
			min: 0
			max: 20
			msg: 'Please select number of visits'

	remove: ->
		id = @get('id')
		unless id
			@destroy()
			return true

		$.ajax
			url: getApiRoute('ajax-provider-service-session-package-delete', {providerId: 'me', packageId: id})
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: =>
				@set 'id', null
				@destroy()

	formatPackage: ->
		formatted = @toJSON()
		formatted.rules = []

		_.each @get('rules'), (val, rule) ->
			formattedRule =
				rule: rule
				value: val.value
			formattedRule.id = val.id if val.id?
			formatted.rules.push formattedRule

		formatted


module.exports = Model

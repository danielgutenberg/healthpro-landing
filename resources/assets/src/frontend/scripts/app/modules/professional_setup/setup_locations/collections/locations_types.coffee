Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Config = require 'app/modules/professional_setup/config/config'

class Collection extends Backbone.Collection

	getLocationsTypes: ->
		$.ajax
			url: getApiRoute('ajax-location-types-get')
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				response.data.forEach (item) =>
					switch item.type
						when 'office'
							item.description = Config.LocationDescriptions.office
							item.icon = Config.LocationIcons.office
						when 'home-visit'
							item.description = Config.LocationDescriptions.homeVisit
							item.icon = Config.LocationIcons.homeVisit
						when 'virtual'
							item.description = Config.LocationDescriptions.virtual
							item.icon = Config.LocationIcons.virtual
					@push item
				@trigger 'data:ready'

module.exports = Collection

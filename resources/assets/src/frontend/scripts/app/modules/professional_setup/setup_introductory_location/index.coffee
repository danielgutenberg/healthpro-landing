Handlebars = require 'handlebars'
Backbone = require 'backbone'

window.ProfessionalSetupIntroductoryLocation =
	Views:
		Base: require('./views/base')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')

# extend module with events
_.extend ProfessionalSetupIntroductoryLocation, Backbone.Events

class ProfessionalSetupIntroductoryLocation.App
	constructor: (options) ->
		@$container = options.$container
		@base 			= options.base
		@rootApp		= @base.rootApp

		@collections 	=
			locationsTypes	: @rootApp.locations.collections.locationsTypes
			locations		: @rootApp.locations.collections.locations

		@view = new ProfessionalSetupIntroductoryLocation.Views.Base
			$container	: options.$container
			collections	: @collections
			model		: new ProfessionalSetupLocations.Models.Location()

	close: ->
		@view.close?()
		@view.remove?()

	validate: -> @view.validateData()

	saveLocation: -> @view.saveLocation()

module.exports = ProfessionalSetupIntroductoryLocation

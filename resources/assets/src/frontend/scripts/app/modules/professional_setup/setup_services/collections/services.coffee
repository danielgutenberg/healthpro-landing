Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Config = require 'app/modules/professional_setup/config/config'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		@paymentsChecked = false

	getServices: ->
		$.ajax
			url: getApiRoute('ajax-profile-provider-services', {providerId: 'me'})
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.forEach response.data, (item) => @addService item
				@trigger 'data:ready'

	addService: (item) ->
		# temp solution for introductory session in this branch
		return if item.name is ""

		primaryServiceId = _.findWhere(item.sub_services, {name: item.name}).id
		subServiceIds = _.without(_.pluck(item.sub_services, 'id'), primaryServiceId)

		@add
			id: item.service_id
			name: item.name
			sessions: item.sessions
			introductory: primaryServiceId is Config.IntroductoryService.id
			location_ids: _.pluck(item.locations, 'id')
			service_type_id: primaryServiceId
			service_type_ids: subServiceIds
			certificates: +item.certificates
			description: item.description

	getWithLocation: (locationId) ->
		_.filter @models, (model) ->
			_.indexOf(model.get('location_ids'), locationId) > -1

	# for now we only check for location
	modelsWithoutLocations: -> @filter (model) -> model.get('location_ids').length is 0

	findDuplicateServices: (serviceIds, serviceModel) ->
		return [] unless serviceIds

		serviceIds = [serviceIds] unless Array.isArray(serviceIds)

		duplicateServices = @filter (model) ->
			return false if model.id is serviceModel.id
			_.intersection(serviceIds, model.getServiceTypeIds()).length > 0

		duplicateServices

	checkOnlinePayments: ->
		$.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			success: (res) =>
				@paymentsChecked = true
				unless res.data.accepted_payment_methods is 'in_person'
					@hasOnlinePayments = true

module.exports = Collection

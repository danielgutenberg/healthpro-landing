Backbone = require 'backbone'
rgb2hex = require 'utils/rgb2hex'
vexDialog = require 'vexDialog'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	className: "professional_setup--listing--item m-location"

	events:
		'click [data-details-edit]': 'editLocation'
		'click [data-details-remove]': 'confirmRemoveLocation'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@$el.addClass 'm-' + @model.getLocationType()

		@bind()
		@initEventListeners()

	bind: ->
		@bindings =
			'[data-details-name]':
				observe: ['name', 'service_area']
				onGet: (values, options) ->
					value = values[0]
					if options.view.model.getLocationType() is 'home-visit'
						value += ' (' + values[1] + ' mile' + (if values[1] > 1 then 's' else '') + ')'
					value

			'[data-details-description]':
				observe: ['address', 'note', 'location_type_id', 'address_label', 'address_line_two']
				updateMethod: 'html'
				onGet: (value, options) -> options.view.model.getFormattedAddressHtml()

			'[data-details-dates]':
				observe: 'availabilities'
				onGet: (value) ->
					return unless value.models
					value.weekdayLabels().join(', ')

	initEventListeners: ->
		@listenTo @model, 'ajax:error', (error) =>
			@baseView.raiseError(null)
			@baseView.raiseError error

		# make sure we have loaded location types
		@listenTo @collections.locationsTypes, 'data:ready', =>
			@model.trigger 'change:location_type_id'

		@listenTo @collections.locations, 'add remove', => @toggleRemoveButton()
		@

	cacheDom: ->
		@$el.$editContainer = @$el.find('[data-locations-edit-container]')
		@$el.$removeBtn = @$el.find('[data-details-remove]')
		@

	render: ->
		@$el.html ProfessionalSetupLocations.Templates.Location
		@stickit()
		@cacheDom()
		@toggleRemoveButton()
		@

	cancelEdit: ->
		@$el.removeClass 'm-edit'
		@

	editLocation: ->
		@baseView.cancelEditLocationWithConfirmation => @forceEditLocation()
		@

	forceEditLocation: ->
		@baseView.initEditLocation @model, @
		@$el.addClass('m-edit')
		@

	confirmRemoveLocation: (e) ->
		e?.preventDefault()
		@baseView.raiseError null

		if @collections.services?
			# check if the location is used in the services
			services = @collections.services.getWithLocation(@model.get('id'))
			if services.length
				servicesNames = services.map (service) -> service.get('name')
				@baseView.raiseError "Prior to deleting this location you must first remove the services offered there: " + servicesNames.join(', ')
				return

		vexDialog.confirm
			buttons: [
				$.extend {}, vexDialog.buttons.YES, {text: 'Yes'}
				$.extend {}, vexDialog.buttons.NO, {text: 'Cancel'}
			]
			message: 'Are you sure you want to remove this location?'
			callback: (status) => @removeLocation() if status

	removeLocation: ->
		@baseView.showLoading()
		@baseView.cancelEditLocation()
		$.when(
			@model.remove()
		).then(
			=>
				@baseView.hideLoading()
				@parentView.locations.splice(@parentView.locations.indexOf(@), 1)
				@collections.locations.trigger 'remove'
				@remove()
		).fail(
			=> @baseView.hideLoading()
		)

	toggleRemoveButton: ->
		if @collections.locations.length > 1
			@$el.$removeBtn.removeClass 'm-hide'
		else
			@$el.$removeBtn.addClass 'm-hide'

module.exports = View

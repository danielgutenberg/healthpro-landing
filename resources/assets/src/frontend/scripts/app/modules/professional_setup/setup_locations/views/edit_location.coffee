BaseEditLocationView = require './abstract/abstract_edit_location'

class View extends BaseEditLocationView

	className: "professional_setup--edit m-location"

	initialize: (options) ->
		super(options)

		_.extend @events,
			'click .professional_setup--location_name input': 'selectInputContent'

		@delegateEvents()

		@model = @initialModel.deepClone()

		@bind()
		@addListeners()
		@render()
		@cacheDom()
		@appendPeriods()

		# trigger location type change event to display the right fields
		@model.trigger 'change:location_type_id'

		# method from mixin
		@initValidation()

	bind: ->
		super

		@bindings['[data-location-type]'] =
			observe: 'location_type_id'
			onGet: (val) => @collections.locationsTypes.get(val).get('name')

	render: ->
		@$el.html(ProfessionalSetupLocations.Templates.EditLocation()).appendTo @$container
		@stickit()

		if @baseView.hasPopup()
			@afterRenderPopup()
		else
			@baseView.scrollTo @parentView.$el # scroll to the location view
		@

	afterSave: ->
		super
		@initialModel.updateFromEdit @model.toJSON()
		@baseView.cancelEditLocation true

	selectInputContent: (ev) ->
		ev.currentTarget.select()

	afterRenderPopup: ->
		@baseView.scrollTo @baseView.$el
		popup = @baseView.getPopup()
		popup.openPopup()

		popup.updateHeader @model.getLocationTypeLabel(), false, true
		popup.bindEvent 'cancel', =>
			# bind single close event
			@baseView.cancelEditLocation true


module.exports = View

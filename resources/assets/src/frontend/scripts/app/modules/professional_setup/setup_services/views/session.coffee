Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'
NumericInput = require 'framework/numeric_input'
Config = require 'app/modules/professional_setup/config/config'

class View extends Backbone.View

	className: 'professional_setup--sessions--item'

	events:
		'click [data-session-remove]': 'removeSession'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions options

		@addValidation()
		@addListeners()
		@bind()

	addListeners: ->
		@listenTo @, 'rendered', =>
			@cacheDom()
			@appendPackages()
			@toggleRemoveButton()

		@listenTo @collection, 'add remove', => @toggleRemoveButton()

	cacheDom: ->
		@$el.$packages = @$el.find('[data-session-packages]')
		@$el.$remove = @$el.find('[data-session-remove]')

	bind: ->
		@bindings =
			'[name="price"]':
				observe: 'price'
				onGet: (val) -> val * 1
				onSet: (val) -> @price.parseValue val
				initialize: ($el) ->
					@price = new NumericInput $el,
						decimal: '.'
						leadingZeroCheck: false
						initialParse: false
						parseOnBlur: false

			'[name="duration"]':
				observe: 'duration'
				onSet: @onSetDuration
				selectOptions:
					collection: Config.SessionDurationOptions
					defaultOption:
						label: 'Select duration'
						value: null

	render: ->
		@$el.html ProfessionalSetupServices.Templates.Session()
		@stickit()
		@trigger 'rendered'
		@

	appendPackages: ->
		$packages = new ProfessionalSetupServices.Views.Packages
			collection: @model.get('packages')
			parentModel: @model
			baseView: @baseView
		$packages.render().$el.appendTo @$el.$packages


	onSetDuration: (val) =>
		sessions = @model.collection
		sameDuration = sessions.findWhere
			duration: val
			active: 1 # search only active sessions
		if sameDuration instanceof Backbone.Model
			@duplicateAlert()
		val

	duplicateAlert: ->
		vexDialog.buttons.YES.text = 'OK'
		vexDialog.alert
			message: 'You already have a session with that duration, please select a different duration'
			callback: => @model.set 'duration', ''

	removeSession: =>
		savedPackagesReccuring = @serviceModel.get('packages_reccuring').where
			is_saved: true

		activeSessions = @collection.where
			active: 1

		if activeSessions.length < 2 and savedPackagesReccuring.length is 0
			vexDialog.alert
				buttons: [
					$.extend {}, vexDialog.buttons.YES, {text: 'Ok'}
				]
				message: Config.Messages.sessionDeleteDecline
			return

		if @model.get('packages').length or @model.get('id')
			vexDialog.confirm
				buttons: [
					$.extend {}, vexDialog.buttons.YES, {text: 'YES'}
					$.extend {}, vexDialog.buttons.NO, {text: 'Cancel'}
				]
				message: 'Are you sure you want to delete this session?'
				callback: (value) =>
					return unless value
					@remove()
		else
			@remove()


	remove: ->
		@baseView.showLoading()

		if @baseView.instances.edit_service
			@baseView.instances.edit_service.raiseGenericError null

		sessionId = @model.get('id') # store id to remove from cached unformatted_sessions

		$.when(@model.remove()).then(
			=>
				# remove from cached while the delete request was sent
				unformatted_sessions = _.reject @serviceModel.get('unformatted_sessions'), (session) ->
					session.session_id is sessionId

				@serviceModel.set('unformatted_sessions', unformatted_sessions)

				@baseView.hideLoading()
				super
		,
			(response) =>
				@baseView.hideLoading()
				if @baseView.instances.edit_service
					msg = response.responseJSON.errors?.error?.messages?[0]
					msg = "Can't delete the session." unless msg
					@baseView.instances.edit_service.raiseGenericError msg
		)

	toggleRemoveButton: ->
#		if @collection.length > 1
#			@$el.$remove.removeClass 'm-hide'
#		else
#			@$el.$remove.addClass 'm-hide'

module.exports = View

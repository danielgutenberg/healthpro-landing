Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
ScrollTo = require 'utils/scroll_to_element'
Timezones = require 'framework/timezones'
Config = require 'app/modules/professional_setup/config/config'

class View extends Backbone.View

	className: 'professional_setup--inner m-dashboard'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@tz = new Timezones()

		@instances = {}

		@bind()
		@render()
		@cacheDom()
		@preload()
		@initLocations()

	cacheDom: ->
		@$body = $('body')
		@$el.$main = @$el.find('[data-view-main]')
		@$el.$error = @$el.find('[data-base-error]')
		@$el.$loading = @$el.find('.loading_overlay')


	render: -> @$el.html(ProfessionalSetupLocations.Templates.Base()).appendTo @$container

	bind: ->
		@listenToOnce @, 'data:loaded', @hideLoading

	preload: ->
		@showLoading()
		$.when(
			@collections.locationsTypes.getLocationsTypes()
		).then(
			=> @collections.locations.getLocations()
		).then(
			=> @trigger 'data:loaded'
		)

	initLocations: ->
		@instances.locations = new ProfessionalSetupLocations.Views.Locations
			collections: @collections
			baseView: @
			type: @type

	saveLocation: ->
		return unless @instances.edit_location
		@instances.edit_location.saveLocation()

	raiseError: (message = null, type = 'error') ->
		if message
			@$el.$error.removeClass 'm-hide'
			@$el.$error.html "<div class='alert m-#{type} m-show'>#{message}</div>"
		else
			@$el.$error.addClass 'm-hide'
			@$el.$error.html ''

	cancelEditLocation: (scrollTop = false) ->
		# cleanup google autocomplete
		$('.pac-container').remove()

		@removeEditLocation()
		@instances.locations.cancelEdit()

		@scrollTo @$el if scrollTop

		if @hasPopup()
			@closePopup()

	removeEditLocation: ->
		@raiseError null
		@closePopup()
		if @instances.edit_location
			@instances.edit_location.remove()
			@instances.edit_location = null

	cancelEditLocationWithConfirmation: (callback) ->
		if @instances.edit_location? and @instances.edit_location.hasChanges
			vexDialog.confirm
				message: Config.Messages.hasChanges
				callback: (value) =>
					callback() if value
					return
		else
			callback()
		@

	initEditLocation: (model, view) ->
		@cancelEditLocation()

		@instances.edit_location = new ProfessionalSetupLocations.Views.EditLocation
			collections		: @collections
			baseView		: @
			initialModel	: model
			parentView		: view
			isPopup			: @hasPopup()
			$container		: do =>
				if @hasPopup()
					@getPopup().getContainer()
				else
					view.$el.$editContainer

	initAddLocation: ->
		@cancelEditLocation()

		@instances.edit_location = new ProfessionalSetupLocations.Views.AddLocation
			collections		: @collections
			baseView		: @
			isPopup			: @hasPopup()
			$container		: do =>
				if @hasPopup()
					@getPopup().getContainer()
				else
					@$el.find('[data-locations-add-container]')


	scrollTo: ($el = null, offset = 100) ->
		ScrollTo (if $el is null then @$el else $el), offset
		@

	hasPopup: -> @app.rootApp?.hasPopup()

	closePopup: ->
		if @hasPopup()
			@getPopup().closePopup()
		return

	getPopup: -> @app.rootApp?.getPopup()

	showLoading: (isSave = false) ->
		if isSave and @hasPopup()
			@getPopup().showLoading()
		else
			@$el.$loading.removeClass('m-hide')

	hideLoading: (isSave = false) ->
		if isSave and @hasPopup()
			@getPopup().hideLoading()
		else
			@$el.$loading.addClass('m-hide')


module.exports = View

Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		id: ''
		name: ''
		location_ids: []
		sessions: []
		packages_reccuring: []
		required: false
		introductory: false
		introductory_location: null
		service_type_id: null # primary service
		service_type_ids: [] # secondary services
		certificates: 0
		description: ''

	validation:
		service_type_id: (value, field, computedState) ->
			return if computedState.id
			'Please select a service' unless value
		location_ids: (value, field, computedState) ->
			# if there is an introductory location professional doesn't need to select another one
			return if computedState.introductory_location
			'Please select at least one location' unless value?.length

	initialize: (attrs, options) ->
		if options?.fromModel?
			@initFromModel options.fromModel

		@initSessions()

	initFromModel: (fromModel) ->
		@set
			location_ids: fromModel.get('location_ids')
			certificates: fromModel.get('certificates')

		fromSessions = fromModel.get('unformatted_sessions')
		sessions = fromSessions.map (fromSession) ->
			session =
				active: fromSession.active
				price: fromSession.price
				duration: fromSession.duration

			session.packages = fromSession.packages.map (fromPackage) ->
				{
					number_of_months: fromPackage.number_of_months
					number_of_visits: fromPackage.number_of_visits
					price: fromPackage.price
					rules: fromPackage.rules.map (fromRule) ->
						{
							rule: fromRule.rule
							value: fromRule.value
						}
				}

			session

		@set 'sessions', sessions
		@

	initSessions: ->
		return if @get('sessions') instanceof ProfessionalSetupServices.Collections.Sessions

		# cache unformatted sessions. We'll grab the reccuring packages from this array
		@set 'unformatted_sessions', @get('sessions') unless @has('unformatted_sessions')

		@set 'sessions', new ProfessionalSetupServices.Collections.Sessions 0,
			model: ProfessionalSetupServices.Models.Session
			parentModel: @

		# store reccuring packages in the service model
		# because we want to render them separately from the sessions
		# they will be combined with the sessions on save
		@set 'packages_reccuring', new ProfessionalSetupServices.Collections.PackagesReccuring 0,
			model: ProfessionalSetupServices.Models.PackageReccuring
			parentModel: @

		@listenTo @get('sessions'), 'change', => @trigger 'change'
		@listenTo @get('packages_reccuring'), 'change', => @trigger 'change'
		@

	save: ->
		sessions = @getSessionsJson()
		exists = if @get('id') then true else false
		params =
			name: @get('name')
			service_type_ids: @getServiceTypeIds()
			location_ids: @get('location_ids')
			description: @get('description')
			gift_certificates: @get('certificates')
			sessions: sessions
			_token: window.GLOBALS._TOKEN

		if exists
			method = 'put'
			url = getApiRoute 'ajax-provider-service-update',
				providerId: 'me'
				serviceId: @get('id')
		else
			method = 'post'
			url = getApiRoute('ajax-provider-service-add', {providerId: 'me'})

		$.ajax
			url: url
			method: method
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify params
			show_alerts: false
			success: (response) =>

				# set the new sessions with ids
				@set 'sessions', response.data.sessions
				@set 'unformatted_sessions', response.data.sessions

				@initSessions()

				unless exists
					@set 'id', response.data.service_id

			error: (jqXHR) =>
				@raiseAlertErrorFromResponse jqXHR.responseJSON

	remove: ->
		$.ajax
			url: getApiRoute('ajax-provider-service-delete', {
				providerId: 'me'
				serviceId: @get('id')
			})
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: =>
				@set('id', null)
				@destroy()

	# make sure we return a proper JSON object
	toJSON: ->
		attributes = _.clone @attributes
		if attributes.sessions instanceof ProfessionalSetupServices.Collections.Sessions
			attributes.sessions = @attributes.sessions.toJSON()

		if attributes.introductory_location instanceof Backbone.Model
			attributes.introductory_location = @attributes.introductory_location.toJSON()

		if attributes.packages_reccuring instanceof ProfessionalSetupServices.Collections.PackagesReccuring
			attributes.packages_reccuring = @attributes.packages_reccuring.toJSON()

		attributes

	# custom clone method
	deepClone: ->
		new @constructor @toJSON()

	updateFromEdit: (args) ->
		@set args
		@initSessions()

	removeInvalidLocations: (locationIds) ->
		modelLocationIds =  @get('location_ids')
		_.each modelLocationIds, (locationId) =>
			if _.indexOf(locationIds, locationId) < 0
				modelLocationIds = _.without modelLocationIds, locationId
		# remove invalid location from the locations list
		@set 'location_ids', modelLocationIds

	raiseAlertErrorFromResponse: (responseJson) ->
		@trigger 'error:alert', responseJson.errors.error.messages[0] if responseJson.errors?.error?.messages

	# this method collects all the standard sessions for the service
	# and also all the sessions with reccuring packages
	# sometimes we can have an inactive session with reccuring packages
	getSessionsJson: ->
		sessions = _.reject @get('sessions').toJSON(), {price: ''}

		packagesReccuring = @get('packages_reccuring').getFormattedPackages()

		# for every reccuring package find a session with same duration
		# append package to the session if found or
		# create a new inactive session and fill it with the package
		_.each packagesReccuring, (packageReccuring) ->
			# we always keep the session id of the package even if the regular rate associated with it
			# changed the time.  So if the duration of the regular rate changes, we create it into a new
			# model without the id and the package remains with the old session id

			session = _.findWhere sessions,
				id: packageReccuring.sessionId

			if session?
				if session.duration != packageReccuring.duration
					session.id = null
					sessions.push
						id: packageReccuring.sessionId
						active: 0
						duration: packageReccuring.duration
						price: 0
						is_first_time: 0
						packages: [packageReccuring]
						title: ''

				else
					session.packages.push packageReccuring
			else
				session = _.findWhere sessions,
					duration: packageReccuring.duration
					is_first_time: 0

				if session?
					session.packages.push packageReccuring
				else
					sessions.push
						active: 0
						duration: packageReccuring.duration
						price: 0
						is_first_time: 0
						packages: [packageReccuring]
						title: ''

		# now we need to double check if we have sessions with same duration.
		# we can have them when we had an inactive session from the backend and
		# then we've added a new active session with the same duration.
		# if that happened - merge them
		@mergeDuplicateSessions sessions

		# remove the params we don't need for saving
		sessions.map (session) -> _.omit session, 'introductory'


	# a bit messy implementation
	# @todo refactor it later
	mergeDuplicateSessions: (sessions) ->

		# group sessions by duration
		grouped = _.groupBy sessions, (session) -> session.duration

		_.each grouped, (sess) ->
			return if sess.length < 2

			primarySession = _.remove sess, (s) -> s.id
			return unless primarySession.length and sess.length

			primarySession = primarySession[0]
			duplicatedSession = sess[0]

			sessions = _.without sessions, primarySession, duplicatedSession

			primarySession.price = duplicatedSession.price if duplicatedSession.price
			primarySession.active = duplicatedSession.active if duplicatedSession.active
			primarySession.packages = primarySession.packages.concat duplicatedSession.packages

			sessions.push primarySession

		# clean empty sessions after merge and return
		_.filter sessions, (session) -> !(session.active is 0 and session.packages.length is 0)

	getActiveSessions: -> @get('sessions').where active: 1

	getServiceTypeIds: -> [@get('service_type_id')].concat @get('service_type_ids')

module.exports = Model

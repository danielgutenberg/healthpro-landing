Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
getApiRoute = require 'hp.api'
require 'moment-timezone'

class View extends Backbone.View

	initialize: (@options) ->
		super

		@eventBus = @options.eventBus

		@cacheDom()
		@bind()
		@formatDatetime()

	cacheDom: ->
		@$el = $('[data-sync]')
		@$loading = $('.loading_overlay', @$el)
		@$googleLoading = $('[data-loading-import]')
		@$unsync_btn = $('.schedule--unsync')
		@$last_sync = $('[data-last-sync]')

	bind: ->
		@$unsync_btn.on 'click', (e) =>
			e.preventDefault()
			@unsync()

	unsync: ->
		$driver = @$unsync_btn.data 'driver'

		@$loading.removeClass 'm-hide'

		$.ajax
			url: getApiRoute('ajax-calendar-disconnect', {
				driver: $driver
			})
			method: 'put'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: (response) =>
				# rerender the calendar after removing imported events
				location.reload()
			error: (response) =>
				@$loading.addClass 'm-hide'

	sync: ->
		if window.GLOBALS._CALENDAR_LINKED is 'false' or window.GLOBALS._CALENDAR_LINKED is 'NULL'
			@$googleLoading.removeClass 'm-hide'
			$.ajax
				url: getApiRoute('ajax-calendar-sync',
					driver: window.GLOBALS._CALENDAR_DRIVER
				)
				method: 'post'
				type  : 'json'
				contentType: 'application/json; charset=utf-8'
				data: JSON.stringify
					_token: window.GLOBALS._TOKEN
				success: (response) =>
					# rerender the calendar after importing events
					location.reload()
				error: (response) =>
					@$googleLoading.addClass 'm-hide'

	formatDatetime: ->
		utcDatetimeString = @$last_sync.data('last-sync')
		localDatetime = $.fullCalendar.moment.utc(utcDatetimeString).toDate()
		localDatetimeMoment = $.fullCalendar.moment(localDatetime)

		@$last_sync.text(localDatetimeMoment.format('MMMM Do') + ' at ' + localDatetimeMoment.format('HH:mm'))


	# this can and should be improved
	# ideally we don't want to render sync panel in the php template
	# but for now it will work
	initMobile: ($container) ->
		# reformat element
		@$el.find('> p').remove()
		@$el.find('.sync--header').remove()
		@$el.find('.sync--right').removeClass('sync--right')
		@$el.removeClass('panel sync')
		@$el.find('.btn_sync').removeClass('btn btn_sync m-google').addClass('schedule--header--list--dropdown--action--link m-google')
			.parent().addClass('schedule--header--list--dropdown--action')

		$select = @$el.find('.m-select_calendar')
		$select.html "<i>#{$select.text()}</i>" if $select.length
		$container.append @$el
		@


module.exports = View

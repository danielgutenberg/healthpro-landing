require 'jquery-ui/autocomplete'

Backbone = require 'backbone'
Datepicker = require 'framework/datepicker'
Select = require 'framework/select'
Timepicker = require 'framework/timepicker'
Moment = require 'moment-timezone'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	attributes:
		'class': 'popup schedule_details'
		'data-popup': 'schedule_details'
		'id' : 'popup_schedule_details'

	events:
		'click .schedule_details--save': 'saveBlockedTime'
		'click .schedule_details--unblock': 'unblockTime'

	initialize: (@options) ->
		super
		@eventBus = @options.eventBus
		@date = @options.date
		@parentView = @options.parentView
		@collections = @options.collections

		@model = new ProfessionalSchedule.Models.BlockedAvailability
			# we don't care about the time here
			date_from: @date.clone().set
				h: 0
				m: 0
				s: 0
			date_until: @date.clone().set
				h: 0
				m: 0
				s: 0
			# we don't care about the seconds here
			time_from: @date.clone().set
				s: 0
			# setting default time (end of the day)
			time_until: @date.clone().add(1, 'hours').set
				s: 0

		@bind()
		@initValidation()

	cacheDom: ->
		@$el.$loading = @$el.find('.loading_overlay')
		@$el.$alert = @$el.find('.schedule--alert')

	hideAlert: ->
		@alertTimeout = null
		@$el.$alert.removeClass('m-show').html('')
		@centerView()

	showAlert: (msg, type = 'info', addTimeout = true) ->

		html = "<div class='alert m-#{type} m-show'>"
		msg = [msg] if _.isString msg
		_.each msg, (m) => html += "<p>#{m}</p>"
		html += "</div>"
		@$el.$alert.addClass('m-show').html(html)
		@centerView()
		if addTimeout
			@alertTimeout = setTimeout(
				=>
					@hideAlert()
			, 3000
			)

	centerView: -> @parentView.centerPopup()

	showLoading: -> @$el.$loading.removeClass('m-hide')
	hideLoading: -> @$el.$loading.addClass('m-hide')

	bind: ->
		@bindings =
			'[name="date_from"]':
				observe: 'date_from'
				onGet: @onDateGet
				onSet: (val) =>
					val = @onDateSet(val)
					if @$el.$datepickerUntil.length
						@datepickerUntil.updateOption('minDate', val.format(ProfessionalSchedule.Config.datepickerFormat))
					return val
				initialize: ($el) =>
					@$el.$datepickerFrom = $el.closest('[data-datepicker]')
					@datepickerFrom = new Datepicker @$el.$datepickerFrom,
						minDate: 0
						dateFormat: ProfessionalSchedule.Config.datepickerPluginFormat
						selectOtherMonths: true

			'[name="time_from"]':
				observe: 'time_from'
				onGet: @onTimeGet
				onSet: @onTimeSet
				initialize: ($el) =>
					timepicker = new Timepicker $el,
						inputName: 'time_from'
						submitLabel: 'Select'

			'[name="date_until"]':
				observe: 'date_until'
				onGet: @onDateGet
				onSet: @onDateSet
				initialize: ($el) =>
					@$el.$datepickerUntil = $el.closest('[data-datepicker]')
					@datepickerUntil = new Datepicker @$el.$datepickerUntil,
						minDate: $.fullCalendar.moment(@model.get('date_from')).format(ProfessionalSchedule.Config.datepickerFormat) # don't allow to select a day before the date_from
						dateFormat: ProfessionalSchedule.Config.datepickerPluginFormat
						selectOtherMonths: true

			'[name="time_until"]':
				observe: 'time_until'
				onGet: @onTimeGet
				onSet: @onTimeSet
				initialize: ($el) =>
					timepicker = new Timepicker $el,
						inputName: 'time_until'
						submitLabel: 'Select'

			'[name="description"]': 'description'

	render: ->
		@$el.html ProfessionalSchedule.Templates.BlockTime
			timezone: @getCurrentTimezone()
		@cacheDom()
		@stickit()
		@hideLoading()
		@

	getCurrentTimezone: ->
		now = $.fullCalendar.moment()
		guessedZone = Moment.tz.guess()

		return Moment.tz(now, guessedZone).format('z')

	onDateGet: (val) ->
		if val then val.format(ProfessionalSchedule.Config.datepickerFormat)

	onDateSet: (val) -> $.fullCalendar.moment(val, ProfessionalSchedule.Config.datepickerFormat)

	onTimeSet: (val, options) =>
		time = val.split(':')
		newVal = @model.get('time_from').clone()
		newVal.set
			h: time[0]
			m: time[1]
		newVal

	onTimeGet: (val) ->
		if val then val.format('HH:mm')

	validateData: ->
		@errors = []
		errors = @model.validate()
		@errors.push errors if errors?
		if @errors.length
			@$el.find('.schedule_details--block--header').addClass('m-error')
		else
			@$el.find('.schedule_details--block--header').removeClass('m-error')

	checkDates: ->
		@hideAlert()
		errors = @model.checkDates()
		if errors? # push to @errors to check for errors before saving
			@errors.push errors
			@showAlert errors.error, 'error', false

	initValidation: ->
		buildSelector = (view, attr, selector) ->
			attr = 'client' if attr is 'client_id' or attr is 'email'
			if attr.indexOf('.') isnt -1
				attr = attr.split('.')
				$field = view.$el.find('[' + selector + '*="' + attr[0] + '[' + attr[1] + ']"]')
			else
				$field = view.$el.find('[' + selector + '*="' + attr + '"]')

		Backbone.Validation.bind @,
			valid: (view, attr, selector) ->
				$field = buildSelector(view, attr, selector)
				$field.parents('.field, .select')
					.removeClass('m-error')
					.find('.field--error')
					.html('')

			invalid: (view, attr, error, selector) ->
				$field = buildSelector(view, attr, selector)
				$field.parents('.field, .select')
					.addClass('m-error')
					.find('.field--error')
					.html(error)
				$field.trigger('error')
				$field.on('focus.validation', ->
					$field.parents('.field, .select').removeClass('m-error')
					$field.off('focus.validation')
					)

	saveBlockedTime: ->
		@validateData()
		@checkDates()
		return if @errors.length
		@showLoading()
		$.when(@model.save()).then(
			(response) =>
				@hideLoading()
				_.each response.data, (availability) =>
					blocked_availability = new @collections.blocked_availabilities.model @collections.blocked_availabilities.formatBlockedAvailability(availability, true)
					@collections.blocked_availabilities.appendBlockedAvailability blocked_availability
			(error) =>
				@hideLoading()
		)

module.exports = View

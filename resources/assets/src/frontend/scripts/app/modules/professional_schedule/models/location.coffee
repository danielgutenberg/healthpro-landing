Backbone = require 'backbone'

class LocationModel extends Backbone.Model

	defaults:
		id: null
		name: ''
		address: null
		availabilities: []
		bg: '#b6e4eb'

	initialize: ->
		super
		@

	getAvailabilities: (view) ->
		availabilities = []

		_.each @get('availabilities'), (availability) =>
			localOffset = $.fullCalendar.moment().utcOffset()
			locationOffset = $.fullCalendar.moment().clone().tz(@get('timezone')).utcOffset()

			timeToAdd = localOffset - locationOffset
			fromAdjustment = @adjustTimes(availability.from, timeToAdd, availability.day)
			untilAdjustment = @adjustTimes(availability.until, timeToAdd, availability.day)

			newFrom = fromAdjustment.formattedTime
			newUntil = untilAdjustment.formattedTime
			newDow = fromAdjustment.dayOfWeek # we subtract 1 to the day in the function because moment starts with day 0

			data =
				id: 'location_' + availability.id
				title: @get('name')
				type: 'location'
				entityId: @get('id')
				availabilityId: availability.id
				className: 'm-location'
				rendering: 'background'
				start: newFrom
				dow: [newDow]
			if untilAdjustment.dayOfWeek == fromAdjustment.dayOfWeek
				data.end = newUntil
				availabilities.push data
			else
				data.end = '23:59'
				if data.start isnt data.end
					availabilities.push data

				if newDow == 6
					nextDay = 0
				else
					nextDay = newDow + 1

				data =
					id: 'location_' + availability.id
					title: @get('name')
					type: 'location'
					entityId: @get('id')
					availabilityId: availability.id
					className: 'm-location'
					rendering: 'background'
					start: '00:00'
					end: newUntil
					dow: [nextDay]
				availabilities.push data

		availabilities

	adjustTimes: (time, timeToAdd, dayOfWeek) ->

		splitTime = time.split(':')
		oldDay = $.fullCalendar.moment().set
			weekday: dayOfWeek - 1
			h: splitTime[0]
			m: splitTime[1]

		newDay = oldDay.clone().add(timeToAdd, 'm')


		unless newDay.clone().format('H:mm') == '0:00'
			formattedTime = newDay.clone().format('H:mm')
		else
			newDay.subtract({'minute': 1})
			formattedTime = '23:59'

		data =
			formattedTime: formattedTime
			dayOfWeek: newDay.weekday()
			moment: newDay


module.exports = LocationModel

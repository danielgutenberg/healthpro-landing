Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: ->
		super
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-profile-provider-services', providerId: 'me')
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.forEach response.data, (item) =>
					@addService item
				@addDetailedNames()
				@trigger 'data:ready'

	addService: (item) ->
		@add
			id: item.service_id
			type_id: item.service_type_id
			slug: item.slug
			name: item.name
			sessions: do ->
				sessions = []
				_.each item.sessions, (sess) ->
					# do not add inactive sessions or sessions with no price
					return unless sess.active or parseInt(sess.price) > 0
					sessions.push
						duration: sess.duration
						price: sess.price
						session_id: sess.session_id
				sessions
			location_ids: _.pluck(item.locations, 'id')
			location_names: _.pluck(item.locations, 'name')

	addDetailedNames: ->
		_.each @models, (service) =>

			service_name = service.get('name')

			if @.where({ name: service.get('name') }).length > 1
				locations = []
				_.each service.get('location_names'), (location_name) ->
					locations.push location_name

				service.set('detailed_name', service_name + ' - ' + locations.join(', '))
			else
				service.set('detailed_name', service_name)

	findBySessionId: (sessionId) ->
		@find (item) -> (_.findWhere item.get('sessions'), session_id: sessionId)?

	findByServiceId: (serviceId) ->
		@find (item) -> (_.findWhere id: serviceId)?

	getUsedLocations: ->
		# getting a list of locations with services
		locations = []
		_.each @models, (model) =>
			locations = locations.concat model.get 'location_ids'
		_.uniq locations

module.exports = Collection

Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Numeral = require 'numeral'

class Collection extends Backbone.Collection

	initialize: ->
		super
		@getData()
		@bind()

	bind: ->
		@listenTo @, 'blocked_availability:append', @appendBlockedAvailability

	appendBlockedAvailability: (model) ->
		@add model
		@trigger 'blocked_availability:appended', model

	removeBlockedAvailability: (model) ->
		@remove model

	getData: ->
		$.ajax
			url: getApiRoute('ajax-provider-availability-get-blocked')
			method: 'get'
			type  : 'json'
			data: {}
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.forEach response.data, (item) =>
					@push @formatBlockedAvailability(item)

				@trigger 'data:ready'
			error: (error) =>

	formatBlockedAvailability: (item) ->
		id: item.id
		description: item.description
		from: item.registration.from
		until: item.registration.until
		location_id: item.location_id
		provider_id: item.provider_id
		className: 'm-blocked'
		dow: item.day_of_week
		validFrom: item.valid_from
		validUntil: item.valid_until
		range:
			start: $.fullCalendar.moment(item.valid_from).startOf('day')
			end: $.fullCalendar.moment(item.valid_until).endOf('day')

	getAvailabilities: (fullCalendarStartDate) ->
		availabilities = []
		_.each @models, (model) =>
			availabilities = availabilities.concat model.getAvailabilities(fullCalendarStartDate)
		availabilities

module.exports = Collection

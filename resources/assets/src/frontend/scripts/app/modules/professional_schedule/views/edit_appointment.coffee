Numeral = require 'numeral'
Datepicker = require 'framework/datepicker'
Select = require 'framework/select'
BaseEditAppointmentView = require './base_edit_appointment'

class View extends BaseEditAppointmentView

	events:
		'click .schedule_details--save': 'saveAppointment'
		'click .schedule_details--convert': 'convert'
		'click .schedule_details--remove_ask': 'removeAppointmentAsk'
		'click .schedule_details--remove_yes': 'removeAppointmentConfirm'
		'click .schedule_details--remove_no': 'removeAppointmentCancel'

	initialize: ->
		super

		# update initial availability
		@initialAvailability = @findAppointmentLocationBetweenDates @model.get('from'), @model.get('until')

		########################
		# Stickit Collections
		########################
		@times = @collections.appointments.getTimeOptions()
		@durations = @collections.appointments.durations

		# update sessions
		@updateSessionsCollection @model.get('service_id')

		@bind()
		@initValidation()

	bind: ->
		super

		# show warning if we have overlapped events
		@listenTo @model, 'change:from change:date change:until change:service_id change:session_id change:location_id', @afterModelChange

		# we need to update model attributes when session is changed
		@listenTo @model, 'change:from change:session_id', @updateModelFromSession

		# check if we can save a standard appointment
		@listenTo @model, 'change', @canSave

		@bindings =
			'[name="service_id"]':
				observe: 'service_id'
				selectOptions:
					collection: @collections.services
					labelPath: 'name'
					valuePath: 'id'
				onSet: @onServiceSet
				initialize: ($el) =>
					@$el.$service = $el
					@selects.service = new Select @$el.$service

			'[name="session_id"]':
				observe: 'session_id'
				selectOptions:
					collection: @sessions
					labelPath: 'label'
					valuePath: 'id'
				initialize: ($el) =>
					@$el.$session = $el
					@selects.session = new Select @$el.$session

			'[name="location_id"]':
				observe: 'location_id'
				selectOptions:
					collection: @collections.locations
					labelPath: 'name'
					valuePath: 'id'
				initialize: ($el) =>
					@$el.$location = $el
					@selects.location = new Select @$el.$location

			'[data-edit-price]':
				observe: 'price'
				onGet: (val) -> val.format(ProfessionalSchedule.Config.priceFormat)

			'[data-edit-client]': 'full_name'

			'[name="date"]':
				observe: 'date'
				initialize: ($el) =>
					@$el.$datepicker = $el.closest('[data-datepicker]')
					@datepicker = new Datepicker @$el.$datepicker,
						minDate: 0
						dateFormat: ProfessionalSchedule.Config.datepickerPluginFormat
				onGet: @onDateGet
				onSet: @onDateSet

			'[name="from"]':
				observe: 'from'
				selectOptions:
					collection: @times
				initialize: ($el) =>
					@$el.$from = $el
					@selects.from = new Select @$el.$from
				onGet: @onTimeGet
				onSet: @onTimeSet

			'[data-edit-duration]':
				observe: 'until'
				onGet: @onDurationGet

	render: ->
		@$el.html ProfessionalSchedule.Templates.EditAppointment()
		@afterRender()
		@afterModelChange()
		@

	onDateGet: (val) => val.format(ProfessionalSchedule.Config.datepickerFormat)

	# since we're storing the dates in string we need to make sure all the times are syncronized
	onDateSet: (val) =>
		date = $.fullCalendar.moment(val, ProfessionalSchedule.Config.datepickerFormat)
		@model.syncDates date
		date

	onTimeGet: (val) => val.format(ProfessionalSchedule.Config.timeFormat)

	# when we change `from` time we need to update `until` time as well
	onTimeSet: (val) =>
		date = @model.get('date').clone()
		time = val.split(':')
		date.set
			h: time[0]
			m: time[1]
		@model.set 'from', date
		date

	# we set duration from the `until`-`from` difference
	onDurationGet: (val) => @collections.appointments.durations[@model.get('until').diff(@model.get('from'), 'minutes')]

	onServiceSet: (val) =>
		@updateSessionsCollection val
		@$el.$session.trigger 'change'
		val

	updateModelFromSession: (model, id, options) ->
		# we don't need to update session on edit cancel
		return if options.cancelled
		session = @sessions.get @model.get('session_id')
		if session
			@model.set 'until', @model.get('from').clone().add(session.get('duration'), 'minutes')
			@model.set 'price', Numeral(session.get('price'))
		else
			@model.set 'until', null
			@model.set 'price', null

	saveAppointment: ->
		@validateData()
		return if @errors.length

		unless @canSave()
			# we do not allow to save standard appointment without availability ID
			# right now we are showing the error
			# in future it may convert standard appointments to custom ones
			return
		@model.set 'availability_id', @currentAvailability?.availabilityId or null

		@edit()

	canSave: ->
		if @warnings.overlap or @warnings.hours or @warnings.locations or @warnings.appointment_location
			@$el.$saveError.addClass('m-show')
			@$el.$actions.addClass('m-hide')
			@$el.$save.attr 'disabled', true
			@centerView()
			return false

		@$el.$saveError.removeClass('m-show')
		@$el.$actions.removeClass('m-hide')
		@$el.$save.attr 'disabled', false
		@centerView()
		return true

	# convert standard appointment to custom appointment
	convert: -> @parentView.convertAppointment @model

module.exports = View

vexDialog = require 'vexDialog'

alertError: (missing_field, type) ->
	data =
		route:
			location: '/dashboard/appointments/setup'
			service: '/dashboard/appointments/setup'
			payment: '/dashboard/settings/payments'
		label:
			location: 'no_location'
			service: 'no_service'
			payment: 'no_payment'

	label = data.label[missing_field]

	vexDialog.buttons.YES.text = ProfessionalSchedule.Messages.Warnings[label].button_text
	vexDialog.alert
		message: ProfessionalSchedule.Messages.Warnings[label][type]
		callback: (value) ->
			return unless value
			window.location.href = window.location.origin + data.route[missing_field]

Backbone = require 'backbone'

class ClassModel extends Backbone.Model

	defaults:
		id: null
		name: ''
		description: ''
		rating: ''
		availabilities: []
		packages: []
		tags: []

	initialize: ->
		super
		@

	getAvailabilities: (fullCalendarView) ->
		availabilities = []
		_.each @get('availabilities'), (availability) =>
			location = ProfessionalSchedule.app.collections.locations.get availability.location_id
			availabilities.push
				id: 'class_' + availability.id
				title: @get('name')
				description: location.get('name') # + "<br>Reservations: TODO/" + availability.max_clients
				type: 'class'
				entityId: @get('id')
				locationId: availability.location_id
				className: 'm-class'
				start: availability.from
				end: availability.until
				dow: [availability.day - 1] # sunday = 0 in fullCalendar
				collection: 'classes'
		availabilities


module.exports = ClassModel

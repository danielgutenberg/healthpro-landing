Backbone = require 'backbone'
DeepModel = require 'backbone.deepmodel'
getApiRoute = require 'hp.api'
Moment = require 'moment-timezone'

class BlockTimeModel extends DeepModel

	defaults: ->
		id: null
		date_from: ''
		time_from: ''
		date_until: ''
		time_until: ''
		description: ''

	initialize: ->
		super
		@

	validation:
		date_from:
			required: true
			msg: 'Please enter from date'
		date_until:
			required: true
			msg: 'Please enter until date'
		description:
			required: true
			msg: 'Please enter title'

	getCurrentTimezone: ->
		now = $.fullCalendar.moment()
		guessedZone = Moment.tz.guess()

	save: ->
		if !@mergeDateTime()
			return false
		data =
			from: @get('from')
			until: @get('until')
			description: @get('description')
			timezone: @getCurrentTimezone()
			_token: window.GLOBALS._TOKEN

		$.ajax
			url: getApiRoute('ajax-provider-availability-block')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify data
			success: (response) ->
				if response.data.length == 0 then window.Alerts.addNew('error', ProfessionalSchedule.Messages.Warnings.day_without_availability)

			error: (response) ->
				if response.responseJSON?.errors
					_.each response.responseJSON.errors, (err) =>
						if err.messages?.length
							window.Alerts.addNew('error', err.messages[0])

	update: ->
		data =
			from: @get('from')
			until: @get('until')
			description: @get('description')
			timezone: @getCurrentTimezone()
			_token: window.GLOBALS._TOKEN

		$.ajax
			url: getApiRoute('ajax-provider-availability-update-blocked', {'registrationId': @get('id')})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify data
			error: (response) ->
				if response.responseJSON?.errors
					_.each response.responseJSON.errors, (err) =>
						if err.messages?.length
							window.Alerts.addNew('error', err.messages[0])

	unblockTime: (bgEventData) ->
		data =
			from: @get('from')
			until: @get('until')
			timezone: @getCurrentTimezone()
			_token: window.GLOBALS._TOKEN

		$.ajax
			url: getApiRoute('ajax-provider-availability-block')
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify data
			error: (response) ->
				if response.responseJSON?.errors
					_.each response.responseJSON.errors, (err) =>
						if err.messages?.length
							window.Alerts.addNew('error', err.messages[0])

	mergeDateTime: ->
		fromDate = @prepareDate(@get('date_from'), @get('time_from'))
		untilDate = @prepareDate(@get('date_until'), @get('time_until'))
		if(!fromDate || !untilDate)
			return false
		@set('from', fromDate)
		@set('until', untilDate)

	prepareDate: (date, time) ->
		hours = $.fullCalendar.moment(time).hours()
		minutes = $.fullCalendar.moment(time).minutes()
		if !hours? || !minutes?
			return false
		date.hours(hours)
		date.minutes(minutes)
		date.format(ProfessionalSchedule.Config.dateTimeFormat)

	fillDatesTimes: ->
		@set
			'date_from': $.fullCalendar.moment.parseZone(@get('from')).stripZone()
			'time_from': $.fullCalendar.moment.parseZone(@get('from')).stripZone()
			'date_until': $.fullCalendar.moment.parseZone(@get('until')).stripZone()
			'time_until': $.fullCalendar.moment.parseZone(@get('until')).stripZone()

	checkDates: ->
		if !@mergeDateTime()
			return error: ProfessionalSchedule.Messages.Warnings.empty
		if $.fullCalendar.moment(@get('from')) >= $.fullCalendar.moment(@get('until'))
			if @get('from') is @get('until')
				return error: ProfessionalSchedule.Messages.Warnings.date_time_equals
			else
				return error: ProfessionalSchedule.Messages.Warnings.date_time

	getAvailabilities: -> [@getEventData()]

	getEventData: ->
		data =
			id: @getCalendarEventId()
			title: @get('description')
			type: 'availability'
			availabilityId: @get('id')
			className: 'm-blocked'

		if @get('dow')?
			data.validFrom = @get('validFrom')
			data.validUntil = @get('validUntil')
			data.start = $.fullCalendar.moment(@get('from')).format('HH:mm')
			data.end = $.fullCalendar.moment(@get('until')).format('HH:mm')
			data.dow = [@get('dow')]
			data.range = @get('range')
		else
			data.start = @get('from')
			data.end = @get('until')
		data

	getCalendarEventId: -> 'blocked_' + @get('id')

	updateCalendarEvent: (event) ->
		event.start = @get('from')
		event.end = @get('until')
		event.title = @get('description')
		event

module.exports = BlockTimeModel

AbstractView = require './abstract'
Tooltip = require '../../utils/tooltip'

class View extends AbstractView

	eventAfterAllRender: =>
		super
		Tooltip @getCalendarView().el
		@

	eventRender: (ev, el, view) =>
		if ev.range?
			unless new Date(ev.start.format('YYYY-MM-DD')) >= new Date(ev.range.start.format('YYYY-MM-DD')) and new Date(ev.end.format('YYYY-MM-DD')) <= new Date(ev.range.end.format('YYYY-MM-DD'))
				return false
		$el = $(el)
		$el.data 'event', ev
		$el.attr 'id', "event_#{ev.id}"

		title = ''
		description = ''
		time = ''

		if ev.rendering is 'background'
			title = ev.title if ev.title
			description = ev.description if ev.description
			if view.type is 'agendaDay'
				time = ev.start.format('h(:mm)a') + ' - ' + ev.end.format('h(:mm)a')
		else
			switch view.type
				when 'month'
					$el.find('.fc-time').text("#{ev.start.format('h(:mm)a')}").addClass 'm-visible'
					$el.find('.fc-title').addClass 'm-normal'
				when 'agendaWeek', 'agendaDay'
					description = ev.description
					time = ev.start.format('h(:mm)a') + ' - ' + ev.end.format('h(:mm)a') if ev.start and ev.end
				when 'listWeek'
					@renderListWeekEvent($el, ev, view)

		$el.append "<div class='fc-title'>#{title}</div>" if title
		$el.append "<div class='fc-description'>#{description}</div>" if description
		$el.append "<div class='fc-event_time'>#{time}</div>" if time

		if ev.shouldConfirm
			tooltip = '<span class="schedule--pending_confirm" data-pending-confirmation><i data-tooltip title="Confirm Appointment Request">?</i></span>'
			if view.type is 'listWeek'
				$el.find('td:last').append tooltip
			else
				$el.append tooltip

		return

	viewRender: (ev, $el) =>
		@preViewRenderCleanup()
		@toggleBlockTimeBtn(ev.type != 'listWeek')
		@highlightToday($el)

		@

	dayClick: (date, e, view) =>
		$target = $(e.target)
		if $target.hasClass('fc-title') or $target.hasClass('fc-event_time')
			$target = $target.parent()

		if $target.hasClass('fc-bgevent')
			now = $.fullCalendar.moment()
			now.stripZone() if now.hasZone()

			if now.diff(date, 'minutes') > 0
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert 'You cannot start events in the past'
				return
			@addAppointment date, $target.data('event')
		else
			# Popup with block time / custom appointmen options
			@appendPopup new ProfessionalSchedule.Views.SelectAction
				eventBus: @eventBus
				parentView: @
				date: date
				collections: @collections

	eventClick: (eventDetails, e, view) =>
		if view.type is 'listWeek'
			return
		switch eventDetails.type
			when 'appointment'
				@openDetails eventDetails, 'appointments', 'DetailsAppointment'
			when 'availability'
				@appendPopup new ProfessionalSchedule.Views.SelectAction
					eventBus: @eventBus
					parentView: @
					calendarView: @
					type: 'is_blocked'
					model: @collections.blocked_availabilities.get eventDetails.availabilityId
					date: eventDetails.start


	openDetails: (details, collection = 'appointments', view = 'DetailsAppointment') ->
		@detailsView = null
		@detailsView = new ProfessionalSchedule.Views[view]
			model: @collections[collection].get details.entityId
			collections: @collections
			eventBus: @eventBus
			parentView: @
			calendarView: @
			eventDetails: details

		@currentTooltip = $("#event_#{details.id}")
		if @currentTooltip.hasClass 'tooltipstered'
			@currentTooltip.tooltipster 'content', @detailsView.render().$el
		else
			@currentTooltip.tooltipster
				animation: 'fade'
				arrow: true
				theme: "schedule--info"
				trigger: "click"
				position: "right"
				hideOnClick: false
				interactive: true
				contentCloning: false
				content: @detailsView.render().$el
				functionAfter: => @currentTooltip = null

		@currentTooltip.tooltipster 'show'

		@bindScroll()


module.exports = View

Backbone = require 'backbone'
Numeral = require 'numeral'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	attributes:
		'class': 'popup schedule_details'
		'data-popup': 'schedule_details'
		'id': 'popup_schedule_details'

	# we want to be able to save current availability
	currentAvailability: null
	# for editing appointments we also need to save initial availability
	initialAvailability: null

	errors: []
	warnings: {}

	alertTimeout: null

	# sessions collection, used for stickit
	sessions: new Backbone.Collection()

	# stores all the inited Select objects
	selects: {}

	initialize: (@options) ->
		super
		@collections = @options.collections
		@parentView = @options.parentView

		if @options.model
			# store initial state in case user cancels the editing
			@model = @options.model
			@model.saveInitialState()

	initValidation: ->
		buildSelector = (view, attr, selector) ->
			attr = 'client' if attr is 'client_id' or attr is 'email'
			if attr.indexOf('.') isnt -1
				attr = attr.split('.')
				$field = view.$el.find('[' + selector + '*="' + attr[0] + '[' + attr[1] + ']"]')
			else
				$field = view.$el.find('[' + selector + '*="' + attr + '"]')

		Backbone.Validation.bind @,
			valid: (view, attr, selector) ->
				$field = buildSelector(view, attr, selector)
				$field.parents('.field, .select')
					.removeClass('m-error')
					.find('.field--error')
					.html('')

			invalid: (view, attr, error, selector) ->
				$field = buildSelector(view, attr, selector)
				$field.parents('.field, .select')
					.addClass('m-error')
					.find('.field--error')
					.html(error)
				$field.trigger('error')
				$field.on('focus.validation', ->
					$field.parents('.field, .select').removeClass('m-error')
					$field.off('focus.validation')
					)

	showLoading: -> @$el.$loading.removeClass('m-hide')
	hideLoading: -> @$el.$loading.addClass('m-hide')

	bind: ->
		# we need to make sure the model remains unchanged when we close popup without saving
		@$el.on 'popup:closed', =>
			@model.applyInitialState cancelled: true

	afterRender: ->
		@$el.$form = @$el.find('[data-form]')

		@$el.$header_title = @$el.find('.schedule_details--header_title')

		@$el.$saveError = @$el.find('.schedule_details--footer_error')
		@$el.$actions = @$el.find('.schedule_details--footer_actions')

		@$el.$save = @$el.find('.schedule_details--save')
		@$el.$cancel = @$el.find('.schedule_details--cancel')
		@$el.$remove = @$el.find('.schedule_details--remove')
		@$el.$alert = @$el.find('.schedule_details--alert')


		@$el.$loading = @$el.find('.loading_overlay')
		@hideLoading()

		# sometimes tooltipster doesn't hide the tooltip by itself
		@parentView.hideTooltip()

		@stickit()

	validateData: ->
		@errors = []
		errors = @model.validate()
		@errors.push errors if errors?
		if @errors.length
			@$el.find('.schedule_details--block--header').addClass('m-error')
		else
			@$el.find('.schedule_details--block--header').removeClass('m-error')

	resetErrors: ->
		@errors = []
		@$el.find('.field--error').text ''
		@$el.find('.field, .select').removeClass 'm-error'

	showWarning: ->
		@showAlert @warnings, 'warning', false

	hideAlert: ->
		@alertTimeout = null
		@$el.$alert.removeClass('m-show').html('')
		@centerView()

	showAlert: (msg, type = 'info', addTimeout = true) ->
		html = "<div class='alert m-#{type} m-show'>"
		msg = [msg] if _.isString msg
		_.each msg, (m) => html += "<p>#{m}</p>"
		html += "</div>"
		@$el.$alert.addClass('m-show').html(html)
		@centerView()
		if addTimeout
			@alertTimeout = setTimeout(
				=>
					@hideAlert()
				, 3000
			)

	resetWarnings: ->
		@warnings = {}
		@hideAlert()
	hasWarnings: -> Object.keys(@warnings).length
	removeWarning: (key) -> @warnings = _.omit @warnings, key
	addWarning: (msg, key) -> @warnings[key] = msg

	updateSessionsCollection: (serviceId) ->
		# clear previous sessions
		@sessions.reset()
		return unless serviceId
		# append sessions
		_.each @collections.services.get(serviceId).get('sessions'), (session) =>

			label = session.duration + ' minutes'
			label += ' for ' + Numeral(session.price).format(ProfessionalSchedule.Config.priceFormat)
			@sessions.add
				id: session.session_id
				label: label
				price: session.price
				duration: parseInt session.duration

		@collections.locations.setCustomState @model.get 'custom'
		@collections.locations.setService @collections.services.get(serviceId)

	findAppointmentLocationBetweenDates: (fromDateTime, untilDateTime) ->
		locations = ProfessionalSchedule.Calendar.fullCalendar 'clientEvents', (ev) =>
			# we need only locations to check the hours
			return false unless ev.type == 'location'

			# check if the appointment is inside the opening hours of the location
			# we don't need the location if the day of week doesn't match
			return false if fromDateTime.day() != ev.start.day()

			location = @collections.locations.get(ev.entityId)
			availabilityId = parseInt ev.id.replace('location_', '')
			availability = _.findWhere location.get('availabilities'),
				id: availabilityId

			startTime = $.fullCalendar.moment(availability.from, 'HH:ss')
			endTime = $.fullCalendar.moment(availability.until, 'HH:ss')

			# now we compare durations objects and check if we have a match
			fromDateTime.time().asMinutes() >= startTime.time().asMinutes() and untilDateTime.time().asMinutes() <= endTime.time().asMinutes()
		return locations[0] if locations.length
		null

	# check if the appointment is being created within normal hours
	checkNormalHours: ->
		if @currentAvailability
			@removeWarning 'hours'
		else
			@addWarning ProfessionalSchedule.Messages.Warnings.hours, 'hours'


	# check if the service location equals selected location
	checkServicesLocation: ->
		service = @collections.services.get(@model.get('service_id'))
		# the service is not selected yet
		unless service
			@removeWarning 'service_location'
			return
		if _.indexOf(service.get('location_ids'), @model.get('location_id')) > -1
			# the service is selected and has the selected location
			@removeWarning 'service_location'
		else
			# don't check if location is not set
			if @model.get('location_id')
				# the service is not selected or doesn't have the selected location
				@addWarning ProfessionalSchedule.Messages.Warnings.service_location, 'service_location'

	# check if the provider already has an appointment at the same time in different location
	checkLocationsOverlap: (fromDateTime, untilDateTime) ->
		overlap = ProfessionalSchedule.Calendar.fullCalendar 'clientEvents', (ev) =>
			# we don't care about background locations
			return false if ev.type == 'location'
			# return if there's no overlap
			return false unless ev.start? and ev.end?
			return false unless fromDateTime.unix() < ev.end.unix() and untilDateTime.unix() > ev.start.unix()

			# now we check the locations for both events
			if @initialAvailability and @currentAvailability
				ev.locationId != @model.get('location_id') and @initialAvailability.availabilityId != @currentAvailability.availabilityId
			else
				ev.locationId != @model.get('location_id')

		if overlap.length
			@addWarning ProfessionalSchedule.Messages.Warnings.appointment_location, 'appointment_location'
		else
			@removeWarning 'appointment_location'

	# check for overlapped events
	checkOverlappedEvents: (fromDateTime, untilDateTime) ->
		overlap = ProfessionalSchedule.Calendar.fullCalendar 'clientEvents', (ev) ->
			return false if ev.type == 'location' # we don't care about background locations
			return false unless ev.start? and ev.end?
			fromDateTime.unix() < ev.end.unix() and untilDateTime.unix() > ev.start.unix()

		if @initialAvailability
			# ignore model event if
			hasOverlap = switch
				# no overlaps = no errors
				when overlap.length < 1 then false
				# if there's more then one overlap return error
				when overlap.length > 1 then true
				# we don't have a problem if overlap id equals model id
				else overlap[0].entityId != @model.get('id')
		else hasOverlap = overlap.length

		if hasOverlap
			@addWarning ProfessionalSchedule.Messages.Warnings.overlap, 'overlap'
		else
			@removeWarning 'overlap'

	afterModelChange: ->
		# we need to make sure the date and from fields are synchronized
		@model.syncDates()
		# cache times
		fromDateTime = @model.get('from')
		unless _.isString(fromDateTime)
			if @model.has('duration')
				untilDateTime = fromDateTime.clone().add @model.get('duration'), 'minutes'
			else
				untilDateTime = @model.get('until')

			# update current availability
			@currentAvailability = @findAppointmentLocationBetweenDates fromDateTime, untilDateTime

			@checkNormalHours()
			@checkServicesLocation()
			@checkLocationsOverlap fromDateTime, untilDateTime
			@checkOverlappedEvents fromDateTime, untilDateTime

			if @hasWarnings() then @showWarning() else @resetWarnings()

			@centerView()

	# update location data after location_id changed
	afterLocationChange: (model, locationId) ->
		location = @collections.locations.get(locationId).toJSON()
		model.set 'location',
			name: location.name
			address: location.address.address
			city: location.address.city
			postal_code: location.address.postal_code
			timezone: location.address.timezone

	edit: ->
		@showLoading()
		$.when(@model.save()).then(
			(response) =>
				@hideLoading()
				# we don't need initial state at this point
				@model.resetInitialState()
				# update event in the calendar view
				@parentView.updateCalendarEvent @model
				# close the popup
				@parentView.removePopup()
		)


	centerView: -> @parentView.centerPopup()

	removeAppointmentAsk: ->
		@$el.$remove.addClass('m-confirm')
	removeAppointmentCancel: ->
		@$el.$remove.removeClass('m-confirm')
	removeAppointmentConfirm: ->
		@removeAppointmentCancel()
		@showLoading()
		@collections.appointments.remove @model
		eventData = @model.getEventData()
		$.when(@model.remove()).then(
			(success) =>
				@collections.appointments.trigger 'appointment:removed', eventData.id
				@parentView.removePopup()
		, (error) =>
			@hideLoading()
		)

module.exports = View

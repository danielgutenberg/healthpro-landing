Backbone = require 'backbone'
vexDialog = require 'vexDialog'
Print = require 'framework/print'
AlertError = require '../../utils/alert_error'
CurrentTimezone = require '../../utils/current_timezone'
ToggleEl = require '../../utils/toggle_el'

class View extends Backbone.View

	popupName: 'schedule_details'

	className: 'schedule--calendar'

	initialize: (options) ->

		@eventBus = options.eventBus
		@$container = options.$container
		@collections = options.collections
		@baseView = options.baseView

		@detailsView = null
		@popupView = null
		@currentTooltip = null
		@eventsListView = null
		@calendarScroll = 0
		@listEventViews = []

		@parseHash()
		@preload()
		@render()
		@cacheDom()
		@bind()

		@initCalendarHeader()

	bind: ->
		@listenTo @collections.appointments, 'appointment:removed', @removedAppointment
		@listenTo @collections.appointments, 'appointment:appended', @appendedAppointment
		@listenTo @collections.blocked_availabilities, 'blocked_availability:appended', @appendedBlockedAvailability
		@listenTo @collections.blocked_availabilities, 'blocked_availability:removed', @removedBlockedAvailability
		@listenTo @eventBus, 'popup:close', @removePopup

		@listenToOnce @, 'data:loaded', =>
			@initCalendar()
			@afterLoad()

	render: ->
		@$el.html ProfessionalSchedule.Templates.Calendar({timezone: CurrentTimezone()})

		@$container.append @$el
		@

	cacheDom: ->
		@$el.$header = @$el.find('[data-calendar-header]')
		@$el.$calendar = @$el.find('[data-fullcalendar]')
		@$el.$eventsList = @$el.find('[data-events-list]')
		@$el.$blockTimeBtn = @$el.find('.schedule--actions--block_time')
		@$printPopup = $('[data-popup="print_schedule"]')

	initCalendarHeader: ->
		@calendarHeaderView = new ProfessionalSchedule.Views.CalendarHeader
			eventBus     : @eventBus
			el           : @$el.$header
			calendarView : @
		@

	preload: ->
		unloadedCollections = _.size @collections
		_.forEach @collections, ($collection) =>
			@listenTo $collection, 'data:ready', =>
				unloadedCollections--
				@trigger 'data:loaded' unless unloadedCollections

	addAppointment: (date, bgEventData, clientId) ->
		unless @checkProfileFilled('appointment') then return false

		# only if initing appointment from availability
		if bgEventData?
			locationTz = @collections.locations.getLocationTimezone(bgEventData.entityId)
			localOffset = date.utcOffset()
			locationOffset = date.clone().tz(locationTz).utcOffset()
			timeToAdd = locationOffset - localOffset

			date = date.add(timeToAdd, 'minutes')

		view = new ProfessionalSchedule.Views.AddAppointment
			collections: @collections
			eventBus: @eventBus
			date: if date? then date else $.fullCalendar.moment().add(1, 'hour')
			from: if date? then date else null
			locationId: if bgEventData? then bgEventData.entityId
			clientId: clientId
			parentView: @

		@appendPopup view

	initCalendar: ->
		date = window.GLOBALS._CALENDAR_DATE

		if date and (date.split 'T').length == 2
			split = date.split 'T'
			day = split[0]
			time = split[1]

		calendarConfig = _.extend {}, ProfessionalSchedule.Config.Calendar,
			eventRender         : @eventRender
			viewRender          : @viewRender
			eventAfterAllRender : @eventAfterAllRender
			eventClick          : @eventClick
			dayClick            : @dayClick
			events              : @fillEvents

		calendarConfig.scrollTime = time if time?

		@$el.$calendar.fullCalendar calendarConfig

		# cache the fullCalendar func
		ProfessionalSchedule.Calendar = @$el.$calendar
		if day?
			ProfessionalSchedule.Calendar.fullCalendar 'gotoDate', day

	eventAfterAllRender: (view) ->
		view.el.find('.fc-bgevent-container').each (k, container) ->
			$bgEvents = $(container).find('.fc-bgevent')
			$bgEvents.addClass "m-width#{$bgEvents.length}"

		if @newAppointmentDate
			ProfessionalSchedule.Calendar.fullCalendar 'gotoDate', @newAppointmentDate
			@newAppointmentDate = null

		# scroll to new added event after native 'scrollTime' scroll
		if @newAppointmentTime
			setTimeout(
				=>
					$event = $('#event_appointment_' + @newAppointmentId)
					@$el.$calendar.find('.fc-scroller').scrollTop($event.position().top)
					@newAppointmentTime = null
			, 1
			)

	fillEvents: (start, end, timezone, callback) =>
		availabilities = []
		availabilities = availabilities.concat(
			@clearUnusedAvailabilities @collections.locations.getAvailabilities start
			@collections.appointments.getAvailabilities start
			@collections.blocked_availabilities.getAvailabilities(start)
		)

		callback availabilities

	clearUnusedAvailabilities: (availability) ->
		# ignoring availabilities without service
		usedAvailabilities = []
		_.each availability, (availability) =>
			if _.contains @collections.services.getUsedLocations(), availability.entityId
				usedAvailabilities.push availability
		usedAvailabilities

	parseHash: ->
		@hash = _.object _.compact _.map window.location.hash.replace(/^#/, '').split('&'), (item) -> if item then item.split ':'
		@hash.new = parseInt @hash.new, 10 if @hash.new
		@hash.edit = parseInt @hash.edit, 10 if @hash.edit
		window.location.hash = ''

	afterLoad: ->
		if @hash.hasOwnProperty('new')
			if @hash.new?
				@addAppointment(null, null, @hash.new)
			else
				@addAppointment(null, null)

		if @hash.edit?
			@editAppointment()

	removePopup: ->
		if @popupView
			window.popupsManager.closePopup @popupName
			window.popupsManager.removePopup @popupName
			@popupView?.remove()
			@popupView = null
		@

	appendPopup: (view) ->
		# remove old popups
		@removePopup()
		# remove tooltips
		@hideTooltip()
		# cache view
		@popupView = view
		# append popup
		$('body').append @popupView.render().$el
		window.popupsManager.addPopup @popupView.$el
		window.popupsManager.openPopup @popupName
		@

	centerPopup: -> $(document).trigger 'popup:center'

	editAppointment: (model) ->
		unless model? then model = @collections.appointments.findWhere('id': @hash.edit)
		view = if model.isCustom() then 'EditAppointmentCustom' else 'EditAppointment'
		@appendPopup new ProfessionalSchedule.Views[view]
			eventBus: @eventBus
			collections: @collections
			model: model
			parentView: @

	convertAppointment: (model) ->
		# remove standard fields
		model.set 'session_id', null, cancelled: true
		model.set 'availability_id', null, cancelled: true

		@appendPopup new ProfessionalSchedule.Views.EditAppointmentCustom
			eventBus: @eventBus
			collections: @collections
			model: model
			parentView: @

	appendedAppointment: (model) =>
		# remove old popup if exists
		@removePopup()
		eventData = model.getEventData()
		@newAppointmentId = eventData.entityId
		@newAppointmentDate = eventData.start.format('YYYY-MM-DD')
		@newAppointmentTime = eventData.start.format('HH:mm')
		ProfessionalSchedule.Calendar.fullCalendar 'renderEvent', eventData

	removedAppointment: (calendarAppointmentId) =>
		@removePopup()
		ProfessionalSchedule.Calendar.fullCalendar 'removeEvents', calendarAppointmentId

	appendedBlockedAvailability: (model) =>
		@removePopup()
		ProfessionalSchedule.Calendar.fullCalendar 'renderEvent', model.getEventData()

	removedBlockedAvailability: (calendarBlockedAvailabilityId) =>
		@removePopup()
		ProfessionalSchedule.Calendar.fullCalendar 'removeEvents', calendarBlockedAvailabilityId

	updateCalendarEvent: (model, eventId) ->
		if eventId
			events = ProfessionalSchedule.Calendar.fullCalendar 'clientEvents', 'blocked_' + eventId
		else
			events = ProfessionalSchedule.Calendar.fullCalendar 'clientEvents', model.getCalendarEventId()
		return false unless events.length

		event = model.updateCalendarEvent events[0]

		ProfessionalSchedule.Calendar.fullCalendar 'updateEvent', event

	hideTooltip: ->
		if @currentTooltip and @currentTooltip.hasClass 'tooltipstered'
			@currentTooltip.tooltipster('hide')

	bindScroll: ->
		@calendarScroll = 0
		$scroller = @$el.$calendar.find('.fc-scroller')
		scroll = $scroller.scrollTop()
		$scroller.on 'scroll', =>
			@calendarScroll = Math.abs(scroll - $scroller.scrollTop())
			if @calendarScroll > 10
				@hideTooltip()
				@calendarScroll = 0
				$scroller.off 'scroll'

	blockTime: (date) ->
		unless @checkProfileFilled('block') then return false

		view = new ProfessionalSchedule.Views.BlockTime
			eventBus: @eventBus
			date: if date? then date else $.fullCalendar.moment().add(1, 'hour')
			parentView: @
			collections: @collections

		@appendPopup view

	printSchedule: ->
		unless @print
			@print = new Print @$printPopup, 'all', 'schedule'
		@print.openPopup()

	editBlockedTime: (model) ->
		view = new ProfessionalSchedule.Views.EditBlockedTime
			eventBus: @eventBus
			parentView: @
			action: 'edit'
			model: model
			collections: @collections

		@appendPopup view

	unblockTime: (date, model) ->
		view = new ProfessionalSchedule.Views.EditBlockedTime
			eventBus: @eventBus
			date: date
			parentView: @
			action: 'unblock'
			model: model
			collections: @collections

		@appendPopup view

	checkProfileFilled: (type) ->
		unless @collections.locations.length
			AlertError('location', type)
			return false

		if type is 'appointment'
			unless @collections.services.length
				AlertError('service', type)
				return false

		if window.GLOBALS._HAS_PAYMENT? and !window.GLOBALS._HAS_PAYMENT
			AlertError('payment', type)
			return false

		return true

	toggleBlockTimeBtn: (display = null) -> ToggleEl(@$el.$blockTimeBtn, display)

	renderListWeekEvent: ($el, ev, calendarView) =>
		@listEventViews.push new ProfessionalSchedule.Views.ListWeekEvent
			eventBus     : @eventBus
			el           : $el
			parentView   : @
			model        : @collections.appointments.get(ev.entityId)
			collections  : @collections
			ev           : ev
			calendarView : calendarView

	preViewRenderCleanup: ->
		# remove old views
		_.each @listEventViews, (view) -> view.remove()
		@listEventViews = []
		@

	highlightToday: ($el) ->
		# there's no other way to highlight `today` in the header
		if $el.find('.fc-today').length
			weekday = $.fullCalendar.moment().format('ddd').toLowerCase()
			$el.find(".fc-#{weekday}").addClass('m-today')
		@

	getCalendarView: -> @$el.$calendar.fullCalendar('getView')

	refetchEvents: -> @$el.$calendar.fullCalendar('refetchEvents')

	rerenderEvents: (refetch = false) ->
		@refetchEvents() if refetch
		@$el.$calendar.fullCalendar('rerenderEvents')

	initEventsListView: ->
		@eventsListView = new ProfessionalSchedule.Views.EventsList
			el          : @$el.$eventsList
			parentView  : @
			eventBus    : @eventBus
			collections : @collections
		@


	selectAction: (date) ->
		# Popup with block time / custom appointmen options
		@appendPopup new ProfessionalSchedule.Views.SelectAction
			eventBus: @eventBus
			parentView: @
			date: date
			collections: @collections

module.exports = View

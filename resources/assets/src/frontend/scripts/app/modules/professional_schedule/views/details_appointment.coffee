Backbone = require 'backbone'
Moment = require 'moment'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Device = require 'utils/device'
vexDialog = require 'vexDialog'
Tooltip = require '../utils/tooltip'

require 'backbone.stickit'
require 'backbone.validation'
require 'moment-timezone'

class View extends Backbone.View

	alertTimeout: null

	messages:
		clientNotified: 'Client was successfully notified.'
		appointmentSaved: 'Appointment was saved successfully.'

	events:
		'click [data-details-reschedule-btn]' : 'editAppointment'
		'click [data-details-notify]'         : 'notifyClient'
		'click [data-header-name]'            : 'openClientProfile'
		'click [data-message-client]'         : 'messageClient'
		'click .schedule_info--remove_ask'    : 'removeAppointment'
		'click .schedule_info--remove_yes'    : 'confirmAppointmentRemoval'
		'click .schedule_info--remove_cancel' : 'cancelAppointmentRemoval'
		'click [data-cancel-appointment]'     : 'cancelAppointment'
		'click [data-confirm="yes"]'          : 'confirmAppointment'
		'click [data-confirm="no"]'           : 'declineAppointment'


	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common # add Mixins
		@setOptions options
		@bind()

	render: ->
		if @isPopup
			template = ProfessionalSchedule.Templates.DetailsAppointmentPopup
			@$el.addClass 'popup schedule_info_popup'
			@$el.attr 'data-popup', 'schedule_details'
			@$el.attr 'id', 'popup_schedule_details'

		else
			template = ProfessionalSchedule.Templates.DetailsAppointment
			@$el.addClass 'schedule_info'

		@$el.html template
			past: @model.get('past')
			pending: @model.get('status') == 'pending'

		@$el.$loading = $('.loading_overlay', @$el)
		@$el.$alert = @$el.find('.schedule_info--alert')
		@$el.$notify = @$el.find('.schedule_info--notify')
		@$el.$remove = @$el.find('.schedule_info--remove')
		@$el.$content = @$el.find('.schedule_info--content')

		Tooltip @$el
		@stickit()
		@

	bind: ->
		@bindings =
			'[data-header-userpic]':
				observe: 'avatar'
				updateMethod: 'html'
				onGet: (val) ->
					return '' unless val
					"<img src='#{val}' alt=''>"
			'[data-header-name]': 'full_name'
			'[data-header-details]':
				observe: 'phone'
				onGet: (val) -> val
				classes:
					'm-hide': (val) -> if val then false else true
			'[data-details-status]':
				observe: 'status'
				updateMethod: 'html'
				onGet: (val) => '<span class="m-' + val + '">' + @model.statuses[val] + '</span>'
			'[data-details-date]':
				observe: 'date'
				onGet: (val) -> val.format(ProfessionalSchedule.Config.displayDateTimeFormat)
			'[data-details-from]':
				observe: 'from'
				onGet: (val) -> val.format(ProfessionalSchedule.Config.displayTimeFormat)
			'[data-details-until]':
				observe: 'until'
				onGet: (val) -> val.format(ProfessionalSchedule.Config.displayTimeFormat)
			'[data-details-timezone]':
				observe: 'location.timezone'
				onGet: (val) ->
					guessedTz = Moment.tz.guess()
					Moment.tz(guessedTz).zoneAbbr()

			'[data-details-location-name]':
				observe: 'location.name'
				classes:
					'm-hide':
						observe: 'location.location_type'
						onGet: (val) -> val is 'home-visit'

			'[data-details-location-address]':
				observe: ['location.address', 'location.city', 'location.postal_code']
				onGet: (values) -> "#{values[0]}, #{values[1]} #{values[2]}"
				classes:
					'm-hide':
						observe: 'location.location_type'
						onGet: (val) -> val isnt 'home-visit'

			'[data-details-home-visit]':
				observe: 'location.location_type'
				onGet: (val) ->
					return '' unless val is 'home-visit'
					'home visit'
				classes:
					'm-hide':
						observe: 'location.location_type'
						onGet: (val) -> val isnt 'home-visit'

			'[data-details-service]':
				observe: 'service'
				updateMethod: 'html'
				onGet: (val) ->
					if val.name? then val.name else '&mdash;'

			'[data-details-package]':
				observe: 'package'
				onGet: (val) ->
					return '' unless val
					val.label
				classes:
					'm-hide':
						observe: 'package'
						onGet: (val) -> val is null

			'[data-details-notify]':
				classes:
					'm-hide':
						observe: ['should_confirm', 'from']
						onGet: (val) -> val[0] or val[1].isBefore($.fullCalendar.moment())

			'[data-details-confirm-row]':
				classes:
					'm-hide':
						observe: 'should_confirm'
						onGet: (val) -> !val

			'[data-details-status-row]':
				classes:
					'm-hide':
						observe: 'should_confirm'
						onGet: (val) -> val

			'[data-details-footer]':
				classes:
					'm-hide':
						observe: 'should_confirm'
						onGet: (val) -> val

			'[data-details-reschedule]':
				classes:
					'm-hide':
						observe: 'should_confirm'

	removeAppointment: (e) ->
		e.preventDefault() if e?
		@$el.$content.addClass 'm-remove'
		return @

	cancelAppointmentRemoval: (e) ->
		e.preventDefault() if e?
		@$el.$content.removeClass 'm-remove'
		return @

	confirmAppointmentRemoval: (e) ->
		e.preventDefault() if e?
		@cancelAppointmentRemoval()
		@showLoading()
		@collections.appointments.remove @model
		eventData = @model.getEventData()
		$.when(@model.remove()).then(
			(success) =>
				@hideLoading()
				@collections.appointments.trigger 'appointment:removed', eventData.id
			, (error) =>
				@hideLoading()
		)

	notifyClient: ->
		@showLoading()
		@$el.$notify.attr('disabled', true)
		@hideAlert()
		$.when(@model.notify()).then(
			(success) =>
				@hideLoading()
				@$el.$notify.attr('disabled', false)

				@showAlert @messages.clientNotified
			,
			(error) =>
				@hideLoading()
		)

	editAppointment: -> @calendarView.editAppointment @model

	hideAlert: ->
		@$el.$alert.removeClass('m-show')
		@alertTimeout = null

	showAlert: (msg) ->
		@$el.$alert.addClass('m-show')
		@$el.$alert.find('.alert').text(msg)
		@alertTimeout = setTimeout(
			=>
				@$el.$alert.removeClass('m-show')
			, 3000
		)

	messageClient: (e) ->
		e?.preventDefault()
		window.location = @model.messageUrl()

	openClientProfile: (e) ->
		e?.preventDefault()
		window.location = @model.profileUrl()

	cancelAppointment: (e) ->
		e?.preventDefault()
		vexDialog.confirm
			buttons: [
				$.extend {}, vexDialog.buttons.YES, {text: 'Yes'}
				$.extend {}, vexDialog.buttons.NO, {text: 'Cancel'}
			]
			message: '<span class="m-error">WARNING</span> Are you sure you want to remove this appointment?'
			callback: (status) =>
				return unless status
				@showLoading()
				@collections.appointments.remove @model
				eventData = @model.getEventData()
				$.when(@model.remove()).then(
					(success) =>
						@hideLoading()
						@collections.appointments.trigger 'appointment:removed', eventData.id
						@parentView.removePopup()
				, (error) =>
					@hideLoading()
				)
		@

	confirmAppointment: (e) ->
		e?.preventDefault()
		return unless @model.get('should_confirm')

		@showLoading()
		$.when(@model.confirm()).then(
			=>
				@hideLoading()
				# hacky way to remove the orange corner
				$('#event_' + @model.getCalendarEventId()).find('[data-pending-confirmation]').remove()
				@parentView.removePopup()
				@parentView.hideTooltip()
				@parentView.rerenderEvents(true)
		).fail(
			=>
				vexDialog.alert('cannot confirm appointment')
				@hideLoading()
		)

	declineAppointment: (e) ->
		e?.preventDefault()
		return unless @model.get('should_confirm')

		@collections.appointments.remove @model
		eventData = @model.getEventData()
		$.when(@model.decline()).then(
			=>
				@hideLoading()
				@collections.appointments.trigger 'appointment:removed', eventData.id
				@parentView.removePopup()
		).fail(
			=>
				vexDialog.alert('cannot decline appointment')
				@hideLoading()
		)

module.exports = View

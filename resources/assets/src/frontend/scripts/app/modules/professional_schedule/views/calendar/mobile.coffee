AbstractView = require('./abstract')

class View extends AbstractView

	eventRender: (ev, el, view) =>
		if ev.range?
			unless new Date(ev.start.format('YYYY-MM-DD')) >= new Date(ev.range.start.format('YYYY-MM-DD')) and new Date(ev.end.format('YYYY-MM-DD')) <= new Date(ev.range.end.format('YYYY-MM-DD'))
				return false
		$el = $(el)
		$el.data 'event', ev
		$el.attr 'id', "event_#{ev.id}"

		title = ''
		description = ''
		time = ''

		if ev.rendering is 'background'
			title = ev.title if ev.title
			description = ev.description if ev.description
			if view.type is 'agendaDay'
				time = ev.start.format('h(:mm)a') + ' - ' + ev.end.format('h(:mm)a')
		else
			switch view.type
				when 'month'
					$el.find('.fc-time').text("#{ev.start.format('h(:mm)a')}").addClass 'm-visible'
					$el.find('.fc-title').addClass 'm-normal'
				when 'agendaWeek', 'agendaDay'
					description = ev.description
					time = ev.start.format('h(:mm)a') + ' - ' + ev.end.format('h(:mm)a')
				when 'listWeek'
					@renderListWeekEvent($el, ev, view)

		$el.append "<div class='fc-title'>#{title}</div>" if title
		$el.append "<div class='fc-description'>#{description}</div>" if description
		$el.append "<div class='fc-event_time'>#{time}</div>" if time

		if ev.shouldConfirm
			$el.append '<span class="schedule--pending_confirm" data-pending-confirmation><i data-tooltip title="Confirm Appointment Request">?</i></span>'

		return


	fillEvents: (start, end, timezone, callback) =>

		availabilities = [].concat(
			@clearUnusedAvailabilities(@collections.locations.getAvailabilities(start))
			@collections.blocked_availabilities.getAvailabilities(start)
		)

		appointments = @collections.appointments.getAvailabilities(start)

		if @getCalendarView().type is 'month'
			# for month view on mobile we need to render only one appointment and it needs to be a full-day one
			appointments = _.uniq appointments, (appointment) -> appointment.start.format('YYYY-MM-DD')
			# update events data
			_.each appointments, (appointment) ->
				appointment.className = 'm-month_full'
				appointment.description = ''

		callback availabilities.concat(appointments)


	viewRender: (ev, $el) =>
		@refetchEvents()

		if @eventsListView
			@eventsListView.cleanup()
			@eventsListView.toggle(false)

		@preViewRenderCleanup()
		@highlightToday($el)
		@

	dayClick: (date, e, view) =>
		# for month view on mobile display a list of events
		if view.type is 'month'
			@openDayEventsDetails(date)
			return

		$target = $(e.target)
		if $target.hasClass('fc-title') or $target.hasClass('fc-event_time')
			$target = $target.parent()

		if $target.hasClass('fc-bgevent')
			now = $.fullCalendar.moment()
			now.stripZone() if now.hasZone()

			if now.diff(date, 'minutes') > 0
				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert 'You cannot start events in the past'
				return
			@addAppointment date, $target.data('event')
		else
			# Popup with block time / custom appointmen options
			@appendPopup new ProfessionalSchedule.Views.SelectAction
				eventBus: @eventBus
				parentView: @
				date: date
				collections: @collections

	eventClick: (eventDetails, e, view) =>
		if view.type is 'month'
			@dayClick eventDetails.start.startOf('day'), e, view
			return

		switch eventDetails.type
			when 'appointment'
				@openDetails eventDetails, 'appointments', 'DetailsAppointment'
			when 'availability'
				@appendPopup new ProfessionalSchedule.Views.SelectAction
					eventBus: @eventBus
					parentView: @
					type: 'is_blocked'
					model: @collections.blocked_availabilities.get eventDetails.availabilityId
					date: eventDetails.start

	openDetails: (details, collection = 'appointments', view = 'DetailsAppointment') ->
		@appendPopup new ProfessionalSchedule.Views[view]
			isPopup     : true
			model       : @collections[collection].get details.entityId
			collections : @collections
			eventBus    : @eventBus
			parentView  : @
			calendarView: @calendarView

	openDayEventsDetails: (date) ->
		if @eventsListView
			@eventsListView.cleanup()
		else
			@initEventsListView()

		@eventsListView.displayEvents date
		@highlightSelectedDate date

		@

	highlightSelectedDate: (date) ->
		@$el.$calendar.find('.m-selected').removeClass('m-selected')
		@$el.$calendar.find("[data-date='#{date.format('YYYY-MM-DD')}']").addClass('m-selected')
		@

module.exports = View

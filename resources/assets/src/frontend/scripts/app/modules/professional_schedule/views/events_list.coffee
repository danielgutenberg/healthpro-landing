Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
ToggleEl = require '../utils/toggle_el'
ScrollToElement = require 'utils/scroll_to_element'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common # add Mixins
		@setOptions options

		@render()
		@cacheDom()

	cacheDom: ->
		@$el.$table = @$el.find('table')
		@

	render: ->
		@$el.html '<table class="schedule_list_table"></table>'
		@

	toggle: (display = null) -> ToggleEl(@$el, display)

	cleanup: ->
		return unless @subViews?
		_.each @subViews, (view) -> view.remove()
		@subViews = []
		@

	displayEvents: (date) ->
		appointments = @collections.appointments.getForDate date

		unless appointments.length
			@toggle false
			return @

		_.each appointments, (model) =>
			@subViews.push view = new ProfessionalSchedule.Views.ListWeekEvent
				eventBus     : @eventBus
				tagName		 : 'tr'
				parentView   : @
				model        : model
				collections  : @collections
				ev           : model.getEventData()
				calendarView : @parentView
			@$el.$table.append view.el
			return

		@toggle true

		ScrollToElement @$el

		@


module.exports = View

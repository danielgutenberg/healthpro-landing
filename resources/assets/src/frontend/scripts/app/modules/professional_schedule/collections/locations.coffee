Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	bgColor: '#3db7cb'
	bgColorStep: 15

	initialize: ->
		super
		@getData()

		# we will need to have custom toJSON function for better usability when selecting locations.
		@useOptionsFormatter = false

		# if we have a service then we format the output with option labels
		@service = null

		@isCustomAppointment = true

	getData: ->
		$.ajax
			url: getApiRoute('ajax-provider-locations', {providerId: 'me'})
			method: 'get'
			type  : 'json'
			data: {}
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.forEach response.data, (item) =>
					@push
						id: item.id
						name: item.name
						address: item.address
						availabilities: item.availabilities
						location_type: item.location_type
						timezone: item.address.timezone

				@trigger 'data:ready'
			error: (error) =>

	getAvailabilities: (fullCalendarStartDate) ->
		availabilities = []
		_.each @models, (model) =>
			availabilities = availabilities.concat model.getAvailabilities(fullCalendarStartDate)
		availabilities

	getLocationTimezone: (id) ->
		location = _.findWhere @models, id: id
		location.get 'timezone'

	enableOptionsFormatter: ->
		@useOptionsFormatter = true
		@
	disableOptionsFormatter: ->
		@useOptionsFormatter = false
		@

	setService: (service = null) ->
		@service = service
		# we need to trigger this event to make sure select options are updated
		@trigger 'sort'
		@

	setCustomState: (isCustom) ->
		@isCustomAppointment = isCustom

	toJSON: ->
		return super unless @service
		@optionsWithLabelsJSON()

	optionsWithLabelsJSON: ->
		return [] unless @service

		serviceLabel = @service.get('name')
		otherLabel = 'Other Locations'
		locationIds = @service.get('location_ids')

		data =
			'opt_labels': [serviceLabel]
		data[serviceLabel] = []
		data[otherLabel] = []

		_.each @models, (model) =>
			if  _.indexOf(locationIds, model.get('id')) > -1
				data[serviceLabel].push model.toJSON()
			else
				data[otherLabel].push model.toJSON()

		# push otherLabel opt_group if it has length
		if @isCustomAppointment
			if data[otherLabel].length then data['opt_labels'].push(otherLabel)

		data

	customColor: -> @bgColor = @changeColorTone @bgColor, @bgColorStep

	changeColorTone: (col, amt) ->
		usePound = false
		if col[0] == '#'
			col = col.slice(1)
			usePound = true
		num = parseInt(col, 16)
		r = (num >> 16) + amt
		if r > 255
			r = 255
		else if r < 0
			r = 0
		b = (num >> 8 & 0x00FF) + amt
		if b > 255
			b = 255
		else if b < 0
			b = 0
		g = (num & 0x0000FF) + amt
		if g > 255
			g = 255
		else if g < 0
			g = 0
		(if usePound then '#' else '') + (g | b << 8 | r << 16).toString(16)


module.exports = Collection

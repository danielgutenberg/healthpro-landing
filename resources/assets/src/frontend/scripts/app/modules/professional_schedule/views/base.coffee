Backbone = require 'backbone'

class View extends Backbone.View

	el: $('[data-provider-schedule]')

	initialize: (@options) ->
		super

		@instances = []
		@eventBus = @options.eventBus
		@collections = @options.collections

		@cacheDom()
		@render()
		@bind()

	cacheDom: ->
		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	render: ->
		@appendParts()

	removeLoading: ->
		@$el.$loading.remove()

	bind: ->
		@listenTo @calendar, 'data:loaded', =>
			@removeLoading()

	appendParts: ->
		@appendSync()
		@appendCalendar()

	appendCalendar: ->
		@instances.push @calendar = new ProfessionalSchedule.Views.Calendar
			eventBus: @eventBus
			collections: @collections
			$container: @$el
			baseView: @

	appendSync: ->
		@instances.push @sync = new ProfessionalSchedule.Views.ScheduleSync
			eventBus: @eventBus
			$container: @$el
			baseView: @

module.exports = View

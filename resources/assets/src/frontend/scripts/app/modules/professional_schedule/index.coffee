Handlebars = require('handlebars')
Backbone = require('backbone')
Device = require('utils/device')

window.ProfessionalSchedule =

	Config: require('./config/config')

	Messages: require('./config/messages')

	# we need fullCalendar entity to check for overlapped events
	# better to store it here then to request it from the view
	Calendar: null

	Views:
		Base: require('./views/base')
		Calendar: do ->
			if Device.isMobile()
				require('./views/calendar/mobile')
			else
				require('./views/calendar/desktop')
		CalendarHeader: require('./views/calendar_header')
		DetailsAppointment: require('./views/details_appointment')
		AddAppointment: require('./views/add_appointment')
		EditAppointment: require('./views/edit_appointment')
		EditAppointmentCustom: require('./views/edit_appointment_custom')
		BlockTime: require('./views/block_time')
		EditBlockedTime: require('./views/edit_blocked_time')
		SelectAction: require('./views/select_action')
		ScheduleSync: require('./views/schedule_sync')
		ListWeekEvent: require('./views/list_week_event')
		EventsList: require('./views/events_list')

	Collections:
		Appointments: require('./collections/appointments')
		Locations: require('./collections/locations')
		Clients: require('./collections/clients')
		Services: require('./collections/services')
		BlockedAvailabilities: require('./collections/blocked_availabilities')

	Models:
		Appointment: require('./models/appointment')
		Location: require('./models/location')
		Lock: require('./models/lock')
		BlockedAvailability: require('./models/blocked_availability')

	Templates:
		Calendar: Handlebars.compile require('text!./templates/calendar.html')
		CalendarHeader: Handlebars.compile do ->
			if Device.isDesktop()
				require('text!./templates/calendar_header/desktop.html')
			else
				require('text!./templates/calendar_header/mobile.html')
		DetailsAppointment: Handlebars.compile require('text!./templates/details_appointment.html')
		DetailsAppointmentPopup: Handlebars.compile require('text!./templates/details_appointment_popup.html')
		AddAppointment: Handlebars.compile require('text!./templates/add_appointment.html')
		EditAppointment: Handlebars.compile require('text!./templates/edit_appointment.html')
		EditAppointmentCustom: Handlebars.compile require('text!./templates/edit_appointment_custom.html')
		BlockTime: Handlebars.compile require('text!./templates/block_time.html')
		UnblockTime: Handlebars.compile require('text!./templates/unblock_time.html')
		SelectAction: Handlebars.compile require('text!./templates/select_action.html')
		SelectActionUnblock: Handlebars.compile require('text!./templates/select_action_unblock.html')
		CalendarListWeekEvent: Handlebars.compile require('text!./templates/calendar_list_week_event.html')

class ProfessionalSchedule.App
	constructor: ->

		require.ensure [], =>
			require('fullcalendar')
			require('fullcalendar/dist/fullcalendar.min.css')

			@eventBus = _.extend {}, Backbone.Events

			@collections =
				appointments: new ProfessionalSchedule.Collections.Appointments [],
					model: ProfessionalSchedule.Models.Appointment
					eventBus: @eventBus
				locations: new ProfessionalSchedule.Collections.Locations [],
					model: ProfessionalSchedule.Models.Location
				clients: new ProfessionalSchedule.Collections.Clients()
				services: new ProfessionalSchedule.Collections.Services()
				blocked_availabilities: new ProfessionalSchedule.Collections.BlockedAvailabilities [],
					model: ProfessionalSchedule.Models.BlockedAvailability

			@views =
				base: new ProfessionalSchedule.Views.Base
					eventBus: @eventBus
					collections: @collections

ProfessionalSchedule.app = new ProfessionalSchedule.App()

module.exports = ProfessionalSchedule

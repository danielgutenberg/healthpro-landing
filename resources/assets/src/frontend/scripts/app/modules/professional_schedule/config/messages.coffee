module.exports =
	Save    :
		success: 'Appointment was successfully updated'
		error  : 'Something went wrong. Please reload the page and try again.'
	Warnings:
		empty                   : 'You should fill time/date for required fields.'
		empty_time              : 'You should fill time for required fields.'
		empty_date              : 'You should fill date for required fields.'
		overlap                 : 'The appointment overlaps other appointments.'
		hours                   : 'The appointment is outside your normal hours.'
		locations               : 'You have chosen a different location than the one in your schedule for this time period.'
		service_location        : 'Chosen service is not available in the selected location.'
		appointment_location    : 'You already have an appointment at another location.'
		date_time               : 'The end date of the date range should not be before the start date. Please reselect your date range'
		date_time_equals        : 'You have selected the same start time and end time. Please select a new date range.'
		day_without_availability: 'You can\'t block the day that has no availability'
		no_location             :
			appointment: 'You can\'t create an appointment until you set up a location'
			block      : 'You can\'t block time until you set up a location'
			button_text: 'Set up location'
		no_service              :
			appointment: 'You can\'t create an appointment until you set up a service'
			block      : 'You can\'t block time until you set up a service'
			button_text: 'Set up service'
		no_payment              :
			appointment: 'You can\'t create an appointment until you enter your debit card or bank information'
			button_text: 'Add payment method'

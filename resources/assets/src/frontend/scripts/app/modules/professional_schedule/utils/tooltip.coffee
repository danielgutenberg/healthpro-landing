Device = require 'utils/device'
require 'tooltipster'

module.exports = ($el) ->
	return if Device.isMobile()
	$el.find('[data-tooltip]:not(.tooltipstered)').tooltipster
		theme: 'tooltipster-dark'
	@

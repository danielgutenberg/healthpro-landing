require 'jquery-ui/autocomplete'

Backbone = require 'backbone'
Numeral = require 'numeral'
Datepicker = require 'framework/datepicker'
Select = require 'framework/select'
Timepicker = require 'framework/timepicker'
BaseEditAppointmentView = require './base_edit_appointment'
Config = require('../config/config')
require 'backbone.stickit'
require 'backbone.validation'

class View extends BaseEditAppointmentView

	attributes:
		'class': 'popup schedule_details'
		'data-popup': 'schedule_details'
		'id' : 'popup_schedule_details'

	events:
		'click .schedule_details--save': 'saveAppointment'
		'click .schedule_details--block_time--button': 'blockTime'

	initialize: (@options) ->
		super
		# reset service when we open the popup
		@collections.locations.setService null

		@eventBus = @options.eventBus
		@parentView = @options.parentView
		@clientId = @options.clientId

		@model = new ProfessionalSchedule.Models.Lock
			location_id: @options.locationId
			duration: ProfessionalSchedule.Config.defaultAppointmentDuration
			# we don't care about the time here
			date: @options.date.clone().set
				h: 0
				m: 0
				s: 0
		if @clientId?
			if @options.from?
				@model.set
					# we just set default time for the next hour from now
					from: @options.date.clone().set
						m: 0
						s: 0
			@model.set
				client_id: @clientId
		else
			if @options.from?
				@model.set
					# we don't care about the seconds here
					from: @options.from.clone().set
						s: 0

		# Now all the appointments are custom
		@model.set 'custom', true

		########################
		# Stickit Collections
		########################
		@times = @collections.appointments.getTimeOptions()
		@durations = @collections.appointments.getDurationOptions()
		# sessions will be populated dynamically depending on the selected session
		# collection is used to auto-populate select box
		@sessions = new Backbone.Collection()

		@bind()
		@initValidation()

	blockTime: ->
		@parentView.blockTime(@options.date)

	render: ->
		@$el.html ProfessionalSchedule.Templates.AddAppointment()

		@$el.$alertCustom = @$el.find('.schedule_details--alert_custom')
		@$el.$serviceRow = @$el.find('[data-service-row]')
		@$el.$clientDetailsRow = @$el.find('[data-client-details]')

		@afterRender()
		@afterModelChange()

		# update service and session if needed
		@setServiceSession()
		@

	bind: ->
		@listenTo @model, 'change:from change:date change:duration change:service_id change:session_id change:location_id', @afterModelChange
		@listenTo @model, 'change:from change:date change:duration change:service_id change:session_id change:location_id', @updateModelAvailability
		@listenTo @model, 'check:email', @checkEmail
		@listenTo @model, 'checked:email', @checkedEmail
		@listenTo @eventBus, 'add_appointment:switch_to_custom', @switchAppointmentTypeYes

		@bindings =
			'[name="service_id"]':
				observe: 'service_id'
				selectOptions:
					collection: @collections.services
					labelPath: 'detailed_name'
					valuePath: 'id'
					defaultOption:
						label: 'Please select service'
						value: null
				onSet: @onServiceSet
				initialize: ($el) =>
					@$el.$service = $el
					@selects.service = new Select @$el.$service

			'[name="session_id"]':
				observe: 'session_id'
				selectOptions:
					collection: @sessions
					labelPath: 'label'
					valuePath: 'id'
					defaultOption:
						label: 'Custom session'
						value: null
				initialize: ($el) =>
					@$el.$session = $el
					@selects.session = new Select @$el.$session
				onSet: @onSessionSet

			'[name="location_id"]':
				observe: 'location_id'
				selectOptions:
					collection: @collections.locations
					labelPath: 'name'
					valuePath: 'id'
					defaultOption:
						label: 'Please select location'
						value: null
				initialize: ($el) =>
					@$el.$location = $el
					@selects.location = new Select @$el.$location

			'[name="payment_method"]':
				observe: ['allow_in_person', 'accepted_methods']
				onGet: (val) ->
					val[0]
				onSet: (val) ->
					val[0]

			'.payment-method':
				visible: (val) ->
					if GLOBALS.profile?.accepted_payment_methods and GLOBALS.profile.accepted_payment_methods == 'online'
						return true
					return false

			'[name="client"]':
				observe: 'client'
				initialize: ($el) =>
					@$el.$autocomplete = $el.closest('[data-autocomplete]')
					@$el.$autocomplete.input = @$el.$autocomplete.find('input')
					@clientId = @model.get 'client_id'
					if @clientId?
						@clientName = @collections.clients.findWhere({'id': @clientId})?.get 'name'
						@$el.$autocomplete.input.val @clientName

					$el.autocomplete
						appendTo: @$el.$autocomplete.find('[data-autocomplete-results]')
						source: @collections.clients.source()
						select: (e, ui) =>
							$el.val(ui.item.label).trigger('change')
							@model.set 'client_id', ui.item.value
							@model.set 'email', null
							false
						change: (e, ui) =>
							return unless ui.item # all the email checks are pefrormed in the `search` event
							@model.set
								'email_exists': null
								'email': null
								'client_id': ui.item.value
						focus: (e, ui) =>
							$el.val(ui.item.label) # make keyboard navigation works
							@value = ui.item.label
							e.preventDefault()

						search: (e, ui) =>
							return unless @model.isEmail(e.target.value) # do not perform check unless we have a valid email
							@model.trigger 'check:email'
							@model.set
								'email_exists': false
								'email': null
								'client_id': null
							$.when(@model.emailExists(e.target.value)).then(
								(response) =>
									@model.trigger 'checked:email'
									@model.set
										'email_exists': response.data.exists
										'email': e.target.value
										'client_id': null
								(error) =>
									@$el.$autocomplete.removeClass('m-loading')
									@model.set
										'email_exists': false
										'email': null
							)

			'[name="date"]':
				observe: 'date'
				onGet: @onDateGet
				onSet: @onDateSet
				initialize: ($el) =>
					@$el.$datepicker = $el.closest('[data-datepicker]')
					@datepicker = new Datepicker @$el.$datepicker,
						minDate: 0
						dateFormat: ProfessionalSchedule.Config.datepickerPluginFormat

			'[name="from"]':
				observe: 'from'
				initialize: ($el) =>
					timepicker = new Timepicker $el,
						inputName: 'time_from'
						submitLabel: 'Select'
				onGet: @onTimeGet
				onSet: @onTimeSet

			'[name="duration"]':
				observe: 'duration'
				selectOptions:
					collection: @durations
					comparator: (val) => val.value
				initialize: ($el) =>
					@$el.$duration = $el
					@selects.duration = new Select @$el.$duration
				onSet: @onDurationSet

			'[data-details-duration]':
				observe: 'duration'
				onGet: (val) -> @collections.appointments.durations[val]

			'[name="price"]':
				observe: 'price'
				initialize: ($el) =>
					@$el.$price = $el
				onSet: @onPriceSet
				onGet: (val) -> val.format(ProfessionalSchedule.Config.priceRawFormat)

			'[data-details-price]':
				observe: 'price'
				onGet: (val) -> val.format(ProfessionalSchedule.Config.priceFormat)

			'[data-client-details]':
				observe: ['client_id', 'email', 'email_exists']
				visible: (values, options) => !values[0] and values[1] and !values[2]
				visibleFn: ($el, isVisible, options) =>
					if isVisible
						@$el.$clientDetailsRow.removeClass('m-hide')
					else
						@$el.$clientDetailsRow.addClass('m-hide')

			'[name="first_name"]':
				observe: 'first_name'

			'[name="last_name"]':
				observe: 'last_name'

			'[data-location-timezone]':
				observe: 'location_id'
				visible: true

			'[data-location-timezone] b':
				observe: 'location_id'
				onGet: (val) ->
					@collections.locations.getLocationTimezone val if val

	saveAppointment: ->
		@validateData()
		return if @errors.length

		unless @model.isCustom() or @model.canSaveStandard()
			@showConvertMessage()
			return

		@showLoading()
		appendClient = if @model.get('email') then true else false
		$.when(@model.save()).then(
			(response) =>
				@hideLoading()
				appointment = new @collections.appointments.model @collections.appointments.formatAppointment(response.data, true)
				@collections.appointments.appendAppointment appointment
				@collections.clients.appendClient response.data.client if appendClient
			(error) =>
				@hideLoading()
		)

	onDateGet: (val) -> val.format(ProfessionalSchedule.Config.datepickerFormat)
	onDateSet: (val) -> $.fullCalendar.moment(val, ProfessionalSchedule.Config.datepickerFormat)

	onTimeSet: (val, options) =>
		time = val.split(':')
		unless time[0] == '--' or time[1] == '--'
			newVal = @model.get('date').clone()
			newVal.set
				h: time[0]
				m: time[1]
			return newVal
		val

	onTimeGet: (val) ->
		if val?
			return val.format('HH:mm')
		val

	onServiceSet: (val) =>
		@updateSessionsCollection val
		@resetLocations val
		@$el.$session.trigger 'change'
		val

	onSessionSet: (val) =>
		return val unless val
		# update price and duration from session values
		session = @sessions.get val
		model_price = @model.get('price')
		price = Numeral(session.get('price'))
		duration = session.get('duration')

		# set only if value differs to preven recursive onSet call
		unless model_price.value() is price.value() then @model.set 'price', price
		unless @model.get('duration') == duration then @model.set 'duration', duration

		# we need to trigger the change event for the select2
		@$el.$duration.trigger('change')

		val

	onDurationSet: (val) =>
		# update price when changing duration
		session = @sessions.findWhere({'duration': val})

		if session
			model_price = @model.get('price')
			price = Numeral(session.get 'price')
			unless model_price.value() is price.value()
				@model.set 'price', Numeral(session.get 'price')
				@$el.$price.trigger('change')

			unless session.get('id') == @model.get('session_id')
				@model.set 'session_id', session.get('id')
				@$el.$session.trigger 'change'

		else
			# reset session selectbox if duration is custom
			@$el.$session.val('')
			@$el.$session.trigger 'change'

		val

	onPriceSet: (val) =>
		# reset session selectbox if price is custom
		price = Numeral(val).format('0.00')
		session = @sessions.findWhere({'price': price})

		unless session
			@$el.$session.val('')
			@$el.$session.trigger 'change'

		return Numeral(val)

	resetLocations: (service_id) ->
		serviceModel = @collections.services.get(service_id)
		location_ids = if serviceModel? then serviceModel.get 'location_ids'
		current_location_id = @model.get 'location_id'
		unless _.contains(location_ids, current_location_id)
			@model.set 'location_id', null
			@$el.$location.trigger('change')

	resetAppointmentValuesAfterSwitch: (fillValues = false) ->

		if fillValues
			attributes = @model.toJSON()
		else
			attributes =
				price: Numeral(0)
				duration: 0
				service_id: null
				location_id: null

		attributes.session_id = null
		attributes.availability_id = null

		@model.set attributes

		@$el.$service.trigger 'change'
		@$el.$session.trigger 'change'
		@$el.$location.trigger 'change'
		@$el.$duration.trigger 'change'

	updateModelAvailability: ->
		@model.updateAvailability @currentAvailability

	checkEmail: ->
		@$el.$autocomplete.addClass('m-loading').find('input').attr('readonly', true)
		@$el.$save.attr('disabled', true)

	checkedEmail: ->
		@$el.$autocomplete.removeClass('m-loading').find('input').attr('readonly', false)
		@$el.$save.attr('disabled', false)

	setServiceSession: ->
		if @collections.services.length
			@model.set 'service_id', @collections.services.at(0).get('id')
			@$el.$service.trigger 'change'

		# Set first session to prepopulate price and duration
		if @sessions.at(0)
			@model.set 'session_id', @sessions.at(0).get('id')

		@$el.$session.trigger 'change'

		if @collections.locations.length == 1
			@model.set 'location_id', @collections.locations.at(0).get('id')
			@$el.$location.trigger 'change'

module.exports = View

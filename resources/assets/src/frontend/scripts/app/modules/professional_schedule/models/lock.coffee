Backbone = require 'backbone'
Validation = require 'backbone.validation'
vexDialog = require 'vexDialog'
Numeral = require 'numeral'
getApiRoute = require 'hp.api'

class LockModel extends Backbone.Model

	defaults:
		client_id: null
		email: null
		email_exists: null
		first_name: null
		last_name: null
		location_id: null
		service_id: null # we don't send it to the server. used only for model binding
		session_id: null
		availability_id: null
		client: null # we don't send it to the server. used only for model binding
		duration: null
		price: Numeral(0)
		allow_in_person: false
		date: null
		from: null
		custom: false # used to check if the new appointment should use custom values
		force_overlap: true # we allow the provider to overlap events when needed

	validation:
		client_id: (value, attr, computedState) ->
			return if computedState.email
			return 'Please enter your client\'s name' unless value

		email: (value, attr, computedState) ->
			return if computedState.client_id
			return 'Please enter a valid email address' unless @isEmail(value)

		location_id:
			required: true
			msg: 'Please add your location'

		service_id: (value) ->
			return if @get 'custom'
			'Please select the appointment service' unless value

		session_id: (value) ->
			return if @get 'custom'
			'Please select the appointment session' unless value

		price: (value) ->
			return if @get 'custom'
			'Please add the appointment price' if value.value() < 1

		date: (value) ->
			'Please select valid date' if $.fullCalendar.moment().diff(value, 'days') > 0

		from: (value) ->
			# if value is still a string that means the time has not been fully set to a momentjs time object
			if _.isString(value)
				return 'Please select a time'

		first_name: (value, attr, computedState) ->
			return if computedState.client_id or computedState.email_exists
			'First name is required' unless value

		last_name: (value, attr, computedState) ->
			return if computedState.client_id or computedState.email_exists
			'Last name is required' unless value

	save: ->
		data =
			provider_id: 'me'
			from: @get('from').format(ProfessionalSchedule.Config.dateTimeFormat)
			force_overlap: @get('force_overlap')
			type: 'provider'
			_token: window.GLOBALS._TOKEN

		if @get('client_id')
			data.client_id = @get('client_id')
		else
			data.email = @get('email')
			unless @get('email_exists')
				data.first_name = @get('first_name')
				data.last_name = @get('last_name')

		# add custom fields
		if @get('custom') or !@get('availability_id')
			data.price = @get('price').value()
			data.until = @get('from').clone().add(@get('duration'), 'minutes').format(ProfessionalSchedule.Config.dateTimeFormat)
			data.location_id = @get('location_id')
			data.service_id = @get('service_id')
		# add standard fields
		else
			data.availability_id = @get('availability_id')
			data.session_id = @get('session_id')

		data.allow_in_person   =  @get('allow_in_person')

		$.ajax
			url: getApiRoute('ajax-appointments')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify data
			show_alerts: false
			error: (res) ->
				message = 'Please make sure that you have accepted our Terms and Conditions and that you have a current debit card set up to receive payments.'
				if res.responseJSON?.errors?.error?
					if res.responseJSON.errors.error.messages[0] == 'Please make sure you have an active debit card to receive payments'
						message = 'You can only book an appointment once we have a way to pay you. Please enter a debit card under My Settings-> Payments'
					else
						message = res.responseJSON.errors.error.messages[0]

				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: message

	syncDates: ->
		unless _.isString(@get('from'))
			@get('from').set
				year: @get('date').get('year')
				month: @get('date').get('month')
				date: @get('date').get('date')

	updateAvailability: (eventData) ->
		# we set availability only if availability location is the same as the model location
		if eventData and eventData.entityId is @get('location_id')
			@set 'availability_id', eventData.availabilityId
		else
			@set 'availability_id', null

	isCustom: -> @get('custom')

	canSaveStandard: -> @get('availability_id') and !@isCustom()

	emailExists: (email) ->
		$.ajax
			url: getApiRoute('ajax-profiles-exists', {
				email: email
			})
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'

	isEmail: (value) ->
		value and value.match(Backbone.Validation.patterns.email)

module.exports = LockModel

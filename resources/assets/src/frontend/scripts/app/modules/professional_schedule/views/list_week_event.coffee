Backbone = require 'backbone'
Moment = require 'moment'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'
Device = require 'utils/device'

require 'backbone.stickit'
require 'moment-timezone'

class View extends Backbone.View

	messages:
		clientNotified: 'Client was successfully notified.'
		appointmentSaved: 'Appointment was saved successfully.'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common # add Mixins
		@setOptions options

		@addEvents()

		@bind()
		@render()
		@cacheDom()

	addEvents: ->
		if Device.isMobile()
			@events =
				'click' : 'openDetails'
		else
			@events =
				'click [data-action="reschedule"]' : 'editAppointment'
				'click [data-action="notify"]'     : 'notifyClient'
				'click [data-action="message"]'    : 'messageClient'
				'click [data-action="cancel"]'     : 'removeAppointment'
				'click [data-confirm="yes"]'       : 'confirmAppointment'
				'click [data-confirm="no"]'        : 'declineAppointment'

		@delegateEvents()

	bind: ->
		@bindings =
			'[data-details-status]':
				observe: 'status'
				updateMethod: 'html'
				onGet: (val) => '<span class="m-' + val + '">' + @model.statuses[val] + '</span>'

			'[data-details-date]':
				observe: 'date'
				onGet: (val) -> val.format(ProfessionalSchedule.Config.displayDateTimeFormat)

			'[data-details-from]':
				observe: 'from'
				onGet: (val) -> val.format(ProfessionalSchedule.Config.displayTimeFormat)

			'[data-details-until]':
				observe: 'until'
				onGet: (val) -> val.format(ProfessionalSchedule.Config.displayTimeFormat)

			'[data-details-timezone]':
				observe: 'location.timezone'
				onGet: (val) ->
					guessedTz = Moment.tz.guess()
					Moment.tz(guessedTz).zoneAbbr()

			'[data-details-location-name]':
				observe: 'location.name'
				classes:
					'm-hide':
						observe: 'location.location_type'
						onGet: (val) -> val is 'home-visit'

			'[data-details-location-address]':
				observe: ['location.address', 'location.city', 'location.postal_code']
				onGet: (values) -> "#{values[0]}, #{values[1]} #{values[2]}"
				classes:
					'm-hide':
						observe: 'location.location_type'
						onGet: (val) -> val isnt 'home-visit'

			'[data-details-home-visit]':
				observe: 'location.location_type'
				onGet: (val) ->
					return '' unless val is 'home-visit'
					'home visit'
				classes:
					'm-hide':
						observe: 'location.location_type'
						onGet: (val) -> val isnt 'home-visit'

			'[data-details-service]':
				observe: 'service'
				updateMethod: 'html'
				onGet: (val) ->
					if val.name? then val.name else '&mdash;'

			'[data-details-package]':
				observe: 'package'
				onGet: (val) ->
					return '' unless val
					val.label
				classes:
					'm-hide':
						observe: 'package'
						onGet: (val) -> val is null

			'[data-action="notify"]':
				classes:
					'm-hide':
						observe: ['should_confirm', 'from']
						onGet: (val) -> val[0] or val[1].isBefore($.fullCalendar.moment())

			'[data-details-confirm]':
				classes:
					'm-hide':
						observe: 'should_confirm'
						onGet: (val) -> !val

			'[data-details-actions]':
				classes:
					'm-hide':
						observe: 'should_confirm'
						onGet: (val) -> val

			'[data-action="reschedule"]':
				classes:
					'm-hide':
						observe: 'status'
						onGet: (val) -> val != 'pending'

			'[data-action="cancel"]':
				classes:
					'm-hide':
						observe: 'from'
						onGet: (val) -> val.isBefore($.fullCalendar.moment())

	render: ->
		# do not render block times in list
		unless @model
			@$el.html ''
			return

		data = @model.toJSON()
		data.profile_url = @model.profileUrl()
		data.time = @ev.start.format('h(:mm)a') + ' - ' + @ev.end.format('h(:mm)a')
		data.is_mobile = Device.isMobile()
		data.pending = data.status == 'pending'

		@$el.html ProfessionalSchedule.Templates.CalendarListWeekEvent(data)
		@$el.addClass('schedule_list_details')

		@stickit()
		@

	cacheDom: ->
		@$el.$loading = @$el.find('.loading_overlay')
		@

	messageClient: ->
		window.location = @model.messageUrl()
		@

	editAppointment: ->
		@parentView.editAppointment @model
		@

	notifyClient: ->
		@showLoading()
		$.when(@model.notify()).then(
			(success) =>
				@hideLoading()

				vexDialog.alert 'Reminder email to ' + @model.get("full_name") + ' was successfully sent'
		,
			(error) =>
				@hideLoading()
		)

	removeAppointment: ->
		vexDialog.confirm
			buttons: [
				$.extend {}, vexDialog.buttons.YES, {text: 'Yes'}
				$.extend {}, vexDialog.buttons.NO, {text: 'Cancel'}
			]
			message: '<span class="m-error">WARNING</span> Are you sure you want to remove this appointment?'
			callback: (status) =>
				@confirmRemoveAppointment() if status
		@


	confirmRemoveAppointment: ->
		@showLoading()
		@collections.appointments.remove @model
		eventData = @model.getEventData()
		$.when(@model.remove()).then(
			(success) =>
				@hideLoading()
				@collections.appointments.trigger 'appointment:removed', eventData.id
		, (error) =>
			@hideLoading()
		)

	openDetails: ->
		# Popup with block time / custom appointmen options
		@calendarView.appendPopup new ProfessionalSchedule.Views.DetailsAppointment
			isPopup     : true
			model       : @model
			collections : @collections
			eventBus    : @eventBus
			parentView  : @
			calendarView: @calendarView

		@


	confirmAppointment: (e) ->
		e?.preventDefault()
		return unless @model.get('should_confirm')

		@showLoading()
		$.when(@model.confirm()).then(
			=>
				@hideLoading()
				$('#event_' + @model.getCalendarEventId()).find('[data-pending-confirmation]').remove()
				@parentView.rerenderEvents(true)
		).fail(
			=>
				vexDialog.alert('cannot confirm appointment')
				@hideLoading()
		)

	declineAppointment: (e) ->
		e?.preventDefault()
		return unless @model.get('should_confirm')

		vexDialog.confirm
			buttons: [
				$.extend {}, vexDialog.buttons.YES, {text: 'Yes'}
				$.extend {}, vexDialog.buttons.NO, {text: 'Cancel'}
			]
			message: '<span class="m-error">WARNING</span> Are you sure you want to remove this appointment?'
			callback: (status) =>
				@confirmDeclineAppointment() if status

	confirmDeclineAppointment: ->
		@showLoading()
		$.when(@model.decline()).then(
			=>
				@hideLoading()
				@collections.appointments.trigger 'appointment:confirmed', @model.getEventData().id
		).fail(
			=>
				vexDialog.alert('cannot decline appointment')
				@hideLoading()
		)


module.exports = View

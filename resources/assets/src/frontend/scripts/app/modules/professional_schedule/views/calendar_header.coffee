Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
ToggleEl = require '../utils/toggle_el'
Device = require 'utils/device'
CurrentTimezone = require '../utils/current_timezone'

class View extends Backbone.View

	events:
		'click [data-add-appointment-ask]'     : 'addAppointmentAsk'
		'click .schedule--actions--custom'     : 'addAppointment'
		'click .schedule--actions--block_time' : 'blockTime'
		'click .schedule--actions--print'      : 'printSchedule'
		'click [data-dropdown-menu-toggle]'	   : 'toggleDropdownMenu'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common # add Mixins
		@setOptions options
		@render()

		if Device.isMobile()
			@initMobile()

	render: ->
		@$el.html ProfessionalSchedule.Templates.CalendarHeader({timezone: CurrentTimezone()})
		@

	addAppointment: ->
		@calendarView.addAppointment null, null
		return

	blockTime: ->
		@calendarView.blockTime null

	printSchedule: ->
		@calendarView.printSchedule()

	addAppointmentAsk: ->
		@calendarView.selectAction null

	toggleDropdownMenu: ->
		ToggleEl @$el, null, 'm-opened'

	initMobile: ->
		# move google calendar button to the dropdown menu
		@calendarView.baseView.sync.initMobile @$el.find('[data-dropdown-menu-sync]')
		@


module.exports = View

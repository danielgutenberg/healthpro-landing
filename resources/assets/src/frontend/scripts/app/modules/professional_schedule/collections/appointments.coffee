Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Numeral = require 'numeral'

class Collection extends Backbone.Collection

	durations:
		5 : '5 mins'
		10 : '10 mins'
		15 : '15 mins'
		20 : '20 mins'
		25 : '25 mins'
		30 : '30 mins'
		35 : '35 mins'
		40 : '40 mins'
		45 : '45 mins'
		50 : '50 mins'
		55 : '55 mins'
		60 : '1 Hour'
		65 : '1 Hour 5 mins'
		70 : '1 Hour 10 mins'
		75 : '1 Hour 15 mins'
		80 : '1 Hour 20 mins'
		85 : '1 Hour 25 mins'
		90 : '1 Hour 30 mins'
		95 : '1 Hour 35 mins'
		100 : '1 Hour 40 mins'
		105 : '1 Hour 45 mins'
		110 : '1 Hour 50 mins'
		115 : '1 Hour 55 mins'
		120 : '2 Hours'
		125 : '2 Hours 5 mins'
		130 : '2 Hours 10 mins'
		135 : '2 Hours 15 mins'
		140 : '2 Hours 20 mins'
		145 : '2 Hours 25 mins'
		150 : '2 Hours 30 mins'
		155 : '2 Hours 35 mins'
		160 : '2 Hours 40 mins'
		165 : '2 Hours 45 mins'
		170 : '2 Hours 50 mins'
		175 : '2 Hours 55 mins'
		180 : '3 Hours'

	initialize: ->
		super
		@getData()
		@bind()

	bind: ->
		@listenTo @, 'appointment:append', @appendAppointment
		@listenTo @, 'appointment:remove', @removeAppointment

	appendAppointment: (model) ->
		@add model
		@trigger 'appointment:appended', model

	removeAppointment: (model) ->
		@remove model

	getData: ->
		$.ajax
			url: getApiRoute('ajax-provider-appointments', {providerId: 'me'})
			method: 'get'
			type  : 'json'
			data:
				status: 'confirmed,pending'
				return_fields: 'all'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.each response.data, (item) =>
					@push @formatAppointment(item)
				@trigger 'data:ready'

	formatAppointment: (item) ->
		unless item.service_id
			item.service_id = ProfessionalSchedule.app.collections.services.findBySessionId(item.session_id)?.get('id') or null

		id: item.id
		client_id: item.client_id
		location_id: item.location_id
		session_id: item.session_id
		service_id: item.service_id
		full_name: item.client.full_name
		avatar: item.client.avatar
		phone: item.client.phone
		location:
			name: item.location.name
			address: item.location.address.address
			city: item.location.address.city
			postal_code: item.location.address.postal_code
			timezone: item.location.address.timezone
			location_type: item.location.location_type
		date: $.fullCalendar.moment(item.date)
		from: $.fullCalendar.moment(item.from_utc)
		until: $.fullCalendar.moment(item.until_utc)
		from_utc: $.fullCalendar.moment(item.from_utc)
		until_utc: $.fullCalendar.moment(item.until_utc)
		initiated_by: item.initiated_by
		status: item.status
		mark: item.mark
		past: item.past
		price: Numeral(item.price)
		service: item.service
		should_confirm: item.status is 'pending' and item.initiated_by is 'client'
		package: do ->
			return null unless item.package?
			{
				auto_renew: item.package.auto_renew
				entities_left: item.package.entities_left
				number_of_entities: item.package.package.number_of_entities
				package_id: item.package.package.id
				charge_id: item.package.id
				expires_on: $.fullCalendar.moment(item.package.expires_on)
				start_time: $.fullCalendar.moment(item.package.start_time)
				entity_id: item.package.package.entity_id
				entity_type: item.package.package.entity_type
				price: item.package.package.price
				number_of_months: item.package.package.number_of_months
				type: if item.package.package.number_of_entities > -1 then 'standard' else 'recurring'
				label: if item.package.package.number_of_entities > -1 then 'Package' else 'Monthly Package'
			}

	getForDate: (date) -> @filter (model) -> model.get('from').isSame(date, 'day')

	getAvailabilities: -> @map (model) -> model.getEventData()

	getTimeOptions: ->
		initialTime = $.fullCalendar.moment().set
			hour: 0
			minute: 0
			second: 0
		intervals = 24 * 60 / ProfessionalSchedule.Config.slot
		i = 0
		times = []
		while i < intervals
			initialTime.add ProfessionalSchedule.Config.slot, 'minutes' if i
			times.push
				label: initialTime.format(ProfessionalSchedule.Config.displayTimeFormat)
				value: initialTime.format(ProfessionalSchedule.Config.timeFormat)
			i++
		times

	getDurationOptions: ->
		_.map @durations, (label, value) =>
			label: label
			value: parseInt value

module.exports = Collection

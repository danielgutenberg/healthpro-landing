DeepModel = require 'backbone.deepmodel'
getApiRoute = require 'hp.api'
getRoute = require 'hp.url'

class AppointmentModel extends DeepModel

	initialState: null

	statuses:
		confirmed: 'Confirmed'
		pending: 'Pending'

	defaults:
		id: null
		client_id: null
		location_id: null
		service_id: null
		session_id: null
		availability_id: null
		full_name: ''
		avatar: ''
		location: null
		phone: ''
		date: ''
		from: ''
		until: ''
		status: null
		price: null
		package: null
		service: null
		should_confirm: false

	validation:
		location_id:
			required: true
			msg: 'Please add your location'
		price: (value, attr, computedState) ->
			return 'Please add the appointment price' if value.value() < 0
		date: (value, attr, computedState) ->
			return 'Please select valid date' if $.fullCalendar.moment().diff(value, 'days') > 0

	getEventData: ->
		data =
			id: @getCalendarEventId()
			type: 'appointment'
			entityId: @get('id')
			locationId: @get('location_id')
			title: @get('full_name')
			description: @get('location').name
			className: 'm-appointment'
			start: @get('from')
			end: @get('until')
			shouldConfirm: @get('should_confirm')

		# we don't have service names in the model so we need to grab it from the services collection.
		service = ProfessionalSchedule.app.collections.services.findByServiceId @get('service_id')
		serviceHtml = if service then service.get('name') + '<br>' else ''
		data.description = serviceHtml + data.description

		if @get('status') is 'pending'
			data.className += ' m-pending'

		data

	remove: (destroy = true) ->
		$.ajax
			url: getApiRoute('ajax-appointment', appointmentId: @get('id'))
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: =>
				if destroy
					@set 'id', null
					@destroy()

	notify: ->
		$.ajax
			url: getApiRoute('ajax-appointment-notify', {
				appointmentId: @get('id')
			})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

	canNotify: -> @get('status') is 'confirmed'

	save: ->
		data =
			from: @get('from').format(ProfessionalSchedule.Config.dateTimeFormat)
			_token: window.GLOBALS._TOKEN

		if @get('availability_id')
			data.session_id = @get('session_id')
			data.availability_id = @get('availability_id')
		else
			data.until = @get('until').format(ProfessionalSchedule.Config.dateTimeFormat)
			data.service_id = @get('service_id')
			data.location_id = @get('location_id')
			data.price = @get('price').value()

		$.ajax
			url: getApiRoute('ajax-appointment', appointmentId: @get('id'))
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify data

	getCalendarEventId: -> 'appointment_' + @get('id')

	updateCalendarEvent: (event) ->
		# convert time back to local time to show on calendar
		localOffset = @get('from').utcOffset()
		locationOffset = @get('from').clone().tz(@get('location').timezone).utcOffset()
		timeToAdd = localOffset - locationOffset

		event.start = @get('from').add(timeToAdd, 'minutes')
		event.end = @get('until').add(timeToAdd, 'minutes')
		event

	isCustom: -> @get('session_id') == null

	syncDates: (date = null) ->
		date = @get('date').clone() unless date
		updatedDate =
			year: date.get('year')
			month: date.get('month')
			date: date.get('date')
		@get('from').set updatedDate
		@get('until').set updatedDate

	saveInitialState: (force = false) ->
		# force save state
		@resetInitialState() if force
		# we don't want to save initial state if one exists
		@initialState = @toJSON() unless @initialState
	resetInitialState: ->
		@initialState = null
	applyInitialState: (options = {}) ->
		@set @initialState, options if @initialState

	messageUrl: -> getRoute 'dashboard-conversation', profileId: @get('client_id')

	profileUrl: -> getRoute 'client-public-profile-url', slug: @get('client_id')

	hasPackage: -> @get('package')?


	confirm: ->
		$.ajax
			url: getApiRoute('ajax-appointment-confirm', appointmentId: @get('id'))
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: =>
				@set('should_confirm', false)
				@set('status', 'confirmed')

	decline: -> @remove()




module.exports = AppointmentModel

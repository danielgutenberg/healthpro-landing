Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: ->
		super
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-provider-clients', {providerId: 'me'})
			method: 'get'
			type  : 'json'
			data: {}
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.forEach response.data, (item) =>
					@push
						id: item.id
						name: item.full_name
						avatar: item.avatar
						phone: item.phone

				@trigger 'data:ready'

	source: ->
		_.map @models, (model) ->
			label: model.get('name')
			value: model.get('id')

	appendClient: (item) ->
		@push
			id: item.id
			name: item.full_name
			avatar: item.avatar
			phone: item.phone

module.exports = Collection

Device = require 'utils/device'

module.exports =
	firstDay                   : 0
	timezone                   : 'local'
	slot                       : 10 # 10 minute slots
	slotDuration               : '00:30:00' # used in fullCalendar only
	defaultAppointmentDuration : 30
	priceFormat                : '$0,0[.]00'
	priceRawFormat             : '0,0[.]00'
	scrollTime                 : '08:00:00'

	datepickerFormat       : 'MM/DD/YYYY' # used for MomentJS
	datepickerPluginFormat : "mm/dd/yy" # used for datepicker plugin
	datepickerDayFormat    : "DD, MM dd, yy" # used for datepicker plugin (long)
	dateTimeFormat         : 'YYYY-MM-DD HH:mm:ss'
	displayTimeFormat      : 'h:mm a'
	timeFormat             : 'HH:mm'
	displayDateTimeFormat  : 'ddd, MMM Do, YYYY'

	PaymentOptions: [
		{
			label: 'Client can pay only in-person'
			value: 'in_person'
		}
		{
			label: 'Client can pay only online'
			value: 'online'
		}
		{
			label: 'Client can pay online or in person'
			value: 'any'
		}
	]

	Calendar :
		height            : 600
		eventLimit        : true
		defaultView       : 'agendaWeek'
		slotDuration      : '00:30:00'
		firstDay          : 0
		timezone          : 'local'
		slotEventOverlap  : false
		scrollTime        : '08:00:00'
		allDaySlot        : false
		titleFormat       : "MMMM D, YYYY"
		columnFormat      : 'ddd'
		views             :
			basic  : {}
			agenda :
				eventLimit : if Device.isMobile() then 1 else 2
			week   :
				columnFormat : 'D ddd'
			day    : {}
		slotLabelFormat   : do ->
			if Device.isMobile()
				'h A'
			else
				'h:mm A'
		slotLabelInterval : '01:00:00'
		header            : do ->
			if Device.isMobile()
				{
					left   : 'today'
					center : 'prev title next'
					right  : 'listWeek month agendaWeek agendaDay'
				}
			else
				{
					center : 'title'
					left   : 'today prev next'
					right  : 'listWeek month agendaWeek agendaDay'
				}

Moment = require 'moment'
require 'moment-timezone'

module.exports = ->
	now = $.fullCalendar.moment()
	guessedZone = Moment.tz.guess()

	Moment.tz(now, guessedZone).format('z')

module.exports = ($el, display = null, className = 'm-hide') ->
	switch display
		when true
			$el.removeClass className
		when false
			$el.addClass className
		else
			$el.toggleClass className
	$el

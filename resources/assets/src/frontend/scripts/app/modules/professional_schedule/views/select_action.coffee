Backbone = require 'backbone'

class View extends Backbone.View

	attributes:
		'class': 'popup schedule_details m-select_action'
		'data-popup': 'schedule_details'
		'id' : 'popup_schedule_details'

	events:
		'click .schedule_details--select_action--custom': 'newAppointment'
		'click .schedule_details--select_action--block_time': 'blockTime'
		'click .schedule_details--select_action--edit_blocked': 'editBlockedTime'
		'click .schedule_details--select_action--unblock_time': 'unblockTime'

	initialize: (options) ->
		@date = options.date
		@parentView = options.parentView
		@type = options.type
		@model = options.model

	render: ->
		if @type == 'is_blocked'
			@$el.html ProfessionalSchedule.Templates.SelectActionUnblock()
		else
			@$el.html ProfessionalSchedule.Templates.SelectAction
				selected_date: if @date then @date.format('dddd, MMMM D, YYYY') else null
		@

	newAppointment: ->
		@parentView.addAppointment @date

	blockTime: ->
		@parentView.blockTime @date

	editBlockedTime: ->
		@parentView.editBlockedTime @model

	unblockTime: ->
		@parentView.unblockTime @date, @model

module.exports = View

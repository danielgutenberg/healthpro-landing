Backbone = require 'backbone'
Stickit = require 'backbone.stickit'
Validation = require 'backbone.validation'
Numeral = require 'numeral'
Datepicker = require 'framework/datepicker'
Select = require 'framework/select'
Timepicker = require 'framework/timepicker'
BaseEditAppointmentView = require './base_edit_appointment'

class View extends BaseEditAppointmentView

	events:
		'click .schedule_details--save': 'saveAppointment'
		'click .schedule_details--remove_ask': 'removeAppointmentAsk'
		'click .schedule_details--remove_yes': 'removeAppointmentConfirm'
		'click .schedule_details--remove_no': 'removeAppointmentCancel'

	initialize: ->
		super

		# update initial availability
		@initialAvailability = @findAppointmentLocationBetweenDates @model.get('from'), @model.get('until')

		########################
		# Stickit Collections
		########################
		@times = @collections.appointments.getTimeOptions()
		@durations = @collections.appointments.getDurationOptions()

		@bind()
		@initValidation()

	bind: ->
		super

		# show warning if we have overlapped events
		@listenTo @model, 'change:from change:date change:until change:service_id change:price change:location_id', @afterModelChange
		@listenTo @model, 'change:location_id', @afterLocationChange

		@bindings =
			'[name="service_id"]':
				observe: 'service_id'
				selectOptions:
					collection: @collections.services
					labelPath: 'name'
					valuePath: 'id'
				defaultOption:
					label: 'Unspecified'
					value: null
				initialize: ($el) =>
					@$el.$service = $el
					@selects.service = new Select @$el.$service
				onSet: => null # changing this field will fail

			'[name="location_id"]':
				observe: 'location_id'
				selectOptions:
					collection: @collections.locations
					labelPath: 'name'
					valuePath: 'id'
				initialize: ($el) =>
					@$el.$location = $el
					@selects.location = new Select @$el.$location
				onSet: => null # changing this field will fail

			'[data-edit-price]':
				observe: 'price'
				onGet: (val) -> val.format(ProfessionalSchedule.Config.priceFormat)

			'[name="date"]':
				observe: 'date'
				initialize: ($el) =>
					@$el.$datepicker = $el.closest('[data-datepicker]')
					@datepicker = new Datepicker @$el.$datepicker,
						minDate: 0
						dateFormat: ProfessionalSchedule.Config.datepickerPluginFormat
				onGet: @onDateGet
				onSet: @onDateSet

			'[name="from"]':
				observe: 'from'
				initialize: ($el) =>
					timepicker = new Timepicker $el,
						inputName: 'time_from'
						submitLabel: 'Select'
						$target: $el.parent()
				onGet: @onTimeGet
				onSet: @onTimeSet

			'[data-edit-client]': 'full_name'

			'[data-edit-duration]':
				observe: 'until'
				onGet: @onDurationGet

	render: ->
		@$el.html ProfessionalSchedule.Templates.EditAppointmentCustom()
		@afterRender()
		@afterModelChange()
		@

	onDateGet: (val) => val.format(ProfessionalSchedule.Config.datepickerFormat)

	# since we're storing the dates in string we need to make sure all the times are syncronized
	onDateSet: (val) =>
		date = $.fullCalendar.moment(val, ProfessionalSchedule.Config.datepickerFormat)
		updatedDate =
			year: date.get('year')
			month: date.get('month')
			date: date.get('date')
		@model.get('from').set updatedDate
		@model.get('until').set updatedDate
		date

	onTimeGet: (val) =>
		# display time in the local time of the location
		time = val.clone().tz(@model.get('location').timezone)
		time.format(ProfessionalSchedule.Config.timeFormat)

	# when we change `from` time we need to update `until` time as well
	onTimeSet: (val) =>
		date = @model.get('date').clone()
		time = val.split(':')
		duration = @model.get('until').diff(@model.get('from'), 'minutes')
		date.set
			h: time[0]
			m: time[1]
		@model.set 'from', date
		@model.set 'until', date.clone().add(duration, 'minutes')
		date

	# we set duration from the `until`-`from` difference
	onDurationGet: (val) => @collections.appointments.durations[@model.get('until').diff(@model.get('from'), 'minutes')]

	saveAppointment: ->
		@validateData()
		return if @errors.length
		@edit()

	# we don't need this check for custom appointments
	checkServicesLocation: -> true

module.exports = View

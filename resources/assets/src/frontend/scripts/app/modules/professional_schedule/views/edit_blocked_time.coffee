require 'jquery-ui/autocomplete'

Backbone = require 'backbone'
Datepicker = require 'framework/datepicker'
Select = require 'framework/select'
Timepicker = require 'framework/timepicker'

require 'backbone.stickit'
require 'backbone.validation'

class View extends Backbone.View

	attributes:
		'class': 'popup schedule_details'
		'data-popup': 'schedule_details'
		'id' : 'popup_schedule_details'

	events:
		'click .schedule_details--save': 'editBlockedTime'
		'click .schedule_details--unblock': 'unblockTime'

	initialize: (@options) ->
		super
		@eventBus = @options.eventBus
		@date = @options.date
		@parentView = @options.parentView
		@action = @options.action
		@collections = @options.collections
		@model = @options.model

		@model.fillDatesTimes()

		@bind()
		@initValidation()

	cacheDom: ->
		@$el.$loading = @$el.find('.loading_overlay')

	showLoading: -> @$el.$loading.removeClass('m-hide')
	hideLoading: -> @$el.$loading.addClass('m-hide')

	bind: ->
		@bindings =
			'[name="date_from"]':
				observe: 'date_from'
				onGet: @onDateGet
				onSet: @onDateSet
				initialize: ($el) =>
					@$el.$datepicker = $el.closest('[data-datepicker]')
					@datepicker = new Datepicker @$el.$datepicker,
						minDate: 0
						dateFormat: ProfessionalSchedule.Config.datepickerPluginFormat
						selectOtherMonths: true

			'[name="time_from"]':
				observe: 'time_from'
				onGet: @onTimeGet
				onSet: @onTimeSet
				initialize: ($el) =>
					timepicker = new Timepicker $el,
						inputName: 'time_from'
						submitLabel: 'Select'

			'[name="date_until"]':
				observe: 'date_until'
				onGet: @onDateGet
				onSet: @onDateSet
				initialize: ($el) =>
					@$el.$datepicker = $el.closest('[data-datepicker]')
					@datepicker = new Datepicker @$el.$datepicker,
						minDate: $.fullCalendar.moment(@model.get('date_from')).format(ProfessionalSchedule.Config.datepickerFormat) # don't allow to select a day before the date_from
						dateFormat: ProfessionalSchedule.Config.datepickerPluginFormat
						selectOtherMonths: true

			'[name="time_until"]':
				observe: 'time_until'
				onGet: @onTimeGet
				onSet: @onTimeSet
				initialize: ($el) =>
					timepicker = new Timepicker $el,
						inputName: 'time_until'
						submitLabel: 'Select'

			'[name="description"]': 'description'

	render: ->
		# Switch template depeding on action
		if @action == 'unblock'
			@$el.html ProfessionalSchedule.Templates.UnblockTime()
		else
			@$el.html ProfessionalSchedule.Templates.BlockTime
				edit: true

		@cacheDom()
		@stickit()
		@hideLoading()
		@

	onDateGet: (val) ->
		if val then val.format(ProfessionalSchedule.Config.datepickerFormat)

	onDateSet: (val) -> $.fullCalendar.moment(val, ProfessionalSchedule.Config.datepickerFormat)

	onTimeSet: (val, options) =>
		time = val.split(':')
		newVal = @model.get('time_from').clone()
		newVal.set
			h: time[0]
			m: time[1]
		newVal

	onTimeGet: (val) ->
		# timepicker works with 5 min step, so we have to round the minutes (in case when can get :59:59 from backend)
		remainder = val.minutes() % 5
		if val then val.subtract(remainder, 'minutes').format('HH:mm')

	validateData: ->
		@errors = []
		errors = @model.validate()
		@errors.push errors if errors?
		if @errors.length
			@$el.find('.schedule_details--block--header').addClass('m-error')
		else
			@$el.find('.schedule_details--block--header').removeClass('m-error')

	checkDates: ->
		errors = @model.checkDates()
		if errors? # push to @errors to check for errors before saving
			@errors.push errors
			window.Alerts.addNew('error', errors.error)

	initValidation: ->
		buildSelector = (view, attr, selector) ->
			attr = 'client' if attr is 'client_id' or attr is 'email'
			if attr.indexOf('.') isnt -1
				attr = attr.split('.')
				$field = view.$el.find('[' + selector + '*="' + attr[0] + '[' + attr[1] + ']"]')
			else
				$field = view.$el.find('[' + selector + '*="' + attr + '"]')

		Backbone.Validation.bind @,
			valid: (view, attr, selector) ->
				$field = buildSelector(view, attr, selector)
				$field.parents('.field, .select')
					.removeClass('m-error')
					.find('.field--error')
					.html('')

			invalid: (view, attr, error, selector) ->
				$field = buildSelector(view, attr, selector)
				$field.parents('.field, .select')
					.addClass('m-error')
					.find('.field--error')
					.html(error)
				$field.trigger('error')
				$field.on('focus.validation', ->
					$field.parents('.field, .select').removeClass('m-error')
					$field.off('focus.validation')
					)

	unblockTime: ->
		@showLoading()
		@collections.blocked_availabilities.remove @model
		$.when(@model.unblockTime()).then(
			(response) =>
				@hideLoading()
				_.each response.data, (availability) =>
					@collections.blocked_availabilities.trigger 'blocked_availability:removed', 'blocked_' + availability.id
			(error) =>
				@hideLoading()
		)

	editBlockedTime: ->
		@validateData()
		@checkDates()
		return if @errors.length
		@showLoading()
		$.when(@model.update()).then(
			(response) =>
				@hideLoading()
				_.each response.data, (availability) =>
					@parentView.updateCalendarEvent @model, availability.id
				@parentView.removePopup()
			(error) =>
				@hideLoading()
		)

module.exports = View

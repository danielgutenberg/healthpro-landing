Backbone = require 'backbone'
Shepherd = require('shepherd')
Device 	 = require('utils/device')
getRoute = require 'hp.url'

class View extends Backbone.View

	initialize: ->
		@bind()

	bind: ->
		$(document).on 'tour:start', => @initTour()
		$('[data-start-tour]').on 'click', => @initTour()

	initTour: ->
		unless @tour?
			@tour = new Shepherd.Tour
				defaults:
					classes: if Device.isMobile() then 'tour m-mobile' else 'tour'
					showCancelLink: false

			@bindTourSteps()
			@addTourSteps()

		if Device.isMobile()
			$(document).trigger 'slideout:open'
			waitSlideout = setInterval(=>
				if $('body').hasClass 'm-slideout'
					@tour.start()
					clearInterval(waitSlideout)
			, 100)
		else
			@tour.start()

	finish: ->
		@remove()
		if Device.isMobile()
			$(document).trigger 'slideout:close'

	bindTourSteps: ->
		Shepherd.on 'start', =>
			$el = do ->
				if Device.isMobile()
					return $('[data-usermenu] .sidebar_nav--item.m-profile')
				else
					return $('.sidebar .sidebar_nav--item.m-profile')

			$el.find('a').trigger('click') unless $el.hasClass('m-opened')

		Shepherd.on 'complete',  => @finish()
		Shepherd.on 'cancel', 	 => @finish()

	navigateUrl: (url) ->
		@tour.cancel()
		Dashboard.trigger 'content:loading'
		window.location.href = url

	addTourSteps: ->
		return unless @tour

		parentSelector = do ->
			if Device.isMobile()
				return '[data-usermenu]'
			else
				return '.sidebar'

		showForPayments = $('.header--trigger_btn.m-receive_payments').length

		buttons =
			skip:
				text: 'Skip'
				action: @tour.cancel
				classes: 'm-link'
			next:
				text: 'Next'
				action: @tour.next
			finish:
				text: 'Finish'
				action: @tour.next
			not_now:
				text: 'Not Now'
				action: @tour.cancel
				classes: 'm-link'
			receive_payments:
				text: 'Receive Payments'
				action: (e) => @navigateUrl(getRoute('dashboard-settings-payment'))

		@tour.addStep 'location',
			title		: 'Get started building your profile by adding locations and services'
			text		: 'Now is your opportunity to let clients get to know you better and learn more about your services.'
			scrollTo 	: false
			attachTo	:
				element: "#{parentSelector} .sidebar_nav--sub--item.m-locations_services"
				on: if Device.isMobile() then 'bottom' else 'right'
			buttons: [buttons.skip, buttons.next]

		unless Device.isMobile()
			@tour.addStep 'view_profile',
				title		: 'View your online profile'
				text		: 'Send your clients a direct link to your public profile. They will be able to learn more about you and easily schedule an appointment with you.'
				scrollTo 	: false
				attachTo	:
					element: "#{parentSelector} .sidebar_card--email"
					on: 'right'
				buttons: [buttons.skip, buttons.next]

		@tour.addStep 'schedule',
			title		: 'Your calendar'
			text		: 'Set up your schedule, mark your availability, block off times when you are unavailable, add padding time between sessions and so much more.'
			scrollTo 	: false
			attachTo	:
				element: "#{parentSelector} .sidebar_nav--item.m-schedule"
				on: if Device.isMobile() then 'bottom' else 'right'
			buttons: if showForPayments then [buttons.skip, buttons.next] else [buttons.finish]

		unless Device.isMobile() or !showForPayments
			@tour.addStep 'receive_payments',
				title		: 'Start Receiving Payments'
				text		: 'Set up your payment settings so you can start accepting payments from clients.'
				scrollTo 	: false
				attachTo	:
					element: ".header--trigger_btn.m-receive_payments"
					on: 'bottom'
				buttons: [buttons.not_now, buttons.receive_payments]

module.exports = View

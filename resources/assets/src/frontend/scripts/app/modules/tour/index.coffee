Handlebars = require 'handlebars'

# list of all instances
require '../../../../styles/modules/tour.styl'

window.DashboardTour = DashboardTour =
	Views:
		Base: require('./views/base')

class DashboardTour.App
	constructor: ->
		@view = new DashboardTour.Views.Base()

new DashboardTour.App()

module.exports = DashboardTour

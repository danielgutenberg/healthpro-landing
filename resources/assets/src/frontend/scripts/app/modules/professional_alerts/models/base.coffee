Backbone = require 'backbone'
Moment = require 'moment'

class Model extends Backbone.Model

	defaults:
		remaining_trial: null
		remaining_credit: null

	initialize: ->
		@bind()

	bind: ->
		return unless Dashboard?
		@listenTo Dashboard, 'profile:loaded', @setData

	setData: (model) ->
		@set 'remaining_credit', model.get('profile.available_credit')

		# this date is returned as UTC
		commissionFreePeriod = Moment.utc model.get('profile.commission_free_period')
		now = Moment.utc()

		# check if trial has ended
		if commissionFreePeriod.isAfter(now, 'days')
			@set 'remaining_trial', commissionFreePeriod.diff(now, 'days')

		@trigger 'data:ready'

module.exports = Model

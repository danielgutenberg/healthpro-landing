Handlebars = require 'handlebars'

window.ProfessionalAlerts =
	Models:
		Base: require('./models/base')
	Views:
		Base: require('./views/base')
	Templates:
		RemainingCreditTrial:  Handlebars.compile require('text!./templates/remaining_credit_trial.html')

class ProfessionalAlerts.App
	constructor: ->
		@model = new ProfessionalAlerts.Models.Base()
		@view = new ProfessionalAlerts.Views.Base
			model: @model

new ProfessionalAlerts.App()

module.exports = ProfessionalAlerts

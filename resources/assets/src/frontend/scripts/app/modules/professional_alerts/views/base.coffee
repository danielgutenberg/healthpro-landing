Backbone = require 'backbone'
Numeral = require 'numeral'
Formats = require 'config/formats'

require 'backbone.stickit'

class View extends Backbone.View

	className: 'alert m-full m-show m-trial'

	initialize: (options) ->
		@model = options.model

		@cacheDom()
		@addStickit()
		@bind()

	bind: ->
		@listenTo @model, 'data:ready', @render

	addStickit: ->
		@bindings =
			'.prompt-trial--days':
				observe: 'remaining_trial'
				updateMethod: 'html'
				onGet: (val) ->
					return '' unless val
					val += " day" + if val > 1 then "s" else ""
					"""
						<h3 class="prompt-trial--title">Free Trial</h3>
						<p class="prompt-trial--label">You have <strong>#{val}</strong> commission free period left</p>
					"""

			'.prompt-trial--credit':
				observe: 'remaining_credit'
				updateMethod: 'html'
				onGet: (val) ->
					return '' unless val
					val = Numeral(val).format(Formats.Price.Default)
					"""
						<h3 class="prompt-trial--title m-credit">#{val}</h3>
						<p class="prompt-trial--label">Available Credit</p>
					"""

	cacheDom: ->
		@$container = $('.layout--content--inner')

	render: ->
		if @model.get('remaining_credit') or @model.get('remaining_trial')
			@$el.html ProfessionalAlerts.Templates.RemainingCreditTrial()
			@$container.prepend @$el
			@stickit()
		else
			@remove()

module.exports = View

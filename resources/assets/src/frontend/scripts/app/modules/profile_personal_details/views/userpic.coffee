Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

Userpic = require 'framework/userpic'
getApiRoute = require 'hp.api'
Mixins = require 'utils/backbone_mixins'
EditImages = require '../../profile_edit/edit_image'

class UserpicView extends Backbone.View

	events:
		'click .js-edit': (e) ->
			@initImageEdit(e)

	attributes:
		'class': 'js-userpic'
		'data-options': JSON.stringify
			"_token": window.GLOBALS._TOKEN
			"hasUpload": true
			"uploadUrl": getApiRoute('ajax-profiles-upload-file', {'id': window.GLOBALS._PID})

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common )

		@setOptions(options)
		@profileId = window.GLOBALS.PROFILE_ID
		@userpicImage = @$el.find('img')

		@addListeners()

	addListeners: ->
		@listenTo @model, 'data:inited', (data) =>
			@$('.userpic img')
				.attr('src', data.avatar)
				.data('original', data.avatar_original)
			@showUserpicEdit()

		@listenTo @, 'rendered', =>
			@userpic = new Userpic( @$el )
			@showUserpicEdit()

		$(document).on 'image:uploaded', =>
			@showUserpicEdit()

	render: ->
		@$el.html PersonalDetails.Templates.UserpicProfile
			"avatar": @data.avatar
			"avatar_original": @data.avatar_original
		@trigger 'rendered'
		return @

	renderData: ->
		return {
			"_token": window.GLOBALS._TOKEN
			"uploadUrl": getApiRoute('ajax-profiles-upload-file', {'id': @profileId})
		}

	showUserpicEdit: ->
		if @$el.find('img').attr('src') != '' && @$el.find('img').attr('src') != undefined
			@$el.find('.userpic--edit').removeClass 'm-hide'

	removePopup: ->
		window.popupsManager.closePopup('edit_image')
		$('#popup_edit_image').remove()
		window.popupsManager.popups = []
		@$popup = null

		$('body').css
			'overflow-x': 'initial'

	initImageEdit: (e) ->
		@originalImageUrl = @$el.find('.userpic--inner img').data 'original'

		@editor = new EditImages(@originalImageUrl, {aspectRatio: 1 / 1}, @saveEdited)

	saveEdited: (blob) =>
		@userpic.uploadEdited( blob )

module.exports = UserpicView

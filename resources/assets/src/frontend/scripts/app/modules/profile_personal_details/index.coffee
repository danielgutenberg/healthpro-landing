Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'
FormView = require './views/form'
UserpicView = require './views/userpic'

# models
BaseModel = require './models/base'

# templates
FormProfileTmpl = require 'text!./templates/form-profile.html'
FormInlineTmpl = require 'text!./templates/form-inline.html'
UserpicProfileTmpl = require 'text!./templates/userpic-profile.html'

window.PersonalDetails =
	Models:
		Base: BaseModel
	Views:
		Base: BaseView
		Form: FormView
		Userpic: UserpicView
	Templates:
		FormProfile: Handlebars.compile FormProfileTmpl
		FormInline: Handlebars.compile FormInlineTmpl
		UserpicProfile: Handlebars.compile UserpicProfileTmpl

_.extend PersonalDetails, Backbone.Events

class PersonalDetails.App
	constructor: (options) ->
		options.model = @model = new PersonalDetails.Models.Base
			type: options.type
			data: options.data

		@view = new PersonalDetails.Views.Base options

		return {
			view: @view
			model: @model
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

module.exports = PersonalDetails

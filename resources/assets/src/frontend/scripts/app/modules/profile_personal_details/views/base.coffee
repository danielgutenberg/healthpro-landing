Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'

Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View
	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions(options)
		@addListeners()
		@initParts()

	initParts: ->
		@initForm()
		@initUserpic()
		@

	addListeners: ->
		@listenTo @model, 'data:saved', =>
			@updateUserName()

	initUserpic: ->

		@$userpic = @$form.find('.edit_personal--userpic_container') unless @$userpic

		if @$userpic.length
			@subViews.push @userpic = new PersonalDetails.Views.Userpic
				model: @model
				type: @type
				data: @data

			@$userpic.html @userpic.render().el

		@

	initForm: ->
		elOptions = @getFormElOptions()

		@subViews.push @form = new PersonalDetails.Views.Form
			model: @model
			type: @type
			className: elOptions.className

		@$form.html @form.render().el

		@

	getFormElOptions: ->
		switch @type
			when 'profile'
				{
					className: 'personal_details--data_inner'
				}
			when 'inline'
				{
					className: 'profile_edit_inline--form'
				}


	sendForm: ->
		@model.save() if !@model.validate()

	# update user name on the page (sidebar, header)
	updateUserName: ->
		fullName = @model.get('first_name') + ' ' + @model.get('last_name')

		#sidebar
		$('.sidebar_card--user_name').text(fullName)

		#user menu
		$('.user_menu--opener_text').text(fullName)
		$('.user_menu--user').eq(0).find('.user_menu--user_name dt strong').text(fullName)


module.exports = View

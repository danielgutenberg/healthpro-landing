Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

Maxlength = require 'framework/maxlength'
Select = require 'framework/select'

require 'backbone.validation'
require 'backbone.stickit'

class FormView extends Backbone.View

	initialize: (options)->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common

		@setOptions options
		@addListeners()
		@addValidation()
		@addStickit()

	addListeners: ->
		@listenTo @model, 'change:dob_day change:dob_month change:dob_year', => @model.updateBirthday()
		@listenTo @model, 'change:birthday', => @model.updateDOB()

	addStickit: ->
		@bindings = {}

		switch @type
			when 'profile', 'inline'
				@bindings['[name="first_name"]'] = 'first_name'
				@bindings['[name="last_name"]'] = 'last_name'
				@bindings['[name="gender"]'] = 'gender'
				@bindings['[name="title"]'] =
					observe: 'title'
					onSet: (val) =>
						if val == 'Mr.'
							@model.set('gender', 'male')
						if val == 'Ms.'
							@model.set('gender', 'female')
						val

		if @type is 'inline'
			@bindings['[name="languages"]'] =
				observe: 'tags.languages'
				initialize: ($el) ->
					new Select $el
					return @
				selectOptions:
					collection: @model.get('language_options')
					labelPath: 'text'
					valuePath: 'value'

		if @type is 'profile' and window.GLOBALS._PTYPE is 'client'
			@bindings['[name="occupation"]'] =
				observe: 'occupation'

			@bindings['[name="dob_year"]'] =
				observe: 'dob_year'
				selectOptions:
					collection: @yearOptions()
					defaultOption:
						label: 'Year'
						value: ''

			@bindings['[name="dob_month"]'] =
				observe: 'dob_month'
				selectOptions:
					collection: @monthOptions()
					defaultOption:
						label: 'Month'
						value: ''

			@bindings['[name="dob_day"]'] =
				observe: 'dob_day'
				selectOptions:
					collection: @dayOptions()
					defaultOption:
						label: 'Day'
						value: ''

	render: ->
		switch @type
			when 'profile'
				@renderForProfile()
			when 'inline'
				@renderForInline()

		@stickit @model
		@

	renderForProfile: ->
		data = @model.toJSON()
		data.is_client = window.GLOBALS._PTYPE is 'client'

		@$el.html PersonalDetails.Templates.FormProfile data
		@

	renderForInline: ->
		@$el.html PersonalDetails.Templates.FormInline @model.toJSON()
		@

	dayOptions: ->
		days = []
		start = 1

		days.push(start++) while start <= 31

		return days

	yearOptions: ->
		years = []
		start = 1920
		current = new Date().getFullYear() - 13

		years.push(current--) while start <= current

		return years

	monthOptions: ->
		return [
			{
				value: 1
				label: "January"
			}
			{
				value: 2
				label: "February"
			}
			{
				value: 3
				label: "March"
			}
			{
				value: 4
				label: "April"
			}
			{
				value: 5
				label: "May"
			}
			{
				value: 6
				label: "June"
			}
			{
				value: 7
				label: "July"
			}
			{
				value: 8
				label: "August"
			}
			{
				value: 9
				label: "September"
			}
			{
				value: 10
				label: "October"
			}
			{
				value: 11
				label: "November"
			}
			{
				value: 12
				label: "December"
			}
		]

module.exports = FormView

Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Moment = require 'moment'

class Model extends Backbone.Model

	default: {}

	initialize: (options)->
		@type = options.type
		@initValidation()
		@fetch options.data


	initValidation: ->
		@validation =
			first_name: [
				{
					required: true
					msg: 'Please enter First Name'
				},
				{
					pattern: /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/
					msg: 'Please enter a valid First Name'
				},
				{
					maxLength: 26
				}
			]
			last_name:[
				{
					required: true
					msg: 'Please enter Last Name'
				},
				{
					pattern: /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/
					msg: 'Please enter a valid Last Name'
				},
				{
					maxLength: 26
				}
			]

		switch @type
			when 'profile'
				if window.GLOBALS._PTYPE is 'client'
					@validation.dob_day = (val, field, model) =>
						if !model.dob_year or !model.dob_month
							return
						return 'Please enter a valid day' unless Moment([model.dob_year, parseInt(model.dob_month) - 1, model.dob_day]).isValid()

					@validation.dob_month = (val, field, model) =>
						if !model.dob_year and !model.dob_day
							return
						return 'Please select a month' unless model.dob_month

					@validation.dob_year = (val, field, model) =>
						if !model.dob_day and !model.dob_month
							return
						return 'Please select a year' unless model.dob_year

	setData: (data) ->
		@set
			first_name: data.first_name
			last_name: data.last_name
			business_name: data.business_name

		switch @type
			when 'profile'
				@set
					title: data.title ? ''
					gender: data.gender ? ''
					occupation: data.occupation ? ''
					birthday: data.birthday ? ''

				@updateDOB()

			when 'inline'
				@set
					'title': data.title ? ''
					'gender': data.gender ? ''
					'tags.languages': data['tags.languages'] ? []
					'language_options': data.language_options

	fetch: (data) ->

		if data?
			@setData data
		else
			@xhr = $.ajax
				url: getApiRoute('ajax-profiles-self')
				method: 'get'
				type: 'json'
				contentType: 'application/json; charset=utf-8'
				success: (response) =>
					@setData response.data
					@trigger 'data:inited', data
	save: ->
		PersonalDetails.trigger 'loading'

		@xhr = $.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify _.extend @toJSON(), { _token: window.GLOBALS._TOKEN }
			success: (response) =>
				@trigger 'data:saved'
				PersonalDetails.trigger 'ready'

			error: (response) =>
				PersonalDetails.trigger 'ready'

	updateBirthday: (val) ->
		@set 'birthday',
			day: @get('dob_day')
			month: @get('dob_month')
			year: @get('dob_year')

	updateDOB: ->
		@set 'dob_year', @get('birthday').year
		@set 'dob_month', @get('birthday').month
		@set 'dob_day', @get('birthday').day

module.exports = Model

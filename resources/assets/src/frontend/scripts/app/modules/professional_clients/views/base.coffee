Backbone = require 'backbone'

ImportClients = require 'app/modules/professional_clients_import'

class View extends Backbone.View

	el: $('[data-provider-clients]')

	popupView: null

	instances: {}

	initialize: (@options) ->
		super

		@eventBus = @options.eventBus
		@collections = @options.collections

		@cacheDom()
		@render()
		@bind()
		@parseHash()

	cacheDom: ->
		@$el.$app = @$el.find('.profiles_list--app')
		@$el.$header = @$el.find('.profiles_list--header')
		@$el.$invites = @$el.find('.profiles_list--invites')
		@$el.$invites_app = @$el.find('.profiles_list--invites--app')

		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	render: ->
		@instances.header = new ProfessionalClients.Views.Header
			eventBus: @eventBus
			collections: @collections
			el: @$el.$header
			parentView: @

		@instances.profilesList = new ProfessionalClients.Views.ProfilesList
			eventBus: @eventBus
			collections: @collections
		@$el.$app.append @instances.profilesList.el

		@instances.invitesList = new ProfessionalClients.Views.InvitesList
			eventBus: @eventBus
			collections: @collections
			container: @$el.$invites
		@$el.$invites_app.append @instances.invitesList.el


		@trigger 'rendered'

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

	showLoading: ->
		@$el.$loading.removeClass('m-hide')

	bind: ->
		@listenTo @eventBus, 'loading:show', @showLoading
		@listenTo @eventBus, 'loading:hide', @hideLoading
		@listenTo @collections.profiles, 'data:ready', =>
			@hideLoading()
			@afterLoad()
		@listenTo @eventBus, 'send:success', @showSuccess
		@listenTo @eventBus, 'send:error', @showError

	parseHash: ->
		@hash = _.object _.compact _.map window.location.hash.replace(/^#/, '').split('&'), (item) -> if item then item.split ':'
		window.location.hash = ''

	afterLoad: ->
		if @hash.hasOwnProperty('upload')
			@instances.header.importClients()

	removePopup: ->
		if @popupView
			@popupView.remove()
			@popupView = null
			window.popupsManager.popups = []

	appendClientPopup: (view) ->
		@removePopup()
		@popupView = view
		$('body').append @popupView.render().$el
		window.popupsManager.addPopup @popupView.$el
		window.popupsManager.openPopup 'add_client'

	addClient: ->
		view = new ProfessionalClients.Views.AddClient
			eventBus: @eventBus
			parentView: @

		@appendClientPopup view

	importClients: (couponCode = null) ->
		@removePopup()
		@importClientsApp = new ImportClients.App
			couponCode: couponCode

	showSuccess: (model) ->
		window.popupsManager.addPopup $('.add_client_success')
		$('.add_client_success').find('[data-client-name]').text model.get('first_name')
		$('[open-data-profiles-add]').click =>
			$('[data-profiles-add]').click()
		$('[data-popup-close]').click =>
			location.reload()
		window.popupsManager.openPopup 'add_client_success'

	showError: (error) ->
		@removePopup()
		window.popupsManager.addPopup $('.add_client_error')

		if error.responseJSON?.errors
			_.each error.responseJSON.errors, (err) =>
				if err.messages?.length
					$('.add_client_error').find('[data-error-text]').text('').append('<p>' + err.messages[0] + '</p>')

		window.popupsManager.openPopup 'add_client_error'

module.exports = View

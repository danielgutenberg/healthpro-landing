Backbone = require 'backbone'
Moment = require 'moment'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	sortStrategy: 'next_appointment'

	sortOptions:
		next_appointment: 'Upcoming Sessions'
		first_name : 'First Name'
		last_name : 'Last Name'

	initialize: ->
		super
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-provider-clients', {providerId: 'me'})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			data:
				return_fields: 'all'
			success: (response) =>
				_.each response.data, (item) => @push @format(item)
				@sort()
				@trigger 'data:ready'

			error: (error) =>
				@trigger 'data:ready'

	format: (item) -> item

	filter: (value) ->
		value = value.toLowerCase()
		_.each @models, (model) ->
			model.set 'is_hidden', value and model.get('full_name').toLowerCase().indexOf(value) is -1

	order: (value) ->
		@sortStrategy = value
		@sort()

	comparator: (a, b) ->
		if @sortStrategy is 'next_appointment'
			return @appointmentComparator a.get(@sortStrategy), b.get(@sortStrategy)
		@charComparator a.get(@sortStrategy), b.get(@sortStrategy)

	charComparator: (a, b) -> a.localeCompare b

	appointmentComparator: (a, b) ->
		if a and b
			return if Moment(a.from).isBefore(b.from) then -1 else 1
		if a
			return -1
		1

module.exports = Collection

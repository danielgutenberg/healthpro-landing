Backbone = require 'backbone'

class View extends Backbone.View

	initialize: (@options) ->
		super

		@eventBus = @options.eventBus
		@collections = @options.collections
		@baseView = @options.baseView

		@container = @options.container

		@subViews = []

		@bind()
		@render()
		@cacheDom()
		@

	bind: ->
		@listenTo @collections.invites, 'data:ready remove', =>
			if @collections.invites.length
				@showEl()
				@renderProfiles()
			else
				@hideEl()

	cacheDom: ->
		@$el.$listing = $('[data-invites-listing]', @$el)
		@$el.$loading = $('.loading_overlay', @$el)

	render: ->
		@$el.html ProfessionalClients.Templates.InvitesList

	renderProfile: (model) ->
		@subViews.push new ProfessionalClients.Views.InviteItem
			parentView: @
			baseView: @baseView
			model: model
			collections: @collections
			eventBus: @eventBus

	renderProfiles: ->
		@collections.invites.each @renderProfile, @

	showEl: ->
		@container.removeClass 'm-hide'

	hideEl: ->
		@container.addClass 'm-hide'

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

	showLoading: ->
		@$el.$loading.removeClass('m-hide')

module.exports = View

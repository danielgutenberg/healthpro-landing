Backbone = require 'backbone'
Stickit = require 'backbone.stickit'
Moment = require 'moment'
Phone = require 'framework/phone'
MomentTimezone = require 'moment-timezone'

vexDialog = require 'vexDialog'
Tooltipster = require 'tooltipster'

class View extends Backbone.View

	className: 'profiles_list--table--row'
	tagName: 'tr'
	events:
		'click [data-resend-invite]': 'resendInvitation'

	initialize: (@options) ->
		super

		@eventBus = @options.eventBus
		@model = @options.model
		@collections = @options.collections
		@parentView = @options.parentView

		@bind()
		@render()

	bind: ->
		@listenTo @eventBus, 'change:can_invite', @updateResendTooltips
		@bindings =
			'[data-name]':
				observe: 'profile'
				onGet: (val) ->
					val.full_name
			'[data-avatar]':
				observe: 'profile'
				updateMethod: 'html'
				onGet: (val) => "<img src='#{val.avatar}' alt=''>" if val.avatar
			'[data-invite-email]':
				observe: 'client_email'
			'[data-last-invite]': 'time_since'
			'[data-resend-invite]':
				attributes: [
					name: 'disabled'
					observe: 'can_invite'
					onGet: (val) ->
						return true if !val
				]

	render: ->
		@$el.html ProfessionalClients.Templates.InviteItem
		@parentView.$el.$listing.append @el
		@stickit()

		_.each @$el.find('[data-tooltip]'), (tooltipContainer) ->
			$(tooltipContainer).tooltipster
				theme: 'tooltipster-' + $(tooltipContainer).data('tooltip')

		@cacheDom()
		@updateResendTooltips()

		@trigger 'rendered'

	cacheDom: ->
		@$el.$resendButton = @$el.find('[data-resend-invite]')
		@$el.$resendButton.$wrap = @$el.$resendButton.parent('.profiles_list--table--btn_wrap')

	updateResendTooltips: ->
		if @$el.$resendButton?.$wrap.hasClass 'tooltipstered'
			if @model.get('can_invite')
				@$el.$resendButton.$wrap.tooltipster('content', null)
			else
				if @model.get('invite_count') >= 3
					@$el.$resendButton.$wrap.tooltipster('content', ProfessionalClients.Config.Messages.limitExceeded)
				else if Moment().diff((@model.get('last_invite')), 'hours') < 24
					@$el.$resendButton.$wrap.tooltipster('content', ProfessionalClients.Config.Messages.timeLimit)

	resendInvitation: ->
		@addClientModel = new ProfessionalClients.Models.AddClient
		@eventBus.trigger 'loading:show'
		$.when( @addClientModel.reInvite(@model.get('profile').id)).then(
			(success) =>
				@model.updateInvitation(success.data)
				@updateResendTooltips()
				@displayResendPopup()
				@eventBus.trigger 'loading:hide'
			(error) =>
				@eventBus.trigger 'loading:hide'
				@eventBus.trigger 'send:error', error
		)

	displayResendPopup: ->
		$popup = $('.resend_invitation_success')
		$popup.find('[data-client-name]').text @model.get('profile').first_name

		window.popupsManager.addPopup $popup
		window.popupsManager.openPopup 'resend_invitation_success'


module.exports = View

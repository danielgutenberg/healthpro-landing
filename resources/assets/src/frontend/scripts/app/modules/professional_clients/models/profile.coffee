DeepModel = require 'backbone.deepmodel'
getApiRoute = require 'hp.api'

class ProfileModel extends DeepModel

	attributes:
		is_hidden: false

	initialize: ->
		super
		@getMeta()

	getMeta: ->
		@trigger 'meta:loading'
		$.ajax
			url: getApiRoute('ajax-provider-client-meta-get', {
				providerId: 'me'
				clientId: @get('id')
				})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				if response.data.range_exemption?
					@set 'range_exemption', response.data.range_exemption
				else
					@set 'range_exemption', false

				@trigger 'meta:ready'
			error: (error) =>
				@trigger 'meta:ready'
				if error.responseJSON?.errors
					_.each error.responseJSON.errors, (err) =>
						if err.messages?.length
							window.Alerts.addNew('error', 'Error:' + err.messages[0])

	messageUrl: -> '/dashboard/conversations#to:' + @get('id')

	newAppointmentUrl: -> '/dashboard/schedule#new:' + @get('id')

	cancelAppointment: ->
		appointment = @get('next_appointment')
		return unless appointment
		$.ajax
			url: getApiRoute('ajax-appointment', appointmentId: appointment.id)
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

	confirmAppointment: ->
		appointment = @get('next_appointment')
		return unless appointment
		$.ajax
			url: getApiRoute('ajax-appointment-confirm', {
				appointmentId: appointment.id
			})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: (response) =>
				@set 'next_appointment.status', 'confirmed' if response.data

	updateMeta: (field) ->
		$.ajax
			url: getApiRoute('ajax-provider-client-meta-toggle', {
				providerId: 'me'
				clientId: @get('id')
			})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
				meta_key: field

	toggleSessionExcemption: ->
		$.ajax
			url: getApiRoute 'ajax-provider-toggle-client-meta',
				providerId: 'me'
				clientId: @get('id')
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				meta_key: 'intro_session_exemption'
				_token: window.GLOBALS._TOKEN
			success: (response) =>
				@set 'meta.intro_session_exemption', response.data.meta_value



module.exports = ProfileModel

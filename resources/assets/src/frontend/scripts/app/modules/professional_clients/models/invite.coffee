Backbone = require 'backbone'
DeepModel = require 'backbone.deepmodel'

class InviteModel extends DeepModel

	initialize: ->
		super

	updateInvitation: (data) ->
		@set data

module.exports = InviteModel

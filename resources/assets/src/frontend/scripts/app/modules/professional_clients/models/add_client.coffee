Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class AddClientModel extends Backbone.Model

	defaults:
		first_name: null
		last_name: null
		email: null
		phone: null

	initialize: () ->
		@addValidationRules()

	addValidationRules: ->
		@validation =
			first_name:
				required: true
				msg: 'Please enter client\'s first name'
			last_name:
				required: true
				msg: 'Please enter client\'s last name'
			email:
				pattern: 'email'
				required: true
				msg: 'Please enter client\'s email'
			phone:
				required: false
				pattern: /^\s*1?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/
				msg: 'Please enter phone number with the following format <br> +1 (XXX) XXX-XXXX. The +1 is optional'

	send: () ->
		$.ajax
			url: getApiRoute('ajax-invite-client')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			show_alerts: false
			data: JSON.stringify
				email: @.get('email')
				first_name: @.get('first_name')
				last_name: @.get('last_name')
				phone: @.get('phone')
				_token: window.GLOBALS._TOKEN

	reInvite: (clientId) ->
		$.ajax
			url: getApiRoute('ajax-reinvite-client',
				providerId: 'me'
				clientId: clientId
			)
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'


module.exports = AddClientModel

Handlebars = require 'handlebars'
Backbone = require 'backbone'

# views
BaseView = require './views/base'
HeaderView = require './views/header'
ProfilesListView = require './views/profiles_list'
ProfileItemView = require './views/profile_item'
InvitesListView = require './views/invites_list'
InviteItemView = require './views/invite_item'
AddClientView = require './views/add_client'

# models
ProfileModel = require './models/profile'
InviteModel = require './models/invite'
AddClientModel = require './models/add_client'

# collections
ProfilesCollection = require './collections/profiles'
InvitesCollection = require './collections/invites'

# templates
HeaderTemplate = require 'text!./templates/header.html'
ProfilesListTemplate = require 'text!./templates/profiles_list.html'
ProfileItemTemplate = require 'text!./templates/profile_item.html'
InvitesListTemplate = require 'text!./templates/invites_list.html'
InviteItemTemplate = require 'text!./templates/invite_item.html'
AddClientTemplate = require 'text!./templates/add_client.html'

window.ProfessionalClients =

	Config:
		Messages:
			error: 'Something went wrong. Please reload the page and try again.'
			cancelAppointment: 'Are you sure you want to cancel this appointment?'
			confirmAppointment: 'You are about to confirm an appointment. Click OK to confirm.'
			limitExceeded: 'You have exceeded the limit of invitations you can send to this client.'
			timeLimit: 'You can only send one invitation every 24 hours.'
		Tooltips:
			allow: 'Allow client to book a home visit outside of your range.'
			limit: 'Limit the client to book a home visit inside your range.'
	Views:
		Base: BaseView
		Header: HeaderView
		ProfilesList: ProfilesListView
		ProfileItem: ProfileItemView
		InvitesList: InvitesListView
		InviteItem: InviteItemView
		AddClient: AddClientView
	Collections:
		Profiles: ProfilesCollection
		Invites: InvitesCollection
	Models:
		Profile: ProfileModel
		Invite: InviteModel
		AddClient: AddClientModel
	Templates:
		Header:  Handlebars.compile HeaderTemplate
		ProfilesList:  Handlebars.compile ProfilesListTemplate
		ProfileItem:  Handlebars.compile ProfileItemTemplate
		InvitesList:  Handlebars.compile InvitesListTemplate
		InviteItem:  Handlebars.compile InviteItemTemplate
		AddClient:  Handlebars.compile AddClientTemplate

_.extend ProfessionalClients, Backbone.Events

class ProfessionalClients.App
	constructor: ->

		@eventBus = _.extend {}, Backbone.Events

		@collections =
			profiles: new ProfessionalClients.Collections.Profiles [],
				model: ProfessionalClients.Models.Profile
				eventBus: @eventBus
			invites: new ProfessionalClients.Collections.Invites [],
				model: ProfessionalClients.Models.Invite
				eventBus: @eventBus

		@views =
			base: new ProfessionalClients.Views.Base
				eventBus: @eventBus
				collections: @collections

ProfessionalClients.app = new ProfessionalClients.App()

module.exports = ProfessionalClients

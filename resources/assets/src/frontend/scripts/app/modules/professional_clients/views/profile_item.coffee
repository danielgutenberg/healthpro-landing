Backbone = require 'backbone'
Moment = require 'moment'
vexDialog = require 'vexDialog'

#Phone = require 'framework/phone'
require 'moment-timezone'
require 'backbone.stickit'
require 'tooltipster'

class View extends Backbone.View

	className: 'profiles_list--table--row'
	tagName: 'tr'

	events:
		'click [data-cancel-link]': 'cancelAppointment'
		'click [data-confirm-link]': 'confirmAppointment'
		'click [data-toggle-session-excemption]': 'toggleSessionExemption'
		'click [data-home-visits-allowance]': 'toggleExemptRange'

	initialize: (@options) ->
		super

		@eventBus = @options.eventBus
		@model = @options.model
		@collections = @options.collections
		@parentView = @options.parentView

		@bind()
		@render()
		@cacheDom()

	cacheDom: ->
		@$el.$homeVistisAllowance = @$el.find('[data-home-visits-allowance]')

		@$el.$homeVistisAllowance.$loading = $('<span class="loading_overlay m-light"><span class="spin m-cog"></span></span>')
		@$el.$homeVistisAllowance.append @$el.$homeVistisAllowance.$loading

	bind: ->
		@listenTo @model, 'remove', @destroy
		@listenTo @model, 'change:is_hidden', @toggleHidden
		@listenTo @model, 'meta:loading', @showMetaLoading
		@listenTo @model, 'meta:ready', @hideMetaLoading

		@bindings =
			'[data-name]': 'full_name'
			'[data-avatar]':
				observe: 'avatar'
				updateMethod: 'html'
				onGet: (val) => "<img src='#{val}' alt=''>" if val

			'[data-profile-link]':
				attributes: [
					name: 'href'
					observe: 'public_url'
				]
			'[data-phone]':
				observe: 'phone'
				updateMethod: 'html'
				onGet: (val) ->
					unless val
						return '&ndash;'

					if val.length is 10 then val = '1' + val # if saved in db without default '1' prefix
					$.formatMobilePhoneNumber(val)

			'[data-visits]':
				observe: 'visits'
				onGet: (val) ->
					val = 'no' unless val
					"#{val} visit" + (if val is '1' then 's' else '')

			'[data-last-appointment-time]':
				observe: 'last_appointment.date'
				updateMethod: 'html'
				onGet: @onGetDate
			'[data-last-appointment-service]': 'last_appointment.service.name'
			'[data-last-appointment-location]': 'last_appointment.location.name'

			'[data-next-appointment-time]':
				observe: ['next_appointment.from', 'next_appointment.location.address.timezone']
				updateMethod: 'html'
				onGet: @onGetDateTime
			'[data-next-appointment-service]': 'next_appointment.service.detailed_name'
			'[data-next-appointment-location]': 'next_appointment.location.name'

			'[data-confirm-appointment]':
				observe: 'next_appointment'
				visible: (val) ->
					return false unless val
					return true if val.status is 'pending' and val.initiated_by is 'client'

			'[data-cancel-appointment]':
				observe: 'next_appointment'
				visible: (val) -> if val then true else false

			'[data-waiting-appointment-confirmation]':
				observe: 'next_appointment'
				visible: (val) ->
					return false unless val
					return true if val.status is 'pending' and val.initiated_by is 'provider'

			'[data-message-link]':
				attributes: [
					name: 'href'
					observe: 'id'
					onGet: => @model.messageUrl()
				]

			'[data-toggle-session-excemption]':
				classes:
					'm-active': 'meta.intro_session_exemption'

			'[data-new-link]':
				attributes: [
					name: 'href'
					observe: 'id'
					onGet: => @model.newAppointmentUrl()
				]

			'[data-home-visits-allowance]':
				classes:
					'm-active':
						observe: 'range_exemption'
						onGet: (val) ->
							if @$el.$homeVistisAllowance?.hasClass 'tooltipstered'
								if val
									@$el.$homeVistisAllowance.tooltipster('content', ProfessionalClients.Config.Tooltips.limit)
								else
									@$el.$homeVistisAllowance.tooltipster('content', ProfessionalClients.Config.Tooltips.allow)
							val

	render: ->
		@$el.html ProfessionalClients.Templates.ProfileItem
		@parentView.$el.$listing.append @el
		@stickit()

		_.each @$el.find('[data-tooltip]'), (tooltipContainer) ->
			$(tooltipContainer).tooltipster
				theme: 'tooltipster-' + $(tooltipContainer).data('tooltip')
				maxWidth: 130

		@trigger 'rendered'

	onGetDate: (val) ->
		return '&ndash;' unless val
		Moment(val).format('MMM D, YYYY')

	onGetDateTime: (val) ->
		return '&ndash;' unless val[0]
		Moment(val[0]).format("hh:mm A [#{val[1].replace(/_/g, ' ')}] / MMM D, YYYY")

	toggleHidden: ->
		if @model.get('is_hidden')
			@$el.addClass('m-hide')
		else
			@$el.removeClass('m-hide')

	destroy: -> @$el.fadeOut 200, => @remove()

	confirmAppointment: (e) ->
		e?.preventDefault()
		vexDialog.confirm
			message: ProfessionalClients.Config.Messages.confirmAppointment
			callback: (value) =>
				return unless value
				@eventBus.trigger 'loading:show'
				$.when(@model.confirmAppointment()).then(
					(response) =>
						@eventBus.trigger 'loading:hide'
					(error) =>
						@eventBus.trigger 'loading:hide'
						alert ProfessionalClients.Config.Messages.error
				)

	cancelAppointment: (e) ->
		e?.preventDefault()
		vexDialog.buttons.YES.text = 'Yes'
		vexDialog.buttons.NO.text = 'No'
		vexDialog.confirm
			message: ProfessionalClients.Config.Messages.cancelAppointment
			callback: (value) =>
				return unless value
				@eventBus.trigger 'loading:show'
				$.when(@model.cancelAppointment()).then(
					(response) =>
						@eventBus.trigger 'loading:hide'
						@model.set 'next_appointment', null
						@stickit() #re-stickit
					(error) =>
						@eventBus.trigger 'loading:hide'
						alert ProfessionalClients.Config.Messages.error
				)

	toggleSessionExemption: (e) ->
		e?.preventDefault()
		@eventBus.trigger 'loading:show'
		$.when(
			@model.toggleSessionExcemption()
		).then(
			=>
				@eventBus.trigger 'loading:hide'
		).fail(
			=>
				@eventBus.trigger 'loading:hide'
				alert ProfessionalClients.Config.Messages.error
		)

	toggleExemptRange: (e) ->
		e?.preventDefault()
		@model.trigger 'meta:loading'
		$.when(@model.updateMeta('range_exemption')).then(
			(response) =>
				@model.set response.data?.meta_key, response.data?.meta_value
				@model.trigger 'meta:ready'
				@stickit() #re-stickit
			(error) =>
				@model.trigger 'meta:ready'
				alert ProfessionalClients.Config.Messages.error
		)

	showMetaLoading: ->
		if @$el.$homeVistisAllowance.$loading.length then @$el.$homeVistisAllowance.$loading.removeClass 'm-hide'

	hideMetaLoading: ->
		if @$el.$homeVistisAllowance.$loading.length then @$el.$homeVistisAllowance.$loading.addClass 'm-hide'

module.exports = View

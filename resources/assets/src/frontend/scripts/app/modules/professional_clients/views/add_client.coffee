Backbone = require 'backbone'
Validation = require 'backbone.validation'
Stickit = require 'backbone.stickit'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Phone = require 'framework/phone'

class View extends Backbone.View

	attributes:
		'class': 'popup add_client'
		'data-popup': 'add_client'
		'id' : 'popup_add_client'

	events:
		'click .add_client--save': 'addClient'

	initialize: (options) ->
		super
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins

		@model = new ProfessionalClients.Models.AddClient
		@eventBus = options.eventBus
		@parentView = options.parentView

		@addValidation
			selector: 'name'

		@addStickit()
		@bind()

	addStickit: ->
		@bindings =
			'[name="first_name"]': 'first_name'
			'[name="last_name"]': 'last_name'
			'[name="email"]': 'email'
			'[name="phone"]':
				observe: 'phone'
				onSet: (val) ->
					# we're ignoring prefixes when formatting, but we store it in DB
					if val then '1' + @phone.formattedVal(true) else ''
				onGet: (val, options) ->
					# not good
					return $.formatMobilePhoneNumber(val).replace('+1 ', '') if val
					''
				afterUpdate: ($el) -> $el.change()
				initialize: ($el) ->
					@phone = new Phone $el
		return @

	cacheDom: ->
		@$el.$loading = @$el.find('.loading_overlay')

	render: ->
		@$el.html ProfessionalClients.Templates.AddClient
		@stickit()
		@cacheDom()
		@

	showLoading: ->
		@$el.$loading.removeClass('m-hide')

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

	addClient: ->
		unless @model.validate()
			@showLoading()
			$.when(@model.send()).then(
				(response) =>
					@hideLoading()
					@parentView.removePopup()
					@eventBus.trigger 'send:success', @model
				(error) =>
					@eventBus.trigger 'send:error', error
					@hideLoading()
			)

module.exports = View

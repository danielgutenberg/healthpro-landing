Backbone = require 'backbone'
getRoute = require 'hp.url'

class View extends Backbone.View

	events:
		'keyup [data-profiles-search]': 'search'
		'change [data-profiles-sort]': 'sort'
		'click [data-profiles-add]': 'addClient'
		'click [data-profiles-import]': 'importClients'

	initialize: (@options) ->
		super

		@eventBus = @options.eventBus
		@collections = @options.collections
		@parentView = @options.parentView

		@render()
		@cacheDom()
		@maybeOpenPopup()
		@

	render: ->
		@$el.html ProfessionalClients.Templates.Header

	cacheDom: ->
		@$el.$search = $('[data-profiles-search]')
		@$el.$sort = $('[data-profiles-sort]')

		@fillSelect()

	sort: (e) ->
		@$el.$search.val ''
		@collections.profiles.order e.target.value

	search: (e) ->
		@collections.profiles.filter e.target.value

	addClient: (e) ->
		e.preventDefault()
		@parentView.addClient() if @canManageClients()
		@

	importClients: (e) ->
		e?.preventDefault()
		@parentView.importClients(@couponCode) if @canManageClients()
		@

	canManageClients: ->
		if $.parseJSON window.GLOBALS._IS_SEARCHABLE
			return true

		window.Alerts.addNew('error', 'You must complete your profile before you can add clients. <a href="' + getRoute('professional-setup-continue') + '" class="btn m-blue m-small">Complete  your profile</a>')
		false


	fillSelect: ->
		_.each @collections.profiles.sortOptions, (label, value) =>
			@$el.$sort.append "<option value='#{value}'>#{label}</option>"

	maybeOpenPopup: ->
		@couponCode = window.GLOBALS.UPLOAD_POPUP

		$('[data-profiles-import]').click() if @couponCode


module.exports = View

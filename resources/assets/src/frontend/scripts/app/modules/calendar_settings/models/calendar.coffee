getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	initialize: (data, options) ->
		@eventBus = options.eventBus
		@collection = options.collection

	connect: (full_sync) ->
		@trigger 'calendar:loading'

		$.ajax
			url: getApiRoute('ajax-calendar-connect', {
				remoteId: @get 'remote_id'
			})
			method: 'post'
			type  : 'json'
			data: JSON.stringify
				oauthId: @get 'oauth_id'
				_token: window.GLOBALS._TOKEN
				full_sync: 1 if full_sync?
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				@updateCalendarData(response.data)
				@set 'connected', true
				@trigger 'calendar:ready'
				@trigger 'data:updated' # when updating the full list
			error: (err) =>
				@set 'connected', false
				@trigger 'calendar:ready'
				@trigger 'data:updated' # when updating the full list

	disconnect: () ->
		@trigger 'calendar:loading'
		$.ajax
			url: getApiRoute('ajax-calendar-disconnect', {
				calendarId: @get 'id'
			})
			method: 'put'
			type  : 'json'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				@set 'connected', false
				@trigger 'calendar:ready'
			error: (err) =>
				@set 'connected', true
				@trigger 'calendar:ready'

	disconnectAll: () ->
		@trigger 'data:loading'
		$.ajax
			url: getApiRoute('ajax-oauth-disconnect', {
				oauthId: @get 'oauth_id'
			})
			method: 'put'
			type  : 'json'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				@trigger 'account:removed', @get 'oauth_id'
				@trigger 'data:updated'
			error: (err) =>
				@trigger 'data:updated'

	mark: () ->
		@trigger 'data:loading'
		$.when(
			@collection.unmarkMain()
		).then(
			=>
				if @get('connected')
					@markRequest()
				else
					@connect(true)
		)

	markRequest: ->
		$.ajax
			url: getApiRoute('ajax-calendar-mark', {
				calendarId: @get 'id'
			})
			method: 'put'
			type  : 'json'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				@trigger 'data:updated'
				@set 'full_sync', response.data.full_sync
			error: (err) =>
				@trigger 'data:updated'
				console.log 'updated here'
				@set 'full_sync', null

	updateCalendarData: (data) ->
		if data.id then @set 'id', data.id
		if data.connected then @set 'connected', data.connected
		if data.full_sync then @set 'full_sync', data.full_sync
		if data.primary then @set 'primary', data.primary

module.exports = Model

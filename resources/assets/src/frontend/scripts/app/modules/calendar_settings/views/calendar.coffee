Validation = require 'backbone.validation'
Stickit = require 'backbone.stickit'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
vexDialog = require 'vexDialog'

class View extends Backbone.View

	events:
		'click [data-calendar-disconnect]': 'disconnectAccount'

	initialize: (options) ->
		super
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins

		@setOptions(options)

		@bind()
		@render()
		@cacheDom()
		@addListeners()

	addListeners: ->
		@listenTo @model, 'calendar:loading', @showLoading
		@listenTo @model, 'calendar:ready', @hideLoading
		@listenTo @model, 'change:full_sync', @updateMainCalendar
		@listenTo @model, 'disconnect:show', @showDisconnect
		@listenTo @model, 'remove', @remove

	bind: ->
		@bindings =
			'[data-calendar-name]':
				observe: 'name'
				classes:
					'm-primary':
						observe: 'primary'

			'[data-calendar-blocking]':
				observe: 'connected'
				initialize: ($el) ->
					$el.on 'click', (e) =>
						return unless @model.get('full_sync')
						e.preventDefault()
						vexDialog.buttons.YES.text = 'Ok'
						vexDialog.alert
							message: CalendarSettings.Messages.UnselectBlockingOnMain

				onSet: (val) ->
					if val? and val
						@model.connect()
					else
						@model.disconnect()

			'[data-calendar-main]':
				observe: 'full_sync'
				initialize: ($el) ->
					$el.on 'click', (e) =>
						e.preventDefault()
						return if @model.get('full_sync')

						mainCalendar = @collections.calendars.findWhere 'full_sync': 1
						if mainCalendar
							vexDialog.confirm
								message: CalendarSettings.Messages.FullSyncConfirm
								callback: (value) =>
									return unless value

									$(e.currentTarget).prop('checked', true)
									@model.mark()
						else
							if $el.is(':checked') then @model.mark()
							if !@model.get 'full_sync' then $el.prop('checked', false)

				onGet: (val) ->
					val? and val

	render: ->
		@$el.html CalendarSettings.Templates.Calendar
			primary: @model.get('primary')
		@parentView.$el.$list.append @el
		@stickit()
		@

	cacheDom: ->
		@$el.$loading = $('.loading_overlay', @$el)
		@$el.$disconnect = $('[data-calendar-disconnect]', @$el)

	showLoading: ->
		@$el.$loading.removeClass('m-hide')

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

	disconnectAccount: ->
		@model.disconnectAll()

	showDisconnect: ->
		@$el.$disconnect.removeClass 'm-hide'

	updateMainCalendar: ->
		mainCalendar = @collections.calendars.findWhere 'full_sync': 1
		if mainCalendar
			@baseModel.set 'main_calendar', mainCalendar.get 'account_name'
		else
			@baseModel.unset 'main_calendar'

module.exports = View

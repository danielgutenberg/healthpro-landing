Validation = require 'backbone.validation'
Stickit = require 'backbone.stickit'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
getApiRoute = require 'hp.api'
vexDialog = require 'vexDialog'

require 'tooltipster'

class View extends Backbone.View

	initialize: (options) ->
		super
		Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins

		@subViews = []
		@setOptions(options)

		@addBindings()
		@render()
		@cacheDom()
		@initTooltips()
		@bind()

	addBindings: ->
		@bindings =
			'[data-main-calendar]': 'main_calendar'

			'[data-settings-header]':
				classes:
					'm-hide':
						observe: 'main_calendar'
						onGet: (val) ->
							return !val?

	bind: ->
		@listenTo @collections.calendars, 'data:loading', =>
			@showLoading()

		@listenTo @collections.calendars, 'calendar:ready', =>
			@toggleArrows()
			@overflowList()

		@listenTo @collections.calendars, 'data:updated', =>
			@hideLoading()

		@listenTo @collections.calendars, 'account:removed', (id) =>
			@cleanRemovedAccount(id)
			@findMainCalendar()

		@listenTo @collections.calendars, 'data:ready', =>
			@findMainCalendar()
			@toggleArrows()
			@appendCalendars()
			@overflowList()
			@hideLoading()

		@$el.$mainNone.on 'click', (e) =>
			e.preventDefault()
			return unless @model.get 'main_calendar'
			vexDialog.confirm
				message: CalendarSettings.Messages.FullSyncConfirm
				callback: (value) =>
					return unless value

					$.when(
						@collections.calendars.unmarkMain()
					).done(
						=>
							$(e.currentTarget).prop('checked', true)
					)

		@$window.on 'resize', =>
			@overflowList()

	render: ->
		@$el.html CalendarSettings.Templates.Base
		@stickit()
		@

	cacheDom: ->
		@$window = $(window)
		@$layout_header = $('.layout--header')
		@$panel_header = $('.panel--header')

		@$el.$loading = @$el.find('.loading_overlay')
		@$el.$header = @$el.find('.calendar_settings--header')
		@$el.$arrows = @$el.find('.calendar_settings--explain')
		@$el.$list = @$el.find('.calendar_settings--list_container')
		@$el.$list_header = @$el.find('.calendar_settings--list_header')
		@$el.$list.$content = @$el.find('.calendar_settings--content')
		@$el.$actions = @$el.find('.calendar_settings--actions')
		@$el.$addNew = @$el.find('[data-new-calendar]')
		@$el.$mainNone = @$el.find('[data-calendar-main-none]')

	showLoading: ->
		@$el.$loading.removeClass('m-hide')

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

	initTooltips: ->
		_.each @$el.find('[data-tooltip]'), (tooltipContainer) ->
			$(tooltipContainer).tooltipster
				theme: 'tooltipster-' + $(tooltipContainer).data('tooltip')

	appendCalendars: ->
		# tricky approach to keep primary-secondary order
		primaries = @collections.calendars.where 'primary': true
		_.each primaries, (primary) =>
			@appendCalendar(primary)
			secondaries = @collections.calendars.where
				'oauth_id': primary.get 'oauth_id'
				'primary': false
			_.each secondaries, (secondary) =>
				@appendCalendar(secondary)

	appendCalendar: (model) ->
		@subViews.push new CalendarSettings.Views.Calendar
			parentView: @
			model: model
			baseModel: @model
			collections: @collections
			eventBus: @eventBus

	cleanRemovedAccount: (oauth_id) ->
		@collections.calendars.remove @collections.calendars.where 'oauth_id': oauth_id
		@hideLoading()

	findMainCalendar: () ->
		mainCalendar = @collections.calendars.findWhere 'full_sync': 1
		if mainCalendar
			@model.set 'main_calendar', mainCalendar.get 'account_name'
		else
			@model.unset 'main_calendar'

	toggleArrows: () ->
		activatedCalendar = @collections.calendars.findWhere 'connected': true
		if activatedCalendar or @model.get('main_calendar')?
			@$el.$arrows.addClass 'm-hide'
		else
			@$el.$arrows.removeClass 'm-hide'

	overflowList: () ->
		list_content_height = @$el.$list.$content.height()
		if window.matchMedia('(max-width: 479px) and (min-height: 480px)').matches
			height = @$window.height() - @$layout_header.outerHeight() - @$panel_header.outerHeight() - @$el.$list_header.outerHeight() - @$el.$actions.outerHeight() - 25

			if (list_content_height - height) > 30
				@$el.$list.$content.css
					'max-height': height

		else
			@$el.$list.$content.css
				'max-height': '100%'

module.exports = View

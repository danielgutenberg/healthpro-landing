Backbone = require 'backbone'
Handlebars = require 'handlebars'

require '../../../../styles/modules/calendar_settings.styl'

# views
BaseView = require './views/base'
CalendarView = require './views/calendar'

# models
BaseModel = require './models/base'
CalendarModel = require './models/calendar'

# collections
CalendarsCollection = require './collections/calendars'

# templates
BaseTmpl = require 'text!./templates/base.html'
CalendarTmpl = require 'text!./templates/calendar.html'


window.CalendarSettings =
	Messages:
		FullSyncConfirm: 'Are you sure you would like to change the main booking calendar?'
		UnselectBlockingOnMain: 'You can\'t unselect blocking times from your main calendar'
	Models:
		Base: BaseModel
		Calendar: CalendarModel
	Collections:
		Calendars: CalendarsCollection
	Views:
		Base: BaseView
		Calendar: CalendarView
	Templates:
		Base: Handlebars.compile BaseTmpl
		Calendar: Handlebars.compile CalendarTmpl

_.extend CalendarSettings, Backbone.Events

class CalendarSettings.App
	constructor: (options) ->
		@eventBus = _.extend {}, Backbone.Events
		@model = new CalendarSettings.Models.Base()
		@collections =
			calendars: new CalendarSettings.Collections.Calendars [],
				model: CalendarSettings.Models.Calendar
				eventBus: @eventBus
		@view = new CalendarSettings.Views.Base
			$el: $('[data-calendar-settings]')
			model: @model
			eventBus: @eventBus
			collections: @collections

		return {
			view: @view
			model: @model
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()

if $('[data-calendar-settings]').length
	new CalendarSettings.App

module.exports = CalendarSettings

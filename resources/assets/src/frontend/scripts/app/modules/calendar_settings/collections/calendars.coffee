Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: ->
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-calendar-calendars')
			method: 'get'
			type  : 'json'
			success: (res) =>
				_.each res.data, (calendar) =>
					@addCalendar(calendar)
				@trigger 'data:ready'
			error: => @trigger 'data:ready'

	addCalendar: (calendar) ->
		_.each calendar.calendars, (subcalendar) =>
			subcalendar.oauth_id = calendar.id
			subcalendar.account_name = calendar.name

			@add subcalendar

	unmarkMain: ->
		@trigger 'data:loading'
		mainCalendar = @findWhere 'full_sync': 1
		mainCalendar.markRequest() if mainCalendar


module.exports = Collection

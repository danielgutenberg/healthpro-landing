Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Formats = require 'config/formats'
Numeral = require 'numeral'
getRoute = require 'hp.url'

class View extends Backbone.View

	tagName: 'ul'
	className: 'list_table--row'

	# events:
	# 	'click .transaction--main': 'toggleDetails'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@render()

	render: ->
		@$el.html BillingHistory.Templates.Transaction
			date: @model.get('date').format('MMM MM, YYYY')
			id: @model.get('id')
			status: @model.get('status')
			amount: Numeral(@model.get('amount')).format(Formats.Price.Simple)
			download_url: getRoute('dashboard-billling-invoice', {orderId: @model.get('id')})

		@parentView.$el.$listing.append @$el

module.exports = View

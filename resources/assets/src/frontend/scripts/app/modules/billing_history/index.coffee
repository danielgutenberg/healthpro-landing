Backbone = require 'backbone'
Handlebars = require 'handlebars'

# Views
BaseView = require './views/base'
TransactionView = require './views/transaction'

# Collections
TransactionsCollection = require './collections/transactions'

# Models
BaseModel = require './models/base'
TransactionModel = require './models/transaction'

# Templates
BaseTmpl = require 'text!./templates/base.html'
TransactionTmpl = require 'text!./templates/transaction.html'

# list of all instances
window.BillingHistory =

	Views:
		Base: BaseView
		Transaction: TransactionView
	Collections:
		Transactions: TransactionsCollection
	Models:
		Base: BaseModel
		Transaction: TransactionModel
	Templates:
		Base: Handlebars.compile BaseTmpl
		Transaction: Handlebars.compile TransactionTmpl

# events bus
_.extend BillingHistory, Backbone.Events

class BillingHistory.App
	constructor: ->
		@eventBus = _.extend {}, Backbone.Events

		@model = new BillingHistory.Models.Base

		@collection = new BillingHistory.Collections.Transactions [],
			model: BillingHistory.Models.Transaction
			baseModel: @model

		@view = new BillingHistory.Views.Base
			model: @model
			collection: @collection
			eventBus: @eventBus

BillingHistory.app = new BillingHistory.App()

module.exports = BillingHistory

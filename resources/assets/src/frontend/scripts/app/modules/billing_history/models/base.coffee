Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class Model extends Backbone.Model

	defaults:
		total_records: 0
		begin: 0
		step: 20

	initialize: (options) ->
		super
		Cocktail.mixin @, Mixins.Models.Validation

module.exports = Model

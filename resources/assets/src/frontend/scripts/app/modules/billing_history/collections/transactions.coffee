Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Moment = require 'moment'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		super
		@baseModel = options.baseModel
		@fetch()

	fetch: () ->
		$.ajax
			url: getApiRoute('ajax-order-billing-get')
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data:
				begin: @baseModel.get('begin')
				end: @baseModel.get('begin') + @baseModel.get('step')
			success: (res) =>
				console.log res
				if res.data?.records?
					@baseModel.set 'total_records', res.data.total_records
					_.forEach res.data.records, (transaction) => @add @format(transaction)

				@trigger 'data:ready'
			error: (err) =>
				@trigger 'data:ready'

	format: (transaction) ->
		date: Moment(transaction.date.date)
		id: transaction.invoice
		status: transaction.status
		amount: transaction.amount

module.exports = Collection

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'backbone.stickit'

class View extends Backbone.View

	el: $('[data-billing-history]')

	events:
		'click [data-billing-submit]': 'submitBillingInfo'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@transactions = []
		@paginationInited = false

		@addListeners()
		@showLoading()

	addListeners: ->
		@listenTo @collection, 'data:ready', @render
		@listenTo @model, 'change:begin', @fetchCollection

	bind: ->
		@$el.$pagination.find('li a').click (e) =>
			e.preventDefault()

			$target = $(e.currentTarget)
			targetBegin = $target.data('begin')

			unless @model.get('begin') is targetBegin
				@model.set 'begin', targetBegin

				@$el.$pagination.find('li').removeClass 'm-active'
				$target.parent('li').addClass 'm-active'

	render: ->
		@$el.html BillingHistory.Templates.Base
			hasTransactions: @collection.length > 0
		@hideLoading()
		@cacheDom()
		@renderTransactions()
		@initPagination() unless @paginationInited
		@bind()

	renderTransactions: ->
		_.forEach @collection.models, (model) =>
			@transactions.push new BillingHistory.Views.Transaction
				parentView: @
				model: model

	showLoading: -> Dashboard.trigger 'content:loading'
	hideLoading: -> Dashboard.trigger 'content:ready'

	cacheDom: ->
		@$window = $(window)
		@$el.$table = $('[data-table]', @$el)
		@$el.$listing = $('[data-listing]', @$el)
		@$el.$pagination = $('[data-pagination]', @$el)

	initPagination: ->
		if @model.get('total_records') > @collection.length
			i = 0
			begin_number = 0
			while @model.get('total_records') > begin_number
				i++
				@$el.$pagination.append("<li><a href='##{1}' data-begin='#{begin_number}'>#{i}</a></li>")
				begin_number += @model.get('step')

			@$el.$pagination.find('li').first().addClass 'm-active'
			@$el.$pagination.removeClass 'm-hide'

		@paginationInited = true

	fetchCollection: ->
		@showLoading()
		@collection.reset()
		@collection.fetch()

module.exports = View

Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class Model extends Backbone.Model

	initialize: (options) ->
		super

module.exports = Model

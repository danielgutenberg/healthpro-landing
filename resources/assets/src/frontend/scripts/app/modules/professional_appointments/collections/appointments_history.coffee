Moment = require 'moment'
Backbone = require 'backbone'
getApiRoute = require 'hp.api'
AppointmentModel = require '../models/appointment'
dateTimeParsers = require 'utils/datetime_parsers'

class AppointmentsHistory extends Backbone.Collection

	model: AppointmentModel

	statuses:
		pending : 'Pending'
		confirmed: 'Confirmed'
		previous: 'Previous'
		cancelled: 'Cancelled'

	initialize: ->
		super
		@

	getData: (clientId) ->

		# check if we already have data
		return loaded: true if @length

		$.ajax
			url: getApiRoute('ajax-provider-client-history', {
				providerId: 'me'
				clientId: clientId
			})
			method: 'get'
			type  : 'json'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.each response.data, (items, status) =>
					_.each items, (item) =>
						@push
							id: item.id
							client_id: item.client_id
							first_name: item.client.first_name
							last_name: item.client.last_name
							full_name: item.client.full_name
							avatar: item.client.avatar
							phone: item.client.phone
							location:
								name: item.location.name
								address: item.location.address.address
								city: item.location.address.city
								postal_code: item.location.address.postal_code
								timezone: item.location.address.timezone
							date: Moment(item.date)
							time_from: Moment(item.from)
							time_until: Moment(item.until)
							submitted: Moment.utc(item.created_at.date).fromNow()
							mark: item.mark
							initiated_by: item.initiated_by
							status: status
							service: item.service
							package: do ->
								return null unless item.package?
								{
									auto_renew: item.package.auto_renew
									entities_left: item.package.entities_left
									number_of_entities: item.package.package.number_of_entities
									package_id: item.package.package.id
									charge_id: item.package.id
									expires_on: dateTimeParsers.parseToMoment item.package.expires_on
									start_time: dateTimeParsers.parseToMoment item.package.start_time
									entity_id: item.package.package.entity_id
									entity_type: item.package.package.entity_type
									price: item.package.package.price
									number_of_months: item.package.package.number_of_months
									type: if item.package.package.number_of_entities > -1 then 'standard' else 'recurring'
									label: if item.package.package.number_of_entities > -1 then 'Package' else 'Monthly Package'
								}
				@trigger 'data:ready'

module.exports = AppointmentsHistory

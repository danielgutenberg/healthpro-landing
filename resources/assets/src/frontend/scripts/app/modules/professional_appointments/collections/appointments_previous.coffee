Backbone = require 'backbone'
AppointmentsCollection = require './appointments_base'

class Appointments extends AppointmentsCollection
	type: 'previous'
	dataType : 'past'
	statuses: ['confirmed']
	columns: [

		{
			label: '&nbsp;'
			sort: false
		}
		{
			label: '&nbsp;'
			sort: false
		}
		{
			label: 'Name'
			sort: true
		}
		{
			label: 'Date/Time'
			sort: true
		}
		{
			label: 'Service'
			sort: false
		}
		{
			label: 'Phone'
			sort: false
		}
		{
			label: 'Actions'
			classes: 'm-right m-last'
			sort: false
		}
		{
			label: 'Attended'
			classes: 'm-right m-last'
			sort: false
		}
		{
			label: '&nbsp;'
			sort: false
		}
	]
module.exports = Appointments

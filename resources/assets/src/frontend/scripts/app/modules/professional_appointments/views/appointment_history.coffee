Backbone = require 'backbone'
Appointment = require './appointment_base'
	
class View extends Appointment

	render: ->
		@beforeRender()
		@$el.html ProfessionalAppointments.Templates.AppointmentHistory()
		@afterRender()
		@

	beforeRender: ->
		@bindings['.listing_table--mark'] =
			observe: 'mark'
			updateMethod: 'html'
			onGet: (val) ->
				val = '&ndash;' unless val
				"<p>#{val}</p>"

		@bindings['.listing_table--initiated_by'] =
			observe: 'initiated_by'
			onGet: (val) => return if val is 'provider' then 'me' else val

module.exports = View

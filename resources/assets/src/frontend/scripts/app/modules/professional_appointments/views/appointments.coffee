Backbone = require 'backbone'
Stickit = require 'backbone.stickit'
datatables = require 'datatables'

class View extends Backbone.View

	table: null
	tableApi: null
	perPage: 8
	appointments: {}
	appointmentViewName: ''

	initialize: (@options) ->
		super

		@model = @options.model
		@collection = @options.collection
		@collections = @options.collections
		@eventBus = @options.eventBus
		@title = @options.title
		@type = @options.type
		@tableClass = @options.tableClass

		cType = @type.charAt(0).toUpperCase() + @type.substring(1).toLowerCase()
		@appointmentViewName = "Appointment#{cType}"


		@model = new Backbone.Model
			title: @title
			id: "professional_appointments_#{@type}"
			count: @collection.length
			columns: @collection.columns



		@bind()

		@render()
		@cacheDom()
		@initDataTable()
		@renderAppointments()

	bind: ->
		@listenTo @, 'appointment:appended', @appendedAppointment
		@listenTo @, 'appointment:removed', @removedAppointment

		@bindings =
			'[data-count]': 'count'
			'[data-title]': 'title'
			'[data-columns]':
				observe: 'columns'
				updateMethod: 'html'
				onGet: (val) ->
					html = ''
					_.each val, (v) ->
						label = if v.sort then "<b>#{v.label}</b>" else v.label
						classes = if v.sort then "m-sort" else "nosort"
						classes += if v.classes? then ' ' + v.classes else ''
						html += "<th class='#{classes}'>#{label}</th>"
					html

	cacheDom: ->
		@$el.$tableContainer = @$el.find('[data-table]')
		@$el.$table = @$el.$tableContainer.find('table')
		@$el.$tbody = @$el.$table.find('tbody')

		@$el.$tableContainer.addClass @tableClass if @tableClass

	render: ->
		@$el.html ProfessionalAppointments.Templates.Appointments()

		@stickit()
		@

	initDataTable: ->
		@table = @$el.$table.dataTable
			paging: true
			pagingType: "simple_numbers"
			searching: false
			lengthChange: false
			info: false
			iDisplayLength: @perPage
			destroy: true
			oLanguage:
				sEmptyTable: 'No appointments'
			aoColumnDefs: [
				bSortable: false
				aTargets: 'nosort'
			]
			fnDrawCallback: =>
				if @collection.length <= @perPage
					@$el.$tableContainer.addClass 'm-no-paging'
				else
					@$el.$tableContainer.removeClass 'm-no-paging'

		@tableApi = @table.api()

	renderAppointments: ->
		_.each @collection.models, (model) => @addAppointment model

	addAppointment: (model) ->
		appointment = new ProfessionalAppointments.Views[@appointmentViewName]
			model: model
			type: @type
			collection: @collection
			collections: @collections
			eventBus: @eventBus

		@appointments[model.get('id')] = appointment
		@tableApi.row.add(appointment.render().el).draw()

		_.each appointment.$el.find('[data-tooltip]'), (item) ->
			$(item).tooltipster
				theme: 'tooltipster-dark'

	appendedAppointment: (model) ->
		@addAppointment model
		@updateCount()

	removedAppointment: (model) ->
		if (model.get('id'))
			@tableApi.row(@appointments[model.get('id').$el]).remove().draw()
		@updateCount()

	updateCount: ->
		@model.set 'count', @collection.length


module.exports = View

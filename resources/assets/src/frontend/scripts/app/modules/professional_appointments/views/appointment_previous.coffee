Backbone = require 'backbone'
Appointment = require './appointment_base'

class View extends Appointment

	render: ->
		@beforeRender()
		@$el.html ProfessionalAppointments.Templates.AppointmentPrevious()
		@afterRender()
		@

	beforeRender: ->
		@bindings['[data-action-attended]'] =
			classes:
				'm-yes':
					observe: 'mark'
					onGet: (val) -> val is 'attended'
		@bindings['[data-action-missed]'] =
			classes:
				'm-no':
					observe: 'mark'
					onGet: (val) -> val is 'missed'
		@bindings['[data-action-message]']=
			attributes: [
				name: 'href'
				observe: 'id'
				onGet: => @model.messageUrl()
			]
		@bindings['[data-action-new]']=
			attributes: [
				name: 'href'
				observe: 'id'
				onGet: => @model.newAppointmentUrl()
			]

module.exports = View

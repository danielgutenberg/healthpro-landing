Backbone = require 'backbone'
Stickit = require 'backbone.stickit'

class View extends Backbone.View

	appointments: []

	initialize: (@options) ->
		super

		@model = @options.model
		@collection = @options.collection

		@bind()
		@render()
		@cacheDom()
		@renderAppointments()

	bind: ->
		@bindings =
			'[data-name]': 'full_name'

	cacheDom: ->
		@$el.$container = $('tbody', @$el)

	render: ->
		@$el.html ProfessionalAppointments.Templates.ClientHistory()
		@stickit()

	renderAppointments: ->
		_.each @collection.statuses, (label, status) =>
			models = @collection.where status: status
			if models.length
				@addAppointmentsHeader label
				_.each models, (model) => @addAppointment model

	addAppointment: (model) ->
		@appointments.push appointment = new ProfessionalAppointments.Views.AppointmentHistory
			model: model
			type: @type
			collection: @collection
			collections: @collections
			eventBus: @eventBus
		@$el.$container.append appointment.render().el


	addAppointmentsHeader: (label) ->
		@$el.$container.append "<tr class='client_history--table_section'><th colspan='5'>#{label}</th>"

module.exports = View

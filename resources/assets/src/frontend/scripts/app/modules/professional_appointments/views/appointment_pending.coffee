Backbone = require 'backbone'
Appointment = require './appointment_base'

class View extends Appointment

	render: ->
		@beforeRender()
		@$el.html ProfessionalAppointments.Templates.AppointmentPending()
		@afterRender()
		@

	beforeRender: ->
		@bindings['[data-can-confirm]'] =
			classes:
				'm-hide':
					observe: 'initiated_by'
					onGet: (val) -> val is 'provider'
		@bindings['[data-cannot-confirm]'] =
			classes:
				'm-hide':
					observe: 'initiated_by'
					onGet: (val) -> val is 'client'

		@bindings['[data-action-message]']=
			attributes: [
				name: 'href'
				observe: 'id'
				onGet: => @model.messageUrl()
			]

module.exports = View

Moment = require 'moment'
Backbone = require 'backbone'
getApiRoute = require 'hp.api'
AppointmentModel = require '../models/appointment'
dateTimeParsers = require 'utils/datetime_parsers'

class AppointmentsBase extends Backbone.Collection

	model: AppointmentModel
	dataType : false

	columns: [
		{
			label: '&nbsp;'
			sort: false
		}
		{
			label: '&nbsp;'
			sort: false
		}
		{
			label: 'Name'
			sort: true
		}
		{
			label: 'Date/Time'
			sort: true
		}
		{
			label: 'Service'
			sort: false
		}
		{
			label: 'Phone'
			sort: false
		}
		{
			label: 'Submitted'
			sort: false
		}
		{
			label: 'Actions'
			classes: 'm-right m-last'
			sort: false
		}
		{
			label: '&nbsp;'
			sort: false
		}
	]

	initialize: () ->
		super
		@getData()
		@bind()

	getRouteOptions: ->
		routeOptions =
			return_fields: 'all'
		routeOptions.type = @dataType if @dataType
		routeOptions.status = @statuses.join(',') if @statuses

		routeOptions

	bind: ->
		@listenTo @, 'appointment:append', @appendAppointment
		@listenTo @, 'appointment:remove', @removeAppointment

	appendAppointment: (model) -> @add model

	removeAppointment: (model) -> @remove model

	getData: ->
		$.ajax
			url: getApiRoute('ajax-provider-appointments', {providerId: 'me'})
			method: 'get'
			type  : 'json'
			data: @getRouteOptions()
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.each response.data, (item) => @push @formatAppointment(item)
				@trigger 'data:ready'

	formatAppointment: (item) ->
		id: item.id
		client_id: item.client_id
		first_name: item.client.first_name
		last_name: item.client.last_name
		full_name: item.client.full_name
		avatar: item.client.avatar
		phone: item.client.phone
		location:
			name: item.location.name
			address: item.location.address.address
			city: item.location.address.city
			postal_code: item.location.address.postal_code
			timezone: item.location.address.timezone
			location_type: item.location.location_type
		date: Moment(item.date)
		time_from: Moment(item.from)
		time_until: Moment(item.until)
		submitted: Moment.utc(item.created_at.date).fromNow()
		mark: item.mark
		service: item.service
		initiated_by: item.initiated_by
		package: do ->
			return null unless item.package?
			{
				auto_renew: item.package.auto_renew
				entities_left: item.package.entities_left
				number_of_entities: item.package.package.number_of_entities
				package_id: item.package.package.id
				charge_id: item.package.id
				expires_on: dateTimeParsers.parseToMoment item.package.expires_on
				start_time: dateTimeParsers.parseToMoment item.package.start_time
				entity_id: item.package.package.entity_id
				entity_type: item.package.package.entity_type
				price: item.package.package.price
				number_of_months: item.package.package.number_of_months
				type: if item.package.package.number_of_entities > -1 then 'standard' else 'recurring'
				label: if item.package.package.number_of_entities > -1 then 'Package' else 'Monthly Package'
			}

module.exports = AppointmentsBase

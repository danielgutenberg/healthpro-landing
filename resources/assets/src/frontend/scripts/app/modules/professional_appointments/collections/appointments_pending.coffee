Backbone = require 'backbone'
AppointmentsCollection = require './appointments_base'


class Appointments extends AppointmentsCollection
	type: 'pending'
	dataType : 'future'
	statuses: ['pending']
module.exports = Appointments

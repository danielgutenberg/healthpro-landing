Backbone = require 'backbone'
Appointment = require './appointment_base'

class View extends Appointment

	render: ->
		@beforeRender()
		@$el.html ProfessionalAppointments.Templates.AppointmentConfirmed()
		@afterRender()
		@

	beforeRender: ->
		@bindings['[data-action-message]']=
			attributes: [
				name: 'href'
				observe: 'id'
				onGet: => @model.messageUrl()
			]
		@bindings['[data-action-reschedule]']=
			attributes: [
				name: 'href'
				observe: 'id'
				onGet: => @model.editAppointmentUrl()
			]

module.exports = View

Backbone = require 'backbone'
vexDialog = require 'vexDialog'
Moment = require 'moment'
Phone = require 'framework/phone'

require 'moment-timezone'
require 'backbone.stickit'
require 'tooltipster'

class View extends Backbone.View

	tagName: 'tr'
	className: 'listing_table--row'

	history: null
	historyView: null

	events:
		'click [data-action-confirm]': 'confirmAppointment'
		'click [data-action-remove]': 'cancelAppointment'
		'click [data-action-email]': 'notifyAppointment'
		'click [data-action-attended]': 'attendedAppointment'
		'click [data-action-missed]': 'missedAppointment'
		'click [data-action-history]': 'viewHistory'

	initialize: (@options) ->
		super
		@model = @options.model
		@type = @options.type
		@collection = @options.collection
		@collections = @options.collections
		@eventBus = @options.eventBus

		@bind()

	bind: ->
		@listenTo @model, 'remove', @destroy
		@listenTo @, 'rendered', =>
			@cacheDom()

		@bindings =
			'.listing_table--avatar':
				observe: 'avatar'
				updateMethod: 'html'
				onGet: (val) ->
					return "<img src='#{val}' alt=''>" if val
					'<span></span>'
			'.listing_table--full_name':
				observe: 'full_name'
				updateMethod: 'html'
				onGet: (val) -> "<p><b>#{val}</b></p>"
			'.listing_table--phone':
				observe: 'phone'
				updateMethod: 'html'
				onGet: (val) ->
					return '&ndash;' unless val

					if val.length is 10
						val = 1 + val # fixing missing prefix

					$.formatMobilePhoneNumber(val)
			'.listing_table--date':
				observe: 'date'
				updateMethod: 'html'
				onGet: (val) ->
					return '' unless val
					date = val.format('ddd, MMM Do, YYYY')
					"<p><b>#{date}</b></p>"
			'.listing_table--time':
				observe: ['time_from', 'time_until', 'location']
				updateMethod: 'html'
				onGet: (val) ->
					timeFrom = val[0].format('h:mm a')
					timeUntil = val[1].format('h:mm a')
					"<p>#{timeFrom}&nbsp;&ndash;&nbsp;#{timeUntil}</p>"
			'.listing_table--timezone':
				observe: 'location'
				updateMethod: 'html'
				onGet: (val) ->
					timezone = val?.timezone.replace(/_/g, ' ')
					"<p>#{timezone}</p>"
			'.listing_table--submitted':
				observe: 'submitted'
				updateMethod: 'html'
				onGet: (val) ->
					return '' unless val
					"<p>#{val}</p>"


			'[data-appointment-package]':
				observe: 'package'
				updateMethod: 'html'
				onGet: (val) ->
					return '' unless val
					val.label
				classes:
					'm-hide':
						observe: 'package'
						onGet: (val) -> !val

			'[data-appointment-service]':
				observe: 'service'
				onGet: (val) -> val.detailed_name

			'[data-appointment-home-visit]':
				observe: 'location'
				onGet: (val) ->
					return '' if val.location_type isnt 'home-visit'
					'Home Visit'
				classes:
					'm-hide':
						observe: 'location'
						onGet: (val) -> val.location_type isnt 'home-visit'

			'[data-appointment-location-name]':
				observe: 'location'
				onGet: (val) ->
					return '' if val.location_type is 'home-visit'
					val.name
				classes:
					'm-hide':
						observe: 'location'
						onGet: (val) -> val.location_type is 'home-visit'

			'[data-appointment-location-address]':
				observe: 'location'
				onGet: (val) -> "#{val.address}, #{val.city} #{val.postal_code}"
				classes:
					'm-hide':
						observe: 'location'
						onGet: (val) -> val.location_type isnt 'home-visit'


	cacheDom: ->
		@$el.$actions = @$el.find('[data-actions]')

	afterRender: ->
		@stickit()
		@trigger 'rendered'

	destroy: ->
		@$el.fadeOut 200, => @remove()

	notifyAppointment: (e) ->
		e.preventDefault()
		return if @isLoading()
		@showLoading()
		$.when(@model.notify()).then(
			(success) =>
				@hideLoading()
				@eventBus.trigger 'appointment:notify', @model, @collection
				vexDialog.buttons.YES.text = 'OK'
				vexDialog.alert
					message: 'Reminder email to ' + @model.get('full_name') + ' was successfully sent.'

			(error) =>
				@hideLoading()
		)

	confirmAppointment: (e) ->
		e.preventDefault()
		return if @isLoading()
		@showLoading()
		$.when( @model.confirm() ).then(
			(success) =>
				@hideLoading()
				@eventBus.trigger 'appointment:move', @model, @collection, @collections.confirmed
			(error) =>
				@hideLoading()
		)

	cancelAppointment: (e) ->
		e.preventDefault()
		return if @isLoading()
		vexDialog.buttons.YES.text = 'YES'
		vexDialog.buttons.NO.text = 'NO'
		vexDialog.confirm
			message: 'Are you sure you would like to cancel this appointment?'
			callback: (value) =>
				return unless value
				@showLoading()
				$.when( @model.cancel() ).then(
					(success) =>
						@hideLoading()
						@eventBus.trigger 'appointment:move', @model, @collection, @collections.declined
					(error) =>
						@hideLoading()
				)

	attendedAppointment: (e) ->
		e.preventDefault()
		return if @isLoading()
		@showLoading()
		$.when( @model.attended() ).then(
			(success) =>
				@hideLoading()
			(error) =>
				@hideLoading()
		)

	missedAppointment: (e) ->
		e.preventDefault()
		return if @isLoading()
		@showLoading()
		$.when( @model.missed() ).then(
			(success) =>
				@hideLoading()
			(error) =>
				@hideLoading()
		)

	showLoading: -> @$el.$actions.addClass('m-loading')

	hideLoading: -> @$el.$actions.removeClass('m-loading')

	isLoading: -> @$el.$actions.hasClass('m-loading')

	viewHistory: (e) ->
		e.preventDefault()
		@eventBus.trigger 'loading:show'
		unless @history
			@history = new ProfessionalAppointments.Collections.AppointmentsHistory()

		$.when( @history.getData(@model.get('client_id')) ).then(
			(success) =>
				@renderPopup()
		)

	renderPopup: ->
		unless @historyView
			@historyView = new ProfessionalAppointments.Views.AppointmentsHistory
				model: @model
				collection: @history

		# remove old popups
		$('#popup_history').remove()
		window.popupsManager.popups = []

		@eventBus.trigger 'loading:hide'

		# append popup
		$('body').append @historyView.$el.html()
		window.popupsManager.addPopup $('#popup_history')
		window.popupsManager.openPopup 'popup_history'

module.exports = View

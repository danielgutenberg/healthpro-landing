Backbone = require 'backbone'
Stickit = require 'backbone.stickit'

class View extends Backbone.View

	initialize: (@options) ->
		super
		@$el = @options.el
		@collections = @options.collections
		@eventBus = @options.eventBus

		@model = new Backbone.Model()
		@update()

		@bind()
		@render()

	bind: ->
		_.each @collections, (collection) =>
			@listenTo collection, 'update', @update

		@bindings =
			'[data-pending]': 'pending'
			'[data-confirmed]': 'confirmed'
			'[data-previous]': 'previous'
			'[data-cancelled]': 'declined'

	render: ->
		@$el.html ProfessionalAppointments.Templates.HeadlineStats()
		@stickit()
		@

	update: ->
		@model.set
			pending: @collections.pending.length
			confirmed: @collections.confirmed.length
			previous: @collections.previous.length
			declined: @collections.declined.length

module.exports = View

Backbone = require 'backbone'
AppointmentsCollection = require './appointments_base'

class Appointments extends AppointmentsCollection
	type: 'confirmed'
	dataType : 'future'
	statuses: ['confirmed']

module.exports = Appointments

Backbone = require 'backbone'
Appointment = require './appointment_base'

class View extends Appointment

	render: ->
		@beforeRender()
		@$el.html ProfessionalAppointments.Templates.AppointmentDeclined()
		@afterRender()
		@

	beforeRender: ->
		@bindings['[data-action-message]']=
			attributes: [
				name: 'href'
				observe: 'id'
				onGet: => @model.messageUrl()
			]
		@bindings['[data-action-new]']=
			attributes: [
				name: 'href'
				observe: 'id'
				onGet: => @model.newAppointmentUrl()
			]

module.exports = View

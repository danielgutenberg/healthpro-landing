Backbone = require 'backbone'

class View extends Backbone.View

	el: $('[data-professional-appointments]')

	initialize: (options) ->
		@instances = {}

		@model = options.model
		@collections = options.collections
		@eventBus = options.eventBus

		@preload()

		@listenTo @, 'data:loaded', ->
			@cacheDom()
			@render()
			@bind()

	bind: ->
		@listenTo @eventBus, 'appointment:move', @moveAppointment
		@listenTo @eventBus, 'appointment:remove', @removeAppointment

		@listenTo @eventBus, 'loading:show', @showLoading
		@listenTo @eventBus, 'loading:hide', @hideLoading

	preload: ->
		unloadedCollections = _.size @collections
		@showLoading()
		_.forEach @collections, ($collection) =>
			@listenTo $collection, 'data:ready', =>
				unloadedCollections--
				unless unloadedCollections
					@hideLoading()
					@trigger 'data:loaded'

	render: ->
		@appendParts()
		@trigger 'rendered'

	cacheDom: ->
		@$el.$headlineStats = @$('.professional_appointments--headline-stats')
		@$el.$appointments =
			$confirmed: @$el.find('[data-appointments-confirmed]')
			$declined: @$el.find('[data-appointments-declined]')
			$pending: @$el.find('[data-appointments-pending]')
			$previous: @$el.find('[data-appointments-previous]')

	appendParts: ->
		@appendHeadlineStats()
		@appendAppointments()

	appendHeadlineStats: ->
		@instances.headlineStats = new ProfessionalAppointments.Views.HeadlineStats
			el: @$el.$headlineStats
			eventBus: @eventBus
			collections: @collections

	appendAppointments: ->
		@appendConfirmedAppointments()
		@appendDeclinedAppointments()
		@appendPendingAppointments()
		@appendPreviousAppointments()

	appendConfirmedAppointments: ->
		@instances.confirmed = new ProfessionalAppointments.Views.Appointments
			el: @$el.$appointments.$confirmed
			eventBus: @eventBus
			collection: @collections.confirmed
			collections: @collections
			model: @model
			title: "Confirmed Appointments"
			type: "confirmed"

	appendDeclinedAppointments: ->
		@instances.declined = new ProfessionalAppointments.Views.Appointments
			el: @$el.$appointments.$declined
			eventBus: @eventBus
			collection: @collections.declined
			collections: @collections
			model: @model
			title: "Cancelled Appointments"
			type: "declined"
			# tableClass: 'm-last'

	appendPendingAppointments: ->
		@instances.pending = new ProfessionalAppointments.Views.Appointments
			el: @$el.$appointments.$pending
			eventBus: @eventBus
			collection: @collections.pending
			collections: @collections
			model: @model
			title: "Pending Appointments"
			type: "pending"

	appendPreviousAppointments: ->
		@instances.previous = new ProfessionalAppointments.Views.Appointments
			el: @$el.$appointments.$previous
			eventBus: @eventBus
			collection: @collections.previous
			collections: @collections
			model: @model
			title: "Previous Appointments"
			type: "previous"

	moveAppointment: (model, oldCollection, newCollection) ->
		newCollection.trigger 'appointment:append', model
		oldCollection.trigger 'appointment:remove', model
		@instances[newCollection.type].trigger 'appointment:appended', model
		@instances[oldCollection.type].trigger 'appointment:removed', model

	removeAppointment: (model, collection) ->
		collection.trigger 'appointment:remove', model
		@instances[collection.type].trigger 'appointment:removed', model

	showLoading: ->
		@$el.addClass('m-loading')

	hideLoading: ->
		@$el.removeClass('m-loading')

	isLoading: ->
		@$el.hasClass('m-loading')

module.exports = View

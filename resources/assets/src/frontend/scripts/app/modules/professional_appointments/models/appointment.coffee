Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class AppointmentModel extends Backbone.Model

	default:
		id: null
		client_id: null
		first_name: ''
		last_name: ''
		full_name: ''
		avatar: ''
		location: null
		submitted: ''
		phone: ''
		date: ''
		time_from: ''
		time_until: ''
		mark: null
		service: null

	cancel: ->
		$.ajax
			url: getApiRoute('ajax-appointment', {
				appointmentId: @get('id')
			})
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: =>
				@set('id', null)
				@destroy()

	notify: ->
		$.ajax
			url: getApiRoute('ajax-appointment-notify', {
				appointmentId: @get('id')
			})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

	canConfirm: -> @get('initiated_by') is 'client'

	confirm: ->
		$.ajax
			url: getApiRoute('ajax-appointment-confirm', {
				appointmentId: @get('id')
			})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN

	attended: -> @mark 'attended'

	missed: -> @mark 'missed'

	mark: (mark) ->
		$.ajax
			url: getApiRoute('ajax-appointment-mark', {
				appointmentId: @get('id')
			})
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				mark: mark
				_token: window.GLOBALS._TOKEN
			success: =>
				@set 'mark', mark

	messageUrl: -> '/dashboard/conversations#to:' + @get('client_id')

	newAppointmentUrl: -> '/dashboard/schedule#new:' + @get('client_id')

	editAppointmentUrl: -> '/dashboard/schedule#edit:' + @get('id')

module.exports = AppointmentModel

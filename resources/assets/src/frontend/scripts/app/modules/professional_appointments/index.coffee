Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'
AppointmentsView = require './views/appointments'
AppointmentsHistoryView = require './views/appointments_history'
AppointmentConfirmedView = require './views/appointment_confirmed'
AppointmentDeclinedView = require './views/appointment_declined'
AppointmentPendingView = require './views/appointment_pending'
AppointmentPreviousView = require './views/appointment_previous'
AppointmentHistoryView = require './views/appointment_history'
HeadlineStatsView = require './views/headline_stats'

# models
AppointmentModel = require './models/appointment'

# Collections
AppointmentsConfirmedCollection = require './collections/appointments_confirmed'
AppointmentsDeclinedCollection = require './collections/appointments_declined'
AppointmentsPendingCollection = require './collections/appointments_pending'
AppointmentsPreviousCollection = require './collections/appointments_previous'
AppointmentsHistoryCollection = require './collections/appointments_history'

# templates
AppointmentConfirmedTmpl = require 'text!./templates/appointment_confirmed.html'
AppointmentDeclinedTmpl = require 'text!./templates/appointment_declined.html'
AppointmentPendingTmpl = require 'text!./templates/appointment_pending.html'
AppointmentPreviousTmpl = require 'text!./templates/appointment_previous.html'
AppointmentHistoryTmpl = require 'text!./templates/appointment_history.html'
AppointmentsTmpl = require 'text!./templates/appointments.html'
HeadlineStatsTmpl = require 'text!./templates/headline_stats.html'
ClientHistoryTmpl = require 'text!./templates/client_history.html'

window.ProfessionalAppointments =
	Views:
		Base: BaseView
		Appointments: AppointmentsView
		AppointmentsHistory: AppointmentsHistoryView
		AppointmentConfirmed: AppointmentConfirmedView
		AppointmentDeclined: AppointmentDeclinedView
		AppointmentPending: AppointmentPendingView
		AppointmentPrevious: AppointmentPreviousView
		AppointmentHistory: AppointmentHistoryView
		HeadlineStats: HeadlineStatsView
	Models:
		Appointment: AppointmentModel
	Collections:
		AppointmentsConfirmed: AppointmentsConfirmedCollection
		AppointmentsDeclined: AppointmentsDeclinedCollection
		AppointmentsPending: AppointmentsPendingCollection
		AppointmentsPrevious: AppointmentsPreviousCollection
		AppointmentsHistory: AppointmentsHistoryCollection
	Templates:
		AppointmentConfirmed: Handlebars.compile AppointmentConfirmedTmpl
		AppointmentDeclined: Handlebars.compile AppointmentDeclinedTmpl
		AppointmentPending: Handlebars.compile AppointmentPendingTmpl
		AppointmentPrevious: Handlebars.compile AppointmentPreviousTmpl
		AppointmentHistory: Handlebars.compile AppointmentHistoryTmpl
		Appointments: Handlebars.compile AppointmentsTmpl
		HeadlineStats: Handlebars.compile HeadlineStatsTmpl
		ClientHistory: Handlebars.compile ClientHistoryTmpl

_.extend(ProfessionalAppointments, Backbone.Events)

class ProfessionalAppointments.App
	constructor: ->

		@eventBus = _.extend {}, Backbone.Events

		@models =
			appointment: new ProfessionalAppointments.Models.Appointment()
		@collections =
			appointmentsConfirmed: new ProfessionalAppointments.Collections.AppointmentsConfirmed()
			appointmentsDeclined: new ProfessionalAppointments.Collections.AppointmentsDeclined()
			appointmentsPending: new ProfessionalAppointments.Collections.AppointmentsPending()
			appointmentsPrevious: new ProfessionalAppointments.Collections.AppointmentsPrevious()
		@views =
			base: new ProfessionalAppointments.Views.Base
				model: @models.appointment
				eventBus: @eventBus
				collections:
					confirmed: @collections.appointmentsConfirmed
					declined: @collections.appointmentsDeclined
					pending: @collections.appointmentsPending
					previous: @collections.appointmentsPrevious

new ProfessionalAppointments.App()

module.exports = ProfessionalAppointments

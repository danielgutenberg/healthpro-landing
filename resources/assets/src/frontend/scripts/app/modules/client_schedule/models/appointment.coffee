DeepModel = require 'backbone.deepmodel'
getApiRoute = require 'hp.api'
getRoute = require 'hp.url'

class AppointmentModel extends DeepModel

	initialState: null

	statuses:
		confirmed: 'Confirmed'
		pending: 'Pending'

	defaults:
		id: null
		client_id: null
		location_id: null
		service_id: null
		session_id: null
		availability_id: null
		full_name: ''
		avatar: ''
		location: null
		phone: ''
		date: ''
		from: ''
		until: ''
		status: null
		price: null
		service: ''
		package: null

	getEventData: ->
		data =
			id: @getCalendarEventId()
			type: 'appointment'
			entityId: @get('id')
			locationId: @get('location_id')
			title: (if @get('provider').title then @get('provider').title + ' ' else '') + @get('provider').full_name
			description: @get('location').name
			className: 'm-appointment'
			start: @get('from')
			end: @get('until')

		service = @get('service')
		data.description += '<br>' + service if service
		data

	getCalendarEventId: -> 'appointment_' + @get('id')

	isCustom: -> @get('session_id') == null

	syncDates: (date = null) ->
		date = @get('date').clone() unless date
		updatedDate =
			year: date.get('year')
			month: date.get('month')
			date: date.get('date')
		@get('from').set updatedDate
		@get('until').set updatedDate

	messageUrl: -> getRoute 'dashboard-conversation', profileId: @get('provider.id')

	profileUrl: ->
		slug = @get('provider.slug')
		slug = @get('provider.id') unless slug
		getRoute 'client-public-profile-url', slug: slug

	hasPackage: -> @get('package')?

	remove: (destroy = true) ->
		$.ajax
			url: getApiRoute('ajax-appointment', appointmentId: @get('id'))
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: =>
				if destroy
					@set 'id', null
					@destroy()

module.exports = AppointmentModel

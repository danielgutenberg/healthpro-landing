Backbone = require 'backbone'

Numeral = require 'numeral'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: ->
		super
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('api-profile-class-upcoming', {clientId: 'me'})
			method: 'get'
			type  : 'json'
			data: {}
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.forEach response.data, (item) =>
					@push
						id: item.id
						name: item.name
						description: item.description
						rating: item.rating
						availabilities: item.availabilities
						packages: _.map item.packages, (p) ->
							p.price = Numeral(p.price)
							p.formatted_price = p.price.format(ClientSchedule.Config.priceFormat)
							p
						tags: item.tags
				@trigger 'data:ready'
			error: (error) =>

	getAvailabilities: (fullCalendarStartDate) ->
		availabilities = []
		_.each @models, (model) =>
			availabilities = availabilities.concat model.getAvailabilities(fullCalendarStartDate)
		availabilities

module.exports = Collection

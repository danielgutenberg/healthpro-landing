Backbone = require 'backbone'
Moment = require 'moment'

require 'backbone.stickit'
require 'backbone.validation'
require 'moment-timezone'

class View extends Backbone.View

	attributes:
		'class': 'schedule_info'

	events:
		'click .schedule_info--msg': 'messageProvider'
		'click .schedule_info--profile_url': 'openProviderProfile'
		'click .schedule_info--remove_ask': 'removeAppointment'
		'click .schedule_info--remove_yes': 'confirmAppointmentRemoval'
		'click .schedule_info--remove_cancel': 'cancelAppointmentRemoval'

	initialize: (@options) ->
		super
		@model = @options.model
		@parentView = @options.parentView
		@collections = @options.collections

		@bind()

	render: ->
		@$el.html ClientSchedule.Templates.DetailsAppointment({
			past: @model.get('past')
			cancellation_message: @getCancellationMessage()
		})

		@$el.$loading = $('.loading_overlay', @$el)
		@$el.$alert = @$el.find('.schedule_info--alert')
		@$el.$notify = @$el.find('.schedule_info--notify')
		@$el.$remove = @$el.find('.schedule_info--remove')
		@$el.$content = @$el.find('.schedule_info--content')
		@hideLoading()

		@initTooltips()
		@stickit()
		@

	bind: ->
		@bindings =
			'[data-header-userpic]':
				observe: 'provider.avatar'
				updateMethod: 'html'
				onGet: (val) ->
					return '' unless val
					"<img src='#{val}' alt=''>"
			'[data-header-name]':
				observe: 'provider'
				onGet: (val) ->
					str = ''
					str += "#{val.title} " if val.title
					str += val.full_name
					str
			'[data-details-status]':
				observe: 'status'
				updateMethod: 'html'
				onGet: (val) => '<span class="m-' + val + '">' + @model.statuses[val] + '</span>'
			'[data-details-price]':
				observe: 'price'
				onGet: (val) -> val.format(ClientSchedule.Config.priceFormat)
			'[data-details-date]':
				observe: 'date'
				onGet: (val) -> val.format(ClientSchedule.Config.displayDateTimeFormat)
			'[data-details-from]':
				observe: 'from'
				onGet: (val) -> val.format(ClientSchedule.Config.displayTimeFormat)
			'[data-details-until]':
				observe: 'until'
				onGet: (val) -> val.format(ClientSchedule.Config.displayTimeFormat)
			'[data-details-timezone]':
				observe: 'location.timezone'
				onGet: (val) ->
					guessedTz = Moment.tz.guess()
					Moment.tz(guessedTz).zoneAbbr()

			'[data-details-location-name]': 'location.name'
			'[data-details-location-address]':
				observe: 'location'
				onGet: (val) ->
					html = "#{val.address}"
					html += ", #{val.address_line_two}" if val.address_line_two
					html += ", #{val.city}" if val.city
					html += ", #{val.postal_code}" if val.postal_code
					return html

			'[data-details-home-visit]':
				observe: 'location.location_type'
				onGet: (val) ->
					return '' unless val is 'home-visit'
					'home visit'
				classes:
					'm-hide':
						observe: 'location.location_type'
						onGet: (val) -> val isnt 'home-visit'

			'[data-details-service]':
				observe: 'service'
				updateMethod: 'html'
				onGet: (val) -> val or '&ndash;'
			'[data-details-package]':
				observe: 'package'
				onGet: (val) ->
					return '' unless val
					val.label
				classes:
					'm-hide':
						observe: 'package'
						onGet: (val) -> val is null

	initTooltips: ->
		@$el.find('[data-tooltip]').tooltipster
			theme: 'tooltipster-dark'

	messageProvider: (e) ->
		e?.preventDefault()
		window.location = @model.messageUrl()

	openProviderProfile: (e) ->
		e?.preventDefault()
		window.location = @model.profileUrl()

	showLoading: ->
		@$el.$loading.removeClass 'm-hide'
		return @

	hideLoading: ->
		@$el.$loading.addClass 'm-hide'
		return @

	removeAppointment: (e) ->
		e.preventDefault() if e?
		@$el.$content.addClass 'm-remove'
		return @

	cancelAppointmentRemoval: (e) ->
		e.preventDefault() if e?
		@$el.$content.removeClass 'm-remove'
		return @

	confirmAppointmentRemoval: (e) ->
		e.preventDefault() if e?
		@cancelAppointmentRemoval()
		@showLoading()
		@collections.appointments.remove @model
		eventData = @model.getEventData()
		$.when(@model.remove()).then(
			(success) =>
				@hideLoading()
				@collections.appointments.trigger 'appointment:removed', eventData.id
			, (error) =>
				@hideLoading()
		)


	getCancellationMessage: ->
		message = ClientSchedule.Config.Messages.cancelAppointment

		difference = Moment(@model.get('from')).diff(Moment(), 'hours')
		if difference < ClientSchedule.Config.CancellationPeriod
			message = ClientSchedule.Config.Messages.cancelAppointmentNoRefund

		message

module.exports = View

Handlebars = require 'handlebars'
Backbone = require 'backbone'

# views
BaseView = require './views/base'
CalendarView = require './views/calendar'
DetailsAppointmentView = require './views/details_appointment'
#	DetailsClassView = require './views/details_class'

# models
AppointmentModel = require './models/appointment'
#	ClassModel = require './models/class'

# collections
AppointmentsCollection = require './collections/appointments'
#	ClassesCollection = require './collections/classes'

#templates
CalendarTemplate = require 'text!./templates/calendar.html'
DetailsAppointmentTemplate = require 'text!./templates/details_appointment.html'
#	DetailsClassTemplate = require 'text!./templates/details_class.html'


window.ClientSchedule =

	Config:
		firstDay: 0
		timezone: 'local'
		slot: 5 # 5 minute slots
		slotDuration: '00:30:00' # used in fullCalendar only
		defaultAppointmentDuration: 30
		priceFormat: '$0,0.00'
		scrollTime: '08:00:00'

		datepickerFormat: 'MM/DD/YYYY' # used for MomentJS
		datepickerPluginFormat: "mm/dd/yy" # used for datepicker plugin
		dateTimeFormat: 'YYYY-MM-DD HH:mm:ss'
		displayTimeFormat: 'h:mm a'
		timeFormat: 'HH:mm'
		displayDateTimeFormat: 'ddd, MMM Do, YYYY'
		CancellationPeriod: 24 # hours

		Messages:
			cancelAppointment: 'Are you sure you want to cancel this appointment?'
			cancelAppointmentNoRefund: '<span class="m-error">This appointment is in less then 24 hours. Cancelling this appointment will not give you a refund.</span> Do you want to cancel anyway?'

	# we need fullCalendar entity to check for overlapped events
	# better to store it here then to request it from the view
	Calendar: null

	Views:
		Base: BaseView
		Calendar: CalendarView
		DetailsAppointment: DetailsAppointmentView
#			DetailsClass: DetailsClassView

	Collections:
		Appointments: AppointmentsCollection
#			Classes: ClassesCollection

	Models:
		Appointment: AppointmentModel
#			Class: ClassModel

	Templates:
		Calendar: Handlebars.compile CalendarTemplate
		DetailsAppointment: Handlebars.compile DetailsAppointmentTemplate
#			DetailsClass: Handlebars.compile DetailsClassTemplate

# extend module with events
_.extend ClientSchedule, Backbone.Events

class ClientSchedule.App
	constructor: ->
		require.ensure [], =>

			require 'fullcalendar'
			require 'fullcalendar/dist/fullcalendar.min.css'

			@eventBus = _.extend {}, Backbone.Events

			@collections =
				appointments: new ClientSchedule.Collections.Appointments [],
					model: ClientSchedule.Models.Appointment
					eventBus: @eventBus
	#				classes: new ClientSchedule.Collections.Classes [],
	#					model: ClientSchedule.Models.Class

			@views =
				base: new ClientSchedule.Views.Base
					eventBus: @eventBus
					collections: @collections

ClientSchedule.app = new ClientSchedule.App()

module.exports = ClientSchedule

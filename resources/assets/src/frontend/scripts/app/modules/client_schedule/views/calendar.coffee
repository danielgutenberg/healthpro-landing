Backbone = require 'backbone'

class View extends Backbone.View

	className: 'schedule--calendar m-loading'

	currentTooltip: null
	detailsView: null
	popupView: null

	calendarScroll: 0

	initialize: (@options) ->
		super
		@eventBus = @options.eventBus
		@$container = @options.$container
		@collections = @options.collections
		@detailsView = null
		@addAppointmentView = null

		@preload()
		@render()
		@cacheDom()
		@bind()

	bind: ->
		@listenTo @, 'data:loaded', @initCalendar
		@listenTo @eventBus, 'popup:close', @removePopup
		@listenTo @collections.appointments, 'appointment:removed', @removedAppointment

	render: ->
		@$el.html ClientSchedule.Templates.Calendar()
		@$container.append @$el
		@

	cacheDom: ->
		@$el.$calendar = @$el.find('[data-fullcalendar]')

	preload: ->
		unloadedCollections = _.size @collections
		_.forEach @collections, ($collection) =>
			@listenTo $collection, 'data:ready', =>
				unloadedCollections--
				@trigger 'data:loaded' unless unloadedCollections

	initCalendar: ->
		date = window.GLOBALS._CALENDAR_DATE
		dateTime = $.fullCalendar.moment(date).local()

		if dateTime.isValid()
			day = dateTime.format('YYYY-MM-DD')
			time = dateTime.format('HH:mm')

		scrollTime = if time? then time else ClientSchedule.Config.scrollTime

		@$el.removeClass 'm-loading'
		@$el.$calendar.fullCalendar
			height: 600
			defaultView: 'agendaWeek'
			slotDuration: ClientSchedule.Config.slotDuration
			firstDay: ClientSchedule.Config.firstDay
			timezone: ClientSchedule.Config.timezone
			slotEventOverlap: true
			scrollTime: scrollTime
			allDaySlot: false
			header:
				center: 'title'
				left: 'prev next today'
				right: ''
			titleFormat:
				week: "MMMM D, YYYY"
			columnFormat:
				week: 'ddd D'
			slotLabelFormat: 'h:mm A'
			slotLabelInterval: '01:00:00'
			eventRender: (ev, el) =>
				$el = $(el)
				$el.data 'event', ev
				$el.attr 'id', "event_#{ev.id}"
				if ev.rendering is 'background' and ev.title
					$el.append "<div class='fc-title'>#{ev.title}</div>"
				if ev.description
					$el.append "<div class='fc-description'>#{ev.description}</div>"
				$el.append "<div class='fc-event_time'>" + ev.start.format('h(:mm)a') + ' - ' + ev.end.format('h(:mm)a') + "</div>"
			viewRender: (ev, el) =>
				# there's no other way to highlight `today` in the header
				return unless el.find('.fc-today').length
				weekday = $.fullCalendar.moment().format('ddd').toLowerCase()
				el.find(".fc-#{weekday}").addClass('m-today')
			eventClick: @eventClick
			dayClick: @dayClick
			events: @fillEvents

		# cache the fullCalendar func
		ClientSchedule.Calendar = @$el.$calendar
		if day?
			ClientSchedule.Calendar.fullCalendar 'gotoDate', day

	eventClick: (eventDetails) =>
		switch eventDetails.type
			when 'appointment'
				collectionName = 'appointments'
				viewName = 'DetailsAppointment'
			when 'class'
				collectionName = 'classes'
				viewName = 'DetailsClass'
		@openDetails eventDetails, collectionName, viewName

		# hide tooltip on scroll
		# moved from @viewRender to correct scroll value calculation
		@calendarScroll = 0
		$scroller = @$el.$calendar.find('.fc-scroller')
		scroll = $scroller.scrollTop()

		$scroller.on 'scroll', (e) =>
			@calendarScroll = Math.abs(scroll - $scroller.scrollTop())
			if @calendarScroll > 10
				# not just @currentTooltip shold be hidden on scroll
				@hideAllTooltips()
				@calendarScroll = 0
				$scroller.unbind 'scroll'

	fillEvents: (start, end, timezone, callback) =>
		availabilities = []
		availabilities = availabilities.concat(
#				@collections.classes.getAvailabilities start
			@collections.appointments.getAvailabilities start
		)
		callback availabilities

	openDetails: (details, collection = 'appointments', view = 'DetailsAppointment') ->
		@detailsView = null
		@detailsView = new ClientSchedule.Views[view]
			model: @collections[collection].get details.entityId
			collections: @collections
			eventBus: @eventBus
			parentView: @
			eventDetails: details

		@currentTooltip = $("#event_#{details.id}")
		if @currentTooltip.hasClass 'tooltipstered'
			@currentTooltip.tooltipster 'content', @detailsView.render().$el
		else
			@currentTooltip.tooltipster
				animation: 'fade'
				arrow: true
				theme: "schedule--info"
				trigger: "click"
				position: "right"
				hideOnClick: false
				interactive: true
				contentCloning: false
				content: @detailsView.render().$el
				functionAfter: => @currentTooltip = null

		@currentTooltip.tooltipster 'show'

	hideTooltip: ->
		if @currentTooltip and @currentTooltip.hasClass 'tooltipstered'
			@currentTooltip.tooltipster 'hide'

	hideAllTooltips: ->
		@allTooltips =  $('.fc-event.tooltipstered')
		@allTooltips.tooltipster 'hide'

	removedAppointment: (calendarAppointmentId) =>
		@removePopup()
		ClientSchedule.Calendar.fullCalendar 'removeEvents', calendarAppointmentId

	removePopup: ->
		if @popupView
			@popupView.remove()
			@popupView = null
			window.popupsManager.popups = []

module.exports = View

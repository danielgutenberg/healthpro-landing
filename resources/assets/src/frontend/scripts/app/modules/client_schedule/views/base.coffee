Backbone = require 'backbone'

class View extends Backbone.View

	el: $('[data-client-schedule]')

	initialize: (@options) ->
		super

		@instances = []
		@eventBus = @options.eventBus
		@collections = @options.collections

		@cacheDom()
		@render()
		@bind()

	cacheDom: ->
		@$el.$loading = $('<span class="loading_overlay m-light"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	render: ->
		@appendParts()

	removeLoading: ->
		@$el.$loading.remove()

	bind: ->
		@listenTo @calendar, 'data:loaded', =>
			@removeLoading()

	appendParts: ->
		@appendCalendar()

	appendCalendar: ->
		@instances.push @calendar = new ClientSchedule.Views.Calendar
			eventBus: @eventBus
			collections: @collections
			$container: @$el

module.exports = View

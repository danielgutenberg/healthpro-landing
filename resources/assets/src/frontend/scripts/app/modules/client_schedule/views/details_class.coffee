Backbone = require 'backbone'

class View extends Backbone.View

	attributes:
		'class': 'schedule_info'

	initialize: (@options) ->
		super
		@eventBus = @options.eventBus
		@model = @options.model
		@collections = @options.collections
		@parentView = @options.parentView

	render: ->
		@$el.html ClientSchedule.Templates.DetailsClass @getPopupData()

		@$el.$loading = $('.loading_overlay', @$el)
		@hideLoading()
		@

	showLoading: ->
		@$el.$loading.removeClass('m-hide')

	hideLoading: ->
		@$el.$loading.addClass('m-hide')

	getPopupData: ->
		data =
			name: @model.get('name')
			description: @model.get('description')
			locations: []
			packages: @model.get('packages')

		data.description = '' if data.description is 'empty'

		_.each @model.get('availabilities'), (availability) =>
			data.locations.push
				address: @collections.locations.get(availability.location_id).get('address')
				weekday: Moment.weekdays(availability.day)
				time_from: availability.from
				time_until: availability.until
				max_clients: availability.max_clients
		data

module.exports = View

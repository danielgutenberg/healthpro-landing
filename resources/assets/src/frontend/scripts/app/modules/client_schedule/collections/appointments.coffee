Backbone = require 'backbone'
getApiRoute = require 'hp.api'
Numeral = require 'numeral'

class Collection extends Backbone.Collection

	initialize: ->
		super
		@getData()

	getData: ->
		$.ajax
			url: getApiRoute('ajax-appointments')
			method: 'get'
			type  : 'json'
			data:
				status: 'confirmed'
				return_fields: 'all'
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.forEach response.data, (item) => @push @formatAppointment(item)
				@trigger 'data:ready'

	formatAppointment: (item) ->
		id: item.id
		client_id: item.client_id
		location_id: item.location_id
		session_id: item.session_id
		full_name: item.client.full_name
		phone: item.client.phone
		provider:
			full_name: item.provider.full_name
			title: item.provider.title
			avatar: item.provider.avatar
			slug: item.provider.slug
			id: item.provider.id
		location:
			name: item.location.name
			address: item.location.address.address
			address_line_two: item.location.address.address_line_two
			city: item.location.address.city
			postal_code: item.location.address.postal_code
			timezone: item.location.address.timezone
			location_type: item.location.location_type
		date: $.fullCalendar.moment(item.from_utc)
		from: $.fullCalendar.moment(item.from_utc)
		until: $.fullCalendar.moment(item.until_utc)
		status: item.status
		mark: item.mark
		past: item.past
		price: Numeral(item.price)
		service: item.service?.name
		package: do ->
			return null unless item.package?
			{
				auto_renew: item.package.auto_renew
				entities_left: item.package.entities_left
				number_of_entities: item.package.package.number_of_entities
				package_id: item.package.package.id
				charge_id: item.package.id
				expires_on: $.fullCalendar.moment(item.package.expires_on)
				start_time: $.fullCalendar.moment(item.package.start_time)
				entity_id: item.package.package.entity_id
				entity_type: item.package.package.entity_type
				price: item.package.package.price
				number_of_months: item.package.package.number_of_months
				type: if item.package.package.number_of_entities > -1 then 'standard' else 'recurring'
				label: if item.package.package.number_of_entities > -1 then 'Package' else 'Monthly Package'
			}

	getAvailabilities: ->
		availabilities = []
		_.each @models, (model) =>
			availabilities.push model.getEventData()
		availabilities


module.exports = Collection

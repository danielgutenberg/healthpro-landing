Backbone = require 'backbone'
Stickit = require 'backbone.stickit'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'


class View extends Backbone.View

	tagName: 'li'
	className: 'cards_manager--cards_item'

	events:
		'click .cards_manager--edit_card': 'editCard'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base

		@model = options.model
		@baseView = options.baseView

		@addStickit()

	addStickit: ->
		@bindings =
			'.cards_manager--card_number':
				observe: 'card_number'
				updateMethod: 'html'
				onGet: (val) -> '<i>&bull;&bull;&bull;&bull;</i><i>&bull;&bull;&bull;&bull;</i><i>&bull;&bull;&bull;&bull;</i>' + val

			'input[name="card"]':
				observe: 'id'
				initialize: ($el, model) -> $el.val model.get('id')

			'.cards_manager--card_image':
				observe: 'card_type'
				updateMethod: 'html'
				onGet: (val) -> "<img src='/assets/frontend/images/content/booking/cc-#{val}.png' alt=''>"

	render: ->
		@$el.html CardsManager.Templates.Card()
		@stickit()
		@

	editCard: ->
		# select card before showing the form
		@baseView.model.set 'card_id', @model.get('id')
		@baseView.showEditCardForm @model

	destroy: ->
		@remove()
		@unbind()

module.exports = View

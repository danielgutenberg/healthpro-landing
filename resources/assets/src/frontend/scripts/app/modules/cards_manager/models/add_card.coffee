Backbone = require 'backbone'
Validation = require 'backbone.validation'
Moment = require 'moment'
getApiRoute = require 'hp.api'

Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class Model extends Backbone.Model

	defaults:
		name: ''
		cardNumber: ''
		expiryMonth: ''
		expiryYear: ''
		cvv: ''
		card_type: ''

	validation:
		name:
			required: true
			msg: 'Please enter a name'

		cardNumber:[
			{
				required: true
				msg: 'Please enter a number'
			}
			{
				minLength: 12
				msg: 'Min length is 12'
			}
		]

		expiryMonth: [
			{
				required: true
				msg: 'Please select a month'
			}
			{
				fn: (value, attr, computedState) ->
					month = computedState.expiryMonth
					year = computedState.expiryYear
					if year?
						selectedDate = +( year.toString() + month.toString() )
						currentDate = +Moment().format('YYYYMM')
						if selectedDate < currentDate
							return 'Card has expired'
			}
		]

		expiryYear:
			required: true
			msg: 'Please select a year'

		cvv: [
			{
				required: true
				msg: 'Please enter a cvv'
			}
			{
				pattern: 'digits'
				msg: 'Only digits'
			}
			{
				maxLength: 4
				msg: 'Max length is 4'
			}
			{
				minLength: 3
				msg: 'Min length is 3'
			}
		]

	initialize: ->
		super
		Cocktail.mixin( @, Mixins.Models.Validation )

	save: ->
		$.ajax
			url: getApiRoute('ajax-credit-cards')
			method: 'post'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				name: @get('name')
				number: @get('number').replace(/\s+/g, '')
				expiryMonth: @get('expiryMonth')
				expiryYear: @get('expiryYear')
				cvv: @get('cvv')
				type: 'credit'
				_token: window.GLOBALS._TOKEN

			error: (error) =>
				@handleErrors(error)

module.exports = Model

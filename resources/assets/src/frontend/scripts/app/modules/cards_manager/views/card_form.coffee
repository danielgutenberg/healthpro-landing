Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
Moment = require 'moment'
scrollTo = require 'utils/scroll_to_element'
vexDialog = require 'vexDialog'
require 'payment'

class View extends Backbone.View

	tagName: 'form'
	className: 'cards_manager--form'

	events:
		'click .cards_manager--add_card': 'submitForm'
		'click .cards_manager--delete': 'removeCard'
		'submit': 'submitForm'
		'click .cards_manager--cancel': 'cancelForm'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base

		@collection = options.collection
		@baseView = options.baseView
		@model = options.model

		@isNew = if @model.get('id') then false else true

		@addStickit()
		@addValidation()

	addStickit: ->
		@bindings =
			'[name="card_holder"]': 'card_holder'
			'[name="card_number"]': 'card_number'
			'[name="card_exp"]':
				observe: ['exp_month', 'exp_year']
				onGet: (val) -> val[0] + ' / ' + val[1] if val[0] and val[1]
				onSet: (val) ->
					exp = val.split '/'
					exp[0] = if exp[0] then exp[0] * 1 else 0
					exp[1] = if exp[1] then exp[1] * 1 else 0
					exp
#			'[name="exp_month"]':
#				observe: 'exp_month'
#				selectOptions:
#					collection: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
#					defaultOption:
#						label: 'MM'
#						value: ''
#			'[name="exp_year"]':
#				observe: 'exp_year'
#				selectOptions:
#					collection: @generateYears
#					defaultOption:
#						label: 'YYYY'
#						value: ''
			'[name="cvv"]': 'cvv'
			'.cards_manager--card_logo':
				classes:
					maestro:
						observe: 'card_type'
						onGet: (val) -> val is 'mastercard' || val is 'maestro' || val is 'master'
					visa:
						observe: 'card_type'
						onGet: (val) -> val is 'visa'
					amex:
						observe: 'card_type'
						onGet: (val) -> val is 'amex'
			'.cards_manager--form_number--label':
				classes:
					'm-hide':
						observe: 'id'
						onGet: (val) -> if val then false else true

			'.cards_manager--form_number--field':
				classes:
					'm-hide':
						observe: 'id'
						onGet: (val) -> if val then true else false

			'.cards_manager--card_number':
				observe: 'card_number'
				updateMethod: 'html'
				onGet: (val) -> '<i>&bull;&bull;&bull;&bull;</i><i>&bull;&bull;&bull;&bull;</i><i>&bull;&bull;&bull;&bull;</i>' + val

	generateYears: ->
		years = []
		currentYear = +Moment().format('YYYY')
		lastYear = currentYear + 10
		for i in [currentYear..lastYear]
			years.push i.toString()
		years

	initCardCheck: ->
		@$el.$cardNumber.payment('formatCardNumber')
		@$el.$cardCvc.payment('formatCardCVC')
		@$el.$cardExp.payment('formatCardExpiry')

		@$el.$cardNumber.on 'keyup', (e) =>
			@model.set 'card_type', $.payment.cardType($(e.target).val())

		# don't allow numbers in card holders name
		@$el.$cardName.on 'keypress', (e) ->
			return false if e.charCode > 47 and e.charCode < 58

	submitForm: (e) ->
		e.preventDefault() if e?
		errors = @model.validate()

		return @scrollToForm() if errors?

		Wizard.loading()
		$.when( @model.save() )
			.then(
				(res) =>
					if @isNew
						@collection.push res.data
						@baseView.selectLast()

					@baseView.hideCardForm()

					Wizard.ready()

					if @isNew
						window.CardsManager.trigger 'card:added' unless e?
					else
						# don't save cvv
						@model.set 'cvv', null
						window.CardsManager.trigger 'card:updated'

				, (request) =>
					Wizard.ready()
					if request.responseJSON?.errors
						_.each request.responseJSON.errors, (err) =>
							@showError err.messages[0] if err.messages?.length
					@scrollToForm()
			)
		@

	scrollToForm: ->
		setTimeout(
			=> scrollTo @$el
			, 10
		)
		@

	cancelForm: ->
		@baseView.hideCardForm()
		@

	# base functions
	cacheDom: ->
		@$el.$cardNumber = @$el.find('[name="card_number"]')
		@$el.$cardCvc = @$el.find('[name="cvv"]')
		@$el.$cardLogo = @$el.find('.cards_manager--card_logo')
		@$el.$cardName = @$el.find('[name="card_holder"]')
		@$el.$cardExp = @$el.find('[name="card_exp"]')

		@$el.$errorsContainer = @$el.find('.cards_manager--errors')

		@$el.$buttonCancel = @$el.find('.cards_manager--cancel')
		@$el.$buttonDelete = @$el.find('.cards_manager--delete')

		@$el.$buttonSave = @$el.find('.cards_manager--add_card')
		@

	render: ->
		@$el.html CardsManager.Templates.CardForm()
		@cacheDom()
		@stickit()
		@initCardCheck()
		@toggleButtons()
		@

	toggleButtons: ->
		unless @collection.length
			@$el.$buttonCancel.addClass 'm-hide'
			@$el.$buttonDelete.addClass 'm-hide'
			unless CardsManager.Config.DisplaySaveButton
				@$el.$buttonSave.addClass 'm-hide'
		@

	removeCard: (e) ->
		e.preventDefault() if e?

		# we can't remove unsaved card
		return unless @model.get('id')

		vexDialog.confirm
			message: 'Are you sure you would like to delete this card?'
			callback: (value) =>
				return unless value


				Wizard.loading()
				$.when( @model.remove() )
				.then(
					(res) =>
						@baseView.removeCard @model

						Wizard.ready()
						window.CardsManager.trigger 'card:removed' unless e?

					, (err) =>
						Wizard.ready()
					)


	showError: (error) ->
		@$el.$errorsContainer.html("<span>#{error}</span>").removeClass('m-hide')

	hideError: ->
		@$el.$errorsContainer.html('').addClass('m-hide')


module.exports = View

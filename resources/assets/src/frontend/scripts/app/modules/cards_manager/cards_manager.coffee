Backbone = require 'backbone'
Handlebars = require 'handlebars'

# views
BaseView = require './views/base'
CardView = require './views/card'
CardFormView = require './views/card_form'

# models
BaseModel = require './models/base'
CardModel = require './models/card'

# collections
CardsCollection = require './collections/cards'

# templates
BaseTmpl = require 'text!./templates/base.html'
CardTmpl = require 'text!./templates/card.html'
CreditFormTmpl = require 'text!./templates/credit_form.html'

# list of all instances
window.CardsManager = CardsManager =
	Config:
		MaxCards: 0
		CardType: 'credit'
		DisplaySaveButton: true
	Views:
		Base: BaseView
		Card: CardView
		CardForm: CardFormView
	Models:
		Base: BaseModel
		Card: CardModel
	Collections:
		Cards: CardsCollection
	Templates:
		Base: Handlebars.compile BaseTmpl
		Card: Handlebars.compile CardTmpl
		CardForm: Handlebars.compile CreditFormTmpl

# events bus
_.extend CardsManager, Backbone.Events

class CardsManager.App
	constructor: (options = {}) ->
		window.CardsManager.Config.MaxCards = options.maxCards if options.maxCards?
		window.CardsManager.Config.CardType = options.cardType if options.cardType?
		window.CardsManager.Config.DisplaySaveButton = options.displaySaveButton if options.displaySaveButton?

		@model = new CardsManager.Models.Base()
		@view = new CardsManager.Views.Base
			model: @model
			collection: new CardsManager.Collections.Cards null, {model: CardsManager.Models.Card}

		model: @model
		view: @view
		close: @close

	close: ->
		@view.close?()
		@view.remove?()

module.exports = CardsManager

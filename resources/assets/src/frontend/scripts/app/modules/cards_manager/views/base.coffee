Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'backbone.stickit'

class View extends Backbone.View

	className: 'cards_manager'

	events:
		'click .cards_manager--add_another': 'showNewCardForm'

	initialize: (options) ->
		Cocktail.mixin( @, Mixins.Views.Base )

		@subViews = {}
		@collection = options.collection
		@model = options.model
		@isFormOpened = false

		@addStickit()
		@bind()

	addStickit: ->
		@bindings =
			'[name="card"]': 'card_id'

	bind: ->
		@listenTo @collection, 'data:ready', @selectCard
		@listenTo @collection, 'add', @addCard
		@

	addCard: (model) ->
		card = new CardsManager.Views.Card
			model: model
			baseView: @

		@subViews[model.get('id')] = card

		@$el.$cards.$list.append card.render().el

		@stickit()
		@

	removeCard: (model) ->
		# a bit dirty but should work for now
		@hideCardForm()
		@collection.remove model
		@subViews[model.get('id')].destroy()
		delete @subViews[model.get('id')]
		model = null
		if @collection.length
			@stickit()
			@selectFirst()
		else
			@showNewCardForm()
		@

	initAddCardForm: (model) ->
		model = new CardsManager.Models.Card() unless model
		@cardForm = new CardsManager.Views.CardForm
			baseView: @
			model: model
			collection: @collection

		window.CardsManager.trigger 'card:edit', model

		@$el.$formWrap.html @cardForm.render().el

	showCardForm: ->
		@$el.$formWrap.removeClass('m-hide')
		@hideList()
		@isFormOpened = true
		@

	showNewCardForm: ->
		@initAddCardForm()
		@showCardForm()
		@

	showEditCardForm: (model) ->
		@initAddCardForm(model)
		@showCardForm()
		@

	hideCardForm: ->
		@$el.$formWrap.addClass('m-hide')
		@cardForm.remove()
		@showList()
		@isFormOpened = false
		window.CardsManager.trigger 'card:close'
		@

	showList: ->
		@$el.$cards.removeClass('m-hide')
		@

	hideList: ->
		@$el.$cards.addClass('m-hide')
		@

	selectFirst: ->
		@$el.$cards.$list
			.find('.cards_manager--cards_item:first-child input')
			.prop('checked', true)
			.trigger('change')
		@

	selectLast: ->
		@$el.$cards.$list
			.find('.cards_manager--cards_item:last-child input')
			.prop('checked', true)
			.trigger('change')
		@

	render: ->
		@$el.html CardsManager.Templates.Base()

		@stickit()
		@cacheDom()
		@initCollection()
		@

	initCollection: ->
		return unless @collection.length
		@collection.forEach (model) =>
			@addCard(model)

	cacheDom: ->
		@$el.$cards = @$('.cards_manager--cards')
		@$el.$cards.$list = @$('.cards_manager--cards_list')
		@$el.$formWrap = @$('.cards_manager--form_wrap')
		@$el.$addCard = @$('.cards_manager--add_another')

		@$el.$addCard.hide() if CardsManager.Config.MaxCards < 2

		@

	selectCard: ->
		if @collection.length
			@stickit()
			@selectFirst()
		else
			@showNewCardForm()

module.exports = View

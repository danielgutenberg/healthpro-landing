Backbone = require 'backbone'

class Model extends Backbone.Model
	defaults:
		card_id: ''


module.exports = Model

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
scrollTo = require 'utils/scroll_to_element'

class View extends Backbone.View

	className: 'client_appointments--list_wrap'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@instances = []

		@addListeners()
		@render()
		@cacheDom()

	addListeners: ->
		@listenTo @collections.appointments, 'data:ready', @renderAppointments
		@listenTo @collections.appointments, 'data:filtered', @reRenderAppointments
		@listenTo @collections.appointments, 'reset', @resetAppointments
		@

	render: ->
		@$el.html ClientAppointments.Templates.Appointments()
		@

	reRenderAppointments: ->
		@resetAppointments()
		@renderAppointments()

	renderAppointments: ->
		appointments = @collections.appointments.getFiltered()
		@toggleEmpty appointments.length is 0
		_.each appointments, (model) => @renderAppointment model
		@maybeActivateAppointment()
		@

	renderAppointment: (model) ->
		@instances.push view = new ClientAppointments.Views.Appointment
			baseView: @baseView
			parentView: @
			model: model
			collections: @collections
		@$el.$list.append view.el
		@

	resetAppointments: ->
		_.each @instances, (instance) -> instance?.remove()
		@instances = []
		@$el.$list.html ''
		@

	cacheDom: ->
		@$el.$list = @$el.find('[data-appointments-list]')
		@$el.$empty = @$el.find('[data-appointments-empty]')

		@

	maybeActivateAppointment: ->
		return unless @baseView.hash?.appointment?

		if @collections.appointments.currentFilter isnt 'upcoming_all'
			# display all active appointments
			@baseView.$el.$filter.val('upcoming_all').trigger 'change'
			return

		appointmentView = _.findWhere @instances, (instance) ->
			instance.model.get('id') is +@baseView.hash.appointment
		return unless appointmentView

		appointmentView.toggleContent()
		scrollTo appointmentView.$el
		@

	toggleEmpty: (display = true) ->
		if display
			@$el.$empty.removeClass 'm-hide'
		else
			@$el.$empty.addClass 'm-hide'

module.exports = View

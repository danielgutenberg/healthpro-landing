Handlebars = require 'handlebars'

require '../../../../styles/modules/client_appointments.styl'

window.ClientAppointments = ClientAppointments =
	Config:
		CancellationPeriod: 24 # hours
		Messages:
			CancelAppointment: 'Are you sure you want to cancel this appointment?'
			CancelAppointmentNoRefund: '<span class="m-error">This appointment is in less then 24 hours. Cancelling this appointment will not give you a refund.</span> Do you want to cancel anyway?'
			CancelAppointmentNoRefundInPerson: 'As the appointment you are canceling is less than 24 hours from now, you will incur charges from your professional according to his or her terms of service. Do you want to cancel anyway?'
	Views:
		Base: require('./views/base')
		Appointments: require('./views/appointments')
		Appointment: require('./views/appointment')
	Models:
		Base: require('./models/base')
		Appointment: require('./models/appointment')
	Collections:
		Appointments: require('./collections/appointments')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')
		Appointments: Handlebars.compile require('text!./templates/appointments.html')
		Appointment: Handlebars.compile require('text!./templates/appointment.html')

class ClientAppointments.App
	constructor: (options) ->
		@collections =
			appointments: new ClientAppointments.Collections.Appointments [],
				model: ClientAppointments.Models.Appointment

		@model = new ClientAppointments.Models.Base()

		_.extend options,
			collections: @collections
			model: @model

		@view = new ClientAppointments.Views.Base options

	close: ->
		@view.close?()
		@view.remove?()

if $('[data-client-appointments]').length
	new ClientAppointments.App
		type: 'dashboard'
		$container: $('[data-client-appointments]')

module.exports = ClientAppointments

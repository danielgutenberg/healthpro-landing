DeepModel = require 'backbone.deepmodel'
Moment = require 'moment'
getApiRoute = require 'hp.api'

class Model extends DeepModel

	defaults: -> {}


	getStatusLabel: ->
		switch @get('status')
			when 'confirmed' then 'Confirmed'
			when 'pending' then 'Pending'

	getCancellationMessage: ->
		message = ClientAppointments.Config.Messages.CancelAppointment
		if @get('from').diff(Moment(), 'hours') < ClientAppointments.Config.CancellationPeriod
			if @isPaidInPerson()
				message = ClientAppointments.Config.Messages.CancelAppointmentNoRefundInPerson
			else
				message = ClientAppointments.Config.Messages.CancelAppointmentNoRefund
		message

	remove: ->
		$.ajax
			url: getApiRoute('ajax-appointment', appointmentId: @get('id'))
			method: 'delete'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify
				_token: window.GLOBALS._TOKEN
			success: =>
				@set 'id', null
				@destroy()

	isFuture: ->
		@get('from_utc').isAfter(Moment().utc())

	isPaidInPerson: ->
		@get('payInPerson')?


module.exports = Model

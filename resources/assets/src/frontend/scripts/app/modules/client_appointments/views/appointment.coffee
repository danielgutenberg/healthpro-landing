Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
getRoute = require 'hp.url'
LocationFormatter = require 'utils/formatters/location'
vexDialog = require 'vexDialog'

require 'tooltipster'

class View extends Backbone.View

	className: 'client_appointment'
	tagName: 'li'

	events:
		'click [data-appointment-header]': 'toggleContent'
		'click [data-appointment-cancel]': 'cancel'


	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@instances = []

		@render()
		@cacheDom()

	render: ->
		# send some generic data directly to the template without stickit
		@$el.html ClientAppointments.Templates.Appointment @getTemplateData()

		@$el.find('[data-tooltip]').tooltipster
			theme: 'tooltipster-dark'

		@

	cacheDom: ->
		@$el.$list = @$el.find('[data-appointments-list]')
		@

	getTemplateData: ->
		{
			professional:
				full_name: @model.get('provider.full_name')
				avatar: do =>
					avatar = @model.get('provider.avatar')
					return '' unless avatar
					"<img src='#{avatar}' alt=''>"

			location:
				type: @model.get('location.location_type')
				name: LocationFormatter.getLocationName @model.get('location')
				address: LocationFormatter.getLocationAddressLabel @model.get('location')
				type_label: LocationFormatter.getLocationTypeLabel @model.get('location')
				url: LocationFormatter.getLocationGmapsUrl @model.get('location')

			service: @model.get('service')

			appointment_status:
				status: @model.get('status')
				label: @model.getStatusLabel()

			appointment_time:
				weekday: @model.get('date').format('dddd')
				date: @model.get('date').format('MMMM D, YYYY')
				from: @model.get('from').format('hh:mm a')
				until: @model.get('until').format('hh:mm a')
				timezone: @model.get('location').timezone

			is_future: @model.isFuture()

			package: do =>
				return null unless pkg = @model.get('package')
				type = if pkg.package.number_of_entities > -1 then 'standard' else 'recurring'
				{
					type: type
					is_recurring: type is 'recurring'
					type_label: if type is 'recurring' then 'monthly package' else 'package'
					number_of_entities: pkg.package.number_of_entities
					entities_left: pkg.entities_left
					url: getRoute('dashboard-package', {packageId: pkg.package.id})
				}
		}

	toggleContent: ->
		if @$el.hasClass 'm-opened'
			@$el.removeClass 'm-opened'
		else
			@$el.addClass 'm-opened'
		@

	cancel: (e) ->
		e?.preventDefault()
		vexDialog.buttons.NO.text = 'No'
		vexDialog.buttons.YES.text = 'Yes'
		vexDialog.confirm
			message: @model.getCancellationMessage()
			callback: (value) =>
				return unless value

				@baseView.showLoading()
				$.when(
					@model.remove()
				).then(
					(response) =>
						@baseView.hideLoading()
						if response.data is true
							@collections.appointments.remove @model
							@parentView.instances.splice @parentView.instances.indexOf(@), 1
							@remove()
						return
				).fail(
					=> @baseView.hideLoading()
				)
		@

module.exports = View

Backbone = require 'backbone'
getApiRoute = require 'hp.api'
datetimeParsers = require 'utils/datetime_parsers'
Moment = require 'moment'

class Collection extends Backbone.Collection

	initialize: ->
		@currentFilter = null
		@currentType = null
		@getData
			filter: @currentFilter

	getFilterOptions: -> [
		{
			value: 'upcoming_week', label: 'Upcoming this week'
		}
		{
			value: 'upcoming_week_next', label: 'Upcoming next week'
		}
		{
			value: 'upcoming_month', label: 'Upcoming this month'
		}
		{
			value: 'upcoming_month_next', label: 'Upcoming next month'
		}
		{
			value: 'upcoming_all', label: 'All upcoming'
		}

		{
			value: 'history', label: 'Past appointments'
		}
	]

	getData: (options = {}) ->
		filter = options.filter ? 'upcoming_all'
		type = if filter is 'history' then 'past' else 'future'

		@currentFilter = filter
		# do not reload data if we have the same type
		if @currentType is type
			@trigger 'data:filtered'
			return true

		@currentType = type

		@reset() if options.reset
		@trigger 'data:loading'

		$.ajax
			url: getApiRoute('ajax-appointments')
			method: 'get'
			type  : 'json'
			data:
				status: if @currentType is 'past' then 'confirmed' else 'pending,confirmed'
				type: @currentType
			contentType: 'application/json; charset=utf-8'
			success: (response) =>
				_.each response.data, (item) => @push @formatAppointment(item)
				@trigger 'data:ready'
		@

	formatAppointment: (item) ->
		{
			id: item.id
			client: item.client
			confirmLink: item.confirmLink
			provider: item.provider
			location: item.location
			date: datetimeParsers.parseToMoment(item.date)
			from: datetimeParsers.parseToMoment(item.from)
			from_utc: datetimeParsers.parseToMoment(item.from_utc)
			payInPerson: item.payInPerson?
			until: datetimeParsers.parseToMoment(item.until)
			service: item.service
			registration: item.registration
			initiated_by: item.initiated_by
			price: item.price
			duration: item.duration
			status: item.status
			package: item.package
			cancellation_policy: item.cancellation_policy
		}

	getFiltered: ->
		start = null
		end = null

		switch @currentFilter
			when 'upcoming_week'
				start = Moment().startOf('day')
				end = start.clone().endOf('week')

			when 'upcoming_week_next'
				start = Moment().weekday(7).startOf('day')
				end = start.clone().endOf('week')

			when 'upcoming_month'
				start = Moment().startOf('day')
				end = start.clone().endOf('month')

			when 'upcoming_month_next'
				start = Moment().add(1, 'month').startOf('month')
				end = start.clone().endOf('month')

			when 'upcoming_all'
				start = Moment().startOf('day')
				end = start.clone().add(1, 'years')

		@sort()

		if start and end
			return @filter (model) -> model.get('from').isAfter(start) and model.get('from').isBefore(end)

		@models


	comparator: (a, b) ->
		switch @currentType
			when 'future'
				if a.get('from').isBefore(b.get('from')) then -1 else 1
			when 'past'
				if a.get('from').isBefore(b.get('from')) then 1 else -1
			else
				1

module.exports = Collection

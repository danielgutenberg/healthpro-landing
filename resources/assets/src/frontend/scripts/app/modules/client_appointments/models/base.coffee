Backbone = require 'backbone'

class Model extends Backbone.Model

	defaults: ->
		filter: 'upcoming_all'

module.exports = Model

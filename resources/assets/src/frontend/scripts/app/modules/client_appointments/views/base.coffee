Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

parseHash = require 'utils/parse_hash'

require 'backbone.stickit'

class View extends Backbone.View

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@hash = parseHash false

		@instances = []

		@addListeners()
		@bind()
		@render()
		@cacheDom()
		@appendAppointments()

	addListeners: ->
		@listenTo @collections.appointments, 'data:loading', @showLoading
		@listenTo @collections.appointments, 'data:ready', @hideLoading
		@listenTo @model, 'change:filter', @filterAppointments

		@

	bind: ->
		@bindings =
			'[data-appointments-filter]':
				observe: 'filter'
				selectOptions:
					collection: @collections.appointments.getFilterOptions()
		@

	render: ->
		@$el.html ClientAppointments.Templates.Base()
		@$container.html @$el
		@stickit()
		@

	appendAppointments: ->
		@instances.push @appointmentsView = new ClientAppointments.Views.Appointments
			collections: @collections
			baseView: @

		@$el.$appointments.append @appointmentsView.el
		@

	cacheDom: ->
		@$body = $('body')
		@$el.$appointments = @$el.find('[data-appointments]')
		@$el.$filter = @$el.find('[data-appointments-filter]')
		@$el.$loading = @$el.find('.loading_overlay')
		@

	filterAppointments: ->
		@collections.appointments.getData
			filter: @model.get('filter')
			reset: true
		@

module.exports = View

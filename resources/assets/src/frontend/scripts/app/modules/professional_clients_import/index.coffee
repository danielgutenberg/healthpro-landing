Handlebars = require 'handlebars'

ProfessionalClientsImport = window.ProfessionalClientsImport =
	Views:
		Base: require('./views/base')
		Steps:
			DownloadTemplate: require('./views/steps/download_template')
			UploadType: require('./views/steps/upload_type')
			UploadCsv: require('./views/steps/upload_csv')
			UploadForm: require('./views/steps/upload_form')
			UploadComplete: require('./views/steps/upload_complete')
			NotifyClients: require('./views/steps/notify_clients')
	Models:
		Base: require('./models/base')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')
		Steps:
			DownloadTemplate: Handlebars.compile require('text!./templates/steps/download_template.html')
			UploadType: Handlebars.compile require('text!./templates/steps/upload_type.html')
			UploadCsv: Handlebars.compile require('text!./templates/steps/upload_csv.html')
			UploadForm: Handlebars.compile require('text!./templates/steps/upload_form.html')
			UploadComplete: Handlebars.compile require('text!./templates/steps/upload_complete.html')
			NotifyClients: Handlebars.compile require('text!./templates/steps/notify_clients.html')

ProfessionalClientsImport.Steps = [
	{
		showClose: true
		title: 'Step 1: Notify Your Clients'
		titleIcon: true
		slug: 'NotifyClients'
	}
	# {
	# 	showClose: true
	# 	title: 'Step 2: Choose Your Way To Upload'
	# 	titleIcon: true
	# 	slug: 'UploadType'
	# }
	{
		showClose: true
		title: 'Step 2: Fill The Form'
		titleIcon: true
		slug: 'UploadForm'
	}
	{
		showClose: true
		title: 'Step 3: Download Template'
		titleIcon: true
		slug: 'DownloadTemplate'
	}
	{
		showClose: true
		title: 'Step 4: Upload CSV'
		titleIcon: true
		slug: 'UploadCsv'
	}
	{
		showClose: true
		title: 'Inviting Client(s) Complete'
		titleIcon: true
		slug: 'UploadComplete'
	}
]

class ProfessionalClientsImport.App
	constructor: (options = {}) ->
		@model = new ProfessionalClientsImport.Models.Base
			couponCode: options.couponCode

		@view = new ProfessionalClientsImport.Views.Base
			model: @model
			popup: options.popup
			couponCode: options.couponCode

		model: @model
		view: @view
		close: @close

	close: ->
		@view.close?()
		@view.remove?()

module.exports = ProfessionalClientsImport

AbstractView = require './abstract'

class View extends AbstractView

	additionalEvents:
		'click [data-save-msg]': 'saveMsg'
		'click [data-edit-msg]': 'editMsg'
		'click [data-cancel-edit-msg]': 'cancelEditMsg'

	afterInit: ->
		super
		@renderVars.emailBody = @model.get('emailBody')
		@renderVars.professionalPublicUrl = @model.get('professional.publicUrl')
		@renderVars.professionalFullName = @model.get('professional.fullName')

	afterRender: ->
		super
		@cacheDom()
		@bind()

	cacheDom: ->
		@$el.$msg = @$el.find('[data-msg]')

	bind: ->
		# force paragraphs on enter
		@$el.$msg.on 'keypress', (e) =>
			if e.keyCode is 13
				document.execCommand 'formatBlock', false, 'p'

	saveMsg: ->
		@model.set 'emailBody', @$el.$msg.html()
		@toggleEdit false
		@

	editMsg: ->
		@toggleEdit true

	cancelEditMsg: ->
		@$el.$msg.html @model.get('emailBody')
		@toggleEdit false

	toggleEdit: (edit = true) ->
		if edit
			@$el.addClass 'm-edit_msg'
			@$el.$msg.prop 'contenteditable', true
		else
			@$el.removeClass 'm-edit_msg'
			@$el.$msg.prop 'contenteditable', false

		@parentView.centerPopup()

module.exports = View

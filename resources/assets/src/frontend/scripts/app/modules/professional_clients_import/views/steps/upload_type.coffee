AbstractView = require './abstract'

class View extends AbstractView

	additionalEvents:
		'click [data-type-upload]': 'typeUpload'
		'click [data-type-fill]': 'typeFill'

	typeUpload: -> @jumpToStep 'DownloadTemplate'

	typeFill: -> @jumpToStep 'UploadForm'

module.exports = View

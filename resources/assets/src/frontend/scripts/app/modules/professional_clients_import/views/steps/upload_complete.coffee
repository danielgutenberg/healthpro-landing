AbstractView = require './abstract'

class View extends AbstractView

	additionalEvents:
		'click [data-statistics-cta]': 'switchStats'
		'click [data-done]': 'done'

	afterInit: ->
		@renderVars.uploadStatistics = @model.get('uploadStatistics')

	afterRender: ->
		super
		@cacheDom()
		@bind()

	cacheDom: ->
		@$el.$short = @$el.find('[data-statistics-short]')
		@$el.$full = @$el.find('[data-statistics-full]')
		@$el.$cta = @$el.find('[data-statistics-cta]')

	switchStats: (e) ->
		e?.preventDefault()
		@$el.$short.toggleClass 'm-hide'
		@$el.$full.toggleClass 'm-hide'
		@parentView.centerPopup()
		@

	done: (e) ->
		e?.preventDefault()
		@showLoading()
		window.location.reload()
		@

module.exports = View

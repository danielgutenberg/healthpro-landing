Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Model extends Backbone.Model

	defaults:
		professional: {}
		sendEmails: true
		emailBody: null
		uploadStatistics: null
		couponDiscount: null

	initialize: (options = {}) ->
		super
		@couponCode = options.couponCode
		@loadProfessionalData()

	setEmailBody: ->
		@set 'emailBody', @getDefaultEmailBody()
#		emailBody = @getDefaultEmailBody()
#		for v in ['full_name', 'public_url']
#			emailBody = emailBody.replace "[#{v}]",  @get("professional.#{v}")
#		@set 'emailBody', emailBody
		@

	loadProfessionalData: ->
		$.ajax
			method: 'get'
			url: getApiRoute('ajax-profiles-self')
			success: (response) =>
				@set
					'professional.fullName': response.data.full_name
					'professional.publicUrl': response.data.public_url
				@setEmailBody()
				@trigger 'data:ready'

	upload: (file) ->
		data = new FormData()
		data.append 'file', file
		data.append 'sendEmails', @get('sendEmails') * 1
		data.append 'customEmailBody', @get('emailBody')
		data.append '_token', window.GLOBALS._TOKEN

		$.ajax
			url: getApiRoute('ajax-provider-import-csv', {providerId: 'me'})
			method: 'post'
			cache: false
			dataType: 'json'
			processData: false
			contentType: false
			show_alerts: false
			data: data

	uploadData: (data) ->
		$.ajax
			url: getApiRoute('ajax-provider-import-list', {providerId: 'me'})
			method: 'post'
			dataType: 'json'
			data:
				data: data
				sendEmails: @get('sendEmails') * 1
				customEmailBody: @get('emailBody')
				couponDiscount: @get('couponDiscount')
				_token: window.GLOBALS._TOKEN

	getDefaultEmailBody: ->
		body = """
		<p>Hi &lt;client&gt;,</p>
		"""
		if @couponCode
			couponAmount = window.GLOBALS.COUPON_AMOUNT
			validUntil = window.GLOBALS.VALID_DATE
			if window.GLOBALS.COUPON_TYPE == 'fixed'
				@set 'couponType', 'fixed'
				discount = '$' + couponAmount
			else
				@set 'couponType', 'percent'
				discount = couponAmount + '%'
			@set 'couponDiscount', discount
			body += """
			<p>I would like to give you #{discount} off your next appointment. To receive your discount, visit #{@get('professional.publicUrl')} and enter coupon code <strong> #{@couponCode} </strong> at checkout. </p>
			<p>The coupon code is valid until #{validUntil}. Book now to claim your discount.</p>
			"""
		else
			body += """
			<p>#{@get('professional.fullName')} is now using HealthPRO to schedule appointments. HealthPRO will allow you, the client, to self schedule, change or cancel appointments 24/7. It’s simple, fast and will SAVE YOU TIME.</p>
			<p>All you need to do is &lt;confirm_link&gt; to choose a password to create your account. Then you will be able to view #{@get('professional.fullName')}'s profile and schedule appointments. </p>
			<p>Questions? &lt;contact_us_link&gt;</p>
			"""
		body += """
		<p>Best in Health,</p>
		<p>The HealthPRO Team</p>
		"""

		body

module.exports = Model

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
#getApiRoute = require 'hp.api'

class View extends Backbone.View

	currentStep: null

	stepView: null

	popupName: 'professional_clients_import'

	attributes:
		'class': 'professional_clients_import popup'
		'data-popup': 'professional_clients_import'
		'id' : 'professional_clients_import_popup'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@couponCode = options.couponCode

		@render()
		@cacheDom()
		@bind()

	render: ->
		# make sure we don't have a popup
		@removePopup()

		# render element
		@$el.html ProfessionalClientsImport.Templates.Base()

		# append to DOM
		@appendPopup()

	appendPopup: ->
		$('body').append @$el

		window.popupsManager.addPopup @$el
		window.popupsManager.openPopup @popupName

		popup = @getPopup()
		popup.setCloseOnEsc false if popup

	cacheDom: ->
		@$el.$header = @$el.find('[data-header]')
		@$el.$title = @$el.find('[data-header-title]')
		@$el.$container = @$el.find('[data-view-container]')
		@$el.$close = @$el.find('[data-close]')
		@$el.$loading = @$el.find('.loading_overlay')

	bind: ->
		# make sure we remove popup when user closes it
		@$el.on 'popup:closed', => @removePopup()

		@$el.$close.on 'click', (e) =>
			e?.preventDefault()
			@removePopup()

		@showLoading()
		@model.on 'data:ready', =>
			@hideLoading()
			@nextStep()

	removePopup: ->
		@close()
		window.popupsManager.removePopup @popupName
		@

	centerPopup: ->
		popup = @getPopup()
		popup.detectBigPopup() if popup

	getPopup: -> window.popupsManager.getPopup @popupName

	prevStep: ->
		@loadStep('prev')
		@openStep()

	nextStep: ->
		@loadStep('next')
		@openStep()

	openStep: ->
		return unless @step
		@maybeShowHeader()
		@maybeShowClose()
		@openStepView()

	openStepView: ->
		if @stepView
			@stepView.close()

		@stepView = new ProfessionalClientsImport.Views.Steps[@step.slug]
			parentView: @
			model: @model
			slug: @step.slug
			couponCode: @couponCode

		@$el.$container.html @stepView.render().$el

		@centerPopup()

		@

	loadStep: (type = 'next') ->
		@currentStep = 0 if @currentStep is null
		@currentStep++ if type is 'next'
		@currentStep-- if type is 'prev'

		if ProfessionalClientsImport.Steps.length >= @currentStep and @currentStep > 0
			@step = ProfessionalClientsImport.Steps[@currentStep-1]
		else
			@currentStep-- if type is 'next'
			@currentStep++ if type is 'prev'
		@

	jumpToStep: (stepSlug) ->
		stepIndex = _.findIndex ProfessionalClientsImport.Steps, slug: stepSlug

		@step = ProfessionalClientsImport.Steps[stepIndex]
		@currentStep = stepIndex + 1

		@openStep()

	maybeShowHeader: ->
		if @step.title
			@$el.$header.removeClass 'm-hide'
			if @step.slug == 'NotifyClients' and @couponCode
				amount = window.GLOBALS.COUPON_AMOUNT
				if window.GLOBALS.COUPON_TYPE == 'fixed'
					@$el.$title.text 'Send Your Clients a Coupon Code So They Can Save $' + amount
				else
					@$el.$title.text 'Send Your Clients a Coupon Code So They Can Save ' + amount + '%'
			else
				@$el.$title.text @step.title
			# if @step.titleIcon
			# 	@$el.$title.addClass 'm-icon'
			# else
			# 	@$el.$title.removeClass 'm-icon'

		else
			@$el.$header.addClass 'm-hide'
			@$el.$title.text ''
		@

	maybeShowClose: ->
		if @step.showClose
			@$el.$close.removeClass 'm-hide'
		else
			@$el.$close.addClass 'm-hide'
		@


module.exports = View

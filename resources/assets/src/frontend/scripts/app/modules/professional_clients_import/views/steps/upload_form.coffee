AbstractView = require './abstract'
NameValidator = require 'utils/validators/name'
EmailValidator = require 'utils/validators/email'
PhoneValidator = require 'utils/validators/phone'
Phone = require 'framework/phone'

# init perfect scrollbar
require('perfect-scrollbar')($)
require 'perfect-scrollbar/dist/css/perfect-scrollbar.min.css'

class View extends AbstractView

	additionalEvents:
		'click [data-upload]': 'upload'
		'click [data-to-upload-type]': 'backToUploadType'
		'click [data-form-add-row]': 'addRow'
		'click [data-form-remove-row]': 'removeRow'

	afterRender: ->
		super
		@cacheDom()
		@initScroll()
		@addRow()

	cacheDom: ->
		@$el.$form = @$el.find('[data-form]')
		@$el.$formBody = @$el.find('[data-form-body]')
		@$el.$error = @$el.find('[data-form-error]')
		@$el.$addRowButton = @$el.find('[data-form-add-row]')

	addRow: ->
		@hideInputErrors()
		@showError false
		@$el.$formBody.append @getRowTemplate()
		@parentView.centerPopup()
		@$el.$formBody.perfectScrollbar('update')
		@$el.$formBody.scrollTop @$el.$formBody.get(0).scrollHeight
		@bindPhone()
		if @$el.$formBody.find('[data-form-row]').length > 4
			@$el.$addRowButton.addClass 'm-hide'

	removeRow: (e) ->
		$(e.currentTarget).closest('[data-form-row]').remove()
		@parentView.centerPopup()
		@addRow() unless @getFormRows().length
		@$el.$formBody.perfectScrollbar('update')
		if @$el.$formBody.find('[data-form-row]').length < 5
			@$el.$addRowButton.removeClass 'm-hide'

	bindPhone: ->
		@$phoneEl = @$el.$formBody.find('[data-field="phone"]')
		_.each @$phoneEl, (item) =>
			unless $(item).data('phone-inited')
				@phone = new Phone $(item)
				$(item).data('phone-inited', true)

	upload: ->
		@collectFormData()
		if !@validateForm()
			@showError 'Please fix form errors before uploading'
			return

		@showError false
		@showLoading()
		$.when(
			@model.uploadData(@collectFormData())
		).then(
			(data) =>
				@hideLoading()
				@model.set 'uploadStatistics', data.data
				@jumpToStep 'UploadComplete'

		).fail(
			(response) =>
				@hideLoading()
				errorMessage = 'Something went wront. Please reload the page and try again'
				if response.responseJSON.errors?.email?
					errorMessage = 'Please provide valid email addresses for your clients.'
				else if response.responseJSON.errors?.error?.messages?
					errorMessage = response.responseJSON.errors.error.messages[0]
				@showError errorMessage
		)

		@


	backToUploadType: -> @jumpToStep 'UploadType'

	getRowTemplate: ->
		"""
			<div class="professional_clients_import--form_row" data-form-row>
				<div class="professional_clients_import--form_cell"><input type="text" data-field='first_name' name="client[][first_name]"></div>
				<div class="professional_clients_import--form_cell"><input type="text" data-field='last_name' name="client[][last_name]"></div>
				<div class="professional_clients_import--form_cell"><input type="text" data-field='email' name="client[][email]"></div>
				<div class="professional_clients_import--form_cell" data-field-parent><input type="text" data-field='phone' name="client[][phone]"></div>
				<div class="professional_clients_import--form_row_remove"><button data-form-remove-row></button></div>
			</div>
		"""

	getFormRows: -> @$el.$form.find('[data-form-row]')

	getFormInputs: -> @$el.$form.find('[data-field]')

	validateForm: ->
		@showError false

		$inputs = @getFormInputs()
		$inputs.removeClass 'm-error'

		isFormValid = true

		$inputs.each (k, input) =>
			$input = $(input)
			val = $input.val().trim()
			isValid = switch $input.data('field')
				when 'first_name', 'last_name'
					NameValidator $input.val()
				when 'email'
					EmailValidator $input.val()
				when 'phone'
					# phone is optional
					if val then PhoneValidator(val) else true

			unless isValid
				$input.addClass 'm-error'
				isFormValid = false
			return

		isFormValid


	collectFormData: ->
		data = []
		@getFormRows().each (k, row) =>
			$row = $(row)
			rowData =
				email: $row.find('input[data-field="email"]').val()
				first_name: $row.find('input[data-field="first_name"]').val()
				last_name: $row.find('input[data-field="last_name"]').val()
				phone: '1' + $row.find('input[data-field="phone"]').val().replace(/\D/g,'')
			data.push rowData
		data

	hideInputErrors: ->
		$inputs = @$el.$form.find('[data-form-row] [data-field]')
		$inputs.removeClass 'm-error'

	showError: (error = false) ->
		if error
			@$el.$error.removeClass 'm-hide'
			@$el.$error.text error
		else
			@$el.$error.addClass 'm-hide'
			@$el.$error.text ''

		@parentView.centerPopup()


	initScroll: ->
		@$el.$formBody.perfectScrollbar
			suppressScrollX: true
			includePadding: true
			minScrollbarLength: 20

module.exports = View

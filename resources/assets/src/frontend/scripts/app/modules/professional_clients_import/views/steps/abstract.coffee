Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

class View extends Backbone.View

	additionalEvents: {}

	renderVars: {}

	className: 'professional_clients_import--container'

	events:
		'click [data-prev-step]': 'prevStep'
		'click [data-next-step]': 'nextStep'
		'click [data-close-popup]': 'closePopup'

	initialize: (options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@couponCode = options.couponCode

		@setupEvents()
		@afterInit()

	afterInit: ->

	afterRender: ->

	prevStep: ->
		@parentView.prevStep()
		@

	nextStep: ->
		@parentView.nextStep()
		@

	jumpToStep: (stepSlug) ->
		@parentView.jumpToStep stepSlug
		@

	showLoading: ->
		@parentView.showLoading()
		@

	hideLoading: ->
		@parentView.hideLoading()
		@

	closePopup: ->
		@parentView.removePopup()

	setupEvents: ->
		_.each @additionalEvents, (method, event) => @events[event] = method
		@delegateEvents()

	render: ->
		@$el.html ProfessionalClientsImport.Templates.Steps[@slug](@renderVars)
		@trigger 'rendered'
		@afterRender()
		@

module.exports = View

AbstractView = require './abstract'
FileUpload = require 'framework/fileupload'

class View extends AbstractView

	errors:
		invalidFile: 'Please select a valid CSV file'
		invalidLength: 'Please select a single CSV file'
		invalidSize: 'File size should be less than 10MB'

	validExtensions: ['csv']

	additionalEvents:
		'click [data-upload]': 'upload'
		'click [data-to-upload-type]': 'backToUploadType'

	afterRender: ->
		super
		@cacheDom()
		@initFileUpload()

	upload: ->
		error = @validateFile()
		if error
			@showError error
			return

		@showError false
		@showLoading()
		$.when(
			@model.upload(@fileUpload.getFile())
		).then(
			(data) =>
				@hideLoading()
				@model.set 'uploadStatistics', data.data
				@jumpToStep 'UploadComplete'
		).fail(
			(response) =>
				@hideLoading()
				errorMessage = 'Something went wront. Please reload the page and try again'
				if response.responseJSON.errors?.email?
					errorMessage = 'Please provide valid email addresses for your clients.'
				else if response.responseJSON.errors?.error?.messages?
					errorMessage = response.responseJSON.errors.error.messages[0]

				@showError errorMessage
		)

		@

	validateFile: ->
		files = @fileUpload.getFilesList()

		return @errors.invalidFile if files.length is 0
		return @errors.invalidLength if files.length > 1

		file = files[0]
		ext = file.name.split('.').pop().toLowerCase()

		return @errors.invalidFile if  _.indexOf(@validExtensions, ext) < 0

		false

	cacheDom: ->
		@$el.$fileupload = @$el.find('.fileupload')
		@$el.$error = @$el.find('[data-file-upload-error]')

	showError: (error = false) ->
		if error
			@$el.$error.removeClass 'm-hide'
			@$el.$error.text error
		else
			@$el.$error.addClass 'm-hide'
			@$el.$error.text ''

		@parentView.centerPopup()

	initFileUpload: ->
		@fileUpload = new FileUpload @$el.$fileupload

		@$el.$fileupload.on 'fileupload:changed', =>
			@showError false

	backToUploadType: -> @jumpToStep 'UploadType'

module.exports = View

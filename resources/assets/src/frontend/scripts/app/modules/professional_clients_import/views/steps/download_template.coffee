AbstractView = require './abstract'

downloadJs = require 'libs/downloadjs'

class View extends AbstractView

	downloadOptions:
		fileName: 'clients_template.csv'
		mimeType: 'text/csv'
		fileContent: """
Last Name,First Name,Email,Phone (optional)
,,,
,,,
,,,
,,,
		"""

	additionalEvents:
		'click [data-download]': 'downloadTemplate'

	downloadTemplate: ->
		downloadJs @downloadOptions.fileContent, @downloadOptions.fileName, @downloadOptions.mimeType
		return

module.exports = View

Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
getRoute = require 'hp.url'
vexDialog = require 'vexDialog'

require 'backbone.stickit'
require 'payment'

class View extends Backbone.View

	el: '[data-membership-container="sign"]'

	initialize:(options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addStickit()
		@addListeners()
		@render()

	addListeners: ->
		@listenTo @, 'email_loading:hide', =>
			@hideEmailLoading()

	cacheDom: ->
		@$el.$socials = @$el.find('.btn_social')
		@$el.$email = @$el.find('[name="email"]')
		@$el.$emailLoading = @$el.find('[data-email-loading]')
		@$el.$validateOnBlur = @$el.find('[data-validate-blur]')

	bind: ->
		@$el.$socials.on 'click', (e) =>
			e.preventDefault()
			@socialLogin $(e.currentTarget)[0].href

		@$el.$email.on 'blur', (e) =>
			@checkEmail(@$el.$email.val())

		@$el.$validateOnBlur.on 'blur', (e) =>
			field = $(e.currentTarget).attr('name')
			@model.isValid(field)


	addStickit: ->
		@bindings =
			'[name="first_name"]': 'first_name'
			'[name="last_name"]': 'last_name'
			'[name="email"]': 'email'
			'[name="password"]': 'password'

	render: ->
		@$el.html MembershipSignup.Templates.SignUp
			dev_env: window.GLOBALS._ENV is 'local'
			route_facebook: getRoute('social-auth', {service: 'facebook'})
			route_linkedin: getRoute('social-auth', {service: 'linkedin'})
			route_google: getRoute('social-auth', {service: 'google'})
		@stickit()
		@cacheDom()
		@bind()
		@

	socialLogin: (url) ->
		link = "#{url}?profile=#{@profile}"
		link += '&login=false'
		link += '&product_id=' + @model.get('product_id')
		# if @$el.find('.auth_form--terms').is(':checked') is false
		# 	@acceptTerms link
		# 	return

		window.location.href = link

	acceptTerms: (link) ->
		termsLink = window.location.origin + '/terms'

		vexDialog.open
			message: 'Please accept our terms and conditions to continue.'
			input: """
				<input class="vex-custom-checkbox" type="checkbox">
					I agree to HealthPRO's <a href="#{ termsLink }" target="_blank">Terms of Service</a>
				</input>
			"""
			afterOpen: ($vexContent) ->

				$btn = $('.vex-dialog-button-primary')
				$check = $('.vex-custom-checkbox')

				$btn.attr("disabled","disabled")
				$btn.css("background","#e0e0e0")
				$check.change ->
					if $(this).is(':checked')
						$btn.removeAttr("disabled")
						$btn.css("background","#2b95a7")
					else
						$btn.attr("disabled","disabled")
						$btn.css("background","#e0e0e0")

			callback: =>
				unless $('.vex-custom-checkbox').is(':checked')
					return false
				window.location.href = link

	checkEmail: (email) ->
		@showEmailLoading()
		unless @emailIsChecking
			@emailIsChecking = true
			$.when( @model.checkEmail(email) )
			.then(
				(response) =>
					@emailIsChecking = false
					if response?.data.exists
						@parentView.setErrors({email: {messages: [MembershipSignup.Config.Messages.email_exists]}})
					@hideEmailLoading()
				, (err) =>
					@emailIsChecking = false
					@parentView.setErrors({email: {messages: ['Please enter a valid email']}})
					@hideEmailLoading()

			)

	showEmailLoading: -> @$el.$emailLoading.removeClass 'm-hide'
	hideEmailLoading: -> @$el.$emailLoading.addClass 'm-hide'

module.exports = View

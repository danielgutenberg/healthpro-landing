Backbone = require 'backbone'
Handlebars = require 'handlebars'
getRoute = require 'hp.url'

window.MembershipSignup =
	Config:
		Messages:
			error: 'Something went wrong. Please reload the page and try again.'
			email_exists: '''
				This email address is already in use on our system. <br>
				Is this email associated with your client account? <br>
				<b><a href="''' + getRoute('login') + '''">LOG IN</a></b> to your HealthPRO account.
			'''
	Views:
		Base: require('./views/base')
		SignUp: require('./views/signup')
		Payment: require('./views/payment')
	Models:
		Base: require('./models/base')
	Templates:
		Base: Handlebars.compile require('text!./templates/base.html')
		SignUp: Handlebars.compile require('text!./templates/signup.html')
		Payment: Handlebars.compile require('text!./templates/payment.html')

class MembershipSignup.App
	constructor: ->
		@eventBus = _.extend {}, Backbone.Events

		@model = new MembershipSignup.Models.Base()

		@views =
			base: new MembershipSignup.Views.Base
				eventBus: @eventBus
				model: @model


MembershipSignup.app = new MembershipSignup.App()

module.exports = MembershipSignup

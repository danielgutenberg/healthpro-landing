Backbone = require 'backbone'
Validation = require 'backbone.validation'
getApiRoute = require 'hp.api'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
DeepModel = require 'backbone.deepmodel'

class Model extends DeepModel

	defaults:
		profile_type: 'provider'
		first_name: ''
		last_name: ''
		email: ''
		password: ''
		payment_type: 'cc'
		card_name: ''
		card_number: ''
		card_exp: ''
		card_cvv: ''
		price_monthly: ''
		price_annual: ''
		product_id: window.GLOBALS.PRODUCTID
		total: ''

	validation:
		first_name: [
			{
				required: ->
					!window.GLOBALS._PID?
				msg: 'Please enter your first name'
			}
			{
				maxLength: 26
				msg: 'Max length is 26'
			}
			{
				pattern: /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/g
				msg: 'Only letters allowed'
			}
		]

		last_name: [
			{
				required: ->
					!window.GLOBALS._PID?
				msg: 'Please enter your last name'
			}
			{
				maxLength: 26
				msg: 'Max length is 26'
			}
			{
				pattern: /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/g
				msg: 'Only letters allowed'
			}
		]

		email: [
			{
				required: ->
					!window.GLOBALS._PID?
				msg: 'Please enter your email'
			}
			{
				pattern: 'email'
				msg: 'Please enter a valid email'
			}
		]

		password: [
			{
				required: ->
					!window.GLOBALS._PID?
				msg: 'Please enter a password'
			}
			{
				minLength: 7
				msg: 'Minimum 7 characters'
			}
		]

		card_name:
			required: ->
				@get('payment_type') == 'cc' and !@get('card_id')?
			msg: 'Please enter a name'

		card_number: (val, field, model) ->
			if @get('payment_type') == 'cc' and !@get('card_id')?
				return 'Please enter a credit card number' unless val
				return 'Min length is 12' if val.length < 12

		card_exp: [
			{
				required: ->
					@get('payment_type') == 'cc' and !@get('card_id')?
				msg: 'Please enter expiration date'
			}
			{
				pattern: /[0-1][0-9] \/ 20[0-9]{2}/
				msg: 'Please enter a valid expiration date'
			}
		]

		card_cvv: [
			{
				required: ->
					@get('payment_type') == 'cc' and !@get('card_id')?
				msg: 'Please enter a cvv'
			}
			{
				pattern: 'digits'
				msg: 'Only digits'
			}
			{
				maxLength: 4
				msg: 'Max length is 4'
			}
			{
				minLength: 3
				msg: 'Min length is 3'
			}
		]

		agree_terms:
			required: true
			acceptance: true
			msg: 'Please agree to "Terms of Service" before registering'

	initialize: ->
		super
		Cocktail.mixin @, Mixins.Models.Validation
		@addListeners()

	addListeners: ->
		@listenTo @, 'change:product_id', =>
			@selectedProduct = _.findWhere @products, {'id': @get('product_id')}
			@setSelected()

	setTotal: ->
		@set('total', @selectedProduct.price)

	setSelected: ->
		@setTotal()
		@set('product_name', @selectedProduct.name)
		switch @selectedProduct.billing_cycles
			when 1
				@set('product_cycle', 'month')
			when 12
				@set('product_cycle', 'year')

	setOptions: (options) ->
		yearProduct = _.findWhere options, {'billing_cycles': 12}
		monthProduct = _.findWhere options, {'billing_cycles': 1}

		@set('products.annual', yearProduct)
		@set('products.monthly', monthProduct)
		@products = _.values(@get('products'))
		@set('product_id', yearProduct.id)

		@setTotal()

	addCoupon: (coupon_name) ->
		$.ajax
			url: getApiRoute('ajax-coupon-get', { coupon: coupon_name })
			data:
				types: ['order', 'plan']
				for: 'provider'
			method: 'get'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			show_alerts: false

	submit: ->
		$.ajax
			url: getApiRoute('ajax-auth-sign-up')
			type: 'post'
			data: @getRegisterData()
			show_alerts: false

	getRegisterData: ->
		data = {
			first_name: @get('first_name') if @get('first_name')?
			last_name: @get('last_name')
			email: @get('email')
			password: @get('password')
			profile_type: @get('profile_type')
			payment_type: @get('payment_type')
			product_id: @get('product_id')
			couponId: @get('coupon.id')
			_token: window.GLOBALS._TOKEN
		}

		if @get('card_id')?
			data.payment_method_id = @get('card_id')
		else
			_.extend(data, {
				card_name: @get('card_name')
				card_number: @get('card_number').replace(/\s+/g, '')
				card_exp: @get('card_exp')
				card_cvv: @get('card_cvv')
			})

		if window.GLOBALS._PID?
			data.profile_id = window.GLOBALS._PID

		data

	checkEmail: (email) ->
		if @isValid('email')
			$.ajax
				url: getApiRoute('ajax-auth-check-email', { email: email })
				method: 'get'
				type: 'json'
				show_alerts: false

	getCards: ->
		$.ajax
			url: getApiRoute('ajax-credit-cards')
			method: 'get'
			success: (res) =>
				if res?.data[0]?
					@setData(res.data[0])

	setData: (data) ->
		if data.card_number?
			card_exp = ''
			if data.exp_month < 10 then card_exp = '0'
			card_exp += data.exp_month + ' / ' + data.exp_year

			@set('card_id', data.id)
			@set('card_name', data.card_holder)
			@set('card_number', data.card_number)
			@set('card_exp', card_exp)
			@set('card_cvv', null)

module.exports = Model

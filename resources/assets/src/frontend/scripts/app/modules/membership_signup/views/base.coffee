Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'
getRoute = require 'hp.url'
Formats = require 'config/formats'
Numeral = require 'numeral'
FillIn = require 'app/blocks/tester_fill_in'
navigate = require 'utils/navigate'
capterraTrack = require 'utils/capterra_tracker'

require 'backbone.stickit'
require 'payment'

class View extends Backbone.View

	el: '.membership--form'

	events:
		'click [data-form-el="submit"]': 'submitForm'
		'click [data-price-coupon-remove]': 'removeCoupon'

	initialize:(options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options
		@socialSignup = null
		@alreadyRegistered = false

		@addListeners()
		@addStickit()

		@addValidation()
		@maybeOpenPopups()
		@render()

	cacheDom: ->
		@$pricingBtn = $('[data-pricing-btn]')
		@$selectedPricingBtn = $('[data-selected-pricing]')
		@$el.$socials = @$el.find('.btn_social')
		@$el.$loading = $('[data-membership-loading]')
		@$el.$total = @$el.find('[data-price-total]')
		@$el.$submit = @$el.find('.membership--actions_submit')

		@$el.$signUpContainer = @$el.find('[data-membership-container="sign"]')
		@$el.$paymentContainer = @$el.find('[data-membership-container="payment"]')

		@$popupFailed = $('[data-popup="popup_payment_failed"]')
		@$popupFailed.details = @$popupFailed.find('.popup_response--details')
		@$popupFailed.retry = @$popupFailed.find('.popup_response--actions_retry')

	addListeners: ->

	addStickit: ->
		@bindings =
			'[data-selected-plan]': 'product_name'
			'[name="agree_terms"]': 'agree_terms'
			'[name="payment_type"]': 'payment_type'
			'[data-product="annual"]':
				attributes:
					[
						name: 'value',
						observe: 'products.annual.id'
					]
			'[data-product="monthly"]':
				attributes:
					[
						name: 'value',
						observe: 'products.monthly.id'
					]

			'[data-month-price]':
				observe: 'products.monthly.price'
				onGet: (val) ->
					Numeral(val).format(Formats.Price.Simple)

			'[data-year-price]':
				observe: 'products.annual.price'
				onGet: (val) ->
					Numeral(val).format(Formats.Price.Simple)

			'[name="product_id"]':
				observe: 'product_id'
				onSet: (val) ->
					parseInt(val)

			'[data-price-subtotal]':
				observe: 'total'
				onGet: (val) ->
					Numeral(val).format(Formats.Price.Simple)

			'[data-price-coupon]':
				observe: 'coupon.type'
				onGet: (val) ->
					if val?
						switch val
							when 'fixed'
								discount = @model.get('coupon.amount')
								return '-' + Numeral(discount).format(Formats.Price.Simple)
							when 'quantity'
								return @model.get('coupon.amount') + ' months free'

			'[data-price-total]':
				observe: ['total', 'coupon.amount', 'coupon.type','product_id']
				updateMethod: 'html'
				onGet: (val) ->
					if val? and val[0] isnt ''
						@setTotal(val)

			'[data-price-discounted]':
				classes:
					'm-hide':
						observe: 'coupon.amount'
						onGet: (val) ->
							return not val?

			'[data-charged-ammount]':
				observe: 'total'
				onGet: (val) ->
					Numeral(val).format(Formats.Price.Simple)

			'[data-charged-cycle]':
				observe: 'product_id'
				onGet: (val) ->
					if val? and val isnt ''
						@model.get 'product_cycle'

			'[data-charged-cycle-upgrade]':
				observe: 'product_id'
				onGet: (val) ->
					if val? and val isnt ''
						productCycle = @model.get 'product_cycle'
						switch productCycle
							when 'year' then return ''
							when 'month' then return 'upgrade or'

	bind: ->
		@$el.find('.field input').on 'keyup', =>
			@unsetErrors()

		if @alreadyRegistered
			@presetOptions()
			@stickit()

		@$pricingBtn.on 'click', (e) =>
			e.preventDefault()
			$item = $(e.currentTarget)
			options = $item.data 'product-options'

			@model.setOptions options

	render: ->
		@$el.html MembershipSignup.Templates.Base
			route_terms: getRoute('terms')
			social_signup: if @socialSignup? then @socialSignup.charAt(0).toUpperCase() + @socialSignup.slice(1)
			alreadyRegistered: @alreadyRegistered

		@cacheDom()
		@initSignUp()
		@initPayment()
		@stickit()
		@bind()

		new FillIn(@$el)
		@


	presetOptions: ->
		@model.set 'product_id', window.GLOBALS.PRODUCTID
		if @$selectedPricingBtn.length
			@model.setOptions @$selectedPricingBtn.data 'product-options'

	initSignUp: ->
		unless @socialSignup? or @alreadyRegistered
			@signUp = new MembershipSignup.Views.SignUp
				eventBus: @eventBus
				parentView: @
				model: @model

	initPayment: ->
		@payment = new MembershipSignup.Views.Payment
			eventBus: @eventBus
			baseView: @
			model: @model
			canTryExistingCard: @alreadyRegistered and !@socialSignup?

	closePopup: (target = 'popup_membership') ->
		window.popupsManager.closePopup target
		@

	openPopup: (target = 'popup_membership') ->
		window.popupsManager.openPopup target
		@

	submitForm: (e) ->
		e.preventDefault() if e?
		errors = @model.validate()
		if !errors
			unless @socialSignup? or @alreadyRegistered
				@signUp.trigger 'email_loading:hide'
			@showLoading()
			$.when( @model.submit() )
			.then(
				(response) =>
					switch response.message
						when 'success'
							@setGlobals response.data
							if response.data.paypal_link?
								window.location.href = response.data.paypal_link.replace('https://', 'http://')
								return
							capterraTrack()
							@closePopup()
							@openPopup('popup_payment_success')
							# navigate response.data.redirect_to ? @getLocationRoot()

						when 'error' then @setErrors(response.errors.messages)

				, (err) =>
					@hideLoading()
					if err?
						errors = err.responseJSON.errors
						if errors?
							if errors.error?
								# hanling card errors from stripe response
								console.log errors.error.messages[0]
								switch errors.error.messages[0]
									when 'Your card\'s security code is incorrect.'
										@setErrors
											cvv:
												messages: errors.error.messages
									when 'Your card has expired.'
										@setErrors
											card_number:
												messages: errors.error.messages
									# failed payment popup with error message
									else
										@showFailedPopup(errors.error.messages, false)
							else if errors.email?
								if errors.email.messages[0] is 'This email address is already in use on our system.'
									@setErrors
										email:
											messages: [MembershipSignup.Config.Messages.email_exists]
							else
								# backend error messages (fields errors)
								@setErrors errors
						else
							# common error popup (if no message passed)
							@showFailedPopup()
			)

	getLocationRoot: ->
		window.location.origin ? "#{window.location.origin}/" : "#{window.location.protocol}/#{window.location.host}/"

	showFailedPopup: (errorText, reload = true) ->
		if errorText?
			@$popupFailed.details.html('')
			errorText.forEach (error) =>
				@$popupFailed.details.append('<p><i>Error:</i> 	' + error + '</p>')

		@openPopup('popup_payment_failed')

		if not reload
			@$popupFailed.retry.on 'click', (e) =>
				e.preventDefault()
				@closePopup('popup_payment_failed')

	setErrors: (errors) ->
		_.forIn errors, (value, key) =>
			if key is 'cvv' then key = 'card_cvv'
			if key is 'number' then key = 'card_number'
			$field = $('input[name='+key+']', @$el)
			@showFieldError($field, value.messages[0])

	unsetErrors: ->
		@$el.find('.field--error').html('')
		@$el.find('.field.m-error, .form--group.m-error').removeClass('m-error')

	showFieldError: ($field, errors) ->
		$field
			.parent().addClass('m-error').end()
			.parents('.form--group').addClass('m-error')

		if errors?
			$errorContainer = $field.parent().find('.field--error')
			unless $errorContainer.length
				$errorContainer = $('<span class="field--error" />').appendTo( $field.parent() )
			errors = [errors] unless $.isArray(errors)
			errors.forEach (error) =>
				html = $errorContainer.html()
				html += error + '<br>' + html
				$errorContainer.html( html )

			@hideLoading()

	setTotal: (val) ->
		if val?
			total = val[0]
			discount = val[1]
			discount_type = val[2]
		else
			total = @model.get('total')
			discount = @model.get('coupon.amount')
			discount_type = @model.get('coupon.type')

		if discount? and discount_type is 'fixed'
			total = total - discount

		total = total.toString().split('.')
		productCycle = @model.get 'product_cycle'

		formattedPrice = '<b><span>$' + total[0] + '</span></b>.'
		formattedPrice += if total[1] then total[1] else '00'

		if discount_type is 'quantity'
			switch productCycle
				when 'year' then totalMonths = parseInt(discount) + 12
				when 'month' then totalMonths = parseInt(discount) + 1
			formattedPrice += ' / ' + totalMonths + ' months'
		else
			formattedPrice += ' / ' + productCycle

		return formattedPrice

	removeCoupon: (e) ->
		e?.preventDefault()
		@model.unset 'coupon.name'
		@model.unset 'coupon.amount'
		@model.unset 'coupon.type'

	setGlobals: (responseData) ->
		if responseData.profile_id?
			window.GLOBALS._PID = responseData.profile_id

		if responseData.profile_type?
			window.GLOBALS._PTYPE = responseData.profile_type
		@

	maybeOpenPopups: ->
		paymentSuccess = window.GLOBALS.PAYMENTSUCCESSPOPUP
		if paymentSuccess? and paymentSuccess != ''
			return window.popupsManager.openPopup paymentSuccess

		socialSignup = window.GLOBALS.SOCIALSIGNUP
		if socialSignup? and socialSignup != ''
			@socialSignup = socialSignup

		productId = window.GLOBALS.PRODUCTID
		if productId? and productId != ''
			@alreadyRegistered = true

		profile = window.GLOBALS._PID
		if profile? and profile != ''
			@alreadyRegistered = true

		if productId
			return window.popupsManager.openPopup 'popup_membership'

	showLoading: -> @$el.$loading.removeClass('m-hide')
	hideLoading: -> @$el.$loading.addClass('m-hide')

module.exports = View

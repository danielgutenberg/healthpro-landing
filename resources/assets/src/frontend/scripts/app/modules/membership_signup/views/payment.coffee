Backbone = require 'backbone'
Cocktail = require 'backbone.cocktail'
Mixins = require 'utils/backbone_mixins'

require 'backbone.stickit'
require 'payment'

class View extends Backbone.View

	el: '[data-membership-container="payment"]'

	events:
		'click [data-coupon-opener]': 'showCouponForm'
		'click [data-coupon-submit]': 'addCoupon'

	initialize:(options) ->
		Cocktail.mixin @, Mixins.Views.Base, Mixins.Common
		@setOptions options

		@addStickit()
		@addListeners()
		if @canTryExistingCard
			@tryExistingCard()
		else
			@render()

	cacheDom: ->
		@$el.$cardForm = @$el.find('[data-card-info]')
		@$el.$cardName = @$el.find('[name="card_name"]')
		@$el.$cardNumber = @$el.find('[name="card_number"]')
		@$el.$cardCvc = @$el.find('[name="card_cvv"]')
		@$el.$cardExp = @$el.find('[name="card_exp"]')

		@$el.$couponOpener = @$el.find('[data-coupon-opener]')
		@$el.$couponFields = @$el.find('[data-coupon-fields]')
		@$el.$couponInput = @$el.find('[data-coupon-input]')
		@$el.$couponSubmit = @$el.find('[data-coupon-submit]')
		@$el.$couponLoading = @$el.find('.membership_payment--coupon_loading')

		@$el.$loading = $('<span class="loading_overlay m-light m-hide"><span class="spin m-logo"></span></span>')
		@$el.append @$el.$loading

	addStickit: ->
		@bindings =
			'[name="first_name"]': 'first_name'
			'[name="last_name"]': 'last_name'
			'[name="email"]': 'email'
			'[name="password"]': 'password'
			'[name="card_name"]':
				observe: 'card_name'
				onSet: (val) ->
					@model.set('card_id', null)
					return val

			'[name="card_number"]':
				observe: 'card_number'
				onGet: (val) ->
					if val? and val.length is 4
						return '•••• •••• •••• ' + val
				onSet: (val) ->
					@model.set('card_id', null)
					return val

			'[name="card_exp"]':
				observe: 'card_exp'
				onSet: (val) ->
					@model.set('card_id', null)
					return val

			'[name="card_cvv"]':
				observe: 'card_cvv'
				onGet: (val) ->
					if @model.get('card_id')?
						return '•••'
				onSet: (val) ->
					@model.set('card_id', null)
					return val

			'[name="coupon"]':
				observe: 'coupon.name'
				onSet: (val) ->
					if val?.length
						@$el.$couponSubmit.removeAttr 'disabled'
					else
						@$el.$couponSubmit.attr 'disabled', true
					return val

			'[data-coupon-form]':
				classes:
					'm-hide':
						observe: 'coupon.amount'
						onGet: (val) ->
							return val?

			'[data-coupon-set]':
				classes:
					'm-hide':
						observe: 'coupon.amount'
						onGet: (val) ->
							return not val?

			'[data-coupon-name]': 'coupon.name'

	addListeners: ->
		@listenTo @model, 'change:payment_type', @togglePaymentForm

	bind: ->
		@$el.$cardForm.find('input').on('click', () ->
			$(@).select()
		)

	render: ->
		@$el.html MembershipSignup.Templates.Payment
		@stickit()
		@cacheDom()
		@bind()
		@initCardCheck()
		@

	tryExistingCard: ->
		@render()
		@showLoading()
		$.when(
			@model.getCards()
		).then(
			() => @hideLoading()
		)

	initCardCheck: ->
		@$el.$cardNumber.payment('formatCardNumber')
		@$el.$cardCvc.payment('formatCardCVC')
		@$el.$cardExp.payment('formatCardExpiry')

		# don't allow numbers in card holders name
		@$el.$cardName.on 'keypress', (e) ->
			return false if e.charCode > 47 and e.charCode < 58

	togglePaymentForm: (model) ->
		switch model.get('payment_type')
			when 'cc'
				@showCardForm()
			when 'paypal'
				@hideCardForm()

	showCardForm: -> @$el.$cardForm.removeClass('m-hide')
	hideCardForm: -> @$el.$cardForm.addClass('m-hide')

	showCouponLoading: -> @$el.$couponLoading.removeClass('m-hide')
	hideCouponLoading: ->
		@$el.$couponLoading.addClass('m-hide')
		$(window).trigger('resize')
	showCouponForm: ->
		@$el.$couponFields.removeClass('m-hide')
		$(window).trigger('resize')
	showLoading: -> @$el.$loading.removeClass('m-hide')
	hideLoading: -> @$el.$loading.addClass('m-hide')

	addCoupon: () ->
		coupon_name = @$el.$couponInput.val()
		@showCouponLoading()

		$.when( @model.addCoupon(coupon_name) )
		.then(
			(res) =>
				if res?.data?
					@applyCoupon(res.data)
					@hideCouponLoading()

			, (err) =>
				message = 'An error occurred. Please reload the page and try again.'
				if err.responseJSON?.errors?.coupon?
					if err.responseJSON.errors.coupon.messages[0] == 'Coupon is not exists'
						message = 'Coupon code <b>' + coupon_name + '</b> is not valid.'
					else
						message = err.responseJSON.errors.coupon.messages[0]
				else if err.responseJSON?.errors?.error?
					message = err.responseJSON.errors.error.messages[0]

				vexDialog.buttons.YES.text = 'Ok'
				vexDialog.alert
					message: message

				@hideCouponLoading()
			)

	applyCoupon: (data) ->
		@model.set 'coupon.amount', data.amount
		@model.set 'coupon.type', data.amountType
		@model.set 'coupon.id', data.id

module.exports = View

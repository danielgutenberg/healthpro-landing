Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class Collection extends Backbone.Collection

	initialize: (models, options) ->
		@taxonomySlug = options.taxonomySlug

	loadOptions: ->
		$.ajax
			method: 'get'
			url: getApiRoute 'ajax-taxonomy-tags', taxonomySlug: @taxonomySlug
			success: (response) =>
				_.each response.data, (item) => @push item
				@trigger 'data:ready'

module.exports = Collection

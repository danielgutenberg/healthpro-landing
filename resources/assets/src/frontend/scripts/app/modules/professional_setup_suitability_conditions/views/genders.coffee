Backbone = require 'backbone'

class View extends Backbone.View

	tagName: 'li'
	className: 'accordion_panes--item'

	initialize: (@options) ->
		super

		@model = @options.model
		@$container = @options.$container

		@taxonomySlug = @options.taxonomySlug
		@inputName = @options.inputName
		@groupName = @options.groupName
		@collection = @options.collection

		@collection.loadOptions()

		@cacheDom()
		@bind()

	cacheDom: ->
		@$contentContainer = $('[data-options-content]', @$el)

	bind: ->
		@listenTo @, 'options:change:value', (id, checked, target) ->
			id = parseInt id
			arr = [id]

			@model.set @inputName, _.uniq(arr)

			@appendSelected(id, target)

		@listenTo @collection, 'data:ready', ->
			@setSelected()
			@render @collection.toJSON()

		@listenTo @, 'rendered', ->
			@$el.find('input[type="radio"]').val @model.get(@inputName)

	setSelected: ->
		# set new "selected" value to options
		arr = @model.get @inputName
		arr.forEach (id) =>
			model = @collection.findWhere {id: id}
			model.set 'selected', true if model

	appendSelected: (id, target) ->
		model = @collection.findWhere {id: id}
		return unless model

		$container = $(target).parents('.accordion_panes--item').find('.accordion_panes--item--opener--selected')
		$('span', $container).remove()
		$container.append '<span data-id="' + id + '">' + model.get('name') + '</span>'

		@

	render: (data) ->
		@$el.html ProfessionalSetupSuitabilityConditions.Templates.Genders
			options: data
			input_name: @inputName
			group_name: @groupName

		@$el.appendTo @$container

		@$el.find('[type="radio"]').on 'change', (e) =>
			@trigger 'options:change:value', e.target.value, e.target.checked, e.target

		@trigger 'rendered'

module.exports = View

Backbone = require 'backbone'
getApiRoute = require 'hp.api'

parseTagArray = require 'utils/parse_tag_array'

class Model extends Backbone.Model

	fetched: false

	initialize: (options) ->
		@groups = options.groups
		@setPresetData options.presetData if options.presetData

	setPresetData: (data) ->
		_.each @getGroupsConfig(), (group) =>
			@set group.input, data[group.input]
		@fetched = true
		@

	fetch: ->
		if @fetched
			@trigger 'data:ready'
			return

		@trigger 'ajax:loading'
		$.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'get'
			success: (response) =>
				_.each @getGroupsConfig(), (group) =>
					@set group.input, parseTagArray response.data.tags[group.input].tags

				@trigger 'data:ready'

	save: ->
		@trigger 'ajax:loading'

		data = @getSaveData()
		data._token = window.GLOBALS._TOKEN

		$.ajax
			url: getApiRoute('ajax-profiles-self')
			method: 'put'
			type: 'json'
			contentType: 'application/json; charset=utf-8'
			data: JSON.stringify data
			success: =>
				@trigger 'ajax:success'
				@trigger 'data:saved'
				
			error: => @trigger 'ajax:error'

	getSaveData: ->
		data =
			tags: {}

		_.each @getGroupsConfig(), (group) =>
			data.tags[group.input] = @get group.input

		data

	getGroupsConfig: ->
		if _.isArray @groups
			return @groups.map (groupSlug) => _.findWhere ProfessionalSetupSuitabilityConditions.Config.Groups, {slug: groupSlug}
		return _.clone ProfessionalSetupSuitabilityConditions.Config.Groups

module.exports = Model

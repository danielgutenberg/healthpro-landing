Backbone = require 'backbone'

class View extends Backbone.View

	data:
		optionsSorted: []

	initialize: (@options) ->
		super

		@$container = @options.$container
		@taxonomySlug = @options.taxonomySlug
		@model = @options.model
		@collection = @options.collection

		@inputName = @options.inputName

		@collection.loadOptions()

		@bind()

	bind: ->
		@listenTo @collection, 'data:ready', =>
			@sortData()

		@listenTo @, 'data:sorted', =>
			@setSelected()
			@render()

		@listenTo @, 'options:change:value', (id, checked, target) ->
			id = +id
			# cache array
			arr = @model.get @inputName

			# array exists
			if arr
				# append new value
				if checked
					arr.push id
					@appendSelected(id, target)

				# remove unchecked value
				else
					arr = _.without arr, id
					@removeUnchecked(id, target)

			# the array doesn't exist. push only if the checkbox is checked
			else if checked
				arr = [id]

			@model.set @inputName, _.uniq(arr)
			@model.trigger 'change' # sometimes change event doesn't fire

		@listenTo @, 'rendered', ->
			@$el.find('.checkbox input[type="checkbox"]').val @model.get(@inputName)

	appendSelected: (id, target) ->
		_.forEach @data.optionsSorted, (value, key) ->
			option =
			_.find value.children, (chr) ->
				chr.id == id
			if option
				$(target).parents('.accordion_panes--item').find('.accordion_panes--item--opener--selected').append '<span data-id="' + id + '">' + option.name + '</span>'

	removeUnchecked: (id, target) ->
		$(target).parents('.accordion_panes--item').find('[data-id="' + id + '"]').remove()

	setSelected: ->
		# set new "selected" value to optionsSorted
		arr = @model.get @inputName
		arr.forEach (id) =>
			_.forEach @data.optionsSorted, (value, key) ->
				@option =
				_.find value.children, (chr) ->
					chr.id == id
				if @option
					@option.selected = true

	render: () ->
		@$el.html ProfessionalSetupSuitabilityConditions.Templates.Conditions
			options: @data.optionsSorted
			input_name: @inputName

		@$el.appendTo @$container

		@$el.find('.checkbox input[type="checkbox"]').on 'change', (e) =>
			@trigger 'options:change:value', e.target.value, e.target.checked, e.target
		@trigger 'rendered'

	sortData: () ->
		result = {}

		options = @collection.toJSON()
		parents = _.where options, parent_id: null

		_.each parents, (parent) =>
			result[parent.id] = _.extend parent, children: _.where options, parent_id: parent.id
			# no children - add same object as a child
			unless result[parent.id].children.length
				result[parent.id].children = [parent]

		@data.optionsSorted = result

		@trigger 'data:sorted'

module.exports = View

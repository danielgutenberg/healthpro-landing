Backbone = require 'backbone'
Handlebars = require 'handlebars'
Device = require 'utils/device'

require '../../../../styles/modules/professional_suitability_conditions.styl'

window.ProfessionalSetupSuitabilityConditions =
	Config:
		Groups: [
			{
				slug: 'concerns-conditions'
				input: 'concerns_conditions'
				view: 'Conditions'
				name: 'Conditions and Concerns'
				container: 'concerns-conditions'
			}
			{
				slug: 'age-group'
				input: 'age_group'
				view: 'Options'
				name: 'Age Group'
				container: 'options-container'
			}
			{
				slug: 'gender-identities'
				input: 'gender_identities'
				view: 'Genders'
				name: 'Gender Identities Served'
				container: 'options-container'
			}
			{
				slug: 'special-needs'
				input: 'special_needs'
				view: 'Options'
				name: 'Specials Needs'
				container: 'options-container'
			}
		]
	Views:
		Base: require('./views/base')
		Options: require('./views/options')
		Conditions: require('./views/conditions')
		Genders: require('./views/genders')
	Templates:
		Base: Handlebars.compile require('text!./templates/partial.html')
		Options: Handlebars.compile require('text!./templates/options.html')
		Conditions: Handlebars.compile require('text!./templates/conditions.html')
		Genders: Handlebars.compile require('text!./templates/genders.html')
	Collections:
		Options: require('./collections/options')
	Models:
		Base: require('./models/base')

# extend module with events
_.extend ProfessionalSetupSuitabilityConditions, Backbone.Events

class ProfessionalSetupSuitabilityConditions.App
	constructor: (options) ->
		type = options.type ? 'partial'

		# update template
		if type is 'page'
			ProfessionalSetupSuitabilityConditions.Templates.Base = Handlebars.compile do ->
				if Device.isMobile()
					require('text!./templates/mobile/base.html')
				else
					require('text!./templates/desktop/base.html')

		@model = new ProfessionalSetupSuitabilityConditions.Models.Base
			presetData	: options.presetData
			groups		: options.groups
			type		: type

		@view = new ProfessionalSetupSuitabilityConditions.Views.Base
			el				: options.el
			displayFooter	: options.displayFooter ? true
			displayLoading	: options.displayLoading ? true
			dirtyForms		: options.dirtyForms ? true
			type 			: type
			model			: @model

		return {
			model: @model
			view: @view
			close: @close
		}

	close: ->
		@view?.close?()
		@view?.remove?()

if $('.js-suitability_conditions').length
	new ProfessionalSetupSuitabilityConditions.App
		el			: '.js-suitability_conditions'
		groups		: 'all'
		type		: 'page'


module.exports = ProfessionalSetupSuitabilityConditions

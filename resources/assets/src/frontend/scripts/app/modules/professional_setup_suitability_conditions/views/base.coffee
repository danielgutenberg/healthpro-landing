Backbone = require 'backbone'
dirtyForms = require 'framework/dirty_forms'
Accordion = require 'framework/accordion'
Device = require 'utils/device'
Tabs = require 'framework/tabs'

class View extends Backbone.View

	events:
		'click .professional_setup_suitability_conditions--btn': 'save'

	initialize: (options) ->
		super
		@model = options.model
		@displayFooter = options.displayFooter
		@displayLoading = options.displayLoading
		@dirtyForms = options.dirtyForms

		@groups = @model.getGroupsConfig()

		@collections = {}
		@instances = {}

		@render()
		@cacheDom()
		@initAccordion()
		@bind()

		@model.fetch()

	bind: ->
		@listenTo @model, 'data:ready', =>
			@initOptionsCollections()
			@initGroups()
			@loadOptionsCollections()

		@listenTo @model, 'ajax:loading', => @showLoading()
		@listenTo @model, 'ajax:success ajax:error', => @hideLoading()
		@listenTo @, 'parts:inited', => @initDirtyForms()

		@listenTo @, 'data:loaded', => @hideLoading()


	initGroups: ->
		_.each @groups, (group) =>
			$container = $("[data-#{group.slug}]", @$el)
			$container = @$el.$optionsContainer unless $container.length

			@instances[group.slug] = new ProfessionalSetupSuitabilityConditions.Views[group.view]
				$container: $container
				model: @model
				taxonomySlug: group.slug
				inputName: group.input
				groupName: group.name
				collection: @collections[group.slug]

		if Device.isMobile()
			new Tabs @$el.find('[data-tabs]')

		@trigger 'parts:inited'

	initOptionsCollections: ->
		_.each @groups, (group) =>
			@collections[group.slug] = new ProfessionalSetupSuitabilityConditions.Collections.Options 0, taxonomySlug: group.slug

	loadOptionsCollections: ->

		unloadedCollections = _.size @collections

		_.forEach @collections, (collection) =>
			@listenToOnce collection, 'data:ready', =>
				unloadedCollections--
				@trigger 'data:loaded' unless unloadedCollections

	initDirtyForms: ->
		return unless @dirtyForms
		model = dirtyForms.addForm( {$el: @$el} )

		_.each @instances, (instance) =>
			@listenToOnce instance, 'rendered', ->
				instance.$('.checkbox input, .radio input').on 'change', => model.set('isDirty', true)

		@$('.checkbox input, .radio input').on 'change', => model.set('isDirty', true)

		@listenTo @model, 'ajax:success', -> model.set('isDirty', false)


	initAccordion: ->
		@accordion = new Accordion @$el

	render: ->
		containers = _.uniq _.pluck @groups, 'container'
		@$el.html ProfessionalSetupSuitabilityConditions.Templates.Base
			displayOptionsContainer: _.indexOf(containers, 'options-container') > -1
			displayConditionsContainer: _.indexOf(containers, 'concerns-conditions') > -1

		@

	cacheDom: ->
		@$el.$optionsContainer = $('[data-options-container]', @$el)
		@$el.$loading = $('.loading_overlay', @$el)
		@

	save: -> @model.save()

	showLoading: -> @$el.$loading.removeClass('m-hide')

	hideLoading: -> @$el.$loading.addClass('m-hide')

module.exports = View

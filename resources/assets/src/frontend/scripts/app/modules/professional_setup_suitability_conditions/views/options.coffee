Backbone = require 'backbone'
getApiRoute = require 'hp.api'

class View extends Backbone.View

	tagName: 'li'
	className: 'accordion_panes--item'

	initialize: (@options) ->
		super

		@model = @options.model
		@$container = @options.$container

		@taxonomySlug = @options.taxonomySlug
		@inputName = @options.inputName
		@groupName = @options.groupName

		@collection = @options.collection

		@collection.loadOptions()

		@cacheDom()
		@bind()

	cacheDom: ->
		@$contentContainer = $('[data-options-content]', @$el)

	bind: ->
		@listenTo @collection, 'data:ready', ->
			@setSelected()
			@render()

		@listenTo @, 'options:change:value', (id, checked, target) ->
			id = +id
			# cache array
			arr = @model.get(@inputName)
			# array exists
			if arr
				# append new value
				if checked
					arr.push id
					@appendSelected(id, target)

				# remove unchecked value
				else
					arr = _.without arr, id
					@removeUnchecked(id, target)

			# the array doesn't exist. push only if the checkbox is checked
			else if checked
				arr = [id]

			@model.set @inputName, _.uniq(arr)
			@model.trigger 'change' # sometimes change event doesn't fire
			@

		@listenTo @, 'rendered', ->
			@$el.find('.checkbox input[type="checkbox"]').val @model.get(@inputName)

	setSelected: ->
		# set new "selected" value to options
		arr = @model.get @inputName
		arr.forEach (id) =>
			model = @collection.findWhere {id: id}
			model.set 'selected', true if model

	appendSelected: (id, target) ->
		model = @collection.findWhere {id: id}
		return unless model
		$(target).parents('.accordion_panes--item').find('.accordion_panes--item--opener--selected').append '<span data-id="' + id + '">' + model.get('name') + '</span>'

	removeUnchecked: (id, target) ->
		$(target).parents('.accordion_panes--item').find('[data-id="' + id + '"]').remove()

	render: ->
		@$el.html ProfessionalSetupSuitabilityConditions.Templates.Options
			options: @collection.toJSON()
			input_name: @inputName
			group_name: @groupName

		@$el.appendTo @$container

		@$el.find('.checkbox [type="checkbox"]').on 'change', (e) =>
			@trigger 'options:change:value', e.target.value, e.target.checked, e.target

		@trigger 'rendered'

	getOptions: ->
		$.ajax
			method: 'get'
			url: getApiRoute 'ajax-taxonomy-tags', taxonomySlug: @taxonomySlug
			success: (response) =>
				@data.options = response.data
				@trigger 'data:ready'

module.exports = View

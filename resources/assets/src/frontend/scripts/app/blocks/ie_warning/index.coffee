vexDialog = require 'vexDialog'

class IeWarning
	constructor: (@$block) ->
		@init()

	init: ->
		version = @detectVersion()
		if version and version < 12
			@showWarning()

	detectVersion: ->
		ua = window.navigator.userAgent
		msie = ua.indexOf('MSIE ')

		if msie > 0
			# IE 10 or older => return version number
			return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10)
		trident = ua.indexOf('Trident/')

		if trident > 0
			# IE 11 => return version number
			rv = ua.indexOf('rv:')
			return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10)
		edge = ua.indexOf('Edge/')

		if edge > 0
			# Edge (IE 12+) => return version number
			return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10)

		# other browser
		false

	showWarning: ->
		vexDialog.buttons.YES.text = 'Ok'
		vexDialog.alert
			message: "This browser is not supported, we recommend you switch to Chrome or Firefox."

new IeWarning

module.exports = IeWarning

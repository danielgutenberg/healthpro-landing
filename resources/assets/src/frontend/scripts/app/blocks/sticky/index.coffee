class Sticky
	constructor: (@$block) ->
		@cacheDom()
		@cacheSizes()
		@init()

	cacheDom: ->
		@$window = $(window)
		@$layoutHeader = $('.layout--header')
		@$articleTitle = $('.data-post-title')
		@$opener = $('[data-sticky-opener]', @$block)
		@$container = @$block.parents('[data-sticky-container]')

	cacheSizes: ->
		@$layoutHeaderHeight = if window.matchMedia("(min-width: 980px)").matches then @$layoutHeader.height() else 0

		@$articleTitleHeight = @$articleTitle.height()
		@$articleTitleStart = @$articleTitle.offset().top
		@$articleTitleEnd = @$articleTitleStart + @$articleTitleHeight

		@$containerHeight = @$container.height()
		@$containerStart = @$container.offset().top
		@$containerEnd = @$containerStart + @$containerHeight

		@$blockHeight = @$block.outerHeight(true)

		@$maxTopValue = @$containerEnd - @$containerStart - @$blockHeight + @$layoutHeaderHeight

	init: ->
		@stickBlock()
		@toggleOpener()
		@$window.on 'scroll', =>
			@stickBlock()
			@toggleOpener()

		@$window.on 'resize', =>
			@cacheSizes()

		@$opener.on 'click', =>
			@toggleBlock()

	stickBlock: ->
		$scrollTop = @$window.scrollTop()
		if $scrollTop > @$containerStart - @$layoutHeaderHeight

			$calcTop = $scrollTop - @$containerStart + @$layoutHeaderHeight
			$topValue = if $calcTop > @$maxTopValue then @$maxTopValue else $calcTop

			@$block.css
				top: $topValue
				bottom: 'auto'
		else
			@$block.css
				top: 0

	toggleBlock: ->
		@$block.toggleClass 'm-opened'

	toggleOpener: ->
		$scrollTop = @$window.scrollTop()
		if $scrollTop > @$articleTitleEnd
			@$opener.addClass 'm-show'
		else
			@$opener.removeClass 'm-show'

$('[data-sticky-block]').each -> new Sticky $(this)

module.exports = Sticky

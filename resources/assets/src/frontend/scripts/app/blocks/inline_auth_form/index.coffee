Form = require 'framework/form'
getApiRoute = require 'hp.api'
vexDialog = require 'vexDialog'
capterraTrack = require 'utils/capterra_tracker'

class AuthForm

	profile: null
	containerSlug: null
	callback: null

	constructor: (@$block) ->

		@$block.$containers = @$block.find('[data-auth-container]')
		@$block.$sidebar = @$block.find('[data-auth-sidebar]')
		@$block.$profileType = @$block.find('[name="profile_type"]')
		@$block.$switch = @$block.find('[data-switch]')
		@$block.$socials = @$block.find('.btn_social')

		@$block.$socials = @$block.find('.btn_social')

		@initSignUpForm()
		@bind()

	bind: ->

	acceptTerms: (link) ->
		termsLink = window.location.origin + '/terms'

		vexDialog.open
			unsafeMessage: 'Please accept our terms and conditions to continue.'
			input: """
				<input class="vex-custom-checkbox" type="checkbox">
					I agree to HealthPRO's <a href="#{ termsLink }" target="_blank">Terms of Service</a>
				</input>
			"""
			afterOpen: ($vexContent) ->

				$btn = $('.vex-dialog-button-primary')
				$check = $('.vex-custom-checkbox')

				$btn.attr("disabled","disabled")
				$btn.css("background","#e0e0e0")
				$check.change ->
					if $(this).is(':checked')
						$btn.removeAttr("disabled")
						$btn.css("background","#2b95a7")
					else
						$btn.attr("disabled","disabled")
						$btn.css("background","#e0e0e0")

			callback: (value) =>
				unless $('.vex-custom-checkbox').is(':checked')
					return false
				window.location.href = link


	initSignUpForm: ->
		@signupForm = new Form @$block,
			ajax: true
			url: getApiRoute('ajax-auth-sign-up')
			onSuccess: (response) =>
				if _.isFunction @callback
					@callback()
					@closePopup()
				else
					window.location.href = response.data.redirect_to ? @getLocationRoot()
				capterraTrack()

	setCallback: (@callback) ->

	setActiveSwitch: ->
		switch @containerSlug
			when 'sign', 'sign_up'
				sw = 'login'
			else
				sw = 'sign'

		@$block.$switch.removeClass('m-show').filter("[data-switch='#{sw}']").addClass('m-show')

	clearFormsErrors: ->
		@loginForm.unsetErrors() if @loginForm
		@signupForm.unsetErrors() if @signupForm

	socialLogin: (url) ->
		link = "#{url}?profile=#{@profile}"
		if @containerSlug is 'sign'
			link += '&login=false'
		else
			link += '&login=true'
		if @containerSlug is 'sign' and @$block.find('.auth_form--terms').is(':checked') is false
			@acceptTerms link
			return

		window.location.href = link

$('.inline_auth_form').each -> new AuthForm $(this)

module.exports = AuthForm

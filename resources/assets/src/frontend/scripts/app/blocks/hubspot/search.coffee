class SearchHubspot

	constructor: (@$block) ->
		return unless @$block.length

		@$block.$toggler = @$block.find('.search_results--know_professional_toggler')

		@bind()
		@initForm()

	bind: ->
		@$block.$toggler.on 'click', (e) =>
			e.preventDefault()
			@toggleForm()


	initForm: ->
		require.ensure [], =>
			Hubspot = require 'framework/hubspot'
			Hubspot.initForm
				sfdcCampaignId: '70124000000LkCMAA0'
				portalId: '496304'
				formId: 'eaf408c2-22d6-45a6-9a83-984b78e83a5f'
				target: '#search_know_professional_form'
				submitButtonClass: 'btn m-green'
				onFormSubmit: =>
					@$block.$toggler.remove()

	toggleForm: ->
		@$block.toggleClass 'm-opened'

new SearchHubspot $('#search_know_professional')

module.exports = SearchHubspot

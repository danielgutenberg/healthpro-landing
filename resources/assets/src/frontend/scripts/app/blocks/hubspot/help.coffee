class HelpHubspot

	constructor: (@$block) ->
		return unless @$block.length
		@initForm()

	initForm: ->
		require.ensure [], =>
			Hubspot = require 'framework/hubspot'
			Hubspot.initForm
				sfdcCampaignId: '70124000000LkCMAA0'
				portalId: '496304'
				formId: 'ba38a388-e620-4bfd-baba-9db3dc98423c'
				target: '#help_form'
				submitButtonClass: 'btn m-green'
				inlineMessage: 'Thank you for submitting your question'

new HelpHubspot $('#help_form')

module.exports = HelpHubspot

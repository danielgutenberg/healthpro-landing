class Feedback
	constructor: ->
		require.ensure [], ->
			require 'framework/hubspot'
			$s.ready 'hp.hubspot', =>
				# init feedback form
				feedbackContainer = $('.feedback-container')
				if feedbackContainer.length
					feedbackContainer.bind 'toggle-form', (e, show) ->
						container = $(e.target)
						show = if typeof show == 'undefined' then !$('.feedback-form', container).is(':visible') else show
						if show
							$('.feedback-form', container).fadeIn()
							$('.feedback-button', container).fadeOut()
							if !$('.hs-form', container).length
								hbspt.forms.create
									css: ''
									cssRequired: ''
									portalId: '496304'
									formId: '213057d8-8ff9-4971-9a09-9fc5e620994e'
									target: '.feedback-container .feedback-target'
									submitButtonClass: 'btn m-green'
									onFormReady: ($form, ctx) ->
										$('textarea', $form).val ''
										$('input[name=page_source]', $form).val $.grep(window.location.pathname.split('/'), (n) ->
											n
										).join(' - ')
									onFormSubmit: (e, hhh) ->
										setTimeout (->
											container.trigger 'toggle-form', false
											$('.feedback-target', container).html ''
											return
										), 2000
										return
						else
							$('.feedback-form', container).fadeOut()
							$('.feedback-button', container).fadeIn()

					feedbackContainer.find('button.feedback-button,a.feedback-close').on 'click', (e) ->
						e.preventDefault()
						$(e.target).parents('.feedback-container').first().trigger 'toggle-form'
						return

new Feedback()

module.exports = Feedback

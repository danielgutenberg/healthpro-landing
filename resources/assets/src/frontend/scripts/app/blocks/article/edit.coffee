class ArticleEdit
	constructor: (@$block) ->

		@cacheDom()

		# append icons styles
		$("head").append("<link rel='stylesheet' type='text/css' href='//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css'>")

		require.ensure [], =>

			Handlebars   = require 'handlebars.runtime'
			MediumEditor = require 'medium-editor'

			# make these packages global
			window.Handlebars = Handlebars.default
			window.MediumEditor = MediumEditor

			require('imports?define=>false!medium-editor-insert-plugin')($)
			require 'datetimepicker'
			require 'select2'

			# require styles
			require 'medium-editor/dist/css/medium-editor.min.css'
			require 'medium-editor/dist/css/themes/default.min.css'
			require 'libs/medium-editor-insert/medium-editor-insert-plugin.css'
			require 'jquery-datetimepicker/jquery.datetimepicker.css'

			@bind()
			@fixParagraphs()
			@initEditor()


			$('input[name="publish_at"]').datetimepicker
				format:'Y-m-d H:i'

			$('#taxonomy-post-categories-input').select2
				maximumSelectionLength: 1

	cacheDom: ->
		@$block.$editable = @$block.find('.editable')
		@$block.$title = $('.data-post-title')
		@$block.$submit = $('.article_edit--submit')
		@$block.$cover = $('input[name="cover_image"]')

	bind: ->
		@$block.$cover.on 'change', (event) =>
			@readURL(event.target)

		@$block.$submit.on 'click', =>
			@setUpElements()


	initEditor: ->

		@editor = new MediumEditor @$block.$editable.get(0),
			'toolbar':
				'buttons': ['bold', 'italic', 'underline', 'anchor', 'h1', 'h2', 'h3', 'quote', 'unorderedlist','orderedlist']
			'disablePlaceholders': false
			'firstHeader': 'h2'
			'secondHeader': 'h3'
			'targetBlank': true

		setTimeout(
			=>
				@initMediumInsert()
			, 500
		)

		return

	initMediumInsert: ->
		@$block.$editable.mediumInsert
			editor: @editor
			enabled: true
			addons:
				images:
					label: '<span class="fa fa-camera"></span>',
					uploadScript: @$block.find('.article_edit').data('image-url')
					preview: true
					autoGrid: 3
					styles:
						left: false
						right: false
					actions:
						remove:
							label: '<span class="fa fa-times"></span>'
							clicked: ($el) ->
								$event = $.Event('keydown')
								$event.which = 8
								$(document).trigger($event)
				embeds:
					label: '<span class="fa fa-youtube-play"></span>'
					placeholder: 'Paste a YouTube, Vimeo, Facebook, Twitter or Instagram link and press Enter'
					captionPlaceholder: 'Type caption (optional)'
					oembedProxy: 'https://iframe.ly/api/oembed?iframe=1&api_key=159f475fad8bd6d081748c'
					actions:
						remove:
							label: '<span class="fa fa-times"></span>'
							clicked: ($el) ->
								$event = $.Event('keydown')

								$event.which = 8
								$(document).trigger($event)
		return

	readURL: (input) ->
		if input.files and input.files[0]
			reader = new FileReader

			reader.onload = (e) ->
				$('#cover_image_preview').attr('src', e.target.result).attr('style', '').height 200
				return

			reader.readAsDataURL input.files[0]
			return

	setHiddenElement: (className, outputClassName) ->
		from = document.getElementsByClassName(className)
		to = document.getElementsByClassName(outputClassName)
		to[0].value = from[0].innerHTML
		return

	setUpElements: ->
		@setHiddenElement 'article--title', 'title-hidden'
		postContent = @editor.serialize()['a']['value']
		postContent = postContent.replace(/<br>/g, '').replace(/<p[^>]>[\s]*<\/p>/, '').replace(/<div[^>]>[\s]*<\/div>/, '')
		$('.content-hidden').val postContent
		return

	fixParagraphs: ->
		@$block.$editable.find('p:empty').append '<br>'




$('.js-article_edit').each -> new ArticleEdit $(this)

module.exports = ArticleEdit

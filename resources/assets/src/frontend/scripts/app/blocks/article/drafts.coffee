class BlogDrafts
	constructor: (@$node) ->
		@$node.link = $('[data-id]', @$node)
		@bind()

	bind: ->
		@$node.link.on 'click', (e) =>
			id = $(e.target).data('id')
			@navigateToDraft(e.target, id)

	navigateToDraft: (link, id) ->
		val = $('#' + id + ' option:selected').val()
		link.href = val

$('.js-blog_drafts').each -> new BlogDrafts $(this)

module.exports = BlogDrafts

require 'socialLikes'
require 'social-likes/dist/social-likes_birman.css'
Hubspot = require 'framework/hubspot'

class Article
	constructor: (@$node) ->
		@cacheDom()
		@initShare()

		@initHubspot() unless @editing()

	cacheDom: ->
		@$node.$share = @$node.find('[data-article-el="share"]')
		@$node.$cta = @$node.find('[data-hubspot-cta]')


	initShare: ->
		@$node.$share.socialLikes()

	initHubspot: ->

		@$node.$cta.each (k, cta) => @initHubspotCta $(cta)


	initHubspotCta: ($cta) ->
		options = $cta.data('hubspot-cta-options')
		return unless options and options.portalId and options.ctaId

		Hubspot.initCta
			portalId: options.portalId
			ctaId: options.ctaId
			target: $cta
			replace: true



#		Hubspot.initForm
#	sfdcCampaignId: '70124000000LkCMAA0'
#	portalId: '496304'
#	formId: '774dd88f-578d-4742-a86c-8225c5a839d3'
#	target: '#footer_newsletter'
#	submitButtonClass: 'btn'



	editing: -> @$node.hasClass 'm-editable'

$('.article').each -> new Article $(this)

module.exports = Article

class Careers
	constructor: (@$block) ->
		require.ensure [], =>
			require 'owlCarousel'

			@$block.gallery = $('[data-node="gallery"]', @$block)
			@$block.gallery.owlCarousel
				margin: 20

$('[data-careers]').each -> new Careers $(this)

module.exports = Careers

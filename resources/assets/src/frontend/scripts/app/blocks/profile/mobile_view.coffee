class ProfileMobile
	constructor: (@$block) ->

		@$block.footer = $('[data-profile-footer]')

		return unless @$block.footer.length

		@$block.footerOffsetTop = @$block.footer.offset().top
		@$block.footerHeight = @$block.footer.height()

		@$block.widgetOpener = $('[data-booking-opener]')
		@$block.bookWidget = $('[data-profile-book]', @$block)
		@$block.bookWidgetHeight = @$block.bookWidget.height()

		@$window = $(window)
		@$windowHeight = @$window.height()

		@bindings()
		@stickFooter()

	bindings: ->
		@$window.on
			'resize': =>
				@resetSizes()
				@stickFooter()
				@adjustPadding()

			'scroll': =>
				@stickFooter()

		@$block.widgetOpener.on 'click', (e) =>
			@toggleSidebar()
			@toggleOpener()

			return false

		$(document).on 'bookAppointment:schedule:loading bookAppointment:schedule:ready', (e) => @adjustPadding()

		$(document).on 'bookAppointment:close', (e) =>
			@toggleSidebar()
			@toggleOpener()

	resetSizes: ->
		@$block.footer.removeClass 'm-fixed' # unstick the block to update offset value

		@$block.footerOffsetTop = @$block.footer.offset().top
		@$block.footerHeight = @$block.footer.height()
		@$windowHeight = @$window.height()

		@$block.footer.addClass 'm-fixed' # stick the block back

		@$block.bookWidgetHeight = @$block.bookWidget.height()

	stickFooter: ->
		if window.pageYOffset + @$windowHeight >= @$block.footerOffsetTop + @$block.footerHeight
			@$block.footer.removeClass 'm-fixed'

		else
			@$block.footer.addClass 'm-fixed'

	toggleSidebar: ->
		@$block.bookWidget.toggleClass 'm-opened'
		@adjustPadding()

	toggleOpener: ->
		@$block.widgetOpener.toggleClass 'm-hide'

	adjustPadding: ->
		if @$block.bookWidget.hasClass 'm-opened'
			@$block.css
				'padding-top': @$block.bookWidget.height()
		else
			@$block.css
				'padding-top': 0

		if window.matchMedia('(min-width: 750px)').matches
			@$block.css
				'padding-top': 0

$('[data-profile]').each -> new ProfileMobile $(this)

module.exports = ProfileMobile

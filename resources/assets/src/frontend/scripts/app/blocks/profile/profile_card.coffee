WizardAuth = require 'helpers/wizard_auth'
require 'tooltipster'

class ProfileCard
	constructor: (@$block) ->
		@cacheDom()
		@bind()
		@initGiftCertificateBtnTooltip()

	cacheDom: ->
		@$block.$ratings = @$block.find('.profile_card--ratings')
		@$block.$giftCertificateBtn = @$block.find('[data-profile-purchase-gift-certificate]')

	bind: ->
		@$block.$ratings.click -> $('html, body').animate { scrollTop: $('.reviews').offset().top - 20 }, 1000
		@$block.$giftCertificateBtn.on 'click', => @openGiftCertificateWizard()

	initGiftCertificateBtnTooltip: ->
		@$block.$giftCertificateBtn.tooltipster
			theme: 'tooltipster-dark'
			maxWidth: 160

	openGiftCertificateWizard: ->
		openWizard = ->
			window.bookingHelper.openWizardGiftCertificate
				bookingData:
					provider_id: +window.GLOBALS.PROVIDER_ID
					client_id: +window.GLOBALS._PID
					resetToFirstStep: 'recalculate'
		WizardAuth openWizard,
			msg: 'Please log in to purchase a gift certificate'

$('[data-profile-card]').each -> new ProfileCard $(this)

module.exports = ProfileCard

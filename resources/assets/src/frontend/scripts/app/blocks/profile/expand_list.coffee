class ExpandList
	constructor: (@$block) ->
		@$block.list = $('ul', @$block)
		@$block.list_opener = $('.conditions--list--opener', @$block)

		@hideExceeds()
		@showItems()

	hideExceeds: ->

		@$block.list.each (k, list) =>
			$list = $(list)
			@$list_items = $list.find 'li'
			@$list_opener = $list.parent().find('.conditions--list--opener')

			if @$list_items.length > 4
				@$list_opener.addClass 'm-show'

	showItems: ->
		@$block.list_opener.on 'click', (e) =>
			e.preventDefault()
			@$block.list.addClass 'm-show_items'
			$(e.currentTarget).removeClass 'm-show'

$('[data-expand-list]').each -> new ExpandList $(this)

module.exports = ExpandList

getApiRoute = require 'hp.api'
parseHash = require 'utils/parse_hash'
isNumeric = require 'utils/is_numeric'

class ReviewPublicProfile
	constructor: (@$block) ->
		require.ensure [], =>
			require 'app/modules/review_professional'

			@bind()
			@maybeInitReview()

	bind: () ->
		@$block.on 'click', (e) =>
			if $('[data-profile]').length # prevent only on profile page
				e?.preventDefault()
				@reviewProfessional()

	maybeInitReview: ->
		hash = parseHash true
		if hash.hasOwnProperty('review')
			@reviewProfessional()

	reviewProfessional: () ->

		#close another review if it is open for some reason
		@review.close?() if @review

		defaults =
			profileId: window.GLOBALS.PROVIDER_ID
			professionalDetails: null
			showCloseButton: true
			redirectUrl: window.location.origin + window.location.pathname
			isPopup: true

		@review = new ReviewProfessional.App defaults


if $('[data-review-professional]').length
	new ReviewPublicProfile $('[data-review-professional]')

module.exports = ReviewPublicProfile

class About
	constructor: (@$block) ->

		require.ensure [], =>
			require 'owlCarousel'
			require 'magnificPopup'

			@$block.gallery = $('[data-about-gallery]', @$block)
			return unless @$block.gallery.length

			@$block.$galleryWrap = $('.about--gallery', @$block)
			@$block.gallery.thumb = $('[data-about-gallery-thumb]', @$block)
			@$block.video_thumb = $('[data-about-video-thumb]', @$block)
			@$block.video_thumb.img = $('img', @$block.video_thumb)

			@initLightbox()
			@preparePreview()

	initCarousel: ->
		@$block.gallery.owlCarousel
			items: 3
			autoWidth: false
			margin: 10
			nav: true
			dots: false
			loop: false
			navRewind: false
			video: true
			responsive:
				0:
					items: 1
				480:
					items: 2
				640:
					items: 3
				750:
					items: 1
				860:
					items: 2
				1080:
					items: 3
			onInitialized: =>
				@$block.$galleryWrap.removeClass('m-loading');
				@$block.gallery.thumb.height( @$block.gallery.height() )

	initLightbox: ->
		if @$block.gallery.thumb.length
			@$block.gallery.thumb.magnificPopup
				gallery:
					enabled: true

	preparePreview: ->
		if @$block.video_thumb.length
			@$block.video_thumb.each (key, video) =>
				$(video).find('img').attr('src', 'http://img.youtube.com/vi/' + @getVideoId($(video).attr('href')) + '/0.jpg')
			@initCarousel()
		else
			@initCarousel()

	getVideoId: (url) ->
		regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/
		match = url.match(regExp)
		if match and match[2].length is 11
		  return match[2]

$('[data-about]').each -> new About $(this)

module.exports = About

Tabs = require 'framework/tabs'

class ProfileTabs
	constructor: (@$el) ->


		@initTabs()
		@cacheDom()

	cacheDom: ->
		@$el.$switch = @$el.find('[data-tab-switch]')
		@$el.$tab = @$el.find('[data-tab]')


	initTabs: ->
		@tabs = new Tabs @$el


$('[data-profile-tabs]').each -> new ProfileTabs $(this)

module.exports = ProfileTabs

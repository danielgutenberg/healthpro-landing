# simple implementation to open booking for services
#
# It doesn't need a backbone structure because all it does is opens the booking wizard by click on the button.
# no need to render the services and make extra calls to the backend - everything is rendered on the profile page.
# we tale all the data from the bookAppointment app

Moment = require 'moment'
WizardAuth = require 'helpers/wizard_auth'

class BookServices
	constructor: (@$block) ->

		unless window.bookAppointment
			@initBlank()
			return

		if window.bookAppointment
			# make sure if the services are loaded before the block is loaded init right away
			if window.bookAppointment.model.get('services').length
				@init()
			else
				window.bookAppointment.model.on 'services:ready', => @init()

	initBlank: ->
		@hideLoading()
		@cacheDom()
		@$block.$btn.remove()
		@$block.$recurringBtn.remove()

	init: ->
		@hideLoading()
		@cacheDom()
		@bind()

	cacheDom: ->
		@$block.$btn = @$block.find('[data-booking-opener]')
		@$block.$recurringBtn = @$block.find('[data-recurring-opener]')
		@$block.$header = @$block.find('[data-booking-item-header]')


	bind: ->
		@$block.$btn.on 'click', (e) =>
			e.preventDefault()
			$btn =$(e.currentTarget)

			@openWizard
				service_id: $btn.closest('[data-service_id]').data('service_id')
				session_id: $btn.siblings('[data-session_id]').data('session_id')
			@

		@$block.$recurringBtn.on 'click', (e) =>
			e.preventDefault()
			$btn = $(e.currentTarget)

			@openWizardRecurring
				service_id: $btn.closest('[data-service_id]').data('service_id')

			@

		@$block.$header.on 'click', (e) =>
			$item = $(e.currentTarget).closest('[data-booking-item]')
			$item.toggleClass('m-expanded') if $item.find('[data-booking-item-content]').length
			@


	openWizard: (bookingDataOptions) ->
		bookingData =  @getBookingData bookingDataOptions
		openWizard = ->
			window.bookingHelper.openWizard
				bookingData: bookingData

		WizardAuth openWizard
		@

	openWizardRecurring: (bookingDataOptions) ->
		bookingData = @getBookingData bookingDataOptions
		openWizard = ->
			window.bookingHelper.openWizardRecurring
				bookingData: bookingData
				forcePage: true

		WizardAuth openWizard,
			msg: 'Please log in to book a monthly package'
		@


	getBookingData: (options) ->
		data =
			bookedFrom: 'profile'
			resetToFirstStep: 'recalculate'
			provider_id: window.GLOBALS.PROVIDER_ID
			from: Moment().format('YYYY-MM-DD')
			date: Moment().format('YYYY-MM-DD')
			services: bookAppointment.model.get('services')
			selected_time: {}

		if options.service_id?
			data.selected_time.service_id = options.service_id
		if options.session_id?
			data.selected_time.session_id = options.session_id

		data

	hideLoading: -> @$block.parent().find('.loading_overlay').remove()


$('[data-book_services]').each -> new BookServices $(this)

module.exports = BookServices

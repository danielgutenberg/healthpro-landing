class Certificates
	constructor: (@$block) ->
		require.ensure [], =>
			require 'owlCarousel'
			require 'magnificPopup'

			@$block.gallery = $('[data-certificates-gallery]', @$block)
			@$block.gallery.thumb = $('[data-certificates-gallery-thumb]', @$block)

			@initLightbox()
			@initCarousel()

	initCarousel: ->
		@$block.gallery.owlCarousel
			items: 2
			autoWidth: true
			margin: 20
			nav: true
			dots: false
			loop: false
			navRewind: false
			responsive:
				1190:
					items: 3

	initLightbox: ->
		@$block.gallery.thumb.magnificPopup
			type: 'image'
			gallery:
				enabled: true

$('[data-certificates]').each -> new Certificates $(this)

module.exports = Certificates

vexDialog = require 'vexDialog'

class Contact

	$popupMessageEl : null

	# the message option can be moved to the popup later if we'll need something similar
	# in a different module. for now keeping it here.
	msg : "To message a professional you must be logged in"

	# here we'll keep the initial redirect_to values
	initialRedirectTo:
		signUp : ''
		login : ''

	popup: null

	isLoggedIn : window.GLOBALS._UID isnt null
	isSameProfile : parseInt(window.GLOBALS.PROVIDER_ID) is parseInt(window.GLOBALS._PID)

	constructor: (@$block) ->
		return if @isLoggedIn and !@isSameProfile

		@cacheDom()
		@bind()

	bind: ->


		if @isSameProfile
			@$block.$messageBtn.on 'click', (e) =>
				e.preventDefault()
				@displaySameProfileNotice()

		else
			# add needed attributes to the button
			@$block.$messageBtn
				.attr('data-toggle', 'popup')
				.attr('data-target', 'popup_auth')

			# bind events to the popup
			window.popupsManager.bindForElement @$block.$messageBtn

			@$block.$messageBtn.on 'click', => @onOpenPopup()
			@popup.$block.on 'popup:closed', => @onClosePopup()

	onOpenPopup: ->
		@$popupMessageEl = $('<div class="popup--msg m-info"><p>' + @msg + '</p></div>')
		@popup.$block.find('.popup--content').prepend @$popupMessageEl
		@popup.$block.find('.popup--container').addClass 'm-msg'

		# show login form
		@popup.$block.find('[data-authpopup-toggle]').trigger 'click'

		# keep the initial values
		@initialRedirectTo.login = @$loginFormRedirectTo.val()
		@initialRedirectTo.signUp = @$signUpFormRedirectTo.val()

		@$loginFormRedirectTo.val @$block.$messageBtn.attr 'href'
		@$signUpFormRedirectTo.val @$block.$messageBtn.attr 'href'

	onClosePopup: ->

		# restore initial redirect_to values if popup was closed
		@$loginFormRedirectTo.val @initialRedirectTo.login
		@$signUpFormRedirectTo.val @initialRedirectTo.signUp

		# remove message element if exists
		if @$popupMessageEl
			@$popupMessageEl.remove()
			@$popupMessageEl = null
			@popup.$block.find('.popup--container').removeClass 'm-msg'

	cacheDom: ->
		@$block.$messageBtn = @$block.find('[data-message]')
		if !@isSameProfile
			@popup = window.popupsManager.getPopup 'popup_auth'

			@$signUpForm = @popup.$block.find('.auth_sign')
			@$loginForm = @popup.$block.find('.auth_login')

			@$signUpFormRedirectTo = @$signUpForm.find('input[name="redirect_to"]')
			@$loginFormRedirectTo = @$loginForm.find('input[name="redirect_to"]')


	displaySameProfileNotice: ->
		vexDialog.alert 'You cannot message your own profile.'

$('[data-contact]').each -> new Contact $(this)

module.exports = Contact

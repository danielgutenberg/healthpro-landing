class Slider
	constructor: (@$block) ->
		require.ensure [], =>
			require 'owlCarousel'

			@cacheDom()

			@init()

	cacheDom: ->
		@$carouselNav = $('.js-slider_nav')
		@$carouselNav_item = $('[data-slide-target]', @$carouselNav)

	init: ->
		@$block.addClass 'owl-carousel'
		@carousel = @$block.owlCarousel
			items: 1
			margin: 0
			dots: false
			nav: false
			loop: true
			autoplay: true
			animateOut: 'fadeOut'

		$('.home_hero--nav_link').on 'click', (e) =>
			e.preventDefault()
			target = $(e.currentTarget).data 'slide-id'
			@carousel.trigger 'to.owl.carousel', target

		@carousel.on 'changed.owl.carousel', (e) =>
			$carouselItem = $('.owl-item', @carousel).eq(e.item.index)
			$slideItem = $carouselItem.find('div')
			@$carouselNav_item.removeClass 'm-active'
			@$carouselNav.find('[data-slide-target="' + $slideItem.attr('id') + '"]').addClass 'm-active'

		# @$block.find('div:gt(0)').hide()
		# @$block.addClass 'm-inited'
		# setInterval (=>
		# 	@$block.find('div:first-child').fadeOut(1000).next('div').fadeIn(1000).end().appendTo @$block
		# ), 6000
js_slider = $('.js-slider');
js_slider.each -> new Slider $(this)
module.exports = Slider

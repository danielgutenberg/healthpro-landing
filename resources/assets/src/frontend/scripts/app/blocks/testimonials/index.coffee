class Testimonials
	constructor: (@$block) ->
		require.ensure [], =>
			require 'owlCarousel'

			@init()

	init: ->
		if @$block.children().length > 1
			@$block.addClass 'owl-carousel'
			@$block.owlCarousel
				items: 1
				autoWidth: false
				nav: true
				dots: true
				loop: true
				responsive:
					0:
						autoHeight: true
					480:
						autoHeight: false

$('.testimonials').each -> new Testimonials $(this)

module.exports = Testimonials

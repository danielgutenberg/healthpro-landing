class Carousel
	constructor: (@$block) ->
		require.ensure [], =>
			require 'owlCarousel'

			@init()

	init: ->
		if @$block.children().length > 1
			@$block.addClass 'owl-carousel'
			@$block.owlCarousel
				nav: true
				dots: false
				loop: true
				autoplay: true
				autoplayTimeout: 1500
				responsive:
					0:
						autoWidth: false
						items: 1
					480:
						items: 3
					640:
						autoWidth: true

$('.js-carousel').each -> new Carousel $(this)

module.exports = Carousel

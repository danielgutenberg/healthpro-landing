getApiRoute = require 'hp.api'

class Blog
	constructor: (@$block) ->
		@init @$block

	init: ($el) ->
		$autocomplete = $el.parent().find('.field--autocomplete')

		$el.autocomplete(
			appendTo: $autocomplete
			source: (request, response) =>
				data = []
				$.ajax
					url: getApiRoute('professionals-names-search')
					method: 'get'
					data:
						query: request.term

					success: (res) =>
						response @formatNames(res.data)
						return

			select: (e, ui) ->
				$el.data('searchVal', ui.item.value)
				$el.val ui.item.label
				$('#professional_id').val ui.item.value
				e.preventDefault()

			create: ->
				$(@).data('ui-autocomplete')._renderItem = (ul, item) ->
					$('<li>')
					.attr('id', "ui-id-#{item.value}")
					.attr('class', "ui-menu-item m-level m-level-#{item.value}")
					.append($( "<a>").text(item.label))
					.appendTo(ul)
		)

	formatNames: (data, results = []) ->
		_.each data, (item) =>
			results.push
				label: item.full_name
				value: item.id
		results

$('#name').each -> new Blog $(this)

module.exports = Blog

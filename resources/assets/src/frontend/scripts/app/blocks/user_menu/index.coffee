class UserMenu
	constructor: (@$el) ->
		@$el.$opener = $('[data-usermenu-el="opener"]', @$el)
		@$el.$dropdown = $('[data-usermenu-el="dropdown"]', @$el)

		@$el.$profilesOpener = $('[data-usermenu-profiles="opener"]', @$el)
		@$el.$profilesDropdown = $('[data-usermenu-profiles="dropdown"]', @$el)

		@bind()

	bind: ->
		@$el.$opener.on 'click', (e) =>
			e.preventDefault()
			@show('el')

		@$el.$profilesOpener.on 'click', (e) =>
			e.preventDefault()
			@show('profiles')

		$(window).on 'click', (e) =>
			$target = $(e.target)
			@hide('el') if !($target.closest(@$el.$opener).length or $target.closest(@$el.$dropdown).length)
			@hide('profiles') if !($target.closest(@$el.$profilesOpener).length or $target.closest(@$el.$profilesDropdown).length)
		@

	show: (type = 'el') ->
		if type is 'el'
			@$el.$dropdown.toggleClass('m-show')
		else
			@$el.$profilesDropdown.toggleClass('m-show')
		@

	hide: (type = 'el') ->
		if type is 'el'
			@$el.$dropdown.removeClass('m-show')
		else
			@$el.$profilesDropdown.removeClass('m-show')
		@

module.exports = UserMenu

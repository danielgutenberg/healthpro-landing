class SidebarNav
	constructor: (@$el) ->
		@name = @$el.data('sidebarnav')

		@cacheDom()
		@bind()

	cacheDom: ->
		@$el.$submenu 			= $('[data-sidebarnav-submenu]', @$el)
		@$el.$submenu.$opener 	= $('[data-sidebarnav-submenu-opener]', @$el.$submenu)
		@$el.$submenu.$menu 	= $('[data-sidebarnav-submenu-menu]', @$el.$submenu)

	bind: ->
		@$el.$submenu.$opener.on 'click', (e) =>
			e.preventDefault()
			@toggleSubMenu $(e.currentTarget)

	toggleSubMenu: ($el, display = null) ->
		$submenu = $el.closest('[data-sidebarnav-submenu]')
		switch display
			when true
				$submenu.addClass 'm-opened'
			when false
				$submenu.removeClass 'm-opened'
			else
				$submenu.toggleClass 'm-opened'

$('[data-sidebarnav]').each -> new SidebarNav $(this)

module.exports = SidebarNav

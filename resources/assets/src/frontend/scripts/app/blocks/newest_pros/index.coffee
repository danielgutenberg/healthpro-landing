getApiRoute = require 'hp.api'

class NewestPros
	constructor: (@$block) ->
		require.ensure [], =>
			require 'owlCarousel'

			@itemsCount = @$block.children().length

			@bind()
			@tryInit()

	bind: ->
		$(window).on 'resize', =>
			@tryInit()

	init: ->
		return if @isInited

		@$block.addClass 'owl-carousel'
		@carousel = @$block.owlCarousel
			items: 6
			margin: 20
			autoWidth: false
			nav: true
			dots: false
			loop: true
			responsive:
				0:
					items: 1
				550:
					items: 2
				800:
					items: 3
				1024:
					items: 4
				1180:
					items: 5
				1350:
					items: 6
			onInitialized: =>
				@isInited = true

	destroy: ->
		return unless @isInited
		
		if @carousel
			@carousel.trigger('destroy.owl.carousel')
			@$block.removeClass 'owl-carousel'
			@isInited = false

	tryInit: ->
		@windowWidth = $(window).width()

		if @itemsCount > 5
			@init()
			return
		else
			switch @itemsCount
				when 5
					if @windowWidth < 1350 then @init() else @destroy()
				when 4
					if @windowWidth < 1180 then @init() else @destroy()
				when 3
					if @windowWidth < 1024 then @init() else @destroy()
				when 2
					if @windowWidth < 800 then @init() else @destroy()
				else return

$('.js-newest_pros').each -> new NewestPros $(this)

module.exports = NewestPros

class Help
	constructor: (@$block) ->
		@$block.$nav = @$block.find('.help--nav')
		@$block.$toggler = @$block.find('.help--nav_toggler')

		@bind()

	bind: ->
		@$block.$toggler.on 'click', (e) =>
			e.preventDefault()
			@$block.$nav.toggleClass 'm-opened'

		$(document).on 'click', (e) =>
			return unless @$block.$nav.hasClass 'm-opened'
			return if $(e.target).closest('.sidebar_nav--item').length
			@$block.$nav.removeClass 'm-opened'


$('.help').each -> new Help $(this)

module.exports = Help

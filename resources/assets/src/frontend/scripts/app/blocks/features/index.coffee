class Features
	constructor: (@$block) ->
		@$featuresLinks = @$block.find('a')
		@defaultFeature = @$featuresLinks.filter(':first').data('feature')

		@init()

	init: ->
		@$block.addClass('m-' + @defaultFeature)

		@$block.find('a[data-feature]').hover(
			(e) =>
				$link = $(e.currentTarget)
				@$block
					.removeClass('m-' + @defaultFeature)
					.addClass('m-' + $link.data('feature'))

			,
			(e) =>
				$link = $(e.currentTarget)
				@$block
					.removeClass('m-' + $link.data('feature'))
					.addClass('m-' + @defaultFeature)
		)

$('.home-grow').each -> new Features $(this)

module.exports = Features

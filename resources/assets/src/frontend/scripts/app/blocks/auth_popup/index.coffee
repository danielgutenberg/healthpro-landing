Form = require 'framework/form'
getApiRoute = require 'hp.api'
vexDialog = require 'vexDialog'
capterraTrack = require 'utils/capterra_tracker'

class AuthPopup

	profile: null
	containerSlug: null
	callback: null

	constructor: (@$block) ->

		@$block.$containers = @$block.find('[data-auth-container]')
		@$block.$sidebar = @$block.find('[data-auth-sidebar]')
		@$block.$profileType = @$block.find('[name="profile_type"]')
		@$block.$redirectTo = @$block.find('[name="redirect_to"]')
		@$block.$switch = @$block.find('[data-switch]')
		@$block.$socials = @$block.find('.btn_social')

		@$block.$signupForm = @$block.find('form.m-sign')
		@$block.$loginForm = @$block.find('form.m-login')
		@$block.$resetForm = @$block.find('form.m-reset')

		@$block.$socials = @$block.find('.btn_social')

		@$block.$loading = @$block.find('[data-authpopup-loading]')

		@$togglers = $('[data-authpopup-toggle]')

		@initForms()
		@bind()
		@maybeOpenPopup()

	bind: ->
		@$togglers.on 'click', (e) =>
			@activateContainer $(e.currentTarget)?.data('authpopup-toggle'), e

		@$block.$socials.on 'click', (e) =>
			e.preventDefault()
			@socialLogin $(e.currentTarget)[0].href

	acceptTerms: (link) ->
		termsLink = window.location.origin + '/terms'

		vexDialog.open
			message: 'Please accept our terms and conditions to continue.'
			input: """
				<input class="vex-custom-checkbox" type="checkbox">
					I agree to HealthPRO's <a href="#{ termsLink }" target="_blank">Terms of Service</a>
				</input>
			"""
			afterOpen: ($vexContent) ->

				$btn = $('.vex-dialog-button-primary')
				$check = $('.vex-custom-checkbox')

				$btn.attr("disabled","disabled")
				$btn.css("background","#e0e0e0")
				$check.change ->
					if $(this).is(':checked')
						$btn.removeAttr("disabled")
						$btn.css("background","#2b95a7")
					else
						$btn.attr("disabled","disabled")
						$btn.css("background","#e0e0e0")

			callback: =>
				unless $('.vex-custom-checkbox').is(':checked')
					return false
				window.location.href = link

	initForms: ->
		@initLoginForm()
		@initSignUpForm()
		@initResetForm()

	initLoginForm: ->
		@loginForm = new Form @$block.$loginForm,
			customLoading: true
			$loading: @$block.$loading
			ajax: true
			url: getApiRoute('ajax-auth-login')
			onSuccess: (response) =>
				@setGlobals response.data
				if _.isFunction @callback
					@callback()
					@closePopup()
				else
					window.location.href = response.data.redirect_to ? @getLocationRoot()

	initSignUpForm: ->
		@signupForm = new Form @$block.$signupForm,
			customLoading: true
			$loading: @$block.$loading
			ajax: true
			url: getApiRoute('ajax-auth-sign-up')
			onSuccess: (response) =>
				@setGlobals response.data
				capterraTrack()
				if _.isFunction @callback
					@callback()
					@closePopup()
				else if response?.data?.profile_type? and response.data.profile_type is 'client'
					@closePopup()
					@showWelcomePopup()
				else
					window.location.href = response.data.redirect_to ? @getLocationRoot()
				@

	initResetForm: ->
		@resetForm = new Form @$block.$resetForm,
			customLoading: true
			$loading: @$block.$loading
			ajax: true
			url: getApiRoute('ajax-auth-reset-password')
			onSuccess: =>
				@activateContainer 'reset_success'

	showWelcomePopup: ->
		window.popupsManager.openPopup 'client_welcome'

	getLocationRoot: ->
		window.location.origin ? "#{window.location.origin}/" : "#{window.location.protocol}/#{window.location.host}/"

	activateContainer: (container = 'sign_up', e) ->

		redirect_to = $(e?.currentTarget)?.data('auth-redirect')
		if redirect_to? then @setRedirectUrl(redirect_to)

		return if @containerSlug is container

		parts = container.split(':')
		@containerSlug = parts[0]

		@$currentContainer = @$block.$containers.filter("[data-auth-container='#{@containerSlug}']")

		# set the sign up form values
		if parts.length is 2
			@setProfileType parts[1]
			@setSidebarText()
			@setSidebarBg()

		@$block.$loading.addClass 'm-hide'
		@$block.$containers.removeClass 'm-show'
		@$currentContainer.addClass 'm-show'

		@setActiveSwitch()
		@clearFormsErrors()

		@

	setRedirectUrl: (redirect_to) ->
		@$block.$redirectTo.val redirect_to
		@

	setProfileType: (profile = 'provider') ->
		@profile = profile
		@$block.$profileType.val @profile
		@

	setSidebarBg: ->
		@$block.$sidebar.closest('.auth_aside')
			.removeClass('m-client')
			.removeClass('m-provider')
			.addClass("m-#{@profile}")

	setSidebarText:  ->
		return unless @containerSlug is 'sign'
		switch @profile
			when 'client'
				@$block.$sidebar.html @getClientSignUpText()
			when 'provider'
				@$block.$sidebar.html @getProfessionalSignUpText()

	setCallback: (@callback) ->

	getClientSignUpText: ->
		"""
			<h2 class="auth_aside--title">Sign Up as a Client</h2>
			<ul class="auth_aside--list">
				<li>Sign up now to become a HealthPRO client and get access to the professionals in your area.</li>
			</ul>
		"""

	getProfessionalSignUpText: ->
		"""
			<h2 class="auth_aside--title">Sign Up as a Professional</h2>
			<ul class="auth_aside--list">
				<li>Create an account with our simple-to-use Wizard.</li>
				<li>Once your account is created, you can change information from your dashboard.</li>
				<li>After your account has been verified, your clients will be able to easily find you in our directory.</li>
			</ul>
		"""


	setActiveSwitch: ->
		switch @containerSlug
			when 'sign', 'sign_up'
				sw = 'login'
			else
				sw = 'sign'

		@$block.$switch.removeClass('m-show').filter("[data-switch='#{sw}']").addClass('m-show')

	maybeOpenPopup: ->
		authPopup = window.GLOBALS.AUTHPOPUP
		if authPopup? and authPopup != ''
			container = authPopup
			validPopups = ['login', 'new_password', 'reset']
			unless authPopup in validPopups
				container = 'sign_up'
				parts = authPopup.split(':')
				if parts[1]?
					@profile = parts[1]
					container = 'sign:' + parts[1]
			@activateContainer container
			@openPopup()

	clearFormsErrors: ->
		@loginForm.unsetErrors() if @loginForm
		@signupForm.unsetErrors() if @signupForm

	socialLogin: (url) ->
		link = "#{url}?profile=#{@profile}"
		if @containerSlug is 'sign'
			link += '&login=false'
		else
			link += '&login=true'
		professionalId = window.GLOBALS.PROVIDER_ID
		if professionalId? and professionalId != ''
			link += '&pro_page=' + professionalId
		if @containerSlug is 'sign' and @$block.find('.auth_form--terms').is(':checked') is false
			@acceptTerms link
			return

		window.location.href = link

	closePopup: (target = 'popup_auth') ->
		window.popupsManager.closePopup target
		@

	openPopup: (target = 'popup_auth') ->
		window.popupsManager.openPopup target
		@

	setGlobals: (responseData) ->
		if responseData.profile_id?
			window.GLOBALS._PID = responseData.profile_id

		if responseData.profile_type?
			window.GLOBALS._PTYPE = responseData.profile_type
		@

module.exports = AuthPopup

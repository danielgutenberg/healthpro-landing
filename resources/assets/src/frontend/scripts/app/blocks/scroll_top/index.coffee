class ScrollTopButton
	constructor: (@$block) ->
		@offset = 500
		@offset_opacity = 1300
		@scroll_top_duration = 700

		@bind()
		@init()

	bind: ->
		@$block.on 'click', (event) ->
			event.preventDefault()
			$('body,html').animate { scrollTop: 0 }, @scroll_top_duration

	init: ->
		$(window).scroll =>
			if $(window).scrollTop() > @offset then @$block.addClass('m-visible') else @$block.removeClass('m-visible m-fade_out')
			if $(window).scrollTop() > @offset_opacity
				@$block.addClass 'm-fade_out'

$('.scroll_top').each -> new ScrollTopButton $(this)

module.exports = ScrollTopButton

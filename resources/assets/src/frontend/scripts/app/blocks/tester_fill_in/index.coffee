class FillIn
	constructor: (@$block) ->
		@cacheDom()
		@init()

	cacheDom: ->
		@$block.$btn = $('[data-fill-btn]', @$block)
		@$block.$firstName = $('[name="first_name"]', @$block)
		@$block.$lastName = $('[name="last_name"]', @$block)
		@$block.$email = $('[name="email"]', @$block)
		@$block.$password = $('[name="password"]', @$block)
		@$block.$cardName = $('[name="card_name"]', @$block)
		@$block.$cardNumber = $('[name="card_number"]', @$block)
		@$block.$cardExp = $('[name="card_exp"]', @$block)
		@$block.$card_cvv = $('[name="card_cvv"]', @$block)
		@$block.$cvv = $('[name="cvv"]', @$block)
		@$block.$terms = $('.auth_form--terms', @$block)

	init: ->
		@bind()

	bind: ->
		@$block.$btn.on 'click', (e) =>
			e.preventDefault()
			if @$block.$firstName?.val() == ''
				@$block.$firstName.val('HealthPRO').focus().trigger('change')

			if @$block.$lastName?.val() == ''
				@$block.$lastName.val('Tester').focus().trigger('change')

			if @$block.$email?.val() == ''
				@$block.$email.val('@healthpro.com').focus().trigger('change')

			if @$block.$password?.val() == '' or @$block.$password?.val().length < 7
				@$block.$password.val('1234567').focus().trigger('change')

			if @$block.$cardName?.val() == ''
				@$block.$cardName.val(@$block.$firstName.val() + ' ' + @$block.$lastName.val()).focus().trigger('change')

			if @$block.$cardNumber?.val() == ''
				@$block.$cardNumber.val('4242 4242 4242 4242').focus().trigger('change')

			if @$block.$cardExp?.val() == ''
				@$block.$cardExp.val('01/2020').focus().trigger('change')

			if @$block.$card_cvv?.val() == ''
				@$block.$card_cvv.val('252').focus().trigger('change')

			if @$block.$cvv?.val() == ''
				@$block.$cvv.val('252').focus().trigger('change')

			@$block.$terms.prop('checked', true).focus().trigger('change')

module.exports = FillIn

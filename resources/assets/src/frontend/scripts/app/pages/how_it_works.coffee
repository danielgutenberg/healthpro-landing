Device = require 'utils/device'

class HowItWorks
	constructor: (@$block) ->
		@startScrollyFrom = 0
		@canScroll = true
		@scrollLocked = false
		@currentSection = 0
		@wheelEvent = if 'onwheel' of document then 'wheel' else if document.onmousewheel != undefined then 'mousewheel.scrolly' else 'DOMMouseScroll.scrolly'
		@scrollySections = []

		@animateDuration = 400
		@animationHold = 1200

		@cacheDom()
		@cacheSizes()
		@collectScrollySections()
		@getInitState()
		@bind()

	cacheDom: ->
		@$window = $(window)
		@$header = $('.header')
		@$htmlBody = $('html, body')
		@$body = $('body')

		@$collapsible = $('[data-collapsible]', @$block)
		@$collapsibleItem = $('[data-collapsible-item]', @$collapsible)
		@$collapsibleOpener = $('[data-collapsible-opener]', @$collapsibleItem)

		@$scrollMoreLink = $('[data-scroll-more]')

		@$scrollySection = $('[data-scrolly-section]')

		@allowScrolly = Device.isDesktop() or window.matchMedia("(min-width: 740px)").matches

	cacheSizes: ->
		@$window.height = @$window.height()
		@$header.height = @$header.outerHeight()

	bind: ->
		@$collapsibleOpener.on 'click', (e) =>
			e.preventDefault()
			@toggleCollapsible(e.currentTarget)

		@$scrollMoreLink.on 'click', (e) =>
			e.preventDefault()

			if @allowScrolly
				targetSection = @scrollySections[1]
				targetScrollTop = targetSection.targetScrollTop
				$(targetSection.$el).addClass('m-animate_in')
			else
				targetScrollTop = $(window).height()

			@$htmlBody.animate
				scrollTop: targetScrollTop
			, @animateDuration

		$('[data-scroll]').on 'click', =>
			@canScroll = true
			@scrollLocked = false
			@currentSection = @scrollySections.length - 1

		if @allowScrolly
			@$window.on @wheelEvent, (e) =>
				@scrollAssist(e)

		@$window.on 'resize', =>
			@collectScrollySections()

	toggleCollapsible: (block) ->
		$target = $(block).parents('[data-collapsible-item]')
		$targetContent = $target.find('[data-collapsible-content]')

		$target.siblings().find('[data-collapsible-content]').slideUp(500, () ->
			$target.siblings().removeClass('m-can_animate')
			)
		$target.siblings().removeClass('m-opened')
		$targetContent.slideToggle(500, () ->
			$target.toggleClass('m-can_animate')
			)
		$target.toggleClass('m-opened')

	collectScrollySections: ->
		@scrollySections = []
		@$scrollySection.each (index, el) =>
			$el = $(el)
			$el.height = $el.outerHeight()
			$el.offsetTop = $el.offset().top

			@scrollySections.push
				$el: $el
				id: index
				height: $el.height
				offsetTop: $el.offsetTop
				offsetBottom: $el.offsetTop + $el.height
				targetScrollTop: @calcScrollTop($el, $el.height, $el.offsetTop)
				isFirst: index is 0
				isLast: index is (@$scrollySection.length - 1)

	getInitState: ->
		scrollTop = @$window.scrollTop()
		if scrollTop >= @startScrollyFrom
			@canScroll = false

		# detecting current section on load
		$.each @scrollySections, (i, section) =>
			if scrollTop >= section.targetScrollTop
				if scrollTop <= section.targetScrollTop + section.height
					@currentSection = section.id
				else
					@currentSection = @$scrollySection.length - 1

			currentSection = @scrollySections[@currentSection]
			$(currentSection.$el).addClass('m-animate_in')

	scrollAssist: (e) ->
		scrollTop = @$window.scrollTop()
		e = e or window.event
		delta = if e.type == 'DOMMouseScroll' then e.originalEvent.detail * -40 else e.originalEvent.wheelDelta

		if @scrollLocked
			e.preventDefault()

		# Handling returning to scrolly sections
		if @canScroll
			if delta > 0
				if scrollTop < @scrollySections[@currentSection].targetScrollTop
					e.preventDefault()
					@canScroll = false
					@isReturning = true

		else
			unless @canScroll
				e.preventDefault()

				currentSection = @scrollySections[@currentSection]

				if delta > 0 # scrolling up
					targetSection = @scrollySections[@currentSection - 1]
					targetScrollTop = if targetSection? then targetSection.targetScrollTop else @startScrollyFrom
				else # scrolling down
					targetSection = @scrollySections[@currentSection + 1]
					# if no further sections then scroll as usual
					if targetSection?
						targetScrollTop = targetSection.targetScrollTop
					else @canScroll = true

				if targetSection? and targetSection?.id isnt @currentSection and !@scrollLocked
					@scrollLocked = true

					if targetSection.isLast
						@canScroll = true
						@scrollLocked = false

					activationHold = 0

					if @isReturning
						activationHold = 1400

					setTimeout((=>
						$(currentSection.$el).removeClass('m-animate_in')
						$(currentSection.$el).addClass('m-animate_out')
						$(targetSection.$el).addClass('m-animate_in')

						if targetSection.isLast
							setTimeout((=>
								$(currentSection.$el).removeClass('m-animate_out')
								if targetSection? then @currentSection = targetSection.id
								@scrollLocked = false
							), @animationHold)
						else
							@$htmlBody.animate
								scrollTop: targetScrollTop
							, @animateDuration, () =>
								setTimeout((=>
									$(currentSection.$el).removeClass('m-animate_out')
									if targetSection? then @currentSection = targetSection.id
									@scrollLocked = false
								), @animationHold)
					), activationHold)


	calcScrollTop: ($el, height, offsetTop) ->
		$el.height = if height then height else $el.outerHeight()
		$el.offsetTop = if offsetTop then offsetTop else $el.offset().top

		# Calculation offset to scrollTo element centered in viewport
		elCenteredPaddings = (@$window.height - $el.height) / 2
		targetScrollTop = $el.offsetTop - (@$header.height / 2) - elCenteredPaddings

		targetScrollTop = Math.floor(targetScrollTop)

		if targetScrollTop < 0 then return 0 else return targetScrollTop

$('.how_it_works').each -> new HowItWorks $(this)

module.exports = HowItWorks

getApiRoute = require 'hp.api'

class Pricing
	constructor: (@$block) ->

		@canStick = true

		@init()

	init: ->
		@cacheDom()
		@cacheSizes()
		@bind()
		@checkCanStick()
		@stickOnScroll()

	cacheDom: ->
		@$boxOptions = $('.pricing_box--short', @$block)
		@$plans = @$block
		@$plansWrap = $('.pricing_plans--wrap', @$block)
		@$layoutHeader = $('.layout--header')

		@$pricingBtn = $('[data-pricing-btn]')

	cacheSizes: ->
		@$boxOptions.offsetTop = @$boxOptions.offset().top
		@$layoutHeader_height = @$layoutHeader.height()
		@$plansWrap_height = @$plansWrap.outerHeight()
		@$plans_paddingTop = @$plans.css('padding-top')

		if window.matchMedia('(max-width: 980px)').matches
			@tartgetOffsetTop = @$boxOptions.offsetTop
		else
			@tartgetOffsetTop = @$boxOptions.offsetTop - @$layoutHeader_height

	bind: ->
		$(window).on 'scroll', =>
			@stickOnScroll()

		$(window).on 'resize', =>
			@cacheSizes()
			@checkCanStick()
			@stickOnScroll()

	stickOnScroll: ->
		scrollTop = $(window).scrollTop()

		if @canStick
			if scrollTop > @tartgetOffsetTop
				@stickPlans()
			else
				@unstickPlans()
		else @unstickPlans()

	stickPlans: ->
		@$plansWrap.addClass('m-fixed')
		@$plans.css
			'padding-top': @$plansWrap_height

	unstickPlans: ->
		@$plansWrap.removeClass('m-fixed')
		@$plans.css
			'padding-top': @$plans_paddingTop

	checkCanStick: ->
		if window.matchMedia('(max-width: 760px)').matches
			@canStick = false
		else
			@canStick = true


$('[data-pricing]').each -> new Pricing $(this)

module.exports = Pricing

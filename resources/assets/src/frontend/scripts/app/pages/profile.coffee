class Profile
	constructor: ->
		require 'app/modules/profile_map'
		require 'app/modules/set_password_popup'

		# init profile
		require 'app/blocks/profile/about'
		require 'app/blocks/profile/certificates'
		require 'app/blocks/profile/profile_contact'
		require 'app/blocks/profile/profile_card'
		require 'app/blocks/profile/expand_list'
		require 'app/blocks/profile/mobile_view'
		require 'app/blocks/profile/careers'
		require 'app/blocks/profile/booking_widget'
		require 'app/blocks/profile/book_services'


		require 'app/blocks/profile/review_public_profile'

		require 'app/modules/profile_invites'

		# init edit profile if needed
		require 'app/modules/profile_edit/inline'

$('[data-profile]').each -> new Profile()

module.exports = Profile

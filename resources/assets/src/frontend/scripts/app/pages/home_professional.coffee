class HomeProfessional
	constructor: (@$page) ->
		@initHubspot()
		@initCallMe()

	initHubspot: ->
		require.ensure [], ->
			Hubspot = require 'framework/hubspot'

			# call me form
			Hubspot.initForm
				portalId: '496304'
				formId: '131f92d3-eb52-45e1-8e3d-4d7b351a6d45'
				target: '#home_professional_call_me_form'
				submitButtonClass: 'cta_btn m-blue m-wide'
				inlineMessage: ' '
				onFormSubmit: ->
					$('#home_professional_call_me').addClass 'm-success'

			# newsletter sign up
			Hubspot.initForm
				portalId: '496304'
				formId: 'd71e5cf5-3e2a-40ee-8529-ab60b145200d'
				target: '#home_professional_subscribe_form'
				submitButtonClass: 'btn'

	initCallMe: ->
		$popup = $('#home_professional_call_me')
		$popup.on 'popup:opened', -> $popup.removeClass 'm-success'

$('.home-professional').each -> new HomeProfessional $(this)

module.exports = HomeProfessional

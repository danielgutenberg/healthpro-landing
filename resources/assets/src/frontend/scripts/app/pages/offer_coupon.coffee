Form = require 'framework/form'
getApiRoute = require 'hp.api'

class OfferCoupon

	constructor: (@$block) ->
		@$block.$containers = @$block.find('[data-auth-container]')
		@$block.$loginForm = @$block.find('form.m-login')
		@$togglers = $('[data-authpopup-toggle]')

		@initLoginForm()
		@activateContainer()
		@bind()

	bind: ->
		@$togglers.on 'click', (e) =>
			@activateContainer $(e.currentTarget).data('authpopup-toggle')

	initLoginForm: ->
		@loginForm = new Form @$block.$loginForm,
			ajax: true
			url: getApiRoute('ajax-auth-login')
			onSuccess: (response) =>
				window.location.href = response.data.redirect_to ? @getLocationRoot()

	activateContainer: (container = 'login') ->
		return if @containerSlug is container

		parts = container.split(':')
		@containerSlug = parts[0]

		@$currentContainer = @$block.$containers.filter("[data-auth-container='#{@containerSlug}']")

		@$block.$containers.removeClass 'm-show'
		@$currentContainer.addClass 'm-show'

		@clearFormsErrors()

		@

	clearFormsErrors: ->
		@loginForm.unsetErrors() if @loginForm

$('.home-offer').each -> new OfferCoupon $(this)

module.exports = OfferCoupon

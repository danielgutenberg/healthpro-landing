class Features
	constructor: ->
		require.ensure [], ->
			Hubspot = require 'framework/hubspot'

			# @TODO - update formID
			Hubspot.initForm
				portalId: '496304'
				formId: 'f2cc8ef3-28f0-47b9-8979-5c51e0ec00e2'
				target: '#promo_form_feedback'
				submitButtonClass: 'btn m-top'

		@cacheDom()
		@bind()

	cacheDom: ->
		@$tabs = $('[data-tabs]')
		@$tabsItem = $('[data-tabs-target]', @$tabs)
		@$tabsPanel = $('[data-tabs-panel]', @$tabs)

	bind: ->
		@$tabsItem.on 'click', (e) =>
			e.preventDefault()
			@togglePanel(e.currentTarget)

	togglePanel: (item) ->
		$item = $(item)
		target = $item.data('tabs-target')

		@$tabsItem.removeClass('m-active')
		$item.addClass('m-active')
		@$tabsPanel.removeClass('m-active')
		$('#' + target).addClass('m-active')

$('.page-features').each -> new Features()

module.exports = Features

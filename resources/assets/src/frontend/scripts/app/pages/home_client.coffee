class HomeClient
	constructor: ->

		require.ensure [], ->
			SearchFilters = require 'app/modules/search/search_filters'
			Hubspot = require 'framework/hubspot'

			# init demo forms
			Hubspot.initForm
				sfdcCampaignId: '70124000000LkCMAA0'
				portalId: '496304'
				formId: 'cda05ec6-49f7-4261-b8d2-28eb194d365d'
				target: '#home_client_feedback_form'
				submitButtonClass: 'btn'

			## init demo forms
			Hubspot.initForm
				sfdcCampaignId: '70124000000LkCMAA0'
				portalId: '496304'
				formId: '5679bdc5-416d-4bd9-8ea3-cd79bbaa0109'
				target: '#home_client_beta_form'
				submitButtonClass: 'btn'

			# init homepage search form
			if $('.home-search .js-search_filters').length
				searchFilters = new SearchFilters.App
					type: 'landing'
				searchFilters.view.render()

#			if $('.js-get_app').length
#				if navigator.userAgent.match(/(iPod|iPhone)/i)
#					$('.js-get_app').show()
#
#				if $('.js-get_app_close').length
#					$('.js-get_app_close').on 'click', (e) ->
#						e.preventDefault()
#						$('.js-get_app').hide()


$('.home-client').each -> new HomeClient()

module.exports = HomeClient

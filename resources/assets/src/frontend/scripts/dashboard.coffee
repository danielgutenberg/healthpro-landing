HP_App = require 'app/app'

# general scripts
require 'framework/tabs'
require 'app/blocks/sidebar_nav'
require 'app/modules/dashboard'

class HP_Dashboard
	constructor: ->
		@app = new HP_App 'dashboard'

		@$body = $('body')
		@currentPage = _.trim @$body.data('page')

		@initPage()

	initPage: ->

		if $('#welcome_returning').length
			require 'app/modules/welcome_returning'

		if $('#dashboard_welcome').length
			require 'app/modules/dashboard_welcome'

		if $('[data-start-tour]').length
			require 'app/modules/tour'

		if $('[data-cs-form]').length
			require.ensure [], ->
				require 'app/modules/cs_form'

		# init notifications
#		require 'app/modules/notifications'

		switch @currentPage
			# my profile / suitability & conditions * professional only
			when 'suitability-conditions'
				require 'app/modules/professional_setup_suitability_conditions'

			# my profile / locations & services * professional only
			when 'locations-services'
				require 'app/modules/professional_setup/setup_locations_services'

			# messages
			when 'conversations'
				require 'app/modules/conversations'

			# settings / account
			when 'settings/account'
				require 'app/modules/profile_edit/edit_settings'

			# settings / calendar
			when 'settings/calendar'
				require 'app/modules/calendar_settings'

			# my profile / basic info
			when 'my-profile/personal-info'
				require 'app/modules/profile_edit/edit_personal'

			# my profile / about me * professional only
			when 'my-profile/about-me'
				require 'app/modules/profile_edit/edit_about_me'

			# my profile / qualifications * professional only
			when 'my-profile/qualifications'
				require 'app/modules/profile_edit/edit_credentials'

			# my profile / media * professional only
			when 'my-profile/media'
				require 'app/modules/profile_edit/edit_assets'

			# edit credit cards * client only
			when 'settings/payments-client'
				require 'app/modules/profile_edit/card_manager'

			# edit debit cards * professionals only
			# unused
			# when 'settings/payments-professional'
			# 	require 'app/modules/profile_edit/debit_manager'

			# receive payments settings * professionals only
			when 'settings/payments-professional'
				require 'app/modules/profile_edit/receive_payments'

			# edit notifications settings
			when 'settings/notifications'
				require 'app/modules/notifications_settings'

			# my clients * professional only
			when 'my-clients'
				require 'app/modules/professional_clients'

			# my professionals * client only
			when 'my-professionals'
				require 'app/modules/booking_helper'
				require 'app/modules/client_professionals'

			# my calendar * professional only
			when 'schedule-professional'
				require 'app/modules/professional_schedule'

			# my calendar * client only
			when 'schedule-client'
				require 'app/modules/client_schedule'

			# my appointments * professional only
			when 'appointments'
				require 'app/modules/professional_appointments'

			# my appointments * client only
			when 'appointments-client'
				require 'app/modules/client_appointments'

			# my packages * professional only
			when 'packages-professional'
				require 'app/modules/professional_packages'

			# my packages * client only
			when 'packages-client'
				require 'app/modules/client_packages'

			# my transactions
			when 'my-transactions'
				require 'app/modules/transactions'

			# my locations
			when 'client-locations'
				require 'app/modules/client_locations'

			# client gift certificates
			when 'client-gift-certificates'
				require 'app/modules/client_gift_certificates'

			# professional gift certificates
			when 'professional-gift-certificates'
				require 'app/modules/professional_gift_certificates'

			# billing
			when 'billing'
				require 'app/modules/billing'

			# billing history
			when 'billing-history'
				require 'app/modules/billing_history'


new HP_Dashboard()

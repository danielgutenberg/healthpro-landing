module.exports = (callback, options = {}) ->

	_.defaults options,
		msg: 'Please log in to book an appointment'
		popupId: 'popup_auth'
		authContainer: 'login'
		disableProfessionalSignUp: true

	if window.GLOBALS._PID
		callback()
		return

	$signupToggler = window.authPopup.$block.find('[data-authpopup-toggle="sign_up"]')

	$message = $("<div class='popup--msg m-info'><p>#{options.msg}</p></div>")

	popup = window.popupsManager.getPopup options.popupId

	$popupContent = popup.$block.find('.popup--content')
	$popupContainer = popup.$block.find('.popup--container')

	$popupContent.prepend $message
	$popupContainer.addClass 'm-msg'

	popup.$block.on 'popup:closed', =>
		$message.remove()
		$popupContainer.removeClass 'm-msg'
		window.authPopup.setCallback null
		# restore signup
		if options.disableProfessionalSignUp
			$signupToggler.data('authpopup-toggle', 'sign_up')

	# for wizard we want only the client signups to be allowed
	if options.disableProfessionalSignUp
		$signupToggler.data('authpopup-toggle', 'sign:client')

	# finally open popup
	window.authPopup.setCallback callback
	window.authPopup.activateContainer options.authContainer
	window.popupsManager.openPopup options.popupId

	@

require 'imports?this=>window!medium-editor-insert-plugin'
require 'framework/hubspot'

pluginName ='mediumInsert'
addonName ='Cta'
defaults =
	label: '<span class="fa fa-stop"></span>'
	placeholder: "Paste your CTA code here."

class MediumEditorInsertCta

	constructor: (el, options) ->
		@el = el
		@$el = $(el)
		@templates = window.MediumInsert.Templates
		@core = @$el.data "plugin_#{pluginName}"
		@options = $.extend true, {}, defaults, options
		@_defaults = defaults
		@_name = pluginName

		# extend editor methods
		if @core.getEditor()
			@core.getEditor()._preSerialize = @core.getEditor().serialize
			@core.getEditor().serialize = @editorSerialize

		@templates =
			empty: "<p><br></p>"

		@init()

	init: ->

		$ctas = @$el.find('.medium-insert-cta')
		$ctas.attr 'contenteditable', false

		$ctas.each ->
			$cta = $(this)
			unless $cta.find('.medium-insert-cta-overlay').length
				$cta.append $('<div />').addClass('medium-insert-cta-overlay')

		@events()

	# Event listeners
	events: ->

		$(document)
			.on('keydown', $.proxy(@, 'removeCta'))
			.on('click', $.proxy(@, 'unselectCta'))

		@$el
			.on('keyup click paste', $.proxy(@, 'togglePlaceholder'))
			.on('keydown', $.proxy(@, 'processCode'))
			.on('click', '.medium-insert-cta', $.proxy(this, 'selectCta'))
			.on('contextmenu', '.medium-insert-cta-placeholder', $.proxy(@, 'fixRightClickOnPlaceholder'))


	getCore: -> @core

	# This function is called when user click on the addon's icon
	add: ->

		$place = @$el.find('.medium-insert-active')

		$place.html @templates.empty

		# Replace paragraph with div to prevent #124 issue with pasting in Chrome,
		# because medium editor wraps inserted content into paragraph and paragraphs can't be nested
		if $place.is('p')
			$place.replaceWith '<div class="medium-insert-active">' + $place.html() + '</div>'
			$place = this.$el.find('.medium-insert-active')
			@core.moveCaret($place)

		$place.addClass('medium-insert-cta medium-insert-cta-input medium-insert-cta-active');

		@togglePlaceholder({target: $place.get(0)})
		$place.click()
		@core.hideButtons()


		return


	togglePlaceholder: (e) ->
		$place = $(e.target)
		selection = window.getSelection()

		range = null
		$current = null
		text = null

		return if !selection or selection.rangeCount == 0

		range = selection.getRangeAt(0)
		$current = $(range.commonAncestorContainer)

		if $current.hasClass('medium-insert-cta-active')
			$place = $current
		else if $current.closest('.medium-insert-cta-active').length
			$place = $current.closest('.medium-insert-cta-active')

		if $place.hasClass('medium-insert-cta-active')
			text = $place.text().trim()
			if text == '' and $place.hasClass('medium-insert-cta-placeholder') == false
				$place.addClass('medium-insert-cta-placeholder').attr 'data-placeholder', @options.placeholder
			else if text != '' and $place.hasClass('medium-insert-cta-placeholder')
				$place.removeClass('medium-insert-cta-placeholder').removeAttr 'data-placeholder'

		else
			@$el.find('.medium-insert-cta-active').remove()

		return

	fixRightClickOnPlaceholder: (e) -> @core.moveCaret $(e.target)


	# parse the code
	processCode: (e) ->

		$place = @$el.find('.medium-insert-cta-active')
		return unless $place.length

		code = $place.text().trim()

		# Return empty placeholder on backspace, delete or enter
		if code is '' and [8, 46, 13].indexOf(e.which) is not -1
			$place.remove()
			return

		if e.which is 13
			e.preventDefault()
			e.stopPropagation()

			@parseCode code

	parseCode: (code) ->

		# hbspt.cta.load(496304, '581c8643-525d-418c-8862-a25c8ba18dfb', {});
		# find the ctaId and portalId
		matches = code.match ///
			hbspt\.cta\.load
			\(
			(\d+)
			\,\s*
			\'
			([^\']+)
			\'
		///i
		# we found the input
		unless matches and matches[1] and matches[2]
			@badCode code
			return false

		embedData =
			portalId: matches[1]
			ctaId: matches[2]

		# find image
		matches = code.match ///
			(\<img[^\>]+>)
		///i
		if matches and matches[1]
			$image = $(matches[1])
			$image.removeAttr 'style'
			$image.removeAttr 'id'
			$image.removeAttr 'class'

			embedData.content = $image
		else
			embedData.content = "<strong>HUBSPOT CTA BUTTON (#{embedData.ctaId})</strong>"

		@embedCta embedData

	embedCta: (data) ->
		$place = this.$el.find('.medium-insert-cta-active')

		options =
			portalId: data.portalId
			ctaId: data.ctaId

		optionsJson = JSON.stringify options

		$cta = $("<div class='hubspot-cta' data-hubspot-cta data-hubspot-cta-options='#{optionsJson}'></div>")
		$cta.append data.content

		$wrapper = @wrapCta $cta

		$place.before $wrapper
		$place.remove()

		@core.triggerInput()


	# remove bad code from the page
	badCode: (code) ->
		$place = @$el.find('.medium-insert-cta-active')

		$empty = $(@templates.empty)
		$place.before $empty
		$place.remove()

		@core.triggerInput()
		@core.moveCaret $empty

		alert 'please insert a valid hubspot CTA code'

	removeCta: (e) ->
		if e.which is 8 or e.which is 46
			$cta = @$el.find('.medium-insert-cta-selected')

			if $cta.length
				e.preventDefault()

				$empty = $(@templates.empty)

				$cta.before($empty)
				$cta.remove()

				# Hide addons
				@core.hideAddons()

				@core.moveCaret($empty)
				@core.triggerInput()


	selectCta: (e) ->
		$cta = if $(e.target).hasClass('medium-insert-cta') then $(e.target) else $(e.target).closest('.medium-insert-cta')
		$cta.addClass('medium-insert-cta-selected')

	unselectCta: (e) ->
		$el = if $(e.target).hasClass('medium-insert-cta') then $(e.target) else $(e.target).closest('.medium-insert-cta')
		$cta = @$el.find('.medium-insert-cta-selected')

		if $el.hasClass 'medium-insert-cta-selected'
			$cta.not($el).removeClass('medium-insert-cta-selected')
			return

		$cta.removeClass('medium-insert-cta-selected')


	editorSerialize: ->
		data = @_preSerialize()

		$.each data, (key) ->
			$data = $('<div />').html(data[key].value)

			$data.find('.medium-insert-cta')
				.removeAttr('contenteditable')
				.removeClass('medium-insert-cta-selected')

			$data.find('.medium-insert-cta-overlay').remove()

			data[key].value = $data.html()

		data


	wrapCta: ($cta) ->
		$wrapper = $('<div class="medium-insert-cta" contenteditable="false"></div>')

		$wrapper.append $cta
		$wrapper.append "<div class='medium-insert-cta-overlay'></div>"

		return $wrapper

$.fn[pluginName + addonName] = (options) ->
	@each ->
		unless $.data(@, 'plugin_' + pluginName + addonName)
			$.data @, 'plugin_' + pluginName + addonName, new MediumEditorInsertCta(@, options)
		return

